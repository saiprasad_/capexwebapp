/* <project-root>/webpack.config.worker.js */

const ProgressPlugin = require('webpack/lib/ProgressPlugin');
const { NoEmitOnErrorsPlugin } = require('webpack');
const { AngularCompilerPlugin } = require('@ngtools/webpack')

module.exports = {
  'mode': 'production',
  'devtool': 'none',
  'resolve': {
    'extensions': [
      '.ts',
      '.js',
      '.tsx'
    ],
    'modules': [
      './node_modules'
    ]
  },
  'resolveLoader': {
    'modules': [
      './node_modules'
    ]
  },
  'entry': {
    './ClientApp/appResourceFiles/workers/httpWebWorker': [
      './ClientApp/webWorker/main.worker.ts'
    ],'./ClientApp/appResourceFiles/workers/WebWorker': [
      './ClientApp/worker/worker.util.ts'
    ]
  },
  'output': {
    'path': process.cwd(),
    'filename': '[name].js'
  },
  'watch': false,
  'module': {
    'rules': [
      {
        'enforce': 'pre',
        'test': /\.js$/,
        'loader': 'source-map-loader',
        'exclude': [
          /\/node_modules\//
        ]
      },
      {
        'test': /\.json$/,
        'loader': 'json-loader'
      },
      {
        'test': /\.ts$/,
        'loader': '@ngtools/webpack'
      }
    ]
  },
  'plugins': [
    new NoEmitOnErrorsPlugin(),
    new ProgressPlugin(),
    new AngularCompilerPlugin({
      'tsConfigPath': './ClientApp/tsconfig.worker.json',
      'entryModule': './ClientApp/webWorker/main.worker.ts'
      // ,'entryModule': ['./ClientApp/webWorker/main.worker.ts', //for dev-worker
      //  './ClientApp/worker/worker.util.ts']
    })
  ]
};
