using System;
using System.Collections.Generic;
using System.Fabric;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using CapexApp.commonconfig;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.ServiceFabric.Services.Communication.AspNetCore;
using Microsoft.ServiceFabric.Services.Communication.Runtime;
using Microsoft.ServiceFabric.Services.Runtime;
using CapexApp.Models;
using CapexApp.SqlUtil;
using ICommonInterfaces.Model;
using Newtonsoft.Json;

namespace CapexApp
{
  /// <summary>
  /// The FabricRuntime creates an instance of this class for each service type instance. 
  /// </summary>
  internal sealed class CapexApp : StatelessService
  {
    public PersistanceQueryService queryServiceInstance;
    private StatelessServiceContext context;
    public CapexApp(StatelessServiceContext context) : base(context)
    {
      this.context = context;
      queryServiceInstance = new PersistanceQueryService(context);
    }




    public async Task<string> OnRouteMessageaAsync(MessageWrapper message)
    {
      string result = null;
      MessageWrapper wrapper = new MessageWrapper();
      try
      {
        wrapper = JsonConvert.DeserializeObject<MessageWrapper>(JsonConvert.SerializeObject(message));
        switch (wrapper.MessageKind)
        {
          case MessageKinds.READ:
            result = await ReadAsync(wrapper);
            break;
        }
        wrapper.Payload = result;
      }
      catch (Exception ex)
      {
        throw;
      }
      return result;
    }


    public async Task<string> ReadAsync(MessageWrapper message)
   {
      string result = null;
      try
      {
        switch (message.DataType)
        {
          case Constants.SCHEMA:
            result = await queryServiceInstance.readSchemaPageFromSql(message);
            break;
          case Constants.READ_DATA:
            result = await queryServiceInstance.readDataFromSql(message);
            break;
          case Constants.FILTER:
            result = await queryServiceInstance.readDataFromSql(message);
            break;

        }
      }
      catch (Exception ex)
      {
        throw;
      }
      return result;
    }


    /// <summary>
    /// Optional override to create listeners (like tcp, http) for this service instance.
    /// </summary>
    /// <returns>The collection of listeners.</returns>
    protected override IEnumerable<ServiceInstanceListener> CreateServiceInstanceListeners()
    {
      return new ServiceInstanceListener[]
      {
        new ServiceInstanceListener(serviceContext =>
        {
          var protocolConfigured = CommonConfiguration.GetConfig(serviceContext,"https");
          bool isHttps = false;
          Boolean.TryParse(protocolConfigured, out isHttps);
          var endpointName = isHttps ? "ServiceEndpoint2" : "ServiceEndpoint1";
          return  new KestrelCommunicationListener(serviceContext, endpointName, (url, listener) =>
          {
            return new WebHostBuilder()
            .UseKestrel(op =>
              {
                try {
                  if(isHttps) {
                    var cert = GetCertificateFromStore(serviceContext);
                    // var port =int.Parse( url.Substring(url.IndexOf("+:") + 2));
                    int port = serviceContext.CodePackageActivationContext.GetEndpoint("ServiceEndpoint1").Port;
                    ServiceEventSource.Current.ServiceMessage(serviceContext, "Kestrel Triggered");

                    var ipString = FabricRuntime.GetNodeContext().IPAddressOrFQDN;
                    IPAddress ip = null;

                    if (ipString=="localhost")
                      ip = (Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(i => i.AddressFamily == AddressFamily.InterNetwork) ?? IPAddress.Loopback);
                    else
                      IPAddress.TryParse(serviceContext.NodeContext.IPAddressOrFQDN, out ip);

                    ServiceEventSource.Current.ServiceMessage(serviceContext, "IpAddress \t " + serviceContext.NodeContext.IPAddressOrFQDN);

                    op.Listen(ip, port, listenConfig=>listenConfig.UseHttps(cert));
                  }
                }
                catch (Exception ex)
                {
                  ServiceEventSource.Current.ServiceMessage(serviceContext, "Exception in Kestrel: "+ex.Message+"\t" +ex.StackTrace );
                  throw ex;
                }
              }
            )
            .ConfigureServices(services => services.AddSingleton<StatelessServiceContext>(serviceContext))
            .UseContentRoot(Directory.GetCurrentDirectory())
            .UseStartup<Startup>()
            .UseServiceFabricIntegration(listener, ServiceFabricIntegrationOptions.None)
            .UseUrls(url)
            .Build();
          });
        })
      };
    }

    private X509Certificate2 GetCertificateFromStore(ServiceContext serviceContext)
    {
      var config = serviceContext.CodePackageActivationContext.GetConfigurationPackageObject("Config");

      //Load Certificate from Store by Thumbprint
      //Uncomment this if you want to load cert from Computer store.
      var thumbprint = config.Settings.Sections["ServiceConfig"].Parameters["CertThumbprint"].Value;

      var store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
      try
      {
        store.Open(OpenFlags.ReadOnly);
        var certCollection = store.Certificates;
        var currentCerts = certCollection.Find(X509FindType.FindByThumbprint, thumbprint, false);
        return currentCerts.Count == 0 ? null : currentCerts[0];
      }
      finally
      {
        store.Close();
      }
    }
  }
}
