using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CapexApp.Models
{
  public static class Constants
  {
    public const string FIXED_MENU = "fixedmenu";
    public const string SCHEMA_PAGE = "schemapage";
    public const string READ_DATA = "readdata";
    public const string USER_DETAIL_TABLE = "userdetailtable";
    public const string SCHEMA = "schema";
    public const string FILTER = "filter";



    public const string COMPANYTYPES = "CompanyTypes";
    public const string PEOPLEUSERSETTING = "People_UserSetting";
    public const string PEOPLEANDCOMPANYUSERSETTING = "People & Companies_UserSetting";
    public const string SECURITYGROUPINFO = "securitygroupinfo";
    public const string CHECKBIPERMISSION = "checkBIPermission";
    public const string SECURITYGROUP = "SecurityGroup";
    public const string USERPERFERENCE = "UserPreference";
    public const string CONSTRUCTION = "Construction";
    public const string MAPS = "Maps";
    public const string SHAREDLIST = "SharedList";
    public const string PEOPLEANDCOMPANIES = "People & Companies";
    public const string PEOPLE = "People";
    public const string USERLIST = "UserList";
    public const string BUSINESSINTELLIGENCE = "Business Intelligence";
    public const string SEARCHTREE = "SearchTree";
    public const string LOADFORMDATA = "LoadFormData";
    public const string REPORTDATA = "ReportData";
    public const string READTABLEDATA = "readtabledata";
    public const string CONNECTIONSTRINGTENANTID = "ConnectionStringTenantId";
    public const string CONNECTIONSTRINGTENANTNAME = "ConnectionStringTenantName";
    public const string CONNECTIONSTRINGTYPE = "ConnectionStringType";
    public const string REVIEWANDAPPROVALS = "UPDATE_REVIEW_APPROVAL";
    public const string PDFDOC = "PDFDOC";

    public const string integrationvisurConnectionString = "Server=tcp:integrationvisur.database.windows.net,1433;Initial Catalog=constructionDB;Persist Security Info=False;User ID=integrationvisur;Password=D657#@$$sdcrt??;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
    public const string constructiontestConnectionString = "Server=tcp:constructiontest.database.windows.net,1433;Initial Catalog=constructiontest;Persist Security Info=False;User ID=constructionvisur;Password=S@3!@dfrqwe@visur;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
    public const string VisurconstructionDemoLiveConnectionString = "Server=tcp:constructiondemolive.database.windows.net,1433;Initial Catalog=constructiondemo;Persist Security Info=False;User ID=constructionlive;Password=GYtmUp@RQB6MrhC;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

  }

  public static class QueryDataType
  {
    //public const string SCHEMA_PAGE = "schemapage";
  }

  public static class TableNames
  {
    public const string IMAGES = "images";
    public const string UserSettings = "UserSettings";
    public const string UserTable = "User";
  }


  public static class MessageKinds
  {
    public const string READ = "READ";
    public const string CREATE = "CREATE";
    public const string UPDATE = "UPDATE";
    public const string DELETE = "DELETE";
  }

  public static class SQLiteCommonColumns
  {
    public const string EntityId = "entityid";
    public const string EntityName = "entityname";
    public const string EntityType = "entitytype";
    public const string Application = "application";
    public const string Parent = "parent";
    public const string ParentEntityId = "parententityid";
    public const string UUID = "uuid";
    public const string CreatedBy = "createdby";
    public const string Payload = "payload";
    public const string Image = "image";
    public const string Thumbnail = "thumbnail";
    public const string Altitude = "altitude";
    public const string Latitude = "latitude";
    public const string Longitude = "longitude";
    public const string Type = "type";
    public const string ModifiedBy = "modifiedby";
    public const string TenantId = "tenantid";
    public const string TenantName = "tenantname";
    public const string ModifiedDateTime = "modifieddatetime";
    public const string SyncDateTime = "syncdatetime";
    public const string CreatedDateTime = "createddatetime";
    public const string CreateStatus = "createstatus";
    public const string UpdateStatus = "updatestatus";
    public const string DeleteStatus = "deletestatus";
    public const string SyncStatus = "syncstatus";
    public const string IsDeleted = "isdeleted";
    public const string EntityTimeZone = "entitytimezone";
    public const string IconFileName = "iconfilename";
    public const string ID = "id";
    public const string SchemaPage = "schemapage";
    public const string LeftMenu = "leftmenu";
    public const string RightMenuID = "rightmenuid";
    public const string UserName = "username";
    public const string EmailId = "emailid";
    public const string UserId = "userid";
    public const string LogInTime = "logintime";
    public const string DataPointer = "datapointer";
    public const string IsDefault = "isdefault";
  }

  public class ParsePayload
  {
    public string tableName { get; set; }
    public string dataType { get; set; }
    public string entityId { get; set; }
    public string filterBy { get; set; }
    public bool primary { get; set; }
    public string key { get; set; }
    public string parentEntityId { get; set; }
    public string type { get; set; }
    public string query { get; set; }
  }
}
