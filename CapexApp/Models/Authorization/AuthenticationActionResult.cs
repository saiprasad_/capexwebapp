using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapexApp.Models.Authorization
{
  /// <summary>
  /// On UnAuthorized User Parsing Context in format of http response
  /// </summary>
  public class AuthenticationActionResult : IActionResult
  {
    private readonly AuthenticationResult _result;

    public AuthenticationActionResult(AuthenticationResult result)
    {
      _result = result;
    }

    public async Task ExecuteResultAsync(ActionContext context)
    {
      var objectResult = new ObjectResult(_result.Exception ?? _result.Data)
      {
        StatusCode = _result.Exception != null ? StatusCodes.Status401Unauthorized : StatusCodes.Status200OK
      };

      await objectResult.ExecuteResultAsync(context);
    }
  }
}
