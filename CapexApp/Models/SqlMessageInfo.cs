﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapexApp.Models
{
    class SqlMessageInfo
    {
        public string Operation { get; set; }
        public SqlConnectionStringInfo Info { get; set; }
    }
}
