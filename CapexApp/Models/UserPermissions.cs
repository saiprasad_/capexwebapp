using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapexApp.Models
{
    public class UserPermissions
    {
        public bool Write { get; set; }
        public bool Read { get; set; }
        public bool Delete { get; set; }
        public bool AddParticipants { get; set; }
        public bool RemoveParticipants { get; set; }
       

    }
}
