﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapexApp.Models
{
    public class ADLMessageInfo
    {
        public string Operation { get; set; }
        public ADLcredentialsInfo Info { get; set; }
    }
}
