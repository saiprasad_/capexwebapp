﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapexApp.Models
{
    public class RoleInfo
    {
        public string RoleId;
        public string Name;
        public string read;
        public string write;
        public string delete;
        public string addParticipants;
        public string removeParticipants;
    }
}
