using CapexApp.commonconfig;
using CapexApp.Models;
using ICommonInterfaces.KeyvaultService;
using System;
using System.Collections.Generic;
using System.Fabric;
using System.Linq;
using System.Threading.Tasks;

namespace CapexApp.keyvault
{
    public class ConnectionStringFromKeyVault
    {
        public KeyvaultServiceConnectionStringApi keyvaultServiceConnectionStringApi;
        private readonly ServiceContext context;
        public ConnectionStringFromKeyVault(ServiceContext context)
        {
            keyvaultServiceConnectionStringApi = new KeyvaultServiceConnectionStringApi();
            this.context = context;
        }

        public async Task<string> GetConnectionStringFromKeyVaultTenant(string tenantId, string applicationName, string connectionStringType= "SqlCredentialAdo")
        {
            string connectionString = await keyvaultServiceConnectionStringApi.GetConnectionStringFromKeyVault(tenantId,applicationName,connectionStringType);
            return connectionString;
        }

        public async Task<string> GetConnectionStringFromKeyVaultServer(string applicationName)
        {
            string connectionStringTenantId = CommonConfiguration.GetConfig(context, Constants.CONNECTIONSTRINGTENANTID);
            string connectionStringType = CommonConfiguration.GetConfig(context, Constants.CONNECTIONSTRINGTYPE);
            string connectionString = await keyvaultServiceConnectionStringApi.GetConnectionStringFromKeyVault(connectionStringTenantId, applicationName, connectionStringType);
            return connectionString;
        }
    }
}
