using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Fabric;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using CapexApp.commonconfig;
using CapexApp.Models;
using CapexApp.Util;
using ICommonInterfaces;
using Microsoft.ApplicationInsights;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Management.DataLake.Store;
using Microsoft.Rest.Azure.Authentication;
using Microsoft.ServiceFabric.Services.Remoting.Client;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ICommonInterfaces.Model;
using CapexApp.Models.Authorization;
using CapexApp.Models.RestClient;
using CapexApp.Ignite;
using Models.common.FDC;

namespace CapexApp.Controllers
{
  [Route("api/[controller]")]
  public class AlThingsController : Controller
  {
    TelemetryClient tc = new TelemetryClient();
    private readonly IgniteClient igniteClient = new IgniteClient();
    private readonly IgniteThinClientService _igniteThinClientService = new IgniteThinClientService(CommonConfiguration.GetConfiguration("endpoint"));
    public readonly StatelessServiceContext context;
    private CapexApp capexAppInstance;
    
    public AlThingsController(StatelessServiceContext context)
    {
      this.capexAppInstance = new CapexApp(context);
      this.context = context;
    }


    // GET api/values/5
    // [HttpGet("{tenantName}")]
    /// <summary>
    ///
    /// </summary>
    /// <param name="tenantName"></param>
    /// <returns></returns>
    [HttpGet("tenantId/{tenantName}")]
    public string GetTenantId(string tenantName,string applicationName)
    {
      var tenantID = new IgniteClient().GetTenantIDAsync(tenantName,applicationName);
      return tenantID.ToString();
      //return "c091e548-b45a-49b4-b8ec-2cb5e27c7af6";
    }



    [HttpGet("search")]
    [AuthActionFilter]
    public async Task<string> GetSearch(String term, String tenantName)
    {

      try
      {
        return new IgniteClient().GetSearchItems(term, tenantName);
      }
      catch (Exception e)
      {
        ServiceEventSource.Current.Message("Exception Occurred At GetSearch() in AlThingsController :  " + e.Message);
        tc.TrackTrace("Tracing GetSearch AlThingsController");
        tc.TrackException(e);
        tc.Context.Device.OperatingSystem = Environment.OSVersion.ToString();
      }

      return "[]";
    } 


    private async Task<string> GetClient(string url)
    {
      try
      {
        string result = null;

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        request.AutomaticDecompression = DecompressionMethods.GZip;

        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        using (Stream stream = response.GetResponseStream())
        using (StreamReader reader = new StreamReader(stream))
        {
          result = reader.ReadToEnd();
        }
        Console.WriteLine(result);
        return await Task.FromResult(result);
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message);
        ServiceEventSource.Current.Message("Exception Occurred At GetClient() in HomeController :  " + ex.Message);
        tc.TrackTrace("Tracing GetClient() HomeController");


        string innerExceptionMessage = null;
        if (ex.InnerException != null)
        {
          innerExceptionMessage = ex.InnerException.Message + "\t" + ex.InnerException.Data + "\t" + ex.InnerException.StackTrace;
        }
        string msgInfo = "{\n\"MethodName\": \"GetClient\",\n\"URL\":" + url + ",\n\"Message\":" + ex.Message + ",\n\"InnerException\":" + innerExceptionMessage + ",\n\"StackTrace\":" + ex.StackTrace + "\n}";
        tc.Context.Device.OperatingSystem = Environment.OSVersion.ToString();
        tc.TrackException(new Exception("\n" + msgInfo));


        //tc.TrackException(e);
        //tc.Context.Device.OperatingSystem = Environment.OSVersion.ToString();
        return await Task.FromResult("false");
        //throw new Exception(e.Message);

        throw new Exception("\n" + msgInfo);
      }
    }

    private async Task<String> PostClient(string url, string postData, string contenttype)
    {
      try
      {

        // for https
        //ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);

        var request = (HttpWebRequest)WebRequest.Create(url);
        var data = Encoding.ASCII.GetBytes(postData);
        request.Method = "POST";
        request.ContentType = contenttype;
        request.ContentLength = data.Length;
        using (var stream = request.GetRequestStream())
        {
          stream.Write(data, 0, data.Length);
        }
        var response = (HttpWebResponse)request.GetResponse();

        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

        return await Task.FromResult(responseString);
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message);
        ServiceEventSource.Current.Message("Exception Occurred At PostClient() in HomeController :  " + ex.Message);
        tc.TrackTrace("Tracing PostClient() HomeController");


        string innerExceptionMessage = null;
        if (ex.InnerException != null)
        {
          innerExceptionMessage = ex.InnerException.Message + "\t" + ex.InnerException.Data + "\t" + ex.InnerException.StackTrace;
        }
        string msgInfo = "{\n\"MethodName\": \"PostClient\",\n\"URL\":" + url + ",\n\"PostData\":" + postData + ",\n\"ContentType\":" + contenttype + ",\n\"Message\":" + ex.Message + ",\n\"InnerException\":" + innerExceptionMessage + ",\n\"StackTrace\":" + ex.StackTrace + "\n}";
        tc.Context.Device.OperatingSystem = Environment.OSVersion.ToString();
        tc.TrackException(new Exception("\n" + msgInfo));
        throw new Exception("\n" + msgInfo);


        //tc.TrackException(e);
        //tc.Context.Device.OperatingSystem = Environment.OSVersion.ToString();
        //return "ERROR";
        //throw new Exception(e.Message);
      }
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="key"></param>
    /// <param name="column"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    [HttpGet("GetTenantInfoJson")]
    public async Task<string> GetTenantInfoJson(string key, string column)
    {
      tc = new TelemetryClient();
      tc.TrackRequest("Track GetTenantInfoJson TenantManagement  Request started", DateTimeOffset.UtcNow, new TimeSpan(0, 0, 3), "200", true);
      tc.TrackMetric("GetTenantInfoJson TenantManagement Metric", 100);
      tc.TrackEvent("Tracked Event GetTenantInfoJson TenantManagement");

      return await igniteClient.ReadTenantAsync(key, column);

    }

    [HttpPost("PeopleFabric/CreateUpdatePeopleEntities")]
    public async Task<String> CreateUpdatePeopleEntities([FromBody] MessageWrapper message)
    {
      string result = "";
      try
      {
        tc.TrackTrace("CreateUpdatePeopleEntities()");
        if (message!=null)
        {
          var wrapper = JsonConvert.SerializeObject(message);
          var client = ServiceProxy.Create<ICommunication>(new Uri("fabric:/ConstructionApp/ConstructionApi"), listenerName: "Construction");
          result = await client.OnRouteMessageaAsync(wrapper);
          tc.TrackTrace("CreateUpdatePeopleEntities() result:"+result);

        }
      }
      catch (Exception ex)
      {
        tc.TrackException(new Exception("\n Exception In CreatePeopleEntities in Controller =" + ex));
        throw;
      }
      return result;
    }


    [HttpPost("GetSchema")]
    public async Task<string> GetSchema([FromBody] MessageWrapper wrapper)
    {
      Uri uri = null;
      ICommunication client = null;
      try
      {
        var obj = JObject.Parse(wrapper.Payload);
        var result = await _igniteThinClientService.GetFormSchemaByNameAsync(obj["SchemaName"].ToString(), wrapper.TenantID);
        wrapper.Payload = JsonConvert.SerializeObject(result);
        return JsonConvert.SerializeObject(wrapper);
      }
      catch (Exception ex)
      {
        throw;
      }
    }
    [HttpPost("GetSchemaData")]
    public async Task<string> GetSchemaData([FromBody] MessageWrapper wrapper)
    {
      Uri uri = null;
      ICommunication client = null;
      try
      {
        tc.TrackTrace("GetSchemaData()");
        switch (wrapper.DataType)
        {
          case "FDCData":
            uri = new Uri("fabric:/FieldDataCaptureApp/FieldDataCapture");
            break;
          case "People":
            tc.TrackTrace("GetSchemaData People");
            wrapper.DataType = wrapper.EntityType;
            uri = new Uri("fabric:/CompaniesServiceApp/CompaniesServiceApi");
            break;
          case "Bi":
            uri = new Uri("fabric:/BusinessIntelligenceFabricApp/BusinessIntelligenceFabricApi");
            break;
          default:
            break;
        }


        //client = ServiceProxy.Create<ICommunication>(uri, listenerName: "People");

        client = ServiceProxy.Create<ICommunication>(uri, listenerName: "People");
        var result = await client.ReadAsync(wrapper);
        tc.TrackTrace("GetSchemaData result"+result);

        wrapper.Payload = result;
      }
      catch (Exception ex)
      {
        tc.TrackTrace("GetSchemaData Exception" + ex);
        tc.TrackException(ex);
        throw ex;
      }

      return JsonConvert.SerializeObject(wrapper);
    }

    [HttpPost("GetDataFromBackEnd")]
    // [AuthActionFilter]   commented because of cross origin issue
    public async Task<string> GetDataFromBackEnd([FromBody] MessageWrapper wrapper)
    {
      var tc = new TelemetryClient();
      ICommunication client = null;
      string serviceNotExists = null;
      try
      {
        tc.TrackTrace("GetDataFromBackEnd()");
        switch (wrapper.EntityType)
        {
          case Constants.SEARCHTREE:
          case Constants.BUSINESSINTELLIGENCE:
          case Constants.PEOPLE:
          case Constants.PEOPLEANDCOMPANIES:
          case Constants.SHAREDLIST:
          case Constants.MAPS:
          case Constants.CONSTRUCTION:
          case Constants.USERPERFERENCE:
          case Constants.USERLIST when wrapper.Fabric == Constants.BUSINESSINTELLIGENCE.ToUpper():
          case Constants.SECURITYGROUP when (wrapper.EntityID == Constants.SECURITYGROUPINFO):
          case Constants.CHECKBIPERMISSION when (wrapper.EntityID == Constants.CHECKBIPERMISSION):
             this.getListener(ref client,ref serviceNotExists,wrapper.EntityType, wrapper.Fabric, wrapper.EntityID);
            break;

          default:
            switch (wrapper.DataType)
            {
              case Constants.PEOPLE:
              case Constants.PEOPLEANDCOMPANIES:
              case Constants.PEOPLEUSERSETTING:
              case Constants.PEOPLEANDCOMPANYUSERSETTING:
              case Constants.COMPANYTYPES:
              case Constants.BUSINESSINTELLIGENCE:
              case Constants.LOADFORMDATA:
              case Constants.REPORTDATA:
              case Constants.PDFDOC:
              case Constants.REVIEWANDAPPROVALS:
              case Constants.READTABLEDATA:
                   this.getListener(ref client,ref serviceNotExists, wrapper.DataType, wrapper.Fabric, wrapper.EntityID);
                break;
              default:
                serviceNotExists = null;
                break;
            }
            break;
        }
        if (serviceNotExists != null)
        {
          var result = string.Empty;
          if (wrapper.DataType.ToUpper()== Constants.REVIEWANDAPPROVALS.ToUpper())
            result = await client.OnRouteMessageaAsync(JsonConvert.SerializeObject(wrapper));
          else
            result = await client.ReadAsync(wrapper);

          tc.TrackTrace("GetDataFromBackEnd() result" + result);
          if (wrapper.DataType == Constants.REPORTDATA)
            wrapper = JsonConvert.DeserializeObject<MessageWrapper>(result);
          else
            wrapper.Payload = result;
        }
      }
      catch (Exception ex)
      {
        tc.TrackTrace("GetDataFromBackEnd() Exception" + ex);

        tc.TrackException(ex);
        wrapper.EntityType = "error";
        if (ex.Message == "One or more errors occurred.")
        {
          serviceNotExists = ex.InnerException.Message;
        }
        wrapper.Payload = ex.Message + "\t" + wrapper.EntityType + "\t" + serviceNotExists + "\t" + ex.StackTrace;
        //return ex.Message + "\t" + ex.StackTrace;
      }
      return JsonConvert.SerializeObject(wrapper);
    }


    [HttpPost("ReadDataFromSql")]
    public async Task<string> ReadDataFromSql([FromBody] MessageWrapper wrapper)
    {
      var tc = new TelemetryClient();
      string result = "";
      try
      {
        tc.TrackTrace("ReadDataFromSql()");
        result = await capexAppInstance.OnRouteMessageaAsync(wrapper);
        wrapper.Payload = result;
      }
      catch (Exception ex)
      {
        tc.TrackTrace("ReadDataFromSql() Exception" + ex);
        tc.TrackException(ex);
        throw;
      }
      return JsonConvert.SerializeObject(wrapper);
    }
    public void getListener(ref ICommunication client, ref string serviceNotExists, string option,string fabric,string EntityId)
    {
      switch (option)
      {
        case Constants.SEARCHTREE:
          client = ServiceProxy.Create<ICommunication>(new Uri("fabric:/SearchApp/SearchApi"), listenerName: "Search");
          serviceNotExists = "fabric:/SearchApp/SearchApi";
          break;

        case Constants.BUSINESSINTELLIGENCE:
        case Constants.USERLIST when fabric == Constants.BUSINESSINTELLIGENCE.ToUpper():
          client = ServiceProxy.Create<ICommunication>(new Uri("fabric:/BusinessIntelligenceFabricApp/BusinessIntelligenceFabricApi"), listenerName: "Bi");
          serviceNotExists = "fabric:/BusinessIntelligenceFabricApp/BusinessIntelligenceFabricApi";
          break;

        case Constants.PEOPLE:
        case Constants.PEOPLEANDCOMPANIES:
        case Constants.SHAREDLIST:
        case Constants.PEOPLEUSERSETTING:
        case Constants.PEOPLEANDCOMPANYUSERSETTING:
        case Constants.COMPANYTYPES:
          client = ServiceProxy.Create<ICommunication>(new Uri("fabric:/CompaniesServiceApp/CompaniesServiceApi"), listenerName: "People");
          serviceNotExists = "fabric:/CompaniesServiceApp/CompaniesServiceApi";
          break;

        case Constants.MAPS:
        case Constants.CONSTRUCTION:
        case Constants.REVIEWANDAPPROVALS:
        case Constants.READTABLEDATA:
          client = ServiceProxy.Create<ICommunication>(new Uri("fabric:/ConstructionApp/ConstructionApi"), listenerName: "Construction");
          serviceNotExists = "fabric:/ConstructionApp/ConstructionApi";
          break;

        case Constants.LOADFORMDATA:
          client = ServiceProxy.Create<ICommunication>(new Uri("fabric:/ConstructionSearchApp/ConstructionSearchApi"), listenerName: "ConstructionSearch");
          serviceNotExists = "fabric:/ConstructionSearchApp/ConstructionSearchApi";
          break;

        case Constants.REPORTDATA:
          client = ServiceProxy.Create<ICommunication>(new Uri("fabric:/ConstructionReportingServiceApp/ConstructionReportingServiceApi"), listenerName: "ReportingService");
          serviceNotExists = "fabric:/ConstructionReportingServiceApp/ConstructionReportingServiceApi";
          break;

        case Constants.USERPERFERENCE:
          client = ServiceProxy.Create<ICommunication>(new Uri("fabric:/UserPreferenceApp/UserPreferenceApi"), listenerName: "UserPreference");
          serviceNotExists = "fabric:/UserPreferenceApp/UserPreferenceApi";
          break;

        case Constants.SECURITYGROUP when (EntityId == Constants.SECURITYGROUPINFO):
        case Constants.CHECKBIPERMISSION when (EntityId == Constants.CHECKBIPERMISSION):
          client = ServiceProxy.Create<ICommunication>(new Uri("fabric:/AuthorizationApp/AuthorizationApi"), listenerName: "Authorization");
          serviceNotExists = "fabric:/AuthorizationApp/AuthorizationApi";
          break;
        case Constants.PDFDOC:
          client = ServiceProxy.Create<ICommunication>(new Uri("fabric:/ConstructionReportingServiceApp/ConstructionReportingServiceApi"), listenerName: "ReportingService");
          serviceNotExists = "fabric:/ConstructionReportingServiceApp/ConstructionReportingServiceApi";
          break;
        default:
          serviceNotExists = null;
          break;
      }
    }


    [HttpGet("GetZoomdataClient")]
    public async Task<string> GetZoomdataClient(string url, string accesstoken)
    {
      string result = JsonConvert.SerializeObject(new { token = "undefined" }, Formatting.Indented);
      try
      {
        if (accesstoken != "null" && accesstoken != null)
        {
          HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
          request.Headers.Add("Authorization", accesstoken);
          request.Accept = "application/vnd.zoomdata.v2+json";
          request.AutomaticDecompression = DecompressionMethods.GZip;

          using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
          using (Stream stream = response.GetResponseStream())
          using (StreamReader reader = new StreamReader(stream))
          {
            result = reader.ReadToEnd();
          }
        }
        Console.WriteLine(result);
      }
      catch (Exception ex)
      {
        throw;
      }
      return await Task.FromResult(result);

    }

    [HttpGet("CreateCopyDashboard")]
    public async Task<string> CreateCopyDashboard(string url, string accesstoken, string dashboardName)
    {
      string result = JsonConvert.SerializeObject(new { result = "Not Created" }, Formatting.Indented);
      try
      {
        if (accesstoken != "null" && accesstoken != null)
        {
          //Export Dashboard
          var getDashboardJson = await getRequest(url, accesstoken);

          //Import Dashboard
          if (getDashboardJson != null)
          {
            url = CommonConfiguration.GetConfig(context, "ZoomData") + "/api/bookmarks/import";
            var postData = getDashboardJson;
            var contenttype = "application/vnd.zoomdata.v2+json";
            var type = "POST";
            var getpostresonse = await postrequest(url, accesstoken, postData, contenttype, type);
            if (getpostresonse != null)
            {
              var parsingconnection = JsonConvert.DeserializeObject<dynamic>(getpostresonse);
              var dashboardid = parsingconnection["dashboards"][0]["id"];

              //read imported Dashboard
              url = CommonConfiguration.GetConfig(context, "ZoomData") + "/api/bookmarks/" + dashboardid;
              var getimportedDashJson = await getRequest(url, accesstoken);

              if (getimportedDashJson != null)
              {
                ////rename imported Dashboard
                var data = JsonConvert.DeserializeObject<dynamic>(getimportedDashJson);
                data["name"] = dashboardName;  //New Name
                postData = JsonConvert.SerializeObject(data);
                contenttype = "application/vnd.zoomdata.v2+json";
                type = "PUT";
                result = await postrequest(url, accesstoken, postData, contenttype, type);
              }
            }
          }
        }
        Console.WriteLine(result);
      }
      catch (Exception ex)
      {

      }
      return await Task.FromResult(result);

    }

    public async Task<string> getRequest(string url, string accesstoken)
    {
      string result = null;
      try
      {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        request.Headers.Add("Authorization", accesstoken);
        request.AutomaticDecompression = DecompressionMethods.GZip;

        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        using (Stream stream = response.GetResponseStream())
        using (StreamReader reader = new StreamReader(stream))
        {
          result = reader.ReadToEnd();
        }
      }
      catch (Exception ex)
      {
        throw;
      }
      return result;
    }

    private async Task<string> postrequest(string url, string accesstoken, string postData, string contenttype, string type)
    {
      string result = null;
      try
      {
        var request = (HttpWebRequest)WebRequest.Create(url);
        // String encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
        //request.Headers.Add("Authorization", "Basic " + encoded);
        request.Headers.Add("Authorization", accesstoken);
        var data = System.Text.ASCIIEncoding.UTF32.GetBytes(postData.ToCharArray());//Encoding.ASCII.GetBytes(postData);
        request.Method = type;
        request.ContentType = contenttype;
        request.ContentLength = data.Length;
        using (var stream = request.GetRequestStream())
        {
          stream.Write(data, 0, data.Length);
        }
        var response = (HttpWebResponse)request.GetResponse();
        result = new StreamReader(response.GetResponseStream()).ReadToEnd();
      }
      catch (Exception)
      {
        throw;
      }
      return await Task.FromResult(result);
    }

    [HttpGet("GetDashBoardUrl")]
    public async Task<string> GetDashBoardUrl(string id, string tenantid, string tenantName)
    {
      string result = "";
      try
      {
        var token = await GetAdminToken(tenantid, tenantName);
        //var readUrl = CommonConfiguration.GetConfig(context, "Ignite") + "/service/entities?&key=" + id + "&cacheName=" + tenantid + "&type=Details";
        var dashResult = await _igniteThinClientService.GetDashboardinfo(tenantid, id);//igniteClient.IgniteCustomRestApi(readUrl);
        if (dashResult != null && dashResult != "" && dashResult != "{}")
        {
          result = JObject.Parse(dashResult)["Dashboardurl"].ToString();
          string[] spliting = result.Split('=');
          if (token != "")
            result = spliting[0] + "=native&access_token=" + token;
        }
        Console.WriteLine(result);
      }
      catch (Exception ex)
      {
        throw;
      }
      return await Task.FromResult(result);

    }


    [HttpPost("ZoomdataPostClient")]
    public async Task<string> ZoomdataPostClient([FromBody] JObject json)
    {
      string responseString = null;
      try
      {
        var res = JObject.Parse(json.ToString());
        var url = res["url"].ToString();
        // var username = res["username"].ToString();
        // var password = res["password"].ToString();
        var accesstoken = res["zoomdataaccesstoken"].ToString();
        var postData = res["postData"].ToString();
        var contenttype = res["contenttype"].ToString();
        var type = res["type"].ToString();
        responseString = await CallZoomdataClient(url, accesstoken, postData, contenttype, type);
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
      return await Task.FromResult(responseString);
    }
    private async Task<string> CallZoomdataClient(string url, string accesstoken, string postData, string contenttype, string type)
    {
      string result = null;
      try
      {
        var request = (HttpWebRequest)WebRequest.Create(url);
        // String encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
        //request.Headers.Add("Authorization", "Basic " + encoded);
        request.Headers.Add("Authorization", accesstoken);
        var data = Encoding.ASCII.GetBytes(postData);
        request.Method = type;
        request.ContentType = contenttype;
        request.ContentLength = data.Length;
        using (var stream = request.GetRequestStream())
        {
          stream.Write(data, 0, data.Length);
        }
        var response = (HttpWebResponse)request.GetResponse();
        result = new StreamReader(response.GetResponseStream()).ReadToEnd();
      }
      catch (Exception)
      {
        throw;
      }
      return await Task.FromResult(result);
    }
    [HttpPut("ZoomdataPutClient")]
    public async Task<string> ZoomdataPutClient([FromBody] JObject json)
    {
      string responseString = null;
      try
      {
        var res = JObject.Parse(json.ToString());
        var url = res["url"].ToString();
        // var username = res["username"].ToString();
        // var password = res["password"].ToString();
        var accesstoken = res["zoomdataaccesstoken"].ToString();
        var postData = res["postData"].ToString();
        var contenttype = res["contenttype"].ToString();
        var type = res["type"].ToString();
        responseString = await CallZoomdataClient(url, accesstoken, postData, contenttype, type);
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
      return await Task.FromResult(responseString);
    }

    [HttpGet("GetAdminToken")]
    public async Task<string> GetAdminToken(string tenantID, string tenanName)
    {
      try
      {
        string token = "";
        var admincredntial = await igniteClient.GetAdminCredetial(tenantID, tenanName);
        if (admincredntial == "" || admincredntial == "[]" || admincredntial == "{}" || admincredntial == null)
        { admincredntial = await igniteClient.GetAdminCredetial(tenantID, tenanName); }
        var domain = CommonConfiguration.GetConfig(context, "Extension");
        var zoomdataadminusername = admincredntial;
        string[] redirecturl = { "https://" + tenanName + domain };
        string url = CommonConfiguration.GetConfiguration("ZoomData");
        var postdata = new
        {
          accessTokenValiditySeconds = 604800,
          autoApprove = true,
          clientName = tenanName,
          registeredRedirectURIs = redirecturl
        };
        var data = JsonConvert.SerializeObject(postdata, Formatting.Indented);
        string result = await GetClientId(url, data, zoomdataadminusername, tenanName);
        if (result == "" || result == "[]" || result == "{}" || result == null)
        { result = await GetClientId(url, data, zoomdataadminusername, tenanName); }

        var parseing = JObject.Parse(result);
        string clientId = parseing["clientId"].ToString();
        token = await TokenPostClient(url, data, zoomdataadminusername, tenanName, clientId, admincredntial);
        return await Task.FromResult(token);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        ServiceEventSource.Current.Message("Exception Occurred At ReadTimeZoneBasedOnEntity() in AlThingsController :  " + e.Message);
        tc.TrackTrace("Tracing GetAdminCredetial AlThingsController");
        tc.TrackException(e);
        tc.Context.Device.OperatingSystem = Environment.OSVersion.ToString(); throw new Exception(e.Message);
        throw new HttpRequestException("exception occured while getting the time cone for this entity:\t" + e.Message + "\t" + e.StackTrace);
      }

    }


    public async Task<string> GetClientId(string base_url, string postdata, string user_Name, string client_Name)
    {
      var adminusername = CommonConfiguration.GetConfig(context, "ZoomDataUsername");
      var adminpassword = CommonConfiguration.GetConfig(context, "ZoomDataPassword");
      var url = base_url + "/api/oauth2/client";
      string responseString = "";
      var contentType = "application/vnd.zoomdata.v2+json";
      var request = (HttpWebRequest)WebRequest.Create(url);
      String encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(adminusername + ":" + adminpassword));
      request.Headers.Add("Authorization", "Basic " + encoded);
      var data = Encoding.ASCII.GetBytes(postdata);
      request.Method = "POST";
      request.ContentType = contentType;
      request.ContentLength = data.Length;
      using (var stream = request.GetRequestStream())
      {
        stream.Write(data, 0, data.Length);
      }
      var response = (HttpWebResponse)request.GetResponse();

      responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
      return await Task.FromResult(responseString);
    }


    public async Task<string> TokenPostClient(string base_url, string postdata, string user_Name, string client_Name, string client_Id, string adminuser)
    {
      string responseString = "";
      var adminusername = CommonConfiguration.GetConfig(context, "ZoomDataUsername");
      var adminpassword = CommonConfiguration.GetConfig(context, "ZoomDataPassword");
      var contentType = "application/vnd.zoomdata.v2+json";
      postdata = "{\"accessTokenValiditySeconds\": 6000000,\"clientName\":\"" + adminuser + "\",\"clientId\":\"" + client_Id + "\"}";
      string url = base_url + "/api/oauth/token?username=" + user_Name;
      var result = await CallAccessToken(url, contentType, postdata, adminusername, adminpassword);
      if (result == "" || result == "[]" || result == "{}" || result == null)
      { result = await CallAccessToken(url, contentType, postdata, adminusername, adminpassword); }

      var parseing = JObject.Parse(result);
      responseString = parseing["tokenValue"].ToString();

      return await Task.FromResult(responseString);
    }


    private async Task<string> CallAccessToken(string url, string contentType, string postData, string username, string password)
    {
      string responseString = "";
      var request = (HttpWebRequest)WebRequest.Create(url);
      String encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
      request.Headers.Add("Authorization", "Basic " + encoded);
      var data = Encoding.ASCII.GetBytes(postData);
      request.Method = "POST";
      request.ContentType = contentType;
      request.ContentLength = data.Length;
      using (var stream = request.GetRequestStream())
      {
        stream.Write(data, 0, data.Length);
      }
      var response = (HttpWebResponse)request.GetResponse();

      responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
      return await Task.FromResult(responseString);
    }

  }

  public class MyModel
  {
    public string Key { get; set; }
  }

}
