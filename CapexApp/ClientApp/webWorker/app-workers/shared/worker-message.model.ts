
export class WorkerMessage {
  topic: string;
  restMethodName: string;
  dataPayload: any;
  AppConfig:any;
  
  constructor(topic: string, restMethodName: string, dataPayload: any,AppConfig?:any) {
    this.topic = topic;
    this.restMethodName = restMethodName;
    this.dataPayload = dataPayload;
    this.AppConfig=AppConfig;
  }

  public static getInstance(value: any): WorkerMessage {
    const { topic, restMethodName, dataPayload ,AppConfig} = value;
    return new WorkerMessage(topic, restMethodName, dataPayload,AppConfig);
  }
}
