
export const WORKER_TOPIC = {
  RESTHTTP: 'REST_HTTP',
  WS: 'REST_WS',
  FDCWorker: 'FDC',
  GisMap:'GisMap',
  AbortEvent:'Abort'
};
