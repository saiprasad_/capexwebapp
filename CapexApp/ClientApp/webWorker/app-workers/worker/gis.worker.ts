import { WorkerMessage } from "../shared/worker-message.model";
import { restRequest, DEFAULT_REQUEST_OPTIONS, RequestResult, RequestOptions } from "../http/httpRequest";
import { WORKER_TOPIC } from "../shared/worker-topic.constants";
export class GisMapWorker {
    workerCtx: any;
    created: Date;

    feature: any;
    GeoJSON: any;

    constructor(workerCtx: any) {
        this.workerCtx = workerCtx;
        this.created = new Date();
    }

    workerBroker($event: MessageEvent): void {
        const { topic, restMethodName, dataPayload } = $event.data as WorkerMessage;
        const workerMessage = new WorkerMessage(topic, restMethodName, dataPayload);

        switch (topic) {
            case WORKER_TOPIC.GisMap:
                let methodType = workerMessage.dataPayload.method;
                let Resturl = workerMessage.dataPayload.url;
                let queryParams: any = workerMessage.dataPayload.queryParams ? workerMessage.dataPayload.queryParams : {};
                let Restbody: any = workerMessage.dataPayload.body ? workerMessage.dataPayload.body : null;
                let restOptions: RequestOptions = workerMessage.dataPayload.options ? workerMessage.dataPayload.options : DEFAULT_REQUEST_OPTIONS;
                console.log("Gis Worker Requesting...");
                this.restRequest(workerMessage, methodType, Resturl, queryParams, Restbody, restOptions);
                break;

            default:  // Add support for other workers here
                console.error('Topic Does Not Match');
        }
    }

    private returnWorkResults(message: WorkerMessage): void {
        this.workerCtx.postMessage(message);
    }

    private restRequest(workerMessage, methodType, Resturl, queryParams, Restbody, restOptions) {
        restRequest(methodType, Resturl, queryParams, Restbody, restOptions,workerMessage.restMethodName).then((responseObject: RequestResult) => {
            console.log("Gis Worker Response...");

            try {
                if (this.IsJsonString(responseObject.data)) {
                    responseObject.data = JSON.parse(responseObject.data);
                    var data = JSON.parse(responseObject.data);

                    switch (data.type) {
                        case "Production":
                            switch (data.subType) {
                                case "Vent":
                                case "Fuel":
                                case "Flare":
                                case "LiftGas":
                                case "FlowPressure":
                                case "heatMapLayer":
                                    this.HeatMapVisulizationRecords(responseObject, workerMessage);
                                    break;
                                case "hexagonLayer":
                                    this.productionRecords(responseObject, workerMessage);
                                    break;
                                case "arcLayer":
                                    this.productionDayTruckingRecords(responseObject, workerMessage);
                                    break;
                                default:
                                    break;

                            }
                            break;
                        case "Locations":
                            this.locationsRecords(responseObject, workerMessage);
                            break;
                        default:
                            break;
                    }
                }
                else {
                    console.error("GisWorker Error:" + responseObject.data)
                }
            }
            catch (e) {
                console.error("Gis Worker error:" + e + " Message:" + responseObject.data);
            }
        });
    }

    private productionRecords(responseObject: any, workerMessage) {
        // responseObject.data = JSON.parse(responseObject.data);
        // var typeOfData = JSON.parse(responseObject.data);
        var resObj = JSON.parse(responseObject.data);
        var typeOfData = JSON.parse(resObj.result);

        var obj = {};
        for (var key in typeOfData) {//typeof Data e.g wells,facility.delivered

            if (typeOfData.hasOwnProperty(key)) {
                //each typeof data iteration
                var featureObject = {};
                var eachTypeDataKey = typeOfData[key];
                for (var objKey in eachTypeDataKey) { // e.g cassingPressure

                    if (eachTypeDataKey.hasOwnProperty(objKey)) {
                        var EntityTYpeFeaturesArray = [];
                        var entities = JSON.parse(eachTypeDataKey[objKey]);
                        if (entities.length) {

                            entities.forEach(obj => {//each entity type key object iteration
                                var layer = {
                                    COORDINATES: [],
                                    properties: {}
                                };
                                if (obj["sLat"] != "0.0" && obj["sLat"] != "" && obj["sLat"] != null) {

                                    var lng = parseFloat(obj["sLng"]);
                                    var lat = parseFloat(obj["sLat"]);
                                    layer.COORDINATES = [lng, lat];
                                    for (var key in obj) {
                                        if (obj.hasOwnProperty(key)) {
                                            if (key != "sLng" && key != "sLat") {
                                                if (key == objKey) {
                                                    layer.properties["height"] = parseFloat(obj[key]);
                                                }
                                                else {
                                                    layer.properties[key] = obj[key];
                                                }
                                            }
                                        }
                                    }
                                    EntityTYpeFeaturesArray.push(layer);//

                                }//if 0.0 end
                                // EntityTYpeFeaturesArray.push(layer);//slat lng not there in obj
                            })
                        } //entities length

                        // like e.g {"cassingPressure":[],"flowPressure":[]}
                        featureObject[objKey] = EntityTYpeFeaturesArray;

                        //create geojson objects
                        /*
                        var myJSONArray = [];
                        EntityTYpeFeaturesArray.forEach(a => {
                            var ffff = {
                                "type": "Feature",
                                "properties": {},
                                "geometry": {
                                    "type": "Point",
                                    "coordinates": a.COORDINATES
                                }
                            };
                            myJSONArray.push(ffff);

                        })
                        console.log("<<GEOJSON>>:")
                        console.log(JSON.stringify(myJSONArray));
                        */
                    }
                }

            }// typeof object key verify
            obj[key] = featureObject;
        }
        resObj.result = JSON.stringify([obj]);
        //send message to ui
        responseObject.data = JSON.stringify(JSON.stringify(resObj));
        this.returnWorkerResult(responseObject, workerMessage);
    }

    private locationsRecords(responseObject: any, workerMessage) {
        // responseObject.data = JSON.parse(responseObject.data);
        // var data = JSON.parse(responseObject.data);
        var resObj = JSON.parse(responseObject.data);
        var data = JSON.parse(resObj.result);

        var featuresArray = [];
        for (var key in data) {//each entity type iteration GasWell,OilWell
            if (data.hasOwnProperty(key)) {

                var entities = JSON.parse(data[key]);
                if (entities.length) {
                    this.GeoJSON = {
                        type: "FeatureCollection",
                        features: []
                    };

                    entities.forEach(obj => {//each entitytypes object iteration

                        if (obj["sLat"] != "0.0" && obj["sLat"] != "" && obj["sLat"] != null) {
                            this.feature = {
                                "type": "Feature",
                                "properties": {},
                                "geometry": {
                                    "type": "Point",
                                    "coordinates": []
                                }
                            };
                            var lng = parseFloat(obj["sLng"]);

                            this.feature.geometry.coordinates[0] = lng;

                            var lat = parseFloat(obj["sLat"]);
                            this.feature.geometry.coordinates[1] = lat;

                            for (var key in obj) {
                                if (obj.hasOwnProperty(key)) {
                                    if (key != "sLat" && key != "sLng") {
                                        this.feature.properties[key] = obj[key];
                                    }
                                }
                            }
                            this.feature.properties["icon"] = obj["type"];
                            this.GeoJSON.features.push(this.feature);
                        }
                    })

                    var llayer = {
                        id: key,
                        type: "symbol",
                        source: {
                            "type": "geojson",
                            "data": this.GeoJSON
                        },
                        layout: {
                            "icon-image": "{icon}",
                            "icon-size": 0.04,
                            "visibility": "visible"
                        },
                        minzoom: 7.4,
                        metadata: {
                            'imageurl': 'wwwroot/images/Mapboxgl/Entities/' + key + '.png',
                            'center': [-119.09913365922579, 56.054006240841346],
                            'zoom': [12]
                        }
                    };
                    featuresArray.push(llayer);
                } //entities length
            }
        }

        resObj.result = JSON.stringify(featuresArray);
        //send message to ui
        responseObject.data = JSON.stringify(JSON.stringify(resObj));
        this.returnWorkerResult(responseObject, workerMessage);
    }

    private HeatMapVisulizationRecords(responseObject: any, workerMessage) {

        //responseObject.data = JSON.parse(responseObject.data);
        //var typeOfData = JSON.parse(responseObject.data);
        var resObj = JSON.parse(responseObject.data);
        var typeOfData = JSON.parse(resObj.result);

        var obj = {};
        for (var key in typeOfData) {//typeof Data e.g wells,facility.delivered

            if (typeOfData.hasOwnProperty(key)) {
                //each typeof data iteration
                var featureObject = {};
                var eachTypeDataKey = typeOfData[key];
                for (var objKey in eachTypeDataKey) { // e.g cassingPressure

                    if (eachTypeDataKey.hasOwnProperty(objKey)) {
                        var EntityTYpeFeaturesArray = [];
                        var entities = JSON.parse(eachTypeDataKey[objKey]);
                        if (entities.length) {
                            this.GeoJSON = {
                                type: "FeatureCollection",
                                features: []
                            };

                            entities.forEach(obj => {//each entity type key object iteration

                                if (obj["sLat"] != "0.0" && obj["sLat"] != "" && obj["sLat"] != null) {
                                    this.feature = {
                                        "type": "Feature",
                                        "properties": {},
                                        "geometry": {
                                            "type": "Point",
                                            "coordinates": []
                                        }
                                    };
                                    var lng = parseFloat(obj["sLng"]);
                                    var lat = parseFloat(obj["sLat"]);
                                    this.feature.geometry.coordinates = [lng, lat];
                                    for (var key in obj) {
                                        if (obj.hasOwnProperty(key)) {
                                            if (key != "sLng" && key != "sLat") {
                                                if (key == objKey) {
                                                    this.feature.properties["height"] = parseFloat(obj[key]);
                                                }
                                                else {
                                                    this.feature.properties[key] = obj[key];
                                                }
                                            }
                                        }
                                    }
                                    this.GeoJSON.features.push(this.feature);
                                }//if 0.0 end
                            })
                            let tempkey = objKey + '_heatmap';
                            var heatmapllayer = {
                                id: tempkey,
                                type: "heatmap",
                                source: {
                                    "type": "geojson",
                                    "data": this.GeoJSON
                                },
                                paint: {
                                    // Increase the heatmap weight based on frequency and property magnitude
                                    "heatmap-weight": [
                                        "interpolate",
                                        ["linear"],
                                        ["get", "height"],
                                        0, 0,
                                        100, 1
                                    ],
                                    // Increase the heatmap color weight weight by zoom level
                                    // heatmap-intensity is a multiplier on top of heatmap-weight
                                    "heatmap-intensity": [
                                        "interpolate",
                                        ["linear"],
                                        ["zoom"],
                                        11, 1,
                                        15, 3
                                    ],
                                    // Color ramp for heatmap.  Domain is 0 (low) to 1 (high).
                                    // Begin color ramp at 0-stop with a 0-transparancy color
                                    // to create a blur-like effect.
                                    "heatmap-color": [
                                        "interpolate",
                                        ["linear"],
                                        ["heatmap-density"],
                                        0, "rgba(33,102,172,0)",
                                        0.2, "rgb(103,169,207)",
                                        0.4, "rgb(209,229,240)",
                                        0.6, "rgb(253,219,199)",
                                        0.8, "rgb(239,138,98)",
                                        1, "rgb(178,24,43)"
                                    ],
                                    // Adjust the heatmap radius by zoom level
                                    "heatmap-radius": [
                                        "interpolate",
                                        ["linear"],
                                        ["zoom"],
                                        0, 2,
                                        9, 20,
                                        15, 30
                                    ],
                                    // Transition from heatmap to circle layer by zoom level
                                    "heatmap-opacity": [
                                        "interpolate",
                                        ["linear"],
                                        ["zoom"],
                                        14, 1,
                                        15, 0
                                    ],
                                },
                                maxzoom: 15
                            };

                            var llayer = {
                                id: objKey,
                                type: "circle",
                                source: {
                                    "type": "geojson",
                                    "data": this.GeoJSON
                                },
                                paint: {
                                    // Size circle radius by earthquake magnitude and zoom level
                                    "circle-radius": [
                                        "interpolate",
                                        ["linear"],
                                        ["zoom"],
                                        15, [
                                            "interpolate",
                                            ["linear"],
                                            ["get", "height"],
                                            1, 1,
                                            10, 2,
                                            50, 3,
                                            100, 4,
                                            200, 5,
                                            300, 6,
                                            400, 7,
                                            500, 9,
                                            600, 9,
                                            700, 10,
                                            800, 11,
                                            900, 12,
                                            1000, 13,
                                            1500, 15,
                                            2000, 17,
                                            2500, 19,
                                            3000, 21,
                                            3500, 23,
                                            4000, 25,
                                            4500, 27,
                                            5000, 29,
                                            5500, 31,
                                            6000, 33,
                                        ],
                                        18, [
                                            "interpolate",
                                            ["linear"],
                                            ["get", "height"],
                                            1, 3,
                                            10, 4,
                                            50, 5,
                                            100, 6,
                                            200, 7,
                                            300, 8,
                                            400, 9,
                                            500, 10,
                                            600, 11,
                                            700, 12,
                                            800, 13,
                                            900, 14,
                                            1000, 15,
                                            1500, 17,
                                            2000, 19,
                                            2500, 21,
                                            3000, 23,
                                            3500, 25,
                                            4000, 27,
                                            4500, 29,
                                            5000, 31,
                                            5500, 33,
                                            6000, 35,
                                        ]
                                    ],
                                    // Color circle by earthquake magnitude
                                    "circle-color": [
                                        "interpolate",
                                        ["linear"],
                                        ["get", "height"],
                                        1, "rgba(33,102,172,0)",
                                        100, "rgb(103,169,207)",
                                        500, "rgb(209,229,240)",
                                        1000, "rgb(253,219,199)",
                                        1500, "rgb(239,138,98)",
                                        2000, "rgb(178, 131, 24)"
                                    ],
                                    "circle-stroke-color": "white",
                                    "circle-stroke-width": 1,
                                    // Transition from heatmap to circle layer by zoom level
                                    "circle-opacity": [
                                        "interpolate",
                                        ["linear"],
                                        ["zoom"],
                                        14, 0,
                                        15, 1
                                    ]
                                },
                                minzoom: 14
                            };
                            EntityTYpeFeaturesArray.push(heatmapllayer);
                            EntityTYpeFeaturesArray.push(llayer);
                        } //entities length

                        // like e.g {"cassingPressure":[],"flowPressure":[]}
                        featureObject[objKey] = EntityTYpeFeaturesArray;
                    }
                }

            }// typeof object key verify
            obj[key] = featureObject;
        }
        resObj.result = JSON.stringify([obj]);
        //send message to ui
        responseObject.data = JSON.stringify(JSON.stringify(resObj));
        this.returnWorkerResult(responseObject, workerMessage);
    }

    private productionDayTruckingRecords(responseObject: any, workerMessage) {
        var resObj = JSON.parse(responseObject.data);
        var typeOfData = JSON.parse(resObj.result);

        var obj = {};
        for (var key in typeOfData) {//typeof Data e.g wells,facility.delivered

            if (typeOfData.hasOwnProperty(key)) {
                //each typeof data iteration
                var featureObject = {};
                var eachTypeDataKey = typeOfData[key];
                for (var objKey in eachTypeDataKey) { // e.g cassingPressure

                    if (eachTypeDataKey.hasOwnProperty(objKey)) {
                        var EntityTYpeFeaturesArray = [];
                        var entities = JSON.parse(eachTypeDataKey[objKey]);
                        if (entities.length) {

                            entities.forEach(obj => {//each entity type key object iteration
                                var layer = {
                                    pickup: [],//flocation
                                    dropoff: [],//tolocation
                                    properties: {}
                                };
                                if (obj["fLng"] != "0.0" && obj["fLat"] != "" && obj["fLat"] != null) {

                                    var fLng = parseFloat(obj["fLng"]);
                                    var fLat = parseFloat(obj["fLat"]);

                                    var tLng = parseFloat(obj["tLng"]);
                                    var tLat = parseFloat(obj["tLat"]);

                                    layer.pickup = [fLng, fLat];

                                    layer.dropoff = [tLng, tLat];

                                    for (var key in obj) {
                                        if (obj.hasOwnProperty(key)) {
                                            if (key != "fLng" && key != "fLat" && key != "tLng" && key != "tLat") {
                                                if (key == "loadtotalvolume") {
                                                    layer.properties["height"] = parseFloat(obj["loadtotalvolume"]);
                                                }
                                                else {
                                                    layer.properties[key] = obj[key];
                                                }
                                            }
                                        }
                                    }
                                    EntityTYpeFeaturesArray.push(layer);//

                                }//if 0.0 end
                                // EntityTYpeFeaturesArray.push(layer);//slat lng not there in obj
                            })
                        } //entities length

                        // like e.g {"cassingPressure":[],"flowPressure":[]}
                        featureObject[objKey] = EntityTYpeFeaturesArray;
                    }
                }

            }// typeof object key verify
            obj[key] = featureObject;
        }
        resObj.result = JSON.stringify([obj]);
        //send message to ui
        responseObject.data = JSON.stringify(JSON.stringify(resObj));
        this.returnWorkerResult(responseObject, workerMessage);
    }

    IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    private returnWorkerResult(response, workerMessage) {
        this.returnWorkResults(new WorkerMessage(workerMessage.topic, workerMessage.restMethodName,
            {
                ok: response.ok,
                data: response.data,
                status: response.status,
                headers: response.headers,
                statusText: response.statusText
            }))
    }


    ////////////////////////////////////////Backup
    private restRequestOld(workerMessage, methodType, Resturl, queryParams, Restbody, restOptions) {
        restRequest(methodType, Resturl, queryParams, Restbody, restOptions,workerMessage.restMethodName).then((responseObject: RequestResult) => {
            console.log("Gis Worker Response...");
            try {
                if (this.IsJsonString(responseObject.data)) {

                    //responseObject.data = JSON.parse(responseObject.data);
                    //var data = JSON.parse(responseObject.data);
                    // var data = dataWells.wells;

                    switch (workerMessage.dataPayload.body.DataType) {
                        case "Production":
                            this.productionRecords(responseObject, workerMessage);
                            break;
                        case "Locations":
                            this.locationsRecords(responseObject, workerMessage);
                            break;
                        default:
                            break;
                    }

                    /* 29-05-2019
                    var featuresArray = [];
                    for (var key in data) {//each entity type iteration
                        if (data.hasOwnProperty(key)) {

                            var entities = JSON.parse(data[key]);
                            if (entities.length) {
                                this.GeoJSON = {
                                    type: "FeatureCollection",
                                    features: []
                                };

                                entities.forEach(obj => {//each entity type key object iteration

                                    if (obj["SurfaceLatitude"] != "0.0" && obj["SurfaceLatitude"] != "" && obj["SurfaceLatitude"] != null) {
                                        this.feature = {
                                            "type": "Feature",
                                            "properties": {},
                                            "geometry": {
                                                "type": "Point",
                                                "coordinates": []
                                            }
                                        };
                                        var lng = parseFloat(obj["SurfaceLongitude"]);

                                        this.feature.geometry.coordinates[0] = lng;

                                        var lat = parseFloat(obj["SurfaceLatitude"]);
                                        this.feature.geometry.coordinates[1] = lat;

                                        for (var key in obj) {
                                            if (obj.hasOwnProperty(key)) {
                                                if (key != "SurfaceLongitude" && key != "SurfaceLatitude") {
                                                    if (key == "CasingPressure") {
                                                        this.feature.properties[key] = parseFloat(obj[key]);
                                                    }
                                                    else {
                                                        this.feature.properties[key] = obj[key];

                                                    }
                                                }
                                            }
                                        }

                                        // this.feature.properties["EntityType"] = obj["EntityType"];
                                        this.feature.properties["icon"] = obj["EntityType"];
                                        //  this.feature.properties["EntityName"] = obj["EntityName"];


                                        this.GeoJSON.features.push(this.feature);
                                    }
                                })

                                // var layer;
                                if (workerMessage.dataPayload.body.DataType == "Production") {
                                    //layer = {};
                                    this.GeoJSON.features.forEach((obj) => {
                                        var layer = {};
                                        layer["properties"] = obj.properties;
                                        layer["COORDINATES"] = obj.geometry.coordinates;
                                        featuresArray.push(layer);
                                    })
                                }
                                else {
                                    var llayer = {
                                        id: key,
                                        type: "symbol",
                                        source: {
                                            "type": "geojson",
                                            "data": this.GeoJSON
                                        },
                                        layout: {
                                            "icon-image": "{icon}",
                                            "icon-size": 0.04,
                                            "visibility": "visible"
                                        },
                                        minzoom: 7.4,
                                        metadata: {
                                            'imageurl': 'wwwroot/images/Mapboxgl/Entities/' + key + '.png',
                                            'center': [-119.09913365922579, 56.054006240841346],
                                            'zoom': [12]
                                        }
                                    };
                                    featuresArray.push(llayer);

                                }
                                ///

                                // featuresArray.push(layer);
                            } //entities length
                        }
                    }

                    */
                }
                else {
                    console.error("GisWorker Error:" + responseObject.data)
                }

                /////////////
                /*
                var featuresArray = [];
                console.log(entitiesJsonArray.length);

                this.GeoJSON = {
                    type: "FeatureCollection",
                    features: []
                };
                entitiesJsonArray.forEach(obj => {

                    if (obj["SurfaceLatitude"] != "0.0" && obj["SurfaceLatitude"] != "" && obj["SurfaceLatitude"] != null) {
                        this.feature = {
                            "type": "Feature",
                            "properties": {},
                            "geometry": {
                                "type": "Point",
                                "coordinates": []
                            }
                        };
                        var lat = parseFloat(obj["SurfaceLatitude"]);
                        var lng = parseFloat(obj["SurfaceLongitude"]);

                        this.feature.geometry.coordinates[0] = lng;
                        this.feature.geometry.coordinates[1] = lat;

                        this.feature.properties["EntityId"] = obj["EntityId"];
                        this.feature.properties["EntityType"] = obj["EntityType"];
                        this.feature.properties["icon"] = obj["EntityType"];
                        this.feature.properties["EntityName"] = obj["EntityName"];
                        this.GeoJSON.features.push(this.feature);
                    }
                })

                var layer = {
                    id: "GasWell",
                    type: "symbol",
                    source: {
                        "type": "geojson",
                        "data": this.GeoJSON
                    },
                    layout: {
                        "icon-image": "{icon}",
                        "icon-size": 0.04,
                        "visibility": "visible"
                    },
                    minzoom: 7.4,
                    metadata: {
                        'imageurl': 'wwwroot/images/Mapboxgl/Entities/GasWell.png',
                        'center': [-119.09913365922579, 56.054006240841346],
                        'zoom': [12]
                    }
                }

                featuresArray.push(layer);*/





                //29-05-2018
                // responseObject.data = JSON.stringify(JSON.stringify(featuresArray));
                // this.returnWorkerResult(responseObject, workerMessage);
            }
            catch (e) {
                console.error("Gis Worker error:" + e + " Message:" + responseObject.data);
            }
        });
    }
}
