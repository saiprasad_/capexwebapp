/* <project-root>/src/worker/app-workers/app.workers.ts */

import { WorkerMessage } from '../shared/worker-message.model';
import { WORKER_TOPIC } from '../shared/worker-topic.constants';
import { restRequest, RequestResult, DEFAULT_REQUEST_OPTIONS, RequestOptions, mapXMLHttpRequest } from '../http/httpRequest';

export class AppWorkers {
  workerCtx: any;
  created: Date;
  constructor(workerCtx: any) {
    this.workerCtx = workerCtx;
    this.created = new Date();
  }

  workerBroker($event: MessageEvent): void {
    const { topic, restMethodName, dataPayload } = $event.data as WorkerMessage;
    const workerMessage = new WorkerMessage(topic, restMethodName, dataPayload);

    switch (topic) {
      case WORKER_TOPIC.RESTHTTP:
        let methodType = workerMessage.dataPayload.method;
        let Resturl = workerMessage.dataPayload.url;
        let queryParams: any = workerMessage.dataPayload.queryParams ? workerMessage.dataPayload.queryParams : {};
        let Restbody: any = workerMessage.dataPayload.body ? workerMessage.dataPayload.body : null;
        let restOptions: RequestOptions = workerMessage.dataPayload.options ? workerMessage.dataPayload.options : DEFAULT_REQUEST_OPTIONS;
        this.restRequest(workerMessage, methodType, Resturl, queryParams, Restbody, restOptions);
        break;
      default:  // Add support for other workers here
        console.error('Topic Does Not Match');
    }

  }

  private returnWorkResults(message: WorkerMessage): void {
    this.workerCtx.postMessage(message);
  }

  private restRequest(workerMessage, methodType, Resturl, queryParams, Restbody, restOptions) {

    let promise = restRequest(methodType, Resturl, queryParams, Restbody, restOptions,workerMessage.restMethodName);
    promise.then((response: RequestResult) => {
      if(response.status==0){//aborted or cancelled request
        this.returnWorkerResult(response, workerMessage);
      }else{
        this.deletemapXMLHttpRequest(workerMessage.restMethodName);
        this.returnWorkerResult(response, workerMessage);
        // var responseString = JSON.stringify(response);
        // var resObject = JSON.parse(responseString);
        // resObject.data = JSON.parse(resObject.data);
  
        // if (this.isResponseJson(resObject.data)) {
        //   var payload = JSON.parse(resObject.data).Payload;
        //   if (this.isResponseJson(payload) && payload!=null) {
        //     var payloadObject = JSON.parse(payload);
        //     if (payloadObject.hasOwnProperty('Type')) {
        //       //var messageType = payloadObject['Type'].toString();
        //       //new UpdateUIAndIDB(this.workerCtx).ReceivedWorkerMessage(messageType, payloadObject, response, workerMessage);
        //     }
        //     else {
        //       this.returnWorkerResult(response, workerMessage);
        //     }
        //   } else {
        //     this.returnWorkerResult(response, workerMessage);
        //   }
        // }
        // else {
        //   this.returnWorkerResult(response, workerMessage);
        // }
      }      
    });
  }
  private deletemapXMLHttpRequest(restMethodName){
    if(mapXMLHttpRequest.has(restMethodName)){
      mapXMLHttpRequest.delete(restMethodName)
    }
  }
  public returnAbortEventMessage(response,workerMessage){
    let requestResult: RequestResult = {
      ok: true,
      data: JSON.stringify(response),
      status: 0,
      headers: "content-type: application/json; charset=utf-8",
      statusText: '',
      json: null
    };
    this.returnWorkerResult(requestResult,workerMessage);
  }

  private returnWorkerResult(response, workerMessage) {
    this.returnWorkResults(new WorkerMessage(workerMessage.topic, workerMessage.restMethodName,
      {
        ok: response.ok,
        data: response.data,
        status: response.status,
        headers: response.headers,
        statusText: response.statusText
      }))
  }

  private isResponseJson(json: string) {
    try {
      JSON.parse(json);
    } catch (e) {
      return false;
    }
    return true;
  }

}
