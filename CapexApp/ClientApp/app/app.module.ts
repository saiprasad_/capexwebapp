import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './components/template/app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule, APP_BASE_HREF } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { FabricRouter } from './components/globals/Model/FabricRouter';
import { CookieService, CookieOptionsProvider } from 'ngx-cookie';
import { WebWorkerService } from './components/globals/components/ngx-web-worker/web-worker.service';
import { AppInsightsService } from './components/common/app-insight/appinsight-service';
import { AppConfig } from './components/globals/services/app.config';
import { MarketingComponent } from './components/capabilities/Admin/marketing/marketing.component';
import { SharedModule1 } from './components/shared/shared1.module';
import { InvalidPageComponent, DialogContent } from './components/common/InvalidPageComponents/invalid.component';
import { LayoutComponent } from './components/template/LayoutComponents/layout.component';
import { AuthGuard } from './components/authguard/services/auth.guard';
import { MdePopoverModule } from '@material-extended/mde';
import { CookieModule } from 'ngx-cookie';
// ######## Services ######################
import { CapabilityAuthGaurd } from './components/authguard/services/capability-authgaurd.service';
import { TreeDraggedElement } from '@circlon/angular-tree-component';

/* New Components */
import { TreeComponent } from './components/template/LayoutComponents/leftsidebar/tree.component';
import { MainHeaderComponent } from './components/template/LayoutComponents/headers/main-header/main-header.component';
import { SideHeaderComponent } from './components/template/LayoutComponents/headers/side-header/side-header.component';
import { SubHeaderComponent } from './components/template/LayoutComponents/headers/sub-header/sub-header.component';
import { RightSideTreeComponent } from './components/template/LayoutComponents/rightsidebar/right-side-tree.component';
import { FormGenComponent } from './components/template/LayoutComponents/rightsidebar/list-form-generate/form-gen.component';
import { MessagingService } from './components/globals/services/MessagingService';
import { ThemeService } from './components/globals/services/theme.service';
import { CommonService } from './components/globals/CommonService';
import { ResizableModule } from 'angular-resizable-element';
import { SideRightHeaderComponent } from './components/template/LayoutComponents/headers/rightHeader/right-header.component';
import { FDCFormGeratorCommon } from './components/common/services/FDCFormGeneratorCommonService/FDCFormGeratorCommon';
import { DataService } from './components/globals/services/data.service';
import { ConstructionService } from './components/fabrics/construction/services/construction.service';
// import {VisurDirectivesModule} from 'visur-angular-common';
const route: Routes = [
  {
    path: '', component: LayoutComponent, canActivate: [AuthGuard], children: [
      {
        path: '',
        loadChildren: () => import('./components/fabrics/HomeFabric/home.module').then(m => m.HomeModule)
      },

      {
        path: FabricRouter.SECURITY_FABRIC,
        loadChildren: () => import('./components/fabrics/Security/Security-fabric.module').then(m => m.SecurityModule)
      },
      {
        path: FabricRouter.HOME_FABRIC,
        loadChildren: () => import('./components/fabrics/HomeFabric/home.module').then(m => m.HomeModule)
      },
      {
        path: FabricRouter.BUSINESSINTELLIGENCE_FABRIC,
        loadChildren: () => import('./components/fabrics/bifabric/bi.module').then(m => m.BIModule)
      },
      {
        path: FabricRouter.MAPS_FABRIC,
        loadChildren: () => import('./components/fabrics/Map/map.module').then(m => m.MapModule)
      },
      {
        path: FabricRouter.ENTITYMANAGEMENT_FABRIC, canLoad: [CapabilityAuthGaurd],
        loadChildren: () => import('./components/fabrics/EntityManagement/EntityManagement.module').then(m => m.EntityManagementModule)
      },
      {
        path: FabricRouter.CONSTRUCTION_FABRIC,
        // canLoad: [CapabilityAuthGaurd],
        loadChildren: () => import('./components/fabrics/construction/module/construction.module').then(m => m.ConstructionModule)
      },
      {
        path: FabricRouter.REPORTING_FABRIC,
        loadChildren: () => import('./components/fabrics/reporting/Module/reporting.module').then(m => m.ReportingModule)
      },
      { path: 'Home/TenantAuthentication', redirectTo: '', pathMatch: 'full' },
      { path: 'Home/UsernameAuthentication', redirectTo: '', pathMatch: 'full' },
      { path: 'Home/PasswordAuthentication', redirectTo: '', pathMatch: 'full' },
      { path: 'Home/OTPAuthentication', redirectTo: '', pathMatch: 'full' },
      { path: 'login', redirectTo: '', pathMatch: 'full' },
      { path: 'login/:id', redirectTo: '', pathMatch: 'full' },
      { path: 'marketingpage/:capability', component: MarketingComponent },
      { path: 'UnauthorizedPage', component: InvalidPageComponent },
      { path: '**', component: InvalidPageComponent }
    ]
  }


];

export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href;
}

export function initializeApp(appConfig: AppConfig) {
  return () => appConfig.load();
}

@NgModule({
  exports: [

  ],
  declarations: []
})
export class AngularMaterialModule { }

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    TreeComponent,
    AppComponent,
    MarketingComponent,
    InvalidPageComponent,
    LayoutComponent,
    MainHeaderComponent,
    SideHeaderComponent,
    SubHeaderComponent,
    RightSideTreeComponent,
    FormGenComponent,
    SideRightHeaderComponent,
    DialogContent
  ],
  imports: [
    AngularMaterialModule,
    CommonModule,
    FormsModule,
    CookieModule.forRoot(),
    SharedModule1,
    BrowserModule.withServerTransition({ appId: 'constructionappcontent' }),
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(route),
    ResizableModule,
    MdePopoverModule
  ],
  entryComponents: [DialogContent],
  providers: [
    AppConfig,
    AppInsightsService, TreeDraggedElement, FDCFormGeratorCommon,
    CommonService, CookieService, { provide: CookieOptionsProvider, useValue: {} },
    MessagingService, DataService, ConstructionService,
    CapabilityAuthGaurd, WebWorkerService, ThemeService, AuthGuard,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [AppConfig], multi: true
    },
    { provide: APP_BASE_HREF, useValue: '/constructionapp' }

  ]
})
export class AppModule {
}
