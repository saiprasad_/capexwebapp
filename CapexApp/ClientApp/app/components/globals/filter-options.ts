import { hasOwn, dateToString } from '@visur/formgenerator-core';
import { AppConsts } from '../common/Constants/AppConsts';

export const Contains: string = 'Contains';
export const DoesNotContain: string = 'Does Not Contain';
export const BeginsWith: string = 'Begins With';
export const DoesNotBeginWith: string = 'Does Not Begin With';
export const EndsWith: string = 'Ends With';
export const DoesNotEndWith: string = 'Does Not End With';
export const Between: string = 'Between';
export const NotBetween: string = 'Not Between';
export const Equal: string = 'Equal';
export const NotEqual: string = 'Not Equal';
export const GreaterThan: string = 'Greater Than';
export const GreaterThanOrEqual: string = 'Greater Than or Equal';
export const LessThan: string = 'Less Than';
export const LessThanOrEqual: string = 'Less Than or Equal';
export const DateFormat:string='yyyy-MM-dd';
export const AndOperator:string="AND";
export const OrOperator:string="OR";

export const STRING_OPTIONS: string[] = [
  Contains,
  DoesNotContain,
  BeginsWith,
  DoesNotBeginWith,
  EndsWith,
  DoesNotEndWith
];

export const BOOLEAN_OPTIONS: string[] = [
  Equal,
  NotEqual
];

export const BOOLEAN_VALUES: boolean[] = [
  true,
  false
];

export const NUMERIC_DATE_OPTIONS: string[] = [
  Between,
  NotBetween,
  Equal,
  NotEqual,
  GreaterThan,
  GreaterThanOrEqual,
  LessThan,
  LessThanOrEqual
];

export const LOGICAL_OPERATOR:string[]=[
AndOperator,
OrOperator
]

export class ColumnTypes {
  public static STRING: string = 'string';
  public static TEXT: string = 'text';
  public static LABEL: string = 'label';
  public static NUMBER: string = 'number';
  public static INTEGER: string = 'integer';
  public static DATE: string = 'date';
  public static BOOLEAN: string = 'boolean';
  public static TIME :string='date-time';
  public static LOGICAL:string='logicalOperator';
  
}

export interface TableFilter {
  prop?: string;
  operator: string;
  value: any;
  type: string;
  entityid: string;
}

export interface TableRowFilter {
  type: string;
  operator: string;
  dateFirst?: string;
  dateSeocond?: string;
  value?: string;
  number?: number;
  rows: any[];
  prop: string;
  format?: string;
}

export function initializeOptions(type: string) {
  let data: any = {};
  switch (type) {
    case ColumnTypes.STRING:
    case ColumnTypes.TEXT:
    case ColumnTypes.LABEL:
      data.operator = STRING_OPTIONS;
      data.defaultOperator = Contains;
      break;

    case ColumnTypes.NUMBER:
    case ColumnTypes.INTEGER:
    case ColumnTypes.DATE:
      data.operator = NUMERIC_DATE_OPTIONS;
      data.defaultValueOperator = Contains;
      break;

    case ColumnTypes.BOOLEAN:
      data.operator = BOOLEAN_OPTIONS;
      data.defaultValueOperator = Equal;
      break;
      
    case ColumnTypes.LOGICAL:
      data.operator = LOGICAL_OPERATOR;
      data.defaultValueOperator = AndOperator;
      break;  
  }
  return data;
}

export function filterRowsBasedOnOperator(operator: string, term: any, prop: string, type: string, rows: any[]) {
  switch (operator) {

    // String types
    case Contains:
      rows = rows.filter((row: any) => hasOwn(row, prop) && (<string>row[prop]).includes(term));
      break;

    case DoesNotContain:
      rows = rows.filter((row: any) => hasOwn(row, prop) && !(<string>row[prop]).includes(term));
      break;

    case BeginsWith:
      rows = rows.filter((row: any) => hasOwn(row, prop) && (<string>row[prop]).startsWith(term));
      break;

    case DoesNotBeginWith:
      rows = rows.filter((row: any) => hasOwn(row, prop) && !(<string>row[prop]).startsWith(term));
      break;

    case EndsWith:
      rows = rows.filter((row: any) => hasOwn(row, prop) && (<string>row[prop]).endsWith(term));
      break;

    case DoesNotEndWith:
      rows = rows.filter((row: any) => hasOwn(row, prop) && !(<string>row[prop]).endsWith(term));
      break;

    // Number or Date Types

    case Between:
      if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
        rows = rows.filter((row: any) => hasOwn(row, prop) && +row[prop] == term);
      }
      else if (type == ColumnTypes.DATE) {
        rows = rows.filter((row: any) => hasOwn(row, prop) && new Date(dateToString(row[prop])).getTime() == new Date(term).getTime());
      }
      break;

    case NotBetween:
      if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
        rows = rows.filter((row: any) => hasOwn(row, prop) && +row[prop] == term);
      }
      else if (type == ColumnTypes.DATE) {
        rows = rows.filter((row: any) => hasOwn(row, prop) && new Date(dateToString(row[prop])).getTime() == new Date(term).getTime());
      }
      break;

    case Equal:
      if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
        rows = rows.filter((row: any) => hasOwn(row, prop) && +row[prop] == term);
      }
      else if (type == ColumnTypes.DATE) {
        rows = rows.filter((row: any) => hasOwn(row, prop) && new Date(dateToString(row[prop])).getTime() == new Date(term).getTime());
      }
      break;

    case NotEqual:
      if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
        rows = rows.filter((row: any) => hasOwn(row, prop) && +row[prop] !== term);
      }
      else if (type == ColumnTypes.DATE) {
        rows = rows.filter((row: any) => hasOwn(row, prop) && new Date(dateToString(row[prop])).getTime() !== new Date(term).getTime());
      }
      break;

    case GreaterThan:
      if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
        rows = rows.filter((row: any) => hasOwn(row, prop) && +row[prop] > term);
      }
      else if (type == ColumnTypes.DATE) {
        rows = rows.filter((row: any) => hasOwn(row, prop) && new Date(dateToString(row[prop])).getTime() > new Date(term).getTime());
      }
      break;

    case GreaterThanOrEqual:
      if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
        rows = rows.filter((row: any) => hasOwn(row, prop) && +row[prop] >= term);
      }
      else if (type == ColumnTypes.DATE) {
        rows = rows.filter((row: any) => hasOwn(row, prop) && new Date(dateToString(row[prop])).getTime() >= new Date(term).getTime());
      }
      break;

    case LessThan:
      if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
        rows = rows.filter((row: any) => hasOwn(row, prop) && +row[prop] < term);
      }
      else if (type == ColumnTypes.DATE) {
        rows = rows.filter((row: any) => hasOwn(row, prop) && new Date(dateToString(row[prop])).getTime() < new Date(term).getTime());
      }
      break;

    case LessThanOrEqual:
      if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
        rows = rows.filter((row: any) => hasOwn(row, prop) && +row[prop] <= term);
      }
      else if (type == ColumnTypes.DATE) {
        rows = rows.filter((row: any) => hasOwn(row, prop) && new Date(dateToString(row[prop])).getTime() <= new Date(term).getTime());
      }
      break;
  }

  return rows;
}

export function filterTablesBasedOnOperator(operator: string, value: any, prop: string, type: string, rows: any[], value2?: any, format?: string,logicalOperator?:string) {
  let rowChanged=false;
  if (rows != undefined && operator != "" && value !="" && rows.length!=0 && value!=null) {
    switch (operator) {
      case Contains:
        rows = rows.filter((row: any) => hasOwn(row, prop) && (<string>row[prop]).toLowerCase().includes(String(value).toLowerCase()));
        rowChanged=true;
        break;

      case DoesNotContain:
        rows = rows.filter((row: any) => hasOwn(row, prop) && !(<string>row[prop]).toLowerCase().includes(String(value).toLowerCase()));
        rowChanged=true;
        break;

      case BeginsWith:
        rows = rows.filter((row: any) => hasOwn(row, prop) && (<string>row[prop]).toLowerCase().startsWith(String(value).toLowerCase()));
        rowChanged=true;
        break;

      case DoesNotBeginWith:
        rows = rows.filter((row: any) => hasOwn(row, prop) && !(<string>row[prop]).toLowerCase().startsWith(String(value).toLowerCase()));
        rowChanged=true;
        break;

      case EndsWith:
        rows = rows.filter((row: any) => hasOwn(row, prop) && (<string>row[prop]).toLowerCase().endsWith(String(value).toLowerCase()));
        rowChanged=true;
        break;

      case DoesNotEndWith:
        rows = rows.filter((row: any) => hasOwn(row, prop) && !(<string>row[prop]).toLowerCase().endsWith(String(value).toLowerCase()));
        rowChanged=true;
        break;

      // Number or Date Types

      case Between:
        if ((type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER)&& value2!=null && value2!="") {
          rows = rows.filter((row: any) => hasOwn(row, prop) && +row[prop] > value && +row[prop]<value2 );
          rowChanged=true;
        }
        else if ((type == ColumnTypes.DATE || format == ColumnTypes.DATE)&&value2!=null && value2!="") {
          rows = rows.filter((row: any) => hasOwn(row, prop) && (new Date(dateToString(row[prop])).getTime() > new Date(value).getTime() && new Date(dateToString(row[prop])).getTime() < new Date(value2).getTime()));
          rowChanged=true;
        }
        break;

      case NotBetween:
        if ((type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER)&& value2!=null && value2!="") {
          rows = rows.filter((row: any) => hasOwn(row, prop) && !(+row[prop] > value && +row[prop]<value2));
          rowChanged=true;
        }
        else if ((type == ColumnTypes.DATE || format == ColumnTypes.DATE)&& value2!=null && value2!="") {
          rows = rows.filter((row: any) => hasOwn(row, prop) && !(new Date(dateToString(row[prop])).getTime() > new Date(value).getTime() && new Date(dateToString(row[prop])).getTime() < new Date(value2).getTime()));
          rowChanged=true;
        }
        break;

      case Equal:
        if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
          rows = rows.filter((row: any) => hasOwn(row, prop) && +row[prop] == value);
          rowChanged=true;
        }
        else if (type == ColumnTypes.DATE || format == ColumnTypes.DATE) {
          rows = rows.filter((row: any) => hasOwn(row, prop) && new Date(dateToString(row[prop])).getTime() == new Date(value).getTime());
          rowChanged=true;
        }
        else if (type == ColumnTypes.BOOLEAN) {
          rows = rows.filter((row: any) => hasOwn(row, prop) && row[prop] == value);
          rowChanged=true;
        }
        break;

      case NotEqual:
        if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
          rows = rows.filter((row: any) => hasOwn(row, prop) && +row[prop] !== value);
          rowChanged=true;
        }
        else if (type == ColumnTypes.DATE || format == ColumnTypes.DATE) {
          rows = rows.filter((row: any) => hasOwn(row, prop) && new Date(dateToString(row[prop])).getTime() !== new Date(value).getTime());
          rowChanged=true;
        }
        else if (type == ColumnTypes.BOOLEAN) {
          rows = rows.filter((row: any) => hasOwn(row, prop) && row[prop] !== value);
          rowChanged=true;
        }
        break;

      case GreaterThan:
        if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
          rows = rows.filter((row: any) => hasOwn(row, prop) && +row[prop] > value);
          rowChanged=true;
        }
        else if (type == ColumnTypes.DATE || format == ColumnTypes.DATE) {
          rows = rows.filter((row: any) => hasOwn(row, prop) && new Date(dateToString(row[prop])).getTime() > new Date(value).getTime());
          rowChanged=true;
        }
        break;

      case GreaterThanOrEqual:
        if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
          rows = rows.filter((row: any) => hasOwn(row, prop) && +row[prop] >= value);
          rowChanged=true;
        }
        else if (type == ColumnTypes.DATE || format == ColumnTypes.DATE) {
          rows = rows.filter((row: any) => hasOwn(row, prop) && new Date(dateToString(row[prop])).getTime() >= new Date(value).getTime());
          rowChanged=true;
        }
        break;

      case LessThan:
        if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
          rows = rows.filter((row: any) => hasOwn(row, prop) && +row[prop] < value);
          rowChanged=true;
        }
        else if (type == ColumnTypes.DATE || format == ColumnTypes.DATE) {
          rows = rows.filter((row: any) => hasOwn(row, prop) && new Date(dateToString(row[prop])).getTime() < new Date(value).getTime());
          rowChanged=true;
        }
        break;

      case LessThanOrEqual:
        if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
          rows = rows.filter((row: any) => hasOwn(row, prop) && +row[prop] <= value);
          rowChanged=true;
        }
        else if (type == ColumnTypes.DATE || format == ColumnTypes.DATE) {
          rows = rows.filter((row: any) => hasOwn(row, prop) && new Date(dateToString(row[prop])).getTime() <= new Date(value).getTime());
          rowChanged=true;
        }
        break;
    }
  }
  if(logicalOperator=="OR" && rowChanged==false)
  {
    return null;  
  }
  else{
    return rows;
  }
}

export function filterTablesBasedOnQueryOperator(operator: string, value: any, fields: string,logicalOperator:string, value2?: any, type?:string,format?: string,queryType?:string) {
  let query="";
  if (type == ColumnTypes.DATE || format == ColumnTypes.DATE){
    value = value? dateToString((<Date>value)):null
  }
  if (operator != "" && value !="" && value!=null && fields!=undefined) {
    switch (operator) {
      case Contains:
        query +=` ${logicalOperator}  ${getFieldByQueryType(fields,queryType)} LIKE '%${value}%' `;
        break;

      case DoesNotContain:
        query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} NOT LIKE '%${value}%' `;
        break;

      case BeginsWith:
        query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} LIKE '${value}%' `;
        break;

      case DoesNotBeginWith:
        query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} NOT LIKE '${value}%' `;
        break;

      case EndsWith:
        query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} LIKE '%${value}' `;
        break;

      case DoesNotEndWith:
        query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} NOT LIKE '%${value}' `;
        break;

      // // Number or Date Types

      case Between:
        if ((type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER)&& value2!=null && value2!="") {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} BETWEEN ${value} AND ${value2}`;
        }
        else if ((type == ColumnTypes.DATE || format == ColumnTypes.DATE)&&value2!=null && value2!="") {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} BETWEEN '${value}' AND '${value2}'`;
        }
        else if ((type == ColumnTypes.TIME || format == ColumnTypes.TIME)&&value2!=null && value2!="") {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} BETWEEN '${value}' AND '${value2}'`;
        }
        break;

        case NotBetween:
        if ((type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER)&& value2!=null && value2!="") {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} NOT BETWEEN ${value} AND ${value2}`;
        }
        else if ((type == ColumnTypes.DATE || format == ColumnTypes.DATE)&& value2!=null && value2!="") {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} NOT BETWEEN '${value}' AND '${value2}'`;
        }
        else if ((type == ColumnTypes.TIME || format == ColumnTypes.TIME)&& value2!=null && value2!="") {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} NOT BETWEEN '${value}' AND '${value2}'`;
        }
        break;

        case Equal:
          if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
            query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} = ${value}`;
          }
          else if (type == ColumnTypes.DATE || format == ColumnTypes.DATE) {
            query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} = DATE('${value}')`;
          }
          else if (type == ColumnTypes.BOOLEAN) {
            query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} = ${value}`;
          }
          else if (type == ColumnTypes.TIME || format == ColumnTypes.TIME) {
            query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} = TIME('${value}')`;
          }
        break;

      case NotEqual:
        if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} != ${value}`;
        }
        else if (type == ColumnTypes.DATE || format == ColumnTypes.DATE) {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} != DATE('${value}')`;
        }
        else if (type == ColumnTypes.BOOLEAN) {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} != ${value}`;
        }
        else if (type == ColumnTypes.TIME || format == ColumnTypes.TIME) {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} != TIME('${value}')`;
        }
        break;

      case GreaterThan:
        if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} > ${value}`;
        }
        else if (type == ColumnTypes.DATE || format == ColumnTypes.DATE) {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} > DATE('${value}')`;
        }
        else if (type == ColumnTypes.TIME || format == ColumnTypes.TIME) {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} > TIME('${value}')`;
        }
        break;

      case GreaterThanOrEqual:
        if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} >= ${value}`;
        }
        else if (type == ColumnTypes.DATE || format == ColumnTypes.DATE) {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} >= '${value}'`;
        }
        else if (type == ColumnTypes.TIME || format == ColumnTypes.TIME) {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} >= '${value}'`;
        }
        break;

      case LessThan:
        if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} < ${value}`;
        }
        else if (type == ColumnTypes.DATE || format == ColumnTypes.DATE) {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} < '${value}'`;
        }
        else if (type == ColumnTypes.DATE || format == ColumnTypes.TIME) {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} < '${value}'`;
        }
        break;

      case LessThanOrEqual:
        if (type == ColumnTypes.INTEGER || type == ColumnTypes.NUMBER) {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} <= ${value}`;
        }
        else if (type == ColumnTypes.DATE || format == ColumnTypes.DATE) {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} <= '${value}'`;
        }
        else if (type == ColumnTypes.TIME || format == ColumnTypes.TIME) {
          query +=` ${logicalOperator} ${getFieldByQueryType(fields,queryType)} <= '${value}'`;
        }
        break;
    }
  }
  return query;
}

export function getFieldByQueryType(fields,queryType){
  let fieldforamte = fields;
  if(queryType == AppConsts.SQKLITEQUERY){
    fieldforamte = ` json_extract(payload,'$.${fields}') `;
  }
  return fieldforamte;
}