import { EventEmitter } from "@angular/core";
import { TreeNode } from "@circlon/angular-tree-component";

export class Datatypes {
  public static PEOPLE = "People";
  public static CONSTRUCTION = "Construction";
  public static BUSINESSINTELLIGENCE = "Business Intelligence";
  public static ENTITYINFO = "EntityInfo";
  public static BUSINESSINTELLIGENCEENTITIES = "BusinessIntelligenceEntities";
  public static USERPERMISSION = "UserPermission";
  public static MAPS = "Maps";
  public static LOADFORMDATA = "LoadFormData";
  public static SEARCHDATA = "SearchData";
  public static REPORTDATA = "ReportData";
  public static LOADDATA = "LoadData";
  public static FILTER = "Filter";
  public static ENTITYDATA = "EntityData";
  public static PDFDOC = "PDFDOC";
  public static REVIEWANDAPPROVALS = "UPDATE_REVIEW_APPROVAL";
  public static USERSIGNATURE = "UserSignature";
}

export class ReportType {
  public static SINGLE = "SINGLE";
  public static MULTIPLE = "MULTIPLE";

}


export class RightSideClickTreeEventModel {

  constructor(public head: any, public capability?: string, data?: any) {

  }

}

export class MessageKind {
  public static PARTIALUPDATE = 'PARTIALUPDATE';
  public static CREATE = 'CREATE';
  public static UPDATE = 'UPDATE';
  public static READ = 'READ';
  public static DELETE = 'DELETE';
}


export class FABRICS {
  public static SECURITY = "Security";
  public static PEOPLEANDCOMPANIES = "People & Companies";
  public static BUSINESSINTELLIGENCE = 'Business Intelligence';
  public static MAPS = "Maps";
  public static COMMON = "CommonFabric";
  public static HOME = "Home";
  public static ENTITYMANAGEMENT = "Entity Management";
  public static CONSTRUCTION = "Construction";
  public static REPORTING = "Reporting";
  public static POSSIBLEPIPELINEINTEGRITY = "Possible Pipeline Integrity";
  public static FABRICATION = "Fabrication";
  public static PROJECTS = "Projects";


}

export class CAPABILITY_NAME {
  public static CONSTRUCTION = "Construction";
  public static REPORTING = "Reporting";
  public static POSSIBLEPIPELINEINTEGRITY = "Possible Pipeline Integrity";
  public static FABRICATION = "Fabrication";
  public static PROJECTS = "Projects";
  public static ENTITYMANAGEMENT = "Entity Management";
  public static PEOPLEANDCOMPANIES = 'People & Companies';
  public static MAPS = 'Maps';
  public static BUSINESSINTELLIGENCE = 'Business Intelligence';
  public static SECURITY = 'Security';
}

export interface AngularTreeMessageModel {
  "fabric": string;
  "treeName": Array<string>;
  "treeOperationType": string;
  "treePayload": any;
  "treeConfig"?: any;
}

export interface AngularTreeEventMessageModel {
  "fabric": string;
  "treeName": Array<string>;
  "treeOperationType": string;
  "treePayload": any;
  "Event": EventEmitter<any>
}

export interface SendToComponentMyFormEvent {
  "ComponentName": string;
  "Event": EventEmitter<any>;
  "MyForm": any;
}

export interface GetServiceInstanceMessage {
  "ServiceName": string;
  "Instance": any;
}

export interface GetServiceInstanceEventModel {
  "ServiceName": string;
  "Instance": any;
  "Event": EventEmitter<any>;
}

export interface ReceiveFromComponentMyFormEvent {
  "ComponentName": string;
  "MyForm": any;
}

export interface TreeDataEventResponseModel {
  "fabric": string;
  "treeName": Array<string>;
  "treePayload": any;
}

export interface ObservableStateMessage {
  Capability: string,
  Tree: string,
  observable: any
}

export interface TreeAndNodeEventModel {
  tree: any;
  treeNode: TreeNode
}


export interface CapabilityMessageModelToFabric {
  "Event": string;
  "selctedNode": any;
  "Tab": string;
  "SelectedMenuName"?: string;
  "SelectedMenuData"?: any
}

export interface CapabilityMessageModel {
  "fabric": string;
  "dataType": string;
  "tab": string;
  "Payload": any;
  "AccordianPayload"?: [{
    "AccordianTabName": string;
    "Payload": any;
  }];
  "others"?: any
}
export interface GetDesktopDependentComponentInstanceModel {
  "ID": string;
  "Type": string;
  "CurrentRouting": string;
  "CurrentInstance": string;
  "RoutingType": string;
  "Valid": boolean;
  "PayLoad": any;
  "Event": EventEmitter<any>;
}

export module FabricsPath {
  export const PEOPLEANDCOMPANIES = "People & Companies";
  export const BUSINESSINTELLIGENCE = "Business Intelligence";
  export const MAPS = "Maps";
  export const SECURITY = "Security";
  export const HOME = "Home";
  export const ENTITYMANAGEMENT = "Entity Management";
  export const CONSTRUCTION = "Construction";
  export const REPORTING = "Reporting";
  export const POSSIBLEPIPELINEINTEGRITY = "Possible Pipeline Integrity";
  export const FABRICATION = "Fabrication";
  export const PROJECTS = "Projects";
}

export class TypeOfForm {
  public static DataEntry = "DataEntry";
  public static Data = "Data";
  public static Configuration = "Configuration";
  public static Settings = "Settings";
  public static MultiDateView = "MultiDateView";
}

export class ServicesNames {
  public static DESKTOPSERVICE = "DESKTOPSERVICE";
}


export module FabricsNames {
  export const PEOPLEANDCOMPANIES = "People & Companies";
  export const BUSINESSINTELLIGENCE = "Business Intelligence";
  export const MAPS = "Maps";
  export const SECURITY = "Security";
  export const HOME = "Home";
  export const ENTITYMANAGEMENT = "Entity Management";
  export const REPORTING = "Reporting";
  export const POSSIBLEPIPELINEINTEGRITY = "Possible Pipeline Integrity";
  export const FABRICATION = "Fabrication";
  export const PROJECTS = "Projects";
  export const CONSTRUCTION = "Construction";
}

export module Tabs {
  export const PEOPLE = "people";
  export const BI = "bi";
  export const MAPS = "maps";
  export const SECURITY = "security";
  export const ENTITYMANAGEMENT = "em";
  export const REPORTING = "reporting";
  export const POSSIBLEPIPELINEINTEGRITY = "ppi";
  export const FABRICATION = "fabrication";
  export const PROJECTS = "projects";
  export const CONSTRUCTION = "construction";
  export const PERSON = "person";

}

export module TreeTypeNames {
  export const ALL = "All";
  export const LIST = "Lists";
  export const SEARCH = "Search";
  export const Construction = "Construction";
  export const Schedule = "Schedule";
  export const TaggedItems = "Tagged Items";
  export const PhysicalInstances = "Physical Instances";
  export const Directory = "Directory";
  export const Procedures = "Procedures";
  export const Materials = "Materials";
  export const Documents = "Documents";
  export const Approvals = "Approvals";
  export const Maps = "Maps";
  export const Turnover = "Turnover";
  export const TurnoverApproveTree = "turnoverApproveTree";


  export const CompaniesOrPeople = "Companies/People";

  // export const LICENSEE = "Licensee";
  export const PHYSICALFLOW = "PHYSICAL FLOW";
  export const AvailableEntities = "AvailableEntities";
  export const AvailableAddListEntities = "AvailableAddListEntities";

  export const RoleTree = 'RoleTree';
  // export const DRAWINGS = 'DRAWINGS';
  // export const WORKFLOW = 'WORK FLOW';
  // export const GeoAnalytics = 'Geo Analytics';
  export const SharedPersons = 'SharedPersons';
  export const ActivityCenter = 'Activity Center';
  // export const AssetManagement = 'Asset Management';
  export const PeopleAndCompanies = 'People & Companies';
  export const BusinessIntelligence = 'Business Intelligence';
  export const GroupsAndPeopleInShare = 'GroupsAndPeopleInShare';
  export const rolesTreeForPermissions = 'rolesTreeForPermissions';
  export const SecurityGroupsPeopleTree = 'SecurityGroupsPeopleTree';
  export const peopleTreeForPermissions = 'peopleTreeForPermissions';
  export const PermissionsPeopleLeftTree = 'PermissionsPeopleLeftTree';
  export const SecurityGroupsEntitiesTree = 'SecurityGroupsEntitiesTree';
  export const securityGroupsTreeForPermissions = 'securityGroupsTreeForPermissions';
  export const rolesTreeForPermissionsRead = 'rolesTreeForPermissionsRead';
  export const SECURITY = 'Security';
  export const ENTITYMANAGEMENT = "Entity Management";
  /** EntityMAnagement Right Tree */
  export const PeopleAndCompanies_Right = 'People & Companies_RIGHT';
  export const BusinessIntelligence_RIGHT = 'Business Intelligence_RIGHT';



  export const REPORTING = "Reporting";
  export const POSSIBLEPIPELINEINTEGRITY = "Possible Pipeline Integrity";
  export const FABRICATION = "Fabrication";
  export const PROJECTS = "Projects";
  export const CONSTRUCTION = "Construction";
}


export module TreeOperations {
  export const filterTree = "filterTree";
  export const ReArrange = "ReArrange";
  export const AddNewNode = "AddNewNode";
  export const NodeOnDrop = "NodeOnDrop";
  export const createTree = "createTree";
  export const getNodeById = "getNodeById";
  export const treeConfig = "treeConfig";
  export const RefreshTree = 'RefreshTree';
  export const NodeOnClick = "NodeOnClick";
  export const filteredTree = "filteredTree";
  export const RemoveFilter = "RemoveFilter";
  export const ClearSearch = "ClearSearch";
  export const entityFilter = "entityFilter";
  export const expanderClick = "expanderClick";
  export const CreateNewNode = "CreateNewNode";
  export const deleteNodeById = "deleteNodeById";
  export const updateNodeById = "updateNodeById";
  export const moveNodeFromTo = "moveNodeFromTo";
  export const MultiOperation = "MultiOperation";
  export const createFullTree = "createFullTree";
  export const NodeOnDblClick = "NodeOnDblClick";
  export const createRightTree = 'createRightTree';
  export const GetTreeInstance = "GetTreeInstance";
  export const addOrUpdateNode = "addOrUpdateNode";
  export const GetFullTreeData = "GetFullTreeData";
  export const filteroptionjson = "filteroptionjson";
  export const ActiveSelectedNode = "ActiveSelectedNode";
  export const FullTreeOnCreation = "FullTreeOnCreation";
  export const AddNewFullChildrens = "AddNewFullChildrens";
  export const selectedfilteroption = "selectedfilteroption";
  export const treeContextMenuOption = "treeContextMenuOption";
  export const deactivateSelectedNode = "deactivateSelectedNode";
  export const deactivateAllAvtiveNode = "deactivateAllAvtiveNode";
  export const AddNewNodeOtherSession = "AddNewNode_OtherSession";
  export const NodeContextOptionClicked = "NodeContextOptionClicked";
  export const CreateUpdateentityFilter = "CreateUpdateentityFilter";
  export const NodeOnContextClickBeforeInit = "NodeOnContextClickBeforeInit";
  export const deleteNodebasedOnparentEntity = "deleteNodebasedOnparentEntity";
  export const multiSelectDeActive = "MultiSelectDeActive";
  export const refreshUpdateTree = "RefreshUpdateTree";
  export const expandeORcollapseClick = "expandeORcollapseClick";
  export const sortingAfterUpdatingTreename = "sortingAfterUpdatingTreename";
  export const updatedropPointer = "updatedropPointer";

}

export module TypeOfActivity {
  export const CONFIGURATION = "Configuration";
  export const GENERAL = "Genaral";
  export const DATAENTRY = "DataEntry";
}
export module TypeOfEntity {

  /**
   * People Fabric
   */
  export const Company = "Company";
  export const Office = "Office";
  export const Person = "Person";
  export const Tagged = "Tagged";
  export const Material = "Material";
  export const Document = "Documents";

}

export class FORMTYPES {
  public static EntitiesSchemas = "EntitiesSchemas";
  public static List = "List";
  public static UserPreference = "UserPreference";
}

export interface ComponentInstanceEventMessageModel {
  "fabric": string;
  "EntityType": string;
  "EntityId": string;
  "FieldName": string;
  "OperationType": string;
  "PayLoad": any;
  "Event": EventEmitter<any>;
  "EntityName": string;
}

export interface UtilMessage {
  MessageType: string;
  Payload: any;
}

export interface IPInfo {
  ip: string;
}

export class FormEventTypeName {
  public static ExpandCollapse = "ExpandCollapse";
  public static clickEvent = "clickEvent";
  public static SelectData = "SelectData";
  public static DataLockDate = "DataLockDate";
}

export class ConstructionRightMenus {
  public static Schedule = "Schedule";
  public static TaggedItems = "Tagged Items";
  public static PhysicalInstances = "Physical Instances";
  public static Materials = "Materials";
  public static Documents = "Documents";
  public static Turnover = "Turnover";
}


export interface CapabilityModel {
  source: string,
  sourceSub: string,
  title: string,
  tooltip: string,
  routerLinkActive: boolean,
  isFavIcon: boolean,
  routerLink: string,
  queryParameter: object,
  id: string,
  baseRoute: string,
  class: string,
  order: number,
  admin: boolean,
  Fabric: string,
  entityId: string,
  offlineStatus: boolean,
  Show: boolean,
}

export class MessagesCount {
  totalCount: number = 0;
  curentCount: number = 0
  constructor(a: number, b: number) {
    this.totalCount = a;
    this.curentCount = b;
  }
}

export class SERVICETYPE {
  public static CONSTRUCTIONAPP = "ConstructionApp";
  public static CONSTRUCTIONSEARCHAPP = "ConstructionService";
  public static CAPEXAPP = "CapexApp";
}