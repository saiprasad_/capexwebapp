export class EntityTypes {
  public static Map = "Map";

  public static Pig = "Pig";
  public static DELIVERED = "Delivered";
  // public static ActivityTree = "ActivityTree";
  // public static TaskCategory = "TaskCategory";
  // public static Collection = "Collection";
  // public static ActivityBoard = "ActivityBoard";
  public static List = "List";
  // public static Task = "Task";
  public static EntityInfo = "EntityInfo";
  public static Bi = "Bi";
  public static BIGDATA = "bigdata";
  // public static DataSource = "DataSource";
  // public static Table = "Table";

  // public static Drawings = "Drawings";
  public static HaulLocation = "HaulLocation";

  // public static IdeasCategory = "IdeasCategory";
  public static DashBoard = "DashBoard";

  public static Category = "Category";

  // public static Abandoned = "Abandoned";

  public static SharedList = "SharedList";
  public static Shared = "Shared";

  // public static Suspended = "Suspended";

  public static People = "People";
  public static Construction = "Construction";
  public static TaggedItems = "TaggedItems";
  public static PhysicalInstances = "PhysicalInstances";
  public static Materials = "Materials";
  public static Documents = "Documents";
  public static Tasks = "Tasks";
  public static Project = "Project";
  public static Task = "Task";
  public static Facility = "Facility";
  public static Area = "Area";
  public static Concrete = "Concrete";
  public static Equipment = "Equipment";
  public static Building = "Building";
  public static Spool = "Spools";
  public static Weld = "Welds";
  public static Pipes = "Pipe";
  public static Safety = "Safety";
  public static Exhibits = "Exhibits";
  public static LEMS = "LEMS";
  public static Environment = "Environment";
  public static Journals = "Journals";
  public static Turnover = "Turnover";



  // public static PEOPLECATEGORY = "PeopleCategory";
  public static COMPANY = "Company";
  // public static GROUP = "Group";
  public static PERSON = "Person";
  public static OFFICE = "Office";

  public static SECURITYGROUP = 'SecurityGroup';
  public static FILTER = 'Filter';
  public static AVAILABLEFILTERS = 'AvailableFilters';
  // public static SECURITYROOTNODE = 'SecurityRootNode';

  public static Carousel = "Carousel";

  // public static Pallete = "Pallete";
  public static UserPreference = "UserPreference";
  // public static UserCreateDeletePermission = "UserCreateDeletePermission";
  public static UserList = "UserList";
  public static PersonalList = "PersonalList";
  public static Personal = "Personal";
  // public static User = "User";
  // public static Capability = "Capability";


  public static Subtopic = "Subtopic";
  public static Participant = "Participant";
  public static ActivityCenter = "ActivityCenter";
  public static channelList = "channelList";
  public static ParticipantTopic = "ParticipantTopic";
  public static ParticipantLastChat = "ParticipantLastChat";


  // public static AssetsLayer = "AssetsLayer";
  // public static AssetsPolyArea = "AssetsLayerPolyArea";
  // public static AssetsAreas = "AssetsAreas";
  // public static Authorization = "Authorization";
  // public static AssetsTileLayerUserPermission = "AssetsTileLayerUserPermission";
  // public static AssetAdminForms = "AssetAdminForms";
  public static ElectricalSubmersible = "ElectricalSubmersible";
  public static FDC = "FDC";
  public static Rod = "Rod";
  public static ProgressiveCavity = "ProgressiveCavity";

  public static DISTRICT = "District";
  public static AREA = "Area";
  public static FIELD = "Field";

  // public static BATTERY = "Battery";
  // public static LOCATIONS = "Locations";
  public static WATERINJECTION = "WaterInjection";
  public static GASWELL = "GasWell";
  public static OILWELL = "OilWell";
  public static GASWELLPRODUCINGOIL = "GasWellProducingOil";
  public static GASINJECTIONWELL = "GasInjectionWell";
  public static WATERINJECTIONFACILITY = "WaterInjectionFacility";
  public static WATERINJECTIONWELL = "WaterInjectionWell";
  public static GASDISPOSALWELL = "GasDisposalWell";
  public static WATERDISPOSALWELL = "WaterDisposalWell";
  public static WATERSOURCEFACILITY = "WaterSourceFacility";
  public static WATERSOURCEWELL = "WaterSourceWell";
  public static STEAMINJECTION = "SteamInjection";
  public static TRUCKTERMINAL = "TruckTerminal";
  public static GASINJECTIONFACILTY = "GasInjectionFacility";
  public static WATERSOURCEFACILTY = "WaterSourceFacility";

  public static PLANT = "Plant";
  public static GASPLANT = "GasPlant";
  public static SATELLITE = "Satellite";
  public static PAD = "Pad";

  public static COMPRESSORSTATION = "CompressorStation";

  public static WASTEFACILITY = "WasteFacility";
  public static TREATINGFACILITY = "TreatingFacility";
  public static BOOSTERSTATION = "BoosterStation";
  public static METERSTATION = "MeterStation";
  public static GASBATTERY = "GasBattery";
  public static OILBATTERY = "OilBattery";
  public static WASTEFACILTY = "WasteFacility";
  public static TREATINGFACILTY = "TreatingFacility";
  public static INVENTORYSTORAGE = "InventoryStorage";
  public static NONMETERED = "NonMetered";
  public static GASINJECTIONFACILITY = "GasInjectionFacility";

  public static SafetyEquipment = 'SafetyEquipment';
  public static CathodicProtection = 'CathodicProtection';
  public static SafetyDeviceLockouts = 'SafetyDeviceLockouts';
  public static EyeWash = 'EyeWash';

  public static FACILITYCODE = "FacilityCode";
  // public static FACILITYEFFICIENCY = "FacilityEfficiency";


  public static DEHY = 'DEHY';
  public static TANK = "Tank";
  public static TURBINE = "Turbine";
  public static ORIFICE = "Orifice";
  public static PDMETER = "PDMeter";
  public static TRUCKTICKET = "TruckTicket";
  public static HEADER = "Header";
  public static RISER = "Riser";
  public static SEPARATOR = "Separator";
  // public static THETER = "Theter";
  public static TREATER = "Treater";
  public static SPLITTER = "Splitter";
  public static FFV = "FFV";
  public static CORIOLIS_METER = "CoriolisMeter";
  public static ROTAMETER = "RotaMeter";
  public static COMPRESSOR = "Compressor";
  public static ULTRASONIC = "Ultrasonic";
  public static PUMP = "Pump";
  public static PUMPJACK = "Pumpjack";
  public static TRUCK = "Truck";
  public static Tanks = "Tanks";
  public static ESD = "ESD";
  public static D13 = "D13";
  public static Bump = "Bump";
  public static HeatMap = "HeatMap";
  public static Burners = "Burners";
  public static PumpJackPressureSwitch = "PumpJackPressureSwitch";
  public static LandOwnersComplaint = "LandOwnersComplaint";
  // public static AssetCategoryCatalog = "AssetCategoryCatalog";
  public static FireExtinguishers = "FireExtinguishers";
  public static PersonalMonitor = "PersonalMonitor";
  public static SCBA = "SCBA";
  // public static PermissionsList = "PermissionsList";
  // public static PermissionsListHeader = "PermissionsListHeader";
  public static Role = "Role";
  // public static AssetWell = "AssetWell";
  public static UltrasonicMeter = "UltrasonicMeter";
  // public static AssetCoriolisMeter = "AssetCoriolisMeter";
  // public static AssetRotaMeter = "AssetRotaMeter";
  // public static AssetPDMeter = "AssetPDMeter";
  public static OrificeMeter = "OrificeMeter";
  public static CathodicRectifier = "CathodicRectifier";
  public static Plunger = "Plunger";
  public static ElectricalLogBook = "ElectricalLogBook";
  public static Carseal = "Carseal";
  public static Schedule = "Schedule";
  public static Vessels = "Vessels";
  public static DownholePumps = "DownholePumps";
  public static Vehicle = "Vehicle";
  public static Tier1Facility = "Tier1Facility";
  public static TurbineMeter = "TurbineMeter";
  public static CompressorBooster = "CompressorBooster";

  public static SafetyEquipmentConfiguration = "SafetyEquipmentConfiguration";
  public static CathodicProtectionConfiguration = "CathodicProtectionConfiguration";
  public static SafetyDeviceLockoutsConfiguration = "SafetyDeviceLockoutsConfiguration";
  public static EyeWashConfiguration = "EyeWashConfiguration";
  public static BurnersConfiguration = "BurnersConfiguration";
  public static CathodicRectifierConfiguration = "CathodicRectifierConfiguration";
  public static ESDConfiguration = "ESDConfiguration";
  public static ElectricalLogBookConfiguration = "ElectricalLogBookConfiguration";
  public static FireExtinguishersConfiguration = "FireExtinguishersConfiguration";
  public static PlungerConfiguration = "PlungerConfiguration";
  public static SCBAConfiguration = "SCBAConfiguration";
  public static TankConfiguration = "TankConfiguration";
  public static VehicleConfiguration = "VehicleConfiguration";
  public static CarsealConfiguration = "CarsealConfiguration";
  public static PersonalMonitorConfiguration = "PersonalMonitorConfiguration";

  public static CATALOG = "Catalog";
  public static DATASET = "Dataset";
  public static TABLE = "Table";
  public static TABLEFIELDS = "TableFields";
  public static SINGLEDATASET = "SingleDataset"
  public static Test = "Test";

  public static SecurityGroup = "SecurityGroup";
  public static FDCGlobalAdmin = "FDCGlobalAdmin";
  public static EMGlobalAdmin = "EMGlobalAdmin";
  static FACILITY: any;
}


export interface ITreeConfig {
  treeActiveOnClick?: boolean
  treeExpandAll?: boolean
  treeDraggable?: boolean
  treeDropable?: boolean
}
export const ENTITYTYPES = {
  TASK: 'Task',
  PROJECT: 'Project'

}
