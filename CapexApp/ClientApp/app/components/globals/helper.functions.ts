import * as jsonPath from "jsonpath";
import { AppConsts, Properties, SQLiteCommonColumns, VirtualKey, METRIC,US_CUSTOMARY_COLUMN } from "../common/Constants/AppConsts";
import { EntityTypes } from "./Model/EntityTypes";

export function getFormVariables(schemaData, defaultOptions) {
  let data = {
    layout: schemaData.layout,
    schema: schemaData.schema,
    roles: schemaData.roles,
    options: getDefaultOptions(schemaData.options, defaultOptions),
    nestedSchema: schemaData.nestedSchema,
    // this.userRoles = this.getUserRoles();
    tableName: schemaData.tableName,
    readDataFromOtherSource: schemaData.readDataFromOtherSource,
    parentPage: schemaData.parentPage,
    queries: schemaData.queries,
    formulas: schemaData.formulas,
    conditionalFields: schemaData.conditionalFields,
    enum: schemaData.enums,
    breadcrumbDisplayName: schemaData.breadcrumbDisplayName,
    droppedRowsMapping: schemaData.droppedRowsMapping ? schemaData.droppedRowsMapping : [],
    title: schemaData.title ? schemaData.title.toUpperCase() : schemaData.title,
  };
  return data;
}

export function getDataFromJson(d: string, data) {
  let formField = null;
  let field = null;
  let fieldIn = null;
  if (d.indexOf("/") >= 0) {
    formField = d.split("/");  // /Header/DailyReportNumber
    field = formField[formField.length - 1];     // DailyReportNumber
    fieldIn = formField[formField.length - 2];  //Header
  } else {
    formField = d.split(".");  // Header.DailyReportNumber
    field = formField[1];     // DailyReportNumber
    fieldIn = formField[0];  //Header
  }

  let fieldInNode = jsonPath.nodes(data, fieldIn, 1);
  let paramValue = null;
  if (fieldInNode.length == 1) {
    let val = fieldInNode[0].value;
    if (val) {
      paramValue = val[field];
    }
  }
  return paramValue;
}

export function getDefaultOptions(options: any, defaultOptions) {
  if (options) {
    for (const key of Object.keys(options)) {
      defaultOptions[key] = options[key];
    }
  }
  return defaultOptions;
}
export function getQueryParam(queries: Array<any>, queryName) {
  let queryNode = queries.find(query => query.property == queryName);
  if (queryNode) {
    return queryNode.queryParameter;
  }
  else {
    return null;
  }
}
export function getDataPointer(key: string, schema: any) {
  key = key != null ? key.toLowerCase() : null;
  let isKeyFound = { "found": false };
  const properties: any = schema.properties;
  const keys: string[] = Object.keys(properties);

  if (keys.includes(key)) {
    return '/' + key;
  }

  let dataPointer: string = '';
  for (let prop of keys) {
    if (!isKeyFound.found) {
      dataPointer = '';
      const node: any = properties[prop];
      if (node.hasOwnProperty(VirtualKey) && node.virtualKey) {
        dataPointer = getNestedProperty(node, key, prop, dataPointer, isKeyFound);
      }
    }
  }
  return dataPointer;
}
export function getNestedProperty(node: any, key: string, prop: string, dataPointer: string, isKeyFound) {
  dataPointer += '/' + prop;
  if (node.hasOwnProperty(VirtualKey) && node.virtualKey && node.hasOwnProperty(Properties)) {
    for (const innerProp of Object.keys(node.properties)) {
      if (key == innerProp) {
        isKeyFound.found = true;
        return dataPointer += '/' + innerProp;
      }
      const innerNode: any = node.properties;
      getNestedProperty(innerNode, key, innerProp, dataPointer, isKeyFound);
    }
  }
  return dataPointer;
}
export function getKeyValuePair(queryParamString: string, liveData: any) {
  let data = {};
  if (queryParamString != null) {
    let queryParams = queryParamString.split(";");
    for (let queryParam of queryParams) {
      let param = queryParam.split("=");
      if (param.length == 2) {
        if (liveData && param[1].indexOf(":") > 0) {
          let formField = param[1].split(":");  // Header:DailyReportNumber
          let field = formField[1];     // DailyReportNumber
          let fieldIn = formField[0];  //Header
          let fieldInNode = jsonPath.nodes(liveData, fieldIn.toLowerCase(), 1);
          let paramValue = null;
          if (fieldInNode.length == 1) {
            let val = fieldInNode[0].value;
            if (val) {
              paramValue = val[field];
            }
          }
          data[param[0]] = paramValue;

        } else {
          data[param[0]] = param[1];
        }
      }
    }
  }
  return data;
  
}
export function getQuery(queries: Array<any>, queryName) {
  let queryNode = queries.find(query => query.property == queryName);
  if (queryNode) {
    return queryNode.query;
  }
  else {
    return null;
  }
}
export function getTableName(schemaName: string, accordianName: string = null) {
  let tableName: string = null;

  if (accordianName) {
    tableName = schemaName + "_" + accordianName;
  }
  else {
    tableName = schemaName;
  }
  tableName = tableName ? tableName.replace(/[^a-zA-Z0-9_]/g, '') : tableName;
  return tableName.toLowerCase();
}



export function isValidJSON(text: string) {
  if (/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
    replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
    replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
    return true;
  }
  else {
    return false;
  }
}

export function setData(data, existingData) {
  if (data != null) {
    let keys = Object.keys(data);
    if (keys.length > 0) {
      let resultdata = existingData ? existingData : {};
      for (let key of keys) {
        if (data[key] instanceof Array) {
          resultdata[key] = (<Array<any>>data[key]).splice(0);
        }
        else if (data[key] instanceof Object) {
          if (resultdata[key] == undefined) {
            resultdata[key] = data[key];
          }
          else {
            for (let key1 of Object.keys(data[key])) {
              resultdata[key][key1] = data[key][key1];
            }
          }
        }
        else {
          resultdata[key] = data[key];
        }
      }

      existingData = Object.assign({}, resultdata);
      if (existingData) {
        for (let key of Object.keys(existingData)) {
          if (!existingData[key])
            delete existingData[key];
        }
      }
    }
    return existingData;
  }
}


export function getNode(dataPointer, schema) {
  if (dataPointer) {
    let pathString = getPathString(dataPointer);
    return jsonPath.query(schema, `$${pathString}`, 1);
  }
}

export function getPathString(dataPointer: string) {
  if (dataPointer.indexOf("/") == 0) {
    dataPointer = dataPointer.substring(1, dataPointer.length);
  }
  let paths = dataPointer.split("/");

  let pathString = "";
  for (let path of paths) {
    pathString += ".properties." + path;
  }
  return pathString;
}


export function getInlineDataPointer(schema, inputDataPointer) {
  let inlinePointer: string = '';
  const dataPointer: string = inlinePointer = '/' + (<string>inputDataPointer).split('/')[1];
  let node = getNode(dataPointer, schema);
  if (node && node.length > 0) {
    let data: any = {};
    const props: any = node[0].properties;
    if (props && Object.keys(props).length > 0) {
      for (const key in props) {
        const prop: any = props[key];
        if (prop.type == "array") {
          data[key] = {};
          inlinePointer += '/' + key;

        }
      }
    }
  }

  return inlinePointer;
}


export function getTableNames(schemaName, tableName, schema) {
  tableName = tableName ? tableName : schemaName;
  tableName = getTableName(tableName);

  let tableNames = [];
  let dataPointerWithTableNames = {}
  if (schema && schema.properties) {
    const properties: any = schema.properties;
    for (let prop of Object.keys(properties)) {
      createTableNames(properties, prop, tableNames, dataPointerWithTableNames, tableName);
    }
  }
  return { "uniqueTables": tableNames, "tables": dataPointerWithTableNames };
}

export function createTableNames(properties: any, prop: any, tableNames: any[], dataPointerTable: {}, schemaPage: string) {
  const node: any = properties[prop];
  if (node.hasOwnProperty(VirtualKey) && node.virtualKey && node.hasOwnProperty(properties)) {
    for (const innerProp of Object.keys(node.properties)) {
      const innerProperties: any = node.properties;
      createTableNames(innerProperties, innerProp, tableNames, dataPointerTable, schemaPage);
    }
  }
  else {
    let type: string = properties[prop].type;
    let tableName: string = properties[prop].tableName;
    let filterBy: string = properties[prop].filterBy;
    let primary = node.primary;
    let items = node.items;

    if (type == 'array' && !primary) {
      if (tableName == null) {
        tableName = getTableName(schemaPage, prop);
      }
      else {
        tableName = getTableName(tableName);
      }
    }
    else {
      tableName = getTableName(tableName != null ? tableName : schemaPage);
    }

    let isTableExist = tableNames.find(d => d == tableName);
    if (!isTableExist) {
      tableNames.push(tableName);
    }
    let fieldNames = [];
    if(node.properties){
      fieldNames = Object.keys(node.properties)
    }
    else if(items.properties){
      fieldNames = Object.keys(items.properties)
    }
    dataPointerTable[prop] = { tableName: tableName, primary: node.primary, type: type, filterBy: filterBy, fieldNames: fieldNames };
  }
}

export function getSearchlistBasedOnType(entityType:string){
  let searchlist = [];
  switch(entityType){
    case EntityTypes.Tasks:
      searchlist.push( 'tasks:entityname');
      break;
    case EntityTypes.TaggedItems:
      searchlist.push( 'taggeditems:tag');
      break;
    case EntityTypes.Materials:
      searchlist.push( 'materials:materialname');
      break;
    case EntityTypes.Documents:
      searchlist.push( 'documents:entityname');
      break;
    case EntityTypes.PhysicalInstances:
      searchlist.push( 'physicalinstance:entityname');
    break;
  }
  return searchlist;
}

export function getNewUUID() {
  return generateUUID();
}

export function generateUUID() {
  let d = new Date().getTime();
  const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    const r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
};

export function getUTCDateTime(dateField: string = new Date().toISOString()) {
  return new Date(dateField).toISOString().replace('T', ' ').split('.')[0];
}

export function replaceAll(string: string, search: string, replace: string) {
  return string.split(search).join(replace);
}

export function getLowerCase(value:string) {
  return value.toLowerCase();
}

export function getqueryParam(router){
  return router ? router.parseUrl(router.url).queryParams : {};
}

export function getShowStatus(id:string,entityType:string,tab:string):Boolean{
  let status = false;
  switch(id){
    case 'ngselect':
      status = AppConsts.HeaderNgSelect.indexOf(tab) >= 0 && entityType == EntityTypes.Project;
      break;
    case 'search':
      status = AppConsts.HeaderSearch.indexOf(tab) >= 0  && entityType == EntityTypes.Project;
      break;
    case 'filter':
      status = AppConsts.HeaderSearch.indexOf(tab) >= 0  && entityType == EntityTypes.Project;
      break;
  } 
  return status;
}

export function formateTreeDataToTable(nodes,rows){
  if(nodes){
    let node = {};
    let children = [];
     Object.keys(nodes).forEach(key =>{
       if(key == "Parent"){
         node['parententityid'] = nodes[key] && nodes[key].length ? nodes[key][0]['EntityId'] : null;
       }
       else if(key == "uuid"){
         node[key] = nodes["EntityId"];
       }else if(key == "children"){
         children = nodes[key];
         delete nodes[key];
       }else{
         node[key.toLowerCase()] = nodes[key];
       }
     });
     rows.push(node);
     children.forEach(item =>{
       formateTreeDataToTable(item,rows)
     });
   }
 }

 export function  getSelectedNodes(nodes) {
  let sampObj = [];
    if (nodes) {        
      var selectedTreeValues = Object.keys(nodes.treeModel.activeNodeIds);
      if (selectedTreeValues.length > 1) {
        selectedTreeValues.forEach(item => {
          let node = nodes.treeModel.getNodeById(item);
          if (node && node.isActive) {
            sampObj.push(node.data);
          }
        })
      } else {
        sampObj.push(nodes.data);
      }
    }
    return sampObj;
}

export function isNullOrEmptyString(data: string): boolean {
  var status = false;
  if (data == "" || data == null || data == undefined || data == "[]" || data == "{}") {
    status = true;
  }
  return status;
}

export function  generatePrimaryTableQuery(fieldMapping, primaryTable, entityid) {
  let fieldkeys = fieldMapping != null ? Object.keys(fieldMapping) : [];//{ "Number":"DailyReport.Header:WDSNumber"} => ["Number"]
  let queryFieldForSingleTable = '';
  let queryFieldForMultipleTable = '';
  let queryPayload = '';
  let queryTables = '';
  let queryConditions = '';
  let tableNameArray = [];
  let join = 'left';
  let primary_Table = '';
  let flattenColumn = '';
  if (!isNullOrEmptyString(primaryTable)) {
    primary_Table = primaryTable.replace(":", "_").replace(".", "_");
  }
  for (let key of fieldkeys) {
    let field = fieldMapping[key];
    let formFields = (<string>field).split(":");  //DailyReport.Header:WDSNumber

    if (formFields && formFields.length == 2) {
      let column = formFields[1]; //WDSNumber
      let tableName = formFields[0].replace(".", "_");//DailyReport_Header


      if (!tableNameArray.includes(tableName)) {
        // check alternative way of doing.. because uuid need to take only from primary table
        if (tableName == primary_Table) {
          queryPayload += ` ${tableName}.${SQLiteCommonColumns.ParentEntityId}, ${tableName}.${SQLiteCommonColumns.Parent},`;
        }
        tableNameArray.push(tableName);
        queryPayload += ` ${tableName}.${SQLiteCommonColumns.Payload} AS ${tableName}_payload,`;
        queryTables += `${tableName} ${join} join `;
      }

      if (SQLiteCommonColumns.DefaultColumnNames.includes(column.toLowerCase())) {
        queryPayload += ` ${tableName}.${column} as ${column},`;
        flattenColumn += `, ${column} as ${key}`;
      }
      else {
        queryFieldForSingleTable += `case  when json_value(payload,'$.${column}') is null then 'NULL' else json_value(payload,'$.${column}') end ${key}, `;
        queryFieldForMultipleTable += `case  when json_value(${tableName}_payload,'$.${column}') is NULL then 'NULL' else json_value(${tableName}_payload,'$.${column}') end ${key}, `;
      }
    }
  }

  let query = ``;
  if (tableNameArray.length == 1) {
    queryFieldForSingleTable = queryFieldForSingleTable.substring(0, queryFieldForSingleTable.lastIndexOf(','));
    query = `select ${SQLiteCommonColumns.UUID}, ${SQLiteCommonColumns.UUID}  as entityid, ${SQLiteCommonColumns.ParentEntityId}  ,  ${queryFieldForSingleTable} ${flattenColumn}  from  ${tableNameArray[0]}`;
  }
  else {
    for (let i = 0; i < tableNameArray.length - 1; i++) {
      queryConditions += `${tableNameArray[i]}.${SQLiteCommonColumns.Parent} = ${tableNameArray[i + 1]}.${SQLiteCommonColumns.Parent} and `;
    }
    queryFieldForMultipleTable = queryFieldForMultipleTable.substring(0, queryFieldForMultipleTable.lastIndexOf(','));
    queryPayload = queryPayload.substring(0, queryPayload.lastIndexOf(','));
    queryTables = queryTables.substring(0, queryTables.lastIndexOf(`${join}`));
    queryConditions = queryConditions.substring(0, queryConditions.lastIndexOf('and'));
    // Parent will be uuid of primary table and in all child forms parent is refering to uuid/parent of primary table
    query = `select ${SQLiteCommonColumns.Parent} as ${SQLiteCommonColumns.UUID}, ${SQLiteCommonColumns.Parent}  as ${SQLiteCommonColumns.EntityId}, ${SQLiteCommonColumns.ParentEntityId}  as ${SQLiteCommonColumns.ParentEntityId},  ${queryFieldForMultipleTable} ${flattenColumn}  from  (select ${queryPayload} from ${queryTables} on ${queryConditions}) as temp `;

  }
  return query;
}
export function updateUOM(schema, metric: string) {
  metric = metric.toLowerCase();
  const properties: any = schema.properties;
  const keys: string[] = Object.keys(properties);
  for (let prop of keys) {
    let node: any = properties[prop];
    setUOMInNested(node, metric);
  }
  return schema;
}

export function setUOMInNested(node: any, metric: string) {
  if (node.type == "object") {
    let nodeProps = node.properties
    const propKeys: string[] = Object.keys(nodeProps);
    for (let key of propKeys) {
      setUOMInNested(nodeProps[key], metric);
    }
  } else if (node.type == "array") {
    setUOMInNested(node.items, metric);
  } else {
    if (!node.suffix) {
      let metricVal = (metric == METRIC) ? METRIC : US_CUSTOMARY_COLUMN;
      if (node[metricVal]) {
        node.suffix = node[metricVal]
      }
    }
  }
}

export function isCapabilityAdmin(fabric:string,capabilityList:Array<any>){
  return capabilityList.findIndex(item => item.id.toLowerCase() == fabric.toLowerCase() && item.admin === true) >= 0; 
}