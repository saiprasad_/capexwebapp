import { Directive, ElementRef, HostListener, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
@Directive({
    selector: 'input[dynamicInputWidth]'
})
export class DynamicInputWidth  {

  @Input() decimalPrecision;
  @Input() PlaceHolderForInput;
  @Input() FieldType;
  @Input() ReactiveField;
  @Input() myForm:FormGroup;
  @Input() isDynamicWidth;

  constructor(private elementRef: ElementRef) { }

  ngOnInit(): void {
    if(this.isDynamicWidth){
    let sizeOfBox = this.elementRef.nativeElement.value.length;
    let placeholderlength = this.elementRef.nativeElement.placeholder.length;
    if(this.PlaceHolderForInput)
     var InputPlaceHolderLength=this.PlaceHolderForInput.length;
      if (sizeOfBox == 0 && placeholderlength==0 && InputPlaceHolderLength)
        this.elementRef.nativeElement.style.width = (InputPlaceHolderLength * 7) + "px";
     else 
    if(sizeOfBox == 0)
      this.elementRef.nativeElement.style.width = (placeholderlength * 7) + "px";
    else if (sizeOfBox * 7 < 100)
      this.elementRef.nativeElement.style.width = (sizeOfBox * 7) + "px";

      this.Valuechanges();
    }
  }

  Valuechanges(){
    if(this.ReactiveField && this.ReactiveField.type == "double" && this.myForm && this.myForm.controls[this.ReactiveField.actualname] ){
      if(this.ReactiveField.actualname.indexOf("TotalVolume") != -1 && 0 == +this.elementRef.nativeElement.value && this.ReactiveField.label.indexOf("Total Vol") != -1){
        this.myForm.controls[this.ReactiveField.actualname].setErrors({"errors":true});
      }
      else if((this.ReactiveField.actualname == "PumpDiameter" || this.ReactiveField.actualname == "PumpDisplacement" || this.ReactiveField.actualname == "DesignedRateDay") && this.elementRef.nativeElement.value == ""){
        this.myForm.controls[this.ReactiveField.actualname].setErrors({"errors":true});
      }
      else if ((( this.ReactiveField.actualname == "TestHours"|| this.ReactiveField.actualname == "ConversionFactor"|| this.ReactiveField.actualname == "JointsToFluid")
        && 0 == +this.elementRef.nativeElement.value)) {
        this.myForm.controls[this.ReactiveField.actualname].setErrors({"errors":true});
      }else{
        if (this.ReactiveField.actualname != "UnLoadTotalVolume" && this.ReactiveField.actualname != "LoadTotalVolume"
          && this.ReactiveField.actualname != "UnLoadGrossVolume" && this.ReactiveField.actualname != "LoadGrossVolume"
          && this.ReactiveField.actualname != "UnLoadTareVolume" && this.ReactiveField.actualname != "LoadTareVolume" && this.ReactiveField.actualname != "AnalysisTotal")
      this.myForm.controls[this.ReactiveField.actualname].setErrors(null);
      }
    }

    // if (this.myForm && this.myForm.controls && this.myForm.controls.Plate && this.myForm.controls.Plate.status == "INVALID" && this.myForm.controls.Plate.value == "") {
    //   this.myForm.controls.Plate.setValue(Number(this.elementRef.nativeElement.value).toFixed(5), { emitEvent: false });
    // }
  }


  @HostListener('input', ['$event'])
  onInputChange(event) {
    if(this.isDynamicWidth){
      let sizeOfBox = this.elementRef.nativeElement.value.length;
      let placeholderlength = this.elementRef.nativeElement.placeholder.length;
      if (sizeOfBox == 0)
        this.elementRef.nativeElement.style.width = (placeholderlength * 7) + "px";
      else if (sizeOfBox * 7 < 100)
        this.elementRef.nativeElement.style.width = (sizeOfBox * 7) + "px";

        this.Valuechanges();
      }
  }
  @HostListener('ngModelChange', ['$event']) onNgModelChange(event) {
    if(this.isDynamicWidth){
      let sizeOfBox = this.elementRef.nativeElement.value.length;
      let placeholderlength = this.elementRef.nativeElement.placeholder.length;
      if (sizeOfBox == 0)
        this.elementRef.nativeElement.style.width = (placeholderlength * 7) + "px";
      else if (sizeOfBox * 7 < 100)
        this.elementRef.nativeElement.style.width = (sizeOfBox * 7) + "px";

        this.Valuechanges();
      }
  }

  @HostListener('blur', ['$event'])
  onblur(event) {
    if (this.decimalPrecision && this.decimalPrecision.maxLength && Number(this.decimalPrecision.maxLength) > 0 && this.FieldType!="popovertable") {
      let precisionLength = Number(this.decimalPrecision.maxLength);
      let value = Number(this.elementRef.nativeElement.value);
      this.elementRef.nativeElement.value = value.toFixed(precisionLength);
    }
    if(this.isDynamicWidth){
     console.log("On blur Event : " + event);
    let sizeOfBox = this.elementRef.nativeElement.value.length;
    let placeholderlength = this.elementRef.nativeElement.placeholder.length;
    if (sizeOfBox == 0)
      this.elementRef.nativeElement.style.width = (placeholderlength * 7) + "px";
    else if (sizeOfBox * 7 < 100)
      this.elementRef.nativeElement.style.width = (sizeOfBox * 7) + "px";
      // this.Valuechanges();
      // document.getElementById(this.fDCService.blurEvent.target.id).blur();
     // this.elementRef.nativeElement.onblur = true
   }
  }
} 
