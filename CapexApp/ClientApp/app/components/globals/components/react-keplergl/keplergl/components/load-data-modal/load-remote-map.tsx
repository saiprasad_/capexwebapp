// Copyright (c) 2020 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// TODO: this will move onto visur.kepler.gl core
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {addDataToMap} from 'visur.kepler.gl/actions';
import {connect} from 'react-redux';
import { getConfig } from '../../config'
import Checkbox from '@material-ui/core/Checkbox';
import { Button, CircularProgress } from '@material-ui/core';
import { processCsvData, processGeojson,processRowObject } from 'visur.kepler.gl/processors';
import {load} from '@loaders.gl/core/src/lib/api/load';
import {ArrowLoader, ArrowWorkerLoader} from '@loaders.gl/arrow';
import {loadInBatches} from '@loaders.gl/core';
import {setLoaderOptions, fetchFile, parse, parseInBatches, makeIterator} from '@loaders.gl/core';

var arrow = require('apache-arrow')
//import {CORS_LINK} from '../../constants/default-settings';
//import {FormattedHTMLMessage, FormattedMessage} from 'react-intl';
// const PYTHON_BACKEND_URL = "http://localhost:5001"//process.env.PYTHON_BACKEND_URL; // eslint-disable-line
// const BACKEND_URL = "http://localhost:9010"//process.env.BACKEND_URL; // eslint-disable-line

const propTypes = {
  onLoadRemoteMap: PropTypes.func.isRequired
};

const StyledDescription = styled.div`
  fontSize: 14px;
  color: ${props => props.theme.labelColorLT};
  line-height: 18px;
  margin-bottom: 12px;
`;

const InputForm = styled.div`
  flex-grow: 1;
  padding: 32px;
  background-color: ${props => props.theme.panelBackgroundLT};
`;

const StyledInput = styled.input`
  width: 100%;
  padding: ${props => props.theme.inputPadding};
  color: ${props => (props.error ? 'red' : props.theme.titleColorLT)};
  height: ${props => props.theme.inputBoxHeight};
  border: 0;
  outline: 0;
  fontSize: ${props => props.theme.inputFontSize};

  :active,
  :focus,
  &.focus,
  &.active {
    outline: 0;
  }
`;
const StyledFromGroup = styled.div`
  margin-top: 30px;
  display: flex;
  flex-direction: row;
`;

const sampleTripData = {
  fields: [
    {name: 'tpep_pickup_datetime', format: 'YYYY-M-D H:m:s', type: 'timestamp'},
    {name: 'pickup_longitude', format: '', type: 'real'},
    {name: 'pickup_latitude', format: '', type: 'real'}
  ],
  rows: [
    ['2015-01-15 19:05:39 +00:00', -73.99389648, 40.75011063],
    ['2015-01-15 19:05:39 +00:00', -73.97642517, 40.73981094],
    ['2015-01-15 19:05:40 +00:00', -73.96870422, 40.75424576],
  ]
 };
 const data = [
  { checked: false, value: 'document 1' },
  { checked: true, value: 'document 2' },
  { checked: true, value: 'document 3' },
  { checked: false, value: 'document 4' },
  { checked: false, value: 'document 5' },
];
const Item = props => (

<div>
<Checkbox
  checked={props.checked}
  onChange={props.onCheckChange}
  color="default"
/>
{props.dataSetName}
</div>
)


 const sampleConfig = {
  visState: {
    filters: [
      {
        id: 'me',
        dataId: 'test_trip_data',
        name: 'tpep_pickup_datetime',
        type: 'timeRange',
        enlarged: true
      }
    ]
  }
}
export const StyledInputLabel = styled.div`
  fontSize: 11px;
  color: ${props => props.theme.textColorLT};
  letter-spacing: 0.2px;
  ul {
    padding-left: 12px;
  }
`;

export const StyledBtn = styled.button`
  background-color:  #318FD7;
  color: ${props => props.theme.primaryBtnActColor};
`;

export const StyledError = styled.div`
  color: red;
`;

export const StyledErrorDescription = styled.div`
  fontSize: 14px;
`;

const Error = ({error, url}) => (
  <StyledError>
    <StyledErrorDescription>{url}</StyledErrorDescription>
    <StyledErrorDescription>{error.message}</StyledErrorDescription>
  </StyledError>
);

class LoadRemoteMap extends Component<any,any> {
  state = {
    dataUrl: '',
    items:[],
    loadingSpinner : false
  };
  BACKEND_URL
  PYTHON_BACKEND_URL
  async componentDidMount(){
    let config:any=await getConfig()
    console.log(config)
    this.BACKEND_URL=config.env.ConstructionApiUrl;
    this.PYTHON_BACKEND_URL=config.env.ConstructionPythonApiUrl;
    this.getAllDataSets();
  }

  async getAllDataSets(){

    const requestOptions = {
      method: 'GET',
      headers: { 'Content-Type': 'application/json'
    },
  };
  fetch(this.BACKEND_URL+'/kepler/dataset',requestOptions).then(res => res.json()).then(data => {
          console.log(data)
          if(data&&data.contents&&data.contents.datasets&&data.contents.datasets.length>0){
            let filtereddatasets=data.contents.datasets.filter(d=>d.tags&&d.tags.includes('GPS'));
            let dataSets=[]
            filtereddatasets.forEach(element => {
              if(element.datasetConfig&&element.datasetConfig.fullPathList&&Array.isArray(element.datasetConfig.fullPathList)){
                let path='"'+element.datasetConfig.fullPathList.join('"."')+'"'
                let dataSetName=element.datasetConfig.fullPathList[element.datasetConfig.fullPathList.length-1]
                let obj={dataSetName:dataSetName,path:path,checked:false}
                dataSets.push(obj)
              }

            });
            this.setState({
              items: dataSets
            });
          }
          //this.props.onLoadRemoteMap({data},{dataUrl});
      });
  //   fetch('https://dremio.visur.io/apiv2/datasets/search?filter=GPS',requestOptions).then(res => res.json()).then(data => {
  //         console.log(data)
  //         if(data&&data['rows']&&data['rows']['0']){
  //           let dataSets=data['rows']['0']
  //           dataSets.forEach(element => {
  //             element['checked']=false;
  //           });
  //           this.setState({
  //             items: dataSets
  //           });
  //         }
  //     });
   }
  onMapUrlChange = e => {
    // TODO: validate url
    this.setState({
      dataUrl: e.target.value
    });
  };

   handleSubmit(e) {
    //e.preventDefault();
    this.setState({
      dataUrl: e.target.value
    });
    const {dataUrl} = this.state;
    console.log('submit');
    console.log(dataUrl);
    fetch('http://192.168.1.31:8888/api/', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify(dataUrl),
    })
      .then(res => res.json())
      .then(res => console.log(res));
  }
  _loadPointData(options,dataset) {
    var rowarray=[];
    Object.keys(options.data["rows"]).forEach(function (key) {
    options.data["rows"][key].forEach(obj=>{
        var myInnerArray=[];
        Object.keys(obj).forEach(key=>{
            myInnerArray.push(obj[key]);
        })
        rowarray.push(myInnerArray);
        })
  })
  ;


  const visurdata = {
   fields:options.data["schema"]["0"],
   rows:rowarray

   };
    this.props.dispatch(
      addDataToMap({
        datasets: {
          info: {
            label: dataset.dataSetName,
            id: dataset.path,
            dataSetProperties:{source:'dremio'}
          },
          data: visurdata
        },
        options: {
          centerMap: true,
          readOnly: false,
          keepExistingConfig: true
        },
       // config: sampleTripDataConfig
      })
    );

  }
  async _loadArrowData(data,dataset) {

    let dataTable = await data.arrayBuffer().then(buffer => {
     return arrow.Table.from(new Uint8Array(buffer));
   });
    var fieldArray = []

     dataTable.schema.fields.some(function (value, index, _arr) {
       fieldArray.push(
           {

         name: value.name.toLowerCase(),
         type: value.type.toString().toLowerCase()
       })

     });
     var rowCount = dataTable.count()
     let slice = [];
      for (let i=0; i <rowCount; i++) {
        slice.push(dataTable.get(i).toArray());
      }

     this.props.dispatch(
       addDataToMap({
         datasets: [
           {
             info: {
               label: dataset.dataSetName,
               id: dataset.path,
               dataSetProperties:{source:'dremio'}
             },
             //data: dataTable
            // data: processRowObject(values)
             data:{
               fields: fieldArray,
               rows: slice
             }
           },
           //  {
           //    info: {label: 'SF Zip Geo', id: 'sf-zip-geo'},
           //    data: processGeojson(sampleGeojson)
           //  }
         ],
         options: {
           centerMap: true,
           readOnly: false,
           keepExistingConfig: true
         },
         //config: sampleGeojsonConfig
       })
     );
   }

  onLoadRemoteMap = e => {
    const items = this.state.items.concat()
    let selectedDataSets=items.filter(d=>d.checked)
    if(selectedDataSets.length > 0){
      this.setState({loadingSpinner : true});
    }
   selectedDataSets.forEach(d=>{
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({Type:'ReadDataSet',Value:'select * from ' + d.path })
  };
  //this.PYTHON_BACKEND_URL
      fetch(this.PYTHON_BACKEND_URL+'/api/',requestOptions).then(data => {
           // console.log(data)
            //this._loadPointData({data},d);
            this._loadArrowData(data,d)
        });
   })



  };
  onCheckChange(idx) {
    return () => {
        const items = this.state.items.concat();
        items[idx].checked = !items[idx].checked;
        this.setState({items});
        console.log(items)
    }
}

  render() {
    const applybutton= {
      //padding: "3px 10px 3px 10px",
      margin: "1px 0 0 740px",
      cursor:'pointer',
      'fontSize':'12px'
    }
    const spinnerCss = {
      margin: "1px 0 0 740px",
      cursor:'pointer'
    }
    const fontSize={
      'fontSize':'14px'
    }
    const {loadingSpinner} = this.state;
    return (
      <div>
        <br></br>
        <span style={fontSize}>Select file you would like to load as a layer</span>
        <br></br><br></br>
        <div style={{height:"300px",overflow:"auto",maxHeight:"300px"}}>
        <InputForm>
          { this.state.items.map((props, idx) => (
                    <Item {...props} key={idx} onCheckChange={this.onCheckChange(idx)} />
                )) }            
          {this.props.error && <Error error={this.props.error} url={this.props.option.dataUrl} />}
        </InputForm>
        </div>
        { loadingSpinner && <CircularProgress style={spinnerCss}></CircularProgress>}
        {!loadingSpinner &&<Button variant="outlined"  style={applybutton} type="submit" onClick={this.onLoadRemoteMap}>
          APPLY
        </Button>}
      </div>
    );
  }
}

const mapStateToProps = state => state;
const dispatchToProps = dispatch => ({dispatch});

export default connect(mapStateToProps, dispatchToProps)(LoadRemoteMap);
