// Copyright (c) 2020 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// TODO: this will move onto visur.kepler.gl core
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {addDataToMap} from 'visur.kepler.gl/actions';
import {connect} from 'react-redux';
import { getConfig } from '../../config'
import Checkbox from '@material-ui/core/Checkbox';
import { getUrlParams } from '../../helper-functions'
 import { processCsvData, processGeojson } from 'visur.kepler.gl/processors';
 import {loadFiles, toggleModal} from 'visur.kepler.gl/actions';

//import Tree from 'react-folder-tree-private/src/Tree/Tree';
import Tree from '../../../../Tree/Tree'
import {request, text as requestText, json as requestJson} from 'd3-request';
import { Button, CircularProgress, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';

//import {CORS_LINK} from '../../constants/default-settings';
//import {FormattedHTMLMessage, FormattedMessage} from 'react-intl';
// const PYTHON_BACKEND_URL = "http://localhost:5001"//process.env.PYTHON_BACKEND_URL; // eslint-disable-line
// const BACKEND_URL = "http://localhost:9010"//process.env.BACKEND_URL; // eslint-disable-line

export const StyledBtn = styled.button`
  background-color: #318FD7;
  font-color:#FFFFFF;
`;
// const data =[
//   {
//     "id": "kml/folder1",
//     "type": "folder",
//     "name": "folder1",
//     "parent": "kml",
//     "files": [
//       {
//         "id": "kml/folder1/folder2",
//         "type": "folder",
//         "name": "folder2",
//         "parent": "kml/folder1",
//         "files": []
//       }
//     ]
//   },
//   {
//     "id": "kml/pipe207l.kml",
//     "type": "file",
//     "name": "pipe207l.kml",
//     "parent": "kml",
//     "files": []
//   }
// ]

class LoadRemoteFiles extends Component<any,any> {
   json:any = getUrlParams()
  state = {
    dataUrl: '',
    data:[],
    loadingSpinner : false,
    isOpen : false,
    payloadData:[]
  };
  data=[]
  BACKEND_URL 
  PYTHON_BACKEND_URL
  async componentDidMount(){
    let config:any=await getConfig()
    console.log(config)
    this.BACKEND_URL=config.env.ConstructionApiUrl;
    this.PYTHON_BACKEND_URL=config.env.ConstructionPythonApiUrl;
    this.getAdlFiles();
  }

  async getAdlFiles(){

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json'},
      body: JSON.stringify({ "TenantID": this.json.TenantId,
      'TenantName':this.json.TenantName,
      "containerName":this.json.TenantName,
      "Type": "ReadFilesPath",
      "TypeOfData":"Private",
      "Path":"constructionapp"
    })
    
  };
  fetch(this.PYTHON_BACKEND_URL+'/api/',requestOptions).then(res => res.json()).then(data => {
          console.log(data)
          let treedata=this.getTree(data.responce)
          this.setState({
            data: treedata
          });  
        });
   }
  onLoadRemoteMap(data){
    let selectedFiles=[]
    if(this.state.data[0])
    this.findSelectedFiles(data[0]['files'],selectedFiles)
    if(selectedFiles.length > 0){
      this.setState({loadingSpinner : true});
    }
    selectedFiles.forEach(element=>{
      const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json'},
      body: JSON.stringify({ "TenantID": this.json.TenantId,
      'TenantName':this.json.TenantName,
      "containerName":this.json.TenantName,
      "Type": "ReadFile",
      "TypeOfData":"Private",
      "Path":element.id,
      "ParentPath":element.parent,
      "fileName":element.name
    })
    };
    // fetch(this.PYTHON_BACKEND_URL+'/api/',requestOptions).then(res => res.json()).then((data:any) => {
    //       if(data.responce)
    //         console.log(data)
    //         this.loadRemoteMap(data.responce)   
    //       });

    fetch(this.PYTHON_BACKEND_URL+'/api/',requestOptions).then(res => res.text()).then(data=> {
        const filename = element.name;
        let filesProperty={id:element.id,path:element.id,source:'adl',typeOfData:'Private',fileName:filename,containerName:this.json.TenantName}
        this.props.dispatch(loadFiles([new File([data], filename)],filesProperty))
      },
      error => {
        const {target = {}} = error;
        const {status, responseText} = target;
      });
      //this.loadRemoteMap("https://pkgstore.datahub.io/core/geo-countries/countries/archive/23f420f929e0e09c39d916b8aaa166fb/countries.geojson")
    })
  }
  loadRemoteMap(dataUrl) {
    //return dispatch => {
      // breakdown url into url+query params
      this.loadRemoteRawData(dataUrl).then(
        // In this part we turn the response into a FileBlob
        // so we can use it to call loadFiles
        ([file, url]) => {
          const filename = "countries.geojson";
          this.props.dispatch(loadFiles([new File([file], filename)]))
        },
        error => {
          const {target = {}} = error;
          const {status, responseText} = target;
        }
      );
    //};
  }
  
  loadRemoteRawData(url) {
    if (!url) {
      // TODO: we should return reject with an appropriate error
      return Promise.resolve(null);
    }
  
    return new Promise((resolve, reject) => {
      request(url, (error, result) => {
        if (error) {
          reject(error);
          return;
        }
        resolve([result.response, url]);
      });
    });
  }
  handleCreateFolder = (message) => {
    console.log(message);
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json'},     
      body: JSON.stringify({"TenantID": this.json.TenantId,'TenantName':this.json.TenantName,"containerName":this.json.TenantName,"Type": message.node.type,"TypeOfData":"Private","Path":"constructionapp","id": message.node.id + "/" + message.node.name, "ParentPath": message.node.id, "name": message.node.name })
    };
    fetch(this.PYTHON_BACKEND_URL+'/api/',requestOptions).then(res => res.text()).then(data => {    
        console.log(data)               
      });
    
  }

  handleCreateFile = (state) => {
    console.log(state);
    
  }
  handleDeleteFolder = (message) => {
    console.log(message)
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json'},     
      body: JSON.stringify({"TenantID": this.json.TenantId,'TenantName':this.json.TenantName,"containerName":this.json.TenantName,"Type": message.type,"TypeOfData":"Private","Path":"constructionapp","id": message.id, "name": message.name})
    };
    fetch(this.PYTHON_BACKEND_URL+'/api/',requestOptions).then(res => res.text()).then(data => {    
        console.log(data)
      });
  }
  handleFileDelete = (message) => {
    console.log(message);
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json'},     
      body: JSON.stringify({"TenantID": this.json.TenantId,'TenantName':this.json.TenantName,"containerName":this.json.TenantName,"Type": message.type,"TypeOfData":"Private","Path": message.id,"id": message.id, "name": message.name })
    };
    fetch(this.PYTHON_BACKEND_URL+'/api/',requestOptions).then(res => res.text()).then(data => {    
        console.log(data)
      });
  }
  handleFileRename = (message) =>{
    console.log(message);
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json'},     
      body: JSON.stringify({"TenantID": this.json.TenantId,'TenantName':this.json.TenantName,"containerName":this.json.TenantName,"Type": 'Rename',"TypeOfData":"Private","Path":message.id,"ParentPath": message.parent, "name": message.name})
    };
    fetch(this.PYTHON_BACKEND_URL+'/api/',requestOptions).then(res => res.text()).then(data => {    
        console.log(data)
      });
  }
  handleRenameFolder = (message) =>{
    console.log(message);
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json'},     
      body: JSON.stringify({"TenantID": this.json.TenantId,'TenantName':this.json.TenantName,"containerName":this.json.TenantName,"Type": 'Rename',"TypeOfData":"Private","Path":message.id,"ParentPath":  message.parent, "name": message.name})
    };
    fetch(this.PYTHON_BACKEND_URL+'/api/',requestOptions).then(res => res.text()).then(data => {    
        console.log(data)               
      });
  }
 handleClick = (node) => {
  console.log(node);
  // const requestOptions = {
  //   method: 'POST',
  //   headers: { 'Content-Type': 'application/json'},
  //   body: JSON.stringify({ "TenantID": "f2dedf6e-393e-42bc-9bb3-e835a1063b30",
  //   "UserID": "21059ad1-190f-427e-3045-1bd3411426c5",
  //   "Type": "GetFile"})
  // };
  // fetch(this.PYTHON_BACKEND_URL+'/api/',requestOptions).then(res => res.text()).then(data => {
  //       if(data.length>0)
  //         console.log(data)
  //         // let filepath=node.id.split('/')
  //         // fs.writeFile(filepath[filepath.length-1], data, (err) => { 
  //         //       // In case of a error throw err. 
  //         //       if (err) throw err; 
  //         //   }) 
             
  //       });
};
findSelectedFiles(files,selectedFiles){
  files.forEach(element => {
    if(element.checked)
    selectedFiles.push(element)
    else if(element.type=='folder'&&element.files.length>0)
    this.findSelectedFiles(element.files,selectedFiles)
  });
}
getTree(data){
  let rootNode={id:data[0].split('/')[0],files:[]}
  console.log(data.sort())
  let treeData=[]
  data.forEach(d=>{
    let elements = d.split('/');
    let lastele=elements[elements.length-1]
    let type=lastele.split('.').length==1?'folder':'file';
    console.log(type)
    let node={
      id:d,
      type: type,
      name: lastele,
      checked:false,
      parent:d.slice(0, d.lastIndexOf('/'))
    }
    if(type=='folder'){
      node['files']=[]
    }
    console.log(node)
    treeData.push(node)
    
  })
  console.log(treeData)
  this.getChildren(treeData,rootNode)
  console.log(rootNode)
  return rootNode.files

}
// async _loadData(url) {

//   //const data = await load(KML_URL, KMLLoader);
//   const data = await load(url, KMLLoader, {gis: {format: 'geojson'}});
//   console.log("data",data)
//   console.log("dataType",data.type)
//   // load geojson
//   this.props.dispatch(
//   addDataToMap({
//   datasets: [
//   {
//   info: {label: 'Pipeline KML', id: 'pipe2071'},
//   data: processGeojson(data)
//   },
//   ],
//   options: {
//   keepExistingConfig: true
//   },
//   //config: sampleGeojsonConfig
//   })
//   );
//   }
getChildren(treeData,node){
  let children =treeData.filter(d=>d.parent==node.id)
  children.forEach(d=>{
    this.getChildren(treeData,d)
  })
  node.files=children
}
 handleUpdate = (state) => {
  this.setState({
    data: state
  }); 
  localStorage.setItem(
    "tree",
    JSON.stringify(state, function (key, value) {
      if (key === "parentNode" || key === "id") {
        return null;
      }
      return value;
    })
  );
};
  render() {
    const applybutton= {
      //padding: "3px 10px 3px 10px",
      margin: "1px 0 0 740px",
      cursor:'pointer',
      'fontSize':'12px'
    }
    const spinnerCss = {
      margin: "1px 0 0 740px",
      cursor:'pointer'
    }
    const fontSize={
      'fontSize':'14px'
    }
    const {loadingSpinner} = this.state;
    return (
      <div>
        <br></br>
        <span style={fontSize}>Select file you would like to load as a layer</span>
        <br></br><br></br>
        <div style={{height:"300px",overflow:"auto",maxHeight:"300px"}}>
          <Tree data={this.state.data} onUpdate={this.handleUpdate} onNodeClick={this.handleClick} folderCreation={this.handleCreateFolder} fileCreating={this.handleCreateFile} deleteFolder={this.handleDeleteFolder} editFolderName={this.handleRenameFolder} deleteFile={this.handleFileDelete} fileEdit={this.handleFileRename}/>
        </div>
        { loadingSpinner && <CircularProgress style={spinnerCss}></CircularProgress>}
        {!loadingSpinner && <Button variant="outlined" style={applybutton} type="submit" onClick={()=>this.onLoadRemoteMap(this.state.data)}>
              APPLY
        </Button>}
      </div>
      
    );
  }
}

const mapStateToProps = state => state;
const dispatchToProps = dispatch => ({dispatch});

export default connect(mapStateToProps, dispatchToProps)(LoadRemoteFiles);
