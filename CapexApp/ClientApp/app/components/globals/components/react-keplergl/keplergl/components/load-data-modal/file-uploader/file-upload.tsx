// Copyright (c) 2020 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import React, {Component, createRef} from 'react';
import styled from 'styled-components';
import {injectIntl} from 'react-intl';
import UploadButton from './upload-button';
import {DragNDrop, FileType} from 'visur.kepler.gl/dist/components/common/icons';
import FileUploadProgress from 'visur.kepler.gl/dist/components/common/file-uploader/file-upload-progress';
import FileDrop from './file-drop';

import {isChrome} from 'visur.kepler.gl/dist/utils/utils';
import {GUIDES_FILE_FORMAT_DOC} from 'visur.kepler.gl/dist/constants/user-guides';
import ReactMarkdown from 'react-markdown';
// Breakpoints
import {media} from 'visur.kepler.gl/dist/styles/media-breakpoints';
import {FormattedMessage} from 'visur.kepler.gl/dist/localization';
import Checkbox from '@material-ui/core/Checkbox';
import {AiOutlineFolder} from "react-icons/ai";
import Tree from 'ClientApp/app/components/globals/components/Tree/Tree';
import { getConfig } from '../../../config';
import { getUrlParams,getTreeData } from '../../../helper-functions';
import { FOLDER } from 'ClientApp/app/components/globals/components/Tree/state/constants';
/** @typedef {import('./file-upload').FileUploadProps} FileUploadProps */

const fileIconColor = '#D3D8E0';

const LinkRenderer = props => {
  return (
    <a href={props.href} target="_blank" rel="noopener noreferrer">
      {props.children}
    </a>
  );
};
const StyledUploadMessage = styled.div`
  color: ${props => props.theme.textColorLT};
  font-size: 14px;
  margin-bottom: 12px;

  ${media.portable`
    font-size: 12px;
  `}
`;

export const WarningMsg = styled.span`
  margin-top: 10px;
  color: ${props => props.theme.errorColor};
  font-weight: 500;
`;

const StyledFileDrop = styled.div`
  background-color: white;
  border-radius: 4px;
  border-style: ${props => (props.dragOver ? 'solid' : 'dashed')};
  border-width: 1px;
  border-color: ${props => (props.dragOver ? props.theme.textColorLT : props.theme.subtextColorLT)};
  text-align: center;
  width: 100%;

  .file-upload-or {
    color: ${props => props.theme.linkBtnColor};
    padding-right: 4px;
  }

  .file-type-row {
    opacity: 0.5;
  }
  ${media.portable`
    padding: 16px 4px 0;
  `};
`;
const StyledFilePath = styled.div`
  border-radius: 4px;
  border-style: ${props => (props.dragOver ? 'solid' : 'dashed')};
  border-width: 1px;
  font-size:16px;
  border-color: ${props => (props.dragOver ? props.theme.textColorLT : props.theme.subtextColorLT)};
  text-align: left;
  width: 530px;
  margin-top: 6px;
  margin-left: 5px;
  padding: 2px 5px 0;
  height: 35px;
  contentEditable='true';
`;

const MsgWrapper = styled.div`
  color: ${props => props.theme.modalTitleColor};
  font-size: 20px;
  height: 36px;
`;

const StyledDragNDropIcon = styled.div`
  color: ${fileIconColor};
  margin-bottom: 48px;

  ${media.portable`
    margin-bottom: 16px;
  `};
  ${media.palm`
    margin-bottom: 8px;
  `};
`;

const StyledFileTypeFow = styled.div`
  margin-bottom: 24px;
  ${media.portable`
    margin-bottom: 16px;
  `};
  ${media.palm`
    margin-bottom: 8px;
  `};
`;

const StyledFileUpload = styled.div`
  .file-drop {
    position: relative;
  }
`;

const StyledsaveFileDiv = styled.div`
  display: flex;
  margin-top: 10px;
`;

const Styledspan = styled.span`
margin-top: 12px;
`;

const StyledVisurAtorage = styled.span`
margin-top: 12px;
margin-left: 60px;
`;

const StyledMessage = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 32px;

  .loading-action {
    margin-right: 10px;
  }
  .loading-spinner {
    margin-left: 10px;
  }
`;

const StyledDragFileWrapper = styled.div`
  margin-bottom: 32px;
  ${media.portable`
    margin-bottom: 24px;
  `};
  ${media.portable`
    margin-bottom: 16px;
  `}
`;

const StyledDisclaimer = styled(StyledMessage)`
  margin: 0 auto;
`;

function FileUploadFactory() {
  /** @augments {Component<FileUploadProps>} */
  class FileUpload extends Component<any,any> {
    state = {
      dragOver: false,
      fileLoading: false,
      files: [],
      errorFiles: [],
      checked:true,
      path:'constructionapp/Maps/Projects',
      treeData:[],
      showTree:false
    };
  path
  BACKEND_URL 
  PYTHON_BACKEND_URL
    json: any = getUrlParams()
    async componentDidMount() {
      let config: any = await getConfig()
      console.log(config)
      this.BACKEND_URL=config.env.ConstructionApiUrl;
      this.PYTHON_BACKEND_URL=config.env.ConstructionPythonApiUrl;
      this.getAdlFiles();
    }
    async getAdlFiles() {

      const requestOptions = {
        method: 'POST',
      headers: { 'Content-Type': 'application/json'},
      body: JSON.stringify({ "TenantID": this.json.TenantId,
      'TenantName':this.json.TenantName,
      "containerName":this.json.TenantName,
      "Type": "ReadFilesPath",
      "TypeOfData":"Private",
      "Path":"constructionapp"
        })
      };
      fetch(this.PYTHON_BACKEND_URL + '/api/', requestOptions).then(res => res.json()).then(data => {
        console.log(data)
        let tree = getTreeData(data.responce)
        this.setState({
          treeData: tree
        });
      });
    }
    
    static getDerivedStateFromProps(props, state) {
      if (state.fileLoading && props.fileLoading === false && state.files.length) {
        return {
          files: [],
          fileLoading: props.fileLoading
        };
      }
      return {
        fileLoading: props.fileLoading
      };
    }

    frame = createRef();

    _isValidFileType = filename => {
      const {fileExtensions = []} = {fileExtensions:[]};
      const fileExt = fileExtensions.find(ext => filename.endsWith(ext));

      return Boolean(fileExt);
    };

    /** @param {FileList} fileList */
    _handleFileInput = (fileList, event) => {
      if (event) {
        event.stopPropagation();
      }

      const files = [...fileList].filter(Boolean);

      //const {disableExtensionFilter = false} = this.props;

      // TODO - move this code out of the component
      const filesToLoad = [];
      const errorFiles = [];
      for (const file of files) {
        //if (disableExtensionFilter || this._isValidFileType(file.name)) {
          filesToLoad.push(file);
        // } else {
        //   errorFiles.push(file.name);
        // }
        if(this.state.checked){
          let  fileReader  = new FileReader();
          const handleFileRead = () => {
            const content = fileReader.result
            this.handleFileUpload({name:file.name,payload:content});
           };
          fileReader.onloadend = handleFileRead
          fileReader.readAsText(file);
        }
        
      }
     

      const nextState = {files: filesToLoad, errorFiles, dragOver: false};
      const filePath=this.state.path+'/'+files[0].name
      this.setState(nextState, () =>
        nextState.files.length ? this.props.onFileUpload(nextState.files,{id:filePath,path:filePath,source:'adl',typeOfData:'Private',fileName:files[0].name,containerName:this.json.TenantName}) : null
      );
    };

    _toggleDragState = newState => {
      this.setState({dragOver: newState});
    };
    setSelection = (checked) => {
      this.setState({checked:!checked})
    }
    handleFileUpload = (message) => {
      console.log(message);
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json'},     
        body: JSON.stringify({"TenantID": this.json.TenantId,
         'TenantName':this.json.TenantName,
         "containerName":this.json.TenantName,
         "Type": FOLDER.UPLOAD,
         "TypeOfData":"Private",
         "Path": this.state.path,
         "fileName": message.name,
         "content":message.payload
         })
      };
      fetch(this.PYTHON_BACKEND_URL+'/api/',requestOptions).then(res => res.text()).then(data => {    
          console.log(data)               
        });
    }
    setFilePath = (node) => {
      console.log(node)
      this.setState({path:  node.node.id})
      this.setState({showTree : false});
    }
    onClick=()=>{
      console.log('onclick');
      this.setState({showTree : !this.state.showTree});

    }
    render() {
      const {dragOver, files, errorFiles,showTree} = this.state;
      const {fileLoading, fileLoadingProgress, theme, intl} = this.props;
      const {fileExtensions = [], fileFormatNames = []} = this.props;
      return (
        <div>
          <div style={{height:showTree? "250px":"",overflow:"auto",maxHeight:"250px"}}>
          {showTree &&<Tree className="file-uploader" data={this.state.treeData} onNodeClick={this.setFilePath} handleFilePath={this.setFilePath}  />}
          </div>
        {!showTree && <StyledFileUpload className="file-uploader" ref={this.frame}>
            {FileDrop ? (
              <FileDrop
                frame={this.frame.current || document}
                onDragOver={() => this._toggleDragState(true)}
                onDragLeave={() => this._toggleDragState(false)}
                onDrop={this._handleFileInput}
                className="file-uploader__file-drop"
              >
                <StyledUploadMessage className="file-upload__message">
                  <ReactMarkdown
                    source={`${intl.formatMessage(
                      {
                        id: 'fileUploader.configUploadMessage'
                      },
                      {
                        fileFormatNames: fileFormatNames.map(format => `**${format}**`).join(', ')
                      }
                    )}(${GUIDES_FILE_FORMAT_DOC}).`}
                    renderers={{link: LinkRenderer}}
                  />
                </StyledUploadMessage>
                <StyledFileDrop dragOver={dragOver}>
                  <StyledFileTypeFow className="file-type-row">
                    {fileExtensions.map(ext => (
                      <FileType key={ext} ext={ext} height="50px" fontSize="9px" />
                    ))}
                  </StyledFileTypeFow>
                  {fileLoading ? (
                    <FileUploadProgress fileLoadingProgress={fileLoadingProgress} theme={theme} />
                  ) : (
                    <>
                      <div
                        style={{opacity: dragOver ? 0.5 : 1}}
                        className="file-upload-display-message"
                      >
                        <StyledDragNDropIcon>
                          <DragNDrop height="44px" />
                        </StyledDragNDropIcon>

                        {errorFiles.length ? (
                          <WarningMsg>
                            <FormattedMessage
                              id={'fileUploader.fileNotSupported'}
                              values={{errorFiles: errorFiles.join(', ')}}
                            />
                          </WarningMsg>
                        ) : null}
                      </div>
                      {!files.length ? (
                        <StyledDragFileWrapper>
                          <MsgWrapper>
                            <FormattedMessage id={'fileUploader.message'} />
                          </MsgWrapper>
                          <span className="file-upload-or">
                            <FormattedMessage id={'fileUploader.or'} />
                          </span>
                          <UploadButton onUpload={this._handleFileInput}>
                            <FormattedMessage id={'fileUploader.browseFiles'} />
                          </UploadButton>
                        </StyledDragFileWrapper>
                      ) : null}

                      <StyledDisclaimer>
                        <FormattedMessage id={'fileUploader.disclaimer'} />
                      </StyledDisclaimer>
                    </>
                  )}
                </StyledFileDrop>
              </FileDrop>
            ) : null}

            <WarningMsg>
              {isChrome() ? <FormattedMessage id={'fileUploader.chromeMessage'} /> : ''}
            </WarningMsg>
                      </StyledFileUpload> }
          <StyledsaveFileDiv>
            <Checkbox
            checked={this.state.checked}
            onChange={() => this.setSelection(this.state.checked)}
            color="default"
            />
            <Styledspan>
            Save for future use
            </Styledspan>
            <StyledVisurAtorage>
              Visur Storage
            </StyledVisurAtorage>
            <StyledFilePath onClick={this.onClick}>
            <AiOutlineFolder /> <span className="filePath">{  this.state.path}</span>
            </StyledFilePath>
          </StyledsaveFileDiv>
        </div>
      );
    }
  }

  return injectIntl(FileUpload);
}

export default FileUploadFactory;
export const FileUpload = FileUploadFactory();
