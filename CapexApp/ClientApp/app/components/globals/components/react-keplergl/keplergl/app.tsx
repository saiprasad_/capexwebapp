// Copyright (c) 2020 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import React, { Component } from 'react';
import AutoSizer from 'react-virtualized/dist/commonjs/AutoSizer';
import styled, { ThemeProvider } from 'styled-components';
import window from 'global/window';
import { connect } from 'react-redux';

import { theme } from 'visur.kepler.gl/styles';
import Banner from './components/banner';
// import Announcement, {FormLink} from './components/announcement';
import { replaceLoadDataModal } from './factories/load-data-modal';
// import {replaceMapControl} from './factories/map-control';
import { replacePanelHeader } from './factories/panel-header';
// import {replaceSidePanel} from './factories/config-panel';
import { AUTH_TOKENS } from './constants/default-settings';
import { messages } from './constants/localization';
import { KeplerGlSchema } from 'visur.kepler.gl/schemas';
import { receiveMapConfig } from 'visur.kepler.gl/actions';
import { getConfig } from './config'
import { getUrlParams } from './helper-functions'
import {
  AddDataButtonFactory,
} from 'visur.kepler.gl/components';
import {
  loadRemoteMap,
  loadSampleConfigurations,
  loadMapFromStateData,
  onExportFileSuccess,
  onLoadCloudMapSuccess
} from './actions';

import { loadCloudMap } from 'visur.kepler.gl/actions';
import LoadingOverlay from 'react-loading-overlay';

//import {CLOUD_PROVIDERS} from './cloud-providers';
const CustomAddDataButtonFactory = () => {
  const CustomAddDataButton = () => <div />;
  return CustomAddDataButton;
};
const KeplerGl = require('visur.kepler.gl/components').injectComponents([
  replaceLoadDataModal(),
  //  //[AddDataButtonFactory, CustomAddDataButtonFactory],
  //   replaceMapControl(),
  //   replaceSidePanel(),
  replacePanelHeader()
]);

// Sample data
/* eslint-disable no-unused-vars */
import sampleTripData, { testCsvData, sampleTripDataConfig } from './data/sample-trip-data';
import sampleGeojson from './data/sample-small-geojson';
import sampleGeojsonPoints from './data/sample-geojson-points';
import sampleGeojsonConfig from './data/sample-geojson-config';
import sampleH3Data, { config as h3MapConfig } from './data/sample-hex-id-csv';
import sampleS2Data, { config as s2MapConfig, dataId as s2DataId } from './data/sample-s2-data';
import sampleAnimateTrip from './data/sample-animate-trip-data';
import sampleIconCsv, { config as savedMapConfig } from './data/sample-icon-csv';
import { addDataToMap, addNotification } from 'visur.kepler.gl/actions';
import { processCsvData, processGeojson, processRowObject } from 'visur.kepler.gl/processors';
import {loadFiles, toggleModal} from 'visur.kepler.gl/actions';
import {load} from '@loaders.gl/core/src/lib/api/load';
import {ArrowLoader, ArrowWorkerLoader} from '@loaders.gl/arrow';
import {loadInBatches} from '@loaders.gl/core';
import {setLoaderOptions, fetchFile, parse, parseInBatches, makeIterator} from '@loaders.gl/core';
import { Service } from './Service';
import { ThemeService } from '../../../services/theme.service';
var arrow = require('apache-arrow')
/* eslint-enable no-unused-vars */

const BannerHeight = 48;
// const BannerKey = `banner-${FormLink}`;
const keplerGlGetState = state => state.demo.keplerGl;
//const VISUR_MAPBOX_TOKEN = process.env.VISUR_MAPBOX_TOKEN; // eslint-disable-line
// const BACKEND_URL = 'http://localhost:9010'//process.env.BACKEND_URL; // eslint-disable-line
// const PYTHON_BACKEND_URL = 'http://localhost:5001'//process.env.PYTHON_BACKEND_URL; // eslint-disable-line

const GlobalStyle = styled.div`
  font-family: ff-clan-web-pro, 'Helvetica Neue', Helvetica, sans-serif;
  font-weight: 400;
  fontSize: 0.875em;
  line-height: 1.71429;

  *,
  *:before,
  *:after {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
  }

  ul {
    margin: 0;
    padding: 0;
  }

  li {
    margin: 0;
  }

  a {
    text-decoration: none;
    color: ${props => props.theme.labelColor};
  }
  .kepler-gl .side-panel__content > div {
    display: flex;
    height: 100%;
    flex-direction: column;
  }
`;
declare var require: any;



class App extends Component<any, any, any> {
  //isActive = true
  BACKEND_URL
  PYTHON_BACKEND_URL
  subscription
  json
  state = {
    showBanner: false,
    width: window.innerWidth,
    height: window.innerHeight,
    isActive : false, 
    dataSetStatus:[]
  };
  root

  async componentDidMount() {

    let config: any = await getConfig()
    console.log(config)
    this.BACKEND_URL = config.env.ConstructionApiUrl;
    this.PYTHON_BACKEND_URL = config.env.ConstructionPythonApiUrl;
    this.json = getUrlParams()
    this.subscription = Service.getMessage().subscribe(value =>{
      if(value["operation"] == "start"){
        this.setState({isActive : false})
      }else if(value["operation"] == "stop"){
        this.setState({isActive : false})
      }      
    })
    // if (!json['TenantName'] || !json['TenantId'] || !json['EntityId']) {
    //   alert("Invalid Params");
    //   return;
    // }

    // if we pass an id as part of the url
    // we ry to fetch along map configurations
    // const {params: {id, provider} = {}, location: {query = {}} = {}} = this.props;

    // const cloudProvider = CLOUD_PROVIDERS.find(c => c.name === provider);
    // if (cloudProvider) {
    //   this.props.dispatch(
    //     loadCloudMap({
    //       loadParams: query,
    //       provider: cloudProvider,
    //       onSuccess: onLoadCloudMapSuccess
    //     })
    //   );
    //   return;
    // }

    // Load sample using its id
    // if (id) {
    //   this.props.dispatch(loadSampleConfigurations(id));
    // }

    // // Load map using a custom
    // if (query.mapUrl) {
    //   // TODO?: validate map url
    //   this.props.dispatch(loadRemoteMap({dataUrl: query.mapUrl}));
    // }
    // delay zs to show the banner
    // if (!window.localStorage.getItem(BannerKey)) {
    //   window.setTimeout(this._showBanner, 3000);
    // }
    // load sample data
    // this._loadSampleData();

    // Notifications
    // this._loadMockNotifications();
    //const query = new URLSearchParams(this.props.location.search);



    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(this.json)
    };
    fetch(this.BACKEND_URL + '/kepler/readMapConfig/', requestOptions).then(res => res.json()).then(data => {
      console.log(data)
      //this.isActive = false
      this.setState({isActive : false})
      if (data[0] && data[0].Config) {
        data = JSON.parse(data[0].Config)
        this.loadDataSets(data)

      }
    });
    //const parsedConfig = KeplerGlSchema.parseSavedConfig(config);

  }
  static getDerivedStateFromProps(props, state) {
    if (state.dataSetStatus.length===0&&props.keplerGl&&props.keplerGl.map) {
      return {isActive:props.keplerGl.map.uiState.loadFiles.fileLoading}
    }
    return state
  }
  loadDataSets(config) {
    const parsedConfig = KeplerGlSchema.parseSavedConfig(config);
    this.props.dispatch(receiveMapConfig(parsedConfig));
    config.dataSets.forEach(d => {
      //this.isActive = true
      this.state.dataSetStatus.push(d.id)
      this.setState({isActive : true})
      if(d.dataSetProperties.source=='dremio')
      this.loaddremioDataSets(d);
      else{
        this.loadAdlDataSets(d);
      }
    })
  }
  _showBanner = () => {
    this.setState({ showBanner: true });
  };

  _hideBanner = () => {
    this.setState({ showBanner: false });
  };

  // _disableBanner = () => {
  //   this._hideBanner();
  //   window.localStorage.setItem(BannerKey, 'true');
  // };

  _loadMockNotifications = () => {
    const notifications = [
      [{ message: 'Welcome to Kepler.gl' }, 3000],
      [{ message: 'Something is wrong', type: 'error' }, 1000],
      [{ message: 'I am getting better', type: 'warning' }, 1000],
      [{ message: 'Everything is fine', type: 'success' }, 1000]
    ];

    this._addNotifications(notifications);
  };

  private loadAdlDataSets(element) {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        "TenantID": this.json.TenantId,
        'TenantName': this.json.TenantName,
        "containerName": element.dataSetProperties.containerName,
        "Type": "ReadFile",
        "TypeOfData": element.dataSetProperties.typeOfData,
        "Path": element.dataSetProperties.id,
        "ParentPath": element.dataSetProperties.id.slice(0, element.dataSetProperties.id.lastIndexOf('/')),
        "fileName": element.dataSetProperties.fileName
      })
    };
    // fetch(this.PYTHON_BACKEND_URL+'/api/',requestOptions).then(res => res.json()).then((data:any) => {
    //       if(data.responce)
    //         console.log(data)
    //         this.loadRemoteMap(data.responce)
    //       });
    fetch(this.PYTHON_BACKEND_URL + '/api/', requestOptions).then(res => res.text()).then(data => {
      //this.isActive = false;
      this.removeElementFromArray(this.state.dataSetStatus,element.id)
      this.checkLoaderStatus(this.state.dataSetStatus)
      const filename = element.dataSetProperties.fileName;
      this.props.dispatch(loadFiles([new File([data], filename)], element.dataSetProperties));
    },
      error => {
        const { target = {} } = error;
        const { status, responseText } = target;
      });
  }

  private loaddremioDataSets(d: any) {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ Type: 'ReadDataSet', Value: 'select * from ' + d.id })
    };
    let pathArray = d.id.split('.');
    let dataSetName = pathArray[pathArray.length - 1];
    let datasetInfo = { dataSetName: dataSetName, path: d.id ,dataSetProperties:d.dataSetProperties};
          //this.PYTHON_BACKEND_URL
    fetch(this.PYTHON_BACKEND_URL + '/api/', requestOptions).then(data => {
      console.log(data);

     // this._loadPointData({ data }, datasetInfo);
     this._loadArrowData(data,datasetInfo)
     //this.isActive = false;
     this.removeElementFromArray(this.state.dataSetStatus,d.id)
     this.checkLoaderStatus(this.state.dataSetStatus)
    });
  }

    removeElementFromArray(array,element){
      let index=array.indexOf(element)
      if (index > -1) {
        array.splice(index, 1);
      }
    }
    checkLoaderStatus(array){
      if(array.length==0)
      this.setState({isActive : false})
    }
  _addNotifications(notifications) {
    if (notifications && notifications.length) {
      const [notification, timeout] = notifications[0];

      window.setTimeout(() => {
        this.props.dispatch(addNotification(notification));
        this._addNotifications(notifications.slice(1));
      }, timeout);
    }
  }


  async _loadArrowData(data,dataset) {

    let dataTable = await data.arrayBuffer().then(buffer => {
     return arrow.Table.from(new Uint8Array(buffer));
   });
    var fieldArray = []

     dataTable.schema.fields.some(function (value, index, _arr) {
       fieldArray.push(
           {

         name: value.name.toLowerCase(),
         type: value.type.toString().toLowerCase()
       })

     });
     var rowCount = dataTable.count()
     let slice = [];
      for (let i=0; i <rowCount; i++) {
        slice.push(dataTable.get(i).toArray());
      }

     this.props.dispatch(
       addDataToMap({
         datasets: [
           {
             info: {
               label: dataset.dataSetName,
               id: dataset.path,
               dataSetProperties:dataset.dataSetProperties
             },
             //data: dataTable
            // data: processRowObject(values)
             data:{
               fields: fieldArray,
               rows: slice
             }
           },
           //  {
           //    info: {label: 'SF Zip Geo', id: 'sf-zip-geo'},
           //    data: processGeojson(sampleGeojson)
           //  }
         ],
         options: {
           centerMap: true,
           readOnly: false,
           keepExistingConfig: true
         },
         //config: sampleGeojsonConfig
       })
     );
   }
  _loadPointData(options, dataset) {
    var rowarray = [];
    Object.keys(options.data["rows"]).forEach(function (key) {
      options.data["rows"][key].forEach(obj => {
        var myInnerArray = [];
        Object.keys(obj).forEach(key => {
          myInnerArray.push(obj[key]);
        })
        rowarray.push(myInnerArray);
      })
    })
      ;


    const visurdata = {
      fields: options.data["schema"]["0"],
      rows: rowarray

    };
    this.props.dispatch(
      addDataToMap({
        datasets: {
          info: {
            label: dataset.dataSetName,
            id: dataset.path,
            dataSetProperties:dataset.dataSetProperties
          },
          data: visurdata
        },
        options: {
          centerMap: true,
          readOnly: false,
          keepExistingConfig: true
        },
        // config: sampleTripDataConfig
      })
    );

  }
  // _loadSampleData() {
  //   this._loadPointData();
  //   // this._loadGeojsonData();
  //   // this._loadTripGeoJson();
  //   // this._loadIconData();
  //   // this._loadH3HexagonData();
  //   // this._loadS2Data();
  //   // this._loadScenegraphLayer();
  // }

  // _loadPointData() {
  //   this.props.dispatch(
  //     addDataToMap({
  //       datasets: {
  //         info: {
  //           label: 'Sample Taxi Trips in New York City',
  //           id: 'test_trip_data'
  //         },
  //         data: sampleTripData
  //       },
  //       options: {
  //         centerMap: true,
  //         readOnly: false
  //       },
  //       config: sampleTripDataConfig
  //     })
  //   );
  // }

  // _loadScenegraphLayer() {
  //   this.props.dispatch(
  //     addDataToMap({
  //       datasets: {
  //         info: {
  //           label: 'Sample Scenegraph Ducks',
  //           id: 'test_trip_data'
  //         },
  //         data: processCsvData(testCsvData)
  //       },
  //       config: {
  //         version: 'v1',
  //         config: {
  //           visState: {
  //             layers: [
  //               {
  //                 type: '3D',
  //                 config: {
  //                   dataId: 'test_trip_data',
  //                   columns: {
  //                     lat: 'gps_data.lat',
  //                     lng: 'gps_data.lng'
  //                   },
  //                   isVisible: true
  //                 }
  //               }
  //             ]
  //           }
  //         }
  //       }
  //     })
  //   );
  // }

  // _loadIconData() {
  //   // load icon data and config and process csv file
  //   this.props.dispatch(
  //     addDataToMap({
  //       datasets: [
  //         {
  //           info: {
  //             label: 'Icon Data',
  //             id: 'test_icon_data'
  //           },
  //           data: processCsvData(sampleIconCsv)
  //         }
  //       ]
  //     })
  //   );
  // }

  // _loadTripGeoJson() {
  //   this.props.dispatch(
  //     addDataToMap({
  //       datasets: [
  //         {
  //           info: {label: 'Trip animation'},
  //           data: processGeojson(sampleAnimateTrip)
  //         }
  //       ]
  //     })
  //   );
  // }

  // _loadGeojsonData() {
  //   // load geojson
  //   this.props.dispatch(
  //     addDataToMap({
  //       datasets: [
  //         {
  //           info: {label: 'Bart Stops Geo', id: 'bart-stops-geo'},
  //           data: processGeojson(sampleGeojsonPoints)
  //         },
  //         {
  //           info: {label: 'SF Zip Geo', id: 'sf-zip-geo'},
  //           data: processGeojson(sampleGeojson)
  //         }
  //       ],
  //       options: {
  //         keepExistingConfig: true
  //       },
  //       config: sampleGeojsonConfig
  //     })
  //   );
  // }

  // _loadH3HexagonData() {
  //   // load h3 hexagon
  //   this.props.dispatch(
  //     addDataToMap({
  //       datasets: [
  //         {
  //           info: {
  //             label: 'H3 Hexagons V2',
  //             id: 'h3-hex-id'
  //           },
  //           data: processCsvData(sampleH3Data)
  //         }
  //       ],
  //       config: h3MapConfig,
  //       options: {
  //         keepExistingConfig: true
  //       }
  //     })
  //   );
  // }

  // _loadS2Data() {
  //   // load s2
  //   this.props.dispatch(
  //     addDataToMap({
  //       datasets: [
  //         {
  //           info: {
  //             label: 'S2 Data',
  //             id: s2DataId
  //           },
  //           data: processCsvData(sampleS2Data)
  //         }
  //       ],
  //       config: s2MapConfig,
  //       options: {
  //         keepExistingConfig: true
  //       }
  //     })
  //   );
  // }

  // _toggleCloudModal = () => {
  //   // TODO: this lives only in the demo hence we use the state for now
  //   // REFCOTOR using redux
  //   this.setState({
  //     cloudModalOpen: !this.state.cloudModalOpen
  //   });
  // };

  _getMapboxRef = (mapbox, index) => {
    if (!mapbox) {
      // The ref has been unset.
      // https://reactjs.org/docs/refs-and-the-dom.html#callback-refs
      // console.log(`Map ${index} has closed`);
    } else {
      // We expect an InteractiveMap created by KeplerGl's MapContainer.
      // https://uber.github.io/react-map-gl/#/Documentation/api-reference/interactive-map
      const map = mapbox.getMap();
      map.on('zoomend', e => {
        // console.log(`Map ${index} zoom level: ${e.target.style.z}`);
      });
    }
  };

  render() {
    return (
      <ThemeProvider theme={theme}>

        <GlobalStyle
          // this is to apply the same modal style as visur.kepler.gl core
          // because styled-components doesn't always return a node
          // https://github.com/styled-components/styled-components/issues/617
          ref={node => {
            node ? (this.root = node) : null;
          }}
        >
          <Banner
            show={this.state.showBanner}
            height={BannerHeight}
            bgColor="#2E7CF6"
            onClose={this._hideBanner}
          >
            {/* <Announcement /> */}
          </Banner>

          <div
            style={{
              transition: 'margin 1s, height 1s',
              position: 'absolute',
              width: '100%',
              height: '100%',
              left: 0,
              top: 0
            }}
          >
            <LoadingOverlay
              active={this.state.isActive}
              spinner text='Loading...'></LoadingOverlay>

            <AutoSizer>
              {({ height, width }) => (

                <KeplerGl
                  mapboxApiAccessToken="pk.eyJ1IjoidmlrYXNoc2luZ2gwMDkiLCJhIjoiY2tlNm9uZnRrMTU3ejJ5bDZob2F6cXl5NiJ9.YdsQMrauCAWUnMI1xM44HA"
                  // id="map"
                  /*
                   * Specify path to keplerGl state, because it is not mount at the root
                   */
                  //getState={keplerGlGetState}
                  width={width}
                  height={height}
                  // cloudProviders={CLOUD_PROVIDERS}
                  localeMessages={messages}
                  onExportToCloudSuccess={onExportFileSuccess}
                  onLoadCloudMapSuccess={onLoadCloudMapSuccess}
                />
              )}
            </AutoSizer>


          </div>
        </GlobalStyle>
      </ThemeProvider>
    );
  }
}

const mapStateToProps = state => state;
const dispatchToProps = dispatch => ({ dispatch });

export default connect(mapStateToProps, dispatchToProps)(App);










