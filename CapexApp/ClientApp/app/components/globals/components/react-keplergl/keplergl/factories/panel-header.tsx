// Copyright (c) 2020 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import React from 'react';
import styled from 'styled-components';
import {getConfig} from '../config'
import {PanelHeaderFactory, Button, Icons, withState} from 'visur.kepler.gl/components';
import {visStateLens} from 'visur.kepler.gl/reducers';
import {KeplerGlSchema} from 'visur.kepler.gl/schemas';
import {setMapConfig} from '../reducers';
import { getUrlParams } from '../helper-functions'
import {Service} from '../../keplergl/Service'
let StyledPanelToggleWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding-right: 20px;
  background-color: ${props => props.theme.sidePanelHeaderBg};
`;

const ButtonWrapper = styled.div`
  margin-bottom: 10px;
`;



const PanelToggleWrapper = props =>
{
  let clickbtnCss={
    width: '300px',
    height: '45px',
    'fontSize': '15px'
  }
  return(
    <StyledPanelToggleWrapper>
      <ButtonWrapper>
        <Button onClick={() => onClickSaveConfig(props.mapState)} style={clickbtnCss}>

          {/* <Icons.Files height="12px" /> */}
          <span className="saveMapText">Save Changes</span>
        </Button>
      </ButtonWrapper>
    </StyledPanelToggleWrapper>
  )
}

async function onClickSaveConfig(state){
  const paramStr = window.location.search
  let loaderMessage = {"operation" : "start"};
  Service.sendMessage(loaderMessage)
  const json:any = getUrlParams()
  let configData:any=await getConfig()
  console.log(configData)
  let BACKEND_URL=configData.env.ConstructionApiUrl;
  console.log(state);
  const config = KeplerGlSchema.getConfigToSave(state);
  config.dataSets=[]
  let dataSets=state.visState.datasets
  for (const key in dataSets) {
    if (Object.prototype.hasOwnProperty.call(dataSets, key)) {
      const element = dataSets[key];
      config.dataSets.push({id:element.id,dataSetProperties:element.dataSetProperties})
    }
  }
  let message:any={};
  let Payload={EntityId:json.EntityId,Config:config}
  message.CapabilityId = "";
  message.Fabric = "Maps";
  message.IsAdmin = "";
  message.UserID = "";
  message.ClientID = "";
  message.Type = "";
  message.Payload = JSON.stringify(Payload);
  message.Routing = "";
  message.MessageKind = "";
  message.DataType = "UpdateMapConfig";
  message.EntityType = "Map";
  message.EntityID = Payload.EntityId;
  message.TenantId = json.TenantId;
  message.TenantName = json.TenantName;//"constructiontest";
  message.MessageID = "";
  message.SourceSystem = "";
  message.Application = "";
  var requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(message)
  };
  fetch(BACKEND_URL+'/kepler/updatedMapConfig/', requestOptions).then(function (res) {
    console.log(res)
    let loaderMessage = {"operation" : "stop"};
    Service.sendMessage(loaderMessage)
  })
  console.log(config)
}

const CustomPanelToggleFactory = () =>
  withState(
    // lenses
    [visStateLens],
    // mapStateToProps
    state => ({mapState: state.keplerGl.map}),
    {
      onClickSaveConfig: setMapConfig
    }
  )(PanelToggleWrapper);

export default CustomPanelToggleFactory;

CustomPanelToggleFactory.deps = PanelHeaderFactory.deps;

export function replacePanelHeader() {
  return [PanelHeaderFactory, CustomPanelToggleFactory];
}
