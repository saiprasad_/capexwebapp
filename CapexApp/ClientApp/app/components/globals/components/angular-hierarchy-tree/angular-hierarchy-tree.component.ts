import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { ActivatedRoute, Router } from '@angular/router';
import { IActionMapping, ITreeOptions, KEYS, TreeDraggedElement, TreeModel, TreeNode, TREE_ACTIONS } from '@circlon/angular-tree-component';
import { AppConsts } from 'ClientApp/app/components/common/Constants/AppConsts';
import * as _ from 'lodash';
import { Subject, Subscription, timer } from 'rxjs';
import { takeUntil } from "rxjs/operators";
import { Observable } from 'rxjs/Rx';
import { ObservableStoreService } from '../../../common/state/observables/observables.entity.state.query';
import { CommonService } from '../../CommonService';
import { formateTreeDataToTable, getSelectedNodes } from '../../helper.functions';
import { AngularTreeEventMessageModel, AngularTreeMessageModel, FABRICS, TreeDataEventResponseModel, TreeOperations, TreeTypeNames, TypeOfEntity } from '../../Model/CommonModel';
import { EntityTypes, ITreeConfig } from '../../Model/EntityTypes';

let singleClickActive: boolean = false;
let clickedNodeUUID: Number = 0;
const actionMapping: IActionMapping = {
  mouse: {
    dragStart: (tree, node, $event, thisComp) => { 
      if(thisComp){
        thisComp.onDragStart($event,node); 
      }
    },
    dragEnd: (tree, node, $event, thisComp) => { 
      if(thisComp){
        thisComp.onDragEnd($event,node); 
      }
    },
    contextMenu: (tree, node, $event, thisComp) => {
      $event.preventDefault();
      AngularHierarchyTreeComponent.clientXValue = 0;
      AngularHierarchyTreeComponent.clientYValue = 0;
      if (thisComp.HierarchyTreeName != "AvailableEntities") {
        thisComp.sendDataToCapabilityTreeComponent("NodeOnContextClickBeforeInit", node, $event);
      }
    },
    dblClick: (tree, node, $event, thisComp) => {
      TREE_ACTIONS.TOGGLE_EXPANDED(tree, node, $event)
    },
    click: (tree, node, $event, thisComp) => {
      singleClickActive = true;
      clickedNodeUUID = node.data.uuid;
      if (node.data.EntityType != "Personal" && node.data.EntityType != "Shared" && node.data.EntityType != "Carousel" || !(node.data.Parent && node.data.Parent.length > 0 && node.data.Parent[0].EntityId == "Shared")) {

        thisComp.callshiftclick(tree, node, $event);
        if (thisComp.HierarchyTreeName != "AvailableEntities") {
          ($event.shiftKey || $event.ctrlKey)
            ? null
            : thisComp.sendDataToCapabilityTreeComponent("NodeOnClick", node);
        }
      }

    },


  },
  keys: {
    [KEYS.ENTER]: (tree, node, $event) => { }
  }
};

@Component({
  selector: 'app-angular-hierarchy-tree',
  templateUrl: './angular-hierarchy-tree.component.html',
  styleUrls: ['./angular-hierarchy-tree.component.scss']
})
export class AngularHierarchyTreeComponent implements OnInit, AfterViewInit, OnDestroy {
  customTemplateStringOptions: ITreeOptions = {
    displayField: 'EntityName',
    isExpandedField: 'expanded',
    idField: 'uuid',
    actionMapping,
    nodeHeight: 18,
    allowDrag: true,
    allowDrop: false,
    useVirtualScroll: true,
    animateExpand: false,
    scrollOnActivate: false,
    animateSpeed: 10,
    animateAcceleration: 1.2,
    dropSlotHeight: 2
  };
  treeDefaultConfigSetting: ITreeConfig = {
    'treeActiveOnClick': true,
    'treeDraggable': true
  }

  @Input() HierarchyTreeName = '';
  @Output() EmmitTreeData: EventEmitter<any> = new EventEmitter<any>();
  takeUntilDestroyObservables = new Subject();
  @ViewChild(MatMenuTrigger, { static: false }) trigger: MatMenuTrigger;
  @ViewChild('tree', { static: false }) angularTree: any;

  locations = [];
  peoples = [TypeOfEntity.Company, TypeOfEntity.Office, TypeOfEntity.Person];
  nodes: any[] = [];
  contextMenuOption = [];
  previousnodes = [];

  searchValue = ''
  nodeWidth = '100%';
  from = 0
  to = 0
  previousid;
  tempCallBackFunction;
  ActiveTreeNode;
  data2;
  tree;
  filterFunc: any;
  public static clientXValue: any;
  public static clientYValue: any;
  public treeDataLengthIsZero = true;
  expanderClickEventRestrict = true;
  isShiftAndCtrlSelectedNodeAvailable = false;
  dropDataPointer:string;
  constructor(private observableStateService: ObservableStoreService, private elementRef: ElementRef, private cdRef: ChangeDetectorRef, public router: Router, public route: ActivatedRoute, public dialog: MatDialog, public commonService: CommonService, public treeDraggedElement: TreeDraggedElement) {
  }

  ngOnInit() {
    try {
      this.customTemplateStringOptions.rootId = this.HierarchyTreeName;
      // if (this.commonService.lastOpenedFabric == "Security") {
      //   this.customTemplateStringOptions.useVirtualScroll = false;
      // } else {
      //   this.customTemplateStringOptions.useVirtualScroll = true;
      // }
    }
    catch (e) {
      console.error('Exception in ngOnInit() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  ngAfterViewInit() {
  }

  changeDefaultTreeConfig(config: ITreeConfig) {
    try {
      if (config.treeActiveOnClick != undefined || config.treeActiveOnClick != null) {
        this.treeDefaultConfigSetting.treeActiveOnClick = config.treeActiveOnClick;
      }
      if (config.treeExpandAll) {
        this.expandAll();
      }
      if (config.treeExpandAll == false) {
        this.collapseAll();
      }
      if (config.treeDropable) {
        this.customTemplateStringOptions.allowDrop = config.treeDropable;
      }

    }
    catch (e) {
      console.error('Exception in changeDefaultTreeConfig() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }


  public setExpandedNodes(expandedNodeIds: any) {
    this.tree.treeModel.expandedNodeIds = expandedNodeIds;
    this.refreshUpdateTree();
  }

  public collapseAll() {
    let expandedNodeIds: any = {};
    this.setExpandedNodes(expandedNodeIds);
  }

  public expandAll() {
    let expandedNodeIds: any = {};
    this.tree.treeModel.update();

    for (let node of this.tree.treeModel.roots) {
      expandedNodeIds = this.updateNode(node, expandedNodeIds, true)
    }

    this.setExpandedNodes(expandedNodeIds);
  }
  private updateNode(node: TreeNode, expandedNodeIds: any, expand: boolean) {
    let newExp = expandedNodeIds
    let children = node.children
    if (children) {
      for (let child of children) {
        newExp = this.updateNode(child, newExp, expand);
      }
    }
    if (node.hasChildren) {
      return Object.assign({}, newExp, { [node.id]: expand });
    }
    return newExp;
  }


  displayData() {

  }

  cancle() {
  }

  reinitializeTreeSubscription(treeData$: Observable<any>) {
    if (this.tree$) {
      this.tree$.unsubscribe();
    }

    this.GenerateTree(treeData$);
  }

  tree$: Subscription;

  GenerateTree(treeData$: Observable<any>) {
    let isSubscribe = true;
    this.treeDataLengthIsZero = false;
    this.tree$ = treeData$.pipe(this.compUntilDestroyed())
      .subscribe((data: any) => {
        var length = data ? data.length : 0;
        if (isSubscribe && this.nodes && data && data.length >= this.nodes.length) {
          this.nodes = data;
          this.treeDataLengthIsZero = !false;
          this.cdRef.detectChanges();
          this.cdRef.markForCheck();

          timer(500)
            .pipe(this.compUntilDestroyed())
            .subscribe(
              res => {
                this.sendDataToCapabilityTreeComponent('FullTreeOnCreation', this.tree);
              });
          isSubscribe = false;
        }
        else {
          this.treeDataLengthIsZero = true;
        }
      });
  }

  async updateTreeNode() {

  }

  receiveMessageFromFabrics(message: AngularTreeMessageModel) {
    try {
      this.expanderClickEventRestrict = false
      switch (message.treeOperationType) {
        case TreeOperations.createFullTree:
        case TreeOperations.createTree:
        case TreeOperations.filteredTree: {
          // if (message.treeName.indexOf(TreeTypeNames.ffvEvent) > -1) {
          //   if (this.customTemplateStringOptions.rootId == TreeTypeNames.ffvGroup || this.customTemplateStringOptions.rootId == TreeTypeNames.ffvEvent) {
          //     this.deactivateAllActiveNodes(message);
          //   }

          //   this.customTemplateStringOptions.rootId = message.treeName[0];
          //   message.treePayload.forEach(node => {
          //     if (node.children) {
          //       node.children = node.children.filter(rowData => rowData.EntityName != undefined);
          //       node.children.sort((firstEntity, secondEntity) => {
          //         return firstEntity['EntityName'].toLowerCase() > secondEntity['EntityName'].toLowerCase() ? 1 : -1;
          //       });
          //     }
          //   });
          // }
          // else if (message.treeName.indexOf(TreeTypeNames.ffvGroup) > -1) {
          //   if (this.customTemplateStringOptions.rootId == TreeTypeNames.ffvGroup || this.customTemplateStringOptions.rootId == TreeTypeNames.ffvEvent) {
          //     this.deactivateAllActiveNodes(message);
          //   }

          //   this.customTemplateStringOptions.rootId = message.treeName[0];
          // }
          // else if (message.treeName.indexOf(TreeTypeNames.opforms) > -1 || message.treeName.indexOf(TreeTypeNames.schedules) > -1) {
          //   this.deactivateAllActiveNodes(message);
          //   this.customTemplateStringOptions.rootId = message.treeName[0];
          // }

          // if (message.treeConfig) {
          //   this.nodes = message.treePayload;
          //   const config: ITreeConfig = message.treeConfig;
          //   setTimeout(() => {
          //     this.changeDefaultTreeConfig(config);
          //   }, 600);
          // }
          // else {
          this.nodes = message.treePayload && message.treePayload.length ? message.treePayload : [];
          //}

          timer(500)
            .pipe(this.compUntilDestroyed())
            .subscribe(
              res => {
                //this.sendDataToCapabilityTreeComponent('FullTreeOnCreation', this.tree);
              });
          break;
        }
        case TreeOperations.treeConfig: {
          const config: ITreeConfig = message.treePayload;
          this.changeDefaultTreeConfig(config);
          break;
        }
        case TreeOperations.treeContextMenuOption: {
          this.contextMenuOption = message.treePayload;
          if (this.contextMenuOption.length > 0) {
            setTimeout(() => {
              this.createContextmenue(this.tempCallBackFunction().currentElement);
            }, 10);
          }
          break;
        }
        case TreeOperations.ActiveSelectedNode: {
          this.ActiveSelectedNode(message);
          break;
        }
        case TreeOperations.deactivateSelectedNode: {
          this.DeActiveSelectedNode(message);
          break;
        }
        case TreeOperations.deactivateAllAvtiveNode: {
          this.deactivateAllActiveNodes(message);
          break;
        }
        case TreeOperations.AddNewNode: {
          this.AddNewNode(message);
          break;
        }
        case TreeOperations.AddNewNodeOtherSession: {
          this.AddNewNode_OtherSession(message);
          break;
        }
        case TreeOperations.AddNewFullChildrens: {
          this.AddFullChildrensForParens(message.treePayload);
          break;
        }
        case TreeOperations.deleteNodeById: {
          this.deleteNodeById(message);
          break;
        }
        case TreeOperations.updateNodeById: {
          this.updateNodeById(message);
          break;
        }
        case TreeOperations.moveNodeFromTo: {
          this.moveNodeById(message.treePayload);
          break;
        }
        case TreeOperations.CreateUpdateentityFilter:
        case TreeOperations.entityFilter: {
          this.TreeFilterByText(message);
          break;
        }
        case TreeOperations.filterTree: {
          this.TreeFilterByEntityIds(message);
          break;
        }
        case TreeOperations.RemoveFilter: {
          this.RemoveFilter();
          break;
        }
        case TreeOperations.ReArrange:
          this.ReArrange(message);

        case TreeOperations.deleteNodebasedOnparentEntity:
          this.deleteNodebasedOnparentEntity(message);
          break;

        case TreeOperations.MultiOperation:
          this.multiOperationOnMessage(message);
          break;

        case TreeOperations.addOrUpdateNode:
          this.addOrUpdateNode(message);
          break;

        case TreeOperations.RefreshTree:
          this.RefreshTree();
          break;
        case TreeOperations.multiSelectDeActive:
          this.clickOutSideTreeBody();
          break;
        case TreeOperations.refreshUpdateTree:
          this.refreshUpdateTree();
          break;
        case TreeOperations.expandeORcollapseClick:
          this.expandeORcollapseOnClickForLists(message);
          break;
        case TreeOperations.sortingAfterUpdatingTreename:
          this.sortingafterupdatingtreename(message);
          break;
        case TreeOperations.updatedropPointer:
          if(message.treePayload && message.treePayload.dropDataPointer){
            this.dropDataPointer = message.treePayload.dropDataPointer;
          }
        break;
      }
      this.expanderClickEventRestrict = true
      this.cdRef.detectChanges();
      this.cdRef.markForCheck();
    }
    catch (e) {
      console.error('Exception in receiveMessageFromFabrics() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  expandeORcollapseOnClickForLists(message: AngularTreeMessageModel) {
    var node;
    let currentOpenedEntity;
    node = this.getNodeByEntityId(message.treePayload.data.EntityId);
    currentOpenedEntity = this.commonService.getEntityIDFromUrl(this.router.url);
    if (node.isExpanded && currentOpenedEntity == message.treePayload.data.EntityId) {
      // node.collapse(); //single click removed collapse for list.
    }
    else if (!node.isExpanded)
      node.expand();
  }

  RefreshTree() {
    try {
      let fabricName = this.commonService.getFabricNameByUrl(this.router.url);
      this.observableStateService.getState$(fabricName, this.HierarchyTreeName)
        .first()
        .pipe(this.compUntilDestroyed())
        .subscribe((obs: any) => {
          this.reinitializeTreeSubscription(obs);
        });
    }
    catch (error) {
      console.error('Exception in RefreshTree() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + error + new Error().stack);
    }
  }

  addOrUpdateNode(message) {
    try {
      let node = this.getNodeById(message.treePayload.EntityId);
      if (node == undefined) {
        node = this.getNodeByEntityId(message.treePayload.EntityId);
      }
      if (node) {
        message.treePayload.NodeData.children = node.data.children && node.data.children.length > 0 ? node.data.children : [];
        this.updateNodeById(message);
      }
      else {
        this.AddNewNode(message);
      }
    }
    catch (e) {
      console.error('Exception in addOrUpdateNode() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  multiOperationOnMessage(message: AngularTreeMessageModel) {
    try {
      if (message.treePayload) {
        let treeItems = message.treePayload.filter(d => d.treeName.indexOf(this.HierarchyTreeName) !== -1)
        if (treeItems.length) {
          treeItems.forEach(treeMsg => {
            this.receiveMessageFromFabrics(treeMsg);
          })
        }
      }
    }
    catch (e) {
      console.error(e);
    }
  }
  //Delete Node Based On Facility (Parent Node Based)
  deleteNodebasedOnparentEntity(message: AngularTreeMessageModel) {
    try {
      var model: TreeModel = this.tree.treeModel;
      var nodepayload = undefined;
      if (message.treePayload && message.treePayload.EntityId) {
        // if (message.treeName.indexOf(TreeTypeNames.LICENSEE) > -1 && message.treePayload.parentId) {
        //   nodepayload = model.getNodeBy((node) => node.data.EntityId == message.treePayload.EntityId && node.parent.data.EntityId == message.treePayload.parentId);
        // } else {
        if (message.treePayload.ParentModelId)
          nodepayload = model.getNodeBy((node) => node.data.EntityId == message.treePayload.EntityId && node.parent && node.parent.id == message.treePayload.ParentModelId);
        // }
      }
      if (nodepayload) {
        _.remove(nodepayload.parent.data.children, nodepayload.data);
        this.refreshUpdateTree();
      }
    }
    catch (e) {
      console.error('Exception in deleteNodebasedOnparentEntity() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }
  TreeFilterByText(message: AngularTreeMessageModel) {
    try {

      if (message.treePayload && message.treePayload.Entities) {
        this.searchValue = message.treePayload.value;

        if (this.searchValue !== undefined) {
          if (this.searchValue === '' && !this.filterFunc && this.tree && this.tree.treeModel) {
            this.tree.treeModel.clearFilter();
            if (message.treeOperationType != TreeOperations.CreateUpdateentityFilter && (this.customTemplateStringOptions ))
              this.partialCollapse()
          }
          else {
        
              let flag = false
              if (this.searchValue && message.treeOperationType != TreeOperations.CreateUpdateentityFilter) {
                flag = true
              }
              const EntityIds = message.treePayload.Entities.filter(
                !this.filterFunc ? d => true : this.filterFunc)
                .filter(d => this.searchValue === '' || d && d.EntityName && d.EntityName.toUpperCase()
                  .includes(this.searchValue.toUpperCase())).map(d => d.EntityId)
              if (this.tree && this.tree.treeModel)
                this.tree.treeModel.filterNodes((node) => {
                  return EntityIds.includes(node.data.EntityId);
                }, flag);
            
          }
        }
      } else {
        // this.filterPayload = message;
        if (this.tree != undefined)
          this.tree.treeModel.filterNodes(message.treePayload);
      }

    } catch (e) {
      console.error('Exception in TreeFilterByText() of angular-hierarchy-tree.component in angular-hierarchy-tree at time '
        + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }

  }

  RemoveFilter() {
    this.filterFunc = undefined
    if (this.searchValue != '')
      this.tree.treeModel.filterNodes(this.searchValue)
    else {
      this.tree.treeModel.clearFilter()
      this.collapseAll();
    }

  }
  TreeFilterByEntityIds(message: AngularTreeMessageModel) {
    this.filterFunc = message.treePayload.filterFunc
    let EntityIds = message.treePayload.Entities.filter(d => this.searchValue == '' || d && d.EntityName && d.EntityName.toUpperCase().includes(this.searchValue && this.searchValue.toUpperCase())).filter(this.filterFunc).map(d => d.EntityId)
    this.tree.treeModel.filterNodes((node) => {
      return EntityIds.includes(node.data.EntityId);
    }, false);
    this.partialCollapse();
  }
  printErrorInConsole(data, errorType) {
    console.error(data + "---AngularTree---" + errorType);
  }


  ActiveSelectedNode(message: AngularTreeMessageModel) {
    // this.customTemplateStringOptions.animateExpand=false;
    var node = this.getNodeByEntityId(message.treePayload);
    if (node && node.data && node.data.uuid)
      clickedNodeUUID = node.data.uuid
    if (node && node.data.EntityType != EntityTypes.List && clickedNodeUUID != 0 && message.treeName[0] == "Lists") {//single clicked node
      let nodes = this.getAllNodeByEntityId(message.treePayload);
      nodes.forEach(ele => {
        if (this.commonService.listsParentData && ele.data.Parent[0].EntityId == this.commonService.listsParentData.EntityId) {
          node = ele;
          if (node && node.data && node.data.uuid)
            clickedNodeUUID = node.data.uuid
        }
      })
      node = this.tree.treeModel.getNodeById(clickedNodeUUID)
      this.commonService.listPreviousLocation = node.data;
    }

    this.ActiveTreeNode = node;
    if (node == undefined) {
    } else {
      var treeNode = this.tree.treeModel.getFocusedNode();
      if (treeNode != null && treeNode && treeNode.isActive) {
        treeNode.toggleActivated();
      }
      node.setActiveAndVisible(true);
    }
    if (node && (node.data.typeOfField == "Location" || node.data.typeOfField == "Locations")) {
      this.nextlocationId = node.id;
    }
    else if (node == undefined && message.treeName[0] == "Lists" && this.tree && this.tree.treeModel.activeNodes.length != 0) {
      this.DeActiveSelectedNode(this.tree.treeModel.activeNodes[0].data.EntityId);
    }
    // this.customTemplateStringOptions.animateExpand=true;
    // this.cdRef.detectChanges();
    // this.cdRef.markForCheck();
  }



  AddNewNode(message: AngularTreeMessageModel) {
    try {
      let nodes: any[] = [];
      var payloadData = JSON.parse(JSON.stringify(message.treePayload));
      let isExist = this.getAllNodeByEntityId(payloadData.NodeData.EntityId);
      if (isExist && isExist.length > 0) {
        return;
      }
      let sorFilter = (firstEntity, secondEntity) => { return firstEntity && secondEntity && firstEntity["EntityName"] && secondEntity["EntityName"] && firstEntity["EntityName"].toLowerCase() > secondEntity["EntityName"].toLowerCase() ? 1 : -1 };
      if (payloadData.InsertAtPosition == 'althingTreeRoot') {
        let isNodeExist = false;
        this.nodes.forEach((node) => {
          if (node.EntityId == payloadData.NodeData.EntityId) {
            isNodeExist = true;
          }
        });
        if (!isNodeExist) {
          this.nodes.unshift(payloadData.NodeData);
        }
        this.nodes.sort((firstEntity, secondEntity) => {
          if (firstEntity["EntityName"] != undefined && secondEntity["EntityName"] != undefined)
            return firstEntity["EntityName"].toLowerCase() > secondEntity["EntityName"].toLowerCase() ? 1 : -1
        });
        this.refreshUpdateTree();
      }
      else {
        nodes = this.getAllNodeByEntityId(payloadData.InsertAtPosition);
        nodes = nodes ? nodes : [];
        nodes.forEach(node => {
          node.data.children = Object.assign([], node.data.children);
          node.data.children = node.data.children ? node.data.children : [];
          node.data.children.push(payloadData.NodeData);

        });
        if (message.treeName.length) {
          nodes.forEach(node => {
            if (node.data.children) {
              node.data.children = node.data.children.filter(rowData => rowData.EntityName != undefined);
              node.data.children.sort(sorFilter);
            }
          });
        }
        this.refreshUpdateTree();
      }
      if (payloadData.SortOrder) {
        if (payloadData.InsertAtPosition == 'althingTreeRoot') {
          this.nodes = this.sortNodesByOrder(this.nodes, payloadData.SortOrder);
        }
        else {
          if (nodes && nodes.length > 0) {
            nodes.forEach((node: any) => {
              if (node.data && node.data.children)
                node.data.children = this.sortNodesByOrder(node.data.children, payloadData.SortOrder);
            });
          }
        }
        this.refreshUpdateTree();
      }
      else if (payloadData && payloadData.SortByName) {
        this.nodes = this.sortNodesByName(this.nodes);
        this.refreshUpdateTree();
      }
      message.treeOperationType = TreeOperations.CreateUpdateentityFilter
      message.treePayload = this.searchValue
      this.EmmitTreeData.next(message);


    }
    catch (e) {
      console.error('Exception in AddNewNode() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  AddNewNode_OtherSession(message: AngularTreeMessageModel) {

  }

  sortNodesByOrder(listOfChildren: any[], SortingOrder: any[]): any[] {
    try {
      let NewSortedListOfChildren = [];
      SortingOrder.map((item: string) => {
        let SortArray = [];
        listOfChildren.forEach((element: any) => {
          if (element.EntityType == item || element.Type == item)
            SortArray.push(element);
        });

        SortArray.sort((firstEntity, secondEntity) => {
          if (firstEntity['EntityName'] && secondEntity['EntityName'])
            return firstEntity['EntityName'].toLowerCase() > secondEntity['EntityName'].toLowerCase() ? 1 : -1
        });
        NewSortedListOfChildren = [...NewSortedListOfChildren, ...SortArray];
      });
      return NewSortedListOfChildren;
    }
    catch (e) {
      console.error('Exception in getSortingListOfNodes()  at time ' + new Date().toString() + '. Exception is : ' + e);
      // this.logger.logException(new Error('Exception in getSortingListOfChildren() of FDCService in FDC/Service at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  sortNodesByName(listOfChildren: any[]): any[] {
    try {
      return listOfChildren.sort((firstEntity, secondEntity) => {
        if (firstEntity['EntityName'] && secondEntity['EntityName'])
          return firstEntity['EntityName'].toLowerCase() > secondEntity['EntityName'].toLowerCase() ? 1 : -1
      });
    }
    catch (e) {
      console.error('Exception in getSortingListOfNodes()  at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  AddFullChildrensForParens(payloadData) {
    try {
      var node = this.getNodeByEntityId(payloadData.InsertAtPosition);
      if (node != null) {
        node.data.children = payloadData.NodeData
        this.refreshUpdateTree();
      }
    }
    catch (e) {
      console.error('Exception in AddFullChildrensForParens() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  refreshUpdateTree() {
    try {
      if (this.tree && this.tree.treeModel) {
        this.tree.treeModel.update();
      }
      this.cdRef.detectChanges();
    }
    catch (e) {
      console.error('Exception in refreshUpdateTree() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }

  }

  deleteNodeById(message: AngularTreeMessageModel) {
    try {
      message = JSON.parse(JSON.stringify(message));
      if (this.tree && this.tree.treeModel) {
        var model: TreeModel = this.tree.treeModel;
        var node = this.getNodeById(message.treePayload); // based on uid

        if (message.treePayload && message.treePayload.parent != undefined) {
          node = model.getNodeBy(
            (node) =>
              node.data.EntityId == message.treePayload.EntityId && message.treePayload.parent && message.treePayload.parent.id &&
              ((node.data.parent && node.data.parent.id == message.treePayload.parent.id) ||
                (node.data.Parent && node.data.Parent[0] && node.data.Parent[0].EntityId == message.treePayload.parent.id)));

          if (node === undefined) {
            node = model.getNodeBy(
              (node) =>
                node.data.EntityId == message.treePayload.EntityId);
          }
        } else if (!node) {
          node = this.getNodeByEntityId(message.treePayload);
        }
        this.commonService.CurrentList = message.treePayload;
        if (node && node.parent.data && node.parent.data.EntityType && node.parent.data.EntityType == EntityTypes.List && message.treePayload.parent &&
          message.treePayload.parent.id && node.parent.data.EntityId == message.treePayload.parent.id) {
          if (node.parent.data && node.parent.data.EntityType && node.parent.data.EntityType == EntityTypes.List) {
            node.parent.data.children = JSON.parse(JSON.stringify(node.parent.data.children));
            node.data = JSON.parse(JSON.stringify(node.data));
          }
          // let nodetoDelete =JSON.parse(JSON.stringify(node));
          _.remove(node.parent.data.children, node.data);
          this.refreshUpdateTree();
        } else if (node) {
          if (node.parent.data && node.parent.data.EntityType && node.parent.data.EntityType == EntityTypes.List) {
            node.parent.data.children = JSON.parse(JSON.stringify(node.parent.data.children));
            node.data = JSON.parse(JSON.stringify(node.data));
          }
          // let nodetoDelete =JSON.parse(JSON.stringify(node));
          _.remove(node.parent.data.children, node.data);
          this.refreshUpdateTree();
        }
      }
    }
    catch (e) {
      console.error('Exception in deleteNodeById() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }

  }

  ReArrange(message: AngularTreeMessageModel) {
    try {
      var node = this.getNodeById(message.treePayload.NodeData.Parent[0]["EntityId"]);
      if (node == undefined) {
        node = this.getNodeByEntityId(message.treePayload.NodeData.Parent[0]["EntityId"]);
      }
      if (node) {
        var nodeData = message.treePayload.NodeData;
        node.data.children.push(nodeData);
      }
      this.refreshUpdateTree();
      this.cdRef.detectChanges();
    }

    catch (e) {
      console.error('Exception in updateNodeById() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }

  }
  sortingafterupdatingtreename(message: AngularTreeMessageModel) {
    var node = this.getNodeById(message.treePayload.EntityId);
    if (node == undefined) {
      node = this.getNodeByEntityId(message.treePayload.EntityId);
    }
    if (node) {
      if (node.data.Downstream && node.data.Downstream.length != 0) {
        node.parent.data.children = this.sortNodesByName(node.parent.data.children);
      }
      else
        if (node.parent && node.parent.data && node.parent.data.children) {
          let sortingorder;
          let fabricName = this.commonService.getFabricNameByUrl(this.router.url);
          switch (fabricName) {
            case FABRICS.ENTITYMANAGEMENT:
              break;
            default:
              break;
          }
          if (sortingorder && (node.parent.data.id == "PHYSICAL FLOW" || node.parent.data.id == "Production Management"))
            this.nodes = this.sortNodesByOrder(this.nodes, sortingorder)
          this.refreshUpdateTree();
        }
    }
  }
  updateNodeById(message: AngularTreeMessageModel) {
    try {
      var node = this.getNodeById(message.treePayload.EntityId);
      if (node == undefined) {
        node = this.getNodeByEntityId(message.treePayload.EntityId);
      }
      if (node) {
        var nodeData = message.treePayload.NodeData;
        if (nodeData && nodeData.children != undefined && nodeData.children.length != 0) {
          node.data.children = nodeData.children;
        }
        for (var key in nodeData) {
          if (key != "children" && nodeData.hasOwnProperty(key)) {

            node.data[key] = nodeData[key];

          }
        }
        if (message.treeName.indexOf(TreeTypeNames.PHYSICALFLOW) == -1 && message.treeName.indexOf(TreeTypeNames.ALL) == -1 ) {
          let newSortList: any;

          node.parent.data.children.sort((firstEntity: any, secondEntity: any) => {
            if (firstEntity && firstEntity["EntityName"] && secondEntity && secondEntity["EntityName"]) {
              if (firstEntity["EntityName"].toLowerCase() > secondEntity["EntityName"].toLowerCase()) {
                newSortList = 1;
              } else {
                newSortList = -1;
              }
            }
            return newSortList;
          });
        }
        this.refreshUpdateTree();

      }
    }
    catch (e) {
      console.error('Exception in updateNodeById() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  moveNodeById(data) {
    try {
      // if anyone changing this method make sure list order functionality should work proper.

      var model: TreeModel = this.tree.treeModel;


      var nodeFrom = model.getNodeBy((node) => node.data.EntityId == data.nodeFrom.EntityId && node.data.Parent && data.nodeFrom.Parent && node.data.Parent[0] && data.nodeFrom.Parent[0] && node.data.Parent[0].EntityId == data.nodeFrom.Parent[0].EntityId);
      var nodeTo = model.getNodeBy((node) => node.data.EntityId == data.nodeTo.EntityId && node.data.Parent && data.nodeTo.Parent[0] && data.nodeTo.Parent && node.data.Parent[0] && node.data.Parent[0].EntityId == data.nodeTo.Parent[0].EntityId);

      if (nodeFrom && nodeTo) {
        let indexDropedNode;
        if (data.listEntityType && nodeTo) {
          indexDropedNode = nodeTo.index;
        }
        else if (nodeTo) {
          indexDropedNode = nodeTo.index + 1;
        }
        model.moveNode(nodeFrom, { parent: nodeTo.parent, index: indexDropedNode, dropOnNode: nodeTo });
        let dragNodeDataParent = nodeFrom.data.Parent[0];
        let droppedNodeDataParent = nodeTo.data.Parent[0];
        let fabricName = this.commonService.getFabricNameByUrl(this.router.url);
        this.tree.treeModel.update();
      }
    }
    catch (e) {
      console.error('Exception in moveNodeById() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  nodeOnDrop(event, node) {
    try {
      this.sendDataToCapabilityTreeComponent("NodeOnDrop", node, event);
    }
    catch (e) {
      console.error('Exception in nodeOnDrop() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  nodeAllowDrop(element) {
    try {
      return true;
    }
    catch (e) {
      console.error('Exception in nodeAllowDrop() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }

  }

  getNodeById(idString: string): TreeNode {
    try {
      if (this.tree && this.tree.treeModel && idString) {
        var model: TreeModel = this.tree.treeModel;
        var node: TreeNode = model.getNodeById(idString);
        return node;
      }
    }
    catch (e) {
      console.error('Exception in getNodeById() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  getNodeByEntityId(idString: string): TreeNode {
    try {
      if (this.tree && this.tree.treeModel && idString) {
        var model: TreeModel = this.tree.treeModel;
        var node: TreeNode = model.getNodeBy((node) => node.data.EntityId == idString);
        return node;
      }
    }
    catch (e) {
      console.error('Exception in getNodeByEntityId() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }

  }
  getAllNodeByEntityId(idString: string): TreeNode[] {
    try {
      if (this.tree && this.tree.treeModel) {
        var model: TreeModel = this.tree.treeModel;
        var nodes = []
        var node: TreeNode = model.getNodeBy((node) => {
          if (node.data.EntityId == idString) {
            nodes.push(node);
          }
        });
        return nodes;
      }
    }
    catch (e) {
      console.error('Exception in getAllNodeByEntityId() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }

  }
  partialCollapse() {
    let expandedNodes = this.tree.treeModel.expandedNodes;
    expandedNodes.forEach(element => {
      element.collapseAll();
    });
  }
  curretFocusedNode(location) {
    try {
      let entityData = location.node.data;
      if (singleClickActive) {
        this.commonService.event = "";
        singleClickActive = false;
      }
      if (this.commonService.event == "drillDown") {
        if (!this.firstlocation && this.tree.treeModel.getFirstRoot() && this.tree.treeModel.getLastRoot()) {
          this.getFirstChildLocation(this.tree.treeModel.getFirstRoot())
          this.getLastChildLocation(this.tree.treeModel.getLastRoot())
        }
        if (this.lastLocation && this.lastLocation.id == this.nextlocationId) {
          this.DeactivateNodeIfOld(location.node.id)
          this.commonService.previousLocationData = this.firstlocation.data;
          // this.commonService.currentNode = this.firstlocation.id;
          return;
        }
        // if (entityData && entityData.children && entityData.children.length != 0 && entityData.TypeOf != undefined && !(this.locations.includes(entityData.TypeOf))) {
        //   if (!this.tree.treeModel.expandedNodeIds[location.node.id]) {
        //     this.tree.treeModel.focusDrillDown()
        //   }
        //   this.tree.treeModel.focusNextNode()
        // }
        else if (entityData && entityData.children && entityData.children.length >= 0 && entityData.TypeOf != undefined && !(this.locations.includes(entityData.TypeOf))) {
          //this.tree.treeModel.focusNextNode()
          if (!this.tree.treeModel.expandedNodeIds[location.node.id]) {
            if (this.tree.treeModel.activeNodeIds[location.node.id] == undefined) {
              this.tree.treeModel.focusNextNode()
            }
            else {
              this.tree.treeModel.focusDrillDown()
            }
          }
          this.DeactivateNodeIfOld(location.node.id)
          this.commonService.previousLocationData = location.node.data;
          // this.commonService.currentNode = location.node.id;
        }
        else if (entityData && entityData.children && entityData.children.length != 0 && entityData.TypeOf == undefined && entityData.TypeOf != "Shared" && this.commonService.queryParamsList.tab == TreeTypeNames.LIST) {
          if (!this.tree.treeModel.expandedNodeIds[location.node.id]) {
            this.tree.treeModel.focusDrillDown()
          }
          this.tree.treeModel.focusNextNode()
        }
        else if (entityData && entityData.children && entityData.children.length == 0 && entityData.TypeOf == undefined && entityData.TypeOf != "Shared" && this.commonService.queryParamsList.tab == "Lists") {
          this.tree.treeModel.focusNextNode()
        }
        else if (this.peoples.includes(entityData.EntityType) || this.locations.includes(entityData.TypeOf) || (entityData.TypeOf == undefined || this.commonService.queryParamsList.tab == "Lists")) {
          if (!this.tree.treeModel.expandedNodeIds[location.node.id] && location.node.data.children && location.node.data.children.length != 0) {
            if (this.tree.treeModel.activeNodeIds[location.node.id] == undefined) {
              this.tree.treeModel.focusNextNode()
            }
            else {
              this.tree.treeModel.focusDrillDown()
            }
          }
          this.DeactivateNodeIfOld(location.node.id)
          this.commonService.previousLocationData = location.node.data;
          // this.commonService.currentNode = location.node.id;
        }
        else {
          if (entityData.TypeOf == undefined) {
            this.commonService.previousLocationData = location.node.data;
          }
        }

      }
      else if (this.commonService.event == "drillUp") {
        if (!this.lastLocation && this.tree.treeModel.getFirstRoot() && this.tree.treeModel.getLastRoot()) {
          this.getFirstChildLocation(this.tree.treeModel.getFirstRoot())
          this.getLastChildLocation(this.tree.treeModel.getLastRoot())
        }
        if (this.firstlocation && this.firstlocation.id == this.nextlocationId && this.tree.treeModel.focusedNodeId != this.firstlocation.id) {
          this.DeactivateNodeIfOld(location.node.id)
          this.commonService.previousLocationData = this.lastLocation.data;
          // this.commonService.currentNode = this.lastLocation.id;
          return;
        }
        // else if(entityData.TypeOf&&!this.locations.includes(entityData.TypeOf) && location.node.data.children && location.node.data.children.length != 0&&!this.tree.treeModel.expandedNodeIds[location.node.id]) {
        //      this.getLastTreeChild(location.node.id)
        //    }
        else if (entityData.TypeOf && !this.locations.includes(entityData.TypeOf)) {
          if (!this.tree.treeModel.expandedNodeIds[location.node.id] && location.node.data.children && location.node.data.children.length != 0) {
            this.getLastTreeChild(location.node.id)
          }
          else {
            //   this.tree.treeModel.focusPreviousNode()
            this.DeactivateNodeIfOld(location.node.id)
            this.commonService.previousLocationData = location.node.data;
            if (this.commonService.previousLocationData.EntityType != EntityTypes.Personal && this.commonService.previousLocationData.EntityType != EntityTypes.List && this.commonService.previousLocationData.EntityType != EntityTypes.Shared)
              this.commonService.listPreviousLocation = this.commonService.previousLocationData;
            // this.commonService.currentNode = location.node.id;
            this.nextlocationId = location.node.id
          }
        }
        else if ((this.locations.includes(entityData.TypeOf)) && entityData.children && entityData.children.length != 0 && !this.tree.treeModel.expandedNodeIds[location.node.id]) {
          this.getLastTreeChild(location.node.id);
        }
        else if (entityData.children && entityData.children.length != 0 && entityData.TypeOf == undefined && entityData.TypeOf != "Shared" && this.commonService.queryParamsList.tab == "Lists") {
          if (!this.tree.treeModel.expandedNodeIds[location.node.id] && location.node.data.children && location.node.data.children.length != 0) {
            this.getLastTreeChild(location.node.id)
          }
          else {
            this.tree.treeModel.focusPreviousNode()
          }
        }
        else if (entityData && entityData.children && entityData.children.length == 0 && entityData.TypeOf == undefined && entityData.TypeOf != "Shared" && this.commonService.queryParamsList.tab == "Lists") {
          this.tree.treeModel.focusPreviousNode()
        }
        else if (this.peoples.includes(entityData.EntityType)) {
          if (!this.tree.treeModel.expandedNodeIds[location.node.id] && location.node.data.children && location.node.data.children.length != 0) {
            this.getLastTreeChild(location.node.id)
          }
          else {
            this.DeactivateNodeIfOld(location.node.id)
            this.commonService.previousLocationData = location.node.data;
            // this.commonService.currentNode = location.node.id;
            this.nextlocationId = location.node.id
          }
        }
        else if (this.locations.includes(entityData.TypeOf) || (entityData.TypeOf == undefined && this.commonService.queryParamsList.tab == TreeTypeNames.LIST)) {
          this.DeactivateNodeIfOld(location.node.id)
          this.commonService.previousLocationData = location.node.data;
          if (this.commonService.previousLocationData.EntityType != EntityTypes.Personal && this.commonService.previousLocationData.EntityType != EntityTypes.List && this.commonService.previousLocationData.EntityType != EntityTypes.Shared)
            this.commonService.listPreviousLocation = this.commonService.previousLocationData;
          // this.commonService.currentNode = location.node.id;
          this.nextlocationId = location.node.id
        }
        else {
          if (entityData.TypeOf == undefined) {
            this.commonService.previousLocationData = location.node.data;
          }
        }

      }
      else if (location.node && entityData && entityData.TypeOf) {
        this.commonService.previousLocationData = location.node.data;
        this.getFirstChildLocation(this.tree.treeModel.getFirstRoot())
        this.getLastChildLocation(this.tree.treeModel.getLastRoot())
      }

    }
    catch (e) {
      console.error("error in curretFocusedNode" + e)
    }
  }
  DeActiveSelectedNode(message?: AngularTreeMessageModel) {
    try {
      if (this.tree && this.tree.treeModel)
        var treeNode = this.tree.treeModel.getFocusedNode();
      // if (treeNode && treeNode.isActive) {
      if (treeNode) {
        treeNode.toggleActivated()
        this.tree.treeModel.getFocusedNode().blur()
      }
    }
    catch (e) {
      console.error('Exception in DeActiveSelectedNode() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }

  }

  toggleExpandedEvent(event: any) {
    try {
      if (this.expanderClickEventRestrict)
        this.sendDataToCapabilityTreeComponent("expanderClick", event.node);
    }
    catch (e) {
      console.error('Exception in toggleExpandedEvent() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  createContextmenue(currentThis) {
    try {
      var dd = this.elementRef.nativeElement.querySelector('.capMenu');
      if (dd && dd != null) {
        dd.style.left = AngularHierarchyTreeComponent.clientXValue + "px";
        dd.style.top = AngularHierarchyTreeComponent.clientYValue + "px";
        this.trigger.openMenu();
        this.cdRef.detectChanges();
        document.getElementsByClassName('cdk-overlay-backdrop')[0].addEventListener('contextmenu', (offEvent: any) => {
          offEvent.preventDefault();
          this.trigger.closeMenu();
        });
      }
    }
    catch (e) {
      console.error('Exception in createContextmenue() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }



  DeactivateNodeIfOld(id) {
    var treeNodes = this.tree.treeModel.getActiveNodes()

    if (treeNodes.filter(d => d.id == id).length == 0) {
      treeNodes.forEach(element => {
        if (this.peoples.includes(element.data.EntityType) || this.locations.includes(element.data.TypeOf)) {
          element.toggleActivated()
        }
      });
    }
  }

  getLastTreeChild(id) {
    var model: TreeModel = this.tree.treeModel;
    var node: TreeNode = model.getNodeById(id);
    node.expand()
    var a = node.getLastChild();
    if (!a && (this.locations.includes(node.data.TypeOf) || !this.locations.includes(node.data.TypeOf) || node.data.TypeOf == undefined || this.peoples.includes(node.data.EntityType))) {

      this.DeactivateNodeIfOld(id);

      this.commonService.previousLocationData = node.data;
      // this.commonService.currentNode = node.id;
      this.nextlocationId = node.id
    }
    if (a) {
      this.getLastTreeChild(a.id)
    }
  }


  firstlocation;
  nextlocationId;
  lastLocation;
  sendDataToCapabilityTreeComponent(treeOperationType: string, treePayload: any, other?: any) {
    try {
      if (other) {
        AngularHierarchyTreeComponent.clientXValue = other.x;
        AngularHierarchyTreeComponent.clientYValue = other.y;
      }

      switch (treeOperationType) {
        case 'NodeOnContextClickBeforeInit':
          {
            this.tempCallBackFunction = function () {
              return { 'selectedNode': treePayload, 'currentElement': other }
            }
          }
          break;
        case 'NodeOnDrop': {
          var otherPayload = {
            'fromTreeName': other.element.treeModel.options.rootId,
            'DraggedNode': other.element,
            'DroppedOntopNode': treePayload
          }
          treePayload = otherPayload;
        } break;
      }

      var message: AngularTreeMessageModel = {
        "fabric": this.commonService.lastOpenedFabric,
        "treeName": [this.HierarchyTreeName],
        "treeOperationType": treeOperationType,
        "treePayload": treePayload
      };
      if (treeOperationType == 'NodeOnClick') {
        // if(this.ActiveTreeNode&&this.ActiveTreeNode.data.EntityId!=treePayload.data.EntityId&&!treePayload.parent.isExpanded&&!this.searchValue)
        //   this.partialCollapse();
        // this.commonService.latlngFinderDivision = false;
        // this.commonService.fdcCollabChannelswicthing$ = true;
        var treeNode = this.tree.treeModel.getFocusedNode();
        if (!treeNode.isActive) {
          treeNode.toggleActivated()
        }
      }
      // else {
      //   this.commonService.fdcCollabChannelswicthing$ = false;
      // }
      this.EmmitTreeData.next(message);
      // this.commonService.nodehighlight1 = message;
      // this.commonService.controlZoomControl = true;
    }
    catch (e) {
      console.error('Exception in sendDataToCapabilityTreeComponent() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }

  }
  getFirstChildLocation(dist) {
    try {
      var temp = dist.getFirstChild()
      if (temp) {
        this.getFirstChildLocation(temp)
      }
      if (temp && temp.data && this.locations.includes(temp.data.TypeOf)) {
        this.firstlocation = temp;
      }
    }
    catch (e) {
      console.warn("first location doesnt exist");
    }
  }
  getLastChildLocation(dist) {
    try {
      var temp = dist.getLastChild()
      if (temp) {
        this.getLastChildLocation(temp)
      }
      if (temp && temp.data && this.locations.includes(temp.data.TypeOf)) {
        this.lastLocation = temp;
      }
    }
    catch (e) {
      console.warn("last location doesnt exist")
    }
  }
  onEvent(event: any) {
    this.cdRef.detectChanges();
  }

  onInitialized(tree: any) {
    try {
      this.tree = tree;
    }
    catch (e) {
      console.error('Exception in onInitialized() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }

  }

  contextOptionClick($event) {
    try {
      this.commonService.latlngFinderDivision = false;
      var payload = this.tempCallBackFunction();
      var ContextpayLoad = {
        "nodeData": payload.selectedNode,
        "optionSelected": $event.displayName,
        "selectedContextMenuOption": $event
      }
      this.sendDataToCapabilityTreeComponent('NodeContextOptionClicked', ContextpayLoad)
      this.cdRef.detectChanges();
    }
    catch (e) {
      console.error('Exception in contextOptionClick() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }

  }

  Bigdatalazyloading(node: any) {
    try {
      if (node.children.length == 1 && node.children != null) {
        var data = { "EntityId": node.data.EntityId, "EntityName": node.data.EntityName, "EntityType": node.data.EntityType, "SpaceName": node.data.SpaceName };
        var payload = JSON.stringify(data);
        this.commonService.calllazyloading(payload);

      }
    }
    catch (e) {
      console.error('Exception in Bigdatalazyloading() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  getIcon(EveData) {
    try {
      var entityType = EveData.NodeEntityType;
      if (EveData.fullNode.WellType == 'Effluent' && entityType == EntityTypes.GASWELL) {
        return 'EffluientGasWellTreeIcon';
      } else if (entityType == 'DowntimeReason') {
        // this condition did not remove because DowntimeReasonTreeIcon is not there
        return;
      }
      else {
        return entityType + "TreeIcon";
      }
    }
    catch (e) {
      console.error('Exception in getIcon() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }


  // getColor(d) {
  //   try {
  //     let color = "red";
  //     if (d.formValidate == undefined) {
  //       color = "transparent"
  //     }
  //     else if (d.formValidate) {
  //       color = 'none';
  //     }
  //     else {
  //       color = "red"
  //     }
  //     return color;
  //   }
  //   catch (e) {
  //     console.error('Exception in getColor() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
  //   }
  // }

  getAngularTreeEvent(model: AngularTreeEventMessageModel) {
    try {
      let treeDataEventResponseModel: TreeDataEventResponseModel = {
        fabric: model.fabric,
        treeName: [this.HierarchyTreeName],
        treePayload: ''
      }
      switch (model.treeOperationType) {
        case "GetTreeInstance": {
          treeDataEventResponseModel.treePayload = this.tree
        }
          break;
        case "getNodeById":
          treeDataEventResponseModel.treePayload = this.getNodeByEntityId(model.treePayload)
          break;
        case TreeOperations.ClearSearch:
          this.TreeFilterByText(model);
          break;
        default:
          break;
      }
      (<EventEmitter<any>>model.Event).next(treeDataEventResponseModel);

    }
    catch (e) {
      this.commonService.appLogException(new Error(e));
    }
  }

  menuClose() {
    if (this.commonService.activeContextMenuNodeId && this.commonService.activeContextMenuNodeId != "" && document.getElementById(this.commonService.activeContextMenuNodeId) != undefined)
      document.getElementById(this.commonService.activeContextMenuNodeId).style.removeProperty("background");
    this.commonService.activeContextMenuNodeId = "";
  }

  clickOutSideTreeBody() {
    if (this.isShiftAndCtrlSelectedNodeAvailable) {
      let treeName = this.commonService.getTreeTabBaseOnUrl(this.router.url);
      if (treeName && treeName == this.HierarchyTreeName) {
        this.deActiveShiftCtrlSelectedNode();
        this.isShiftAndCtrlSelectedNodeAvailable = false;

      }
    }
  }

  deActiveShiftCtrlSelectedNode() {
    try {
      let entityId = "";
      let routerUrl = this.router.url;
      if (this.tree && this.tree.treeModel) {
        var treeNodes = this.tree.treeModel.getActiveNodes();
        if (treeNodes && treeNodes.length != 0) {
          treeNodes.forEach((node) => {
            if (node && node.isActive) {
              if (routerUrl && node.data && !routerUrl.includes(node.data.EntityId)) {
                node.toggleActivated();
                //TREE_ACTIONS.DEACTIVATE(this.tree, node,null);
                //console.log("routerurl includes");
                //console.log(routerUrl.includes(node.data.EntityId));
                this.tree.treeModel.getFocusedNode().blur();

              } else {
                entityId = node.data.EntityId;
              }
            }
          })
        }
      }

      //activate selected node
      if (entityId != "") {
        this.ActivateEntityId(entityId);
      }
      else {
        this.highlightRouterEntityId();
      }
    }
    catch (e) {

    }
  }
  highlightRouterEntityId() {
    this.route.queryParams.pipe(this.compUntilDestroyed()).subscribe((params: any) => {
      if (params && params.EntityId) {
        this.ActivateEntityId(params.EntityId);
      }
    })
  }

  ActivateEntityId(entityId) {
    var node = this.getNodeByEntityId(entityId);
    if (node) {
      this.ActiveTreeNode = node;
      node.setActiveAndVisible(true);
    }
  }

  deactivateAllActiveNodes(message: AngularTreeMessageModel) {
    try {
      if (this.tree && this.tree.treeModel) {
        var treeNodes = this.tree.treeModel.getActiveNodes();
        if (treeNodes && treeNodes.length != 0) {
          treeNodes.forEach((node) => {
            if (node && node.isActive) {
              node.toggleActivated()
              this.tree.treeModel.getFocusedNode().blur();
            }
          })
        }
      }

    }
    catch (e) {
      console.error('Exception in deactivateAllActiveNodes() of angular-hierarchy-tree.component in angular-hierarchy-tree at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }

  }

  callshiftclick(tree, node, $event) {
    if (!$event.shiftKey) {
      this.from = node.index;
    }
    if ($event.shiftKey) {  //click with Shift
      this.isShiftAndCtrlSelectedNodeAvailable = true;
      for (var index = 0; index < this.previousnodes.length; index++) {
        TREE_ACTIONS.DEACTIVATE(tree, this.previousnodes[index], $event);
      }
      this.to = node.index
      if (this.previousid != node.parent.data.uuid) {
        this.from = 0;
        this.previousid = node.parent.data.uuid;
      }
      var nodes = node.parent ? node.parent.children : tree.roots
      var start = this.from < this.to ? this.from : this.to
      var end = this.from > this.to ? this.from : this.to
      for (var index = start; index <= end; index++) {
        var hiddenNodes = tree.hiddenNodes
        var hflag = hiddenNodes.length ? (hiddenNodes.findIndex(hnode => hnode.data && nodes[index].data && hnode.data.EntityId == nodes[index].data.EntityId) == -1) : true;
        this.previousnodes = [];
        this.activateChildNode(tree,nodes[index],hflag,$event);
      }

    }
    else if ($event.ctrlKey) { //click with CTRL
      this.isShiftAndCtrlSelectedNodeAvailable = true;

      TREE_ACTIONS.TOGGLE_ACTIVE_MULTI(tree, node, $event)

      if (node.isActive) {
        this.previousnodes.push(node);
      }
      this.previousid = node.parent.data.uuid;
    }
    else {
      TREE_ACTIONS.ACTIVATE(tree, node, $event);
      if (node.isActive) {
        this.previousnodes.push(node);
      }
      this.previousid = node.parent.data.uuid;
    }
  }
  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables);
  }

  activateChildNode(tree,nodes,hflag,$event){
    
      if (!nodes.isActive && hflag) {
        TREE_ACTIONS.TOGGLE_ACTIVE_MULTI(tree, nodes, $event);
      }
      if (nodes.isActive) {
        this.previousnodes.push(nodes);
      }
      
      if(nodes.children){
        for(var node of nodes.children){
        this.activateChildNode(tree,node,hflag,$event)
        }
      }
  
  }

  onDragStart(event,nodes) {
    //logic to bind tree data to form drop time
    if(this.dropDataPointer && AppConsts.DropTreeNameList.indexOf(this.HierarchyTreeName) >= 0){
     let rows = [];
     let selectedNodes = getSelectedNodes(nodes)
     selectedNodes.forEach(selectedNode =>{
       formateTreeDataToTable(selectedNode,rows);
     });
     let droppedRowsMapping = JSON.parse( JSON.stringify(AppConsts.droppedRowsMapping))
     droppedRowsMapping.forEach(item =>{
      item.dropDataPointer = this.dropDataPointer;
     });
     let eventdata = JSON.stringify({'rows':rows,'dropDataPointers':this.dropDataPointer,index:'',droppedRowsMapping:droppedRowsMapping})
     event.dataTransfer.setData('data', eventdata);
    }
    else{
      this.dropDataPointer = '';
    }
  }

  onDragEnd(event,nodes) {
    if(this.dropDataPointer && AppConsts.DropTreeNameList.indexOf(this.HierarchyTreeName) >= 0){
       this.DeActiveSelectedNode();     
    }
  }
 
  ngOnDestroy() {

    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }
}
