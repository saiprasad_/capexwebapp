import React, { useRef, useState } from "react";
import { AiOutlineFile, AiOutlineDelete, AiOutlineEdit } from "react-icons/ai";
import Checkbox from '@material-ui/core/Checkbox';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, makeStyles } from '@material-ui/core';
import { StyledFile } from "../File/TreeFile.style";
import { useTreeContext } from "../state/TreeContext";
import { ActionsWrapper, StyledName } from "../Tree.style.js";
import { PlaceholderInput } from "../TreePlaceholderInput";

import { FILE, FOLDER } from "../state/constants";
import FILE_ICONS from "../FileIcons";

const File = ({ name, id, node }) => {
  const { dispatch, isImparative, onNodeClick, deleteFile, fileEdit } = useTreeContext();
  const [isEditing, setEditing] = useState(false);
  const [checked, setChecked] = React.useState(false);
  const [popupOpen, setPopupOpen] = useState(false);

  const ext = useRef("");

  let splitted = name?.split(".");
  ext.current = splitted[splitted.length - 1];

  const toggleEditing = () => setEditing(!isEditing);
  const setSelection = (node) => {
    node.checked=node.checked?false:true;
    setChecked(node.checked)
  }
  const commitEditing = (name) => {
    dispatch({ type: FILE.EDIT, payload: { id, name } });
    let data = {"id": id, "type": FILE.EDIT, "name": name,parent:node.parent}
    setEditing(false);
    fileEdit(data);
  };
  const commitDelete = () => {
    setPopupOpen(false)
    dispatch({ type: FILE.DELETE, payload: { id } });
    let data = {"id" : id, "type": FOLDER.DELETE, "name": name} 
    deleteFile(data)
  };
  const handleNodeClick = React.useCallback(
    (e) => {
      e.stopPropagation();
      onNodeClick({ node });
    },
    [node]
  );
  const handleCancel = () => {
    setEditing(false);
  };

  const popUpOpen = () => {
    setPopupOpen(true)  
  }
  const handleClose = () => {
    setPopupOpen(false)
  }
  return (
    <StyledFile onClick={handleNodeClick} className="tree__file">
      <Checkbox
        checked={checked}
        onChange={() => setSelection(node)}
        color="default"
          />
      {isEditing ? (
        <PlaceholderInput
          type="file"
          style={{ paddingLeft: 0 }}
          defaultValue={name}
          onSubmit={commitEditing}
          onCancel={handleCancel}
        />
      ) : (
        <ActionsWrapper>
          
          <StyledName>
            {FILE_ICONS[ext.current] ? (
              FILE_ICONS[ext.current]
            ) : (
              <AiOutlineFile />
            )}
            &nbsp;&nbsp;{name}
          </StyledName>
          { (
            <div className="actions">
              <AiOutlineEdit onClick={toggleEditing} />
              <AiOutlineDelete onClick={popUpOpen} />
            </div>
          )}
          <Dialog
            open = {popupOpen}
            onClose={handleClose}>
            <DialogTitle id="alert-dialog-title" style={{width: 468}}>{"Attention!!"}</DialogTitle>
            <DialogContent style={{width: 468}}>
              <DialogContentText id="alert-dialog-description">
              Are you sure you want to delete {name}
              </DialogContentText>
            </DialogContent>
            <DialogActions style={{width: 500}}>
              <Button onClick={handleClose} color="primary">Disagree</Button>
              <Button onClick={commitDelete} color="primary">Agree</Button>
            </DialogActions>
          </Dialog>
        </ActionsWrapper>
      )}
    </StyledFile>
  );
};

export { File };
