import React, { useState, useEffect } from "react";
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, makeStyles } from '@material-ui/core';
import {
  AiOutlineFolderAdd,
  AiOutlineFileAdd,
  AiOutlineFolder,
  AiOutlineFolderOpen,
  AiOutlineDelete,
  AiOutlineEdit,
} from "react-icons/ai";

import {
  ActionsWrapper,
  Collapse,
  StyledName,
  VerticalLine,
} from "../Tree.style";
import { StyledFolder } from "./TreeFolder.style";

import { FILE, FOLDER } from "../state/constants";
import { useTreeContext } from "../state/TreeContext";
import { PlaceholderInput } from "../TreePlaceholderInput";

const FolderName = ({ isOpen, name, handleClick ,handleFilePath}) => (
  <StyledName >
   <span onClick={handleClick}>{isOpen ? <AiOutlineFolderOpen /> : <AiOutlineFolder />}</span> 
    &nbsp;&nbsp;<span onClick={handleFilePath}>{name }</span>
  </StyledName>
);

const Folder = ({ id, name, children, node }) => {
  const { dispatch, isImparative, onNodeClick, folderCreation, fileCreating, deleteFolder, editFolderName } = useTreeContext();
  const [isEditing, setEditing] = useState(false);
  const [isOpen, setIsOpen] = useState(true);
  const [childs, setChilds] = useState([]);
  const [popupValue, setPopupValue] = useState(false);

  useEffect(() => {
    setChilds([children]);
  }, [children]);

  const commitFolderCreation = (name) => {
      dispatch({ type: FOLDER.CREATE, payload: { id, name } });
      node["name"] = name;
      node["type"] = FOLDER.CREATE
      folderCreation({node})
   };
  const commitFileCreation = (name) => {
    dispatch({ type: FILE.CREATE, payload: { id, name } });
    fileCreating({name})
  };
  const commitDeleteFolder = () => {
    setPopupValue(false)
    dispatch({ type: FOLDER.DELETE, payload: { id } })    
    let data = {"id" : id, "type": FOLDER.DELETE, "name": name}    
    deleteFolder(data)
  };
  const commitFolderEdit = (name) => {
    dispatch({ type: FOLDER.EDIT, payload: { id, name } });
    let data = {"id" : id, "type" : FOLDER.EDIT, "name" : name,parent:node.parent}
    setEditing(false);
    editFolderName(data);
  };

  const handleCancel = () => {
    setEditing(false);
    setChilds([children]);
  };

  const handleNodeClick=()=> {
      onNodeClick({ node });
    }

  const handleFileCreation = (event) => {
    event.stopPropagation();
    setIsOpen(true);
    setChilds([
      ...childs,
      <PlaceholderInput
        type="file"
      onSubmit={commitFileCreation}
        onCancel={handleCancel}
      />,
    ]);
  };

  const handleFolderCreation = (event) => {
    event.stopPropagation();
    setIsOpen(true);
    setChilds([
      ...childs,
      <PlaceholderInput
        type="folder"
        onSubmit={commitFolderCreation}
        onCancel={handleCancel}
      />,
    ]);
  };

  const handleFolderRename = () => {
    setIsOpen(true);
    setEditing(true);
  };
  const popUpOpen = (state) => {
    setPopupValue(true)  
  }
  const handleClose = (state) => {
    setPopupValue(false)
  }
  return (
    <StyledFolder id={id}  className="tree__folder">
      <VerticalLine>
        <ActionsWrapper>
          {isEditing ? (
            <PlaceholderInput
              type="folder"
              style={{ paddingLeft: 0 }}
              defaultValue={name}
              onCancel={handleCancel}
              onSubmit={commitFolderEdit}
            />
          ) : (
            <FolderName
              name={name}
              isOpen={isOpen}
              handleClick={() => setIsOpen(!isOpen)}
              handleFilePath = { handleNodeClick}
            />
          )}

          {isImparative && (
            <div className="actions">
              <AiOutlineEdit onClick={handleFolderRename} />
              {/* <AiOutlineFileAdd onClick={handleFileCreation} /> */}
              <AiOutlineFolderAdd onClick={handleFolderCreation} />
              <AiOutlineDelete onClick={popUpOpen} />
            </div>
          )}
          <Dialog
            open = {popupValue}
            onClose={handleClose}>
            <DialogTitle id="alert-dialog-title" style={{width: 468}}>{"Attention!!"}</DialogTitle>
            <DialogContent style={{width: 468}}>
              <DialogContentText id="alert-dialog-description">
                Are you sure you want to delete {name}
              </DialogContentText>
            </DialogContent>
            <DialogActions style={{width: 500}}>
              <Button onClick={handleClose} color="primary">Disagree</Button>
              <Button onClick={commitDeleteFolder} color="primary">Agree</Button>
            </DialogActions>
          </Dialog>
        </ActionsWrapper>
        <Collapse className="tree__folder--collapsible" isOpen={isOpen}>
          {childs}
        </Collapse>
      </VerticalLine>
    </StyledFolder>
  );
};

export { Folder, FolderName };
