//external imports
import { DOCUMENT } from '@angular/common';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { EventEmitter, Inject, Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { TreeModel, TreeNode } from '@circlon/angular-tree-component';
import countryTimezone from 'countries-and-timezones';
import * as localForage from 'localforage';
import { CookieService } from 'ngx-cookie';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { fromWorker } from 'observable-webworker';
import { from, fromEvent, Observable, Observer, of, Subject, throwError, observable, combineLatest } from 'rxjs';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/buffer';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/shareReplay';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toArray';
import { catchError, filter, map, retry, switchMap } from 'rxjs/operators';
import { WORKER_TOPIC } from '../../../../ClientApp/webWorker/app-workers/shared/worker-topic.constants';
//state modes
import { UserStateConstantsKeys } from '../../../webWorker/app-workers/commonstore/user/user.state.model';
//state queries
import { UsersStateQuery } from '../../../webWorker/app-workers/commonstore/user/user.state.query';
import { DEFAULT_REQUEST_OPTIONS, RequestResult } from '../../../webWorker/app-workers/http/httpRequest';
import { WorkerMessage } from '../../../webWorker/app-workers/shared/worker-message.model';
//internal services imports
import { AppInsightsService } from '../common/app-insight/appinsight-service';
import { AppConsts, SourceSystem } from '../common/Constants/AppConsts';
import { SESSION_EXPIRE_MESSAGE } from '../common/Constants/AttentionPopupMessage';
import { CommonJsMethods } from '../common/library/CommonJavaScriptMethods';
import { LoadingBarService } from '../common/ngx-loading-bar/LoadingBarService';
import { AuthStateModel } from '../common/state/Auth/auth.state.model';
import { AuthCommonStoreQuery } from '../common/state/Auth/auth.state.query';
import { StateRecords } from '../common/state/common.state';
import { PeopleCompanyTypes } from '../common/state/CompanyTypes/companytype.state.model';
import { EntityCompanyTypesStoreQuery } from '../common/state/CompanyTypes/companytype.state.query';
import { EntityCommonStoreQuery } from '../common/state/entity.state.query';
import { EntityForm } from '../common/state/entityforms/form.model';
import { FormStateQuery } from '../common/state/entityforms/form.query';
import { EntityHeaderContentSequence, FDCStateConstantsKeys, HeaderContentSequence, TabletKeyPadState } from '../common/state/KeyValueState/fdc.state.model';
import { FDCStateQuery } from '../common/state/KeyValueState/fdc.state.query';
import { ObservableStoreService } from '../common/state/observables/observables.entity.state.query';
import { SchemaEntityCommonStoreQuery } from '../common/state/schemas/schema.entity.state.query';
//Appconsts and classes imports
import { AngularTreeEventMessageModel, AngularTreeMessageModel, CAPABILITY_NAME, Datatypes, FABRICS, FabricsNames, FabricsPath, GetDesktopDependentComponentInstanceModel, GetServiceInstanceEventModel, GetServiceInstanceMessage, IPInfo, MessageKind, ObservableStateMessage, RightSideClickTreeEventModel, SendToComponentMyFormEvent, SERVICETYPE, Tabs, TreeAndNodeEventModel, TreeDataEventResponseModel, TreeOperations, TreeTypeNames, TypeOfEntity, TypeOfForm } from '../globals/Model/CommonModel';
import { MessageModel, MessageType, Routing, IModelMessage } from '../globals/Model/Message';
import { UUIDGenarator } from 'visur-angular-common';
import { EntityTypes, ITreeConfig } from './Model/EntityTypes';
import { FabricRouter } from './Model/FabricRouter';
import { AppConfig } from './services/app.config';
import { DataService, HttpMethod } from './services/data.service';
import { isCapabilityAdmin, isValidJSON } from './helper.functions';
import {ApplicationState} from '../common/Constants/application.state';

@Injectable()
export class CommonService {
  /**
   * ######collections variable declaration block
   *
   */
  private httpKeyValue: Map<string, Observer<Object>> = new Map<string, Observer<Object>>();

  /**
   * #####Observables and Subject declaration block
   */
  private online$ = Observable.fromEvent(window, 'online');
  private offline$ = Observable.fromEvent(window, 'offline');
  onSessionExpirePopupAlert$ = new Subject<any>();
  updateRightSideHeader$ = new Subject<any>();
  opendialogboxObs = new Subject<any>();
  enablerightTreeTab = new Subject<any>();
  rightTreeClick: Subject<RightSideClickTreeEventModel> = new Subject<RightSideClickTreeEventModel>();
  changeTheme = new Subject<any>();
  HomeFabricSettingJson = new Subject<any>();
  updateLayout = new Subject<any>();
  public physicalInstanes$: Subject<any> = new Subject<any>();
  appLeftTree$ = new Subject();//move to biservice
  publishedToCloseTheDeletedFrom$ = new Subject();

  public SendSuccessCall$ = new Subject();//remove and move to fabricservice

  public openListTab = new Subject();
  public resetControl = new Subject();

  sendDataToAngularTree = new Subject<AngularTreeMessageModel>();
  visurUser = new Subject<AngularTreeMessageModel>();
  sendDataFromAngularTreeToFabric = new Subject<AngularTreeMessageModel>();
  // deleteContextData = new Subject();
  // public WelltypeMeasuredTested$ = new Subject();
  public crudResponse = new Subject();
  selectedNode = new Subject(); //move to bi service

  addNew$ = new Subject();
  menuFavIconsHide = new Subject();
  public scrollup = new Subject();
  public doActivity$ = new Subject();//future modification

  public closeAllPopupForms$ = new Subject();
  public AddNewSecurityRoleOrGroup$ = new Subject();
  CurrentCapabilityId;
  public userSettingForm = new Subject();

  public addNewEntity$ = new Subject();
  public DBFailErrorHandling$: Subject<any> = new Subject<any>();
  public UpdateDataToFabric = new Subject<any>();

  public sendMessagesToFabricServices = new Subject();
  public triggerDetectChangesGlobalAdminUtil$ = new Subject();

  recalculateHistory$: Subject<any> = new Subject<any>(); //change to input
  public getAngularTreeEvent$ = new Subject();
  // public UpdateControlsData = new Subject();
  public zoomApplicationForTabletMode$ = new Subject<any>();
  public subheadertoggleMode$ = new Subject<any>();
  public getComponentMyFormEvent$ = new Subject<SendToComponentMyFormEvent>();
  public getServiceInstanceEvent$ = new Subject<GetServiceInstanceEventModel>();
  public getCurrentNgSelectUtil$ = new Subject();
  public onAddNewOpenedFormContextMenuCreateEntity$ = new Subject();
  public ShowPermissionTree$: Subject<any> = new Subject<any>();
  public userProfileMenuRefreshOnBrowserBackFowardButton = new Subject();
  public changeSecurityLeftTreeBasedOnMode = new Subject();
  public listOtherSessionCRUDOperation = new Subject();
  public updateSharedListUser$ = new Subject();
  public permissionListTab$ = new Subject();
  public permissionEntitiesTab$ = new Subject();
  public listCRUDOperations = new Subject();
  public LastSyncTime: Date = new Date();
  public showlist = new Subject();
  public addnewobservable$ = new Subject();
  public triggerSubmitOnFormGenerator$ = new Subject();
  deleteContextData = new Subject();
  public formGenDataForCreateUpdate$ = new Subject();
  public emFormSubmitEvent = new Subject();
  public headerExpandCollapseEvent = new Subject();

  /**
   * #####Boolean variable declaration block
   *
   */
  //isCreateDestroyLeftTree: boolean = true;
  // multiDateViewContent: boolean = true;
  isMultiGridView: boolean = false;
  entityExist: boolean = false;
  subHeaderDisable: boolean = false;
  tabletMode: boolean = false;
  // tabletPadOutSideClick:boolean=false;
  // isUserSettingFormTabletMode: boolean = false;
  tableButtonMode: boolean = false;
  // isMinimizeKeypad: boolean = true;
  tabletKeyPadMode: boolean = false;
  sideHeaderView = 'Default';
  DataSourceSelection;
  CreatenewList: boolean = false;
  deletedEntityName: string = "";
  // OpFormRightPannel: boolean = false;
  SkipSurePopUp: boolean = false;
  // historyFdcChatmessage: boolean = false;
  // shareformEnable: boolean = false;
  // isNewFFVGroup: boolean = false;
  // isFFVGroupValidation: boolean = false;
  // isNewScheduleGroup: boolean = false;
  isUserSettingForm: boolean = true;
  isSubHeaderClick: boolean = false;
  islistformValidation: boolean = false;
  isFormModified: boolean = false;
  isLeftHeaderClick: boolean = false;
  rightreeCollapse: boolean = false;
  themeDark: boolean = false;
  // deleteTruckTicket: boolean = false;
  // EfmIconActive = false;
  // isExistingURL: boolean = false;
  // listgaswellloaderdisbale: boolean = true;
  // messageUpdated: boolean = false;
  public isRefreshedUrl = false;
  public navigateMutualFabric = false;
  isSubHeaderEnable: boolean = false;
  isLeftTreeEnable: boolean = false;
  isRightTreeEnable: boolean = false;
  isLeftHeaderActive: boolean = true;
  // isTimeLineShow: boolean = true;
  filteredDataNotFound: boolean = false;
  isContextMenuHeader: boolean = false;
  isUserMenuHeader: boolean = false;
  //using in addnewentitydetails
  isclose: boolean = false;
  // isdesktopMenuHeader: boolean = false;
  // isDeletingData: boolean = false;
  // isSwitch: boolean = true;
  isMultiView: boolean = false;
  isCreateNewEntity: boolean = false;
  IsEntityExistEMCapability: any = {};
  latlngFinderDivision: boolean = false;
  // signinin: boolean = false;
  // isQaSqliteDb: boolean = false;
  public onlineOffline: boolean = navigator.onLine;
  // isfilemenu: boolean = false;
  // iseditmenu: boolean = false;
  // isviewmenu: boolean = false;
  // ishistorymenu: boolean = false;
  // ishelpmenu: boolean = false;
  // desktopprogressbar: boolean = false;
  // desktoppDownloading: boolean = true;
  // AddTemp = false;
  addanalysisform: boolean = false;
  // controlZoomControl = false;
  sessionSucess: boolean = false;
  Addformclose: boolean = false;
  isPlusAddForm: boolean = false;
  popupformclose: boolean = false;
  isNewEntityType = true;
  logoutbtn: boolean = false;
  // Abandoned = false;
  // Suspended = false;
  // Active = false;
  inputselected = false;
  // isTabbuttonClick = false;
  searchActive = false;
  showRightsideBar = true;
  FabricIdInSecurityFabric;
  createbyContextmenu = false;
  allExpandState = false;
  // childFalse = false;
  // closeExponstion: boolean = false;
  // isFDCcontextmenu = false;
  // statusValue: boolean = false;
  // hashNameClick: boolean = false;
  // isDropFileDms: boolean = false;
  // FdcCollabStatous: boolean = false;
  CreatedByAddIcon: boolean = false;
  isDisable: boolean = false;
  isclickoncreatelist = false;
  isDBError: boolean = false;
  // isNewEntityType = true;
  // UserSettings = false;
  // uservar = false;
  // FormGeneratorStatus: boolean = false;
  IsFdcAdmin: boolean = false;
  IsFdcGlobalAdmin: boolean = false;
  // deletenode: boolean = false;
  // isCreateEntity: boolean = false;
  // collapse: boolean = false;
  // allExpandStatetrue: boolean = false;
  // destroyAll: boolean = false;
  //isFabricAdmin: boolean = false;
  showtoggle = false;
  checkTreeActive = true;
  IsUserPreferrence: boolean = false;
  // isPaletteBodyOpened: boolean = false;
  // isPaletteOpened: boolean = false;
  // isPalleteShape: boolean = true;
  appcanIntialize: boolean = false;
  globalfdcAdmin: boolean = false;
  // AssetAdminForm: boolean = false;
  righttreeContextClick: boolean = false;
  isUpdateUserpreferencePinnedTab: boolean = true;
  // public isSecurityCapability = false;
  public add: boolean = false;
  showDownstreamLocation: boolean = false;
  isFilterIconActive: boolean = false;
  isOnlineDataSyncDone = false;
  showSyncStatusText: boolean = false;
  // productManagementContextClick: boolean = false;
  public favIconsClicked: boolean = false;
  // public isUsing: boolean = true;
  public isNextOrPreviousClicked: boolean = false;
  public isSharedListClicked: boolean = false;
  ShowHierarchyName: boolean = false;
  // disableautoCompleteForTab = false;
  // enabledisableglobalkeypad = true;
  // disableKeyPadHeader = true;
  isdisableTableModeButton = false;
  // otherSessionUnnecessaryMsg:boolean=false;//Added For Desktop
  /**
   * #######Undefinded variable declaration
   */
  // routParams;
  previousLocationData;
  event;
  // currentNode;
  // FdcCollabHtmlText;
  hostname;
  websocketProtocol;
  socket_URL;
  ZoomPercentage: string = "100%";
  CurrentList;
  public worker: Worker;
  queryParamsList: any;
  currentRoutingSource: string = 'V3 Home';
  treeIndexLabel: string = 'All';
  treeIndexLabelRight: string = '';
  chatMessageArray;
  maxDate: Date = new Date();
  userProfileIcon: string = "wwwroot/images/V3 Icon set part1/V3 UserSettings.svg";
  companyLog: string = "wwwroot/images/visur logo.svg";
  // percentagebutton = 0;
  switchButton;
  selectedCapabilityForSecurityForm = "Field Data Capture";
  // CurrentActiveTab = "";
  // ToggleWellMeterPurpose; //need to remove
  sBar: any;
  // vectorLayerforMap: any;
  // public tokenInCommonService = "";
  loc: any;
  // extention: any;
  domain: any = AppConfig.AppMainSettings.env.domain;
  // private data: UrlCacheData;
  // private observable: Observable<any>;
  BiObservableCount = 0;
  DeleteEntityType;
  TypeVal;
  userProfilePicture = '';
  capabilityTreeSelectedIndex: any = 0;
  // tabIndexIdVar: any;
  statusText = '';
  capabilityFabricId;
  activeContextMenuNodeId = "";
  locationName;
  // d3HoverIdForList: any;
  deletedahsoardid;
  // minimumZoom = 1
  // maximumZoom = 3.5
  // hashName;
  // NewCollapse: string = "Default";
  errorDetails: any;
  // fdcheading;
  nameoflist;
  ContextMenuData: any;
  ContextdisableData: any;
  ListContextMenuData;
  public instrumentationKey = "";
  zoomdataadminusername;
  // entityProperty;
  fabricToDisplay;
  username;
  // ObjectPath;
  // cssJson: Observable<any>;
  tenantName;
  tenantID;
  // diagramgraph;
  // assetsgraph;
  clientGuid;
  lastOpenedFabric: string;
  fabric;
  fdcfabricid;
  entityData;
  // FormDataObject;
  // selectedChannel;
  // FdcMessagingData;
  FabricCapabilityIdAtLoginTime;
  PeopleParentBinding;
  NameOfEntity: string;
  // fdcViewMode = 1;
  // fdcDate = '12/04/2018';
  // fdcSelectedLocationId;
  // viewType = "Map"
  // capabilitiesArray;
  // MapDefaultSetHomeType = "Last Viewed location";
  public baseUrl = AppConfig.BaseURL;
  public CustomerAppsettings = AppConfig.AppMainSettings;
  zoomdataURl;
  // public ipc: typeof ipcRenderer;
  CurrentUserEntityName = '';
  currentUserName;
  currentUserId;
  // DiagramId;
  // FdcNewNextForm;
  // public SeletedEntityID;
  // remote: typeof remote;
  FormSchemaData$: any;
  flag: any = 0;
  LeftTreePayload; //need to refactor
  httprequestid = 'Default';
  RerouteToAddNew = false;
  sideHeaderElement;
  ttouchtime: any = 0;
  // curretnCursorIndex: any = 0;
  Messagelist = [{ label: "Sending Source System ID's To Entities", value: 0 }, { label: "Saving Source System ID's", value: 0 },
  { label: "Confirming Found Source System ID's To Entities", value: 0 }, { label: "Refreshing Found Source System ID's To Entities", value: 0 }
    , { label: "Ready For Import", value: 0 }];
  /**using in fdc and entitymanagement */
  ListOfVesselsEquipments: any = [];
  ImportMessagelist = [{ label: "Importing Data", value: 0 }];
  leftHeaderSearchObject;
  listPreviousLocation;
  // sharedListValidateForm:boolean = false;
  listsParentData;
  /**
   * ######Object Type declaration Variables
   */
  darkTheme = {
    '--button-group-checked-unchecked': ':#302F30',
    '--loadFluid-BackGroundColor': '#2D2C2D',
    '--Hover-BackGround': '#4B4A4B',
    '--defaultmainHeader-BackGroundColor': '#232323',
    '--titleColor': '#ffffff',
    '--popUpHeader-BackGroundColor': '#323132',
    '--tooltip-color': '#7D7D7D',
    '--border-content-lines': 'rgba(241, 234, 234, 0.1)',
    '--mainHeader-BackGroundColor': '#232323',
    '--mainHeader-Opacity': '1',
    '--statusHeader-BackGroundColor': 'rgba(255, 255, 255, 0.01)',
    '--statusHeader-Opacity': '.01',
    '--statusHeader-FontColor': '#FFFFFF',
    '--statusHeader-FontOpacity': '.4',
    '--sideHeader-BackGroundColor': '#323132',
    '--sideHeader-Opacity': '1',
    '--sideContainer-BackGroundColor': '#414041',
    '--sideContainer-Opacity': '1',
    '--mouseHover-BackGroundColor': 'rgba(49, 143, 215, 0.3)',
    '--mouseHover-Opacity': '.13',
    '--active-BackGroundColor': '#318FD7',
    '--active-Opacity': '.3',
    '--sideNode-Opacity': '.2',
    '--sideNode-LightBackGround': 'rgba(255,255,255,0.02)',
    '--sideNode-DarkBackGround': '#3A3A3A',
    '--border': '0.5 solid #000000',
    '--scroll-BackGround': '#3A6F9A',
    '--scroll-Opacity': '.2',
    '--common-FontStyle': 'Roboto Regular',
    '--gas-BackGround': '#A8565F',
    '--condy-BackGround': '#437489',
    '--water-BackGround': '#728D99',
    '--ngl-BackGround': '#866390',
    '--propane-BackGround': '#8D519E',
    '--butane-BackGround': '#996AA6',
    '--pentane-BackGround': '#624969',
    '--oil-BackGround': '#698979',
    '--fluid-BackGround': '#766852',
    '--sand-BackGround': '#8B8B69',
    '--form-Title-Color': '#73BFF3',
    '--form-value-Color': '#ffffff',
    '--load-color': '#c7781a',
    '--unload-color': '#1fa232',
    '--icon-value-color': '#000000',
    '--icon-theme-value-color': '#FFFFFF',
    '--disable-capability-icon-theme-value-color': '#676667',
    '--Well-icon-value-color': '#FF0000',
    '--OilWell-icon-value-color': '#009245',
    '--disposal-icon-value-color': '#319AF0',
    '--footer-background-value-color': 'rgba(0,0,0,0.7)',
    '--calculate-values': '#39B54A',
    '--black-white-theme-color': '#000000',
    '--filter-border-color': '#E6E6E6',
    '--select-option': '#375a74',
    '--description-background-color': '#5c5c5c',
    '--desktop-shortcut-color': '#7D7D7D',
    '--desktop-header-color': '#1c1c1c',
    '--error-color': '#b66666',
    '--negativeValueColor': '#eb0c0c',
    '--collaboration-backgroundColor': '#454445',
    '--form-lebal-text-shadow': '#000000',
    '--map-timeline-background': 'rgba(0, 0, 0, 0.6)',
    '--map-tooltip-background': '#7D7D7D',
    '--formHeader-BackGroundColor': '#323132',
    '--filter-BackGroundColor': '#363636',
    '--multiday-value-datecolor': '#8D8C8D',
    '--multiday-value-rotatedcolor': '#ffffff',
    '--multiday-values-digitcolor': '#FFFFFF',

    '--multidayentry-backgroundColor-gas': '#504445',
    '--multidayentry-backgroundColor-condy': '#414648',
    '--multidayentry-backgroundColor-oil': '#494E4B',
    '--multidayentry-backgroundColor-water': '#46484A',
    '--multidayentry-backgroundColor-sand': '#4C4B47',
    '--multidayentry-backgroundColor-ngl': '#564E58',
    '--multidayentry-backgroundColor-propane': '#625366',
    '--multidayentry-backgroundColor-butane': '#6B5273',
    '--multidayentry-backgroundColor-pentane': '#5B515E',
    '--multidayentry-vertical-line-gas': '6px solid #Ad5760',
    '--multidayentry-vertical-line-condy': '6px solid #426675',
    '--multidayentry-vertical-line-oil': '6px solid #698979',
    '--multidayentry-vertical-line-water': '6px solid #657881',
    '--multidayentry-vertical-line-sand': '6px solid #78775e',
    '--multidayentry-vertical-line-ngl': '6px solid #866390',
    '--multidayentry-vertical-line-propane': '6px solid #8D519E',
    '--multidayentry-vertical-line-butane': '6px solid #996AA6',
    '--multidayentry-vertical-line-pentane': '6px solid #624969',
    '--ZoomRefreshButtonPosition-BackgroundColor': '#2B2A2B',
    '--subzoompercent-BackgroundColor': '#1A191A',
    '--subzoompercent-Color': '#FFFFFF',
    '--zoombutton-hover-color': '#555555',
    '--datatable-body-active': 'lightgray',
    '--ngselect-box-shadow': 'rgba(0,0,0,.38)',
    '--toggle-bar': 'gray',
    '--form-body-bg-color': '#424242',
    '--table-header-bg-color': '#2F2F2F',
    '--font-color': 'white',
    '--image-color': 'none',
    '--table-row-even-color': '#4D4D4D',
    '--ion-background-color': '#414041',
    '--table-row-odd-color': '#404040',
    '--ion-ep-header-background-color': '#3F3F3F',
    '--background-image-color': `url("data:image/svg+xml;charset=utf8,%3C?xml version='1.0' encoding='utf-8'?%3E%3C!-- Generator: Adobe Illustrator 23.0.3, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E%3Csvg version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 21 21' style='enable-background:new 0 0 21 21;' xml:space='preserve'%3E%3Cstyle type='text/css'%3E .st0{fill:%23FFFFFF;} %3C/style%3E%3Cg id='Layer_1'%3E%3C/g%3E%3Cg id='Layer_2'%3E%3Cg%3E%3Cpath class='st0' d='M9.2,8v4.3H4.8V8H9.2 M9.9,7.3H4.1V13h5.7V7.3L9.9,7.3z'/%3E%3Cpath class='st0' d='M21,3.2L21,3.2L21,0H0v21h21V3.9h0V3.2z M20.3,0.7v2.5H0.7V0.7H20.3z M20.3,20.3H0.7V3.9h19.6V20.3z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E");`
  };

  lightTheme = {
    '--ion-ep-header-background-color': '#F3F3F3',
    '--ion-background-color': '#white',
    '--font-color': 'black',
    '--image-color': 'invert(100)',
    '--table-row-odd-color': '#D9D9D9',
    '--table-row-even-color': '#FFFFFF',
    '--table-header-bg-color': '#2F2F2F',
    '--form-body-bg-color': '#ffffff',
    '--toggle-bar': 'rgba(0,0,0,.38)',
    '--ngselect-box-shadow': 'rgba(0,0,0,0.2)',
    '--button-group-checked-unchecked': 'white',
    '--datatable-body-active': 'white',
    '--loadFluid-BackGroundColor': '#FFFFFF',
    '--Hover-BackGround': '#ECECEC',
    '--defaultmainHeader-BackGroundColor': '#232323',
    '--titleColor': '#ffffff',
    '--popUpHeader-BackGroundColor': '#323132',
    '--form-lebal-text-shadow': '#ffffff',
    '--tooltip-color': '#E9EAEC',
    '--border-content-lines': '#d2d0d0',
    '--mainHeader-BackGroundColor': '#E9EAEC',
    '--mainHeader-Opacity': '1',
    '--statusHeader-BackGroundColor': 'rgba(255, 255, 255, 0.01)',
    '--statusHeader-Opacity': '.01',
    '--statusHeader-FontColor': '#000000',
    '--statusHeader-FontOpacity': '.4',
    '--sideHeader-BackGroundColor': '#E4E4E4',
    '--sideHeader-Opacity': '1',
    '--sideContainer-BackGroundColor': '#FFFFFF',
    '--sideContainer-Opacity': '1',
    '--mouseHover-BackGroundColor': 'rgba(49, 143, 215, 0.3)',
    '--mouseHover-Opacity': '.04',
    '--active-BackGroundColor': '#318FD7',
    '--active-Opacity': '1',
    '--sideNode-Opacity': '1',
    '--sideNode-LightBackGround': '#FFFFFF',
    '--sideNode-DarkBackGround': '#F0F0F0',
    '--border': '0.5 solid #ffffff',
    '--scroll-BackGround': '#3A6F9A',
    '--scroll-Opacity': '.2',
    '--common-FontStyle': 'Roboto Regular',
    '--gas-BackGround': '#A8565F',
    '--condy-BackGround': '#437489',
    '--water-BackGround': '#728D99',
    '--ngl-BackGround': '#866390',
    '--propane-BackGround': '#8D519E',
    '--butane-BackGround': '#996AA6',
    '--pentane-BackGround': '#624969',
    '--oil-BackGround': '#698979',
    '--fluid-BackGround': '#766852',
    '--sand-BackGround': '#8B8B69',
    '--form-Title-Color': '#73BFF3',
    '--form-value-Color': '#000000',
    '--load-color': '#c7781a',
    '--unload-color': '#1fa232',
    '--active-checkbackground-color': '#000000',
    '--icon-value-color': '#FFFFFF',
    '--icon-theme-value-color': '#000000',
    '--enable-capability-icon-theme-value-color': '#000000',
    '--disable-capability-icon-theme-value-color': '#C5C5C5',
    '--OilWell-icon-value-color': '#009245',
    '--disposal-icon-value-color': '#319AF0',
    '--footer-background-value-color': '#d2d0d0',
    '--calculate-values': '#39B54A',
    '--black-white-theme-color': '#ffffff',
    '--filter-border-color': '#414041',
    '--select-option': '#375a74',
    '--description-background-color': '#a29f9f',
    '--desktop-shortcut-color': '#b3b3b3',
    '--desktop-header-color': '#FFFFFF',
    '--error-color': '#b66666',
    '--negativeValueColor': '#eb0c0c',
    '--collaboration-backgroundColor': '#FFFFFF',
    '--map-timeline-background': 'rgba(225, 225, 225, 0.5)',
    '--map-tooltip-background': 'rgba(225, 225, 225)',
    '--formHeader-BackGroundColor': '#E4E4E4',
    '--filter-BackGroundColor': '#E9EAEC',
    '--multiday-value-datecolor': '#7F7F7F',
    '--multiday-value-rotatedcolor': '#000000',
    '--multiday-values-digitcolor': '#000000',

    '--multidayentry-backgroundColor-gas': '#F7DFE2',
    '--multidayentry-backgroundColor-condy': '#D9E0E3',
    '--multidayentry-backgroundColor-oil': '#D5E0D9',
    '--multidayentry-backgroundColor-water': '#E8EAEC',
    '--multidayentry-backgroundColor-sand': '#EDEDE8',
    '--multidayentry-backgroundColor-ngl': '#E4D8E8',
    '--multidayentry-backgroundColor-propane': '#D8C3DE',
    '--multidayentry-backgroundColor-butane': '#DFBEEA',
    '--multidayentry-backgroundColor-pentane': '#E8D3EE',
    '--multidayentry-vertical-line-gas': '6px solid #Ad5760',
    '--multidayentry-vertical-line-condy': '6px solid #426675',
    '--multidayentry-vertical-line-oil': '6px solid #698979',
    '--multidayentry-vertical-line-water': '6px solid #657881',
    '--multidayentry-vertical-line-sand': '6px solid #A7A78E',
    '--multidayentry-vertical-line-ngl': '6px solid #866390',
    '--multidayentry-vertical-line-propane': '6px solid #8D519E',
    '--multidayentry-vertical-line-butane': '6px solid #996AA6',
    '--multidayentry-vertical-line-pentane': '6px solid #624969',
    '--ZoomRefreshButtonPosition-BackgroundColor': '#FFFFFF',
    '--subzoompercent-BackgroundColor': '#E4E4E4',
    '--subzoompercent-Color': '#000000',
    '--zoombutton-hover-color': '#AECAE0',
    '--background-image-color': `url("data:image/svg+xml;charset=utf8,%3C?xml version='1.0' encoding='utf-8'?%3E%3C!-- Generator: Adobe Illustrator 23.0.3, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E%3Csvg version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='15px' y='15px' viewBox='0 0 21 21' style='enable-background:new 0 0 21 21;' xml:space='preserve'%3E%3Cg id='Layer_1'%3E%3C/g%3E%3Cg id='Layer_2'%3E%3Cg%3E%3Cpath d='M9.2,8v4.3H4.8V8H9.2 M9.9,7.3H4.1V13h5.7V7.3L9.9,7.3z'/%3E%3Cpath d='M21,3.2L21,3.2L21,0H0v21h21V3.9h0V3.2z M20.3,0.7v2.5H0.7V0.7H20.3z M20.3,20.3H0.7V3.9h19.6V20.3z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E")`
  };
  layoutJSON = { 'LeftHeader': true, 'LeftTree': true, 'RightHeader': true, 'RightTree': false, 'OverLayRightTree': true };
  // commonFdcModel = { "EntityId": "", "EntityType": "", "LocationType": "", "LocationId": "", "LocationName": "", "DistrictId": "", "District": "", "Area": "", "AreaId": "", "Field": "", "FieldId": "", "ProductionDate": "" };

  blobUrl = {
    [FABRICS.PEOPLEANDCOMPANIES]: AppConfig.BlobURL + 'icons/',
    'drawingsFabric': AppConfig.BlobURL + 'icons/',
    'biFabric': AppConfig.BlobURL + 'icons/',
    'palettestencils': AppConfig.BlobURL + 'stencilsfiles/',
    'palettesvgs': AppConfig.BlobURL + 'palletsvg/',
    'fdcFabric': AppConfig.BlobURL + 'fdc/',
    'taskFabric': AppConfig.BlobURL + 'taskfabric/'
  };

  /**
   * #######Array type variable declaration
   */
  // FdcCurrentdaymonthDivider: any = [];
  // FdcDayName: any = [];
  // FDCCurrentDate: any = [];
  rightSideCapabilityList = [];
  // GisMapGridLayerArray = [];
  history = [];
  leftHeaderConfig: any = [];
  faviconList = [];
  AllChildrens: any = [];
  // ConversationList = [];
  availableentities: any = [];
  // PermissionFdcForms = [];
  // LasFileData = [];
  // usersListBasedOnTenant = []; //not using
  public AllHeaderRoutingListService = [];
  public AllHeaderRoutingList = [];
  // FDCListMainLocationsForForms = [];//not using
  // channelsDetails = [];
  // tempSearchArray = [];
  // contextMenuSetMapLocation: any = [];
  emPumpStatus = [];

  public listEntityId: string;
  public EMProductionDate: Date = new Date();
  constructor(public entityCompanyTypesStoreQuery: EntityCompanyTypesStoreQuery, public schemaEntityCommonStoreQuery: SchemaEntityCommonStoreQuery, private _cookieService: CookieService,
    public entityCommonStoreQuery: EntityCommonStoreQuery,
    public stateRecords: StateRecords, public usersStateQuery: UsersStateQuery, public loadingBar: LoadingBarService,
    private http: HttpClient, private router: Router, private route: ActivatedRoute,
    private logger: AppInsightsService, public appConfig: AppConfig,
    @Inject(DOCUMENT) private document: HTMLDocument, public dialog: MatDialog,
    public authCommonStoreQuery: AuthCommonStoreQuery, private dataService: DataService,
    public formStateQuery: FormStateQuery, private titleService: Title, public fdcStateQuery: FDCStateQuery,
    public observableStateService: ObservableStoreService) {
    this.workerInit();

    this.online$.pipe(untilDestroyed(this, 'destroy')).subscribe(e => {
      if (!this.CustomerAppsettings.env.UseAsDesktop) {
        this.loadingBarAndSnackbarStatus("", 'Reconnecting...');
        this.sendMessageToServerNext(null, "OnlineEvent", "connect");
        setTimeout(() => {
          this.loadingBarAndSnackbarStatus("", "");
        }, 5000);
      }
    });

    this.offline$.pipe(untilDestroyed(this, 'destroy')).subscribe(e => {
      this.loadingBarAndSnackbarStatus("", 'No Internet Connection...');
      setTimeout(() => {
        this.loadingBarAndSnackbarStatus("", "");
      }, 5000);
    });
  }

  FilterArr = [];

  initializeTreeWorkersObservablesPeople() {
    //********people All tree
    var people_entities$ = this.entityCommonStoreQuery
      .selectAll({
        filterBy: (entity: any) => entity && ([TypeOfEntity.Company, TypeOfEntity.Office, TypeOfEntity.Person].indexOf(entity.EntityType) >= 0) && entity.SG && (<Array<any>>entity.SG).indexOf(FABRICS.PEOPLEANDCOMPANIES) >= 0
      })
      .debounceTime(150)
      .filter(data => data.length != 0)
      .map(data => JSON.parse(JSON.stringify(data)))
      .shareReplay(1);

    var people_all$ = people_entities$.switchMap(d => from<any>(this.flatToPeopleAllTree(d))
      // .do(d => console.log("Peole All tree data"))
      // // remove this after solving circular depencency
      // .filter(data => {
      //   try {
      //     JSON.parse(JSON.stringify(data));
      //     return true
      //   } catch (e) {
      //     console.log("ciruclar")
      //     return false;
      //   }
      // })
      .filter(entity => [TypeOfEntity.Company, TypeOfEntity.Office, TypeOfEntity.Person].indexOf(entity["EntityType"]) >= 0).toArray())
      .shareReplay(1);
    this.observableStateService.add({ Capability: FABRICS.ENTITYMANAGEMENT + "_" + FABRICS.PEOPLEANDCOMPANIES + "_" + FabricsNames.SECURITY, Tree: TreeTypeNames.PeopleAndCompanies + '_' + TreeTypeNames.ALL, observable: people_all$.shareReplay(1) });
  }

  initializeTreeWorkersObservablesConstruction() {
    //******** Construction All tree
    const entities$ = this.entityCommonStoreQuery
      .selectAll({
        filterBy: (entity: any) => entity && entity.SG && (<Array<any>>entity.SG).indexOf(FABRICS.CONSTRUCTION) >= 0
      })
      .debounceTime(150)
      .filter(data => data.length != 0)
      .map(data => JSON.parse(JSON.stringify(data)))
      .shareReplay(1);

    const constructionAll$ = entities$.switchMap(d => from<any>(JSON.parse(JSON.stringify(d)))
      .filter(entity => [EntityTypes.Project].indexOf(entity["EntityType"]) >= 0).toArray())
      .map(d => this.flattenToConstructionHierarchy(d))
      .shareReplay(1);

    // const constructionAll$ = entities$.switchMap(d => from<any>(JSON.parse(JSON.stringify(d)))
    //   .filter(entity => [EntityTypes.Project, EntityTypes.Task].indexOf(entity["EntityType"]) >= 0).toArray())
    //   .map(d => this.flattenToConstructionHierarchy(d))
    //   .shareReplay(1);

    // const taggedItemsAll$ = entities$.switchMap(d => from<any>(JSON.parse(JSON.stringify(d)))
    //   .filter(entity => [TypeOfEntity.Tagged].indexOf(entity[AppConsts.TYPEOF]) >= 0).toArray())
    //   .map(d => this.flatToHierarchyState(d))
    //   .shareReplay(1);

    // const physicalInstancesItemsAll$ = entities$.switchMap(d => from<any>(JSON.parse(JSON.stringify(d)))
    //   .filter(entity => [EntityTypes.PhysicalInstances].indexOf(entity["EntityType"]) >= 0).toArray())
    //   .shareReplay(1);

    // const materialsItemsAll$ = entities$.switchMap(d => from<any>(JSON.parse(JSON.stringify(d)))
    //   .filter(entity => [TypeOfEntity.Material].indexOf(entity[AppConsts.TYPEOF]) >= 0).toArray())
    //   .map(d => this.flatToHierarchyState(d))
    //   .shareReplay(1);

    // const documentsItemsAll$ = entities$.switchMap(d => from<any>(JSON.parse(JSON.stringify(d)))
    //   .filter(entity => [EntityTypes.Documents].indexOf(entity["EntityType"]) >= 0).toArray())
    //   .shareReplay(1);
    var trees = [TreeTypeNames.CONSTRUCTION, TreeTypeNames.Schedule, TreeTypeNames.TaggedItems, TreeTypeNames.Materials, TreeTypeNames.Documents, TreeTypeNames.PhysicalInstances, TreeTypeNames.Approvals, TreeTypeNames.Procedures, TreeTypeNames.Turnover, TreeTypeNames.Directory].join('_');
    var capabilities = [FABRICS.ENTITYMANAGEMENT, FABRICS.CONSTRUCTION, FABRICS.SECURITY].join('_');
    this.observableStateService.add({ Capability: capabilities, Tree: trees, observable: constructionAll$.shareReplay(1) });

    // this.observableStateService.add({ Capability: FABRICS.ENTITYMANAGEMENT + "_" + FABRICS.CONSTRUCTION, Tree: TreeTypeNames.TaggedItems, observable: taggedItemsAll$.shareReplay(1) });

    // this.observableStateService.add({ Capability: FABRICS.ENTITYMANAGEMENT + "_" + FABRICS.CONSTRUCTION, Tree: TreeTypeNames.PhysicalInstances, observable: physicalInstancesItemsAll$.shareReplay(1) });

    // this.observableStateService.add({ Capability: FABRICS.ENTITYMANAGEMENT + "_" + FABRICS.CONSTRUCTION, Tree: TreeTypeNames.Materials, observable: materialsItemsAll$.shareReplay(1) });

    // this.observableStateService.add({ Capability: FABRICS.ENTITYMANAGEMENT + "_" + FABRICS.CONSTRUCTION, Tree: TreeTypeNames.Documents, observable: documentsItemsAll$.shareReplay(1) });
  }

  initializeTreeWorkersObservablesBI() {
    //******************BI Fabric
    var bi_entities$ = this.entityCommonStoreQuery
      .selectAll({
        filterBy: (entity: any) => entity && [EntityTypes.Category, EntityTypes.DashBoard].indexOf(entity.EntityType) >= 0 && entity.SG && (<Array<any>>entity.SG).indexOf(FABRICS.BUSINESSINTELLIGENCE) >= 0
      })
      .debounceTime(150)
      .filter(data => data.length != 0)
      .map(data => JSON.parse(JSON.stringify(data)))
      .shareReplay(1);

    let bi_all$ = bi_entities$.map(data => {
      this.loadingBarAndSnackbarStatus(AppConsts.loaderComplete);
      return this.GetParentChildRelationship(data);
    });

    this.observableStateService.add({ Capability: FABRICS.ENTITYMANAGEMENT + "_" + FABRICS.BUSINESSINTELLIGENCE + "_" + FabricsNames.SECURITY, Tree: TreeTypeNames.BusinessIntelligence + "_" + TreeTypeNames.ALL, observable: bi_all$.shareReplay(1) });
  }

  initializeTreeWorkersObservablesMaps() {
    //******************BI Fabric
    var Maps_entities$ = this.entityCommonStoreQuery
      .selectAll({
        filterBy: (entity: any) => entity && [EntityTypes.Category, EntityTypes.Map].indexOf(entity.EntityType) >= 0 && entity.SG && (<Array<any>>entity.SG).indexOf(FabricsNames.MAPS) >= 0
      })
      .debounceTime(150)
      .filter(data => data.length != 0)
      .map(data => JSON.parse(JSON.stringify(data)))
      .shareReplay(1);

    let all$ = Maps_entities$.map(data => {
      this.loadingBarAndSnackbarStatus(AppConsts.loaderComplete);
      return this.GetParentChildRelationship(data);
    });

    this.observableStateService.add({ Capability: FABRICS.ENTITYMANAGEMENT + "_" + FabricsNames.MAPS + "_" + FabricsNames.SECURITY, Tree: TreeTypeNames.ALL, observable: all$.shareReplay(1) });
  }

  flattenToConstructionHierarchy(nodes: any[]) {
    const treeSortedData = this.getConstructionTreeData(nodes);
    const flattenAllTreeData = this.flatToHierarchyState(treeSortedData);
    return flattenAllTreeData;
  }

  getConstructionTreeData(treeData) {
    let treeEntities: Array<any> = [];
    const nodeDataArray = this.getConstructionNodeEntities(treeData);
    treeEntities = JSON.parse(JSON.stringify(nodeDataArray));

    let data = treeEntities.filter((nodeData: any) => nodeData && nodeData.EntityName != undefined && nodeData.EntityName != null);

    data.sort((firstEntity, secondEntity) => {
      return firstEntity["EntityName"].toLowerCase() > secondEntity["EntityName"].toLowerCase() ? 1 : -1;
    });
    return data;
  }

  getConstructionNodeEntities(treeData) {
    let nodeDataArray = [];
    for (let nodeIndex in treeData) {
      let nodeData = {};
      const data = treeData[nodeIndex];
      nodeData["EntityName"] = data["EntityName"];
      nodeData["EntityType"] = data["EntityType"];
      nodeData["EntityId"] = data["EntityId"];
      nodeData['children'] = [];

      if (data["NestedUnder"] != undefined) {
        nodeData["Parent"] = data["NestedUnder"];
      }
      nodeDataArray.push(nodeData);
    }
    return nodeDataArray;
  }

  // get isElectron() {
  //   return window && window.process && window.process.type;
  // }
  clearObservables() {
    // this.observable = null;
  }

  public loadRouting(): void {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd)).pipe(untilDestroyed(this, 'destroy'))
      .subscribe(({ urlAfterRedirects }: NavigationEnd) => {
        this.history = [...this.history, urlAfterRedirects];
      });
  }

  public getHistory(): string[] {
    return this.history;
  }

  public getPreviousUrl(): string {
    return this.history[this.history.length - 1] || '/index';
  }

  public getPreviousUrlForBackButton(): string {
    return this.history[this.history.length - 2] || '/index';
  }
  getCurrentFabricName(name) {
    try {
      let fabricName = '';
      switch (name) {
        case "bi":
          fabricName = FABRICS.BUSINESSINTELLIGENCE;
          break;
        case "people":
          fabricName = FABRICS.PEOPLEANDCOMPANIES;
          break;
        case "Security":
          fabricName = FABRICS.SECURITY;
          break;
        case "home":
          fabricName = FABRICS.HOME;
      }
      return fabricName;
    }
    catch (e) {
      this.appLogException(new Error('Exception in getCurrentFabricName() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  getEntityDataFromAuthorization(message): any {
    try {
      const resultMessage = JSON.parse(message);
      const payload = JSON.parse(resultMessage.Payload);
      let entityDetails = {
        EntityDetails: []
      };
      let entity = this.getEntitiesById(payload.LocationId);
      if (entity && entity.Capabilities) {
        const capabilities = entity.Capabilities.filter(resId => resId == payload.CapabilityId);
        if (capabilities.length > 0) {
          entityDetails.EntityDetails.push(entity);
          return of(entityDetails);
        }
        else {
          return of(null);
        }
      }
      else if (this.CustomerAppsettings.env.UseAsDesktop == false && this.onlineOffline == true) {
        const url = this.baseUrl + `api/AlThings/EntityData?message=` + message;
        return this.http.
          get(url)
          .pipe(map((response: Response) => {
            const data = response;
            if (data) {
              return data;
            }
            else {
              return null;
            }
          }));
      }
    }
    catch (ex) {
      this.appLogException(new Error('Exception in getEntityDataFromAuthorization() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + ex));
    }
  }

  getKeyByValue(object, value) {
    let data = Object.keys(object).find(key => object[key] === value);
    if (data && data != '') {
      return JSON.parse(data);
    }
    else
      return false;
  }

  getTimeZoneFromState(): string {
    let timeZone = this.fdcStateQuery.getStateKey(FDCStateConstantsKeys.timeZone);
    if (timeZone == "" || timeZone == undefined) { //temprary written will remove
      return "Alberta, Canada (GMT-06:00)";
    }
    else {
      return timeZone;
    }
  }

  getFromFDCStateByKey(key) {
    return this.fdcStateQuery.getStateKey(key);
  }

  setToFDCStateByKey(key, data: any) {
    this.fdcStateQuery.add(key, data);
  }

  compare(firstform, secondForm) {
    if (Number(firstform.sequenceNumber) > Number(secondForm.sequenceNumber)) return 1;
    if (Number(secondForm.sequenceNumber) > Number(firstform.sequenceNumber)) return -1;
    return 0;
  }

  upsertGridTypeHeaderContentCols(entityId, entityType, headerGrid, contentGrid, properties, isExpand, listData, operation, ArrayInsertion: Array<HeaderContentSequence>, sequenceNumber = "") {
    let stateArray = [];
    let existingStateArray = this.getFromFDCStateByKey(TabletKeyPadState.HeaderContentSequenceColGridKey);

    switch (operation) {
      case "insert":
        let stateEntity = new EntityHeaderContentSequence().getInstance();
        stateEntity.EntityId = entityId;
        stateEntity.headerGrid = headerGrid;
        stateEntity.contentGrid = contentGrid;
        stateEntity.sequenceNumber = sequenceNumber;
        stateEntity.EntityType = entityType;
        stateEntity.properties = JSON.stringify(properties);
        stateEntity.isExpand = isExpand;
        stateEntity.listData = listData;
        if (existingStateArray) {
          stateArray = JSON.parse(JSON.stringify(existingStateArray));
          let elementIndex = existingStateArray.findIndex(item => item && item.EntityId == entityId);
          if (elementIndex > -1) {
            stateArray.splice(elementIndex, 1);
          }
          stateArray.push(JSON.parse(JSON.stringify(stateEntity)));
        }
        else {
          stateArray.push(JSON.parse(JSON.stringify(stateEntity)));
        }
        stateArray.sort(this.compare);
        this.insertGridToState(stateArray);
        break;
      case "update":
        if (existingStateArray) {
          let existingItems = JSON.parse(JSON.stringify(existingStateArray));
          existingItems.forEach((item, index, array) => {
            if (item.EntityId == entityId) {
              if (headerGrid)
                array[index].headerGrid = headerGrid;
              if (contentGrid)
                array[index].contentGrid = contentGrid;
              if (sequenceNumber)
                array[index].sequenceNumber = sequenceNumber;
              if (isExpand != null)
                array[index].isExpand = isExpand;
              if (listData)
                array[index].listData = listData;
              if (properties)
                array[index].properties = JSON.stringify(properties);
              if (sequenceNumber != "")
                array[index].sequenceNumber = sequenceNumber;
            }
          });
          this.insertGridToState(existingItems);
        }
        break;
      case "insertArray":
        this.insertGridToState(ArrayInsertion);
        break;
      case "reset":
        this.insertGridToState([]);
        break;
      case "delete":
        if (existingStateArray) {
          stateArray = JSON.parse(JSON.stringify(existingStateArray));
          let elementIndex = stateArray.findIndex(item => item && item.EntityId == entityId);
          if (elementIndex > -1) {
            stateArray.splice(elementIndex, 1);
          }
        }
        stateArray.sort(this.compare);
        this.insertGridToState(stateArray);
        break;
      default:
        break;
    }

  }

  private insertGridToState(stateEntity: Array<HeaderContentSequence>) {
    this.setToFDCStateByKey(TabletKeyPadState.HeaderContentSequenceColGridKey, stateEntity);
  }

  upsertCopyToNewEntityInState(obj) {
    if (obj)
      this.setToFDCStateByKey('CopyToNewEntity', JSON.parse(JSON.stringify(obj)));
  }
  getUsersStateKey(key) {
    return this.usersStateQuery.getStateKey(key);
  }

  addUserStateKeyValue(key, value): void {
    this.usersStateQuery.add(key, value);
  }

  isCapbilityAdmin() {
    return isCapabilityAdmin(this.getFabricNameByUrl(this.router.url),this.appConfig.AllHeaderRoutingList);
  }


  getUIProjectState(projectId) {
    let state$ = this.getUIState();
    let project$ = this.getProject(projectId);
    return combineLatest(state$,project$).map(([state,project])=>{
      if (project) {
        state.projectName = project.EntityName;
        state.projectId = project.EntityId;
        state.projectUOM = project.ProjectUOM;
      } else {
        console.error("Project Id not exist in state:" + projectId);
      }
      return state;
    })
  }

  getProject(projectId):Observable<any> {
    var obj:any = this.entityCommonStoreQuery.getEntity(projectId);
    if(obj && obj.ProjectUOM){
      return of(obj);
    }
    else {
     //readfrombackend
      var payload = {};
      var message = this.getMessageDefaultProperties();
      message.CapabilityId = this.getCapabilityId(FABRICS.CONSTRUCTION)
      message.Payload = JSON.stringify(payload);
      message.DataType = "ProjectRecord";
      message.EntityID = projectId;
      message.EntityType = EntityTypes.Construction;
      message.MessageKind = MessageType.READ;
      message.Routing = Routing.OriginSession;
      message.Type = "Details";
      message.Fabric = FABRICS.CONSTRUCTION;

      return this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message).map((res: any) => {
        if (res["Data"]) {
          if (res["Data"]["Data"] == "Unauthorized") {
            this.appLogException(new Error(res));
          }
        } else {
          var messageData = typeof res === 'string' ? JSON.parse(res) : res;
          let payload = JSON.parse(messageData.Payload);
          if(payload){
            let modelPayload = JSON.parse(payload.Payload);
            var stateObject:any = this.entityCommonStoreQuery.getEntity(projectId);
            if(stateObject) {
              let obj = JSON.parse(JSON.stringify(stateObject));
              obj.ProjectUOM=modelPayload.projectuom
              this.AddorUpdateEntityState(obj.EntityId,obj,FABRICS.CONSTRUCTION);
            }
            return {
              EntityId:modelPayload.uuid,
              EntityName:modelPayload.projectname,
              ProjectUOM:modelPayload.projectuom
            }
          }
        }
      });
    }
  }

  getUIState() {
    let userDetails = this.usersStateQuery.getAll();
    if(!userDetails.userid){
        return this.GetUserSettings().map(res=>{
          this.userSettingState(res);
          let userState = this.usersStateQuery.getAll();
          return this.applicationState(userState);
        });
    }
    else{
      return Observable.of(this.applicationState(userDetails))
    } 
  }

  applicationState(userDetails){
    let state = new ApplicationState();
    state.userName = userDetails.username;
    state.fullName = userDetails.firstname +' ' + userDetails.lastname;
    state.firstName = userDetails.firstname;
    state.lastName = userDetails.lastname;    
    state.email = userDetails.emailid;
    state.userId = userDetails.userid;
    state.companyId = userDetails.companyid;
    state.companyName = userDetails.companyname;
    state.tenantId = userDetails.tenantid;
    state.tenantName = userDetails.tenantname;  
    state.projectId = null;
    state.projectName = null;    
    state.projectUOM = null;
    return state;
  }

  GetUserSettings() {
    try {
      var payload = { "EntityType": EntityTypes.PERSON, "Fabric": "People", "UserSettings": "yes", DataType: EntityTypes.EntityInfo };
      var message = this.getMessageDefaultProperties();
      message.CapabilityId = this.getCapabilityId(FABRICS.PEOPLEANDCOMPANIES)
      message.Payload = JSON.stringify(payload);
      message.DataType = FABRICS.PEOPLEANDCOMPANIES + "_UserSetting";
      message.EntityID = this.currentUserId;
      message.EntityType = EntityTypes.PERSON;
      message.MessageKind = MessageType.READ;
      message.Routing = Routing.OriginSession;
      message.Type = "Details";
      message.Fabric = FABRICS.PEOPLEANDCOMPANIES;

      return this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message).map((res: any) => {
        this.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, "");
        if (res["Data"]) {
          if (res["Data"]["Data"] == "Unauthorized") {
            this.appLogException(new Error(res));
          }
        } else {
          var messageData = typeof res === 'string' ? JSON.parse(res) : res;
          return messageData;
        }
      });

    } catch (e) {
      console.error('Exception in GetUserSettings() of CommonService  at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  userSettingState(messageData) {
      try {
        if (messageData) {
          let msgPayload = JSON.parse(messageData['Payload'])
          let personPayloadData = JSON.parse(msgPayload["Data"]);
          let payload = JSON.parse(personPayloadData.Payload);
          let person = payload.person;
          let userDetails = this.usersStateQuery.getAll();
          userDetails.userid=person.EntityId;
          userDetails.firstname=person.firstname;
          userDetails.lastname=person.lastname;

          userDetails.username=person.email;
          userDetails.emailid=person.email;

          userDetails.companyid=person.Parent;
          userDetails.companyname=person.company;
          userDetails.tenantid=this.tenantID;
          userDetails.tenantname=this.tenantName;

          this.usersStateQuery.updateState(userDetails);
        }
  
      } catch (e) {
        console.error('Exception in userSettingState() of commonservice  at time ' + new Date().toString() + '. Exception is : ' + e);
      }
  }

  routeToFabrics(data) {
    try {
      this.sendAbortRequest();
      this.capabilityFabricId = data.entityId;
      this.RouteBasedOnPreviousUrl(data);
    }
    catch (e) {
      this.appLogException(new Error('Exception in routeToFabrics() in CommonService  at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack));

    }
  }


  RouteBasedOnPreviousUrl(data) {
    try {
      let url: string = this.getPreviousUrl();
      let urlTree = this.router.parseUrl(this.router.url);
      const fabricName = this.router.url.split('/')[1];
      let routeObj = { entityId: "", date: "", path: "", tab: "", fdcProductionDate: "", opfToFdcdate: "", fromFabric: "" };
      this.getFdcProductionDate(routeObj);

      switch (fabricName) {
        case FABRICS.ENTITYMANAGEMENT:
          if (urlTree.root.children && urlTree.root.children.primary && urlTree.root.children.primary.segments) {
            urlTree.root.children.primary.segments.forEach(data => {
              switch (data.path) {
                case 'Data':
                case 'Settings':
                  routeObj.path = data.path;
                  break;
                  break;
                default:
                  if (fabricName == FABRICS.ENTITYMANAGEMENT) {
                    routeObj.fromFabric = this.getFabricNameByTab(this.queryParamsList.tab);
                  }
                  break;
              }
            });

            routeObj.entityId = urlTree.queryParams.EntityID;
            routeObj.date = urlTree.queryParams.Date;
          }
          break;
        default:
          break;
      }

      if (routeObj.entityId && AppConsts.mutualFabric.includes(routeObj.fromFabric) && data.Fabric != 'Home' && data.Fabric != 'BI' && data.Fabric != 'Security') {
        let message = this.getMessage();
        message.Payload = JSON.stringify({ CapabilityId: this.capabilityFabricId, LocationId: routeObj.entityId });
        message.EntityID = "EntityData";
        message.MessageKind = MessageKind.READ;
        this.getEntityDataFromAuthorization(JSON.stringify(message)).pipe(untilDestroyed(this, 'destroy')).subscribe(res => {
          let entityType;
          if (res != null && res['EntityDetails'] && res['EntityDetails'].length > 0 && res['EntityDetails'][0]['EntityId']) {
            entityType = res['EntityDetails'][0]['EntityType'];
            this.RouteBasedOnEntityType(url, data, routeObj);
          }
          else if (data.baseRoute == FABRICS.ENTITYMANAGEMENT) {
            this.RouteBasedOnEntityType(url, data, routeObj);
          }
          // else if (data.baseRoute == FABRICS.PIGGING) {
          //   this.router.navigate([data.routerLink], { queryParams: data.queryParameter })
          // }
          else if (!res || (res['EntityDetails'] && res['EntityDetails'].length == 0)) {
            this.router.navigate([data.routerLink], { queryParams: data.queryParameter });
            if (this.queryParamsList.tab != TreeTypeNames.PeopleAndCompanies)
              this.entityExist = true;
            this.navigateMutualFabric = true;
          }
        });


      }
      else {
        this.router.navigate([data.routerLink], { queryParams: data.queryParameter });
      }
    }
    catch (e) {
      this.appLogException(new Error('Exception in RouteBasedOnPreviousUrl() in CommonService  at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack));
    }
  }

  RouteBasedOnEntityType(url, data, routeObj) {
    let timeZone = this.getTimeZoneFromState();
    const fabricName = url.split('/')[1];

    if (AppConsts.mutualFabric.includes(fabricName) && AppConsts.mutualFabric.includes(data.baseRoute)) {
      switch (fabricName) {
        case FABRICS.ENTITYMANAGEMENT:    // navigating from EM to pigging / OPF/FDC

          this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, '', 'post', this.baseUrl + 'api/AlThings/ProductionDate', {}, timeZone)
            .subscribe((res: any) => {
              this.navigateFromEntityManagementFabric(url, data, routeObj, fabricName, res);
            });
          break;

        default:
          break;
      }
    }
    else {
      this.router.navigate([data.routerLink], { queryParams: data.queryParameter });
    }
  }

  navigateFromEntityManagementFabric(url, data, routeObj, fabricname, res) {
    let fdcProdDate = this.GetProdDateBasedonTimeZonefromserver(res);
    if (fdcProdDate && fabricname == FABRICS.ENTITYMANAGEMENT) {
      const fdcPDate = JSON.stringify(fdcProdDate);
      let fdcPDate1 = fdcPDate.split('T')[0];
      routeObj.fdcProductionDate = fdcPDate1.substring(1);
      let currentDate = this.getCurrentDate();
      routeObj.date = currentDate;

      let pdate1 = routeObj.date.split('-');
      if (pdate1[1].length == 1) {
        pdate1[1] = '0' + pdate1[1];
        if (pdate1[2].length == 1) {
          pdate1[2] = '0' + pdate1[2];
        }
      }

      routeObj.date = pdate1[2] + '-' + pdate1[1] + '-' + pdate1[0];
    }
  }

  getFdcProductionDate(routeObj) {
    // let fdcProdDate;
    let timeZone = this.getTimeZoneFromState();
    this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.baseUrl + 'api/AlThings/ProductionDate', {}, timeZone).subscribe((res: any) => {
      this.getProductionDateBasedOnTimeZone(routeObj, res);
    });
  }

  getProductionDateBasedOnTimeZone(routeObj, res) {
    let fdcProdDate = this.GetProdDateBasedonTimeZonefromserver(res);
    if (fdcProdDate) {
      const fdcPDate = JSON.stringify(fdcProdDate);
      let fdcPDate1 = fdcPDate.split('T')[0];
      routeObj.fdcProductionDate = fdcPDate1.substring(1);
      routeObj.opfToFdcdate = routeObj.fdcProductionDate;
      let prodDate = routeObj.opfToFdcdate.split('-');
      routeObj.opfToFdcdate = prodDate[2] + '-' + prodDate[1] + '-' + prodDate[0];
      routeObj.date = prodDate[2] + '-' + prodDate[1] + '-' + prodDate[0];
    }
  }
  MultipleDraggedEntities(event, treeType): Array<any> {
    let data: Array<any> = [];
    let activateNodes = event.element.treeModel.activeNodeIds;
    let selectedTreeValues = Object.keys(Object.keys(activateNodes).filter(d => activateNodes[d]).reduce((obj, key) => { obj[key] = activateNodes[key]; return obj; }, {}));
    if (selectedTreeValues.length > 1) {
      selectedTreeValues.forEach(item => {
        let entityNode = event.element.treeModel.getNodeById(item);
        if (entityNode.data.EntityType != EntityTypes.List) {
          data.push(entityNode.data);
        }
      });
      this.sendDataToAngularTree1(treeType, TreeOperations.deactivateSelectedNode, "");
    } else {
      data.push(event.element.data);
    }
    return data;
  }
  getNodeByEntityId(idString: string, model: TreeModel): TreeNode {
    try {
      const node: TreeNode = model.getNodeBy((node) => node.data.EntityId == idString);
      return node;
    }
    catch (e) {
      this.appLogException(new Error('Exception in getNodeById() in CommonService  at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack));
    }
  }

  getTreeAndTreeNodeByEntityId(fabric, treeName: Array<string>, entityId): Observable<TreeAndNodeEventModel> {
    let treeAndTreeNode: TreeAndNodeEventModel;
    let nodeEvent = this.getAngularTreeEvent(fabric, treeName, TreeOperations.GetTreeInstance)
      .map((response: TreeDataEventResponseModel) => {
        if (treeName.indexOf(response.treeName[0]) > -1) {
          let tree: any = response.treePayload;
          if (tree) {
            let treeNode: TreeNode = this.getNodeByEntityId(entityId, tree.treeModel);
            let treeAndTreeNode: TreeAndNodeEventModel = {
              tree: tree,
              treeNode: treeNode
            };
            return treeAndTreeNode;
          }
        }
      });
    return treeAndTreeNode ? of(treeAndTreeNode) : nodeEvent;
  }

  getAngularTreeEvent(fabric: string, treeName: Array<string>, treeOperationType: string, treePayload?): EventEmitter<any> {
    let eventModel: AngularTreeEventMessageModel = {
      fabric: fabric,
      treeName: treeName,
      treeOperationType: treeOperationType,
      treePayload: treePayload,
      Event: new EventEmitter()
    };
    setTimeout(() => {
      this.getAngularTreeEvent$.next(eventModel);
    });
    return eventModel.Event;
  }

  deleteStateNodeById(key: string, EntityId: string) {
    this.entityCommonStoreQuery.deleteNodeById(key, EntityId);
  }

  validatePhoneNumberBased(country, mobile): Observable<any> {
    try {
      return this.http.get('wwwroot/json/countrycode.json').pipe(switchMap((data: any) => {
        let countryCode = data['countryCode'][country];
        let phoneNumber = countryCode + mobile;
        //https://numverify.com/dashboard
        return this.http.get('http://apilayer.net/api/validate?access_key=3ea142c5175b7efae4987ea42cd184cd&number=' + phoneNumber);
      }));
    }
    catch (e) {
      this.appLogException(new Error('Exception in getCountryCodeJSON() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }


  GetFieldStatusFromConfigRecords(EntityId, EntityType) {
    // try{
    //   let message = this.getMessage(FABRICS.FDC);
    //   message.EntityID = EntityId;
    //   message.EntityType = EntityType;
    //   message.MessageKind = MessageKind.READ;
    //   message.Fabric = FABRICS.FDC.toUpperCase();
    //   message.DataType = "EM_HIERARCHY_READ";
    //   return this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message).pipe(map((res: string) => {
    //     var messageData = typeof res === 'string' ? JSON.parse(res) : res;
    //     if (messageData.Payload) {
    //       return  messageData;
    //     } else {
    //       return null
    //     }
    //   }));
    // }
    // catch(e){
    //   this.appLogException(new Error('Exception in GetFieldStatusInConfig() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));

    // }

  }


  getMessageDefaultProperties(message?: MessageModel) {
    if (message == null)
      message = new MessageModel();
    message.MessageId = UUIDGenarator.generateUUID();
    message.ClientID = this.clientGuid;
    message.TenantName = this.tenantName;
    message.TenantID = this.tenantID;
    message.UserID = this.currentUserId;
    message.CapabilityId = this.capabilityFabricId;
    message.SourceSystem = "Visur";
    message.Routing = Routing;
    message.MessageKind = MessageKind;
    message.ApplicationName = AppConfig.AppMainSettings.env.ApplicationName;
    if (this.lastOpenedFabric) {


      message.Fabric = this.lastOpenedFabric.toUpperCase();

    } else {
      message.Fabric = "HOMEFABRIC";
    }
    message;
    return message;
  }
  getMessage(fabric?, entityid?, entityType?, messageType?, datatype?, routing?, messageKind?, payload?): MessageModel {
    try {
      let fabricName = this.getFabricNameByUrl(this.router.url);
      var message = new MessageModel();
      message.MessageId = UUIDGenarator.generateUUID();
      message.ClientID = this.clientGuid;
      message.TenantName = this.tenantName;
      message.TenantID = this.tenantID;
      message.UserID = this.currentUserId;
      message.CapabilityId = this.getCapabilityIdForMsg(fabric, fabricName);
      message.SourceSystem = "Visur";
      message.Type = messageType;
      message.Routing = routing;
      message.MessageKind = messageKind;
      message.Payload = payload;
      message.DataType = datatype;
      message.EntityID = entityid;
      message.EntityType = entityType;
      message.ApplicationName = AppConfig.AppMainSettings.env.ApplicationName;
      if (fabricName == FabricsNames.SECURITY) {
        message.Fabric = fabricName;
      }
      else if (this.lastOpenedFabric) {
        message.Fabric = ([FabricsNames.ENTITYMANAGEMENT].includes(fabricName)) && fabric ? fabric.toUpperCase() : this.lastOpenedFabric.toUpperCase();
      }
      else {
        message.Fabric = "HOMEFABRIC";
      }

      message;
      return message;
    }
    catch (e) {
      this.appLogException(new Error('Exception in getMessage() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }



  getCapabilityIdForMsg(fabric, fabricName) {
    if (([FabricsNames.ENTITYMANAGEMENT, FABRICS.BUSINESSINTELLIGENCE, FABRICS.CONSTRUCTION].includes(fabricName)) && fabric)
      return this.getCapabilityId(fabric);
    else
      return this.capabilityFabricId;
  }
  GetNewUUID() {
    try {
      return UUIDGenarator.generateUUID();
    }
    catch (e) {
      this.appLogException(new Error('Exception in GetNewUUID() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }
  getDateBasedOnGMT(date) {
    try {
      var userTimezoneOffset = date.getTimezoneOffset() * 60000;
      return new Date(date.getTime() - userTimezoneOffset);
    } catch (error) {

    }
  }
  getCurrentDate() {
    let date = new Date();
    let month = date.getUTCMonth() + 1;
    var lastest_date = date.getUTCFullYear() + '-' + month + '-' + date.getUTCDate();
    return lastest_date;
  }
  GetFabricName(url) {
    try {
      switch (url) {
        case "/peoplefabric":
          return "People";
        case "/bifabric":
          return "BI";
        case "/EntityManagement":
          return "EntityManagement";
      }
    }
    catch (e) {
      this.appLogException(new Error('Exception in GetFabricName() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }
  GetFabricURL(name) {
    try {
      switch (name) {
        case "People":
          return FABRICS.PEOPLEANDCOMPANIES;
        case "BI":
          return 'bifabric';
      }
    }
    catch (e) {
      this.appLogException(new Error('Exception in GetFabricURL() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  localtoTimestamp() {
    try {
      var curDate = new Date();
      var epoch = curDate.getTime() / 1000;
      return epoch.toString();
    }
    catch (e) {
      this.appLogException(new Error('Exception in localtoTimestamp() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  /*
   * To prevent default browser context menu
   */
  onContextMenuPrevent(event) {
    event.preventDefault();
  }

  //get entitycapabilities and roles
  getEntityCapabilityRoles(entityId) {
    try {
      var permissions = this.http.get(this.baseUrl + `api/AlThings/GetEntityCapabilities?entityId=` + entityId + "&tenantId=" + this.tenantID)
        .pipe(map((response: any) => {
          var data = response;
          if (data) {
            return data;
          } else {
            return null;
          }
        }));
      return permissions;
    }
    catch (e) {
      this.appLogException(new Error('Exception in getEntityCapabilityRoles() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }



  getUrl(key) {
    try {
      var url = this.baseUrl + 'Home/getUrl?key=' + key;

      return this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "get", url).pipe(map((res: any) => {
        return res;
      }));


    } catch (e) {
      this.appLogException(new Error('Exception in getUrl() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  // getData(key) {
  //   try {
  //     if (this.data) {
  //       return of(this.data);
  //     } else if (this.observable) {
  //       return this.observable;
  //     } else if (key == "GeoServer") {
  //       var url = this.baseUrl + 'Home/getUrl?key=' + key;
  //       return this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "get", url);
  //     } else {
  //       var url = this.baseUrl + 'Home/getUrl?key=' + key;
  //       this.observable = this.http.get(url).pipe(map((response: any) => {
  //         this.observable = null;
  //         if (response.status == 400) {
  //           return "FAILURE";
  //         } else if (response.status == 200) {
  //           this.data = new UrlCacheData(response._body);
  //           return this.data;
  //         }
  //       })).pipe(share());
  //       return this.observable;
  //     }
  //   }
  //   catch (e) {
  //     this.appLogException(new Error('Exception in getData() of commonservice at time ' + new Date().toString() + '. Exception is : ' + e));
  //   }

  // }

  checkEmailExist(email, key): Observable<any> {
    try {
      this.loadingBarAndSnackbarStatus("start", "validating email...");
      setTimeout(() => {
        this.loadingBarAndSnackbarStatus("", "");
    }, 1000);

      if (email != null || email != "") {
        return this.http.get(this.baseUrl + 'Home/UserEmailValidationinLdap?tenantName=' + this.tenantName + "&email=" + email);

      }
    }
    catch (e) {
      this.appLogException(new Error('Exception in checkEmailExist() of commonservice at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }

  printException(method, component, e) {
    this.appLogException(new Error('Exception in ' + method + '() of ' + component + ' at time ' + new Date().toString() + '. Exception is : ' + e));
  }

  recieveMessageFromMessagingService(message: MessageModel) {
    try {
      switch (message.MessageKind.toUpperCase()) {
        case MessageKind.CREATE:
          this.recieveMessageFromBackend_CREATE(message);
          break;
        case MessageKind.READ:
          this.recieveMessageFromBackend_READ(message);
          break;
        case MessageKind.UPDATE:
          this.recieveMessageFromBackend_UPDATE(message);
          break;
        case MessageKind.DELETE:
          this.recieveMessageFromBackend_DELETE(message);
          break;
        case MessageKind.PARTIALUPDATE:
          this.recieveMessageFromBackend_PARTIALUPDATE(message);
          break;
        default:
          console.warn('CommonService at RecieveMessageFromMessagingService ,Switch MessageKind' + message);
          break;
      }

      this.SuccessAndDBErrorMessage(message);

      setTimeout(() => {
        this.loadingBarAndSnackbarStatus("", "");
      }, 3000);
    }
    catch (e) {
      this.appLogException(new Error('Exception in recieveMessageFromMessagingService() of commonservice at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  recieveMessageFromBackend_CREATE(message: MessageModel) {
    console.log("create msg...");
  }

  recieveMessageFromBackend_READ(message: MessageModel) {
    if (message.MessageKind.toUpperCase() == "READ" && message.Type == "Details" && message.EntityID == this.currentUserId && message["DataType"] == "EntityInfo" && message['EntityType'] == "Person") {
      let Payload = JSON.parse(message.Payload);
      if (Payload['Data'] != "" && Payload['Data'] != null) {
        let personData = JSON.parse(Payload['Data']);
        if (personData != null && personData["EntityName"] != null)
          this.CurrentUserEntityName = personData["EntityName"];
        if (personData != null && personData["UserImage"] != null)
          this.userProfilePicture = personData["UserImage"];
        this.userSettingForm.next(message);
        this.loadingBarAndSnackbarStatus("", "Reading data");
      }
    }
  }

  recieveMessageFromBackend_UPDATE(message: MessageModel) {
    console.log('Socket update msg...');
  }

  recieveMessageFromBackend_DELETE(message: MessageModel) {
    console.log('Socket delete msg...');
  }

  recieveMessageFromBackend_PARTIALUPDATE(message: MessageModel) {
    console.log('Socket partial update msg...');
  }


  SuccessAndDBErrorMessage(message: MessageModel) {
    if (message["DataType"] == "SUCCESS") {

    }
    else if (message["DataType"] == "DBError" || message["DataType"] == "Error") {
      this.crudResponse.next("ERROR");
      if (message["DataType"] == "DBError") {
        this.loadingBarAndSnackbarStatus("", "DataBaseError");
        if (message.MessageKind == MessageKind.UPDATE) {
          var Data = JSON.parse(message["Payload"]);
          this.errorDetails = Data["ExceptionMessage"];
        } else {
          this.errorDetails = message.Payload;
        }
        this.DBFailErrorHandling$.next(message);
      }
      else if (message["DataType"] == "Error") {
        this.loadingBarAndSnackbarStatus("", "Error");
        this.errorDetails = message.Payload;
        this.appLogException(new Error(JSON.stringify(message)));
      }
    }
    else {
      this.sendMessagesToFabricServices.next(message);
    }
  }

  /**
   * Return Observable for the entityId Object data
   */
  getEntitiesById$(entityId) {
    try {
      return this.entityCommonStoreQuery.selectEntity(entityId);
    }
    catch (e) {
      this.appLogException(new Error("getEntitiesById$ " + e));
    }
  }

  /**
   *
   * @param entityIds List of EntityId e.g. ["123","xxxxx-xxxx-xx-xxxxx-xxxx"]
   * @param mapperFunc function to map data e.g. d=>{return {EntityId:d.EntityId,EntityType:d.EntityType}}
   */
  getEntitiesByIds(entityIds, filterFunc, mapperFunc) {
    return this.entityCommonStoreQuery.getAll({ filterBy: (entity: any) => entity && entity.EntityId != "" && entityIds.indexOf(entity.EntityId) >= 0 })
      .filter(filterFunc ? filterFunc : true)
      .map(mapperFunc);
  }

  getEntitiesArrayByIds(entityIds, filterFunc, mapperFunc: any) {
    return this.entityCommonStoreQuery.selectAll({ filterBy: (entity: any) => entity && entity.EntityId != "" && entityIds.indexOf(entity.EntityId) >= 0 })
      .flatMap(d => from<any>(d)
        .filter(filterFunc ? filterFunc : true)
        .map(mapperFunc).toArray());
  }

  /**
   *
   * @param entityId "EntityId of the object"
   */
  getEntitiesById(entityId) {
    let entityData = this.entityCommonStoreQuery.getEntity(entityId);
    return entityData ? JSON.parse(JSON.stringify(entityData)) : null;
  }

  /**
   *
   * @param fabric "EntityId of the string"
   * @param entityIds "EntityId of the Array"
   */
  removeEntitiesByIds(fabric: string, entityIds: Array<any>) {
    let capabilityId = this.getCapabilityId(fabric);
    this.entityCommonStoreQuery.deleteInSGAndCapabilities(fabric, capabilityId, entityIds);
  }

  deleteEntitiesByIds(entityIds: any[]) {
    this.entityCommonStoreQuery.deleteNodeByIds(entityIds);
  }

  resetEntityStore() {
    this.entityCommonStoreQuery.resetStore();
  }



  getEntitiesByParent(parentId, entityType, filterFunc, mapperFunc, fabric): Array<any> {
    return this.entityCommonStoreQuery.getAll({
      filterBy: (entity: any) => entity && entity.SG && (<any[]>entity.SG).length > 0 && (<any[]>entity.SG).indexOf(fabric) >= 0 && (entityType ? entity.EntityType == entityType : true) && entity.Parent && entity.Parent.length > 0 && entity.Parent[0].EntityId == parentId
    }).filter(filterFunc ? filterFunc : true).map(mapperFunc);
  }
  getEntityDataByParent(parentId, entityType, filterFunc, mapperFunc, fabric): Array<any> {
    return this.entityCommonStoreQuery.getAll({
      filterBy: (entity: any) => entity && entity.SG && (<any[]>entity.SG).length > 0 && (<any[]>entity.SG).indexOf(fabric) >= 0 && (entityType ? entity.EntityType == entityType : true) && entity.Downstream && entity.Downstream.length > 0 && entity.Downstream[0].EntityId == parentId
    }).filter(filterFunc ? filterFunc : true).map(mapperFunc);
  }


  getEntityParent(entityId) {
    let entityData: any = this.entityCommonStoreQuery.getEntity(entityId);
    let parent = entityData ? entityData.Parent : undefined;
    if (parent && (<Array<any>>parent).length > 0) {
      return this.entityCommonStoreQuery.getEntity(parent[0].EntityId);
    } else {
      return undefined;
    }
  }
  /**
   *
   * @param type "EntityType"
   * @param types ["Person","Company"]
   * @param mapValues "d=>{return {EntityId:d.EntityId,EntityType:d.EntityType,.....}}"
   * @param capability fabric name optional
   */
  getEntititesBy(type, types: Array<any>, mapValues, capability?) {
    return this.entityCommonStoreQuery
      .getAll().filter((d: any) => {
        return types.indexOf(d[type]) >= 0 && (d["SG"] && d["SG"].indexOf(capability) >= 0);
      }).map(mapValues);
  }

  /**
   *
   * @param type
   * @param types
   * @param capability
   */
  getEntititesBy$(type, types: Array<any>, capability?) {
    return this.entityCommonStoreQuery
      .selectAll({
        filterBy: (d: any) => types.indexOf(d[type]) >= 0
      })
      .debounceTime(150);
  }


  getEntitiesByParents(parents, filterFunc, mapperFunc, fabric): Array<any> {
    return this.entityCommonStoreQuery.getAll({
      filterBy: (entity: any) => entity && entity.SG && (<any[]>entity.SG).length > 0 && (<any[]>entity.SG).indexOf(fabric) >= 0 && entity.Parent && entity.Parent.length > 0 && parents && (entity.Parent[0].EntityId == parents.locationId || entity.Parent[0].EntityId == parents.downstreamId)
    }).filter(filterFunc ? filterFunc : true).map(mapperFunc);
  }

  getEntititesFromStore(type, types: Array<any>, filterFunc, mapperFunc, capability?) {
    let capabilityId = this.getCapabilityId(capability);
    return this.entityCommonStoreQuery.getAll({
      filterBy: entity => entity && (types.indexOf(entity[type]) >= 0) && (capabilityId != undefined ? (entity["Capabilities"] != undefined ? entity["Capabilities"].indexOf(capabilityId) >= 0 : true) : true)
    }).filter(filterFunc).map(mapperFunc);
  }

  getEntititesFromStore$(type, types: Array<any>, filterFunc, mapperFunc, capability?) {
    let capabilityId = capability ? this.getCapabilityId(capability) : capability;
    return this.entityCommonStoreQuery.selectAll({
      filterBy: entity => entity && (types.indexOf(entity[type]) >= 0) && (capabilityId != undefined ? (entity["Capabilities"] != undefined ? entity["Capabilities"].indexOf(capabilityId) >= 0 : true) : true)
    })
      .flatMap(d => from(d).filter(d => {
        return types.indexOf(d[type]) >= 0;
      }).filter(filterFunc).map(mapperFunc).toArray());
  }


  getEntititesFromStoreBasedOnSG$(type, types: Array<any>, filterFunc, mapperFunc, capability?) {
    return this.entityCommonStoreQuery.selectAll({
      filterBy: entity => entity && (types.indexOf(entity[type]) >= 0) && (entity["SG"] && entity["SG"].indexOf(capability) >= 0)
    })
      .flatMap(d => from(d).filter(d => {
        return types.indexOf(d[type]) >= 0;
      }).filter(filterFunc).map(mapperFunc).toArray());
  }



  getEntititesByTypeOf$(typeOf) {
    return this.entityCommonStoreQuery
      .selectAll()
      .debounceTime(150);
  }

  getListTreeData$(capability: string) {
    return this.getEntititesFromStore$("EntityType", [EntityTypes.List], d => true, d => { return d; }, capability).first();
  }

  getCompanyEntititesFromStore$(type, types: Array<any>, filterFunc, mapperFunc, capability?) {
    let capabilityId = capability ? this.getCapabilityId(capability) : capability;
    return this.entityCompanyTypesStoreQuery.selectAll({
      filterBy: entity => entity && (types.indexOf(entity[type]) >= 0) && (capabilityId != undefined ? (entity["Capabilities"] != undefined ? entity["Capabilities"].indexOf(capabilityId) >= 0 : true) : true)
    })
      .flatMap(d => from(d).filter(d => {
        return types.indexOf(d[type]) >= 0;
      }).filter(filterFunc).map(mapperFunc).toArray());
  }


  /**
 *  Get all children's hierarchy entityIds
 *
 * @param {string} parentId
 * @param {any[]} enityIds
 * @returns {any[]}
 */
  getEntityIdsOfChildren(parentId: string, fabric: string): any[] {
    try {
      let entities: any[] = this.getEntitiesByParent(parentId, undefined, d => true, d => { return d; }, fabric);
      return entities.reduce((tempIds, entity) => {
        console.log("tempIds: ", tempIds);
        return [...tempIds, ...this.getEntityIdsOfChildren(entity.EntityId, fabric)];
      }, [parentId]);
    }
    catch (e) {
      this.appLogException(new Error('Exception in getEntityIdsOfChildren() of CommonService  at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  updateListEntities(tempjson: any, rootNode: string, key: string): Array<any> {
    try {
      let entities: Array<any> = [];
      let treejson: Array<any> = JSON.parse(JSON.stringify(tempjson));

      if (treejson[0] && treejson[0][key] && treejson[0][key].length > 0) {
        treejson[0][key].forEach(item => {
          if (!item.Parent)
            item.Parent = [{ "EntityId": rootNode, "EntityName": rootNode }];

          if (item.entities) {
            item.children = [];
            item.entities.forEach(entity => {
              if (entity["EntityName"]) {
                entity['EntityOrder'] = entity['entities|order'];
                entity['Parent'] = [{ "EntityId": item.EntityId, "EntityName": item.EntityName }];
                item.children.push(entity);

                delete entity['entities|order'];
              }
            });
            delete item.entities;
          }
          else
            item.children = [];
        });
        entities = treejson[0][key];
      }
      return entities;
    }
    catch (e) {
      this.appLogException(new Error('Exception in updateListEntities() of CommonService in at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  addListEntitiesToState(payload: any, fabric: string) {
    try {
      let personalData = this.updateListEntities(payload['personal'], EntityTypes.Personal, 'list');
      let sharedData = this.updateListEntities(payload['shared'], EntityTypes.Shared, 'list');
      let rootsArray: Array<any> = [];
      let entities: Array<any> = [...personalData, ...sharedData];

      if (fabric == FABRICS.BUSINESSINTELLIGENCE) {
        let CarouselData = this.updateListEntities(payload['carousel'], EntityTypes.Carousel, 'carousel');

        rootsArray = this.createDefaultList([EntityTypes.Personal, EntityTypes.Shared, EntityTypes.Carousel], fabric);
        entities = [...entities, ...CarouselData];
      }
      else {
        rootsArray = this.createDefaultList([EntityTypes.Personal, EntityTypes.Shared], fabric);
      }
      entities = [...entities, ...rootsArray];
      let tempEntities: any = this.updateSGAndCapabilities(entities, fabric);

      if (tempEntities['ExistingEntities'].length > 0) {
        let props = { "SG": fabric, "Capabilities": this.capabilityFabricId };
        this.entityCommonStoreQuery.updateEntityProperties(tempEntities['ExistingEntities'], props);
      }

      if (tempEntities['NewEntities'].length > 0)
        this.entityCommonStoreQuery.AddAll(tempEntities['NewEntities']);
    }
    catch (e) {
      console.error(e);
      this.appLogException(new Error('Exception in addListEntitiesToState() of CommonService in at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  updateSGAndCapabilities(entities: Array<any>, fabric: string): any {
    try {
      let capabilityFabricId = this.getCapabilityId(fabric);
      let allEntities = { 'ExistingEntities': [], 'NewEntities': [] };
      let entitiesList: Array<any> = JSON.parse(JSON.stringify(entities));

      for (var index in entitiesList) {
        if (entitiesList[index]) {
          let entityId = entitiesList[index].EntityId;
          let entityData = this.entityCommonStoreQuery.getEntity(entityId);
          if (entityData) {
            if (entityData["SG"] != undefined) {
              if (!((<Array<any>>entityData["SG"]).indexOf(fabric) >= 0) && !((<Array<any>>entityData["Capabilities"]).indexOf(capabilityFabricId) >= 0)) {
                allEntities['ExistingEntities'].push(entityId);
              }
              continue;
            }
          }
          entitiesList[index]["SG"] = [fabric];
          entitiesList[index]['TypeOf'] = EntityTypes.List;
          entitiesList[index]['Capabilities'] = [capabilityFabricId];
          allEntities['NewEntities'].push(entitiesList[index]);
        }
      }
      return allEntities;
    }
    catch (e) {
      this.appLogException(new Error('Exception in updateSGAndCapabilities() of CommonService in at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  createDefaultList(roots: Array<any>, capability): Array<any> {
    try {
      let capabilityId = this.getCapabilityId(capability);
      let treeRoots: Array<any> = [];
      for (let index in roots) {
        let root = roots[index];
        let rootNode = { "children": [], "EntityId": root, "Type": root, "TypeOf": root, "EntityName": root, "EntityType": root, "Parent": [], "SG": [capability], "Capabilities": [capabilityId] };
        treeRoots.push(rootNode);
      }
      return treeRoots;
    }
    catch (e) {
      this.appLogException(new Error('Exception in createDefaultList() of CommonService in at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  createListEntities(listArray: Array<any>, fabric: string): Array<any> {
    try {
      let treeRoots: Array<any> = [];
      let tempListArray: Array<any> = [];
      if (listArray && listArray.length > 0) {
        listArray.filter((entity: any) => {
          if (entity && entity.SG && entity.SG.indexOf(fabric) >= 0) {
            if ([EntityTypes.Personal, EntityTypes.Shared, EntityTypes.Carousel].indexOf(entity.EntityType) >= 0) {
              entity.children = [];
              treeRoots.push(entity);
            }
            else
              tempListArray.push(entity);
          }
        });

        tempListArray.forEach((element) => {
          let entityIds = [];
          if (element.children) {
            element.children.forEach(d => {
              try {
                let entity: any = this.getEntitiesById(d.EntityId);
                if (entity && entity["SG"] && ((<Array<any>>entity["SG"]).indexOf(fabric) >= 0))
                  d.EntityName = entity.EntityName;
                else
                  entityIds.push(d.EntityId);
              }
              catch (e) {
                console.log(e);
              }
            });

          }

          treeRoots.forEach((root: any) => {
            if (root.EntityId == element.Parent[0].EntityId) {
              root.children.push(element);
            }
          });
        });
        treeRoots.forEach((root: any) => {
          root.children = this.sortByEntityName((<any[]>root.children));
        });
      }

      //list tree remove unwanted properties
      let deletpropertiesFromArray = JSON.parse(JSON.stringify(treeRoots));
      let rootsArray = this.createListTreePayload(deletpropertiesFromArray);

      return rootsArray;
    }
    catch (e) {
      this.appLogException(new Error('Exception in createListEntities() of CommonService in at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  createListTreePayload(list: Array<any>) {
    let treeRootsArray = [];

    //personal, shared
    list.forEach((element, index) => {//root element
      if (element.children && element.children.length) {
        var listTypes = [];
        element.children.forEach(listTypeElement => { //list type
          let listObject = this.elementToDeleteProperties(listTypeElement);

          if (listObject.children && listObject.children.length) {
            var locationsArray = [];
            listObject.children.forEach(locationObject => { //location types
              let locationObjectDel = this.elementToDeleteProperties(locationObject);
              locationsArray.push(locationObjectDel);
            });
          }

          listObject.children = locationsArray;

          listTypes.push(listObject);
        });
      }
      element.children = listTypes;
      treeRootsArray.push(element);
    });

    let roots = [];
    treeRootsArray.forEach((rootElmnt, index) => {
      let rootElement = this.elementToDeleteProperties(rootElmnt);
      roots.push(rootElement);
    });

    return roots;
  }

  elementToDeleteProperties(deleteObject) {
    let propertyToDelete = ["capability", "Created", "owner", "SG", "Capabilities", "TimeZone"];
    //let deleteObject = JSON.parse(JSON.stringify(element));
    for (var deleteProperty in propertyToDelete) {
      let property = propertyToDelete[deleteProperty];

      if (property && deleteObject[property])
        delete deleteObject[property];
    }
    return deleteObject;
  }

  CheckExistEntityProperty(propertyname, value, fabric) {
    return this.entityCommonStoreQuery
      .getAll().filter((d: any) => {
        return d && d[propertyname] == value && d.SG && (<Array<any>>d.SG).indexOf(fabric) >= 0;
      }).length > 0 ? true : false;
  }

  getAddNewEntitySchemaAndDatamodel(fabric) {
    try {
      var payload = null;
      var message = this.getMessage();
      switch (fabric) {
        case FABRICS.PEOPLEANDCOMPANIES:
          message.Type = "Details";
          payload = { "EntityType": EntityTypes.People, "Fabric": FABRICS.PEOPLEANDCOMPANIES };
          message.EntityType = "PeopleAddNew";
          message.EntityID = "PeopleSchema";
          message.DataType = "People";
          break;

        // case FABRICS.DRAWINGSFABRIC:
        //   message.Type = "Details";
        //   payload = { "EntityType": "AddNewDrawing", "Fabric": "Drawings" };
        //   message.EntityType = "AddNewDrawing";
        //   message.DataType = Datatypes.PALLETE;
        //   break;

        case FABRICS.BUSINESSINTELLIGENCE:
        case "BI":
          message.Type = "Details";
          payload = { "EntityType": "AddNewBi", "Fabric": "Bi" };
          message.EntityType = "AddNewBi";
          message.DataType = "Bi";
          break;

        // case FABRICS.TASKFABRIC:
        //   message.Type = "Details";
        //   payload = { "EntityType": "AddNewTask", "Fabric": "TaskFabric" };
        //   message.EntityType = "AddNewTask";
        //   message.DataType = Datatypes.PALLETE;
        //   break;

        // case FABRICS.ACTIVITYCENTER:
        //   message.Type = "Details";
        //   payload = { "EntityType": "Schema", "Fabric": "FDC", "Info": "Admin", "Datatype": "FDCData" };
        //   message.EntityType = "AddNew";
        //   message.DataType = Datatypes.PALLETE;
        //   break;

        //   break;
      }
      message.Routing = Routing.OriginSession;
      message.MessageKind = MessageKind.READ;
      message.Payload = JSON.stringify(payload);
      return message;
    }
    catch (e) {
      this.appLogException(new Error('Exception in getAddNewEntitySchemaAndDatamodel() of commonservice at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  updateCapabilityList(lists) {
    if (lists != null) {
      lists.forEach((list, index) => {
        if (this.appConfig.AllHeaderRouting[index]['Fabric'] == list['Fabric'])
          this.appConfig.AllHeaderRouting[index]['queryParameter'] = list['queryParameter'];
      });
    }
  }

  GetProdDateBasedonTimeZone(timezone): Observable<Date> {
    try {

      return this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.baseUrl + 'api/AlThings/ProductionDate', {}, timezone).map((res: any) => {
        return this.GetProdDateBasedonTimeZonefromserver(res);
      });
    }
    catch (e) {
      console.error('Exception in GetProdDateBasedonTimeZone() of FDCService in FDC/Service at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in GetProdDateBasedonTimeZone() of FDCService in FDC/Service at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }
  GetProdDateBasedonTimeZonefromserver(res) {
    let maxDate = new Date(res);
    maxDate.setHours(8, 0, 0, 0);
    return maxDate;
  }
  userPreferenceData(message, isUpdate?) {
    try {
      var payload = message['Payload'];
      if (payload != '' && payload != null) {
        var prefJson = JSON.parse(payload);
        if (prefJson.Fabric && message.Fabric != FabricsPath.HOME.toLocaleUpperCase())
          this.changeFavIcon(prefJson.Fabric);

        this.saveUserprefrenceDataToState(message, isUpdate);

        if (message.DataType == EntityTypes.UserPreference + "Lists") {
          var jsonData = JSON.stringify(prefJson["Categories"]);
          localStorage.setItem("Categories", jsonData);
        } else {
          for (var key in prefJson) {

            if (prefJson[key] == "HOME" && this.onlineOffline == false && this.CustomerAppsettings.env.UseAsDesktop == true) {
              this.HomeFabricSettingJson.next(null);
            }
            else if (key == "Tasks") {
              var jsonData = JSON.stringify(prefJson[key]);
            }
            else if (key == "Theme") {
              console.log("Theme change debugging start.....");
              this.themeDark = prefJson[key];
              console.log("Theme " + this.themeDark);
              this.themeDark = !this.themeDark;
              console.log("Theme " + this.themeDark);

              if (this.changeTheme.observers.length == 0) {
                console.log("Theme change debugging this.changeTheme.observers.length.....");
                setTimeout(() => {
                  this.changeTheme.next("updated");
                }, 1000);
              } else {
                console.log("Theme change debugging this.changeTheme.observers.length else.....");
                this.changeTheme.next("updated");
              }
            }
            else if (key == 'ZoomPercentage') {
              this.ZoomPercentage = prefJson[key];
            }
            else if (key == 'UserPinnedTabs') {
              var value = prefJson[key];
              if (value.length != 0 && value != "[]") {
                if (isUpdate)
                  this.faviconList = prefJson[key];
                this.faviconList.forEach((pinList) => {
                  this.appConfig.AllHeaderRoutingList.forEach((list: any) => {
                    if (pinList.Fabric == list.Fabric)
                      list.isFavIcon = true;
                  });
                });
              }
            }

            else if (key.toUpperCase() == 'HOME') {
              this.HomeFabricSettingJson.next(prefJson[key]);
            }
            else if (key == 'Fabric') {
              if (this.CustomerAppsettings.env.UseAsDesktop == true) {
                var checkFabric = window.location.pathname.split('/');
                var checkIndex, homeFabricExist = false;
                checkFabric.forEach(obj => {
                  if (obj == "Home") {
                    homeFabricExist = true;
                    checkIndex = checkFabric.indexOf(obj);
                  }
                });
                if (homeFabricExist) {
                  pathArray = window.location.pathname.split('/')[checkIndex];
                }
                else {
                  pathArray = window.location.pathname.split(window.location.pathname)[2];
                }
              }
              else {
                pathArray = window.location.pathname.split('/')[2];
              }

              this.fabricToDisplay = pathArray ? decodeURIComponent(pathArray) : prefJson[key];
              if (!this.fabricToDisplay || this.fabricToDisplay == "HOME") {
                this.fabricToDisplay = FABRICS.HOME;
              }
              this.showtoggle = prefJson[key] && prefJson[key].toUpperCase() == "HOME" ? false : true;
              let selectedList;
              let length = this.appConfig.AllHeaderRoutingList.length;
              this.appConfig.AllHeaderRoutingList.forEach((list) => {
                if (list.title && list.title == this.fabricToDisplay) {
                  let userPrefData = this.usersStateQuery.getStateKey(UserStateConstantsKeys.userPrefrence);
                  if (userPrefData && userPrefData[list['title']] && userPrefData[list['title']]["SetAsDefault"]) {
                    if (list.Fabric == FABRICS.ENTITYMANAGEMENT && length <= 2) {//siva

                    } else {
                      let tab = userPrefData[list['title']]["SetAsDefault"]["Value"];
                      list.queryParameter.tab = tab;
                      if (list.Fabric.toLowerCase() == FabricsNames.SECURITY.toLowerCase()) {
                        let tablist = this.getAdminFabriclist();
                        if (!tablist.find(hCItem => hCItem == list.queryParameter.tab.replace(" Role", ""))) {
                          delete list.queryParameter.tab;
                          list.queryParameter.leftnav = false;
                        } else {
                          list.queryParameter.leftnav = true;
                        }
                      }
                      else if (list.Fabric.toLowerCase() == FabricsNames.ENTITYMANAGEMENT.toLowerCase()) {
                        let tablist = this.getAdminFabriclist(); //if user dont have Default tab access then Close left tree
                        if (!tablist.find(hCItem => hCItem == list.queryParameter.tab)) {
                          delete list.queryParameter.tab;
                          list.queryParameter.leftnav = false;
                        }
                      }
                    }

                  }
                  list.routerLinkActive = true;
                  this.capabilityFabricId = list.entityId;
                  this.currentRoutingSource = list.sourceSub;
                  selectedList = list;
                }
              });
              var pathArray;

              if (pathArray == '' || pathArray == 'Home') {
                if (selectedList) {
                  this.router.navigate([selectedList.routerLink], { queryParams: selectedList.queryParameter });
                }
                else {
                  this.router.navigate(['/Home']);
                }
              }
            }
          }
        }
      }
    }
    catch (e) {
      this.appLogException(new Error('Exception in userPreferenceData() of commonservice at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }
  updatelastopenedfabricinuserpreferenc(fabricname) {
    try {
      let payload = { "Fabric": fabricname, };
      var dateTime = new Date().toISOString().slice(0, 19).replace('T', ' ');

      var userPayload = { "email": this.username, "cacheName": "TenantDetails", "UpdatePref": payload, "ModifiedDateTime": dateTime };
      this.sendMessageToServer(JSON.stringify(userPayload), EntityTypes.UserPreference, this.currentUserName, EntityTypes.UserPreference, MessageType.UPDATE, Routing.AllFrontEndButOrigin, '', FABRICS.COMMON.toUpperCase());
      //this.updateIndexDBUserPrefrence(null, null, null, "Fabric", fabricname);

    } catch (error) {
      this.appLogException(new Error('Exception in updatelastopenedfabricinuserpreferenc() of commonservice at time ' + new Date().toString() + '. Exception is : ' + error));
    }

  }
  otherSessionUserprefrenceDataToState(message) {
    try {
      if (CommonJsMethods.isvalidJson(message.Payload)) {
        var payload = JSON.parse(message.Payload);
        if (payload.hasOwnProperty(UserStateConstantsKeys.windowCloseTime)) {
          console.log("CommonService windowCloseTime...");
          this.usersStateQuery.add(UserStateConstantsKeys.windowCloseTime, payload[UserStateConstantsKeys.windowCloseTime]);
          delete payload[UserStateConstantsKeys.windowCloseTime];
        }
        delete payload["Theme"];//due backend message receieving continously
        this.usersStateQuery.updateObjectMultipleValue(UserStateConstantsKeys.userPrefrence, payload);
      }
      this.updateStateModifiedTime();
    }
    catch (e) {
      this.appLogException(new Error(e));
    }
  }

  updateStateModifiedTime() {
    var utc = Math.floor((new Date()).getTime() / 1000);
    this.usersStateQuery.add(UserStateConstantsKeys.modifiedTime, utc);
  }

  deleteResetStoreTime() {
    let flag = 0;
    while (flag < 2) {
      if (this._cookieService.get("reSetStore")) {
        this._cookieService.remove("reSetStore");
      }
      flag++;
    }
  }

  saveUserprefrenceDataToState(message, isUpdate?) {
    try {
      var payload = JSON.parse(message['Payload']);
      payload["UserId"] = message.UserID;
      let fabricDefaultTab = {};
      if (payload) {
        UserStateConstantsKeys.setAsDefaultFabrics.forEach(userPrefrencePropty => {
          if (!payload[userPrefrencePropty]) {//new fabric adding into userprefrence
            payload[userPrefrencePropty] = {};
            if (userPrefrencePropty == UserStateConstantsKeys.EntityManagement)
              payload[userPrefrencePropty][UserStateConstantsKeys.SetAsDefault] = UserStateConstantsKeys.EntityManagementLeftTreeDefaultValue;
            else
              payload[userPrefrencePropty][UserStateConstantsKeys.SetAsDefault] = UserStateConstantsKeys.LeftTreeSetAsDefaultValue;
          }
          else if (payload[userPrefrencePropty] && !payload[userPrefrencePropty].hasOwnProperty(UserStateConstantsKeys.SetAsDefault)) {//fabric key but does not have setasdefult property
            if (userPrefrencePropty == UserStateConstantsKeys.EntityManagement)
              payload[userPrefrencePropty][UserStateConstantsKeys.SetAsDefault] = UserStateConstantsKeys.EntityManagementLeftTreeDefaultValue;
            else
              payload[userPrefrencePropty][UserStateConstantsKeys.SetAsDefault] = UserStateConstantsKeys.LeftTreeSetAsDefaultValue;
          }
          //if fabric tree tab get removed write logic

          fabricDefaultTab[userPrefrencePropty] = payload[userPrefrencePropty][UserStateConstantsKeys.SetAsDefault]["Value"];
        });

        let capabilitiesLength = this.appConfig.AllHeaderRoutingList.length;
        Object.keys(fabricDefaultTab).forEach(fabricName => {
          let title = this.getAllHeaderRoutingFabricNameForUserPrefrence(fabricName);
          this.appConfig.AllHeaderRoutingList.forEach((list) => {
            if (list.Fabric == title) {
              if (list.Fabric == FABRICS.ENTITYMANAGEMENT && capabilitiesLength <= 2) {//siva

              } else {
                list.queryParameter['tab'] = fabricDefaultTab[fabricName];
                list.queryParameter['leftnav'] = 'true';
              }

            }
          });
        });

        if (!isUpdate && !this.faviconList.length && payload["UserPinnedTabs"] && payload["UserPinnedTabs"].length) {
          this.faviconList = payload["UserPinnedTabs"];

          this.faviconList.forEach((pinList) => {
            this.appConfig.AllHeaderRoutingList.forEach((list: any) => {
              if (pinList.Fabric == list.Fabric)
                list.isFavIcon = true;
            });
          });
        }

        //delete properties from user prefrence
        UserStateConstantsKeys.deleteKeysFromUserPrefrence.forEach(key => {
          delete payload[key];
        });
      }

      if (payload[UserStateConstantsKeys.FieldDataCapture] && !payload[UserStateConstantsKeys.FieldDataCapture].hasOwnProperty(FDCStateConstantsKeys.userPrefrenceSetAsDefault)) {
        payload[UserStateConstantsKeys.FieldDataCapture][FDCStateConstantsKeys.userPrefrenceSetAsDefault] = { "Type": "string", "Value": FDCStateConstantsKeys.Lists };
      }

      // if (payload[UserStateConstantsKeys.FieldDataCapture] && payload[UserStateConstantsKeys.FieldDataCapture].hasOwnProperty(FDCStateConstantsKeys.userPrefrenceSetAsDefault)) {
      //   if (payload[UserStateConstantsKeys.FieldDataCapture][FDCStateConstantsKeys.userPrefrenceSetAsDefault]["Value"] == TreeTypeNames.Hierarchy)
      //     payload[UserStateConstantsKeys.FieldDataCapture][FDCStateConstantsKeys.userPrefrenceSetAsDefault] = { "Type": "string", "Value": FDCStateConstantsKeys.Lists };
      // }

      this.usersStateQuery.add(UserStateConstantsKeys.userPrefrence, payload);

    } catch (e) {
      this.appLogException(new Error('Exception in saveUserprefrenceDataToState() of commonservice at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  // sendUserPreferenceCallToBackendForListsData() {
  //   try {
  //     var userPayload = { "email": this.username, "cacheName": "TenantDetails" };
  //     this.sendMessageToServer(JSON.stringify(userPayload), EntityTypes.UserPreference + "List", this.currentUserName, "UserPreference", MessageKind.READ, "OriginSession", "Details", FABRICS.COMMON.toUpperCase());
  //   }
  //   catch (e) {
  //     this.appLogException(new Error('Exception in sendUserPreferenceCallToBackendForListsData() of commonservice at time ' + new Date().toString() + '. Exception is : ' + e));
  //   }
  // }

  receivedMessageFromServer(message) {
    try {
      this.loadingBarAndSnackbarStatus(AppConsts.loaderComplete);
      if (message.Payload == "SUCCESS" || message["DataType"] == "SUCCESS") {
        this.SendSuccessCall$.next(message);
      }

      this.recieveMessageFromMessagingService(message);
    } catch (ex) {
      this.appLogException(new Error(ex));
    }
  }

  GetFabricnameForRouterUrl(choice) {
    try {
      switch (choice) {
        case "People & Companies":
          return FABRICS.PEOPLEANDCOMPANIES;
        case "Business Intelligence":
          return "bifabric";
        case "Home":
          return "Home";
        case "Security":
          return "Security";
      }
    }
    catch (e) {
      this.appLogException(new Error('Exception in GetFabricnameForRouterUrl() of commonservice at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  updateEntitySecurityGroup(entityData: any): Array<any> {
    try {
      //let isViewedFabric: any[] = this.usersStateQuery.getStateKey(UserStateConstantsKeys.viewedFabrics);
      let fabric = this.getFabricNameByUrl(this.router.url);
      let fabricList: Array<string> = [];
      if (this.lastOpenedFabric == FABRICS.ENTITYMANAGEMENT) {
        fabricList = entityData.hasOwnProperty('Capability') && entityData.Capability.length != 0 ? entityData.Capability : (entityData.hasOwnProperty('Capabilities') && entityData.Capabilities.length != 0 ? this.getCapabilityNamesBy(entityData.Capabilities) : entityData.hasOwnProperty('SG') ? entityData.SG : []);
      }
      else
        fabricList.push(fabric);


      entityData["SG"] = fabricList;
      return fabricList;
    }
    catch (e) {
      this.appLogException(new Error('Exception in updateEntitySecurityGroup() method of commonservice at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }





  createnewlist() {
    try {
      // this.categorylist = [];
      // this.categorylist.push({ "id": "Personal", "text": "Personal" });
      // if (this.lastOpenedFabric.toUpperCase() == FABRICS.BUSINESSINTELLIGENCE.toUpperCase()) {
      //   this.categorylist.push({ "id": "Carousel", "text": "Carousel" });
      // }
      let config = {
        header: 'ADD NEW LIST',
        type: 'input',
        isSubmit: true,
        fieldName: 'List Name'
      };
      this.opendialogboxObs.next(config);
    }
    catch (e) {
      this.appLogException(new Error('Exception in  createnewlist() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }

  changeFavIcon(fabric) {
    if (!this.CustomerAppsettings.env.UseAsDesktop) {
      let fabricName = this.getFabricNameByUrl(this.router.url);
      let headerList = this.appConfig.AllHeaderRoutingList.filter((list) => (list.Fabric && fabricName == list.Fabric));
      if (headerList && headerList.length > 0) {
        this.titleService.setTitle(headerList[0].tooltip);
      }
      else {
        this.titleService.setTitle('ConstructionApp');
      }

      switch (fabricName) {

        case FabricsNames.SECURITY:
        case FABRICS.SECURITY:
          this.document.getElementById('FaviconId').setAttribute('href', 'wwwroot/images/Favicon/Security_blue_outlined_favicon.png');
          break;

        // case FABRICS.ASSETMANAGEMENT:
        case FABRICS.ENTITYMANAGEMENT:
          this.document.getElementById('FaviconId').setAttribute('href', 'wwwroot/images/Favicon/EntityManagement_blue_outlined_favicon.png');
          break;

        // case FabricsNames.ActivityCenter:
        // case FABRICS.ACTIVITYCENTER:
        //   this.document.getElementById('FaviconId').setAttribute('href', 'wwwroot/images/Favicon/Activity_Center_Outlined_white.png');
        //   break;

        case FABRICS.PEOPLEANDCOMPANIES:
        case 'peoplefabric':
          this.document.getElementById('FaviconId').setAttribute('href', 'wwwroot/images/Favicon/Peeps_And_Comps_Outlined_white.png');
          break;
        
        case FABRICS.CONSTRUCTION:
        case 'Construction':  
          this.document.getElementById('FaviconId').setAttribute('href', 'wwwroot/images/Favicon/Construction_blue_outlined_favicon.png');
          break;
        case FABRICS.MAPS:
        case 'Maps':
          this.document.getElementById('FaviconId').setAttribute('href', 'wwwroot/images/Favicon/Maps_blue_outlined_favicon.png');
          break;    

        // case FABRICS.OPFFabric:
        //   this.document.getElementById('FaviconId').setAttribute('href', 'wwwroot/images/Favicon/Operational_Forms_Outlined_white.png');
        //   break;

        case FABRICS.BUSINESSINTELLIGENCE:
        case FABRICS.BUSINESSINTELLIGENCE:
          this.document.getElementById('FaviconId').setAttribute('href', 'wwwroot/images/Favicon/BusinessIntelligence_blue_outlined_favicon.png');
          break;
        // case FABRICS.PIGGING:
        //   this.document.getElementById('FaviconId').setAttribute('href', 'wwwroot/images/Favicon/Pigging.png');
        //   break;

        case FabricsNames.HOME:
        case FABRICS.HOME:
        case FabricsPath.HOME:
          this.document.getElementById('FaviconId').setAttribute('href', 'wwwroot/images/Favicon/Home_blue_outlined_favicon.png');
          break;

        // case FabricsNames.BigData:
        // case FABRICS.BIGDATA:
        //   this.document.getElementById('FaviconId').setAttribute('href', 'wwwroot/images/Favicon/Big_Data_Outlined_white.png');
        //   break;

        default:
          this.document.getElementById('FaviconId').setAttribute('href', 'wwwroot/images/Favicon/VISUR_Favicon.ico');
          break;
      }
    }

  }


  UpdateListChildrens(data) {
    try {

      let availablechildrens = [];
      let value;
      if (data != null && data != undefined) {

        let entityid = data.selectedContextMenuOption.id;
        let entityname = data.selectedContextMenuOption.text;
        let selectedlist = this.getEntitiesById(entityid);
        var childrens = selectedlist.children;
        availablechildrens = JSON.parse(JSON.stringify(childrens));
        let entityData = JSON.parse(JSON.stringify(data.data));
        entityData.children = [];
        availablechildrens.push(entityData);
        if (selectedlist.Parent) {
          value = selectedlist.Parent["0"].EntityName;
        }
        this.ValidateListForm(value, null, null, availablechildrens, value, entityname, entityid);
      }
    }
    catch (e) {
      this.appLogException(new Error('Exception in  UpdateListChildrens() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  ValidateListForm(value, sharedList?, sharedArray?, listofEntities?, CategoryType?, updatedlistname?, currentactivelistId?) {
    try {
      this.ContextMenuData = this.ContextMenuData ? JSON.parse(JSON.stringify(this.ContextMenuData)) : undefined;
      let id = this.GetNewUUID();
      let capability = this.getFabricNameByUrl(this.router.url);
      var dateForFDCEntity = new Date().toISOString().slice(0, 19).replace('T', ' ');
      var isNewList: boolean = false;
      var isDelete = sharedArray ? sharedArray.isDelete : false;
      let Categorydata;
      let DataArray = [];
      let TypeOfList = "Categories";
      let createdPeople: any;
      let BiDashboardurl = this.ContextMenuData && this.ContextMenuData['Dashboardurl'] != null && this.ContextMenuData['Dashboardurl'] != undefined ? this.ContextMenuData['Dashboardurl'] : "";
      var capabilityName = this.getCapabilityNameBasedOnFabric();
      var userPayload = null;
      var capabilityId = this.capabilityFabricId;
      var activeFabric = this.appConfig.AllHeaderRouting.filter(obj => obj["routerLinkActive"] == true);
      if (this.nameoflist) {
        this.nameoflist = this.nameoflist.trim();
      }
      if (activeFabric.length > 0) {
        capabilityId = activeFabric[0]["entityId"];
      }
      if (currentactivelistId == undefined)
        currentactivelistId = id;
      if (listofEntities) {
        var childrens = listofEntities;
        var updatedarray: any = [];
        var count = 1;
        childrens.forEach((children) => {
          // children=JSON.parse(JSON.stringify(children))
          if (!children["Deleted"]) {
            children["order"] = count;
            delete children.uuid;
            children["Parent"] = [{ "EntityId": currentactivelistId, "EntityName": updatedlistname }];
            updatedarray.push(children);
            count++;
          }
        });

        let listNodeData: any;

        if (this.CreatenewList) {
          isNewList = true;
          listNodeData = { "EntityId": currentactivelistId, "EntityName": updatedlistname, TypeOf: EntityTypes.List, "Type": EntityTypes.List, "EntityType": EntityTypes.List, "children": childrens, "Created": dateForFDCEntity, "SG": capability, "Capabilities": [capabilityId], "Parent": [{ "EntityId": CategoryType, "EntityName": CategoryType }] };

          Categorydata = { "EntityId": currentactivelistId, "EntityType": EntityTypes.List, "Type": EntityTypes.List, "EntityName": updatedlistname, "data": childrens, Created: dateForFDCEntity, "EntityCreated": new Date() };

          createdPeople = {
            'InsertAtPosition': CategoryType, 'NodeData': {
              EntityId: Categorydata.EntityId, EntityName: Categorydata.EntityName, EntityType: Categorydata.Type,
              children: updatedarray, Parent: [{ EntityId: CategoryType, EntityName: CategoryType }]
            }
          };
          this.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.AddNewNode, createdPeople);

          if (currentactivelistId) {
            this.listEntityId = currentactivelistId;
          }

        }
        else {
          let tempListNodeData = this.getEntitiesById(currentactivelistId);
          if (!tempListNodeData) {
            listNodeData = { "EntityId": currentactivelistId, "EntityName": updatedlistname, TypeOf: EntityTypes.List, "Type": EntityTypes.List, "EntityType": EntityTypes.List, "children": childrens, "Created": dateForFDCEntity, "SG": capability, "Capabilities": [capabilityId], "Parent": [{ "EntityId": CategoryType, "EntityName": CategoryType }] };
          }
          else {
            listNodeData = JSON.parse(JSON.stringify(tempListNodeData));
            listNodeData.EntityName = updatedlistname;
            listNodeData.children = updatedarray;
          }

          Categorydata = { "EntityId": currentactivelistId, "EntityType": EntityTypes.List, "Type": EntityTypes.List, "EntityName": updatedlistname, "data": childrens, Created: dateForFDCEntity, "EntityCreated": this.ContextMenuData ? this.ContextMenuData.Created : "" };

          createdPeople = {
            'InsertAtPosition': Categorydata.EntityId, 'NodeData': updatedarray
          };

          //** To Update List Name **/
          let updatedNode = { "EntityId": currentactivelistId, "NodeData": listNodeData };
          this.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.updateNodeById, JSON.parse(JSON.stringify(updatedNode)));
        }
        this.entityCommonStoreQuery.upsert(listNodeData.EntityId, listNodeData); // Add / Update to entity state

        userPayload = { "CategorhyData": Categorydata ? Categorydata : { "data": "[]" }, "TypeOfList": TypeOfList, "NewListUserId": value ? value.id : "", "isNewListCreation": isNewList, "CapabilityId": capabilityId, "CapabilityName": capabilityName, "Fabric": FABRICS.COMMON.toUpperCase(), "SharedListArray": DataArray, "isDelete": sharedArray ? sharedArray.isDelete : false, "UserID": this.currentUserId };
        if (this.CreatenewList) {
          this.sendMessageToServer(JSON.stringify(userPayload), EntityTypes.UserList, currentactivelistId, EntityTypes.List, MessageKind.CREATE, "AllFrontEndButOrigin", "Details", this.lastOpenedFabric.toUpperCase());
        }
        else {
          id = Categorydata.EntityId;
          this.sendMessageToServer(JSON.stringify(userPayload), EntityTypes.PersonalList, currentactivelistId, EntityTypes.List, MessageKind.UPDATE, "AllFrontEndButOrigin", "Details", this.lastOpenedFabric.toUpperCase());
        }
      }
      else {
        if (sharedList) {
          if (sharedList.CategoryName == EntityTypes.Carousel) {
            id = value;
            Categorydata = { "EntityId": id, "Type": EntityTypes.List, "EntityName": sharedList.CategoryName, "data": sharedList.EntitiesArray, Created: dateForFDCEntity, "EntityCreated": dateForFDCEntity };
            sharedList = undefined;
          }
        }
        else if (this.nameoflist != null && this.nameoflist != "") {
          this.ContextMenuData.children = [];
          id = this.GetNewUUID();
          isNewList = true;
          var newNode = { "EntityId": this.ContextMenuData.EntityId, "order": 1, "EntityName": this.ContextMenuData.EntityName, "EntityType": this.ContextMenuData.EntityType };
          Categorydata = { "EntityId": id, "Type": EntityTypes.List, "EntityName": this.nameoflist, "data": [newNode], Created: dateForFDCEntity, "EntityCreated": new Date() };
          let listNodeData = { "EntityId": id, "EntityName": this.nameoflist, "Type": EntityTypes.List, TypeOf: EntityTypes.List, "EntityType": EntityTypes.List, children: [newNode], Created: dateForFDCEntity, "SG": capability, "Capabilities": [capabilityId], "Parent": [{ EntityId: EntityTypes.Personal, EntityName: EntityTypes.Personal }] };
          this.entityCommonStoreQuery.upsert(listNodeData.EntityId, listNodeData); // Add / Update to entity state
        }
        else {
          this.ContextMenuData.children = [];
          var updateNode = { "EntityId": this.ContextMenuData.EntityId, "order": value.countOfChildren + 1, "EntityName": this.ContextMenuData.EntityName, "EntityType": this.ContextMenuData.EntityType };
          Categorydata = { "EntityId": value.id, "Type": EntityTypes.List, "EntityName": this.nameoflist, "data": [updateNode], Created: dateForFDCEntity, "EntityCreated": this.ContextMenuData.Created };
        }

        var keyData = undefined;
        if (sharedList) {
          TypeOfList = sharedList;
          if (this.ListContextMenuData && this.ListContextMenuData.EntityId)
            id = this.ListContextMenuData.EntityId;
          if (sharedArray && sharedArray.SharedTo && sharedArray.SharedTo.length != 0) {
            DataArray = sharedArray.SharedTo;
          }
          keyData = this.ListContextMenuData;
        }
        else if (!isNewList) {
          id = Categorydata.EntityId;
        }
        userPayload = { "CategorhyData": Categorydata ? Categorydata : { "data": "[]" }, "TypeOfList": TypeOfList, "NewListUserId": value ? value.id : "", "isNewListCreation": isNewList, "CapabilityId": capabilityId, "CapabilityName": capabilityName, "Fabric": FABRICS.COMMON.toUpperCase(), "SharedListArray": DataArray, "isDelete": sharedArray ? sharedArray.isDelete : false, "Key": keyData };

        if (isDelete) {
          this.sendMessageToServer(JSON.stringify(userPayload), EntityTypes.UserList, id, EntityTypes.List, MessageKind.UPDATE, Routing.AllFrontEndButOrigin, "Details", this.lastOpenedFabric.toUpperCase());
        }
        else {
          this.sendMessageToServer(JSON.stringify(userPayload), EntityTypes.UserList, id, EntityTypes.List, MessageKind.CREATE, Routing.AllFrontEndButOrigin, "Details", this.lastOpenedFabric.toUpperCase());
        }

        if (isNewList) {
          createdPeople = {
            'InsertAtPosition': EntityTypes.Personal,
            'NodeData': {
              EntityId: Categorydata.EntityId,
              EntityName: Categorydata.EntityName,
              EntityType: Categorydata.Type,
              children: [
                {
                  EntityId: this.ContextMenuData.EntityId,
                  EntityName: this.ContextMenuData.EntityName,
                  EntityType: this.ContextMenuData.EntityType,
                  EntityOrder: newNode.order,
                  children: this.ContextMenuData.children,
                  TypeOfField: "Locations",
                  WellType: this.ContextMenuData.WellType,
                  TimeZone: this.ContextMenuData.TimeZone,
                  Parent: [{ EntityId: Categorydata.EntityId, EntityName: Categorydata.EntityName, EntityType: Categorydata.EntityType }],
                  Dashboardurl: BiDashboardurl
                }
              ],
              Parent: [{ EntityId: EntityTypes.Personal, EntityName: EntityTypes.Personal }],
              Type: Categorydata.Type
            }
          };

        }
        else {
          if (!sharedList) {
            createdPeople = {
              'InsertAtPosition': Categorydata.EntityId,
              Parent: [{ EntityId: Categorydata.EntityId, EntityName: Categorydata.EntityName }],
              'NodeData': {
                EntityId: this.ContextMenuData.EntityId,
                EntityName: this.ContextMenuData.EntityName,
                EntityType: this.ContextMenuData.EntityType,
                EntityOrder: updateNode.order,
                children: this.ContextMenuData.children,
                TypeOfField: "Locations",
                WellType: this.ContextMenuData.WellType,
                TimeZone: this.ContextMenuData.TimeZone,
                Parent: [{ EntityId: Categorydata.EntityId, EntityName: Categorydata.EntityName, EntityType: Categorydata.EntityType }],
                Dashboardurl: BiDashboardurl
              }
            };
          }
        }
        if (createdPeople) {
          this.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.AddNewNode, createdPeople);
        }
      }

      this.isclickoncreatelist = false;
      // this.deletenode = false;
      setTimeout(() => {
        this.nameoflist = 'null';
      });
      // this.Category = null;
    }
    catch (e) {
      this.appLogException(new Error('Exception in  ValidateListForm() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }
  CopyToList(data: any) {

    let listInfo = { ...data.nodeData.data };
    listInfo["EntityId"] = UUIDGenarator.generateUUID().toString();
    listInfo["Created"] = new Date().toISOString().slice(0, 19).replace('T', ' ');
    listInfo["UserID"] = this.currentUserId;
    var count = 1;
    listInfo.children = listInfo.children ? listInfo.children : [];
    listInfo.children.forEach((children) => {
      if (!children["Deleted"]) {
        children["EntityOrder"] = count;
        count++;
      }
    });
    let payload = { SelectedCapability: data.selectedContextMenuOption.id, Data: listInfo };

    this.sendMessageToServer(JSON.stringify(payload), "CopyToCapability", data.nodeData.data.EntityId, EntityTypes.List, MessageKind.UPDATE, Routing.AllFrontEndButOrigin, "Details", FABRICS.COMMON.toUpperCase());
  }

  sendDataToAngularTree1(treeName: string, treeOperationType: string, payload: any, fabric: string = null) {
    try {
      const treeMessage: AngularTreeMessageModel = {
        'fabric': fabric != null ? fabric.toLocaleUpperCase() : this.lastOpenedFabric.toLocaleUpperCase(),
        'treeName': [treeName],
        'treeOperationType': treeOperationType,
        'treePayload': payload,
      };
      this.sendDataToAngularTree.next(treeMessage);
    }
    catch (e) {
      this.appLogException(new Error('Exception in  sendDataToAngularTree1() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  sendDataToMultipleAngularTree(treeNames: string[], treeOperations: string[], payload: any) {
    try {
      let treeSendList: any[] = [];
      treeNames.forEach((tree: string) => {
        treeOperations.forEach((operation: string) => {
          treeSendList.push({ tree: tree, operation: operation, data: payload });
        });
      });
      if (treeSendList.length > 0) {
        this.sendMultiOperationPayloadAngularTree(treeSendList);
      }
    }
    catch (e) {
      console.error('Exception in sendMultipleDataToAngularTree() of CommonService in CommonService at time ' + new Date().toString() + '. Exception' + ' is' + ' :' + ' ' + e);
      this.logger.logException(new Error('Exception in sendMultipleDataToAngularTree() of CommonService in CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  sendMultiOperationPayloadAngularTree(list: Array<any>) {
    try {
      let multiOperationData: Array<AngularTreeMessageModel> = [];
      let listOfTreeName: Array<string> = [];
      list.forEach(d => {
        listOfTreeName.push(d.tree);
        let treePayload: AngularTreeMessageModel = {
          "fabric": this.lastOpenedFabric.toLocaleUpperCase(),
          "treeName": [d.tree],
          "treeOperationType": d.operation,
          "treePayload": d.data,
        };
        multiOperationData.push(treePayload);
      });
      this.sendDataToAngularTreeMultiOperation(listOfTreeName, multiOperationData);
    }
    catch (e) {
      this.appLogException(new Error("Exception in sendMultiOperationPayloadAngularTree in CommonService :: " + e));
    }
  }

  sendDataToAngularTreeMultiOperation(listOfTreeName: Array<string>, multiOperationData: Array<AngularTreeMessageModel>) {
    const treeMessage: AngularTreeMessageModel = {
      'fabric': this.lastOpenedFabric.toLocaleUpperCase(),
      'treeName': listOfTreeName,
      'treeOperationType': TreeOperations.MultiOperation,
      'treePayload': multiOperationData,
    };
    this.sendDataToAngularTree.next(treeMessage);
  }
  updateAngularTree() {
    this.sendDataToAngularTree1(TreeTypeNames.ALL, TreeOperations.refreshUpdateTree, null, null);
  }
  getCotegoryList(ListData, fabric?) {
    try {
      var ListData: any = this.getEntititesFromStore("EntityType", ["List"], d => true, d => { return { "id": d.EntityId, "text": d.EntityName, "displayName": d.EntityName, "children": d.children, "countOfChildren": d.children.length }; }, fabric)
        .filter(d => { if (d["children"].filter(obj => obj.EntityId == this.ContextMenuData.EntityId).length == 0) { delete d["children"]; return d; } });

      return ListData;
    }
    catch (e) {
      this.appLogException(new Error('Exception in  getCotegoryList() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  statusCheck(message) {
    try {
      this.loadingBar.start();
      if (message.DataType != "ActivityCenter") {
        switch (message.MessageKind) {
          case MessageKind.UPDATE:
            var statusMessage = "Updating Entity...";
            if (message.Payload) {
              var payload = JSON.parse(message.Payload);
              if (payload && payload.EntityId == "RecalculateHistory" && "RecalculateHistory") {
                var statusMessage = "Creating Entity...";
              }
            }
            this.loadingBarAndSnackbarStatus("", statusMessage);
            break;
          case MessageKind.READ:
            this.loadingBarAndSnackbarStatus("", "Reading Data...");
            break;
          case MessageKind.CREATE:
            this.loadingBarAndSnackbarStatus("", "Creating Entity...");
            break;
          case MessageKind.DELETE:
            this.loadingBarAndSnackbarStatus("", "Deleting Entity...");
            break;
          case MessageKind.PARTIALUPDATE:
            var statusMessage = "Updating Entity...";
            this.loadingBarAndSnackbarStatus("", statusMessage);
            break;
        }
      } else {
        switch (message.MessageKind) {
          case MessageKind.UPDATE:
            this.loadingBarAndSnackbarStatus("", "Updating Message...");
            break;
          case MessageKind.READ:
            this.loadingBarAndSnackbarStatus("", "Reading Data...");
            break;
          case MessageKind.CREATE:
            this.loadingBarAndSnackbarStatus("", "Creating Entity...");
            break;
          case MessageKind.DELETE:
            this.loadingBarAndSnackbarStatus("", "Deleting Entity...");
            break;
        }
      }
      if (message.Fabric.toUpperCase() == "FDC" && message.DataType == "FDC_NODE_SETASPRIMARY") {
        setTimeout(() => {
          this.loadingBarAndSnackbarStatus("complete", "Entity Updated...");
          setTimeout(() => {
            this.loadingBarAndSnackbarStatus("", "");
          }, 1000);
        }, 4000);

      }

    }
    catch (e) {
      this.appLogException(new Error('Exception in  statusCheck() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  getCapabilityNameBasedOnFabric() {
    try {
      var capabilityName;
      switch (this.lastOpenedFabric) {

        case FABRICS.BUSINESSINTELLIGENCE:
          capabilityName = 'Business Intelligence';
          break;


        default:
          capabilityName = '';
          break;
      }
      return capabilityName;
    }
    catch (e) {
      this.appLogException(new Error('Exception in  getCapabilityNameBasedOnFabric() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }


  sendRequestForSharedToListPeopleData() {
    this.sendMessageToServer(null, EntityTypes.People, EntityTypes.People, EntityTypes.SharedList, MessageKind.READ, Routing.OriginSession, 'Details', FABRICS.PEOPLEANDCOMPANIES);
  }

  makeReadRequestForList(contextMenuStatus?: boolean, tab?: string, ListId?: any) {
    try {
      var capabilityName = this.getCapabilityNameBasedOnFabric();
      var userPayload = {
        "Info": EntityTypes.UserList, "isContextMenu": contextMenuStatus, "tab": tab, "CapabilityName": capabilityName, "ListOperationsData": ListId
      };
      // this.isListReadRequest = true;
      if (!this.ContextMenuData)
        this.sendMessageToServer(JSON.stringify(userPayload), "Lists", this.currentUserName, EntityTypes.UserList, MessageKind.READ, "OriginSession", "Details", this.lastOpenedFabric.toUpperCase());


      if (this.ContextMenuData) {
        var sharedPayload = { "Info": "listsharedusers", "ListId": this.ContextMenuData.EntityId };
        this.sendMessageToServer(JSON.stringify(sharedPayload), "Lists", "listsharedusers", EntityTypes.UserList, MessageKind.READ, Routing.OriginSession, "Details", this.lastOpenedFabric.toUpperCase());
      }
    }
    catch (e) {
      this.appLogException(new Error('Exception in  makeReadRequestForList() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  deleteRequestForList(listData) {
    try {
      let payload = {
        'EntityId': listData.EntityId,
        'TenantName': this.tenantName,
        'TenantId': this.tenantID,
        'EntityType': listData.EntityType,
        "TypeOfField": listData.TypeOfField
      };
      this.sendMessageToServer(JSON.stringify(payload), "DeleteList", listData.EntityId, listData.EntityType, MessageKind.DELETE, Routing.AllFrontEndButOrigin, "Details", this.lastOpenedFabric.toUpperCase());
      this.entityCommonStoreQuery.deleteNodeById('EntityId', listData.EntityId);
      this.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.deleteNodeById, listData.EntityId);
      this.route.queryParams.subscribe(params => {
        this.islistformValidation = false;
        if (params['EntityId'] == listData.EntityId) {
          let queryParams =
          {
            'EntityId': this.listEntityId,
            'tab': this.treeIndexLabel,
            'leftnav': this.isLeftTreeEnable
          };
          switch (this.getFabricNameByUrl(this.router.url)) {
            // case FABRICS.PIGGING:
            //   this.router.navigate(['/' + FabricRouter.PIGGING], { queryParams: queryParams });
            //   break;
            // case FABRICS.OPFFabric:
            //   this.router.navigate(['/' + FabricRouter.OPERATIONALFORM], { queryParams: queryParams });
            //   break;
            default:
              break;
          }
        }
      });
    } catch (e) {
      this.appLogException(new Error('Exception in  deleteRequestForList() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  sendMessageToBackend(message: MessageModel) {
    try {

      switch (message.EntityType) {
        case EntityTypes.DashBoard:
          break;
        default:
          this.loadingBarAndSnackbarStatus("complete");
          this.statusCheck(message);
          break;
      }

      if (message.Fabric == FABRICS.COMMON.toUpperCase()) {
        message.Fabric = message.Fabric;
      }


      if (message.MessageKind.toUpperCase() == MessageType.READ && message.Fabric != "Authorization") {
        switch (message.EntityType) {
          case EntityTypes.UserPreference:
            var result = this.stateRecords.isStateObjectExists(message);
            if (result) {
              console.log("state userprefrence get..." + result);
              this.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, "");
              this.userPreferenceData(result, this.isUpdateUserpreferencePinnedTab);
            }
            else
              this.getLeftTreeData(message);
            break;
          case "FDCTimeZone":
          case "FDCEntities":
          case "SearchTree":
          // case "AllocationTree":
          case "OperationEntities":
          case EntityTypes.People:
          case EntityTypes.SharedList:
          case "UserCreateDeletePermission":
          case "UserList":
          case "bifabric":
          case "DataSetEntities":
          // case "AssetManagementEntities":
          // case "AssetManagementEntitiesForm":
          // case "AssetManagementSchema":
          case "SANKEYJS":
          case "FDCTanksData":
          //case "UserSecurityGroupsEntities":
          case "ReadSankeyMetersVolume":
          case EntityTypes.COMPANY:
          case EntityTypes.PERSON:
            this.getLeftTreeData(message);
            break;
          default:
            switch (message.DataType) {
              case "Configuration":
              case "DataEntry":
              case "GlobalAdmin":
              case "GlobalForms":
              case "EMGlobalAdmin":
              case "FDCHistoricalData":
              // case "AssetManagementRightTree":
              case "ANALYTICS":
              case "FDCData":
              case "People":
              case "BI":
              case "People_UserSetting":
              case "People & Companies_UserSetting":
              case "DataIntegrationTag":
              case "TruckTicketDensitybuttonStatus":
              case "ListOwner":
              case Datatypes.CONSTRUCTION:
              case Datatypes.LOADFORMDATA:
                this.getLeftTreeData(message);
                break;
              case "Authorization":
                switch (message.EntityID) {
                  case "securitygroupinfo":
                    this.getLeftTreeData(message);
                    break;
                  default:
                    this.sendMessageToSocket_Backend(message);
                    break;
                }
                break;
              default:
                switch (message.EntityType) {
                  case "LayerSchema":
                    break;
                  case "ProductionSchema":
                    break;
                  case "PropertiesSchema":
                    break;
                  default:
                    this.sendMessageToSocket_Backend(message);
                    break;
                }
                break;
            }
            break;
        }
      }
      else if (message.DataType == "PublishToFinder") {
        this.getLeftTreeData(message);
      }
      else if (message.DataType == FABRICS.PEOPLEANDCOMPANIES) {
        this.createUpdatePeopleEntities(message);
      }
      else {
        this.sendMessageToSocket_Backend(message);
      }
    } catch (e) {
      this.appLogException(new Error('Exception in  sendMessageToServer() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  sendMessageToServer(Payload: string, DataType: string, EntityID: string, EntityType: string, Messagekind: string, Routing: string, Type: string, Fabric: string, AbortSignal: boolean = false) {

    let message = this.getMessage(Fabric);
    // if (EntityType && EntityType == EntityTypes.List && this.lastOpenedFabric == FABRICS.PIGGING)
    //   message.Fabric = this.getFabricNameByUrl(this.router.url);
    if (DataType == "CopyToCapability")
      message.Fabric = FABRICS.COMMON;

    message.Payload = Payload;
    message.DataType = DataType;
    message.EntityID = EntityID;
    message.EntityType = EntityType;
    message.MessageKind = Messagekind;
    message.Routing = Routing;
    message.Type = Type;
    message.AbortSignal = AbortSignal;
    this.sendMessageToBackend(message);
  }

  getUrlfromActviatedRoute(activateroute: ActivatedRoute) {
    let data: any = activateroute.snapshot.queryParams['data'];
    return data;
  }
  readDataFromApiCall(payload, dataType, entityId, entityType, fabric, serviceType = AppConsts.READDATAFROMSQL) {
    let fabricName = this.getFabricNameByUrl(this.router.url);
    let message = this.getMessage();
    message.Payload = JSON.stringify(payload);
    message.DataType = dataType;
    message.EntityID = entityId;
    message.EntityType = entityType;
    message.MessageKind = MessageKind.READ;
    message.Routing = Routing.OriginSession;
    message.CapabilityId = this.getCapabilityIdForMsg(fabric, fabricName);
    message.Type = "Details";
    message.Fabric = fabric;

    return this.http.post(this.baseUrl + 'api/AlThings/' + serviceType, message)
      .map(message => {
        let payloadJson = JSON.parse(message['Payload']);
        return payloadJson;
      });
  }
  async dismissStausMessage() {
    // if (this.toast) {
    //   await this.toast.dismiss();
    // }
  }

  getLeftTreeData(message: MessageModel) {
    try {
      this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message).subscribe((res: any) => {
        this.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, "");
        if (res["Data"]) {
          if (res["Data"]["Data"] == "Unauthorized") {
            this.appLogException(new Error(res));
          }
        } else {
          var messageData = typeof res === 'string' ? JSON.parse(res) : res;
          if (messageData) {
            if (messageData.EntityType == "error") {
              this.appLogException(new Error(messageData.Payload));
            }
            else if (messageData.EntityType == EntityTypes.UserPreference) {
              this.userPreferenceData(messageData);
            }
            else if (messageData.DataType == "People & Companies_UserSetting") {
              this.userSettingForm.next(messageData);
              this.userSettingState(messageData);
            }
            else if (messageData.DataType == "ListOwner") {
              this.updateSharedListUser$.next(messageData);
            }
            else {
              this.sendMessagesToFabricServices.next(messageData);
              this.loadingBarAndSnackbarStatus("", "");
            }
          }
        }
      });
    } catch (e) {
      this.appLogException(new Error('Exception in  getLeftTreeData() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  createUpdatePeopleEntities(message: MessageModel) {
    try {
      this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.baseUrl + 'api/AlThings/PeopleFabric/CreateUpdatePeopleEntities', {}, message).subscribe((res: any) => {
        this.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, "");
        if (res["Data"]) {
          if (res["Data"]["Data"] == "Unauthorized") {
            this.appLogException(new Error(res));
          }
        } else {
          var messageData = typeof res === 'string' ? JSON.parse(res) : res;
          if (messageData) {
            if (messageData.EntityType == "error") {
              this.appLogException(new Error(messageData.Payload));
            }
            else {
              this.sendMessagesToFabricServices.next(messageData);
              this.loadingBarAndSnackbarStatus("", "");
            }
          }
        }
      });
    } catch (e) {
      this.appLogException(new Error('Exception in  CreateUpdatePeopleEntities() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }


  removeDuplicateObjectFromArray(arr) {
    try {
      const uniqueArray = arr.filter((thing, index) => {
        return index === arr.findIndex(obj => {
          return JSON.stringify(obj) === JSON.stringify(thing);
        });
      });
      return uniqueArray;
    }
    catch (e) {
      this.appLogException(new Error('Exception in  removeDuplicateObjectFromArray() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }
  updateTreeDataBasedOnOperationalStatus(treeData, EntitiesOperationalStatus) {
    let locaions = [
      ...treeData.facilities,
      ...treeData.headers,
      ...treeData.wells
    ];
    locaions.forEach(d => {
      d.OperatedStatus = EntitiesOperationalStatus[d.EntityId];
    });
  }
  /**
   * Update Userprefrence for right side bar
   */
  updateUserPrefrence(PayloadKey) {
    try {
      var IsSendRequestNeeded = false;
      var stateAssets = this.getUserPrefrenceFromState(PayloadKey);
      if (stateAssets != null && stateAssets != "null") {
        var payload = { [PayloadKey]: stateAssets };
        var dateTime = new Date().toISOString().slice(0, 19).replace('T', ' ');

        var userPayload = {
          "email": this.username,
          "cacheName": "TenantDetails",
          "UpdatePref": payload,
          "ModifiedDateTime": dateTime
        };

        var messageForUserPreferences = this.getMessage();
        messageForUserPreferences.EntityID = this.currentUserName;
        messageForUserPreferences.EntityType = 'UserPreference';
        messageForUserPreferences.DataType = EntityTypes.UserPreference;
        messageForUserPreferences.MessageKind = 'Update';
        messageForUserPreferences.Routing = 'AllFrontEndButOrigin';
        messageForUserPreferences.Payload = JSON.stringify(userPayload);
        messageForUserPreferences.Fabric = this.lastOpenedFabric.toUpperCase();
        this.sendMessageToSocket_Backend(messageForUserPreferences);
        //updating userprefrence in indexed db
        this.updateIndexDBUserPrefrence(null, null, null, PayloadKey, stateAssets, IsSendRequestNeeded);
      }
    }
    catch (e) {
      this.appLogException(new Error('Exception in updateUserPrefrence() of AssetsService at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }
  /**
   * common method for index db and backend service operation
   *
   * @param DataType
   * @param operationType
   * @param method
   * @param url
   * @param queryParams
   * @param body
   * @param options
   */
  httpRestRequestsFromWebWorker(workerTopic: string, operationType: string, method: 'get' | 'post', url: string, queryParams: any = {},
    body: any = null, options = DEFAULT_REQUEST_OPTIONS): Observable<any> {
    var self = this;
    if (workerTopic == WORKER_TOPIC.RESTHTTP || workerTopic == WORKER_TOPIC.GisMap || WORKER_TOPIC.AbortEvent) {
      operationType = CommonJsMethods.newGuid() + "_" + this.httprequestid;
    }
    this.httprequestid = "Default";
    return Observable.create((observer: Observer<Object>) => {

      var workerMsg: WorkerMessage = new WorkerMessage(workerTopic, operationType,
        {
          "method": method, "url": url, "queryParams": queryParams,
          "body": body, "options": options,
        }, AppConfig.AppMainSettings);

      if (this.worker) {
        this.worker.postMessage(workerMsg);
      }
      self.httpKeyValue.set(workerMsg.restMethodName, observer);
    });

  }

  abortRequestFromWorker(workerMessage: WorkerMessage) {
    try {
      var resobserver: Observer<Object> = this.httpKeyValue.get(workerMessage.restMethodName);
      if (resobserver) {
        resobserver.complete();
        this.httpKeyValue.
          delete(workerMessage.restMethodName);
      }
    }
    catch (e) {
      this.appLogException(new Error(e));
    }
  }

  sendAbortRequest(url = null, body = null, queryParameter = null) {
    try {
      if (!body)
        body = "abort";

      this.httpRestRequestsFromWebWorker(WORKER_TOPIC.AbortEvent, "", "get", url, queryParameter, body).subscribe((res: any) => {
        console.log('aborted request' + res);
      });
    }
    catch (e) {
      this.appLogException(new Error(e));
    }
  }

  calllazyloading(payload: any) {
    try {

    }
    catch (e) {
      this.appLogException(new Error('Exception in calllazyloading() of CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }

  DataSetTree() {
    try {

    }
    catch (e) {
      this.appLogException(new Error('Exception in DataSetTree() of CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  worker$: Observable<Event>;
  worker_Rest$: Observable<any>;
  worker_WS$: Observable<any>;

  workerInit(): void {
    try {
      if (!!this.worker === false) {
        this.worker = new Worker(AppConsts.apiWorkerPath);

        this.worker$ = fromEvent(this.worker, 'message');

        var workerResponse = this.worker$.map(response => WorkerMessage.getInstance((<MessageEvent>response).data));

        this.worker_WS$ = workerResponse.filter(data => data.topic == WORKER_TOPIC.WS).map(data => {
          var messageWrapper = data.dataPayload.data;
          var json = JSON.parse(JSON.parse(messageWrapper));
          return json;
        });

        this.worker$
          .subscribe((response: MessageEvent) => {
            try {
              var responcemsg: WorkerMessage = WorkerMessage.getInstance(response.data);
              switch (responcemsg.topic) {
                case WORKER_TOPIC.RESTHTTP:
                  var httpresponce: RequestResult = response.data.dataPayload;
                  if (httpresponce.status == 0) {// aborted and cancelled requests
                    this.abortRequestFromWorker(responcemsg);
                  }
                  else if (httpresponce.status != 504) {
                    if (isValidJSON(response.data.dataPayload.data)) {
                      response.data.dataPayload.data = JSON.parse(response.data.dataPayload.data);
                    }
                    var resobserver: Observer<Object> = this.httpKeyValue.get(responcemsg.restMethodName);
                    if (resobserver) {
                      if (CommonJsMethods.isvalidJson(httpresponce.data)) {//message json string with single quote
                        //this is the condition to stored inexeded data read
                        var msgPayload = JSON.parse(httpresponce.data).Payload;
                        if (msgPayload != null && CommonJsMethods.isvalidJson(msgPayload)) {
                          var parseData = JSON.parse(msgPayload);
                          if (parseData.hasOwnProperty("message_store_type")) {
                            var idbMessageValue = JSON.stringify(parseData.value);
                            var message = JSON.parse(httpresponce.data);
                            message.Payload = idbMessageValue;
                            httpresponce.data = JSON.stringify(message);
                          }

                          this.httpRequestResponseNext(resobserver, httpresponce, responcemsg);
                        }
                        else {
                          var msgPayload = JSON.parse(httpresponce.data);
                          var parseData = msgPayload;
                          if (parseData.hasOwnProperty("message_store_type")) {
                            var idbMessageValue = JSON.stringify(parseData.value);
                            var message = JSON.parse(httpresponce.data);
                            message.Payload = idbMessageValue;
                            httpresponce.data = JSON.stringify(message);
                          }

                          this.httpRequestResponseNext(resobserver, httpresponce, responcemsg);

                        }
                      }
                      else {
                        this.httpRequestResponseNext(resobserver, httpresponce, responcemsg);
                      }
                    }
                  }

                  break;
                case WORKER_TOPIC.WS:
                  console.log("************in commonservice WORKER_TOPIC.WS*******************");
                  var wsResponse: RequestResult = response.data.dataPayload;
                  var data = JSON.parse(JSON.parse(wsResponse.data));
                  if (data.Fabric && data.Fabric.toLowerCase() != "fdc" && data.Fabric.toLowerCase() != FABRICS.BUSINESSINTELLIGENCE && data.Fabric.toLowerCase() != FABRICS.PEOPLEANDCOMPANIES.toLowerCase() && data.Fabric.toLowerCase() != "operationalform") {
                    this.receivedMessageFromSocketServer(data);
                  }
                  else if (data.DataType == "UserPreference") {
                    this.receivedMessageFromSocketServer(data);
                  }
                  break;
                case WORKER_TOPIC.GisMap:
                  var httpresponce: RequestResult = response.data.dataPayload;
                  if (httpresponce.status != 504) {
                    response.data.dataPayload.data = JSON.parse(response.data.dataPayload.data);
                    var resobserver: Observer<Object> = this.httpKeyValue.get(responcemsg.restMethodName);
                    if (resobserver) {
                      if (CommonJsMethods.isvalidJson(httpresponce.data)) {

                        this.httpRequestResponseNext(resobserver, httpresponce, responcemsg);
                      }
                    }
                  }
                case WORKER_TOPIC.AbortEvent://all aborted event at onces
                  this.abortRequestFromWorker(responcemsg);
                  break;
                default:
                  break;
              }

            } catch (ex) {
              this.appLogException(new Error(ex));
            }

          }, (error) => this.appLogException(new Error('WORKER ERROR::' + error)));
      }
    } catch (e) {
      this.appLogException(new Error('Exception in workerInit() of CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  httpRequestResponseNext(resobserver: Observer<Object>, httpresponce: RequestResult, responcemsg: WorkerMessage): void {
    let currentActiveForm = this.getCurrentFormNameByurl(this.router.url);
    if (!currentActiveForm) {//no active form request or it's popup forms
      resobserver.next(httpresponce.data);
      resobserver.complete();
      this.httpKeyValue.delete(responcemsg.restMethodName);
    }
    else {//current form requests
      let currentFormName = responcemsg.restMethodName.split('_');
      if (currentFormName.length > 2) {
        this.sendResponseToNext(resobserver, httpresponce, responcemsg, currentFormName[2], currentActiveForm);
      }
      else {
        this.sendResponseToNext(resobserver, httpresponce, responcemsg, currentFormName[1], currentActiveForm);
      }
    }
  }

  sendResponseToNext(resobserver: Observer<Object>, httpresponce: RequestResult, responcemsg: WorkerMessage, backendFormName: string, currentActiveForm: string) {
    if (backendFormName == "Default" || backendFormName.toUpperCase() == currentActiveForm.toUpperCase()) {
      resobserver.next(httpresponce.data);
      resobserver.complete();
      this.httpKeyValue.delete(responcemsg.restMethodName);
    }
    else {
      resobserver.complete();
      this.httpKeyValue.delete(responcemsg.restMethodName);
    }
  }

  receivedMessageFromSocketServer(message) {
    this.loadingBarAndSnackbarStatus(AppConsts.loaderComplete);
    if (message.DataType == "AuthGrant") {
      this.appLogException(new Error("Message:" + message.Payload));
      //clear all store except users store
      this.stateRecords.reSetStore();
      this.usersStateQuery.add(UserStateConstantsKeys.viewedFabrics, []); // Reset Viewed Fabrics
      if (this.flag++ == 0) {

        this.onSessionExpirePopupAlert$.next(SESSION_EXPIRE_MESSAGE);
      }
    }
    else
      this.receivedMessageFromServer(message);
  }

  sendMessageToSocket_Backend(message) {
    this.sendMessageToServerNext(message, "sendMessageToSocket", "next");
  }



  sendMessageToServerNext(message, callerMethodName, methodType) {
    let socket_URL;
    let body = message;
    socket_URL = `${this.websocketProtocol}://${this.hostname}:32003/StockServiceWS/?t=` + this.appConfig.token + '&tname=' + this.appConfig.tenantName + '&appname=' + AppConfig.AppMainSettings.env.ApplicationName;
    var workerMsg: WorkerMessage = new WorkerMessage(
      WORKER_TOPIC.WS,
      CommonJsMethods.newGuid(),
      {
        "method": methodType, "url": socket_URL, "queryParams": { "callerMethodName": callerMethodName },
        "body": body, "options": DEFAULT_REQUEST_OPTIONS,
      }
    );
    if (this.worker)
      this.worker.postMessage(workerMsg);
  }

  getLatitudeAndLongitude(query) {
    try {
      if (this.CustomerAppsettings.env.UseAsDesktop == true && this.onlineOffline == false) {
        //tempararly i kept hard coded value
        return Observable.create((observer: Observer<Object>) => {
          let latandlongitude = { "LATITUDE": 0.0, "LONGITUDE": 0.0 };
          observer.next(latandlongitude);
          observer.complete();
        });
      } else {
        var url = AppConfig.BaseURL + "api/AlThings/GetLatitudeAndLongitude/?" + query;
        return this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "get", url).pipe(map((res: any) => {
          try {
            var latLongArray = JSON.parse(res);
            if (latLongArray[0]["error"]) {
              this.appLogException(new Error(res));
              return null;
            }
            return latLongArray[0];
          } catch (e) {

          }
        }));
      }
    }
    catch (e) {
      this.appLogException(new Error('Exception in getLatitudeAndLongitude() of CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }

  getCurrentFormNameByurl(url: string): string {//Settings //MultiDateView //Data
    if (url.toUpperCase().includes(TypeOfForm.DataEntry.toUpperCase())) {
      return TypeOfForm.DataEntry;
    }
    else if (url.toUpperCase().includes(TypeOfForm.Data.toUpperCase()) && !url.toUpperCase().includes('DATAINTEGRATIONS')) {
      return TypeOfForm.DataEntry;
    }
    else if (url.toUpperCase().includes(TypeOfForm.Configuration.toUpperCase())) {
      return TypeOfForm.Configuration;
    }
    else if (url.toUpperCase().includes(TypeOfForm.Settings.toUpperCase())) {
      return TypeOfForm.Configuration;
    }
    else {
      return null;
    }
  }

  /********** FOR READING ALL GLOBAL ADMIN DEPENDENCY DATA FOR FDC FORMS *******/

  payloadForReadingGlobalAdminData(Key) {
    let payload = { "EntityType": "Schema", "Fabric": "FDC", "Info": Key, "TypeOfRead": "StateManagementRequest" };
    var PayLoad = JSON.stringify(payload);
    let message;
    let fabric = this.getFabricNameByUrl(this.router.url);
    message = this.getMessage("FDC");
    message.Payload = PayLoad;
    message.DataType = "GlobalAdmin";
    message.EntityID = Key;
    message.EntityType = EntityTypes.FDCGlobalAdmin;
    message.MessageKind = MessageKind.READ;
    message.Routing = Routing.OriginSession;
    message.Type = "Details";
    message.Fabric = this.lastOpenedFabric.toUpperCase();
    return message;
  }


  /***
   * Reading FDC GlobalAdmin Lists Data
   */
  getFDCGlobalAdminListData$(Key) {
    try {
      return this.fdcStateQuery.select(state => state[Key]).pipe(switchMap((hashCache: any) => {
        let newState;
        let apiCall: Observable<any[]>;

        if (!(hashCache && hashCache.length != 0)) {
          let message: any = this.payloadForReadingGlobalAdminData(Key);
          // if (this.isElectron && this.onlineOffline != true) {
          //   apiCall = this.DesktopServiceInstances["DeskTopService"].GetGlobalAdmninMessage(message).pipe(map((res: string) => {
          //       let msg = JSON.parse(res);
          //       let payload = JSON.parse(msg.Payload);
          //       if (typeof (payload) == "string") {
          //         payload = JSON.parse(payload);
          //       }
          //       let data = payload["ListsData"] ? payload["ListsData"] : payload;
          //       if (typeof (data) == "string") {
          //         data = JSON.parse(data);
          //       }
          //       this.fdcStateQuery.add(Key, data);
          //       return data;
          //     }));
          // } else {
          //   apiCall = this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
          //     .pipe(map((res: string) => {
          //       let msg = typeof res === 'string' ? JSON.parse(res) : res;
          //       let data = [];
          //       if (msg.Payload != null) {
          //         let payload = JSON.parse(msg.Payload);
          //         data = payload["ListsData"] ? payload["ListsData"] : payload;
          //         this.fdcStateQuery.add(Key, data);
          //       }

          //       return data;
          //     }));
          // }
        }
        if (hashCache && hashCache.length != 0) {
          newState = JSON.parse(JSON.stringify(hashCache));
        }
        return newState ? of(newState) : apiCall;
      }));
    }
    catch (Ex) {
      console.error(Ex);
    }
  }
  getTimeZoneOptions(countryShortName) {
    return countryTimezone.getTimezonesForCountry(countryShortName).filter(d => d && d.name != "Asia/Calcutta" && d.name != "Asia/Katmandu").map(d =>
      d.name + '(' + d.utcOffsetStr + ')'
    );
  }

  public getCountryForTimezone(timeZone: string): object {
    console.log('getCountryForTimezone()');

    try {
      const tempTimeZone = timeZone.split('(')[0];
      const countryObj = countryTimezone.getCountryForTimezone(tempTimeZone);

      return countryObj;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  getAllTimeZone() {
    let timezoneObj = countryTimezone.getAllTimezones();
    if (timezoneObj["Asia/Calcutta"])
      delete timezoneObj["Asia/Calcutta"];

    if (timezoneObj["Asia/Katmandu"])
      delete timezoneObj["Asia/Katmandu"];

    return timezoneObj;
  }

  getTimezoneDetails(timezone){
    const ct = require('countries-and-timezones');
    timezone =  timezone.split('(')[0];
    timezone = timezone.split(' ')[0];
    const timezonedata = ct.getTimezone(timezone);
    const country = ct.getCountry(timezonedata.country);
    return country;
  }

  getFabricTabByUrl(queryParamKey, routerUrl?) {
    if (!routerUrl)
      routerUrl = this.router.url;

    if (routerUrl != "") {
      let routeAndQueryParams = routerUrl.split('?');
      if (routeAndQueryParams.length == 2) {
        let queryParams = routeAndQueryParams[1].split('&');
        let tabValue = queryParams.filter(params => params.startsWith(queryParamKey));
        if (tabValue && tabValue.length) {
          let val = decodeURIComponent(tabValue[0].toString().replace(queryParamKey + "=", "").toString());
          return val;
        }
      }
    }
  }


  getFabricNameByUrl(url: string) {
    try {

      // if (url.toUpperCase()
      //   .includes(FabricsPath.ENTITYMANAGEMENT.toUpperCase())) {
      //   return FabricsNames.ENTITYMANAGEMENT;
      // }
      // else if (url.toUpperCase()
      //   .includes(FABRICS.SECURITY.toUpperCase())) {
      //   return FabricsNames.SECURITY;
      // }
      // else if (url.toUpperCase()
      //   .includes(FABRICS.PEOPLEANDCOMPANIES.toUpperCase())) {
      //   return FABRICS.PEOPLEANDCOMPANIES;
      // }
      // else if (url.toUpperCase()
      //   .includes(FABRICS.BUSINESSINTELLIGENCE.toUpperCase())) {
      //   return FABRICS.BUSINESSINTELLIGENCE;
      // }
      // else if (url.toUpperCase()
      //   .includes(FabricsPath.HOME.toUpperCase())) {
      //   return FabricsNames.HOME;
      // }
      // else if (url.toUpperCase()
      // .includes(FABRICS.MAPS.toUpperCase())) {
      // return FabricsNames.MAPS;
      // }
      // else if (url.toUpperCase()
      // .includes(FabricsPath.CONSTRUCTION.toUpperCase())) {
      //   return FabricsNames.CONSTRUCTION;
      // }
      // else {
      //   return null;
      // }
      let fabric = null;
      if (url) {
        url = decodeURIComponent(url).split('?')[0]
        fabric = url.split('/')[1]
      }
      return fabric;
    }
    catch (e) {
      this.appLogException(new Error('Exception in getFabricNameByUrl() of CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }

  getFabricNameByTab(key) {
    let result = null;
    switch (key) {
      case TreeTypeNames.PeopleAndCompanies:
        result = FABRICS.PEOPLEANDCOMPANIES;
        break;
      case TreeTypeNames.BusinessIntelligence:
        result = FABRICS.BUSINESSINTELLIGENCE;
        break;
      case TreeTypeNames.CONSTRUCTION:
        result = FabricsNames.CONSTRUCTION;
        break;
      case TreeTypeNames.PROJECTS:
        result = FabricsNames.PROJECTS;
        break;
      case TreeTypeNames.FABRICATION:
        result = FabricsNames.FABRICATION;
        break;
      case TreeTypeNames.POSSIBLEPIPELINEINTEGRITY:
        result = FabricsNames.POSSIBLEPIPELINEINTEGRITY;
        break;
      case TreeTypeNames.REPORTING:
        result = FabricsNames.REPORTING;
        break;
      case TreeTypeNames.Maps:
        result = FabricsNames.MAPS;
        break;
      default:
        break;
    }
    return result;
  }
  GetFabricNameFromURL(url) {
    try {
      switch (url) {

        case FABRICS.PEOPLEANDCOMPANIES:
          return FABRICS.PEOPLEANDCOMPANIES;
          break;

        case "bifabric":
          return "BI";
          break;

        case "Security":
          return "Security";
          break;

        default:
          return null;
      }
    }
    catch (e) {
      this.appLogException(new Error('Exception in GetFabricNameFromURL() of CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }

  getTreeTabBaseOnUrl(url) {
    let fabric = this.getFabricNameByUrl(url);
    let tabKey = "TAB=";
    let result = "";

    switch (fabric) {
      case FabricsNames.ENTITYMANAGEMENT:
        if (url.toUpperCase().includes(tabKey + "People".toUpperCase()))
          result = "People & Companies";
        else if (url.toUpperCase().includes(tabKey + "Business".toUpperCase()))
          result = "Business Intelligence";
        break;
      case FABRICS.BUSINESSINTELLIGENCE:
        if (url.toUpperCase().includes(tabKey + "Lists".toUpperCase()))
          result = "Lists";
        else if (url.toUpperCase().includes(tabKey + "All".toUpperCase()))
          result = "All";
        break;
      default:
        //console.error('No fabric exists' + fabric);
        break;
    }
    return result;
  }

  getEntityIDFromUrl(url) {
    let entityId = null;
    if (url) {
      let paramsArray = url.split(';');
      if (paramsArray && paramsArray.length) {
        if (paramsArray.length == 1) {
          let paramUrlArray = url.split('?');
          if (paramUrlArray && paramUrlArray.length) {
            let queryParams = paramUrlArray[1];
            let andParams = queryParams.split('&');
            andParams.forEach(strValue => {
              if (strValue && strValue.includes('EntityID')) {
                entityId = strValue.replace('EntityID=', '');
              } else if (strValue && strValue.includes('EntityId')) {
                entityId = strValue.replace('EntityId=', '');
              }
            });
          }
        }
        else {
          paramsArray.forEach(strValue => {
            if (strValue.includes('EntityID'))
              entityId = strValue.replace('EntityID=', '');
          });
        }
      }
    }
    return entityId;
  }

  updateIndexDBUserPrefrence(fabric?: any, theme?: any, pinnedTab?: any, key?: any, value?: any, IsBackendRequestNeed?: any, isUpdateIndexDB?: any) {
    try {
      let openFabric = fabric ? this.getCurrentFabricName(fabric) : this.lastOpenedFabric;
      let userPinnedTab = pinnedTab ? pinnedTab : this.faviconList;

      //let selectedTheme = theme ? theme : this.themeDark;

      let fabricKey = key ? key : null;
      let fabricValue = value ? value : null;

      var payload = {
        "Fabric": openFabric,
        "UserPinnedTabs": userPinnedTab,
        "ZoomPercentage": this.ZoomPercentage
      };
      if (theme == true || theme == false) {
        console.log("updateIndexDBUserPrefrence theme " + theme);
        payload["Theme"] = theme;
      }
      if (fabricKey != null && fabricValue != null) {
        payload[fabricKey] = fabricValue;
      }
      var dateTime = new Date().toISOString().slice(0, 19).replace('T', ' ');

      var userPayload = { "email": this.username, "cacheName": "TenantDetails", "UpdatePref": payload, "ModifiedDateTime": dateTime };
      this.sendMessageToServer(JSON.stringify(userPayload), EntityTypes.UserPreference, this.currentUserName, EntityTypes.UserPreference, MessageType.UPDATE, 'AllFrontEndButOrigin', '', FABRICS.COMMON.toUpperCase());
      //To update in state
      this.usersStateQuery.updateObjectMultipleValue(UserStateConstantsKeys.userPrefrence, JSON.parse(JSON.stringify(payload)));
      this.updateStateModifiedTime();

    } catch (e) {
      this.appLogException(new Error('Exception in updateIndexDBUserPrefrence() of CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  biFabricLists = [EntityTypes.DashBoard, EntityTypes.Carousel];
  commonLists = [EntityTypes.Personal, EntityTypes.Carousel, EntityTypes.Shared];


  biFabricListDragOperation(droppedOntopNode, draggedFromNode) {
    let entityOrder = 1;
    let child = [];
    let insertAtPosition = "";
    if (droppedOntopNode.data.children.length != 0)
      entityOrder = droppedOntopNode.data.children.length + 1;
    insertAtPosition = droppedOntopNode.data.EntityId;
    let createdPeople = {
      'InsertAtPosition': insertAtPosition,
      Parent: [{ EntityId: droppedOntopNode.data.EntityId, EntityName: droppedOntopNode.data.EntityName }],
      'NodeData': {
        EntityId: draggedFromNode.data.EntityId,
        EntityName: draggedFromNode.data.EntityName,
        EntityType: draggedFromNode.data.EntityType,
        EntityOrder: entityOrder,
        children: child,
        Parent: [{ EntityId: droppedOntopNode.data.EntityId, EntityName: droppedOntopNode.data.EntityName }],
        uuid: droppedOntopNode.data.uuid
      }
    };
    let payload = {
      'EntityType': EntityTypes.Carousel,
      'CapabilityId': this.capabilityFabricId,
      'CategorhyData': createdPeople,
      'isNewListCreation': false,
      'TypeOfList': EntityTypes.Carousel
    };
    this.sendDataToAngularTree1(TreeTypeNames.LIST, "AddNewNode", createdPeople);
    this.sendMessageToServer(JSON.stringify(payload), EntityTypes.UserList, draggedFromNode.data.EntityId, TreeTypeNames.LIST, MessageKind.CREATE, Routing.OriginSession, "Details", this.lastOpenedFabric.toUpperCase());
  }
  /*
   * Sort List based on order
   * */
  moveNodeFromTo(eventTreeData) {
    try {
      var data = eventTreeData;//JSON.parse(JSON.stringify(eventTreeData));
      var draggedFromNode = data.DraggedNode;
      var droppedOntopNode = data.DroppedOntopNode;
      var isDropeNodeEmptyList = false;
      let checkflag = (droppedOntopNode.data && droppedOntopNode.data.children) ? droppedOntopNode.data.children.filter(d => d.EntityId == draggedFromNode.data.EntityId).length > 0 : false;
      if (this.biFabricLists.includes(droppedOntopNode.data.EntityType) && !checkflag) {
        this.biFabricListDragOperation(droppedOntopNode, draggedFromNode);
      }
      else if (draggedFromNode.parent.data.EntityType == EntityTypes.List && !this.commonLists.includes(droppedOntopNode.data.EntityType)) {
        //draggedNode Parrent is List means then Only do operation
        let droppedNodeData;
        let dragNodeData = JSON.parse(JSON.stringify(draggedFromNode.data));
        var dragNodeParent;
        var dragtoNodeParent;

        if (draggedFromNode && draggedFromNode.parent && draggedFromNode.parent.data) {
          dragNodeParent = [{ "EntityId": draggedFromNode.parent.data.EntityId, "EntityName": draggedFromNode.parent.data.EntityName }];
        }
        if (droppedOntopNode && droppedOntopNode.parent && droppedOntopNode.parent.data) {
          dragtoNodeParent = [{ "EntityId": droppedOntopNode.parent.data.EntityId, "EntityName": droppedOntopNode.parent.data.EntityName }];
        }
        var isEntityTypeList = false;
        var isEntityExistInList = false;
        if (droppedOntopNode.data && droppedOntopNode.data.EntityType == EntityTypes.List && droppedOntopNode.data.Parent && droppedOntopNode.data.Parent[0].EntityId && draggedFromNode.parent && draggedFromNode.parent.data && draggedFromNode.parent.data.Parent && Array.isArray(draggedFromNode.parent.data.Parent) && draggedFromNode.parent.data.Parent[0] && draggedFromNode.parent.data.Parent[0].EntityId && draggedFromNode.parent.data.Parent[0].EntityId && droppedOntopNode.data.Parent[0] && droppedOntopNode.data.Parent[0].EntityId == draggedFromNode.parent.data.Parent[0].EntityId) {
          if (droppedOntopNode.data.children.length > 0 && droppedOntopNode.data.Parent[0].EntityId != draggedFromNode.parent.data.Parent[0].EntityId) {
            droppedNodeData = JSON.parse(JSON.stringify(droppedOntopNode.data.children[0]));
            droppedOntopNode.data.children.forEach((nodeData) => {
              if (nodeData.EntityId == dragNodeData.EntityId) {
                isEntityExistInList = true;
              }
            });
          }
          else {
            isDropeNodeEmptyList = true;
          }
          isEntityTypeList = true;
        }
        else if ((EntityTypes.FACILITYCODE == droppedOntopNode.data.EntityType || droppedOntopNode.data.EntityType == EntityTypes.Category || droppedOntopNode.data.EntityType == EntityTypes.DashBoard) && droppedOntopNode.parent.data.Parent[0] && draggedFromNode.parent.data.Parent[0] && droppedOntopNode.parent.data.Parent[0].EntityId == draggedFromNode.parent.data.Parent[0].EntityId) {
          droppedOntopNode.parent.data.children.forEach((nodeData) => {
            if (nodeData.EntityId == droppedOntopNode.data.EntityId) {
              let index = droppedOntopNode.parent.data.children.findIndex((data) => (data && data.EntityId == nodeData.EntityId));
              droppedNodeData = JSON.parse(JSON.stringify(droppedOntopNode.parent.data.children[index]));
              isEntityTypeList = false;
            }

            if (nodeData.EntityId == dragNodeData.EntityId) {
              isEntityExistInList = true;
            }
          });
        }

        if (isDropeNodeEmptyList || (dragNodeData.EntityId != undefined && droppedNodeData != undefined && dragNodeData.EntityId != droppedNodeData.EntityId) && (!isEntityExistInList || (dragNodeData.Parent && droppedNodeData.Parent && dragNodeData.Parent[0] && droppedNodeData.Parent[0] && dragNodeData.Parent[0].EntityId == droppedNodeData.Parent[0].EntityId))) {
          var DraggedNodeList = [];
          var dragedNodeListData = JSON.parse(JSON.stringify(draggedFromNode.parent.data));
          var droppedNodeListData = JSON.parse(JSON.stringify(droppedOntopNode.parent.data));
          let dragNodeIndex: number = dragedNodeListData.children.findIndex((data) => (data && data.EntityId == dragNodeData.EntityId));
          var dropNodeIndex = droppedNodeListData.children.findIndex((data) => (data && data.EntityId == droppedNodeData.EntityId));
          if (draggedFromNode.parent.data.EntityId == droppedOntopNode.parent.data.EntityId || draggedFromNode.parent.data.EntityId == droppedOntopNode.data.EntityId) {
            var isDragNodeIndex = false;
            var isDropNodeIndex = false;

            droppedNodeListData.children.forEach((obj, i) => {
              obj["EntityOrder"] = i + 1;
            });
            dragNodeData["EntityOrder"] = dragNodeIndex + 1;
            droppedNodeData["EntityOrder"] = dropNodeIndex + 1;

            if (dragNodeData["EntityOrder"] < droppedNodeData["EntityOrder"]) {
              for (let nodeIndex = 0; nodeIndex < droppedNodeListData.children.length; nodeIndex++) {
                var dropNodeObj = {};
                dropNodeObj = JSON.parse(JSON.stringify(dropNodeObj));
                if (isDragNodeIndex) {
                  droppedNodeListData["children"][nodeIndex]["EntityOrder"] = droppedNodeListData["children"][nodeIndex]["EntityOrder"] - 1;
                }
                if (dragNodeData["EntityId"] == droppedNodeListData["children"][nodeIndex]["EntityId"]) {
                  droppedNodeListData["children"][nodeIndex]["EntityOrder"] = droppedNodeData["EntityOrder"];
                  isDragNodeIndex = true;
                }
                else if (droppedNodeData["EntityId"] == droppedNodeListData["children"][nodeIndex]["EntityId"]) {
                  isDragNodeIndex = false;
                }
                dropNodeObj["EntityId"] = droppedNodeListData["children"][nodeIndex]["EntityId"],
                  dropNodeObj["EntityName"] = droppedNodeListData["children"][nodeIndex]["EntityName"],
                  dropNodeObj["EntityOrder"] = droppedNodeListData["children"][nodeIndex]["EntityOrder"];
                DraggedNodeList.push(dropNodeObj);
                // var treepayload = { "EntityId": droppedNodeListData["children"][nodeIndex]["EntityId"], "NodeData": droppedNodeListData["children"][nodeIndex] }
                // this.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.updateNodeById, treepayload);
              }
            }
            else if (dragNodeData["EntityOrder"] > droppedNodeData["EntityOrder"]) {
              if (isEntityTypeList) {
                for (let nodeIndex = 0; nodeIndex < droppedOntopNode.data.children.length; nodeIndex++) {
                  var dropNodeObj = {};
                  dropNodeObj = JSON.parse(JSON.stringify(dropNodeObj));

                  if (isDropNodeIndex) {
                    droppedOntopNode["data"]["children"][nodeIndex]["EntityOrder"] = droppedOntopNode["data"]["children"][nodeIndex]["EntityOrder"] + 1;
                  }
                  if (dragNodeData["EntityId"] == droppedOntopNode["data"]["children"][nodeIndex]["EntityId"]) {
                    droppedOntopNode["data"]["children"][nodeIndex]["EntityOrder"] = droppedNodeData["EntityOrder"] - 1;
                    isDropNodeIndex = false;
                  }
                  else if (droppedNodeData["EntityId"] == droppedOntopNode["data"]["children"][nodeIndex]["EntityId"]) {
                    droppedOntopNode["data"]["children"][nodeIndex]["EntityOrder"] = droppedOntopNode["data"]["children"][nodeIndex]["EntityOrder"] + 1;
                    isDropNodeIndex = true;
                  }
                  dropNodeObj["EntityId"] = droppedOntopNode["data"]["children"][nodeIndex]["EntityId"],
                    dropNodeObj["EntityName"] = droppedOntopNode["data"]["children"][nodeIndex]["EntityName"],
                    dropNodeObj["EntityOrder"] = droppedOntopNode["data"]["children"][nodeIndex]["EntityOrder"];
                  DraggedNodeList.push(dropNodeObj);
                  // var treepayload = { "EntityId": droppedOntopNode["data"]["children"][nodeIndex]["EntityId"], "NodeData": droppedOntopNode["data"]["children"][nodeIndex] }
                  // this.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.updateNodeById, treepayload);
                }
              }
              else {
                for (let nodeIndex = 0; nodeIndex < droppedNodeListData.children.length; nodeIndex++) {
                  var dropNodeObj = {};
                  dropNodeObj = JSON.parse(JSON.stringify(dropNodeObj));

                  if (isDropNodeIndex) {
                    droppedNodeListData["children"][nodeIndex]["EntityOrder"] = droppedNodeListData["children"][nodeIndex]["EntityOrder"] + 1;
                  }
                  if (dragNodeData["EntityId"] == droppedNodeListData["children"][nodeIndex]["EntityId"]) {
                    droppedNodeListData["children"][nodeIndex]["EntityOrder"] = droppedNodeData["EntityOrder"] + 1;
                    isDropNodeIndex = false;
                  }
                  else if (droppedNodeData["EntityId"] == droppedNodeListData["children"][nodeIndex]["EntityId"]) {
                    isDropNodeIndex = true;
                  }
                  dropNodeObj["EntityId"] = droppedNodeListData["children"][nodeIndex]["EntityId"],
                    dropNodeObj["EntityName"] = droppedNodeListData["children"][nodeIndex]["EntityName"],
                    dropNodeObj["EntityOrder"] = droppedNodeListData["children"][nodeIndex]["EntityOrder"];
                  DraggedNodeList.push(dropNodeObj);
                  // var treepayload = { "EntityId": droppedNodeListData["children"][nodeIndex]["EntityId"], "NodeData": droppedNodeListData["children"][nodeIndex] }
                  // this.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.updateNodeById, treepayload);
                }
              }
            }
          }
          else {
            let num = dragNodeIndex + 1;
            for (var nodeIndex = num; nodeIndex < dragedNodeListData.children.length; nodeIndex++) {
              var dragedNodeObj = {};
              dragedNodeObj = JSON.parse(JSON.stringify(dragedNodeObj));

              dragedNodeListData["children"][nodeIndex]["EntityOrder"] = dragedNodeListData["children"][nodeIndex]["EntityOrder"] - 1;
              dragedNodeObj["EntityId"] = dragedNodeListData["children"][nodeIndex]["EntityId"],
                dragedNodeObj["EntityName"] = dragedNodeListData["children"][nodeIndex]["EntityName"],
                dragedNodeObj["EntityOrder"] = dragedNodeListData["children"][nodeIndex]["EntityOrder"];
              DraggedNodeList.push(dragedNodeObj);

              var treepayload = { "EntityId": dragedNodeListData["children"][nodeIndex]["EntityId"], "NodeData": dragedNodeListData["children"][nodeIndex] };
              this.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.updateNodeById, treepayload);
            }
          }
          if (DraggedNodeList.length > 0) {
            DraggedNodeList = JSON.parse(JSON.stringify(DraggedNodeList));
            DraggedNodeList = this.sortByEntityOrder(DraggedNodeList);
            // DraggedNodeList.forEach((obj)=>{
            //    var treepayload = { "EntityId":obj.EntityId, "NodeData": obj }
            //    this.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.updateNodeById, treepayload);
            //  })
          }
          if (draggedFromNode.data.Parent != undefined && draggedFromNode.data.Parent.length != 0) {
            this.sendMessageToServer(JSON.stringify(DraggedNodeList), "EntitiesOrder", draggedFromNode.data.Parent[0].EntityId, EntityTypes.List, MessageKind.UPDATE, Routing.OriginSession, "Details", this.lastOpenedFabric.toUpperCase());
          }
          else {
            if (draggedFromNode.parent && draggedFromNode.parent.data) {
              this.sendMessageToServer(JSON.stringify(DraggedNodeList), "EntitiesOrder", draggedFromNode.parent.data.EntityId, EntityTypes.List, MessageKind.UPDATE, Routing.OriginSession, "Details", this.lastOpenedFabric.toUpperCase());
            }
          }

          if (isDropeNodeEmptyList || dragNodeParent.length > 0 && dragtoNodeParent.length > 0 && dragtoNodeParent[0].EntityId != dragNodeParent[0].EntityId ||
            draggedFromNode.data.Parent && draggedFromNode.data.Parent.length > 0 && droppedOntopNode.data.Parent && droppedOntopNode.data.Parent > 0
            && draggedFromNode.data.Parent[0].EntityId != droppedOntopNode.data.Parent[0].EntityId
            || (draggedFromNode.parent && draggedFromNode.parent.data.EntityId != droppedOntopNode.parent.data.EntityId
              && draggedFromNode.parent.data.EntityId != droppedOntopNode.data.EntityId)) {

            var DroppedNodeList = [];
            if (EntityTypes.FACILITYCODE == droppedOntopNode.data.EntityType) {
              for (var nodeIndex: number = dropNodeIndex + 1; nodeIndex < droppedNodeListData.children.length; nodeIndex++) {
                var dropedNodeObj = {};
                droppedNodeListData["children"][nodeIndex]["EntityOrder"] = droppedNodeListData["children"][nodeIndex]["EntityOrder"] + 1;
                dropedNodeObj["EntityId"] = droppedNodeListData["children"][nodeIndex]["EntityId"],
                  dropedNodeObj["EntityName"] = droppedNodeListData["children"][nodeIndex]["EntityName"],
                  dropedNodeObj["EntityOrder"] = droppedNodeListData["children"][nodeIndex]["EntityOrder"];
                DroppedNodeList.push(dropedNodeObj);

                var treepayload = { "EntityId": droppedNodeListData["children"][nodeIndex], "NodeData": droppedNodeListData["children"][nodeIndex] };
                this.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.updateNodeById, treepayload);
              }
              dragNodeData["EntityOrder"] = droppedNodeData["EntityOrder"] + 1;
            }
            else if (droppedOntopNode.data.EntityType == EntityTypes.List) {
              for (let nodeIndex = 0; nodeIndex < droppedOntopNode.data.children.length; nodeIndex++) {
                var dropedNodeObj = {};
                droppedOntopNode["data"]["children"][nodeIndex]["EntityOrder"] = droppedOntopNode["data"]["children"][nodeIndex]["EntityOrder"] + 1;
                dropedNodeObj["EntityId"] = droppedOntopNode["data"]["children"][nodeIndex]["EntityId"],
                  dropedNodeObj["EntityName"] = droppedOntopNode["data"]["children"][nodeIndex]["EntityName"],
                  dropedNodeObj["EntityOrder"] = droppedOntopNode["data"]["children"][nodeIndex]["EntityOrder"];
                DroppedNodeList.push(dropedNodeObj);

                var treepayload = { "EntityId": droppedOntopNode["data"]["children"][nodeIndex]["EntityId"], "NodeData": droppedOntopNode["data"]["children"][nodeIndex] };
                this.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.updateNodeById, treepayload);
              }
              dragNodeData["EntityOrder"] = 1;
            }
            var DropNodeDetails = [];
            var nodeObj = {
              "PreviousListId": dragedNodeListData["EntityId"],
              "EntityId": dragNodeData["EntityId"],
              "EntityName": dragNodeData["EntityName"],
              "EntityOrder": dragNodeData["EntityOrder"]
            };
            DropNodeDetails.push(nodeObj);
            let PayloadDropNodeList = {
              "DroppedEntities": DropNodeDetails,
              "OrderChangedEntites": DroppedNodeList
            };
            if (isDropeNodeEmptyList) {
              this.sendMessageToServer(JSON.stringify(isDropeNodeEmptyList), "EntityChangedToAnotherList", droppedOntopNode.data.EntityId, EntityTypes.List, MessageKind.UPDATE, Routing.OriginSession, "Details", this.lastOpenedFabric.toUpperCase());
            }
            else {
              if (droppedNodeData.Parent && droppedNodeData.Parent[0] && droppedNodeData.Parent[0].EntityId) {
                this.sendMessageToServer(JSON.stringify(isDropeNodeEmptyList), "EntityChangedToAnotherList", droppedNodeData.Parent[0].EntityId, EntityTypes.List, MessageKind.UPDATE, Routing.OriginSession, "Details", this.lastOpenedFabric.toUpperCase());
              }
            }
          }

          let draggedNodeParent: any;
          let droppedOnNodeParent: any;
          let draggedNodeUpdatedData: any;

          if (isDropeNodeEmptyList) {
            if (draggedFromNode.data.parent) {
              this.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.deleteNodeById, dragNodeData);
            }
            else {
              this.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.deleteNodeById, draggedFromNode.data.EntityId);
            }

            if (Array.isArray(dragNodeData.Parent) && dragNodeData.Parent.length != 0) {
              let tempDraggedNodeParent = this.getEntitiesById(dragNodeData.Parent[0].EntityId);
              draggedNodeParent = JSON.parse(JSON.stringify(tempDraggedNodeParent));
            }

            if (droppedNodeData && Array.isArray(droppedNodeData.Parent) && droppedNodeData.Parent.length != 0) {
              let tempDroppedOnNodeParent = this.getEntitiesById(droppedNodeData.Parent[0].EntityId);
              droppedOnNodeParent = JSON.parse(JSON.stringify(tempDroppedOnNodeParent));
            }

            if (dragNodeData["Parent"] != null)
              dragNodeData["Parent"] = [{ EntityId: droppedOntopNode.data.EntityId, EntityName: droppedOntopNode.data.EntityName }];

            draggedNodeUpdatedData = dragNodeData;
            let createdPeople = {
              'InsertAtPosition': droppedOntopNode.data.EntityId,
              Parent: [{ EntityId: droppedOntopNode.data.EntityId, EntityName: droppedOntopNode.data.EntityName }],
              'NodeData': dragNodeData
            };
            this.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.AddNewNode, createdPeople);
          }
          else {
            if (dragNodeData.Parent && Array.isArray(dragNodeData.Parent) && dragNodeData.Parent.length > 0) {
              let tempDraggedNodeParent = this.getEntitiesById(dragNodeData.Parent[0].EntityId);
              draggedNodeParent = JSON.parse(JSON.stringify(tempDraggedNodeParent));
            }

            if (droppedNodeData && droppedNodeData.Parent && Array.isArray(droppedNodeData.Parent) && droppedNodeData.Parent.length > 0) {
              let tempDroppedOnNodeParent = this.getEntitiesById(droppedNodeData.Parent[0].EntityId);
              droppedOnNodeParent = JSON.parse(JSON.stringify(tempDroppedOnNodeParent));
            }

            draggedNodeUpdatedData = dragNodeData;
            //added due to wrong parent or like empty array going so that list to drag and drop was not haoppening
            if (dragNodeParent) {
              dragNodeData.Parent = dragNodeParent;
            }
            if (droppedNodeData) {
              droppedNodeData.Parent = dragtoNodeParent;
            }
            let Payload = {
              "nodeFrom": dragNodeData,
              "nodeTo": droppedNodeData,
              "listEntityType": isEntityTypeList
            };
            this.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.moveNodeFromTo, Payload);
            dragNodeData.Parent = droppedNodeData.Parent;
          }


          if (draggedNodeUpdatedData && droppedOnNodeParent && draggedNodeParent) {
            <Array<any>>(droppedOnNodeParent.children).push(draggedNodeUpdatedData);
            <Array<any>>(draggedNodeParent.children).filter((entity: any, index, entities) => {
              if (entity && entity.EntityId == draggedNodeUpdatedData.EntityId && droppedOnNodeParent.EntityId != droppedOnNodeParent.EntityId) {
                entities.splice(index, 1);
              }
              return entities;
            });

            droppedOnNodeParent.children = this.sortByEntityOrder(droppedOnNodeParent.children);
            draggedNodeParent.children = this.sortByEntityOrder(draggedNodeParent.children);

            if (droppedOnNodeParent.EntityId == draggedNodeParent.EntityId) {
              if (droppedNodeListData["children"].length > 0) {
                DraggedNodeList.forEach((obj) => { });
                draggedNodeParent.children = this.sortByEntityOrder(droppedNodeListData["children"]);
              }
              this.entityCommonStoreQuery.upsert(draggedNodeParent.EntityId, draggedNodeParent);
            } else {
              this.entityCommonStoreQuery.upsert(droppedOnNodeParent.EntityId, droppedOnNodeParent);
              this.entityCommonStoreQuery.upsert(draggedNodeParent.EntityId, draggedNodeParent);
            }
          }

          this.listCRUDOperations.next(dragNodeData);
        }

      }
    }
    catch (e) {
      this.appLogException(new Error('Exception in moveNodeFromTo() of CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  sortByEntityOrder(entities: Array<any>): Array<any> {
    try {
      let entitiesArray = JSON.parse(JSON.stringify(entities));
      entitiesArray = entitiesArray.sort((n1, n2) => {
        if (n1.EntityOrder > n2.EntityOrder)
          return 1;

        if (n1.EntityOrder < n2.EntityOrder)
          return -1;

        return 0;
      });

      return entitiesArray;
    }
    catch (e) {
      this.appLogException(new Error('Exception in sortByEntityOrder() of CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  sortByEntityName(entities: Array<any>): Array<any> {
    try {
      let entitiesArray = JSON.parse(JSON.stringify(entities));
      entitiesArray = entitiesArray.sort((n1, n2) => {
        if (n1.EntityName > n2.EntityName)
          return 1;

        if (n1.EntityName < n2.EntityName)
          return -1;

        return 0;
      });

      return entitiesArray;
    }
    catch (e) {
      this.appLogException(new Error('Exception in sortByEntityOrder() of CommonService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  dropIDBInstance() {
    try {
      localForage.dropInstance({
        name: "Visur",
        storeName: this.tenantName
      }).then(function () {
        console.log('Dropped otherStore');
      });

      this.deleteCookies();
    }
    catch (e) {
      this.deleteCookies();
    }
  }

  logout() {
    try {
      sessionStorage.setItem('logout', 'true');
      this.logoutbtn = true;
      this.appConfig.loadHead = [];
      this.appConfig.AllHeaderRoutingList = [];
      this.appConfig.AllHeaderRoutingListService = [];
      this.dropIDBInstance();
    }
    catch (e) {
      this.appLogException(new Error('Exception in logout() of CommonService.ts at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  LogoutFromAllOpenTabs() {
    try {
      if (!this._cookieService.get("token")) {
        //this.deleteCookies();
        this.dropIDBInstance();
        return;
      }
    } catch (e) {
      this.appLogException(new Error('Exception in logout() of CommonService.ts at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  deleteCookies() {
    try {
      let numberOfTry = 1000;
      this.sessionSucess = false;
      localStorage.clear();
      this._cookieService.removeAll();

      if (this._cookieService.get("token"))
        this.http.get(AppConfig.BaseURL + 'Home/RemoveCustomCookies?deleteType=All').pipe(untilDestroyed(this, 'destroy')).subscribe();

      let flag: any = 0;
      while (this._cookieService.get("token")) {
        flag++;

        if (flag == numberOfTry) {
          this.appLogException(new Error('Failed to logout.'));
          break;
        }

        console.log("failed to deleteCookies." + flag + "/" + numberOfTry + " time.");
        if (this._cookieService.get("token") && flag <= numberOfTry) {
          localStorage.clear();
          this._cookieService.removeAll();

          if (this._cookieService.get("token"))
            this.http.get(AppConfig.BaseURL + 'Home/RemoveCustomCookies?deleteType=All').pipe(untilDestroyed(this, 'destroy')).subscribe();
        }
      }

      setTimeout(() => {
        window.location.replace(window.location.origin);
      }, 500);
    }
    catch (e) {
      this.appLogException(new Error('Exception in deleteCookies() of CommonService.ts at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }


  securityModeOnAthentication(data) {

  }
  setTypeOfField(jsonData) {

    if (jsonData.facilities) {
      jsonData.facilities.forEach(facilitie => {
        facilitie["typeOfField"] = "Location";
        facilitie["children"] = [];
      });
    }


    if (jsonData.wells) {
      jsonData.wells.forEach(well => {
        well["typeOfField"] = "Location";
        well["children"] = [];
      });
    }

    if (jsonData.headers) {
      jsonData.headers.forEach(header => {
        header["typeOfField"] = "Location";
        header["children"] = [];
      });
    }

    if (jsonData.facilitycode) {
      jsonData.facilitycode.forEach(facilitycode => {
        facilitycode["typeOfField"] = "FacilityCode";
        facilitycode["children"] = [];
      });

    }


  }

  getCapabilityIdsBy(capabilityNameArray) {
    try {
      if (capabilityNameArray)
        return this.authCommonStoreQuery.getValue().Capabilities.filter(
          obj => capabilityNameArray.includes(obj.Fabric)
        ).map(obj => obj.CapabilityId);
    }
    catch (e) {
      this.appLogException(new Error("getCapabilityIdsBy " + e));
    }
  }

  getCapabilityNamesBy(capabilityNameArray) {
    try {
      return this.authCommonStoreQuery.getValue().Capabilities.filter(
        obj => capabilityNameArray.includes(obj.CapabilityId)
      ).map(obj => obj.Fabric);
    }
    catch (e) {
      this.appLogException(new Error("getCapabilityNamesBy " + e));
    }
  }
  getCapabilityName(capabilityId) {
    try {
      if (capabilityId) {
        return this.authCommonStoreQuery.getValue()
          .Capabilities
          .filter((capability: any) => capability && capability.CapabilityId == capabilityId)
          .map(capability => capability.Fabric)[0];
      }
    }
    catch (e) {
      this.appLogException(new Error("getCapabilityName " + e));
    }
  }
  getCapabilityId(capabilityName) {
    try {
      if (capabilityName) {
        capabilityName = capabilityName.toLowerCase() == FABRICS.BUSINESSINTELLIGENCE ? FABRICS.BUSINESSINTELLIGENCE : capabilityName;

        return this.authCommonStoreQuery.getValue()
          .Capabilities
          .filter((capability: any) => capability && capability.Fabric && capability.Fabric.toLowerCase() == capabilityName.toLowerCase())
          .map(capability => capability.CapabilityId)[0];
      }
    }
    catch (e) {
      this.appLogException(new Error("getCapabilityId " + e));
    }
  }

  selectCapabilityId$(fabric: string) {
    return this.authCommonStoreQuery.select(state => state.Capabilities.filter((capability: any) => capability && capability.Fabric && capability.Fabric.toLowerCase() == fabric.toLowerCase()).map(capability => capability.CapabilityId)[0]).first();
  }


  checkStateStatus(capability) {
    return this.entityCommonStoreQuery.getAll().some((d: any) => d && d["SG"] && (<Array<any>>d["SG"]).indexOf(capability) >= 0 && d.EntityType &&
      [EntityTypes.List, EntityTypes.Personal, EntityTypes.Shared].indexOf(d.EntityType) == -1);
  }

  checkListStateStatus(key, value, capability, capabilityID = null) {
    let capabilityId;
    if (capabilityID == null) {
      capabilityId = this.getCapabilityId(capability);
    }
    else {
      capabilityId = capabilityID;
    }

    return this.entityCommonStoreQuery.getAll().some((d: any) => d && d[key] && d[key] == value && d.capability && d.capability == capabilityId);
  }

  getEntitiesFromStateBy(capability) {
    return this.entityCommonStoreQuery.getAll().filter(d => d && d["SG"] && (<Array<any>>d["SG"]).indexOf(capability) >= 0);
  }
  /**
   *
   * @param type worker compution type e.g. All,Licesse,entities etc. etc.
   * @param data data to compute on worker
   */
  getObserveable(type, data, fabric) {
    var data1 = { "Type": type, "flattenData": data, 'fabricName': fabric };

    // this.appLogTrace("type" + type + ",data length:" + data.length);

    return fromWorker<any, any>(() => new Worker(AppConsts.treeWorkerPath), Observable.of(data1));
  }



  EntityIdArrays;//don't remove below method

  /**
   * To get object itself or it's all tree children entities id ,in case of no children for object it will return existing object entity id
   *
   * @param arrayOfHierarchyData array of json object e.g [{},{}...]
   * @param entityId id of the object for from which fetch it's and childrends entityId e.g district obj
   */
  getEntityIds(arrayOfHierarchyData: Array<any>, entityId): Array<string> {
    this.EntityIdArrays = [];
    var objArray = arrayOfHierarchyData.filter(obj => obj.EntityId === entityId);

    //entityId obj exists
    //entity obj has property childeren
    //entity obj childeren property has lenght
    if (objArray.length && objArray[0].hasOwnProperty("children") && objArray[0].children.length) {
      this.EntityIdArrays.push(entityId);
      // logic to get all children
      var childrens: any = objArray[0].children;
      childrens.forEach(obj => {
        this.getChildrens(obj);
      });
    }
    else {//no children
      this.EntityIdArrays.push(entityId);
    }
    return this.EntityIdArrays;
  }

  getChildrens(obj) {
    this.EntityIdArrays.push(obj.EntityId);
    console.log(obj);
    if (obj["children"] && obj["children"].length) {
      obj.children.forEach(chObject => {
        this.getChildrens(chObject);
      });
    }
  }

  maxModifiedDateTimeStateObject(arrayOfObject) {
    return new Date(Math.max.apply(null, arrayOfObject.filter(d => d.ModifiedDateTime != undefined).map(function (e) {
      return new Date(e.ModifiedDateTime);
    })));
  }

  getViewedFabrics(): Array<any> {
    try {
      let viewedFabrics: Array<any> = this.usersStateQuery.getStateKey(UserStateConstantsKeys.viewedFabrics);
      return viewedFabrics ? viewedFabrics : [];
    }
    catch (e) {
      this.appLogException(new Error('Exception in getViewedFabrics() of commonservice at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  appLogException(exception: Error) {
    console.error(exception);
    this.logger.logException(exception);
  }

  appLogEvent(message: string) {
    this.logger.logEvent(message);
  }

  appLogTrace(message: string) {
    console.log(message);
    this.logger.logEvent(message);
  }

  getViewedFabricsAndCheckStatus(fabric: string, options?): boolean {
    try {
      let isEntitiesExist = true;
      console.log("ViewedFabric........");
      if (fabric) {
        let isFabricSplit = fabric.split("_");
        if (isFabricSplit.length > 1) {//entityManagement fabric
          isEntitiesExist = this.ViewedAndStateStatus(fabric);
          if (!isEntitiesExist) {
            isEntitiesExist = this.ViewedAndStateStatus(isFabricSplit[1]);
          }
        }
        else {//individual fabric like fdc,people etc etc
          isEntitiesExist = this.ViewedAndStateStatus(fabric);
        }
      }
      return isEntitiesExist;
    }
    catch (e) {
      this.appLogException(new Error('Exception in getViewedFabricsAndCheckStatus() of commonservice at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  getViewedFabricsAndCheckStatus1(fabric: string, options?): Observable<boolean> {
    try {
      let isEntitiesExist = true;
      console.log("ViewedFabric........");
      if (fabric) {
        let isFabricSplit = fabric.split("_");
        if (isFabricSplit.length > 1) {//entityManagement fabric
          isEntitiesExist = this.ViewedAndStateStatus(fabric);
          if (!isEntitiesExist) {
            isEntitiesExist = this.ViewedAndStateStatus(isFabricSplit[1]);
          }
        }
        else {//individual fabric like fdc,people etc etc
          isEntitiesExist = this.ViewedAndStateStatus(fabric);
        }
      }
      return Observable.of(isEntitiesExist);
    }
    catch (e) {
      this.appLogException(new Error('Exception in getViewedFabricsAndCheckStatus() of commonservice at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  /**
   *
   * @param fabric e.g. EntityManagement_People
   */
  ViewedAndStateStatus(fabric) {
    let isEntitiesExist = true;

    if (fabric == FABRICS.PEOPLEANDCOMPANIES) {
      isEntitiesExist = this.checkStateStatus(FABRICS.PEOPLEANDCOMPANIES);
    }
    else if (fabric == FABRICS.ENTITYMANAGEMENT + "_" + FABRICS.PEOPLEANDCOMPANIES) {
      isEntitiesExist = this.checkStateStatus(FABRICS.PEOPLEANDCOMPANIES);
    }
    else {
      let viewedFabrics: Array<any> = this.getViewedFabrics();

      if (viewedFabrics.indexOf(fabric) >= 0 || viewedFabrics.indexOf(FABRICS.ENTITYMANAGEMENT + "_" + fabric)) {
        let status = this.checkStateStatus(fabric);
        if (!status)
          isEntitiesExist = false;
        else
          isEntitiesExist = true;
      }
      else
        isEntitiesExist = false;
    }
    return isEntitiesExist;
  }

  updateViewedFabric(fabric: any) {
    try {
      if (fabric) {
        let tempData: Array<any> = this.getViewedFabrics();
        let tempViewedFabrics: Array<any> = JSON.parse(JSON.stringify(tempData));
        if (!(tempViewedFabrics.indexOf(fabric) >= 0)) {
          tempViewedFabrics.push(fabric);
          this.usersStateQuery.add([UserStateConstantsKeys.viewedFabrics], tempViewedFabrics);
        }
      }
    }
    catch (e) {
      this.appLogException(new Error('Exception in updateViewedFabric() of commonservice at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  updateEntityFilterStatus(status: boolean) {
    try {
      if (status != undefined) {
        this.usersStateQuery.add([UserStateConstantsKeys.entityFilter], status);
      }
      else {
        this.appLogException(new Error("updateEntityFilterStatus couldn't be undefined"));
      }
    }
    catch (e) {
      this.appLogException(new Error(e));
    }
  }

  getEntityFilterStatus(): boolean {
    try {
      let entityFilterStatus: boolean = this.usersStateQuery.getStateKey(UserStateConstantsKeys.entityFilter);
      return entityFilterStatus;
    }
    catch (e) {
      this.appLogException(new Error(e));
    }
    return false;
  }


  updateEntitiesStatePropertyBy(entityId: string, propertyToUpdate: any, value: any) {
    var entity = this.getEntitiesById(entityId);
    if (entity[propertyToUpdate]) {
      entity[propertyToUpdate] = value;
      this.entityCommonStoreQuery.upsert(entityId, entity);
    }
    else {
      this.appLogException(new Error("given property" + propertyToUpdate + " does not exist for the EntityId " + entityId));
    }
  }

  getPeopleEntitiesFromStoreOrBackend$(type, types: Array<any>, filterFunc, mapperFunc, capability?) {
    let capabilityId = this.getCapabilityId(capability);
    return this.entityCommonStoreQuery.selectAll({
      filterBy: entity => entity && (types.indexOf(entity[type]) >= 0) && (capabilityId != undefined ? (entity["Capabilities"] != undefined ? entity["Capabilities"].indexOf(capabilityId) >= 0 : true) : true)
    })
      .flatMap(d => from(d).filter(d => {
        return types.indexOf(d[type]) >= 0;
      }).filter(filterFunc).map(mapperFunc).toArray()).first().pipe(switchMap((res) => {
        let apiCall: Observable<any[]>;

        // if (this.isElectron && !this.onlineOffline) {
        //   apiCall = this.DesktopServiceInstances["DeskTopService"].readPeopleEntitiesForStateMessage(this.getMessageForPeopleRead(false)).pipe(map((res: string) => {
        //     return this.setPeopleAndCompaniesIntoState(JSON.stringify(res),filterFunc, mapperFunc, capabilityId);
        //   }))
        // }
        // else {
        //read from backend and set into state
        apiCall = this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, this.getMessageForPeopleRead(false))
          .map(message => {
            return this.setPeopleAndCompaniesIntoState(message, filterFunc, mapperFunc, capabilityId);
          });
        // }

        // return to caller either state or backend result
        return res.length ? of(res) : apiCall;
      }));
  }

  getCompaniesFromStore$(): Observable<any[]> {
    return this.entityCommonStoreQuery.selectAll().flatMap(d => from(d)
      .filter((d: any) => d && d.EntityType == "Company").map(d => {
        return { "entityid": d.EntityId, "entitytype": d.EntityType, "entityname": d.EntityName };
      }).toArray());
  }

  userSettingCompanyAndOfficeRead$(type, types: Array<any>, filterFunc, mapperFunc, officeId, companyId, capability?) {
    let entities = [];
    let apiCall: Observable<any[]>;

    // if (this.isElectron && !this.onlineOffline) {
    //   apiCall = this.DesktopServiceInstances["DeskTopService"].readPeopleEntitiesForStateMessage(this.getMessageForPeopleRead(true, officeId, companyId)).pipe(map((message: string) => {
    //     let igniteData = this.getPeopleMapper(message);
    //     if (igniteData && igniteData.Companies && igniteData.Offices) {
    //       entities = [...igniteData.Companies, ...igniteData.Offices];
    //     }
    //     return entities;
    //   }))
    // }
    // else {
    //read from backend
    apiCall = this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, this.getMessageForPeopleRead(true, officeId, companyId))
      .map(message => {
        let igniteData = this.getPeopleMapper(message);
        if (igniteData && igniteData.Companies && igniteData.Offices) {
          entities = [...igniteData.Companies, ...igniteData.Offices];
        }
        return entities;
      });
    // }
    // return to caller backend result
    return entities.length ? of(entities) : apiCall;
  }


  readCompanyTypes$(type, types: Array<any>, filterFunc, mapperFunc, capability, readingType?) {
    let capabilityId = this.getCapabilityId(capability);
    return this.entityCompanyTypesStoreQuery.selectAll({
      filterBy: entity => entity && (types.indexOf(entity[type]) >= 0) && entity["CapabilityId"] == capabilityId
    })
      .flatMap(d => from(d).filter(d => {
        return types.indexOf(d[type]) >= 0;
      }).filter(filterFunc).map(mapperFunc).toArray()).first().pipe(switchMap((res) => {

        let apiCall: Observable<any[]>;
        // if (this.isElectron && !this.onlineOffline) {
        //   apiCall = this.DesktopServiceInstances["DeskTopService"].GetCompanyTypeData(this.companyTypesPayloadMessage()).pipe(map((res: string) => {
        //       let message = typeof (res) == "string" ? JSON.parse(res) : res;
        //       if (readingType)
        //         return this.setCompanyTypesIntoState(message);
        //       else
        //         return this.setCompanyTypesIntoState(message, capabilityId, filterFunc,mapperFunc);
        //     }));
        // }
        // else {
        apiCall = this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, this.companyTypesPayloadMessage())
          .map(message => {
            if (readingType)
              return this.setCompanyTypesIntoState(message);
            else
              return this.setCompanyTypesIntoState(message, capabilityId, filterFunc, mapperFunc);
          });
        // }

        return res.length ? of(res) : apiCall;
      }));

  }

  companyTypesPayloadMessage() {
    var payload = {
      DataType: "CompanyTypes",
      EntityID: "CompanyTypes",
      EntityType: 'CompanyTypes',
      MessageKind: "READ",
      Routing: Routing.OriginSession,
      Type: "Details",
      Fabric: FABRICS.PEOPLEANDCOMPANIES
    };

    let message = this.getMessage();
    message.Payload = JSON.stringify(payload);
    message.DataType = payload.DataType;
    message.EntityID = payload.EntityID;
    message.EntityType = payload.EntityType;
    message.MessageKind = payload.MessageKind;
    message.Routing = payload.Routing;
    message.Type = payload.Type;
    message.Fabric = payload.Fabric;
    message.CapabilityId = this.getCapabilityId(payload.Fabric);

    return message;
  }

  setCompanyTypesIntoState(backendResponse: any, capability?, filterFunc?, mapperFunc?) {
    let igniteData = this.getPeopleMapper(backendResponse);
    return this.formattingCompanyTypesAndStoreIntoState(igniteData, capability, filterFunc, mapperFunc);
  }

  formattingCompanyTypesAndStoreIntoState(igniteData, capability?, filterFunc?, mapperFunc?) {
    let existingEntities = [];
    let newEntities = [];

    if (igniteData && igniteData.CompanyTypes) {
      var entities = [...igniteData.CompanyTypes];

      for (var index in entities) {
        if (entities[index]) {
          let entityId = entities[index].EntityId;
          let entityData = this.entityCompanyTypesStoreQuery.getEntity(entityId);
          if (entityData) {
            existingEntities.push(entityId);
            continue;
          }
          newEntities.push(entities[index]);
        }
      }

      if (existingEntities.length > 0)
        this.entityCompanyTypesStoreQuery.UpdateArrayValue(existingEntities, "SG", FABRICS.PEOPLEANDCOMPANIES);

      if (newEntities.length > 0)
        this.entityCompanyTypesStoreQuery.AddAll(newEntities);
    }

    if (newEntities.length && capability && filterFunc) {//only when state data not there and very first time reading with form at needed
      newEntities = newEntities.filter(entity => entity && entity["CapabilityId"] == capability).filter(filterFunc);
    }
    if (mapperFunc && filterFunc)
      return entities.filter(entity => entity && entity["CapabilityId"] == capability).filter(filterFunc).map(mapperFunc);
    else
      return newEntities;
  }


  getMessageForPeopleRead(isUserSetting, officeId?, companyId?) {
    var payload = {
      DataType: "PeopleEntities",
      EntityID: "PeopleAndCompany",
      EntityType: Datatypes.PEOPLE,
      MessageKind: "READ",
      Routing: Routing.OriginSession,
      Type: "Details",
      Fabric: FABRICS.PEOPLEANDCOMPANIES
    };

    if (isUserSetting) {
      payload["DataType"] = "PersonUserSetting";
      payload["EntityID"] = "ComapnyAndOffice";
      payload["Office"] = officeId;
      payload["Company"] = companyId;
    }

    let message = this.getMessage();
    message.Payload = JSON.stringify(payload);
    message.DataType = payload.DataType;
    message.EntityID = payload.EntityID;
    message.EntityType = payload.EntityType;
    message.MessageKind = payload.MessageKind;
    message.Routing = payload.Routing;
    message.Type = payload.Type;
    message.Fabric = payload.Fabric;
    return message;
  }

  setPeopleAndCompaniesIntoState(backendResponse: any, filterFunc?, mapperFunc?, capability?) {
    let igniteData = this.getPeopleMapper(backendResponse);
    return this.getPeopleMapperSubscription(igniteData, filterFunc, mapperFunc, capability);
  }

  getPeopleMapper(message) {
    var msg = typeof (message) == "string" ? JSON.parse(message) : message;
    if (CommonJsMethods.isvalidJson(msg['Payload'])) {
      var entities = JSON.parse(msg['Payload']);
      return entities;
    }
    else {
      var ex = new Error(msg["Payload"]);
      this.appLogException(ex);
      return ex;
    }
  }

  getPeopleMapperSubscription(igniteData, filterFunc?, mapperFunc?, capability?) {
    if (igniteData) {
      var entities = [...igniteData.Companies, ...igniteData.Offices, ...igniteData.Persons];
      let existingEntities = [];
      let newEntities = [];
      let capabilityId = this.getCapabilityId(FABRICS.PEOPLEANDCOMPANIES);
      for (var index in entities) {
        if (entities[index]) {
          if (!entities[index]["Capabilities"]) {
            entities[index]["Capabilities"] = [];
            entities[index]["Capabilities"].push(capabilityId);
          }
          else {
            if (!((<Array<any>>entities[index]["Capabilities"]).indexOf(capabilityId) >= 0)) {
              entities[index]["Capabilities"].push(capabilityId);
            }
          }

          let entityId = entities[index].EntityId;
          let entityData = this.entityCommonStoreQuery.getEntity(entityId);
          if (entityData) {
            // if fdc not exisit in SG then add into existing entities list.
            if (entityData["SG"] != undefined) {
              if (!((<Array<any>>entityData["SG"]).indexOf(FABRICS.PEOPLEANDCOMPANIES) >= 0)) {
                existingEntities.push(entityId);
              }
              continue;
            }
          }
          //entities[index]["SG"] = [FABRICS.PEOPLEANDCOMPANIES];
          newEntities.push(entities[index]);
        }
      }
      if (existingEntities.length > 0)
        this.entityCommonStoreQuery.UpdateArrayValue(existingEntities, "SG", FABRICS.PEOPLEANDCOMPANIES);

      if (newEntities.length > 0)
        this.entityCommonStoreQuery.AddAll(newEntities);


      if (filterFunc && mapperFunc)
        newEntities = entities.filter(entity => entity && (capabilityId != undefined ? (entity["Capabilities"] != undefined ? entity["Capabilities"].indexOf(capabilityId) >= 0 : true) : true)).filter(filterFunc).map(mapperFunc);

      return newEntities;
    }
  }

  //going to remove
  readPeopleEntitiesBackend(payload: { DataType: String, EntityID: String, EntityType: String, MessageKind: String, Routing: String, Type: String, Fabric: String; }) {
    let message = this.getMessage();
    message.Payload = JSON.stringify(payload);
    message.DataType = payload.DataType;
    message.EntityID = payload.EntityID;
    message.EntityType = payload.EntityType;
    message.MessageKind = payload.MessageKind;
    message.Routing = payload.Routing;
    message.Type = payload.Type;
    // if (this.isElectron && !this.onlineOffline) {
    //   this.DesktopServiceInstances["DeskTopService"].readPeopleEntitiesForStateMessage(message).pipe(map((res: string) => {
    //     return this.getPeopleMapper(res);
    //   })).pipe(untilDestroyed(this, 'destroy')).subscribe(igniteData => {
    //     this.getPeopleMapperSubscription(igniteData);
    //   });
    // } else {
    message.Fabric = payload.Fabric;

    this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .map(message => {
        return this.getPeopleMapper(message);
      }).pipe(untilDestroyed(this, 'destroy'))
      .subscribe(igniteData => {
        this.getPeopleMapperSubscription(igniteData);
      });
    // }
  }

  /******Schema Store ***********/
  /**
   * @param key schema entity key e.g. Fabric_SchemaType_EntityType
   * //for example
   *  FDC_Config_GasWell etc.
   * @param value entity schema
   */
  upsertSchema(key: string, value: string) {
    this.schemaEntityCommonStoreQuery.Add(key, JSON.parse(value));
  }

  storeSchemaStatus(key) {
    let schema = this.schemaEntityCommonStoreQuery.getEntity(key);
    if (!schema) {
      return false;
    }
    else
      return schema;
  }

  readAddNewSchema(fabric): Observable<any> {
    let message = this.getAddNewEntitySchemaAndDatamodel(fabric);
    let key = message.Fabric + "_" + message.EntityType + "_" + message.DataType + "_" + this.isPlusAddForm;
    return this.schemaEntityCommonStoreQuery
      .select(state => state.entities).first()
      .pipe(switchMap((hashCache) => {
        let apiCall;
        // if (this.isElectron && !this.onlineOffline) {
        //   apiCall = this.DesktopServiceInstances["DeskTopService"].GetPiggingAddNewMessage(message)
        //     .pipe(map((res: string) => {
        //       let msg = typeof (res) == "string" ? JSON.parse(res) : res;
        //       let schema = JSON.parse(msg.Payload)["Schema"];
        //       if (schema && schema != "" && schema != "{}")
        //         this.upsertSchema(key, schema);
        //       else
        //         console.error("offline received Schema can't be null or undefined")
        //       return JSON.parse(schema);
        //     }));
        // }
        // else {
        apiCall = this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.baseUrl + 'api/AlThings/GetSchemaData', {}, message)
          .pipe(map((res: string) => {
            let msg = JSON.parse(res);
            let schema = JSON.parse(msg.Payload)["Schema"];
            if (schema && schema != "" && schema != "{}")
              this.upsertSchema(key, schema);
            else
              console.error("backend received Schema can't be null or undefined");
            return JSON.parse(schema);
          }));
        // }

        // check in schema store key exist or not. if not exist switch map to backend else return schema from store.
        let newState;
        if (hashCache[key] && hashCache[key]['id']) {
          newState = JSON.parse(JSON.stringify(hashCache[key]));
          delete newState['id'];
        }
        //<EM> while creating receiver after creating sender if hashCache[key] is undefined then .
        else if (hashCache && hashCache.ENTITYMANAGEMENT_AddNewPig_FDCData) {
          newState = JSON.parse(JSON.stringify(hashCache.ENTITYMANAGEMENT_AddNewPig_FDCData));
          delete newState['id'];
        }
        return newState ? of(newState) : apiCall;
      }));
  }

  getAllEntityTypeBasedOnFabric(CapabilityName?) {
    var Payload = { "CapabilityId": CapabilityName ? this.getCapabilityId(CapabilityName) : this.capabilityFabricId };
    var message = this.getMessage(null, "tenantcapabilityentitytype", "FDCEntities", "Read", "FDCData", Routing.OriginSession, MessageKind.READ, JSON.stringify(Payload));
    // if (this.isElectron && this.onlineOffline != true) {
    //   this.DesktopServiceInstances["DeskTopService"].GetTenantCapabilityEntityType(message).pipe(map((res: string) => {
    //       return res;
    //     }));
    // } else {
    return this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .pipe(map((res: string) => {
        return res;
      }));
    // }
  }

  readSchema(SchemaName, EntityType, datatype) {
    var message = this.getMessage();
    let payload = { "EntityType": EntityType, "Fabric": this.getFabricNameByUrl(this.router.url), "Info": "Admin", "Datatype": datatype, SchemaName: SchemaName };
    message.EntityID = "ReadSchema";
    message.EntityType = EntityType;
    message.DataType = datatype;
    message.MessageKind = MessageKind.READ;
    message.Payload = JSON.stringify(payload);
    let key = message.Fabric + "_" + message.EntityType + "_" + message.DataType;

    // if (this.isElectron && this.onlineOffline != true) {
    //   this.DesktopServiceInstances["DeskTopService"].GetReadEntityTypeSchema(message).pipe(map((res: string) => {
    //     let msg = JSON.parse(res);
    //     return JSON.parse(msg.Payload);
    //   }));
    // } else {
    return this.schemaEntityCommonStoreQuery
      .select(state => state.entities).first()
      .pipe(switchMap((hashCache) => {
        // get schema from backend
        const apiCall = this.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.baseUrl + 'api/AlThings/GetSchema', {}, message)
          .pipe(map((res: string) => {
            let msg = JSON.parse(res);
            let schema = JSON.parse(msg.Payload)["Schema"];
            return JSON.parse(schema);
          }));

        // check in schema store key exist or not. if not exist switch map to backend else return schema from store.
        let newState;
        if (hashCache[key] && hashCache[key]['id']) {
          newState = JSON.parse(JSON.stringify(hashCache[key]));
          delete newState['id'];
        }
        return newState ? of(newState) : apiCall;
      }));
    // }
  }

  public getEntityTreePayload(infoJsonObject: any) {
    try {
      var json = {
        TimeZone: "",
        EntityName: "",
        children: [],
        EntityId: "",
        EntityType: "",
        CreatedBy: "",
        ModifiedBy: "",
        Modified: "",
        Created: "",
        parent: {},
        TypeOfField: "",
        LocationId: "",
        formValidate: "",
        DownStreamLocationId: "",
        DownStreamLocationName: "",
        DownStreamLocationType: "",
        UpStreamLocationId: "",
        UpStreamLocationName: "",
        UpStreamLocationType: "",
        DistrictId: "",
        District: "",
        Area: "",
        AreaId: "",
        Field: "",
        FieldId: "",
        Status: "",
        Latitude: 0.0,
        Longitude: 0.0,
        Scale: 0.0,
        EquipmentStatus: false,
        HaulLocation: "",
        WellType: "",
        ExistingParent: {},
        Active: "",
        LicenseeId: "",
        Capability: []
      };
      if (infoJsonObject.TimeZone) {
        json.TimeZone = typeof (infoJsonObject.TimeZone) == "object" ? infoJsonObject.TimeZone.Value : infoJsonObject.TimeZone;
      }
      if (infoJsonObject.EntityName) {
        json.EntityName = typeof (infoJsonObject.EntityName) == "object" ? infoJsonObject.EntityName.Value : infoJsonObject.EntityName;
      }
      if (infoJsonObject.children) {
        json.children = typeof (infoJsonObject.children) == "object" ? infoJsonObject.children.Value : infoJsonObject.children;
      }
      if (infoJsonObject.DownStreamLocationId) {
        json.DownStreamLocationId = typeof (infoJsonObject.DownStreamLocationId) == "object" ? infoJsonObject.DownStreamLocationId.Value : infoJsonObject.DownStreamLocationId;
      }
      if (infoJsonObject.DownStreamLocationType) {
        json.DownStreamLocationType = typeof (infoJsonObject.DownStreamLocationType) == "object" ? infoJsonObject.DownStreamLocationType.Value : infoJsonObject.DownStreamLocationType;
      }
      if (infoJsonObject.DownStreamLocationName) {
        json.DownStreamLocationName = typeof (infoJsonObject.DownStreamLocationName) == "object" ? infoJsonObject.DownStreamLocationName.Value : infoJsonObject.DownStreamLocationName;
      }
      if (infoJsonObject.UpStreamLocationId) {
        json.UpStreamLocationId = typeof (infoJsonObject.UpStreamLocationId) == "object" ? infoJsonObject.UpStreamLocationId.Value : infoJsonObject.UpStreamLocationId;
      }
      if (infoJsonObject.UpStreamLocationType) {
        json.UpStreamLocationType = typeof (infoJsonObject.UpStreamLocationType) == "object" ? infoJsonObject.UpStreamLocationType.Value : infoJsonObject.UpStreamLocationType;
      }
      if (infoJsonObject.UpStreamLocationName) {
        json.UpStreamLocationName = typeof (infoJsonObject.UpStreamLocationName) == "object" ? infoJsonObject.UpStreamLocationName.Value : infoJsonObject.UpStreamLocationName;
      }
      if (infoJsonObject.EntityId) {
        json.EntityId = typeof (infoJsonObject.EntityId) == "object" ? infoJsonObject.EntityId.Value : infoJsonObject.EntityId;
      }
      if (infoJsonObject.EntityType) {
        json.EntityType = typeof (infoJsonObject.EntityType) == "object" ? infoJsonObject.EntityType.Value : infoJsonObject.EntityType;
      }
      if (infoJsonObject.LocationId) {
        json.LocationId = typeof (infoJsonObject.LocationId) == "object" ? infoJsonObject.LocationId.Value : infoJsonObject.LocationId;
      }
      if (infoJsonObject.CreatedBy) {
        json.CreatedBy = typeof (infoJsonObject.CreatedBy) == "object" ? infoJsonObject.CreatedBy.Value : infoJsonObject.CreatedBy;
      }
      if (infoJsonObject.ModifiedBy) {
        json.ModifiedBy = typeof (infoJsonObject.ModifiedBy) == "object" ? infoJsonObject.ModifiedBy.Value : infoJsonObject.ModifiedBy;
      }
      if (infoJsonObject.Modified) {
        json.Modified = typeof (infoJsonObject.Modified) == "object" ? infoJsonObject.Modified.Value : infoJsonObject.Modified;
      }
      if (infoJsonObject.Created) {
        json.Created = typeof (infoJsonObject.Created) == "object" ? infoJsonObject.Created.Value : infoJsonObject.Created;
      }
      if (infoJsonObject.HaulLocation) {
        json.HaulLocation = typeof (infoJsonObject.HaulLocation) == "object" ? infoJsonObject.HaulLocation.Value : infoJsonObject.HaulLocation;
      }
      if (infoJsonObject.Parent) {
        if (infoJsonObject.Parent.Value == null && infoJsonObject.EntityType.Value == "District") {
          json.parent = { "id": "lkjhgfdsa098765", "text": "lkjhgfdsa098765" };
        } else {
          json.parent = typeof (infoJsonObject.parent) == "object" ? infoJsonObject.parent.Value : infoJsonObject.parent;
        }
      }

      if (!infoJsonObject.Parent) {
        json.parent = this.bindParent(infoJsonObject);
      }
      if (infoJsonObject.TypeOfField) {
        json.TypeOfField = typeof (infoJsonObject.TypeOfField) == "object" ? infoJsonObject.TypeOfField.Value : infoJsonObject.TypeOfField;
      }
      if (infoJsonObject.LocationId) {
        json.LocationId = typeof (infoJsonObject.LocationId) == "object" ? infoJsonObject.LocationId.Value : infoJsonObject.LocationId;
      }
      if (infoJsonObject.formValidate) {
        json.formValidate = typeof (infoJsonObject.formValidate) == "object" ? infoJsonObject.formValidate.Value : infoJsonObject.formValidate;
      }
      if (infoJsonObject.District) {
        json.District = typeof (infoJsonObject.District) == "object" ? infoJsonObject.District.Value : infoJsonObject.District;
      }
      if (infoJsonObject.DistrictId) {
        json.DistrictId = typeof (infoJsonObject.DistrictId) == "object" ? infoJsonObject.DistrictId.Value : infoJsonObject.DistrictId;
      }
      if (infoJsonObject.Area) {
        json.Area = typeof (infoJsonObject.Area) == "object" ? infoJsonObject.Area.Value : infoJsonObject.Area;
      }
      if (infoJsonObject.AreaId) {
        json.AreaId = typeof (infoJsonObject.AreaId) == "object" ? infoJsonObject.AreaId.Value : infoJsonObject.AreaId;
      }
      if (infoJsonObject.Field) {
        json.Field = typeof (infoJsonObject.Field) == "object" ? infoJsonObject.Field.Value : infoJsonObject.Field;
      }
      if (infoJsonObject.FieldId) {
        json.FieldId = typeof (infoJsonObject.FieldId) == "object" ? infoJsonObject.FieldId.Value : infoJsonObject.FieldId;
      }
      if (infoJsonObject.Status) {
        json.Status = typeof (infoJsonObject.Status) == "object" ? infoJsonObject.Status.Value : infoJsonObject.Status;
      }
      if (infoJsonObject.Latitude) {
        json.Latitude = typeof (infoJsonObject.Latitude) == "object" ? infoJsonObject.Latitude.Value : infoJsonObject.Latitude;
      }
      if (infoJsonObject.Longitude) {
        json.Longitude = typeof (infoJsonObject.Longitude) == "object" ? infoJsonObject.Longitude.Value : infoJsonObject.Longitude;
      }
      if (infoJsonObject.Scale) {
        json.Scale = typeof (infoJsonObject.Scale) == "object" ? infoJsonObject.Scale.Value : infoJsonObject.Scale;
      }
      if (infoJsonObject.Active) {
        json.Active = typeof (infoJsonObject.Active) == "object" ? infoJsonObject.Active.Value : infoJsonObject.Active;
      }
      if (infoJsonObject.WellType) {
        json.WellType = typeof (infoJsonObject.WellType) == "object" ? infoJsonObject.WellType.Value : infoJsonObject.WellType;
      }
      if (infoJsonObject.LicenseeId) {
        json.LicenseeId = typeof (infoJsonObject.LicenseeId) == "object" ? infoJsonObject.LicenseeId.Value : infoJsonObject.LicenseeId;
      }
      if (infoJsonObject.Capability) {
        json.Capability = typeof (infoJsonObject.Capability) == "object" ? infoJsonObject.Capability : infoJsonObject.Capability;
      }

      return json;
    }
    catch (ex) {
      this.appLogException(new Error(ex));
    }
  }

  bindParent(infoJsonObject) {
    var parent = {};

    if (infoJsonObject.TypeOfField && infoJsonObject.TypeOfField == "Locations" && infoJsonObject.FieldId != undefined) {
      parent["id"] = infoJsonObject.FieldId;
      parent["text"] = infoJsonObject.Field;
    }
    else if (infoJsonObject.EntityType && infoJsonObject.EntityType == "Field") {
      parent["EntityId"] = infoJsonObject.AreaId;
      parent["EntityName"] = infoJsonObject.Area;
    }
    else if (infoJsonObject.EntityType && infoJsonObject.EntityType == "Area") {
      parent["EntityId"] = infoJsonObject.DistrictId;
      parent["EntityName"] = infoJsonObject.District;
    }

    return parent;
  }

  getMessageModel(Payload: string, DataType: string, EntityID: string, EntityType: string, Messagekind: string, Routing: string, Type: string, Fabric: string) {
    let message = this.getMessage();
    message.Payload = Payload;
    message.DataType = DataType;
    message.EntityID = EntityID;
    message.EntityType = EntityType;
    message.MessageKind = Messagekind;
    message.Routing = Routing;
    message.Type = Type;
    message.Fabric = Fabric;
    return message;
  }

  getauthElement(): AuthStateModel {
    let data = this.authCommonStoreQuery.getValue();
    return data;
  }


  bindParentProperties(formValues) {
    try {
      if (!formValues.Parent) {
        formValues.Parent = this.bindParent(formValues);
      }


      return formValues;
    }
    catch (e) {
      this.appLogException(new Error('Exception in bindParentProperties() of common Service at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }

  /**
*
* Used to manipulate data and create child array and assign to parent node, for every node, whatever data is coming from dgraph server after reading.
* @param {object} payloadJson: is the data fetched from dgraph server for a particular Security group.
* @example example of createEntitiesTreeBasedOnPhysicalFlow(payloadJson) method
*
*  createEntitiesTreeBasedOnPhysicalFlow(payloadJson){
*     //todo
*    }
*/
  createEntitiesTreeBasedOnPhysicalFlow(fabric, entitiesPayload, treeName): Observable<any[]> {
    try {
      let rootNode = [];
      let treeEvent = this.getAngularTreeEvent(fabric, [TreeTypeNames.PHYSICALFLOW], TreeOperations.GetTreeInstance).map((responseModel: TreeDataEventResponseModel) => {
        let treeNames: Array<string> = responseModel.treeName;
        let tree = responseModel.treePayload;
        entitiesPayload.forEach(element => {
          switch (element.EntityType) {
            case "List":
              let ele = element;
              ele["children"] = element["list.entity"] ? element["list.entity"] : [];
              ele["children"] = ele["children"].filter(item => item["EntityName"] != undefined);
              delete element["list.entity"];
              rootNode.push(ele);
              break;
          }
        });
        if (treeNames.indexOf(TreeTypeNames.PHYSICALFLOW) > -1 && tree && tree.treeModel && tree.treeModel.nodes) {
          var treeData = JSON.parse(JSON.stringify(tree.treeModel.nodes));
          let haullocations = { EntityId: EntityTypes.HaulLocation, EntityType: EntityTypes.HaulLocation, EntityName: 'Haul Location', children: [] };
          entitiesPayload.forEach(element => {
            switch (element.TypeOf) {
              case "well":
                treeData.forEach(location => {
                  var entityObj = this.filterFFVPhysicalFlowTreeData(location, element);
                  if (entityObj != null) {
                    let entitydata = this.getEntitiesById(entityObj.EntityId);
                    //if (entityObj.Parent && ((entityObj.Parent.length && entityObj.Parent[0]["EntityName"] == "ExternalLocation") ||
                    //  (entitydata && entitydata.Properties && entitydata.Properties.length && entitydata.Properties[0].HaulLocation)) && this.isSecurityAdmin) {
                    //  haullocations.children.push(entityObj);
                    //}
                    //else{
                    //rootNode.push(entityObj);
                    //}
                    rootNode.push(entityObj);
                  }
                });
                break;
              case "facility":
              case "delivered":
                treeData.forEach(location => {
                  var entityObj = this.filterFFVPhysicalFlowTreeData(location, element);
                  if (entityObj != null) {
                    let entitydata = this.getEntitiesById(entityObj.EntityId);
                    //if (entityObj.Parent && ((entityObj.Parent.length && entityObj.Parent[0]["EntityName"] == "ExternalLocation") ||
                    //  (entitydata && entitydata.Properties && entitydata.Properties.length && entitydata.Properties[0].HaulLocation)) && this.isSecurityAdmin) {
                    //  haullocations.children.push(entityObj);
                    //}
                    //else{
                    //rootNode.push(entityObj);
                    //}
                    rootNode.push(entityObj);
                  }
                });
                break;
            }
          });

          //if (this.isSecurityAdmin) {
          //  if (haullocations.children.length) {
          //    rootNode.splice(0, 0, haullocations);
          //  }
          //  rootNode = this.sortNodesByOrder(rootNode, AppConsts.SGEntitySortingOrderArray)
          //}
          entitiesPayload = rootNode;
        }
        if (rootNode) {
          rootNode.forEach(item => {
            item["isParent"] = true;
          });
        }
        let treeNodeData = rootNode.reverse();
        this.sendDataToAngularTree1(treeName, 'createTree', treeNodeData);
        return rootNode;
      });

      return rootNode.length ? of(rootNode) : treeEvent;
    }
    catch (e) {
      console.error('Exception in createEntitiesTreeBasedOnPhysicalFlow() of CommonserService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in createEntitiesTreeBasedOnPhysicalFlow() of CommonserService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  createEntitiesTreeBasedOnALLTree(fabric, entitiesPayload, treeName): Observable<any[]> {
    try {
      let rootNode = [];
      let treeEvent = this.getAngularTreeEvent(fabric, [TreeTypeNames.ALL], TreeOperations.GetTreeInstance).map((responseModel: TreeDataEventResponseModel) => {
        let treeNames: Array<string> = responseModel.treeName;
        let tree = responseModel.treePayload;
        entitiesPayload.forEach(element => {
          switch (element.EntityType) {
            case "List":
              let ele = element;
              ele["children"] = element["list.entity"] ? element["list.entity"] : [];
              ele["children"] = ele["children"].filter(item => item["EntityName"] != undefined);
              delete element["list.entity"];
              rootNode.push(ele);
              break;
          }
        });
        if (treeNames.indexOf(TreeTypeNames.ALL) > -1 && tree && tree.treeModel && tree.treeModel.nodes) {
          entitiesPayload.forEach(element => {
            switch (element.TypeOf) {
              case "Category":
              case "DashBoard":
                let entitydata = this.getEntitiesById(element.EntityId);
                if (entitydata) {
                  rootNode.push(entitydata);
                }
                break;
            }
          });
          rootNode = this.GetParentChildRelationship(rootNode);
          entitiesPayload = rootNode;
        }
        if (rootNode) {
          rootNode.forEach(item => {
            item["isParent"] = true;
          });
        }
        this.sendDataToAngularTree1(treeName, 'createTree', rootNode.reverse());
        return rootNode;
      });

      return rootNode.length ? of(rootNode) : treeEvent;
    }
    catch (e) {
      console.error('Exception in createEntitiesTreeBasedOnPhysicalFlow() of CommonserService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in createEntitiesTreeBasedOnPhysicalFlow() of CommonserService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  filterFFVPhysicalFlowTreeData(element, matchingTitle) {
    if (element.EntityId == matchingTitle.EntityId) {
      return element;
    } else if (element.children != undefined && element.children.length != undefined && element.children.length != 0) {
      let result = null;
      for (let i = 0; result == null && i < element.children.length; i++) {
        result = this.filterFFVPhysicalFlowTreeData(element.children[i], matchingTitle);
      }
      return result;
    }
    return null;
  }


  loadingBarAndSnackbarStatus(loaderStatus, status?: string, duration?) {
    if (loaderStatus == "start") {
      this.loadingBar.start();
    }
    else if (loaderStatus == "complete") {
      this.loadingBar.complete();
    }
    if (status != undefined) {
      setTimeout(() => {
        this.statusText = status;
      }, 0);
    }
    if (duration) {
      setTimeout(() => {
        this.statusText = "";
      }, duration);
    }
  }

  setAsDefaultToState(fabric, setAsDefaultTitle) {
    //set as default to state
    var pathArray = [fabric, "SetAsDefault", "Value"];
    if (fabric.toLowerCase() == FabricsNames.SECURITY.toLowerCase()) {
      let value = {
        "SetAsDefault": {
          "Type": "string",
          "Value": setAsDefaultTitle
        }
      };
      pathArray = [fabric];
      setAsDefaultTitle = value;
    }
    this.usersStateQuery.update(pathArray, setAsDefaultTitle);


    this.updateUserPrefrence(fabric);
  }

  getSetAsDefaultFabricNameBy(fabricName) {
    let fabric = "";
    switch (fabricName) {
      case "BI":
        fabric = UserStateConstantsKeys.BI;
        break;
      case "Pigging":
        fabric = UserStateConstantsKeys.Pigging;
        break;
      case "OperationalForm":
        fabric = UserStateConstantsKeys.OperationalForms;
        break;
      case "EntityManagement":
        fabric = UserStateConstantsKeys.EntityManagement;
        break;
      case "Security":
        fabric = UserStateConstantsKeys.Security;
        break;
      default:
        console.warn(fabricName + 'does not have setAsDefault');
        break;
    }
    return fabric;
  }

  getAllHeaderRoutingFabricNameForUserPrefrence(fabricName) {
    let fabric = "";
    switch (fabricName) {
      case UserStateConstantsKeys.BI:
        fabric = "BI";
        break;
      case "Pigging":
        fabric = UserStateConstantsKeys.Pigging;
        break;
      case UserStateConstantsKeys.OperationalForms:
        fabric = "OperationalForm";
        break;
      case UserStateConstantsKeys.EntityManagement:
        fabric = "EntityManagement";
        break;
      case UserStateConstantsKeys.Security:
        fabric = "Security";
        break;
      default:
        console.warn(fabricName + 'does not have setAsDefault');
        break;
    }
    return fabric;
  }

  getTreeDataFromState$(fabric, treeName) {
    return this.observableStateService.getState$(fabric, treeName).first().switchMap(obs => obs);
  }

  /**
   *
   * @param key e.g 'Field Data Capture'
   */
  getUserPrefrenceFromState(key) {
    try {
      let stateValue = this.usersStateQuery.getValue();
      return stateValue && stateValue['userPrefrence'] && stateValue['userPrefrence'][key] ? JSON.parse(JSON.stringify(stateValue['userPrefrence'][key])) : false;
    }
    catch (e) {
      this.appLogException(new Error(e));
    }
  }

  /**
   *
   * @param payload object of fabric related userprefrence
   */
  updateUserPrefrenceIntoState(payload) {
    try {
      this.usersStateQuery.updateObjectMultipleValue(UserStateConstantsKeys.userPrefrence, JSON.parse(JSON.stringify(payload)));
    }
    catch (e) {
      this.appLogException(new Error(e));
    }
  }

  idbClearTempExceptDB() {
    try {
      //Kept only for some time to clear all browsers
      let idb: any = window.indexedDB;
      idb.databases().then(res => {
        res.forEach(idbDetails => {
          if (idbDetails && idbDetails.name && idbDetails.name != "Visur") {
            let deleteRequest = idb.deleteDatabase(idbDetails.name);
            deleteRequest.onsuccess = function (evt) {
              console.warn('clear idb');
              console.warn(evt);
            };
            deleteRequest.onerror = function (evt) {
              console.error(evt);
            };
          }
        });
      });
    }
    catch (e) {
      console.error("idbClearTempExceptVisur" + e);
    }
  }

  deleteEntriesFromStore(objectStoreKey: string) {
    try {
      switch (objectStoreKey) {
        case "forms":
          this.formStateQuery.resetStore();
          break;
        default: break;
      }
    }
    catch (e) {
      this.appLogException(new Error(e));
    }
  }

  getNodePropertyByEntityId(tree: any, entityId: string, propertyName: string): Array<string> {
    let arrOfPropertyValue: Array<string> = this.getAllNodeByEntityId(tree, entityId).map((entityIdNode: any) => entityIdNode[propertyName]);
    return arrOfPropertyValue;
  }

  getAllNodeByEntityId(tree: any, entityId: string): Array<any> {
    var duplicateEntitiesList = [];
    if (tree) {
      let nodes = tree.treeModel.nodes;
      nodes.forEach((node: any) => {
        if (node.EntityId == entityId) {
          duplicateEntitiesList.push(node);
        }
        if (node.children && node.children.length) {
          this.nodeChildren(node, entityId, duplicateEntitiesList);
        }
      });
    }
    return duplicateEntitiesList;
  }

  nodeChildren(node, entityId, duplicateEntitiesList) {
    node.children.forEach((children: any) => {
      if (children.EntityId == entityId)
        duplicateEntitiesList.push(children);
      if (children.children && children.children.length)
        this.nodeChildren(children, entityId, duplicateEntitiesList);
    });
  }

  flatToPeopleAllTree(entitiesData) {
    console.log("flatToPeopleAllTree..." + Object.keys(entitiesData).length);
    var peopleTreeSortedData = this.getPeopleTreeData(entitiesData);
    // this.peopleTreeData = peopleTreeSortedData;//need to remove

    var flattenAllTreeData = this.flatToHierarchyState(peopleTreeSortedData);

    return flattenAllTreeData;
  }


  getPeopleTreeData(treeData) {
    let peopletreeData: Array<any> = [];
    var nodeDataArray = this.getNodeData(treeData);
    peopletreeData = JSON.parse(JSON.stringify(nodeDataArray));

    let data = peopletreeData.filter((nodeData: any) => nodeData && nodeData.EntityName != undefined && nodeData.EntityName != null);

    data.sort((firstEntity, secondEntity) => {
      return firstEntity["EntityName"].toLowerCase() > secondEntity["EntityName"].toLowerCase() ? 1 : -1;
    });
    // this.globalpeopledata = data;
    // this.Nodelist = this.peopleTreeData;
    return data;
  }

  flatToHierarchyState(list) {
    let roots = [];
    let all = {};
    if (!list.length) {
      return roots;
    }
    list.forEach(Element => {
      all[Element.EntityId] = Element;
    });
    Object.keys(all).forEach(Element => {
      let item = all[Element];
      if (!item.Parent) {
        item.Parent = "";
        item.ParentType = "";
        roots.push(item);
      }
      else if ((item.Parent in all) || (item.Parent && item.Parent.length && item.Parent[0]["EntityId"] in all)) {
        let parentId = item.Parent && item.Parent.length && item.Parent[0]["EntityId"] ? item.Parent[0]["EntityId"] : item.Parent;
        let p = all[parentId];
        if (p) {
          if (!p.children) {
            p.children = [];
          }
          p.children.push(item);
        }
      }
      else if (item.Parent) {
        item.Parent = "";
        item.ParentType = "";
        roots.push(item);
      }
    });
    return roots;
  }

  //Create or update CompanyType in state
  createCompanyTypesIntoState(entityId, entityType, capabilityId, capabilityName, companyType: Array<string>) {

    let entityCapId = entityId + "_" + capabilityId;
    //create new entity
    let peopleAndCompaniesEntity = this.getEntitiesById(entityId);

    let compayTypesStateModel = new PeopleCompanyTypes().getdefultData();
    compayTypesStateModel.EntityId = entityCapId;
    compayTypesStateModel.EntityName = peopleAndCompaniesEntity.EntityName;
    compayTypesStateModel.EntityType = entityType;
    compayTypesStateModel.CapabilityId = capabilityId;
    compayTypesStateModel.CapabilityName = capabilityName;
    compayTypesStateModel.CompanyType = companyType;
    this.entityCompanyTypesStoreQuery.upsert(entityCapId, (<PeopleCompanyTypes>compayTypesStateModel));

  }

  getNodeData(treeData) {
    let nodeDataArray = [];
    for (let nodeIndex in treeData) {
      let nodeData = {};
      var data = treeData[nodeIndex];
      nodeData["EntityName"] = data["EntityName"];
      nodeData["EntityType"] = data["EntityType"];
      nodeData["EntityId"] = data["EntityId"];
      nodeData['children'] = [];

      if (data["CompanyType"] != undefined)
        nodeData["CompanyType"] = data["CompanyType"];

      if (data["Parent"] != undefined) {
        nodeData["Parent"] = data["Parent"][0]["EntityId"];
        nodeData["ParentType"] = data["Parent"][0]["EntityType"];
      }
      nodeDataArray.push(nodeData);
    }
    return nodeDataArray;
  }


  GetParentChildRelationship(res) {
    try {
      var flat = this.gettreeArray(res);
      flat.forEach(i => {
        i.children = flat.filter(ch => (ch.Parent && ch.Parent.length > 0 && ch.Parent[0]["EntityId"] == i.EntityId));
        flat = flat.filter(el => i.children.indexOf(el) < 0);
      });
      var availableData = flat.filter(obj => obj["EntityId"] == "lkjhgfdsa098765");
      var allData = flat.filter(obj => obj["EntityId"] != "lkjhgfdsa098765");
      if (availableData.length != 0) {
        this.LeftTreePayload = availableData["0"];
        allData.forEach(item => {
          let flag = false;
          if (!this.DoSearch(this.LeftTreePayload, item.EntityId, flag)) {
            this.LeftTreePayload["children"].push(item);
          }
        });
      }
      var resList = this.sortBIHerarchyData(this.LeftTreePayload["children"]);
      return resList;
    } catch (e) {
      console.error('Exception in GetParentChildRelationship() of Commonservice  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  DoSearch(node, value, flag) {
    if (node.EntityId == value) { flag = true; return flag; }
    node.children.forEach(child => { if (this.DoSearch(child, value, flag)) { flag = true; return flag; } });
    return flag;
  }

  sortBIHerarchyData(BIHierarchydataData1) {
    BIHierarchydataData1.sort((firstEntity, secondEntity) => { return firstEntity["EntityName"].toLowerCase() > secondEntity["EntityName"].toLowerCase() ? 1 : -1; });
    BIHierarchydataData1.forEach(element => {
      if (element.children.length != 0) {
        this.sortBIHerarchyData(element.children);
      }
    });
    return BIHierarchydataData1;
  }

  gettreeArray(child) {
    let biLeftTreeData = [];
    for (var i = 0; i < child.length; i++) {
      var info;
      if (child[i]["EntityType"] != (undefined && '')) {
        info = child[i];
        info["children"] = [];
        info["Capability"] = this.capabilityFabricId;
        info["Parent"] = child[i]["Parent"] != null ? child[i]["Parent"] : [{ EntityId: "lkjhgfdsa098765", EntityName: "lkjhgfdsa098765" }];
        if (info["DashBoardURL"]) {
          info["Dashboardurl"] = info["DashBoardURL"];
          delete info["DashBoardURL"];
        }
        biLeftTreeData.push(info);
        // if (child[i]["EntityType"] == EntityTypes.Category)
        //   this.CategoryListBI.push({ "Name": child[i]["EntityName"], "Id": child[i]["EntityId"] });
        // else
        //   this.DashboardList.push({ "Name": child[i]["EntityName"], "Id": child[i]["EntityId"] });
      }

    }
    var info1 = { ["Capability"]: "", ["children"]: [], ["EntityType"]: "", ["EntityName"]: "lkjhgfdsa098765", ["EntityId"]: "lkjhgfdsa098765", ["Parent"]: [{ EntityId: "", EntityName: "" }] };
    biLeftTreeData.push(info1);

    return biLeftTreeData;
  }

  returnTreeJson(list) {
    let checklist2 = list;
    try {
      for (var i = 0; i < list.length; i++) {
        var k = i;
        for (var j = 0; j < checklist2.length; j++) {

          if (list[k].EntityId === checklist2[j].Parent[0]["EntityId"] && this.contain(list[k].children, checklist2[j])) {
            list[k].children.push(checklist2[j]);
          }
          if (list[k].children != null) {
            if (list[k].children.length > 0) {
              if (list[k].EntityId == "lkjhgfdsa098765" && j >= checklist2.length) {
                list[k].children = this.returnTreeJson(list[k].children);
              } else if (list[k].EntityId != "lkjhgfdsa098765") {
                list[k].children = this.returnTreeJson(list[k].children);
              }

            }
          }
        }
      }
    } catch (e) {
      console.error('Exception in returnTreeJson() of CommonService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
    return list;
  }

  // createDesktopLog(logLevel: string, ...message: any[]) {
  //   if (this.isElectron) {
  //     let tempMessage: any = {
  //       logLevel: logLevel,
  //       message: message
  //     }
  //     this.ipc.send(IpcChannels.Logs, tempMessage);
  //   }
  // }

  contain(list1, obj) {
    var flag = true;
    try {
      if (list1.length > 0) {
        for (var j = 0; j < list1.length; j++) {
          if (list1[j].EntityId === obj.EntityId) {
            return !flag;
          }
        }
      }
    } catch (e) {
      console.error('Exception in contain() of CommonService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
    return flag;
  }

  destroy() { }

  public GetClientIP(): Observable<IPInfo> {
    const methodName = 'GetClientIP()';

    try {
      console.log(methodName);

      return this.http
        .get<IPInfo>('https://api.ipify.org?format=json')
        .pipe(retry(3),
          catchError((err: HttpErrorResponse) => {
            const errorMessage: HttpErrorResponse | ErrorEvent = err.error instanceof ErrorEvent ? err.error : err;

            return throwError(errorMessage);
          }));
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  getPeopleTreeIcon(entityType) {
    try {
      let icon;
      let peopleFabricBlobUrl = this.blobUrl.peopleFabric;
      if (peopleFabricBlobUrl == undefined) {
        peopleFabricBlobUrl = "https://visurblob.blob.core.windows.net/icons/";
      }
      switch (entityType) {
        case EntityTypes.People:
          {
            icon = "wwwroot/images/V3 Icon Set part2/V3 Person.svg";
            break;
          }
        // case EntityTypes.PEOPLECATEGORY:
        //   {
        //     icon = "wwwroot/images/V3 Icon Set part2/V3 Person.svg";
        //     break;
        //   }
        case EntityTypes.COMPANY:
          {
            icon = "wwwroot/images/V3 Icon Set part2/V3 Company.svg";
            break;
          }
        // case EntityTypes.GROUP:
        //   {
        //     icon = "wwwroot/images/V3 Icon Set part2/GroupPeople.svg";
        //     break;
        //   }
        case EntityTypes.PERSON:
          {
            icon = "wwwroot/images/V3 Icon Set part2/V3 Person.svg";
            break;
          }
        default:
          {
            icon = "wwwroot/images/V3 Icon Set part2/V3 Person.svg";
            break;
          }
      }
      return icon;
    } catch (e) {
      console.error('Exception in getIcon() of CommonserService at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  public btnFocusActivate(): void {
    try {
      console.log('btnFocusActivate');

      const btnAction: HTMLCollectionOf<HTMLElement> = document.getElementsByClassName('rectangle') as HTMLCollectionOf<HTMLElement>;
      if (btnAction.length !== 0) {
        btnAction[0].focus();
      }
    } catch (error) {
      throw (error);
    }
  }


  gettimezonefromparent(timeZone: string) {
    try {
      let tempProductionDay;
      if (timeZone != null && timeZone != "") {
        let start: any = timeZone.indexOf("GMT") + 4;
        let end: any = timeZone.indexOf(")");

        const sign = timeZone[start - 1];
        timeZone = timeZone.substring(start, end);
        console.log("TimeZone:: ", timeZone);

        let tempOffset;
        switch (sign) {
          case '+':
            tempOffset = Math.abs(this.getDecimalHours(timeZone));
            break;

          case '-':
            tempOffset = -Math.abs(this.getDecimalHours(timeZone));
            break;

          default:
            break;
        }

        let DateObject: any = this.getDateObject(tempOffset);
        tempProductionDay = DateObject.Date;
        let hours = DateObject.Hours;

        if (hours >= 8) {
          tempProductionDay.setDate(tempProductionDay.getDate() - 1);
        }
        else {
          tempProductionDay.setDate(tempProductionDay.getDate() - 2);
        }
      }
      console.log('gettimezonefromparent():: ', tempProductionDay);
      return tempProductionDay;
    }
    catch (e) {
      console.error("Throwing exception in gettimezonefromparent():: Exception :: ", e);
    }
  }

  getDateObject(offset: number) {
    let timeZoneDate = new Date(new Date().getTime() + offset * 3600 * 1000).toISOString();
    let tempDate = new Date();
    tempDate.setDate(parseInt(timeZoneDate.substring(8, 10)));
    tempDate.setMonth(parseInt(timeZoneDate.substring(5, 7)) - 1);
    tempDate.setFullYear(parseInt(timeZoneDate.substring(0, 4)));
    tempDate.setHours(8, 0, 0, 0);

    let dateObject = {
      Date: tempDate,
      Hours: parseInt(timeZoneDate.substring(11, 13))
    };
    console.log('getDateObject():: ', dateObject);
    return dateObject;
  }

  getDecimalHours(hours: string): number {
    let tempDate = new Date("01/01/2007 " + hours);
    let tempHours = tempDate.getHours();
    let tempMinutes = tempDate.getMinutes();
    let decimalMinutes = tempMinutes / 60;
    // console.log('getDecimalHours()::', tempHours + decimalMinutes);
    return tempHours + decimalMinutes;
  }

  getTreePayloadFDC(payload) {
    var json = {
      EntityName: payload.EntityType == EntityTypes.FACILITYCODE ? payload.FacilityCode : payload.EntityName ? payload.EntityName : "",
      children: payload.children ? payload.children : [],
      EntityId: payload.EntityId,
      EntityType: payload.EntityType,
      Parent: payload.Parent ? payload.Parent : [],
      WellType: payload.WellType ? payload.WellType : "",
      Downstream: payload.Downstream ? payload.Downstream : [],
      Upstream: payload.Upstream ? payload.Upstream : [],
    };
    return JSON.parse(JSON.stringify(json));
  }

  SendDataToAngularTreeFDC(treeName: string[], treeOperationType: string, payload: any) {
    try {
      if (this.getFabricNameByUrl(this.router.url) == FABRICS.ENTITYMANAGEMENT) {
        var treeMessage: AngularTreeMessageModel = {
          "fabric": this.lastOpenedFabric.toLocaleUpperCase(),
          "treeName": treeName,
          "treeOperationType": treeOperationType,
          "treePayload": payload,
        };
        this.sendDataToAngularTree.next(treeMessage);
      }
    }
    catch (e) {
      console.error(e);
    }
  }

  userProfileMenuRefreshOnBrowserButtons() {
    if (this.isUserMenuHeader) {
      //this.isUserMenuHeader=false;
      this.userProfileMenuRefreshOnBrowserBackFowardButton.next('BrowserBackForwardButton');
    }
  }

  //Fabric by tree worker initialization ********************************






  addIntoObservableStateService(data: ObservableStateMessage) {
    this.observableStateService.add(data);
  }


  getMyFormByComponentName(componentName: string): EventEmitter<any> {
    let eventModel: SendToComponentMyFormEvent = {
      ComponentName: componentName,
      MyForm: '',
      Event: new EventEmitter()
    };
    setTimeout(() => {
      this.getComponentMyFormEvent$.next(eventModel);
    });
    return eventModel.Event;
  }




  getCapabilityFormNameBasedOnTab(tab) {
    let FormCapabality = "";
    switch (tab) {
      case Tabs.PEOPLE:
      case TreeTypeNames.PeopleAndCompanies:
        FormCapabality = FABRICS.PEOPLEANDCOMPANIES;
        break;
      case Tabs.REPORTING:
      case TreeTypeNames.REPORTING:
        FormCapabality = FabricsNames.REPORTING;
        break;
      case Tabs.PROJECTS:
      case TreeTypeNames.PROJECTS:
        FormCapabality = FabricsNames.PROJECTS;
        break;
      case Tabs.FABRICATION:
      case TreeTypeNames.FABRICATION:
        FormCapabality = FabricsNames.FABRICATION;
        break;
      case Tabs.MAPS:
      case TreeTypeNames.Maps:
        FormCapabality = FabricsNames.MAPS;
        break;
      case Tabs.POSSIBLEPIPELINEINTEGRITY:
      case TreeTypeNames.POSSIBLEPIPELINEINTEGRITY:
        FormCapabality = FabricsNames.POSSIBLEPIPELINEINTEGRITY;
        break;
      case Tabs.SECURITY:
      case TreeTypeNames.SECURITY:
        FormCapabality = FabricsNames.SECURITY;
        break;
      case Tabs.ENTITYMANAGEMENT:
      case TreeTypeNames.ENTITYMANAGEMENT:
        FormCapabality = FabricsNames.ENTITYMANAGEMENT;
        break;
      case Tabs.BI:
      case TreeTypeNames.BusinessIntelligence:
        FormCapabality = FABRICS.BUSINESSINTELLIGENCE;
        break;
      case Tabs.PERSON:
      case EntityTypes.PERSON:
        FormCapabality = EntityTypes.PERSON;
        break;
      case Tabs.CONSTRUCTION:
      case TreeTypeNames.Construction:
      case TreeTypeNames.TaggedItems:
      case TreeTypeNames.Materials:
      case TreeTypeNames.Documents:
      case TreeTypeNames.PhysicalInstances:
      case TreeTypeNames.Schedule:
      case TreeTypeNames.Turnover:
        FormCapabality = EntityTypes.Construction;
        break;
      default:
        break;
    }
    return FormCapabality;
  }

  getCapabilityNameBasedOnTab(tab) {
    let FormCapabality = "";
    switch (tab) {
      case Tabs.PEOPLE:
        FormCapabality = FABRICS.PEOPLEANDCOMPANIES;
        break;
      case Tabs.REPORTING:
        FormCapabality = CAPABILITY_NAME.REPORTING;
        break;
      case Tabs.PROJECTS:
        FormCapabality = CAPABILITY_NAME.PROJECTS;
        break;
      case Tabs.FABRICATION:
        FormCapabality = CAPABILITY_NAME.FABRICATION;
        break;
      case Tabs.MAPS:
        FormCapabality = CAPABILITY_NAME.MAPS;
        break;
      case Tabs.POSSIBLEPIPELINEINTEGRITY:
        FormCapabality = CAPABILITY_NAME.POSSIBLEPIPELINEINTEGRITY;
        break;
      case Tabs.SECURITY:
        FormCapabality = CAPABILITY_NAME.SECURITY;
        break;
      case Tabs.ENTITYMANAGEMENT:
        FormCapabality = CAPABILITY_NAME.ENTITYMANAGEMENT;
        break;
      case Tabs.BI:
        FormCapabality = FABRICS.BUSINESSINTELLIGENCE;
        break;
      case Tabs.CONSTRUCTION:
        FormCapabality = CAPABILITY_NAME.CONSTRUCTION;
        break;
    }
    return FormCapabality;
  }






  getServiceInstanceByName(getServiceInstanceMessage: GetServiceInstanceMessage) {
    let eventModel: GetServiceInstanceEventModel = {
      ServiceName: getServiceInstanceMessage.ServiceName,
      Instance: '',
      Event: new EventEmitter()
    };
    setTimeout(() => {
      this.getServiceInstanceEvent$.next(eventModel);
    });
    return eventModel.Event;
  }


  /**
   * Get dependent componentInstance data (incase of Desktop)
   * @param GetDesktopDependentComponentInstanceModel
   */
  getDependentComponentInstanceData(ID, Type, CurrentInstance, CurrentRouting?, RoutingType?, Valid?, PayLoad?) {
    let eventModel: GetDesktopDependentComponentInstanceModel = {
      ID: ID,
      Type: Type,
      CurrentInstance: CurrentInstance,
      CurrentRouting: CurrentRouting,
      RoutingType: RoutingType,
      Valid: Valid,
      PayLoad: PayLoad,
      Event: new EventEmitter()
    };
    setTimeout(() => {
      //this.getDesktopComponentDependentsInstanceEvent$.next(eventModel)
    });
    return eventModel.Event;
  }

  //form state query
  getFormStateRecord(key) {
    return this.formStateQuery.getValue()["entities"][key];//where key is entityid
  }

  upsertInnerKeyDataInFormState(entityId, modelConstructorName, originalKey, innerKey, value) {
    this.formStateQuery.upsertInnerKeyDataInState(entityId, modelConstructorName, originalKey, innerKey, value);
  }

  updateStateValueBasedOnType(entityId, typeOfForm, propertyName, propertyVal) {
    this.formStateQuery.updateStateValueBasedOnType(entityId, typeOfForm, propertyName, propertyVal);
  }

  upsertModelFormState(entityId, model) {
    console.log("upsertModelFormState");
    console.log(entityId);
    console.log(model);
    console.log(new EntityForm(entityId, model).createInstance());
    this.formStateQuery.upsertModel(entityId, new EntityForm(entityId, model).createInstance());
  }
  upsertKeyFormState(entityId, newState) {
    console.log("upsertKeyFormState");
    console.log(entityId);
    console.log(newState);
    this.formStateQuery.upsertKey(entityId, newState);
  }

  upsertStateFormState(entityId, model) {
    console.log("upsertStateFormState");
    console.log(entityId);
    console.log(model);
    console.log(new EntityForm(entityId, model).createInstance());

    this.formStateQuery.upsertState(entityId, new EntityForm(entityId, model).createInstance());
  }
  getOptionForCopyToList() {
    let options = [];
    return options;
  }
  activateEntityWhenSearchActive(params) {
    this.searchActive = false;
    var configdata: ITreeConfig = { "treeExpandAll": false };
    this.sendDataToAngularTree1(params.tab, TreeOperations.treeConfig, configdata);
    setTimeout(() => {
      this.sendDataToAngularTree1(params.tab, TreeOperations.ActiveSelectedNode, params.EntityID);
    });
  }

  public isEntityExistInOtherFabric(entityID: string, fabricName: string): boolean {
    const methodName: string = 'isEntityExistInOtherFabric()';

    try {
      console.log(methodName);

      let tempFabricName: string = this.getFabricNameByTab(fabricName);
      tempFabricName ? tempFabricName : tempFabricName = fabricName;

      const capabilityName: string[] = this.getCapabilityNamesBy(this.getEntitiesById(entityID).Capabilities);

      return capabilityName.includes(tempFabricName);
    } catch (error) {
      console.error(error);
      throw error;
    }
  }
  removeEntityExistInLists(locationId, fabric) {
    try {
      let filterFun = d => d && d.EntityType == 'List' && d.SG && (<any[]>d.SG).length > 0 && (<any[]>d.SG).indexOf(fabric) >= 0 && d.children && d.children.length > 0 && d.children.filter(childObject => childObject.EntityId == locationId).length > 0;
      let listData = this.getListDataFromState(filterFun);
      if (listData && listData.length) {
        let deletedList = JSON.parse(JSON.stringify(listData));
        listData.forEach((list: any, idx) => {
          let index = list.children.findIndex(obj => obj.EntityId == locationId);
          deletedList[idx].children.splice(index, 1);
          this.entityCommonStoreQuery.upsert(list.EntityId, deletedList[idx]); // Add / Update to entity state
          //  this.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.deleteNodeById, locationId);
        });
      }

    } catch (e) {
      console.error(e);
    }
  }
  getListDataFromState(filterFunc) {
    return this.entityCommonStoreQuery.getAll({ filterBy: (entity: any) => entity }).filter(filterFunc);
  }


  public otherSessionUpdateEntity(payloadString) {

    //add into fdc state if left tree empty
    try {

      let key = "OtherSessionEntities";
      let existingdata = this.fdcStateQuery.getStateKey(key);
      let entities = [];
      if (existingdata) {
        entities = [...existingdata, payloadString];
        //existingdata.push(entity);
      } else {
        entities.push(payloadString);
      }
      if (entities.length > 0)
        this.fdcStateQuery.add(key, entities);
    } catch (e) {
      console.error(e);
    }

  }

  public getOtherSessionEntity(): any[] {
    let key = "OtherSessionEntities";
    let existingdata = this.fdcStateQuery.getStateKey(key);
    if (existingdata) {
      return [...existingdata];
    } else {
      return [];
    }


  }

  public UpadateCapabilityInState(statePayload) {
    statePayload['SG'] = this.updateEntitySecurityGroup(statePayload);
    statePayload["Capabilities"] = this.getCapabilityIdsBy(statePayload['SG']);
    //this.entityCommonStoreQuery.upsert(statePayload.EntityId, statePayload);
    if (statePayload.hasOwnProperty("SG"))
      this.entityCommonStoreQuery.UpdateProperties('SG', [statePayload]);
    if (statePayload.hasOwnProperty("Capabilities"))
      this.entityCommonStoreQuery.UpdateProperties('Capabilities', [statePayload]);
    if (statePayload.hasOwnProperty("Capability"))
      this.entityCommonStoreQuery.UpdateProperties('Capability', [statePayload]);

  }

  public otherSessionUpdateEntityStateReinitializeToZero() {
    let key = "OtherSessionEntities";
    let entities = [];
    this.fdcStateQuery.add(key, entities);
  }
  public sharedListFormRouting(entityId) {
    // this.sharedListValidateForm = false;
    switch (this.lastOpenedFabric) {
      case "fdcfdc":
        break;
      default: {
        this.router.navigate([this.lastOpenedFabric], { queryParams: { 'tab': this.treeIndexLabel, 'leftnav': this.isLeftTreeEnable } });
        setTimeout(() => {
          this.router.navigate([this.lastOpenedFabric, 'capabilitylistsearch'], { queryParams: { EntityId: entityId, 'tab': this.treeIndexLabel, 'leftnav': this.isLeftTreeEnable } });
        }, 10);
      } break;
    }
  }

  sortNodesByOrder(listOfChildren: any[], SortingOrder: any[]): any[] {
    try {
      let NewSortedListOfChildren = [];
      SortingOrder.map((item: string) => {
        let SortArray = [];
        listOfChildren.forEach((element: any) => {
          if (element.EntityType == item || element.Type == item)
            SortArray.push(element);
        });

        SortArray.sort((firstEntity, secondEntity) => {
          if (firstEntity['EntityName'] && secondEntity['EntityName'])
            return firstEntity['EntityName'].toLowerCase() > secondEntity['EntityName'].toLowerCase() ? 1 : -1;
        });
        NewSortedListOfChildren = [...NewSortedListOfChildren, ...SortArray];
      });
      return NewSortedListOfChildren;
    }
    catch (e) {
      console.error('Exception in sortNodesByOrder()  at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  RemoveHaulLocatonFromSecurityGroupEntites(fabric, treeName): Observable<any[]> {
    try {
      let ContainChild;
      let treeevent = this.getAngularTreeEvent(fabric, [treeName], TreeOperations.GetTreeInstance).map((responseModel: TreeDataEventResponseModel) => {
        let treeNames: Array<string> = responseModel.treeName;
        let tree = responseModel.treePayload;
        ContainChild = true;
        if (treeNames.indexOf(treeName) > -1 && tree && tree.treeModel && tree.treeModel.nodes) {
          var treeData = JSON.parse(JSON.stringify(tree.treeModel.nodes));
          treeData.forEach(location => {
            if (location.EntityType == EntityTypes.HaulLocation && location.children && location.children.length == 0) {
              ContainChild = false;
            }
          });
        }
        return ContainChild;
      });
      return ContainChild ? of(ContainChild) : treeevent;
    }
    catch (ex) {
      console.error('Exception in RemoveHaulLocatonFromSecurityGroupEntites()  at time ' + new Date().toString() + '. Exception is : ' + ex);
    }
  }

  onTabletMode() {
    this.tabletMode = true;
    this.tableButtonMode = true;
    // this.AppendToBodyObservable$.next({ "Mode": "TabletMode", "EventFrom": "TabletMode", "Value": this.tabletMode });
  }

  offTabletMode() {
    this.tabletMode = false;
    this.tableButtonMode = false;
  }

  enableKeyPad() {
    // this.enabledisableglobalkeypad = true;
  }

  disableKeyPad() {
    // this.enabledisableglobalkeypad = false;
  }

  enableTableModeButton() {
    this.isdisableTableModeButton = false;
  }

  disableTableModeButton() {
    this.isdisableTableModeButton = true;
  }



  doubleclick(event) {
    try {
      if (this.tabletMode && event && event.target && event.target.select && !event.target.readOnly) {
        if (window.getSelection) {
          window.getSelection().removeAllRanges();
        }
        event.target.select();
        this.inputselected = true;
      }
    }
    catch (ex) {

    }
  }

  counttouchtime() {
    try {
      if (this.ttouchtime == 0) {
        this.ttouchtime = new Date().getTime();
      } else {
        // compare first click to this click and see if they occurred within double click threshold
        if (((new Date().getTime()) - this.ttouchtime) < 800) {
          this.doubleclick(event);
          this.ttouchtime = 0;
        } else {
          // not a double click so set as a new first click
          this.ttouchtime = new Date().getTime();
          this.inputselected = false;
        }
      }
    }
    catch (ex) {

    }
  }

  getProdDateFromUrl(url) {
    let date = null;
    if (url) {
      let paramsArray = url.split(')');
      paramsArray.forEach(strValue => {
        if (strValue.includes('Date'))
          paramsArray = strValue;
      });
      paramsArray = paramsArray.split(';');
      if (paramsArray && paramsArray.length) {
        if (paramsArray.length == 1) {
          let paramUrlArray = url.split('?');
          if (paramUrlArray && paramUrlArray.length) {
            let queryParams = paramUrlArray[1];
            let andParams = queryParams.split('&');
            andParams.forEach(strValue => {
              if (strValue && strValue.includes('Date')) {
                date = strValue.replace('Date=', '');
              }
            });
          }
        }
        else {
          paramsArray.forEach(strValue => {
            if (strValue.includes('Date'))
              date = strValue.replace('Date=', '');
          });
        }
      }
    }
    return date;
  }

  getCapabilityidbasedontabforSecurity() {
    let capabilityid = null;
    let tab = this.route.snapshot.queryParamMap.get('tab');

    let fabricName = this.getCapabilityNamebasedontabforSecurity();
    capabilityid = this.getCapabilityId(fabricName);

    return capabilityid;
  }

  getCapabilityNamebasedontabforSecurity() {
    let fabricName = null;
    let tab = this.route.snapshot.queryParamMap.get('tab');
    if (tab) {
      tab = tab.replace(" Role", "");
      fabricName = this.getFabricNameByTab(tab);

    }
    return fabricName;
  }
  getQueryParamValue(key: string) {
    return this.route.snapshot.queryParamMap.get(key);
  }


  getRightClickEventModelObject(head: any, capability?: string, data?: any) {

    return new RightSideClickTreeEventModel(head, capability, data);

  }

  //if default tab not exist for new user then remove tab and close left tree
  ResetheaderData(headerData) {
    try {
      let tablist = this.getAdminFabriclist();
      switch (headerData.Fabric) {
        case FABRICS.ENTITYMANAGEMENT:
          if (headerData.queryParameter && headerData.queryParameter.tab && tablist.findIndex(item => item.includes(headerData.queryParameter.tab)) == -1) {
            delete headerData.queryParameter.tab;
            if (headerData.queryParameter.leftnav) {
              headerData.queryParameter.leftnav = false;
            }
          }
          break;
        case FABRICS.SECURITY:
          if (headerData.queryParameter && headerData.queryParameter.tab && tablist.findIndex(item => item.includes(headerData.queryParameter.tab.replace(" Role", ""))) == -1) {
            delete headerData.queryParameter.tab;
            if (headerData.queryParameter.leftnav) {
              headerData.queryParameter.leftnav = false;
            }
          }
          break;
      }
      return headerData;
    }
    catch (ex) {

    }
  }

  getRoleName(fabric) {
    try {
      if (fabric) {
        return this.authCommonStoreQuery.getValue()
          .UserRoles
          .filter((role: any) => role && role.Fabric == fabric)
          .map(role => role.RoleName)[0];
      }
    }
    catch (e) {
      this.appLogException(new Error("getRoleName " + e));
    }
  }

  getSecurityGroupIds(fabric) {
    try {
      if (fabric) {
        return this.authCommonStoreQuery.getValue()
          .UserSecurityGroups
          .filter((securitygroup: any) => securitygroup && securitygroup.Fabric == fabric)
          .map(securitygroup => securitygroup.SecurityGroupId);
      }
    }
    catch (e) {
      this.appLogException(new Error("getSecurityGroupIds " + e));
    }
  }

  getuserDetails() {
    return this.authCommonStoreQuery.getValue();
  }

  getAdminFabriclist() {
    try {
      let tablist = [];
      this.appConfig.AllHeaderRoutingList.forEach(item => {
        if (item.admin) {
          switch (item.Fabric) {
            case FABRICS.PEOPLEANDCOMPANIES:
              tablist.push(TreeTypeNames.PeopleAndCompanies);
              break;
            case FABRICS.BUSINESSINTELLIGENCE:
            case FABRICS.BUSINESSINTELLIGENCE:
              tablist.push(TreeTypeNames.BusinessIntelligence);
              break;
            case FABRICS.SECURITY:
              tablist.push(TreeTypeNames.SECURITY);
              break;
            case FABRICS.ENTITYMANAGEMENT:
              tablist.push(TreeTypeNames.ENTITYMANAGEMENT);
              break;
            case FABRICS.PROJECTS:
              tablist.push(TreeTypeNames.PROJECTS);
              break;
            case FABRICS.FABRICATION:
              tablist.push(TreeTypeNames.FABRICATION);
              break;
            case FABRICS.POSSIBLEPIPELINEINTEGRITY:
              tablist.push(TreeTypeNames.POSSIBLEPIPELINEINTEGRITY);
              break;
            case FABRICS.REPORTING:
              tablist.push(TreeTypeNames.REPORTING);
              break;
            case FABRICS.CONSTRUCTION:
              tablist.push(TreeTypeNames.CONSTRUCTION);
              break;
          }
        }

      });
      return tablist;
    } catch (ex) {

    }
  }
  postDataToBackend(messageModel: IModelMessage, loadingmessage?: string): Observable<any> {
    this.loadingBarAndSnackbarStatus(AppConsts.loaderStart, loadingmessage);
    return this.dataService.postDataToBackend(messageModel);
  }

  getMessageWrapper(config: { Payload?: any, DataType?: string, EntityType?: string, EntityID?: string, Routing?: string, MessageKind?: string, Type?: string }): IModelMessage {
    let msg: IModelMessage = <any>{};
    let keys = Object.keys(config);
    for (let key of keys) {
      msg[key] = key == 'Payload' ? (typeof (config[key]) == 'string' ? config[key] : JSON.stringify(config[key])) : config[key];
    }
    msg.MessageId = this.GetNewUUID();
    msg.ClientId = this.clientGuid;
    msg.TenantId = this.getauthElement().TenantId;
    msg.TenantName = this.getauthElement().TenantName;
    msg.UserId = this.getauthElement().UserId;
    msg.Fabric = this.getFabricNameByUrl(this.router.url);
    msg.CapabilityId = this.getCapabilityId(msg.Fabric);
    msg.Application = msg.Fabric;
    msg.IsAdmin = this.isCapbilityAdmin();
    msg.SourceSystem = SourceSystem.System;
    msg.Routing = Routing.OriginSession;
    msg.ApplicationName = AppConfig.AppMainSettings.env.ApplicationName;
    return msg;
  }
  getFormSchemaDataFromBackend(messageModel: IModelMessage, loadingmessage?: string): Observable<any> {
    this.loadingBarAndSnackbarStatus(AppConsts.loaderStart, loadingmessage);
    return this.dataService.getDataFromBackend(messageModel);
    //return this.dataService.getPeopleSchema(messageModel.EntityType);
  }
  // get tenantId() {
  //   return this.userAuthService.userProperties.tenantId;
  // }
  // get getUserId() {
  //   return this.userAuthService.userProperties.tenantId;
  // }
  getRolesOrSecurityGroupsMode(): string {
    return this.route.snapshot.queryParamMap.get('enabledmode');
  }
  createEntity(messageModel: any) {

    return this.dataService.createEntity(messageModel);
  }


  leftTreeForConstruction(EntityTypeORDataTypeORFabric, EntityArray, capabality, dateTime?, entityType?, entityId?, treeName?) {
    if (this.capabilityFabricId == null || this.capabilityFabricId == undefined) {
      this.capabilityFabricId = this.FabricCapabilityIdAtLoginTime;
    }
    let Payload;
    // if (entityId == EntityTypes.Construction + 'Entities') {
    Payload = {
      depth: 15,
      isCapitalResult: true,
      entityTypes: [EntityTypes.Project],
      rightMenus: [{ EntityName: EntityTypes.Construction, EntityId: this.getCapabilityId(FABRICS.CONSTRUCTION) }]
    };
    //}
    // else {
    //   Payload = {};
    // }

    if (dateTime && Object.keys(Payload).length == 0) {
      Payload = {};
      Payload["ModifiedDateTime"] = dateTime;
    }
    else if (dateTime) {
      Payload["ModifiedDateTime"] = dateTime;
    }

    let message = this.getMessage();
    message.Payload = JSON.stringify(Payload);
    message.DataType = EntityTypeORDataTypeORFabric;
    message.EntityID = entityId ? entityId : EntityTypeORDataTypeORFabric;
    message.EntityType = EntityTypeORDataTypeORFabric == Datatypes.CONSTRUCTION ? entityType : EntityTypeORDataTypeORFabric;
    message.MessageKind = MessageKind.READ;
    message.Routing = Routing.OriginSession;
    message.CapabilityId = this.getCapabilityId(capabality);
    message.Type = "Details";
    message.Fabric = EntityTypeORDataTypeORFabric;

    this
      .httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .map(res => {
        var messageData = JSON.parse(res);
        let data = JSON.parse(messageData["Payload"]);
        return data;
      })
      .subscribe(data => {
        this.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, '');
        if (EntityTypeORDataTypeORFabric == Datatypes.CONSTRUCTION) {
          // if (entityId == EntityTypes.Construction + 'Entities') {
          this.onEntitiesTreeDataReceive(data);
          //// }
          // else {
          // this.filterForPhysicalItems(entityId, data, treeName);
          // }
        }
      });
  }

  filterForPhysicalItems(entityId, newEntities, treeName) {
    this.createTaggedItemsLeftTree(entityId, newEntities, treeName);
  }

  createTaggedItemsLeftTree(entitiesName, newEntities, treeName) {
    switch (entitiesName) {
      case EntityTypes.TaggedItems + 'Entities':
        newEntities = [...newEntities.areaData, ...newEntities.buildingData, ...newEntities.concreteData, ...newEntities.equipmentData, ...newEntities.facilityData, ...newEntities.spoolDataConcrete, ...newEntities.spoolDataEquipment]
        break;
      case EntityTypes.Materials + 'Entities':
        newEntities = [...newEntities.materialData];
        newEntities.forEach(item => { item[AppConsts.TYPEOF] = TypeOfEntity.Material });
        break;
      case EntityTypes.Documents + 'Entities':
        newEntities = [...newEntities.documentData];
        newEntities.forEach(item => { item[AppConsts.TYPEOF] = TypeOfEntity.Document });
        break;
      default:
        newEntities = newEntities ? newEntities : [];
        break;
    }
    if (newEntities.length > 0) {
      this.filterForConstruction(newEntities);
      this.AddorUpdateEntityState(newEntities[0].EntityId, newEntities[0], FABRICS.CONSTRUCTION);
    }
  }

  onEntitiesTreeDataReceive(entitiesData: Array<any>) {
    let nodes: any[] = [];
    if(entitiesData)
    entitiesData.forEach(data => {
      let taskData = JSON.parse(JSON.stringify(data));
      if (taskData['tasks'].length > 0) {
        nodes = taskData['tasks'] && taskData['tasks'].length ? taskData['tasks'] : []

      }
    });

    if (nodes.length > 0) {
      let flattenNodes: any[] = [];
      this.createFlattenEntitiesList(nodes, flattenNodes);
      this.filterForConstruction(flattenNodes);
    }
  }

  createFlattenEntitiesList(nodes: any[], flattenNodes: any[]) {
    const tempNodes: any[] = JSON.parse(JSON.stringify(nodes));
    for (const node of tempNodes) {
      if (node.hasOwnProperty('children')) {
        const tempNode: any = JSON.parse(JSON.stringify(node));

        const children: any[] = tempNode.children;
        delete tempNode.children;
        flattenNodes.push(tempNode);
        this.createFlattenEntitiesList(children, flattenNodes);
      }
      else {
        flattenNodes.push(node);
      }
    }
  }

  filterForConstruction(data) {
    const capabality: string = FABRICS.CONSTRUCTION;
    var entities = JSON.parse(JSON.stringify(data));

    let existingEntities = [];
    let newEntities = [];
    for (var index in entities) {
      if (entities[index]) {
        let entityId = entities[index].EntityId;
        let entityData = this.entityCommonStoreQuery.getEntity(entityId);
        if (entityData) {
          if (entityData["SG"] != undefined) {
            if (!((<Array<any>>entityData["SG"]).indexOf(capabality) >= 0)) {
              existingEntities.push(entityId);
            }
            continue;
          }
        }
        else {
          entities[index]["SG"] = [capabality];
          newEntities.push(entities[index]);
        }
      }
    }

    if (existingEntities.length > 0)
      this.entityCommonStoreQuery.UpdateArrayValue(existingEntities, "SG", capabality);

    if (newEntities.length > 0)
      this.entityCommonStoreQuery.AddAll(newEntities);

    this.updateViewedFabric(capabality);
  }

  AddorUpdateEntityState(entityId, entityData, fabric: string) {
    try {
      entityData['SG'] = [fabric];
      entityData["ModifiedDateTime"] = new Date().toISOString();
      this.entityCommonStoreQuery.upsert(entityId, entityData);
    }
    catch (e) {
      console.error("AddorUpdateEntitiyState" + e);
    }
  }

}
