import { DataBaseErrorComponentComponent } from './../common/DbError/data-base-error-component/data-base-error-component.component';
import { NgModule } from '@angular/core';
import { CapabilityTreeComponent } from '../template/LayoutComponents/leftsidebar/CapabilityTree/capability.tree.component';
import { CommonEntityFilter } from '../template/LayoutComponents/leftsidebar/CommonEntityFilter/common-entity-filter.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AddDataSourceComponent } from '../fabrics/bifabric/components/datasourceform/datasourceform.component';

import { SharedModule } from './shared.module';

import { AdminModule } from '../capabilities/Admin/admin.module';
import { AngularMaskModule } from '../common/mask/mask.module';

// import {CustomAlphabeticSort} from 'visur-angular-common'
// import { CustomSortArrayPipe } from 'visur-angular-common';

import { FdcHeaderPopupTsComponent } from '../template/LayoutComponents/headers/fdc-header-popup/fdc-header-popup.component';
// import { EntityTypeTransForm } from '../fabrics/FDC/Pipes/EntityTypeTransform';

// import { DecimaNumberDirective } from './../fabrics/FDC/directives/decimal.directive';
// import { MaskValidationDirective } from './../fabrics/FDC/directives/maskValidation.directive';
// import { NumberDirective } from './../fabrics/FDC/directives/number.directive';


/**Operationalform */

/** People */

/**Pigging */
/**global */
import { DialogAlertComponent } from '../template/LayoutComponents/DialogAlert/dialog-alert.component';

// import { FdcHeaderDataValue } from '../fabrics/FDC/components/FDCAdminForm/fdc.adminforms.component';
import { FormSettingComponent } from '../common/UserActivities/userSettings/user-setting.component';
import { UserGridComponentNew } from '../common/UserActivities/userGridNew/user.grid.component';
import {UserJsonSchemaFormGeneratorComponent} from '../common/UserActivities/user-json-schema-form/user-json-schema-form-generator.component'
import { MaterialDesignFrameworkModule } from '@visur/formgenerator-material';
import { CapabilityLandingPageComponent } from '../common/CapabilityLandingPage/capability-landing-page/capability-landing-page.component';

// import { CreateNewEntityComponent } from '../template/GlobalAddNewComponents/CreateNewEntity/add-new-component';

// import { CreateNewListComponent } from '../template/GlobalAddNewComponents/AddNewList/create-newlist-dialog/create-newlist.component'
/** Asset Management fabric*/
// import { OtherTypesComponent } from '../common/form-types/other-type/other-type.component';
// import { ObjectTypesComponent } from '../common/form-types/object-type/object-type.component';
// import { GridTypesComponent } from '../common/form-types/grid-type/grid-type.component';
// import { PopoverTableTypeComponent } from '../common/form-types/popover-table-type/popover-table-type.component';
// import { PopupFlexTypesComponent } from '../common/form-types/popup-flex-type/popup-flex-type.component';

// import { DateFormat } from '../common/form-types/datePicker/dateFormat.component';

// import { IndicatorTimer } from '../common/form-types/indicatortimer/indicatortimer.component';
import { FormHeaderComponent } from '../common/form-header/form-header.component';
// import { HavePermissionDirective } from '../common/form-types/permissions-directive/have-permission.directive';
import { PopUpAlertComponent } from '../common/PopUpAlert/popup-alert.component';
// import { CommonLoaderComponent } from '../common/common-loader/common-loader.component';
// import { MenuActionIconsComponent } from '../common/form-types/menu-action-type/menu-action-type.component';
// import { CustomEllipsisExtraCharactor } from 'visur-angular-common';
import { RenameComponent } from '../common/rename-dialog-form/rename-form.component';
// import { TableTypesComponent } from '../common/form-types/table-type/table-type.component';
// import { FacilityTableTypeComponent } from '../common/form-types/facility-table-type/facility-table-type.component';
// import { CommonInputPopupComponent } from "../common/commonInputPopup/common-InputPopup.component"
import { DynamicInputWidth } from '../globals/directives/inputboxincrease.directive';
import { AngularHierarchyTreeComponent } from '../globals/components/angular-hierarchy-tree/angular-hierarchy-tree.component';
// import { CustomTitleCasePipe } from 'visur-angular-common'
import { DisableProperty } from '../globals/directives/disable-property.directive';
// import { ExpandChartComponent } from '../fabrics/FDC/components/AnalyticsDashboardFormTemplate/chart-dialog/expand-chart.component'
import { MdePopoverModule } from '@material-extended/mde';
// import { VirtualScrollComponent } from '../common/virtualScroll/virtual-scroll.component';
import {EntityManagementFormgenerator } from '../../components/fabrics/EntityManagement/EntityManagementFormTemplate/EMCommonFormgenerator/entitymanagement.formgenerator.component';
import {EntityManagementCommonHeader } from '../../components/fabrics/EntityManagement/EntityManagementFormTemplate/EMCommonHeader/entitymanagement.commonheader.component';
import { EntityManagementTableTypeComponenet } from '../../components/fabrics/EntityManagement/EntityManagementFormTemplate/EMTableType/em-tabletype.component';
import {NgSelectEditFocusedDirective} from '../globals/directives/ng-select.autofocus'



import {ScrollingModule} from '@angular/cdk/scrolling';

import { NewEntityBIComponent } from '../fabrics/bifabric/components/newentitybidialog/newentitybi.component';
import { BIFormGeneratorComponent } from '../fabrics/bifabric/components/bi-form-generator/bi-form-generator.component';
// import { EntityTypeTransForm } from '../common/services/Pipes/EntityTypeTransform';
// import { DecimaNumberDirective } from '../common/services/directives/decimal.directive';
// import { MaskValidationDirective } from '../common/services/directives/maskValidation.directive';
// import { NumberDirective } from '../common/services/directives/number.directive';
import {AddNewEntityDetailsHeaderComponent} from '../template/AddNewComponents/AddNewEntityDetailsHeader/add-new-entity-details-header.component';
import {AddNewEntityDetailsComponent} from '../template/AddNewComponents/AddNewEntityDetials/add-new-entity-details-component';

import {PeopleJsonSchemaFormGeneratorComponent} from '../fabrics/PeopleFabric/components/people-json-schema-form-generator/people-json-schema-form-generator.component'
import { ConstructionFormGeneratorComponent } from '../fabrics/construction/components/construction-form-template/construction-form-generator/construction-form-generator.component';
import { NewEntityMapComponent } from '../fabrics/Map/components/newentitymapdialog/newentitymap.component';

import {FilterDialogComponent}from'../common/filter-dialog/filter-dialog.component';

const AddNewComponents =[AddNewEntityDetailsHeaderComponent,AddNewEntityDetailsComponent];

const SHARED_COMPONENTS = [
  // CustomTitleCasePipe,
  NgSelectEditFocusedDirective
]


// const CUSTOM_PIPES = [CustomAlphabeticSort, CustomSortArrayPipe];
const PEOPLE_COMPONENTS = [PeopleJsonSchemaFormGeneratorComponent]
const CONSTRUCTION_COMPONENTS = [ConstructionFormGeneratorComponent]
const BI_COMPONENTS = [BIFormGeneratorComponent]
const EntityManagement_Components = [EntityManagementFormgenerator, EntityManagementCommonHeader, EntityManagementTableTypeComponenet];
@NgModule({
  entryComponents: [ RenameComponent, AddDataSourceComponent,PopUpAlertComponent, DialogAlertComponent, FormSettingComponent,UserGridComponentNew, FdcHeaderPopupTsComponent, NewEntityBIComponent],
  providers: [],
  imports: [CommonModule, ScrollingModule, AdminModule, RouterModule, SharedModule, MdePopoverModule, AngularMaskModule,MaterialDesignFrameworkModule],
  declarations: [
    AddDataSourceComponent,
    FormSettingComponent,
    PopUpAlertComponent,
    // CommonInputPopupComponent,
    DialogAlertComponent,
    RenameComponent,
    // VirtualScrollComponent,
    UserGridComponentNew,
    FdcHeaderPopupTsComponent,
    // FdcHeaderDataValue,
    DataBaseErrorComponentComponent,
    FormSettingComponent,
    // CUSTOM_PIPES,
    AddNewComponents,
    PEOPLE_COMPONENTS,
    UserJsonSchemaFormGeneratorComponent,
    CONSTRUCTION_COMPONENTS,
    BI_COMPONENTS,
    // EntityFilterComponent,
    CommonEntityFilter,
    SHARED_COMPONENTS,
    // OtherTypesComponent,
    // ObjectTypesComponent,
    // DateFormat,
    // IndicatorTimer,
    // GridTypesComponent, PopupFlexTypesComponent,
    // PopoverTableTypeComponent,
    // TableTypesComponent,
    // FacilityTableTypeComponent,
    FormHeaderComponent,
    // HavePermissionDirective,
    // MenuActionIconsComponent,
    // CustomEllipsisExtraCharactor,
    DynamicInputWidth,
    DisableProperty,
    NewEntityBIComponent,
    NewEntityMapComponent,
    EntityManagement_Components,
    FilterDialogComponent,
    CapabilityLandingPageComponent
  ],
  exports: [
    ScrollingModule,
    AddDataSourceComponent,
    AdminModule,
    // CUSTOM_PIPES,
    AddNewComponents,
    PEOPLE_COMPONENTS,
    UserJsonSchemaFormGeneratorComponent,
    CONSTRUCTION_COMPONENTS,
    BI_COMPONENTS,
    FormSettingComponent,
    UserGridComponentNew,
    PopUpAlertComponent,
    // CommonInputPopupComponent,
    // VirtualScrollComponent,
    SHARED_COMPONENTS,
    // FdcHeaderDataValue,
    DataBaseErrorComponentComponent,
    CapabilityTreeComponent,
    AngularHierarchyTreeComponent,
    // EntityFilterComponent,
    CommonEntityFilter,
    // OtherTypesComponent,
    // ObjectTypesComponent,
    // TableTypesComponent,
    // FacilityTableTypeComponent,
    // GridTypesComponent, PopupFlexTypesComponent,
    // PopoverTableTypeComponent,
    FormHeaderComponent,
    // HavePermissionDirective,
    // MenuActionIconsComponent,
    SharedModule,
    // CustomEllipsisExtraCharactor,
    RenameComponent,
    DynamicInputWidth,
    DisableProperty,
    // ExpandChartComponent,
    AngularMaskModule,
    NewEntityBIComponent,
    NewEntityMapComponent,
    EntityManagement_Components,
    MaterialDesignFrameworkModule,
    FilterDialogComponent,
    CapabilityLandingPageComponent
  ]
})

export class SharedModule1 {

}
