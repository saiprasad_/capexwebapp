import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { getNewUUID, getUTCDateTime, replaceAll } from '../../globals/helper.functions';
import { BOOLEAN_VALUES, initializeOptions, ColumnTypes, Between, NotBetween, filterTablesBasedOnQueryOperator } from '../../globals/filter-options';
import { MessageType,MessageModel, IModelMessage, MessageDataTypes } from '../../globals/Model/Message';
import { DatePipe } from '@angular/common';
import { FilterTable } from '../../common/constants/db-table-constants'
import { CommonService } from '../../globals/CommonService'
import { Platform } from '@ionic/angular';
import { AppConsts, UserKey } from '../../common/Constants/AppConsts';
import { AppConfig } from '../../globals/services/app.config';
import { PopUpAlertComponent } from '../../common/PopUpAlert/popup-alert.component';
import { Observable } from 'rxjs';
import { PopupOperation } from '../../globals/Model/AlertConfig';
import { EntityTypes } from '../../globals/Model/EntityTypes';
import { Datatypes } from '../../globals/Model/CommonModel';
@Component({
  selector: 'app-filter-dialog',
  templateUrl: './filter-dialog.component.html',
  styleUrls: ['./filter-dialog.component.scss'],
})

export class FilterDialogComponent{
  alartActiv:boolean=false;
  clearFilter:boolean=false;
  activeFilterName:string;
  filterTypes:string[]=["Personal","Project","Company"];
  filterType:string;
  displayNewFilter:boolean=false;
  display:boolean=true;
  isDisable:boolean=true;
  show: any[] = [];
  modelChange: boolean = false;
  currentUUID: string = "";
  currentTitle: string = "";
  wrapper: MessageModel = new MessageModel();
  isDefault: boolean = false;
  previousUUID: string;
  //Option and values for diffrent type
  dateOptions: any = [];
  stringOptions: any = [];
  numberOptions: any = [];
  booleanOption: any = [];
  logicalOptions: any = [];
  timeType = ColumnTypes.TIME;
  stringType = ColumnTypes.STRING;
  dateType = ColumnTypes.DATE;
  numberType = ColumnTypes.NUMBER;
  booleanType = ColumnTypes.BOOLEAN;
  booleanValue = BOOLEAN_VALUES;
  between = Between;
  notbetween = NotBetween;
  itemsList: any[] = [];
  fieldList: any[] = [];
  listModel = {};
  schema = {};

  tableData: any[];
  toShow: any[] = [];
  parentKey: any;
  resultObj = {};
  models = {};
  setText: string;
  filterName:string;
  options: any[] = [];
  addCustomTag = (item) => {
    const option: any = {};
    option["entityid"] = getNewUUID();
    option["entityname"] = item;
    option['isnew'] = true;
    this.options = [...this.options, option];
    return option;
  };

  constructor(public dialogRef: MatDialogRef<FilterDialogComponent>,private matDialog:MatDialog,
    @Inject(MAT_DIALOG_DATA) private modalData: any, private datePipe: DatePipe, private commonService: CommonService, private platform: Platform) {
      this.wrapper.ApplicationName =  AppConfig.AppMainSettings.env.ApplicationName;
      this.initalizeData();  
  }
  initalizeData() {
    if(this.modalData.clearFilter){
      this.clearFilter = true;
      this.activeFilterName = this.modalData.activeFilterName;
    }
    else{
      let dataPointer = this.modalData.dataPointer;
      let paths = dataPointer.split("/");
      this.parentKey = paths[paths.length - 1];
      if(this.modalData.inline){
        this.schema = this.modalData.schema['items'].properties;
      }else{
        this.schema =this.getNonInlineTableSchemaProperties(this.modalData.schema);
      }


      this.initializeOptions();
      //this.commonService.presentStatusMessageAsync("Fetching saved Options...")
      this.fetchDataFromFilterTable().then(() => {
        this.getDefaultmodel(this.show);
        //this.commonService.dismissStausMessage();
      });

      Object.keys(this.schema).forEach(element => {
        if (this.schema[element].hide == false) {
          this.fieldList.push(element);
        }
      });
      this.fieldList.sort();
      this.createEmptyModel();
      this.createNewListModel();
    }
  }

  getNonInlineTableSchemaProperties(inputSchemas:Array<string>){
    let schemaProperties ={}
    if(inputSchemas){
      for(let inputSchemaString of inputSchemas){
        let inputSchemaJSon = JSON.parse(inputSchemaString)
        let schema = inputSchemaJSon.schema;
        if(schema && schema.properties){
          let keys = Object.keys(schema.properties);
          for(let key of keys){
            let node =  schema.properties[key];
            if(!node.virtualKey && node.type =='object' && node.properties){
              let properties  = Object.keys(node.properties)
              for(let prop of properties){
                if(!schemaProperties[prop]){
                  schemaProperties[prop]= node.properties[prop]
                }
              }
              
            }
          }
        }
      }
    }
    return schemaProperties;
  }
   closeModal() {
    if(this.displayNewFilter==true)
    {
      let alartResponce
     this.alartResponce().subscribe(res=>
        {
           alartResponce=res;
           if(alartResponce==true)
           {
           this.displayNewFilter=false;
           this.display=true;
           this.emptyItemList();
           }
        })
     
    }
    else{
    this.dialogRef.close();
  }
}
emptyItemList()
{
  this.filterName = '';
  this.filterType = '';
  this.itemsList=[];
  this.addNewItem();
  
}
addnewiteminngselect()
{
  this.currentTitle= this.filterName;
  this.currentUUID=getNewUUID();
  let item={};
  item["entityid"]=this.currentUUID;
  item["entityname"]=this.currentTitle;
  this.onSubmit();
  if(this.filterName!=null || this.filterName!=""){
    this.setText=this.currentUUID;
  }
}
  async onSubmit() {
    

    this.itemsList;
    let sqllitequery = this.getSQLLiteFilterQuery(this.itemsList,this.schema);
    let nonsqllitequery = this.getNonSQLLiteFilterQuery(this.itemsList,this.schema);
    let payload = {'listitem':this.itemsList,'sqllitequery':sqllitequery,'nonsqllitequery':nonsqllitequery,'filterName':this.currentTitle,'activateFilter':true};
    if (this.modelChange == true) { // save the filtern only if values are changed.
      this.insertIntoTableAndChangeDefault(sqllitequery,nonsqllitequery).then(d=>{
        this.fetchDataFromFilterTable();
        this.emptyItemList();
      })
    }
    else {
      this.changeDefaultModel(this.previousUUID, this.currentUUID);
     this.emptyItemList();
    }
    if(this.display==true)
    {
      this.dialogRef.close(payload);
    }
    this.displayNewFilter=false;
    this.display=true;
    
  }

  async onClear() {   
    let payload = {'deActivateFilter':true,'activateFilter':false,'filterName':''};
    this.dialogRef.close(payload);  
  }

  onChange(event) {    
    if(event!=undefined) {
      this.isDisable=false
      this.currentUUID = event.entityid;
      this.currentTitle = event.entityname;
      if (event.isnew) {
        //this.commonService.presentStatusMessageAsync("Added Item...");
        this.setText = event.entityid;
        setTimeout(() => {
          //this.commonService.dismissStausMessage();
        }, 2000)
        this.modelChange = true;
      }
      else {
        this.itemsList = JSON.parse(event.payload).model;
      }
    }
  }

  clear() {
    this.isDisable=true;
    //this.commonService.presentStatusMessageAsync("Clearing Defaults...");
    this.clearDefault();
    this.createEmptyModel();
    this.modelChange = false;
    this.currentUUID = "";
    this.currentTitle = "";
    setTimeout(() => {
      //this.commonService.dismissStausMessage();
    }, 1000)  
  }


  modelChangeFn(event) {
    this.modelChange = true;
  }


  async insertOrUpdateTable(sqllitequery,nonsqllitequery) {
    const payload: any = {
      model: this.itemsList,
      sqllitequery:sqllitequery,
      nonsqllitequery:nonsqllitequery,
      filterType:this.filterType
    }
    let insertData = {};
    if (this.currentUUID != "" && this.currentTitle != "" && this.currentTitle != undefined && this.currentUUID != undefined) { // undefined
      await this.clearDefault();
      insertData["entityid"] = this.currentUUID;
      insertData["entityname"] = this.currentTitle;
      insertData["userid"] = this.commonService.currentUserId;
      insertData["schemapage"] = this.modalData.PrentId;
      insertData["datapointer"] = this.parentKey;
      insertData["payload"] = JSON.stringify(payload);
      insertData["tenantid"] = this.commonService.tenantID;
      insertData["tenantname"] = this.commonService.tenantName;
      insertData["createddatetime"] = getUTCDateTime();
      insertData["modifieddatetime"] = getUTCDateTime();
      insertData["createdby"] = this.commonService.currentUserName;
      insertData["modifiedby"] = this.commonService.currentUserName;
      insertData["isdefault"] = "true";
      insertData["filtertype"] = this.filterType;
      insertData["filtertypeid"] = this.getFilterTypeId(this.filterType);
      insertData["applicationid"] = this.modalData.applicationId;
      let imessageModel: IModelMessage = this.commonService.getMessageWrapper({Payload:JSON.stringify(insertData),DataType:Datatypes.FILTER,EntityType:EntityTypes.FILTER,EntityID:this.currentUUID,MessageKind:MessageType.UPDATE});
      this.commonService.postDataToBackend(imessageModel).subscribe(res => {
        return res;
      });;
    }
    return null;
  }

  getFilterTypeId(filterType:string):string{
    let filterTypeId:string;
    switch(filterType){
      case "Personal":
        filterTypeId = this.commonService.currentUserId
        break;
      case "Project":
        filterTypeId = this.modalData.entityId;
        break;
      case "Company":
        filterTypeId = this.commonService.tenantID;
        break;
    }
    return filterTypeId;
  }

  getSQLLiteFilterQuery(itemList,schema){
    return this.getQueryForFilterTable(itemList,schema,AppConsts.SQKLITEQUERY);
  }

  getNonSQLLiteFilterQuery(itemList,schema){
    return this.getQueryForFilterTable(itemList,schema,AppConsts.NONSQLLITEQUERY);
  }

  async fetchDataFromFilterTable() {
    let filterTypeIds = `('${this.commonService.currentUserId}','${this.modalData.entityId}','${this.commonService.tenantID}')`;
    let payload = {tableName:FilterTable,datapointer:this.parentKey,filterTypeIds:filterTypeIds,applicationId:this.modalData.applicationId};
    let imessageModel: IModelMessage = this.commonService.getMessageWrapper({Payload:JSON.stringify(payload),DataType:Datatypes.FILTER,EntityType:EntityTypes.AVAILABLEFILTERS,EntityID:this.parentKey,MessageKind:MessageType.READ});
    this.commonService.postDataToBackend(imessageModel).subscribe(data => {
      let show = typeof(data) == 'string' ? JSON.parse(data) : data;
      if(Array.isArray(show)){
        this.show = [];
        show.forEach(element => {
          let keys = Object.keys(element);
          let temoobj = {};
          keys.forEach(key => {
            temoobj[key.toLowerCase()] = element[key];
          });
          this.show.push(temoobj);
        });  
      }
    });
  }

  createEmptyModel() {
    this.itemsList.splice(0, this.itemsList.length);
  }

  initializeOptions() {
    this.dateOptions = initializeOptions(ColumnTypes.DATE);
    this.stringOptions = initializeOptions(ColumnTypes.STRING);
    this.numberOptions = initializeOptions(ColumnTypes.INTEGER);
    this.booleanOption = initializeOptions(ColumnTypes.BOOLEAN);
    this.logicalOptions = initializeOptions(ColumnTypes.LOGICAL);
  }

  getDefaultmodel(model) {
    var defaultdata = model.filter((row: any) => row.isdefault == 'true');
    if (defaultdata != null && defaultdata != undefined && defaultdata.length > 0) {
      this.itemsList = JSON.parse(defaultdata[0].payload).model;
      this.currentUUID = defaultdata[0].entityid;
      this.currentTitle = defaultdata[0].entityname;
      this.setText = this.currentUUID;
      this.previousUUID = defaultdata[0].entityid;
    }
    else if (defaultdata.length == 0) {
      this.previousUUID = "empty";
    }
  }

  async changeDefaultModel(previousUUID, currentUUID) {
    try {
      if (previousUUID != "" && currentUUID != "" && previousUUID != undefined && currentUUID != undefined && previousUUID != null && currentUUID != null && previousUUID != currentUUID && previousUUID != "empty") {
        this.pschangeDefaultModel(FilterTable, this.parentKey, "false", previousUUID);
        this.pschangeDefaultModel(FilterTable, this.parentKey, "true", currentUUID);
      }
      else if (previousUUID == "empty") {
        this.pschangeDefaultModel(FilterTable, this.parentKey, "true", currentUUID);
      }
    } catch (mes) {
      console.log(mes);
    }
  }

  async clearDefault() {
    try {
      await this.fetchDataFromFilterTable()
      var defaultdata = this.show.filter((row: any) => row.isdefault == 'true');
      defaultdata.forEach(element => {
        this.pschangeDefaultModel(FilterTable, this.parentKey, "false", element.entityid);
      });
    } catch (e) {
      console.log(e);
    }
  }

  async insertIntoTableAndChangeDefault(sqllitequery,nonsqllitequery) {
    return await this.insertOrUpdateTable(sqllitequery,nonsqllitequery)
  }

  addNewItem() {
    this.itemsList.push({ ...this.listModel });
  }

  createNewListModel() {
    this.listModel = {
      key: "",
      operator: "",
      value: "",
      valuetwo: "",
      logicalOperator: "AND",
    }
  }

  deleteItem(event, i) {
    this.modelChange = true;
    this.itemsList.splice(i, 1);
  }
  goToNewFilter()
  {
    this.emptyItemList();
    this.display=false;
    this.displayNewFilter=true;
  }
  closePopupAlart()
  {
    let message= 'Are you sure you want to discrad  your work?';
    let config = 
    {
      setTitle: 'Attention!',
      isSubmit: true,
      operation: PopupOperation.AlertConfirm,
      content: [message],
    };
    let matConfig = new MatDialogConfig();
    matConfig.data = config;
    matConfig.width = '500px';
    matConfig.disableClose = true;
    matConfig.id = message;

    let dialogRef = this.matDialog.open(PopUpAlertComponent, matConfig);
      return dialogRef;
  }
  alartResponce(): Observable<boolean> {
    
    let dialogRef = this.closePopupAlart();

    return dialogRef.componentInstance.emitResponse;
  }

  //query operation persistance service
getQueryForFilterTable(itemList,schema,queryType) {
  let queryFilter = "";
  try {
    itemList.forEach((element, i) => {
      if (element.operator != null && element.operator != "") {
        let currentElement = schema[element.key];
        let type = currentElement.type;
        let format = currentElement.format;
        let logicalOperator;
        if (i == 0) {
          logicalOperator = "AND";
        } else {
          logicalOperator = itemList[i - 1]?.logicalOperator;
        }
        queryFilter += filterTablesBasedOnQueryOperator(element.operator, element.value, element.key, logicalOperator, element.valuetwo, type, format,queryType);
      }
    });
  } catch (err) {
  } 
  if(queryType == AppConsts.SQKLITEQUERY){
    queryFilter = replaceAll(queryFilter,"'","''");
  }
  return queryFilter;
} 

async pschangeDefaultModel(FilterTable, dataPointer, value, id) {
  if (id != "" && id != undefined) {
    const Query: string = this.QueryBuilderForUpdateDefault(FilterTable, dataPointer, value, id);
    let payload = {query : Query};
    let imessageModel: IModelMessage = this.commonService.getMessageWrapper({Payload:JSON.stringify(payload),DataType:MessageDataTypes.FILTER,EntityType:EntityTypes.FILTER,EntityID:this.currentUUID,MessageKind:MessageType.UPDATE});
    return this.commonService.postDataToBackend(imessageModel);
  }
}

//query builder methods
QueryBuilderForUpdateDefault(tableName,datapointer,value,id)
{
 try {
   let query = "";
   query = `update ${tableName} set isdefault='${value}' where entityid='${id}' and datapointer='${datapointer}';`;
   return query;
   }
 catch (error) {
 }
}

}


