import { Component, Input, Output, OnInit, EventEmitter, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { Observable, Subject } from 'rxjs';

import { CommonService } from '../../globals/CommonService';
// import { DesktopSQLiteState } from '../Constants/DesktopConstants';
import { FABRICS } from '../../globals/Model/CommonModel';
import { takeUntil } from 'rxjs/operators';
import { debounced } from '../../template/LayoutComponents/leftsidebar/tree.component';
import { FormControl } from '@angular/forms';

export const NEXT = 'NEXT';
export const CHANGE = 'CHANGE';
export const PREVIOUS = 'PREVIOUS';

@Component({
  selector: 'form-header',
  templateUrl: './form-header.component.html',
  styleUrls: ['./form-header.component.scss']
})
export class FormHeaderComponent implements OnInit, OnDestroy {

  @Input() headerConfig: any;
  @Input() maxDate: any;
  @Input() locationName: string = '';
  @Input() selectedDate: any;
  @Input() popUPForm: boolean = false;
  
  @Output() Click: EventEmitter<any> = new EventEmitter();
  @Output() ChangeDate: EventEmitter<any> = new EventEmitter();
  @Output() ChangeLocation: EventEmitter<any> = new EventEmitter();

  // locationData: any;
  changedetactfordate;
  //productionDate: string = new Date().toLocaleDateString();
  selectedDateObj: Date;
  dataconfig = {
    opreation: '',
    location: '',
    locationId: '',
    date: '',
    prevDate: ''
  }
  filterText = '';
  // private desktopOnline$ = Observable.fromEvent(window, DesktopSQLiteState.online);
  // private desktopOffline$ = Observable.fromEvent(window, DesktopSQLiteState.offline);
 disableIcons=["Settings","Analytics","PhysicalFlow"];
 debouncedClick
closeCurrentForm:boolean=false;
  constructor(public router: Router, public route: ActivatedRoute, public commonService: CommonService) {
   // this.commonService.setPrevDate.pipe(this.compUntilDestroyed()).subscribe((res: any) => this.changeToPrevDate());

    // this.desktopOnline$.pipe(this.compUntilDestroyed()).subscribe(e => {
    //   if(this.headerConfig && this.headerConfig.length!=0){
    //     this.headerConfig.forEach(element => {
    //       if(element.id && this.disableIcons.includes(element.id)){
    //         element.disable=false;
    //       }
    //     });
    //   }
    // });

    this.debouncedClick = debounced($event => this.searchEntity($event), 400);
  }


  ngOnInit() {
    try {
      if (!this.selectedDate) {
        this.selectedDate = new Date();
      }
      //this.selectedDateObj = new Date(this.selectedDate);
      if (!this.changedetactfordate || this.changedetactfordate != this.selectedDate) {
        this.selectedDateObj = new Date(this.selectedDate);
        this.changedetactfordate = this.selectedDate;
      }

    
    }
    catch (e) {
      console.error('Exception in ngOnInit() of form-header.component in form-header at time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }

  ngAfterViewInit(){
    
  }

  ngAfterViewChecked() {
    if (!this.changedetactfordate || this.changedetactfordate != this.selectedDate) {
      this.selectedDateObj = new Date(this.selectedDate);
      this.changedetactfordate = this.selectedDate;
    }
    // if (this.fDCService.FdcFormLocationName != this.locationName) {
    //   setTimeout(() => {
    //     this.locationName = this.fDCService.FdcFormLocationName;
    //   }, 0);
    // }
  }

  onClick(data) {
    try {
      switch(data.id){
        case "search":
          this.filterText = '';
          this.headerConfig.forEach(element =>{
             if(element.id == "searchinput"){
               element["show"] = !element["show"];
               if(!element["show"]){
                this.searchEntity({target:{value:''}});
               }
              }
              else if(element.id == "search"){
                element["opacity"] = !element["opacity"];
              }
          });
          break;
          default:
            this.Click.emit(data);
            break;
      }
    }
    catch (e) {
      console.error('Exception in onClick() of form-header.component in form-header at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  clearFilter(){
    this.filterText = '';
    this.Click.emit({id:"searchinput",value:this.filterText})
  }

  getPrevLocation(data) {
    try {
      // this.fDCService.TestForm = false;
      this.dataconfig.opreation = PREVIOUS;
      this.commonService.switchButton = this.dataconfig.opreation;
      this.dataconfig.location = this.locationName;
      this.dataconfig.date = this.selectedDate;
      this.ChangeLocation.emit(this.dataconfig);
    }
    catch (e) {
      console.error('Exception in getPrevLocation() of form-header.component in form-header at time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }

  getNextLocation(data) {
    try {
      // this.fDCService.TestForm = false;
      this.dataconfig.opreation = NEXT;
      this.commonService.switchButton = this.dataconfig.opreation;
      this.dataconfig.location = this.locationName;
      this.dataconfig.date = this.selectedDate;
      this.ChangeLocation.emit(this.dataconfig);
    //if (this.selectedDateObj <= this.maxDate)
    //  this.selectedDateObj.setDate(this.selectedDateObj.getDate() + 1);
    }
    catch (e) {
      console.error('Exception in getNextLocation() of form-header.component in form-header at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  getPrevDate(data) {
    try {
      this.dataconfig.opreation = PREVIOUS;
      this.commonService.switchButton = this.dataconfig.opreation;
      this.dataconfig.date = this.selectedDate;
      this.dataconfig.prevDate = this.selectedDate;
      this.ChangeDate.emit(this.dataconfig);
      //this.selectedDateObj.setDate(this.selectedDateObj.getDate() - 1);
      // this.selectedDateObj = new Date(this.fDCService.calenderProductionDate);
      this.selectedDateObj.setDate(this.selectedDateObj.getDate());
    }
    catch (e) {
      console.error('Exception in getNextLocation() of form-header.component in form-header at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  getNextDate(data) {
    try {
      this.dataconfig.opreation = NEXT;
      this.commonService.switchButton = this.dataconfig.opreation;
      this.dataconfig.date = this.selectedDate;
      this.dataconfig.prevDate = this.selectedDate;
      this.ChangeDate.emit(this.dataconfig);
      //this.selectedDateObj = new Date(this.selectedDate);
      //if (this.selectedDateObj <= this.maxDate)
      //this.selectedDateObj.setDate(this.selectedDateObj.getDate() + 1);
      // this.selectedDateObj = new Date(this.fDCService.calenderProductionDate);
      this.selectedDateObj.setDate(this.selectedDateObj.getDate());
    }
    catch (e) {
      console.error('Exception in getNextDate() of form-header.component in form-header at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  dateChangeEvent(ev) {
    try {
      this.dataconfig.opreation = CHANGE;
      let newDate = ev.value;
      newDate.setHours(8, 0, 0, 0);
      this.dataconfig.prevDate =  this.selectedDate;
      this.selectedDate = this.dataconfig.date = newDate;
      this.commonService.switchButton = this.dataconfig.opreation;
      this.ChangeDate.emit(this.dataconfig);
      // this.selectedDateObj = new Date(this.fDCService.calenderProductionDate);
      // this.selectedDateObj.setDate(this.selectedDateObj.getDate());
      // this.fDCService.MakeFormsAsReadOnly = false;
    }
    catch (e) {
      console.error('Exception in dateChangeEvent() of form-header.component in form-header at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  // changeToPrevDate(){
  //   if(this.dataconfig.prevDate && !this.fDCService.MakeFormsAsReadOnly)
  //     this.selectedDate = this.dataconfig.prevDate;
  // }

  searchEntity(event) {
    try {
      let value;
      if(event){
       value = event.target.value ? event.target.value : '';
       this.Click.emit({id:"searchinput",value:value})
      }
    }
    catch (e) {
      console.error('Exception in searchEntity() of Form-header  at orm-headerComponent time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }


  
  ngOnDestroy() {
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }
  takeUntilDestroyObservables=new Subject();
      compUntilDestroyed():any {
          return takeUntil(this.takeUntilDestroyObservables);
      }
}
