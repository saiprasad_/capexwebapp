import { Component, Inject, EventEmitter, Output, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommonService } from '../../globals/CommonService';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'rename-form',
  templateUrl: 'rename-form.component.html',
  styleUrls: ['./rename-form.component.styles.scss']
})
export class RenameComponent implements OnInit {


  @Output() emitResponse = new EventEmitter<any>(true);
  headerConfig;
  Operation;
  ExistingName = "";
  title;
  rename = '';
  renameConst = "Rename";
  copyConst = "Copy Dashboard";
  //copyConst = "_Copy";
  dashboardNameConst = "Dashboard Name";
  isValid = true;
  headertitle;
  parentList = [];
  SelectedParent = [];
  PrentValue = new FormControl();
  sortByProperty = function (property) {
    return function (x, y) {
      return ((x[property].toLowerCase() === y[property].toLowerCase()) ? 0 : ((x[property].toLowerCase() > y[property].toLowerCase()) ? 1 : -1));
    };
  };

  constructor(public dialogRef: MatDialogRef<RenameComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public commonService: CommonService) {
    this.headertitle = data.Header;
    this.Operation = data.Operation;
    //this.ExistingName = this.Operation == this.renameConst ? "" : data.ExistingName + this.copyConst;
    this.title = this.Operation == this.renameConst ? this.renameConst : this.dashboardNameConst;
    if (this.Operation == this.copyConst) {
      this.parentList = this.commonService.getEntititesBy('EntityType', ['Category'], d => { return { "id": d.EntityId, "text": d.EntityName } })
        .map(d => JSON.parse(JSON.stringify(d))).sort(this.sortByProperty('text'));
      if (data.Parent && data.Parent.length > 0  && data.Parent[0].EntityId != "lkjhgfdsa098765") {
        this.PrentValue.setValue(data.Parent[0].EntityName);
        this.SelectedParent.push(data.Parent[0]);
      }
    }
  }


  ngOnInit() {
    this.headerConfig = [
      {
        'source': '',
        'title': this.headertitle,
        'routerLinkActive': false,
        'id': 'title',
        'class': 'title',
        'float': 'left',
        'type': 'title',
        'show': true,
        'uppercase':'Isuppercase'
      },
      {
        'source': 'V3 CloseCancel',
        'title': 'Close',
        'routerLinkActive': false,
        'id': 'close',
        'class': 'icon21-21',
        'float': 'right',
        'type': 'icon',
        'show': true
      },
      {
        'source': 'FDC_Checkmark_Icon_Idle',
        'title': 'Save',
        'routerLinkActive': false,
        'id': 'save',
        'class': 'icon21-21',
        'float': 'right',
        'type': 'icon',
        'show': true
      }
    ];
  }

 
  headerClickEvent(event) {
    if (event.id == 'save') {
      if (this.ExistingName.trim() != "") {
        if (this.Operation == this.renameConst) {
          this.emitResponse.emit(this.ExistingName);
        }
        else if (this.Operation == this.copyConst) {
          this.emitResponse.emit({ Name: this.ExistingName, Parent: this.SelectedParent });
        }
        this.dialogRef.close();
      }
      this.isValid = this.ExistingName.trim() == "" ? true : false;
    }
    else if (event.id == 'close') {
      this.dialogRef.close();
    }
    
  }

  checkfiled() {
    this.isValid = this.ExistingName.trim() == "" ? true : false;
  }

  onChangeParent(data) {
    try {
      if (!data) {
        this.SelectedParent = [];
      }
      else {
        this.SelectedParent = [{ EntityId: data.id, EntityName: data.text}]
        this.PrentValue.setValue(data.text);
      }
    }
    catch (e) {
      console.error('Exception in onChangeParent() of renameformComponent at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

}

