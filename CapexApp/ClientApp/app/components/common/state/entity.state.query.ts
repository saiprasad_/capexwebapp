import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { produce } from 'immer';
import { debounceTime } from 'rxjs/operators';
import { EntityStateConstantsKeys, EntityStateModel } from './entity.state.model';
import { CommonEntityState, EntityCommonStore } from './entity.state.store';
@Injectable({ providedIn: 'root' })
export class EntityCommonStoreQuery extends QueryEntity<CommonEntityState, EntityStateModel> {
  private currentExcutionMethod = "";

  constructor(protected store: EntityCommonStore) {
    super(store);
  }

  /**
   * intialize put into state
   *
   * @param entities state of the entities store
   */
  Set(entities: any[]) {
    // if(!Array.isArray(entities))
    if(entities && Object.keys(entities).length > 0)
    Object.keys(entities).forEach(key => {if (entities[key] === undefined) {delete entities[key]; } });

    this.store.set(entities);
  }

  /**
   * Add new Entitiy to the existing store
   */
  upsert(entityId: string, entity: any) {
    if (!entity || !entityId)
      throw new Error('Error :: Entity or EntityId is undefined!');

    this.store.upsert(entityId, entity);
  }

  AddAll(entities: any[]) {
    entities = entities.filter((entity: any) => entity != undefined);
    this.store.add(entities);
  }

  // Update Security group and Capability array in state
  UpdateArrayValue(entities, key, value) {
    try {
      this.store.update(entities, state => produce(state, draft => {
        let existingValues = <Array<any>>draft[key];
        if (existingValues&&existingValues.indexOf(value) < 0) {
          draft[key] = [...existingValues, value]
        }
        else{
          draft[key]=[value]
        }
      }))
    }
    catch (e) {
      console.error(e);
    }
  }

  UpdateProperties(key,ArrayEntities) {
    try {
      this.store.update(state => produce(state, draft => {
        ArrayEntities.forEach(entity=>{
          // draft[key]=entity[key]
          draft[EntityStateConstantsKeys.entities][entity.EntityId][key]=entity[key]
        })
      }))
    }
    catch (e) {
      console.error(e);
    }
  }

  GetStateKey(key: string) {
    return this.getValue()[key];
  }

  /**
   *
   * @param key "EntityId"
   * @param EntityId "EntityId value"
   */
  deleteNodeById(key, EntityId) {
    try {
      this.store.update(state => produce(state, draft => {
        delete draft[EntityStateConstantsKeys.entities][EntityId];
        draft['ids'].splice(draft['ids'].findIndex(id => id === EntityId), 1);
      }))
    }
    catch (e) {
      console.error(e);
    }
  }

  /**
   * Delete Entities from state
   *
   * @param {any[]} entityIds
   * @memberof EntityCommonStoreQuery
   */
  deleteNodeByIds(entityIds: any[]) {
    try {
      this.store.remove(entityIds);
    }
    catch (e) {
      console.error(e);
    }
  }

  resetStore() {
    try {
      // this.store.destroy();
      this.store.reset();
    }
    catch (e) {
      console.error(e);
    }
  }

  /**
   *
   * To update SG Group
   *
   * @param fabric key of the SG Array e.g fdc
   * @param entityId id of the entities
   */
  deleteInSGAndCapabilities(sgFabric: string, capabilityId: string, entityIdList: Array<any>) {
    try {
      this.currentExcutionMethod = "deleteInSGAndCapabilities";
      if (this.stateStatus()) {
        this.store.update(state => produce(state, draft => {
          entityIdList.forEach(entityId => {
            let entity =draft[EntityStateConstantsKeys.entities][entityId]
            if (entity) {

                //delete fabric from SG
                var sgIndex = entity["SG"].indexOf(sgFabric);
                entity["SG"].splice(sgIndex, 1);
                let entityCapbilities =entity["Capabilities"];
                //delete capability id from Capabilities Array
                if (entityCapbilities && entityCapbilities.length && entityCapbilities.indexOf(capabilityId) >= 0) {
                  var capIdIndex = entityCapbilities.indexOf(capabilityId);
                  entityCapbilities.splice(capIdIndex, 1);
                }

            }
          });
        }));
      }
    }
    catch (e) {
      console.error(e);
    }
  }


  //future implmentations
  //after delete entity again if want to assgin for the fabric ["ghjk"]
  updateInSGAndCapabilities(sgFabric, capabilityId, entityIdList) {
    try {
      this.currentExcutionMethod = "updateInSGAndCapabilities";
      if (this.stateStatus()) {
        this.store.update(state => produce(state, draft => {
          entityIdList.forEach(entityId => {
            if (draft[EntityStateConstantsKeys.entities][entityId]) {
              if (draft[EntityStateConstantsKeys.entities][entityId]["SG"].indexOf(sgFabric) === -1) {
                draft[EntityStateConstantsKeys.entities][entityId]["SG"].push(sgFabric);
              }
              if (draft[EntityStateConstantsKeys.entities][entityId]["Capabilities"] && draft[EntityStateConstantsKeys.entities][entityId]["Capabilities"].indexOf(capabilityId) === -1) {
                draft[EntityStateConstantsKeys.entities][entityId]["Capabilities"].push(capabilityId);
              }
            }
          });
        }));
      }
    }
    catch (e) {

    }
  }


  /**
   * Update entity's multiple properties at once of multiple entities
   *
   * @param {Array<any>} entityIdList ie. => ['5678fghj-fghj-gh55-91c7-9ef75d3ee99d', 'fgh67-fe685987-5be9-417dfghj-k56789']
   * @param {any} properties ie. => { "SG" : 'FDC', "Capabilities": 'fe685987-5be9-417c-91c7-9ef75d3ee99d' };
   * @memberof EntityCommonStoreQuery
   */
  updateEntityProperties(entityIdList: Array<any>, properties: any) {
   try {
    for (const key in properties) {
      if (properties.hasOwnProperty(key)) {
        const value = properties[key];
        this.store.update(entityIdList, state => produce(state, draft => {
          let existingValues = <Array<any>>draft[key];
          if (existingValues && existingValues.indexOf(value) < 0) {
            draft[key] = [...existingValues, value];
          }
        }));
      }
    }
   }
   catch (e) {
     console.error("Error at EntityStore Query updateEntityProperties() for the method :: " + e)
   }
  }

  stateStatus() {
    var stateExistingData = this.GetStateKey(EntityStateConstantsKeys.entities);
    try {
      if (stateExistingData && this.isNotEmpty(stateExistingData)) {
        return true;
      }
    }
    catch (e) {
      console.error("Error at EntityStore Query stateStatus() for the method " + this.currentExcutionMethod + " " + e)
    }
    return false;
  }

  isNotEmpty(jsonObject) {
    for (var key in jsonObject) {
      if (jsonObject.hasOwnProperty(key))
        return true;
    }
    return false;
  }

  //desktop
  selectStateEntities(){
    return this.select().pipe(debounceTime(250));
  }

}
