import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { EntityStateModel } from './entity.state.model';


export interface CommonEntityState extends EntityState<EntityStateModel> { }

const createInitialState = (json): EntityStateModel => {
    return JSON.parse(json);
}
const json = {};

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'entities', resettable: true,idKey: 'EntityId' })
export class EntityCommonStore extends EntityStore<CommonEntityState, EntityStateModel> {
    // public CustomerAppsettings = AppConfig.AppMainSettings;
    // private appConfig: AppConfig;

    constructor() {
        super({ initialState: createInitialState(JSON.stringify(json)), loading: false });

        // if ((this.CustomerAppsettings.env.name == "PROD" || this.CustomerAppsettings.env.name == "DEV") && this.CustomerAppsettings.env.UseAsDesktop == false) {
        //     if (this.appConfig.tenantName) {
        //         localForage.config({
        //             driver: localForage.INDEXEDDB,
        //             name: 'Visur',
        //             storeName: this.appConfig.tenantName
        //         });
        //         persistState({ key: this.storeName, include: [this.storeName], storage: localForage
        //             // ,
        //             // preStorageUpdateOperator: () => debounceTime(3000)
        //          });
        //     }
        //     else {
        //         console.error(this.storeName + " TenantName can not be undefinded");
        //     }

        // }
    }
}

