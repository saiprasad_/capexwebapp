
import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Entity, FormStateModel } from './form.model';

export interface CommonEntityState extends EntityState<FormStateModel<Entity>> { }

const createInitialState = (json): FormStateModel<Entity> => {
    return JSON.parse(json);
}
const json = {};

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'forms', resettable: true, idKey: "EntityId" })
export class FormStore extends EntityStore<CommonEntityState, FormStateModel<Entity>>{
    constructor() {
        super({ initialState: createInitialState(JSON.stringify(json)), loading: false });
        // localForage.config({
        //     driver: localForage.INDEXEDDB,
        //     name: 'Visur',
        //     storeName: 'visurtest'
        // });
        // persistState({ key: 'forms', include: ["forms"], storage: localForage });
    }
}