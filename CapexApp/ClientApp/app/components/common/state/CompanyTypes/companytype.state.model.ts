import { FABRICS } from "ClientApp/app/components/globals/Model/CommonModel";

export interface EntityCompanyTypesStateModel {
}

export class PeopleCompanyTypes{
    EntityId: string;
    EntityType: string;
    EntityName: string;
    CompanyType: Array<string>;
    CapabilityId:string;
    CapabilityName:string;
    SG:Array<string>;
    ModifiedDateTime:string;

    public getdefultData() {
      let defaultData ={
        EntityId:"",
        EntityType:"",
        EntityName:"",
        CapabilityId:"",
        CapabilityName:"",
        CompanyType:[],
        SG:[FABRICS.PEOPLEANDCOMPANIES],
        ModifiedDateTime: new Date().toISOString()
      };
      
      return defaultData;
    }
}