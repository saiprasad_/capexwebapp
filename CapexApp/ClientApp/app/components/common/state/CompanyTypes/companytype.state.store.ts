import { StoreConfig,EntityState, EntityStore } from '@datorama/akita';
import { EntityCompanyTypesStateModel  } from './companytype.state.model';
import { Injectable } from '@angular/core';

export interface EntityCompanyTypesState extends EntityState<EntityCompanyTypesStateModel> { }

const createInitialState = (json): EntityCompanyTypesStateModel => {
    return JSON.parse(json);
}
const json = {};

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'companytypes', resettable: true,idKey: 'EntityId' })
export class EntityCompanyTypesStore extends EntityStore<EntityCompanyTypesState, EntityCompanyTypesStateModel> {
    constructor() {
        super({ initialState: createInitialState(JSON.stringify(json)), loading: false });
    }
}

