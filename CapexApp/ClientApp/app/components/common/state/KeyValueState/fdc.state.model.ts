import { ID } from '@datorama/akita';











export interface FDCState {
  id: ID;
  DowntimeReasons: Array<any>;
  DataAlertRules: Array<any>;
  TankLibrary: Array<any>;
  Statuses: Array<any>;
  Formations: Array<any>;
  FuelFlareVent: Array<any>;
  VolumeOverrideReasons: Array<any>;
  OrificeLibrary: Array<any>;
  RecalculateHistory: Array<any>;
  schema: any;
  timeZone : string;
}


export interface EntityList {
  id: ID;
  data;
}

export interface EntitySchema {
  id: ID;
  schema;
  datamodel;
}

export class FDCStateConstantsKeys {
  public static timeZone = "timeZone";
  public static downtimeReasons = "DowntimeReasons";
  public static dataAlertRules = "DataAlertRules";
  public static tankLibrary = "TankLibrary";
  public static status = "Statuses";
  public static formations = "Formations";
  public static ffv = "FuelFlareVent";
  public static volumeOverrideReasons = "VolumeOverrideReasons";
  public static orificeLibrary = "OrificeLibrary";
  public static recalculateHistory = "RecalculateHistory";
  public static schema = "schema";
  public static entityState = "entityState";
  public static setAsDefault = "setAsDefault";
  public static Lists = "Lists";
  public static PHYSICALFLOW = "PHYSICAL FLOW";

  public static Hierarchy = "Hierarchy";
  public static All = "All";
  // public static Hierarchy = "Hierarchy";
  public static Licensee = "Licensee";

  public static userPrefrenceSetAsDefault = "SetAsDefault";

  public static entityStateAreas = "areas";

  public static entityStateDistrict = "district";

  public static entityStateEquipments = "equipments";
  public static entityStateFacilities = "facilities";
  public static entityStateFields = "fields";

  public static entityStateHeaders = "headers";
  public static entityStateWells = "wells";

  public static activeAnalysis = "activeAnalysis";
  public static entityStatecurrentEntityDetails = "currentEntityDetails";
}

export class GlobalAdminstatusesModel {
  Active: string;
  ConfigId: string;
  CreatedBy: string;
  CreatedDateTime: string;
  Description: string;
  IsDefault: string;
  ModifiedBy: string;
  ModifiedDateTime: string;
  StatusesName: string;
  Type: string;

  public getDefaultModel() {
    var defultData =
    {
      Active: "",
      ConfigId: "",
      CreatedBy: "",
      CreatedDateTime: "",
      Description: "",
      IsDefault: "",
      ModifiedBy: "",
      ModifiedDateTime: "",
      StatusesName: "",
      Type: ""
    }
    return defultData;
  }

}

export class GlobalAdminFormationsModel {
  Active: string;
  ConfigId: string;
  FormationName: string;
  CreatedBy: string;
  CreatedDateTime: string;
  ModifiedBy: string;
  ModifiedDateTime: string;

  public getDefaultModel() {
    var defultData =
    {
      Active: "",
      ConfigId: "",
      FormationName: "",
      CreatedBy: "",
      CreatedDateTime: "",
      ModifiedBy: "",
      ModifiedDateTime: ""
     
    }
    return defultData;
  }

}

export class GlobalAdminDowntimeReasonsModel {
  Active: string;
  ConfigId: string; 
  Description: string;
  DowntimeReason: string;
  LostProduction: string;
  Planned: string;
  Type: string;
  CreatedBy: string;
  CreatedDateTime: string;
  ModifiedBy: string;
  ModifiedDateTime: string;
  

  public getDefaultModel() {
    var defultData =
    {
      Active: "",
      ConfigId: "",
      Description: "",
      DowntimeReason: "",
      LostProduction: "",
      Planned: "",
      Type: "",
      CreatedBy: "",
      CreatedDateTime: "",
      ModifiedBy: "",
      ModifiedDateTime: "",
    }
    return defultData;
  }

}

export class GlobalAdminOrificeLibraryModel {
  OrificeSizeInch: string;
  ConfigId: string; 
  OrificeSizeMM: string;
  CreatedBy: string;
  CreatedDateTime: string;
  ModifiedBy: string;
  ModifiedDateTime: string;
  

  public getDefaultModel() {
    var defultData =
    {
      OrificeSizeInch: "",
      ConfigId: "",
      OrificeSizeMM: "",
      CreatedBy: "",
      CreatedDateTime: "",
      ModifiedBy: "",
      ModifiedDateTime: "",
    }
    return defultData;
  }

}

export class GlobalAdminTankLibraryModel {
  TankSizeInFeet: string;
  TankSizeInGallon: string;
  TankSizeInMeter: string;
  TankSizeInbbl: string;
  ConfigId: string; 
  CreatedBy: string;
  CreatedDateTime: string;
  ModifiedBy: string;
  ModifiedDateTime: string;
  

  public getDefaultModel() {
    var defultData =
    {
      TankSizeInFeet: "",
      TankSizeInGallon: "",
      TankSizeInMeter: "",
      TankSizeInbbl : "",
      ConfigId: "",
      CreatedBy: "",
      CreatedDateTime: "",
      ModifiedBy: "",
      ModifiedDateTime: "",
    }
    return defultData;
  }

}

export class HeaderContentSequence {
  EntityId: string;
  EntityType: string;
  properties: string;
  headerGrid:Array<any>;
  contentGrid:Array<any>;
  sequenceNumber: string;
  listData: Array<any>;
  isExpand: false;
}

export class EntityHeaderContentSequence{
  public getInstance() {
    return new HeaderContentSequence();
  }
}

export class TabletKeyPadState {
  public static HeaderContentSequenceColGridKey="HeaderContentSequenceColGrid";
}
