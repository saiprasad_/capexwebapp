import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import {filter,map,distinctUntilChanged,flatMap} from 'rxjs/operators';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/distinctUntilChanged';


export class ObservablesStoreConstantKeys {
    public static LayerSchema = "LayerSchema";
    public static ProductionSchema = "ProductionSchema";
    public static PropertiesSchema = "PropertiesSchema"
}
@Injectable({ providedIn: 'root' })
export class ObservableStoreService {
    private store$ = new BehaviorSubject([]);

    private getStore() {
        return this.store$.asObservable();
    }
    public getState$(capability, treeName): Observable<Observable<any>> {
        // return this.getStore().pipe(filter((d:any) => d != null && d != undefined && d.length > 0)).pipe(flatMap(d => d))
        // .pipe(filter((d:any) => d.Capability.includes(capability) && d.Tree.includes(treeName)))
        // .pipe(distinctUntilChanged())
        // .pipe(map(d => (<any>d).observable))

        return this.getStore().pipe(filter((d:any) => d != null && d != undefined && d.length > 0),flatMap(d => d),
        filter((d:any) => d.Capability.includes(capability) && d.Tree.includes(treeName)),distinctUntilChanged(),
        map(d => (<any>d).observable))

     
            
    }
    public removeState(capability, treeName) {

        var existingData = this.store$.getValue();
        var filteredData = existingData.filter(d => !(d.Capability == capability && d.Tree == treeName))
        this.store$.next(filteredData);
    }
    add(data) {
        var existingData = this.store$.getValue();
        existingData.push(data);
        this.store$.next(existingData);
    }

    public clearAllObservableStore(){
        this.store$ = new BehaviorSubject([]);
        var existingData = this.store$.getValue();
        if(existingData&&existingData.length){
            this.store$.next(existingData);
        }
    }
}

