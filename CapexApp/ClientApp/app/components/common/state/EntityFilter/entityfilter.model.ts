export interface EntityFilterState {
    schema: any;
    data: any;
    capability: string;
    treeName: string;
  }