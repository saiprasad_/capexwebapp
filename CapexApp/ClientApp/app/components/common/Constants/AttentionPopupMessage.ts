export const OPF_CONFIG_DATA_NOT_EXIST = 'Operational Form selected is not configured for this Location. Please contact Administrator.';
export const Mandatory_Message = `Mandatory fields have not been entered. Do you want to leave without saving changes?`;
export const ExpandCollapse_Message = `Mandatory fields have not been entered. Please complete the form or delete the FFV event.`;
export const DISCARD_CHANGES = `Are you sure you want to discard your changes?`;
export const SELECT_ENTITY = `Please Select Existing Project to Create Security Group!`;
export const DUPLICATE_DROP = 'This role is already added in the roles form.';

export const EMPTY_ROLE = `Role name cannot be empty. Please enter a valid role name`;
export const VALID_ROLE_NAME = `Please enter a valid role name`;
export const CANT_DRAG_FOLDER = `You cannot drag and drop folder on the form.`;
export const CANT_DRAG_ROLE = `You cannot drag and drop role on the form.`;
export const ALREADY_EXIST_ROLE = (msg: any) => `${msg} already exists Please enter a valid role name.`;
export const FORM_ALREADY_EXIST_ROLE = (msg: any) => `${msg} form already exists in role.`;
export const FORM_ARCHIVED = (msg: any) => `${msg} form will be archived and no longer visible.`;//archived?  or deleted? if its deleted then re-phrase
export const FORM_ARCHIVED2 = (msg: any) => `This ${msg} form will be archived and no longer visible. Would you like to proceed?`;//archived?  or deleted? if its deleted then re-phrase
export const CANT_DELETE = (msg: any) => `${msg} linked with another form. Cannot delete the ${msg}.`;
export const FORM_ARCHIVED3 = (msg1: any, msg2: any) => `${msg1} will be archived and no longer visible in ${msg2}`;//archived?  or deleted? if its deleted then re-phrase
export const CANT_DRAG_SECURITYGROUPS = `You cannot drag and drop Security Groups on the form.`;
export const SECURITYGROUPS_EMPTY_NAME = `Security Group name cannot be empty. Please enter a valid group name.`;
export const VALID_SECURITYGROUPS_NAME = `Please enter a valid group name.`;
export const ALREADY_EXIST_GROUP = (msg: any) => `${msg} already exists. Please enter a valid group name.`;
export const ALREADY_EXIST = (msg1: any, msg2: any) => `${msg1} already exists in ${msg2} form.`;
export const SOME_ALREADY_EXIST = (msg1: any, msg2: any) => `One or more of the ${msg1} already exists in ${msg2} `;
export const SELECT_CAPABILITY = `Please select an Application.`;
export const DOESNT_HAVE_SCHEMA = (msg: any) => `NOTE: ${msg} form does not have Schema.`;
export const CANT_MODIFY_GROUP = (msg: any) => `You do not have permission to modify ${msg} group.`;
export const CREATE_PERMISSION = `You do not have permission to create form. Please contact Administrator.`;
export const READ_PERMISSION = `You do not have permission to read form. Please contact Administrator.`;
export const READ_FORM_PERMISISON = `You do not have read permission. Please contact Administrator`;
export const DELETE_SECURITY_GROUP = (msg: any) => `Are you sure you want to delete ${msg} Security group?`;
export const DELETE_ROLE = (msg: any) => `Are you sure you want to delete ${msg} Role?`;
export const DELETE_FORM = (msg:any) => `Are you sure you want to delete ${msg} Form?`;
export const ENTITY_EXIST = `Entity already exists in the list`;
export const LIST_NAME_EXISTS = `List name already exists. Please use a unique name.`;
export const SAVE_LISTFROM_IF_LIST_MODIFIED = `Changes has been made to the list, Please save the changes to continue.`;
export const MISSING_OR_NON_VALID_DATA = `There is missing or invalid data input. Please review mandatory fields in red.`;
export const STRAPPING_TABLE_VOLUME_EXCEED_TANKSIZE_MSG = `Strapping table volume should not exceed the tank size`;
export const PERSON_EMAIL_ADDRESS_ALREADY_EXIST = `This Email address is already is in use. Please use a unique Email address to continue.`;
export const FOUR_CHARACTERS_REQUIRED = `Minimum four characters are required to search.`;
export const LOGOUT = `Are you sure you want to Logout?`;
export const OFFLINE_WARNING = `You are currently offline. This function is not available offline.`;
export const ENTITY_NOT_EXIST = (msg: any) => `Entity is not enabled in ${msg} Capabillity`;
export const CANT_ACCESS_CAPABILITY = (msg: any) => `You do not have access to open this Application. ${msg}`;
export const ANALYSIS_CONFIGURED_ORIFICE = `Analysis not configured for this Orifice Meter, volume will not calculate. Please contact Administrator.`;
export const ANALYSIS_CONFIGURED_PDMETER = `Analysis not configured for this PD Meter, volume will not calculate. Please contact Administrator.`;
export const FILL_LIST_NAME = `Please populate list name to create a list`;
export const Facility_Code_Delete = `If you want to see updated changes, please logout and login.`;
export const FORM_ARCHIVED_WELLS_DOWNTIME = `This wells downtime will be archived and no longer visible.`;//archived?  or deleted? if its deleted then re-phrase
export const ALREADY_EXIST2 = (msg1: any, msg2: any) => `"${msg1}" ${msg2} already exists.`;
export const ALREADY_EXIST3 = (msg1: any, msg2: any) => `${msg1} "${msg2}" already exists.`;
export const SURE_WANT_CHANGE = (msg1: any) => `Are you sure you want to change ${msg1}?`;
export const START_DATE_LESS_THAN_30DAYS = `The start date must be 30 days less than the end date.`;
export const END_DATE_MORE_THAN_30DAYS = ` The end date must be 30 days greater then the start date.`;
export const ONLY_NUMBERS = `Alpha and special characters are not accepted in this field. Only numbers are allowed.`;
export const TEN_DIGIT_NUMBERS = `Phone number should consist of 10 digits`;
export const VALIDATION_FAILED = `There is missing or invalid data input. Please review mandatory fields in red.`;
export const SOMETHING_WRONG = `An issue has occured. If it continues please contact Administrator.`;
export const CONTACT_SUPPORT = `Contact support.`;
export const CHAT_NO_LONGER_VISIBLE = `This chat message will no longer be visible.`;
export const DOWNTIME_USED_CANT_REMOVED = `This Downtime Reason has been used. It cannot be deleted but, can be made inactive`;
export const FACILITYCODE_USED_CANT_REMOVED = `This Facility Code has been used. It cannot be deleted, but can be replaced.`;//you can amend/edit me
export const DOWNTIME_NEVER_USED = `This Downtime Reason has never been used. It will be deleted, would you like to proceed?`;
export const FACILITYCODE_NEVER_USED = `This Facility Code has never been used. It will be deleted, would you like to proceed?`;
export const STATUS_USED_CANT_REMOVED = `This Status has been used. It cannot be deleted but, can be made inactive`;
export const STATUS_NEVER_USED_CANT_REMOVED = `This Status has never been used. It will be deleted, would you like to proceed?`;
export const FORMATIONS_USED_CANT_REMOVED = `This Formation has been used. It cannot be deleted but, can be made Inactive`;
export const FORMATIONS_NEVER_USED_CAN_REMOVED = `This Formation has never been used. It will be deleted, would you like to proceed?`;
export const DELETE_MULTIPLE_CAPABILITIES = `You are about to delete an entity which is used in multiple Applications. Deleting it here will remove it from all Applicaitons, would you like to proceed?`;
export const SELECT_CHECK_MARK = `Please select the check mark if you wish to proceed.`;
export const SURE_CLOSE_DASHBOARD = `Are you sure you want to close existing dashboard?`;
export const DASHBOARD_EXISTS = (msg1: any) => `${msg1} Dashboard already exists.`;
export const DELETE_ENTITY_PROCEED = `You are about to delete a run, would you like to proceed?`;
export const DELETE_ENTITY_PLUS_ALL_CHILD = `You're about to delete  a run, this action will also delete all of the child entities, would you like to proceed?`;
export const DATA_LOCK_DATE_VISUR_START_DATE = (msg: any) => `Entry is locked down for this date - ${msg}.<br/>Contact Administrator to unlock this date.`;
export const DATA_LOCK_DATE_VISUR_START_DATE1 = (msg: any) => `You are in ${msg}, entity card is not editable. If you want to edit, please update ${msg} from PM Configuration.`;
export const DATA_LOCK_DATE_VISUR_START_DATE2 = (msg: any) => `You are in ${msg}, entity card is not editable. If you want to edit, please contact Administrator.`;
export const STATUSES_DISABLE_POPUP = (msg1: any, msg2: any) => `This ${msg1} is currently ${msg2}`;
export const DATA_LOCK_DATE_VISUR_START_DATE_NONADMIN = (msg: any) => `Entry is locked down for this date - ${msg}.<br/>Contact Administrator to unlock this date.`;
export const DATA_LOCK_DATE_VISUR_START_DATE_ADMIN = (msg: any) => `Entry is locked down for this date - ${msg}.<br/>If you want to edit, please update ${msg} from PM Configuration.`;

export const ALREADY_EXISTS = (msg1: any) => `This ${msg1} already exists.`;
export const NOT_ALLOWED_LISTFORM = (msg1: any) => `${msg1} Entity type  is not allowed in List form.`;
export const SOME_ENTITIES_ALREADY_EXIST = `Some Entities are already exists..`;//not sure what this is?
export const MICROPHONE_ACCESS = `Grant access to microphone in current browser and try again.`;
export const DASHBOARD_UNSUCCESSFULL = `Dashboard copy unsuccessful. Please contact Administrator.`;
export const TRY_AFTER_SOMETIME = `An issue has occured. If it continues please contact Administrator.`;
export const CHART_TYPE = `Please select a Chart Type to proceed.`;
export const DATA_SOURCE = `Please select a Data Source to proceed.`;
export const SELECTALL_INPUT_FIELDS = `Select all Input Fields.`;
export const DATA_FOR_GIVEN_DATE_NOT_EXIST = `The data for the given date does not exist.`;//this one need a re-word - for one why does it even happen?
export const EM_MUTUAL_FABRIC_DELETE_MSG = `Please select any additional applications from which you would like to delete this location`;
export const DELETE_LOCATION_PRODUCTION_HISTORY_EQUIPMENT_WILL_DELETE = `You have chosen to delete a location. All production history and equipment will be deleted, would you like to proceed?`;
export const DELETE_FACILITYCODE_TREE = `You have chosen to delete a Facility Code. All production history will be deleted, would you like to proceed?`;
// export const DELETE_LOCATION_WITH_LINKED_LOCATIONS = `You have chosen to delete a location with linked upstream locations.`;
export const DELETE_LOCATION_WITH_LINKED_LOCATIONS = `You have chosen to delete a location with linked upstream locations. All linked locations will become un-connected.`;// sub message- Do you want to re-connect to another location?.
export const DELETE_PEOPLE_WITH_OFFICES_PERSONS = `You have chosen to delete a Company that contain People.People will be deleted, would you like to proceed?`;// is this deleting a company?
export const DELETE_PEOPLE_WITH_PERSONS = `You have chosen to delete an Office that contain People. These People will be deleted, would you like to proceed?`;
export const DELETE_FACILITYCODE_WITH_LINKED_FACILITYCODES = `You have chosen to delete a Facility Code with linked Facility Codes.`;
export const SHOW_DOWNSTREAMLOCATION_UNDER_POPUP_MESSAGE = `Upstream/DownStream Locations will not flow anywhere.`;//not sure where this occurs. but we should discuss
export const SHOW_NESTEDUNDER_FACILITYCODE_POPUP_MESSAGE = `Nested under Facility Code will not flow anywhere.`;//not sure where this occurs. but we should discuss
export const IMPACT_PRODUCTION_DATA = `Production data may be impacted, would you like to proceed?`;
export const ENTITY_TYPE_USED_CAN_REPLACED = (msg1: any) => ` This ${msg1} is already in use and cannot be Inactivated. It can be replaced with another ${msg1} in PM DATA ENTRY and can attempt to Inactivate again.`;
export const ENTITY_TYPE_USED_CAN_DELETED = (msg1: any) => `This ${msg1} is already in use and cannot be Deleted. It can be replaced with another ${msg1} in PM DATA ENTRY and can attempt to Delete again`;
export const DOI_SHOULD_100 = `Division of Interest total must equal 100%, please validate.`;
export const TICKET_NUMBER_ALREADY_EXIST = `Ticket Number already exists.`;
export const LOCATION_CREATING_LOOP = `The location is creating a loop.`;
export const CHANGE_DOWNSTREAM_LOCATION = `You are about to change the existing Downstream Location, would you like to proceed?`;
export const SAND_CANT_GREATER_BSW = `Sand % cannot be greater than BSW %.`;
export const TANK_SIZE_CANT_LESS_TOTAL_VOLUME = `Tank size cannot be less than Total Volume.`;// I assume this is for strapping tables?
export const TURBINE_PRODUCING_TO_TANK_EXCEEDS_TANKSIZE = "Metered volume is set 'To Tank'. Total tank volume is exceeding the tank size.";
export const GAUGE_GREATER_THAN_STRAP = "Your entered gauge is greater than the max strapping table level.";
export const CORIOLIS_PRODUCING_TO_TANK_EXCEEDS_TANKSIZE = "Metered volume is set 'To Tank'. Total tank volume is exceeding the tank size.";
export const ERROR_OCCURRED_MESSAGE = 'An issue has occurred, please refresh your browser.<br/>If it continues please contact Administrator.';
export const CONTENT_MESSAGE = "You have chosen to delete an entity used in multiple applications. Deleting here will remove it from this application but will remain in others. Delete in Entity Management to delete entirely.";
export const DISABLE_EQUIPMENT = "You have chosen to disable an equipment.it will remove from this location.";
export const MESSAGE_LINKED_WITH_EQUIPMENT = "This Tank has meters linked to it in data entry. You cannot delete.";//is this trying to delete a tank that has a meter set "totank"?
export const MESSAGE_LINKED_WITH_TEST = "This Vessel is linked with Test Equipment. You cannot update.";// not sure what this is but needs rewording
export const MESSAGE_CHANGE_METERPURPOSE_VESSELINK = "This Meter Purpose Vessel Link option not available. You can not update";// not sure what this is but needs rewording
export const Strapping_Table_MESSAGE = "Please Upload Strapping Table File";
export const CANT_DROP_SAME_SAMP = "Cannot use same Sample Point Code analysis";// not sure what this is but needs rewording
export const DROP_REASONS = "First drop Available Downtime Reasons";// not sure what this is but needs rewording
export const DOWNTIME_REASON_DRAG_ACCORDION_MSG = "Available Downtime Reason must be selected and added to Current Used Downtime Reasons.";// not sure what this is but needs rewording
export const REASONS_MUST_BE_SELECTED_TO_DROP_LOCATION = "Must drag and drop selected Well within the existing Downtime Reason accordion.";// not sure what this is but needs rewording
export const REASON_ARCHIVED = "This Downtime Reason will be archived and no longer visible!";//archived?  or deleted? if its deleted then re-phrase
export const EXIT_WITHOUT_CHANGES_ALERT_IN_FDC = "Downtime changes were made. Do you want to leave without saving changes?";
export const EXIT_WITHOUT_CHANGES_ALERT_IN_TEST = "Well Test changes were made. Do you want to leave without saving changes?";
export const SCHEDULE_DATE_CANT_LESSER_CURRENT_DATE = "Scheduled date & time should not be less than current date & time";
export const TEST_FOR_SAME_VESSELS = 'Test vessel cannot be used more than 24 hours on the same production day.';
export const NEXT_DAY_TEST_FOR_SAME_VESSELS = (hours: any) => `For same Vessels next day test Available. Not allow ${hours} hour!`;// not sure what this is but needs rewording
export const ALERT_SELECTED_WELL = 'Attention! Selected well is currently on test.';
export const ALERT_SELECTED_TEST_Vessel = 'Attention! Selected Test Vessel is currently in use.';
export const CURRENT_TEST_EXCEEDS = 'Attention! Current test exceeds test threshold set for this well.';
export const INPUT_FFV_GROUP = "Please enter a FFV Group Name.";
export const ASSIGN_FFV_EVENTS = "Please select FFV events to publish.";
export const SELECT_LOCATIONS = "Please select the Locations you want the events published to."
export const LONGITUDE_MUST_NUMERIC = 'Longitude must be a numeric value between -180 and 180.';
export const LATITUDE_MUST_NUMERIC = 'Latitude must be a numeric value between -85 and 85.';
export const VALID_LONGITUDE = 'Enter valid Longitude value.';
export const INVALID_LONGITUDE = 'Invalid Longitude value.';
export const VALID_LATITUDE = 'Enter valid Latitude value.';
export const INVALID_LATITUDE = 'Invalid Latitude value.';
export const TOTAL_VOLUME_CROSSING_TANK_STRAP_VOLUME = "TodayTotalVolume is crossing tank strapping table volume in case of CalculateTankForProducingTo while coriolisis or turbine passing data to tank";// not sure what this is but needs rewording
export const DELETED_CONTENT = "You cannot open the link as content has been deleted";// not sure what this is but needs rewording
export const ANALYTICS_AVAILABLE_MESSAGE = "You are currently Offline. Analytics are only available when Online.";
export const CONFIGURATION_AVAILABLE_MESSAGE = "You are currently Offline. Configuration is only available when Online.";
export const LOADFLUID_AVAILABLE_MESSAGE = "You are currently Offline. Load Fluid only available when Online.";
export const PHYSICAL_FLOW_AVAILABLE_MESSAGE = "You are currently Offline. Physical Flow is only available when Online.";
export const ACTIVE_GOR_VALUE_GREATER_ON_WELL = (msg: any) => `Cannot create an ${msg} . An active GOR factor is currently on this well.`;
export const EFM_TOGGLE_OFF_MESSAGE = (name: any) => `Unable to edit. ${name} Volume can only be changed if EFM toggle is 'On'. Please contact Administrator.`;
export const EFM_TOGGLE_ON_MESSAGE = (name: any) => `Unable to edit. ${name} Value can only be changed if EFM toggle is 'Off', please contact Administrator.`;
export const LINKED_VESSEL_MESSAGE = (type: any) => `An active ${type} is currently linked to this vessel`;
export const DOI_RECORD_ARCHIVED_MESSAGE = "You have chosen to delete a DOI Record, would you like to proceed?";
export const DELETE_SOURCE_SYSTEM_MESSAGE = "You have chosen to delete a Source System, would you like to proceed?";
export const DELETE_RECORD_MESSAGE = "You have chosen to delete a Record, would you like to proceed?";// not sure what this is
export const DELETE_ENTITY = "You have chosen to delete an Entity, would you like to proceed?";
export const TENANT_COMPANY_DELETE_MESSAGE = "You are attempting to delete a tenant company. This is not allowed. Please contact your administrator for more information. ";

export const TOTALIZER_TODAYREADING_YESTERDAYREADING = "This meter is set as a Totalizer. Today reading is less than Yesterday reading. Please address before moving forward";
export const DELETE_LINKED_EQUIPMENT_VESSEL_MESSAGE = "You have chosen to delete a Vessel with linked equipment. All equipment will be unlinked, would you like to proceed?";
export const DELETE_ALREADY_LINKED_TANK = "This Tank currently has meters linked to it. You can not delete";//same as 100?
export const VISUR_SOURCE_TOGGLE_OFF_MESSAGE = "Visur Source System toggle is 'OFF', please contact Administrator !";// not sure what this is but needs rewording
export const ALLOCATE_MESSAGE = "Do you want to Allocate now?";
export const FILE_NOT_SUPPORTED_MESSAGE = "File not supported. Please select a .CSV format file";
export const RECOVERED_LESSER_MESSAGE = "Recovered volume cannot be greater than yesterday's remaining volume.";
export const EXIT_WITHOUT_CHANGES_MESSAGE = (type: any) => `${type} changes were made. Do you want to leave without saving changes?`;
export const UNIQUE_SOURCE_ID_MESSAGE = "Source System Id must be unique";
export const ALREADY_EXIST_MESSAGE = (orific: any) => `"${orific}" Effective Date already exists`;
export const EC_FACTOR_LESSER_EQUAL = "EC Factor must be less than or equal to 1.00000";// we have to remove this validation for now!! 
export const GAS_COMPOSITIONS_MESSAGE = "Composition total does not = 100 (1.00000). Please validate.";
export const CHANGE_ENTITY_TYPE_MESSAGE = "You have chosen to change Entity Type, would you like to proceed?";
export const ACTIVE_LOCATIONS_FLOW_MESSAGE = (name: any, status: string) => ` ${name} has active locations linked to it. Location cannot be ${status}.`;
export const HAUL_LOCATION_CANT_SET_MESSAGE = (name: any) => ` ${name} has active locations linked to it. Haul Location cannot be set to "On".`;
export const DOWNSTREAM_CANT_SET_MESSAGE = "Circular Flow Error - You cannot set this downstream location as it will create a circular flow loop. Please edit the flow path of the appropriate upstream location prior to this change.";
export const VOLUME_CANT_GREATER_TANKSIZE = "Total Volume cannot be greater than Tank Size.";
export const GAUGE_CANT_EXCEEDS_STRAPPING_TABLE = "Gauge cannot exceed strapping table maximum increment value.";//same as 96?
export const HEALER_MESSAGE_MULTIDATEVIEW = "This is a future enhancement! <br/><br/> The purpose of this button will be to assist you with auto correcting negative production influenced by truck tickets and tank gauges as well as BSW% <br/><br/> It will be available if the button font is Red indicating it is ready to assist you!";
export const LEAVE_WITHOUT_COMMIT_MESSAGE = 'Mandatory fields have not been populated. <br/> Do you want to leave without saving changes?';//same as 1?
export const EDIT_CONFIGURATION_VALUE_MESSAGE = "You have chosen to edit a configuration value with optional date effectiveness, would you like to proceed?";
export const EDIT_VESSELS_LOCATION_VALUE_MESSAGE = "Would you like to select a NEW location for this Portable Separator?";
export const EDIT_CONFIGURATION_VALUE_APPLY_CREATION_UTIL_MESSAGE = "You have chosen to edit a configuration value that will apply back to creation until to current day, would you like to proceed?";
export const DISABLE_DATA_LOCK_DATE_MESSAGE = "The effective date of your request is prior to the data lock date. If you want to edit, please disable lock date.";
export const CONTEXT_MENU_ACTIVE_MESSAGE = "You are currently Offline. Context Menu is only available when Online.";
export const OFFLINE_ACTIVE_MESSAGE = "You are currently Offline. Please check your Internet connection and try again.";
export const CONFIG_FORMS_SHOW_MESSAGE = "You are currently Offline. Configuration  is only available when Online.";
export const D13_FORM_CREATE_MESSAGE = 'D13 form can only be created on a well type location.';
export const DROP_DIFFERENT_FORM_MESSAGE = (name: any) => ` ${name} already available, please drop different form.`;
export const ALREADY_EXIST_MESSAGE2 = (name: any) => `This " ${name} " already exists in Available Location.`;
export const SCHEDULE_COMPONENT_CHANGES_MESSAGE = "Schedule Component changes were made. Do you want to leave without saving changes?";
export const SCHEDULE_EXIST_MESSAGE = (name: any) => ` ${name} schedule already exists, please enter a different name.`;
export const SELECT_EQUIPMENT_MESSAGE = "Please select the Equipment to Publish Schedule.";
export const FORMS_NOT_AVAILABLE_MESSAGE = "Forms are not available for creation.";// not sure what this is but needs rewording
export const SESSION_EXPIRE_MESSAGE = "Your web session has expired. Please refresh your browser to resume working.";
export const SELECT_ENTITY_TYPE_MESSAGE = "Please select a valid Entity Type.";
export const ENTER_FIELDS_MESSAGE = "There is missing or invalid data input. Please review mandatory fields.";//same as 28?
export const DELETE_ENTITY_MESSAGE_LIST = "Are you sure you want to delete this entity from the list?";
export const DELETE_ENTITITIES_MESSAGE_LIST = "Are you sure you want to delete these entities from the list?";
export const LOADING_MESSAGE = "Please wait. Data is currently loading.";
export const FILL_REQUIRED_FIELDS = "There is missing or invalid data input. Please review mandatory fields.";// same as 28?
export const VALID_COMPANY_NAME = "Please enter valid Company Name.";
export const DESKTOP_SESSION_EXPIRE_MESSAGE = "Your Desktop Appliation session has expired. Please Log in again."
export const VALID_OTP = "Please enter valid Verification Code.";
export const VALID_PASSWORD = "Please enter valid Password.";
export const VALID_EMAIL = "Please enter valid Email address.";
export const VALID_USERNAME = "Please enter valid Email address.";//same as 179
export const SENT_VERIFICATION_CODE_MESSAGE = "Verification code has been sent.";
export const VALUE_SET_SQL_LITE = ": this value need to set in sql-lite and remove username, companyname and password stored data from sql-lite";// not sure what this
export const SENT_LINK_RESET_PASSWORD = "A password link was sent to your email.";
export const REMOVE_SELECTED_PEOPLE_ENTITIES = "Are you sure you want to remove all selected people entities?";// not sure what this
export const REMOVE_PEOPLE_ENTITIES = (name: any) => `Are you sure you want to remove ${name} people entity?`;// not sure what this
export const REMOVE_ENTITY1 = (name: any) => `Are you sure you want to remove ${name} entity?`;// not sure what this
export const REMOVE_ROLE = (name: any) => `Are you sure you want to remove ${name} role?`// not sure what this
export const REMOVE_ENTITY = (name: any, type: any, cap: any) => `Are you sure you want to remove "${name}"  ${type} from ${cap} ? `;// not sure what this
export const RESTORE_ENTITY = (name: any, type: any, cap: any) => `Are you sure do you want to restore "${name}" ${type} from ${cap} ?`;// not sure what this
export const REMOVE_ALL_SELECTED_ENTITIES = "Are you sure you want to remove all selected entities?";
export const REMOVE_SELECTED_ROLES = "Are you sure you want to remove all selected roles?";
export const CONTEXT_MENU = 'Security Groups Menu';
export const NAME_CANT_NULL = 'Name cannot be null';
export const FIELD_EXIST = 'Field already exists';
export const PRODUCTION_CAPABILITY = 'Production Management Application ';
export const OPERATIONAL_CAPABILITY = 'Operational Forms Application ';
export const DELETE_FFV_EVENT = 'Are you sure you want to delete FFV Event?';
export const VOLUME_REASONS = "Volume Override Reasons";
export const USE_PLUS_ICON_TO_ADD = (data: any) => `${data} are already available, Please use plus icon (+) to add more forms.`;
export const CANT_HAVE_MORE_PUMP = "You cannot have more than one pump with the same installation date";
export const MOVE_DELETE_DASHBOARDS = "you must move or delete dashboards prior to deletetion";// not sure what this
export const NO_LONGER_AVAILABLE = (child: any) => ` ${child} will be deleted and no longer be available, would you like to proceed?`;
export const CONTAIN_DASHBOARDS = (child: any) => ` ${child} may contain dashboards, would you like to proceed?`;
export const CONTAIN_MAPS = (child: any) => ` ${child} may contain maps, would you like to proceed?`;
export const CONFIGURING_DATA_SOURCE = "Please wait. Configuring DataSource and Charts.";
export const SELECT_TANK_UOM = "Please select Tank UOM before selecting Tank Size.";
export const FORMATING_BASED_ON_PARENT_TIMEZONE = "Formating datetime based on parent timezone";
export const AFTER_ADDING_ZERO = (result: any) => `after adding zero to Due Date ${result}`;
export const INSERTING_CONFIG_DATA = (id: any, type: any, day: any, name: any) => `INSERTING CONFIG DATA INTO SQLLITE DB... ENTITYID= ${id}. ENTITYTYPE = ${type}. PRODUCTION DAY=${day}. TABLE NAME =${name}`;
export const CONFIG_DATA_INSERTED = (id: any, type: any, day: any, name: any) => `CONFIG DATA INSERTED INTO SQLLITE DB SUCCESSFULLY... ENTITYID= ${id}. ENTITYTYPE = ${type}. PRODUCTION DAY=${day}. TABLE NAME =${name}`;
export const INSERTING_PRODUCTION_DATA = (id: any, type: any, day: any, name: any) => `INSERTING PRODUCTION DATA INTO SQLLITE DB... ENTITYID= ${id}. ENTITYTYPE = ${type}. PRODUCTION DAY=${day}. TABLE NAME =${name}`;
export const PRODUCTION_DATA_INSERTED = (id: any, type: any, day: any, name: any) => `PRODUCTION DATA INSERTED INTO SQLLITE DB SUCCESSFULLY... ENTITYID= ${id}. ENTITYTYPE = ${type}. PRODUCTION DAY=${day}. TABLE NAME =${name}`;
export const INSERTING_PRODUCT_TOLARENCE = (id: any, type: any) => `INSERTING PRODUCT TOLARENCE DATA INTO SQLLITE DB... ENTITYID= ${id}. ENTITYTYPE = ${type}`;
export const DEFAULT_RECORD_INSERTED_FDCPDMETERINFO = "inserted or updated default record FDCPDMeterInfo";
export const DEFAULT_RECORD_INSERTED_FDCORIFICEMETERINFO = "inserted or updated default record FDCOrificeMeterInfo";
export const PRODUCT_TOLARENCE_INSERTED = (id: any, type: any, products: any) => `INSERTED PRODUCT TOLARENCE DATA SUCCESSFULLY...ENTITYID= ${id}. ENTITYTYPE = ${type}. PRODUCTS = ${products}`;
export const PIG_CONFIG_DATA_INSERTING = (id: any, type: any, day: any, name: any) => `INSERTING PIG CONFIG DATA INTO SQLLITE DB... ENTITYID= ${id}. ENTITYTYPE = ${type}. PRODUCTION DAY=${day}. TABLE NAME =${name}`;
export const AUTO_SYNC_FOR_CREATE = (type: any, day: any) => `AUTO SYNC FOR CREATE ENTITIES EXECUTING... ENTITYTYPE = ${type} . PRODUCTION DAY = ${day}`;
export const AFTER_ASSIGN_CHILDRENNODES = "after assign childrennodes to array list ====";
export const FIRST_JSONDATAVALUE = "first jsonDataValue array event===";
export const FINAL_ASSIGNMENT = "final assignment parent children events ====";
export const CREATE_EMPTY_MODEL = "Data is not there so, creating empty model";
export const FIELD_ID_IN_BIND_DATA = "FIELD ID IN BIND HIERARCHIE DATA :";
export const DISPLAY_RECORDS = "display the produtionday recordssss";
export const DISPLAY_GLOBAL_ADMIN_DATA = "Display the global admin data for unitodmeserments====";
export const UPDATING_SETTING_DATA = "UPDATING GLOBAL SETTINGS  DATA...";
export const UPDATING_ADMIN_DATA = "UPDATING GLOBAL ADMIN DATA...";
export const SETTINGS_DATA_UPDATED_SUCCESSFULLY = "UPDATED GLOBAL SETTINGS DATA SUCCESSFULLY...";
export const ADMIN_DATA_UPDATED_SUCCESSFULLY = "UPDATED GLOBAL ADMIN DATA SUCCESSFULLY...";
export const UPDATING_DOWNTIME_DATA = "UPDATING DOWNTIE REASONS  DATA...";
export const DOWNTIME_DATA_UPDATED_SUCCESSFULLY = "UPDATED DOWNTIME REASONS DATA SUCCESSFULLY...";
export const UPDATING_INTO_SQLLITEDB = "UPDATING DATA INTO SQLLITEDB... TYPE : ";
export const UPDATED_SUCCESSFULLY_INTO_SQLLITEDB = "DATA UPDATED SUCCESSFULLY INTO SQLLITEDB. TYPE : ";
export const ERROR_MSG_FOR_UPDATE = "Error message return for update";
export const UPDATE_SYNCOFFLINEDATA = "came to Update syncOfflineData";
export const INSERTING_CURRENT_DOWNTIME_IN_SQLLITEDB = "INSERTING CURRENT USED DOWNTIME REASONS INTO SQLLITE DB...";
export const DOWNTIME_REASONS_INSERTED_SUCCESSFULLY = "CURRENT USED DOWNTIME REASONS INSERTED INTO SQLLITE DB SUCCESSFULLY.";
export const INSERTING_ANALYSIS_DATA_INTO_SQLLITEDB = "INSERTING CURRENT USED ANALYSIS DATA INTO SQLLITE DB...";
export const ANALYSIS_DATA_INSERTED_SUCCESSFULLY = "CURRENT USED ANALYSIS DATA INSERTED INTO SQLLITE DB SUCCESSFULLY.";
export const INSERTING_STRAPPING_TABLE_DATA = "INSERTING STRAPPING TABLE DATA INTO SQLLITE DB...";
export const STRAPPING_TABLE_DATA_INSERTED_SUCCESSFULLY = "INSERTED STRAPPING TABLE DATA INTO SQLLITE DB SUCCESSFULLY...";
export const INSERTING_TRUCK_TICKET_LIST = "INSERTING TRUCK TICKET LIST DATA INTO SQLLITE DB...";
export const TRUCK_TICKET_LIST_INSERTED_SUCCESSFULLY = "TRUCK TICKET LIST DATA INSERTED INTO SQLLITE DB SUCCESSFULLY.";
export const DELETED_NOTSYNCED_ENTITIES = "deleted not synced entities from there for";
export const DATA_READ_FROM = "data read from TruckTicketUpdateFormatData";
export const CREATE_NOT_SYNCED_ENTITY = "Created not synced entities";
export const FINAL_DATA_MODEL = "final data model in read Updated genral entities 1234567 ====>";
export const RETRY_AFTER = "retry for some time ............";// not sure what this is but needs rewording
export const OFFLINE_RESPONSE_COMING = "Offline response coming undefined or empty ";// not sure what this
export const NO_INTERNET = 'No Internet connection.';
export const TABLES_CREATED = "Created all tables successfully";
export const CHANNEL_NAME = "Enter a name for the Channel";
export const ENTITY_CARD_DOESNT_EXIST = (locationname: any) => `Analytics Entity Card does not exist for ${locationname} .`;
export const DATA_NOT_EXIST = 'The data for this location does not exist.';
export const STARTDATE_LESSER_ENDDATE = 'The start date cannot be before the end date.';
export const ENDDATE_GREATER_STARTDATE = 'The end date cannot be after the start date.';
export const MUST_SELECT_DOWNTIME_REASON = "Hours On is less than 24 Hours. You must select a Downtime Reason.";
export const WELL_CURRENTLY_SHUT = (name: any) => `You have production on a shut in ${name}. Would you like to proceed?`;
export const ENTITY_NOT_ALLOWD = (name: any) => `'${name}' Location is not allowed to be dropped in 'Downtime Reason' as entity already exists. `;//what is "dropped in"?
export const SUSPENDED_ABANDONED_INACTIVE_SOLD_NOT_ALLOWD =  `SUSPENDED, ABANDONED, INACTIVE and SOLD Locations are not allowed to be dropped in Downtime Reason. `;//what is "dropped in"?
export const ENTITY_NOT_ALLOWD2 = (type1: any, type2: any) => ` ${type1} is not allowed to be dropped in ${type2} .`//what is "dropped in"?
export const ANALYSIS_ARCHIVED = "You have chosen to delete this analysis, would you like to proceed?";
export const FROMDATE_CANT_GREATER_TODATE = (entity: any) => `From Date cannot be after the To Date for ${entity} .`;
export const FROMDATE_CANT_GREATER_TODATE2 = "From Date cannot be after the To Date."
export const SAVE_WHEN_DRAG_DROP = (name: any) => `You can save only when locations have been dragged and dropped in ${name}`;
export const SAVE_WELL_WITH_VOLUMES_HOURS = 'You cannot have a well test with 0 volumes or 0 test hours.';
export const SURE_DELETE_RECORD = "You have chosen to delete a test record, would you like to proceed?";
export const SURE_REMOVE_ENTITY = (name: any) => `Are you sure you want to remove  ${name} from FFV Group form? `;
export const ALREADY_EXIST_INFFV = (name: any) => `This   ${name} already exist in FFV EVENT.`;// not sure what this is but needs rewording
export const STRAPPING_TABLE_NEEDED_LEVELS = "Strapping Table must contain a minimum of two records. Please validate.";
export const FILE_UPLOADING = "Please wait. File is uploading.";
export const UPDATE_EQUIPMENT_FACILITY_CODE = "Do you want to update Equipment Facility Code?";
export const VALUE_NOT_EXIST_In_ORIFICE_LIBRARY = `This value does not exist in the orifice library`;// not sure how to create this
export const VALUE_NOT_EXIST_In_TANK_LIBRARY = `This value does not exist in the tank library`;// not sure how to create this
export const FACILITYCODE_NESTEDUNDER_SAME_VALUE = 'Facility Code and Nested Under options are the same. Please select a different option.';
export const SWITCH_SHOW_MESSAGE = 'You are currently Offline. Switching users is only available when Online.';
export const IMPORT_DATA_DOESNT_EXIST = `Import Data functionality does not exist`;
export const DOWNSTREAM_CHANGE = `Do you wish to update Downstream Locations and Nested Under?`;
export const TRANSFERPERSON = `Do you wish to update People?`;
export const TRANSFEROFFICEPERSON = `Do you wish to update People?`;
export const NESTED_UNDER_CHANGE = `Do you wish to change Nested Under?`;
export const CHOOSE_ACTIVITY_UNSELECT = "Please Select Choose Activity";
export const IMPORT_FILE_HEADER_STATUS = "Import File does not contain Header.";
export const DISABLE_CAPABILITY = "You cannot disable current Application.";
export const DESKTOP_DELETE_SHOW_MESSAGE = 'You are currently Offline. Clearing Cache/Clear Data is only available when Online.';
export const SWITCH_ALERT_MESSAGE = 'This will remove the offline user account and its data cache. This operation can take some time, please ensure you have synchronized any changes.';// not sure what this
export const SWITCH_ALERT_HEADER_MESSAGE = 'Are you sure you want to switch user?';
export const NETWORK_CONNECTION_ESTABLISHED = 'CONNECTION ESTABLISHED';
export const INITIAL_OFFLINE_DATA_LOADING = "INITIAL OFFLINE DATA LOADING";
export const ONLINE_DATA_LOADING = "ONLINE DATA IS LOADING";
export const OFFLINE_DATA_LOADING = "OFFLINE DATA IS SYNCING";
export const DESKTOP_SYNC_STATUS: string = `THE DATA LOAD HAS BEEN SUCCESSFULLY COMPLETED!`;
export const SYSTEM_ONLINE = "SYSTEM IS ONLINE";
export const SYSTEM_OFFLINE = "SYSTEM IS OFFLINE";
export const ACTIVE_OFF_STATUS = (name: any) => `This ${name} cannot be Deactivate!. It is linked with some other form!`;// not sure what this
export const PURPOSE_OFF_STATUS = (name: any) => `This ${name} is used in another form. You cannot change the purpose.`;// not sure what this
export const DELETE_OFFLINE_DATA = "Are you sure you want to delete the offline data?";// not sure what this
export const DELETE_OFFLINE_DATA_CACHE = "This will remove the offline data cache. This operation can take some time, please ensure you have synchronized any changes.";
export const OFFLINE_DATA_REMOVED = "The offline data has been successfully removed.";
export const DO_NOT_DISCONNECT = `Do not disconnect until the initial load of offline data completes.`;
export const ATTENTION = `Attention`;
export const IP_ADDRESS_ERR = `Either your browser or AdBlock extension is not allowing the confirmation of your IP address. <br>
Please change settings and try again, or contact your Administrator.`;
export const VERIFYING_DEVICE = `VERIFYING DEVICE.`;
export const SUPPORT_MSG: string = `The verification code was sent to the phone<br> number associated with your account.<br> It may take a few minutes to receive your<br> verification code.<br><br> If you still experience difficulties with login,<br> please contact our <a href="https://visur.one/premium-support/" class="support" target="_blank">support</a> or use our Live Chat<br> located at the right lower corner of the screen.`;
export const REFRESH_URL_NOTFOUND = "The Requested URL is not found or The Requested Entity Data does not exist.";
export const WEB_LOGOUT = `Log Out`;
export const DESKTOP_LOGOUT = `Log Out / Switch User`;
export const DESKTOP_LOGOUT_HEADERTEXT = `Are you sure you want to Log Out / Switch User?`;
export const DESKTOP_LOGOUT_CONTENTTEXT = `This will remove the offline user account and its data cache. This operation can take some time, please ensure you have synchronized any changes.`;
export const DATA_REMOVED = `All data has been successfully removed.`;
export const REMOVING_USER_ACCOUNT = `Removing offline user account and it's data cache.`;
export const DOWNTIME_ALREADY_EXISTS = (msg1: any) => `This ${msg1} downtime already exists.`;
export const FACILITYCODE_HAULTOGGLE_CANT_SET_MESSAGE = (name: any) => `"Nested Under" For ${name} will be cleared and location moved under Haul Locations, Would you like to Proceed?`;
export const DELETE_PRODUCTION_RECORD = "Do you wish to delete Production Day Records?";
export const LIKE_TO_PROCEED = "Are you sure  you  would  like to proceed?";
export const RECALCULATE_PRODUCTION_RECORD = "Do you wish to recalculate production day records?";
export const DELETE_DENSITY_MESSAGE = "All truck tickets utilizing this density will be recalculated using the prior density on this location.Are you sure you want to delete this density record ?";
export const DELETE_F_R_RECORD_MESSAGE = "The tests and production days, utilizing the most current factor and ratio, will be recalculated using the prior factor and ratio record on this location. Are you sure you want to delete this factor and ratio?";
export const EXTERNAL_LINK_CANNOT_OPEN = 'You are currently Offline. An internet connection is required for online connectivity.';
export const DELETE_EM_EQUIPMENT = "You have chosen to delete an Equipment, would you like to proceed?";

export const FACILITYCODE_HAUL_LOCATION_CANT_SET_MESSAGE = (name: any) => ` ${name} has active Facility Codes linked to it. Haul Location cannot be set to "On".`;
export const PUMP_ACTIVE_STATUS = (name: any) => ` ${name} pump type is In Service, Please deactivate the existing pump !`;
export const FACTOR_RATIO_EFFECTIVEDATE_STATUS = (date: any) => `Factor and Ratio already exists for ${date}`;
export const PUMP_INSTALLDATE_STATUS = (date: any) => `Pump already exists for ${date}`;
export const EM_DISABLE_EQUIPMENT_ALERT = "You have chosen to disable this location. Existing equipment in this location on this capability will be inactivated."
export const EM_DISABLE_LOCATION = "Are you sure you want to disable this location?";
export const Hierarchy_CHANGE = (name: any) => `Do you wish to update ${name} ?`;
export const DELETE_HIERARCHY= (name: any) => ` You have chosen to delete a  ${name}. All childrens will be Deleted.`;

export const DELETE_EM_DISTRICT_HIERARCHY = "This District is using in Areas/Fields  and you cannot  delete until those fields are reassigned";
export const DELETE_EM_AREA_HIERARCHY = "This Area is using in Fields  and you cannot  delete until those fields are reassigned";
export const DELETE_EM_FIELD_HIERARCHY ="This Field is using in some EntityConfiguration  and you cannot  delete until those fields are reassigned";
export const DELETE_EM_HIERARCHY  = (name: any) => `This ${name} is not using anywhere, Are you sure you want to delete this hierarchy?`;
export const INSTALL_DATE_DATA_STATUS = (date: any) => `Data not exists for ${date}`;
export const Company_used_inTruckTicket= "This company has been used in historical truck tickets and cannot be disabled from trucking. For more information, please call support";


