export class ApplicationState {
    public firstName: string
    public lastName: string
    public email: string
    public fullName: string
    public userId: string
    public userName: string
    public companyId: string
    public companyName: string
    public projectName: string
    public projectId: string
    public projectUOM: string
    public tenantName: string
    public tenantId: string
    
    public constructor() {

    }
}