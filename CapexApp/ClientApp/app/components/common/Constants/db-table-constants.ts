// SQLite DataBase Name ::--
export const SQLiteDBName = 'VisurConstructionDB';

// Custom tables :;--
export const CommonFormsSchemaTable = 'CommonFormsSchema';
export const RightMenuTable = 'RightMenu';
export const RightSubMenuTable = 'RightSubMenu';
export const LeftMenuTable = 'LeftMenu';
export const LeftNavTable = 'LeftNav';
export const SignatureTable = 'signatures';
export const Icons = 'Icons';
export const RECENT_ACTIVITY = 'recentactivity';

export const LastSyncTable = 'LastSync';
export const UserTable = 'User';
export const UserSignaturesTable = 'UserSignatures';
export const UserDeviceInfoTable = 'UserDeviceInfo';
export const UserSetting = 'usersettings';
export const UserSettings = 'UserSettings_usersettings';
export const UserSettingsSync = 'UserSettings_SyncTimeList';
export const UserSettingsTable='usersettings';

export const Construction_Tree = "Construction_Tree ";
export const FilterTable = "filter";
export const Tree = "Tree ";
export const TreeTable = (treeName: string) => `${treeName}_${Tree}`;

export const Tasks_tasks: string = "Tasks_tasks";
export const CommonStaticTables: Array<string> = [CommonFormsSchemaTable, RightMenuTable, RightSubMenuTable, LeftMenuTable, LeftNavTable, LastSyncTable, Tasks_tasks]; //UserTable

//CommonFormsSchema Table Columns:-
export class CommonFormsSchemaColumns {
  public static readonly SchemaId: string = 'schemaid';
  public static readonly SchemaName: string = 'schemaname';
  public static readonly Schema: string = 'schema';
  public static readonly ModifiedDateTime: string = 'modifieddatetime';
  public static readonly Version: string = 'version';
  public static readonly PageType: string = 'pagetype';
  public static readonly CATEGORY_TYPE: string = 'categorytype';
  public static readonly FAVORITE: string = 'favorite';
  
}

export class SQLiteCommonColumns {
  public static readonly EntityId: string = 'entityid';
  public static readonly EntityName: string = 'entityname';
  public static readonly EntityType: string = 'entitytype';
  public static readonly Application: string = 'application';
  public static readonly Parent: string = 'parent';
  public static readonly ParentEntityId: string = 'parententityid';
  public static readonly UUID: string = 'uuid';
  public static readonly CreatedBy: string = 'createdby';
  public static readonly Payload: string = 'payload';
  public static readonly Image: string = 'image';
  public static readonly Thumbnail: string = 'thumbnail';
  public static readonly Altitude: string = 'altitude';
  public static readonly Latitude: string = 'latitude';
  public static readonly Longitude: string = 'longitude';
  public static readonly Type: string = 'type';
  public static readonly ModifiedBy: string = 'modifiedby';
  public static readonly TenantId: string = 'tenantid';
  public static readonly TenantName: string = 'tenantname';
  public static readonly ModifiedDateTime: string = 'modifieddatetime';
  public static readonly SyncDateTime: string = 'syncdatetime';
  public static readonly CreatedDateTime: string = 'createddatetime';
  public static readonly CreateStatus: string = 'createstatus';
  public static readonly UpdateStatus: string = 'updatestatus';
  public static readonly DeleteStatus: string = 'deletestatus';
  public static readonly SyncStatus: string = 'syncstatus';
  public static readonly IsDeleted: string = 'isdeleted';
  public static readonly EntityTimeZone: string = 'entitytimezone';
  public static readonly IconFileName: string = 'iconfilename';
  public static readonly ID: string = 'id';
  public static readonly SchemaPage: string = 'schemapage';
  public static readonly LeftMenu: string = 'leftmenu';
  public static readonly RightMenuID: string = 'rightmenuid';
  public static readonly UserName: string = 'username';
  public static readonly EmailId: string = 'emailid';
  public static readonly UserId: string = 'userid';
  public static readonly LogInTime: string = 'logintime';
  public static readonly DefaultColumnNames = [SQLiteCommonColumns.Parent, SQLiteCommonColumns.ParentEntityId, SQLiteCommonColumns.UUID, SQLiteCommonColumns.Payload, SQLiteCommonColumns.Type, SQLiteCommonColumns.TenantId, SQLiteCommonColumns.TenantName,SQLiteCommonColumns.CreatedBy, SQLiteCommonColumns.ModifiedBy, SQLiteCommonColumns.CreatedDateTime, SQLiteCommonColumns.ModifiedDateTime, SQLiteCommonColumns.EntityTimeZone, SQLiteCommonColumns.CreateStatus, SQLiteCommonColumns.UpdateStatus, SQLiteCommonColumns.DeleteStatus, SQLiteCommonColumns.SyncStatus ]

}

export class FilterColumns {
  public static readonly DataPointer: string = 'datapointer';
  public static readonly IsDefault: string = 'isdefault';
}

// RightMenu Table Columns :-
export class RightMenuColumns {
  public static readonly RightMenu: string = 'rightmenu';
  public static readonly Sequence: string = 'sequence';
  public static readonly ShowInMobile: string = 'showinmobile';
  public static readonly ShowInDesktop: string = 'showindesktop';
  public static readonly ShowInWeb: string = 'showinweb';
}

// RightSubMenu Table Columns :-
export class RightSubMenuColumns {
  public static readonly SubMenu: string = 'submenu';
}

// LeftMenu Table Columns :-
export class LeftMenuColumns {
  public static readonly RightSubMenuID: string = 'rightsubmenuid';
  public static readonly Navigation: string = 'navigation';
}

export class LeftNavColumns {
  public static readonly SortOrder: string = 'sortorder';
  public static readonly PageParameter: string = 'pageparameter';
  public static readonly Depth: string = 'depth';
  public static readonly MenuId: string = 'menuid';
}

export class TreeTableColumns {
  public static readonly NestedUnder: string = 'nestedunder';
  public static readonly Child: string = 'child';

}

export class LastSyncColumns {
  public static readonly UUID: string = 'uuid';
  public static readonly LastSyncTime: string = 'lastsynctime';
  public static readonly OperationType: string = 'operationtype';
  public static readonly SyncStartTime: string = 'syncstarttime';
  public static readonly SyncEndTime: string = 'syncendtime';
  public static readonly LastFileVersion: string = 'lastfileversion';
}

export class UserTableColumns {
  public static readonly UserName: string = 'username';
  public static readonly TenantName: string = 'tenantname';
  public static readonly TenantId: string = 'tenantid';
  public static readonly Password: string = 'password';
  public static readonly EmailId: string = 'emailid';
  public static readonly UserId: string = 'userid';
  public static readonly IsChangePassword: string = 'ischangepassword';
  public static readonly CreatePassword: string = 'createpassword';
  public static readonly ConfirmPassword: string = 'confirmpassword';
  public static readonly Domain: string = 'domain';
  public static readonly Rpotpcode: string = 'rpotpcode';
  public static readonly Otpcode: string = 'otpcode';
  public static readonly DeviceDetail: string = 'devicedetail';
  public static readonly Token: string = 'token';
  public static readonly Url: string = 'url';
  public static readonly LoginTime: string = 'logintime';
}


//create table query for common columns
export const CreateTableQuery = (tableName: string) => `create table if not exists ${tableName}(${SQLiteCommonColumns.UUID} varchar(255) not null primary key, ${SQLiteCommonColumns.Payload} varchar, ${SQLiteCommonColumns.Type} varchar(255), ${SQLiteCommonColumns.TenantId} varchar(255), ${SQLiteCommonColumns.TenantName} varchar(255), ${SQLiteCommonColumns.CreatedBy} varchar(255), ${SQLiteCommonColumns.ModifiedBy} varchar(255), ${SQLiteCommonColumns.ModifiedDateTime} varchar(255), ${SQLiteCommonColumns.CreatedDateTime} varchar(255), ${SQLiteCommonColumns.EntityTimeZone} varchar(255), ${SQLiteCommonColumns.CreateStatus} varchar(255), ${SQLiteCommonColumns.UpdateStatus} varchar(255), ${SQLiteCommonColumns.DeleteStatus} varchar(255), ${SQLiteCommonColumns.SyncStatus} varchar(255));`;

//create table query for common columns with Parent
export const CreateTableQueryWithParent = `(${SQLiteCommonColumns.Parent} varchar(255), ${SQLiteCommonColumns.ParentEntityId} varchar(255), ${SQLiteCommonColumns.UUID} varchar(255) not null primary key, ${SQLiteCommonColumns.Payload} varchar, ${SQLiteCommonColumns.Type} varchar(255), ${SQLiteCommonColumns.TenantId} varchar(255), ${SQLiteCommonColumns.TenantName} varchar(255), ${SQLiteCommonColumns.CreatedBy} varchar(255), ${SQLiteCommonColumns.ModifiedBy} varchar(255), ${SQLiteCommonColumns.ModifiedDateTime} varchar(255), ${SQLiteCommonColumns.CreatedDateTime} varchar(255), ${SQLiteCommonColumns.SyncDateTime} varchar(255), ${SQLiteCommonColumns.EntityTimeZone} varchar(255), ${SQLiteCommonColumns.IsDeleted} varchar(255), ${SQLiteCommonColumns.CreateStatus} varchar(255), ${SQLiteCommonColumns.UpdateStatus} varchar(255), ${SQLiteCommonColumns.DeleteStatus} varchar(255), ${SQLiteCommonColumns.SyncStatus} varchar(255))`;


export const CreateTreeTableQuery =  `(${SQLiteCommonColumns.EntityId} varchar(255) not null primary key, ${SQLiteCommonColumns.EntityName} varchar(255), ${SQLiteCommonColumns.EntityType} varchar(255), ${TreeTableColumns.NestedUnder} varchar(255), ${TreeTableColumns.Child} varchar(255), ${SQLiteCommonColumns.CreateStatus} varchar(255), ${SQLiteCommonColumns.UpdateStatus} varchar(255), ${SQLiteCommonColumns.DeleteStatus} varchar(255), ${SQLiteCommonColumns.SyncStatus} varchar(255));`;

