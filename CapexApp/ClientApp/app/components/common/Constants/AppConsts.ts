import { TreeTypeNames, FABRICS } from "../../globals/Model/CommonModel";
import { EntityTypes, ENTITYTYPES } from "../../globals/Model/EntityTypes";

export class AppConsts {
  public static apiWorkerPath = 'wwwroot/workers/httpWebWorker.js';
  public static treeWorkerPath = 'wwwroot/workers/WebWorker.js';
  public static GETDATAFROMBACKEND = 'GetDataFromBackEnd';
  public static READDATAFROMSQL = 'ReadDataFromSql';
  public static UPDATING = "updating";
  public static UPDATED = "updated";
  public static DELETING = "deleting";
  public static DELETED = "deleted";
  public static CREATING = "creating";
  public static CREATED = "created";
  public static READING = "reading";
  public static READ = "read";
  public static CREATE = "create";
  public static UPDATE = "update";
  public static DELETE = "delete";
  public static SQKLITEQUERY = "sqlLiteQuery";
  public static NONSQLLITEQUERY = "nonSqlLiteQuery";
  public static LOADER_MESSAGE_DURATION = 2000;
  public static loaderStart = "start";
  public static loaderComplete = "complete";
  public static globalAdmin = "GlobalAdmin";
  public static WATERINJECTIONFACILITY = "Water Injection Facility";
  public static TYPEOF = "TypeOf";
  public static ERROR = "error";
  public static TaggedItems = [EntityTypes.Building, EntityTypes.Concrete, EntityTypes.AREA, EntityTypes.Facility, EntityTypes.Equipment, EntityTypes.Spool, EntityTypes.Weld];
  public static Materials = [EntityTypes.Pipes];
  public static HeaderNgSelect = [TreeTypeNames.Approvals]
  public static HeaderSearch = [TreeTypeNames.Approvals, TreeTypeNames.TaggedItems, TreeTypeNames.Materials, TreeTypeNames.Schedule, TreeTypeNames.PhysicalInstances, TreeTypeNames.Turnover]
  public static ConstructionTreeList = [TreeTypeNames.Schedule, TreeTypeNames.Approvals, TreeTypeNames.TaggedItems, TreeTypeNames.Materials, TreeTypeNames.PhysicalInstances, TreeTypeNames.Documents, TreeTypeNames.Turnover]
  public static ConstructionSGTreeList = [TreeTypeNames.Schedule, TreeTypeNames.TaggedItems, TreeTypeNames.Materials]
  //form readonly based on tab
  public static FromReadOnly = { [TreeTypeNames.Schedule]:true,
                                 [TreeTypeNames.Approvals]:false,
                                 [TreeTypeNames.TaggedItems]:true,
                                 [TreeTypeNames.Materials]:true,
                                 [TreeTypeNames.PhysicalInstances]:true,
                                 [TreeTypeNames.Documents]:true,
                                 [TreeTypeNames.Procedures]:true,
                                 [TreeTypeNames.Directory]:true,
                                 [TreeTypeNames.Turnover]:false
                                } 

  //Security
  public static mutualFabric: Array<any> = [FABRICS.ENTITYMANAGEMENT];
  public static treeNameForPermission = [TreeTypeNames.peopleTreeForPermissions, TreeTypeNames.rolesTreeForPermissions, TreeTypeNames.securityGroupsTreeForPermissions, TreeTypeNames.rolesTreeForPermissionsRead];
  public static permissionRightSideAddNew = [TreeTypeNames.rolesTreeForPermissions, TreeTypeNames.securityGroupsTreeForPermissions];
  public static treeNameForAuthorization = [TreeTypeNames.peopleTreeForPermissions, TreeTypeNames.rolesTreeForPermissions, TreeTypeNames.securityGroupsTreeForPermissions, TreeTypeNames.SecurityGroupsPeopleTree, TreeTypeNames.SecurityGroupsEntitiesTree, TreeTypeNames.RoleTree, TreeTypeNames.PeopleAndCompanies, TreeTypeNames.BusinessIntelligence, TreeTypeNames.rolesTreeForPermissionsRead, TreeTypeNames.CONSTRUCTION];
  public static DropTreeNameList = [TreeTypeNames.peopleTreeForPermissions];
  public static treeNameForEntityManagement = [TreeTypeNames.PeopleAndCompanies, TreeTypeNames.BusinessIntelligence, TreeTypeNames.Construction];

  public static HighlightHeaderButton = ["Data", "MultiDateView", "Settings", "Analytics", "PhysicalFlow", "ActivityCenter", "FacilityCodeAnalytics"];

  public static droppedRowsMapping = [{ "dropDataPointer": "", "field": "uuid", "value": "entityid" }, { "dropDataPointer": "", "field": "entityid", "value": "entityid" }, { "dropDataPointer": "", "field": "parententityid", "value": "parententityid" }, { "dropDataPointer": "", "field": "entityname", "value": "entityname" }]

  public static SGEntitySortingOrderArray = [EntityTypes.FACILITYCODE, EntityTypes.METERSTATION, EntityTypes.PLANT, EntityTypes.GASBATTERY, EntityTypes.OILBATTERY, EntityTypes.COMPRESSORSTATION, EntityTypes.BOOSTERSTATION, EntityTypes.SATELLITE, EntityTypes.PAD, EntityTypes.HEADER, EntityTypes.RISER, EntityTypes.GASINJECTIONFACILITY, EntityTypes.WATERINJECTIONFACILITY, EntityTypes.INVENTORYSTORAGE, EntityTypes.TRUCKTERMINAL, EntityTypes.WASTEFACILITY, EntityTypes.TREATINGFACILITY, EntityTypes.GASWELL, EntityTypes.OILWELL, EntityTypes.GASWELLPRODUCINGOIL, EntityTypes.GASINJECTIONWELL, EntityTypes.GASDISPOSALWELL, EntityTypes.WATERINJECTIONWELL, EntityTypes.WATERDISPOSALWELL, EntityTypes.STEAMINJECTION, EntityTypes.WATERSOURCEWELL, EntityTypes.DISTRICT, EntityTypes.AREA, EntityTypes.FIELD, EntityTypes.HaulLocation, EntityTypes.List];



  public static Settings = "Settings";


}


export class SourceSystem {
  public static System = "System";
  public static LiveDataIngestion = "livedataingestion";

}

export const ApprovalsType = [EntityTypes.Journals, EntityTypes.Safety, EntityTypes.Environment, EntityTypes.Exhibits];//EntityTypes.LEMS (Removed LEM from Approval List for now)



export const DefaultPeopleSortingOrderArray = [EntityTypes.COMPANY, EntityTypes.OFFICE, EntityTypes.PERSON];

export const General = "General";
export const Admin = "Admin";
export const Configuration = "Configuration";
export const DataEntry = "DataEntry";
export const globalAdmin = "globalAdmin";



export const DefaultAttributesForAllEntitites: Array<any> = ["DistrictId", "AreaId", "FieldId", "TimeZone"];

export const peopleEntityTypes = [EntityTypes.COMPANY, EntityTypes.PERSON];
export const BiEntityTypes = [EntityTypes.DashBoard, EntityTypes.Category];
export const ConstructionEntityTypes = [EntityTypes.Construction, EntityTypes.TaggedItems, EntityTypes.Documents, EntityTypes.Materials, EntityTypes.Schedule, EntityTypes.PhysicalInstances];


export const ALLENTITYTYPES = [ENTITYTYPES.TASK, EntityTypes.Materials, EntityTypes.TaggedItems, EntityTypes.Category, EntityTypes.DashBoard];


export const BIentitiesarray = ["categories", "dashboards"];
export const FilterEntitiesfor_BI = [EntityTypes.Category, EntityTypes.DashBoard];
export const MapsEntities = [EntityTypes.Category, EntityTypes.Map];
export const PEOPLEentitiesarray = ["Companies", "Groups", "Persons"];


export const PermissionFormListForDeleteButtonDisable = { "Production Management Role": ["CompressorDataEntry", "CoriolisMeterDataEntry", "DeliveredDataEntry", "FacilityDataEntry", "GasInjectionFacilityDataEntry", "InjectionAndDisposalAndSourceWellDataEntry", "InventoryStorageDataEntry", "MeterStationDataEntry", "NonMeteredDataEntry", "OrificeDataEntry", "PDMeterDataEntry", "PumpDataEntry", "RotaMeterDataEntry", "TankDataEntry", "TruckTicketConfiguration", "TurbineDataEntry", "UltrasonicDataEntry", "WaterInjectionFacilityDataEntry", "WellDataEntry"] };


// Construction constants

/* Form Types*/
export const POPUP = "popup";
export const FilterEvent = "filterevent";
export const ROUTE = "route";
export const FORMS = "forms";
export const MENU = "menu";
export const APP = "app";
export const CAPABILITY = "capability";
export const MENU_ID = "menuId";
export const SCHEMA = "schema";
export const OPERATION_TYPE = "operationType";
export const ENITTY_ID = "entityId";
export const PARENT_ID = "parentId";
export const HOME = "home";
export const VIRTUALKEY = 'virtualKey';
export const PROPERTIES = 'properties';

// licence wizard form related
export const LICENCE = "licence";
export const WELCOME_PAGE = "Welcome - EULA";
export const ROUTING = "routing";
export const ACCEPT = "accept";
export const REJECT = "reject";

export const DELETE = "delete";
export const INLINE = "inline";
export const CLOSE = "close";
export const SUBMIT = "submit";
export const SEARCH = "search";
export const CLICK = "click";
export const DB_CLICK = "dbclick";
export const CHANGE = "change"
export const SAVE = "save"
export const SELECT = "ngselect"
export const SEARCHINPUT = "searchinput"
export const ROWS_DROPPED = "rows_dropped"
export const DELETE_ALL = "delete_all"
export const EXPAND_ALL = "expand_all"
export const CONTEXTMENU = "contextmenu"
export const DELETE_ROWS = "delete_rows"
export const NEW_ROWS = "new_rows"
export const DELETEROW = "deleterow"


export const COPY_PREVIOUS = "copyprevious";
export const CLEAR_TABLE = "cleartable";
export const UPLOADLOGFILE = "uploadlogfile";
export const FAVORITE = "favorite";
export const BACK = "back";
export const ROUTEBACKWARD = "routebackward";
export const COMPLETE = "complete";
export const Attachement = 'attachement';
export const CAMERA = 'camera';
export const LONG_PRESS = 'longpress';
export const ROW_CONTEXT_CLICK = 'table-row-context-click';
export const FILTERLONGPRESS = 'filterlongpress';

export const IMAGES = 'images';
export const INLINE_GPS = 'inline-gps';
export const BARCODESCAN = "barcodescan";
export const BARCODESEARCH = "barcodesearch";
export const READ_IMAGE = "read_image";
export const ENUM_NEW_OPTION = 'enum_new_option'
export const READ_SPECIFICATION = 'read_specification';
export const LOGGER = 'logger';
export const TREE_ACTION = 'tree_action';


/* Application Events*/
export const SQLITE_INITIALIED = 'SQLITE_INITIALIED';
export const SQLITE_DB_OPENED = 'SQLITE_DB_OPENED';
export const MENU_SYNC_COMPLETED = 'MENU_SYNC_COMPLETED';
export const REFRESH_DATA = 'REFRESH_DATA';
export const APPLICATION_INITIALIED = 'APPLICATION_INITIALIED';
export const UPDATEFORM_FROM_CHILDPAGE = 'UPDATEFORM_FROM_CHILDPAGE';
export const THEME_CHANGE = 'themechange';

export const POPUP_DEFAULT_HEIGHT = '450px';
export const POPUP_DEFAULT_WIDTH = '600px';
export const ADD_POPUP = 'addpopup';
export const FILTER_POPUP = 'filterpopup';

export const PHYSICAL = 'physical'

export class UserKey {
  public static userName = 'username';
  public static firstName = 'firstname';
  public static lastName = 'lastname';
  public static userId = 'userid';
  public static token = 'token';
  public static tenantName = 'tenantname';
  public static tenantId = 'tenantid';
  public static password = 'password';
  public static emailId = 'emailid';
  public static isChangePassword: 'ischangepassword';
  public static createPassword = 'createpassword';
  public static confirmPassword = 'confirmpassword';
  public static domain = 'domain';
  public static rpotpcode = 'rpotpcode';
  public static otpCode = 'otpcode';
  public static deviceDetail = 'devicedetail';
  public static initial = 'initial';
  public static initialId = 'initialid';
  public static signature = 'signature';
  public static signatureId = 'signatureid';
  public static deviceId = 'deviceid';
  public static url = 'url';
  public static loginTime = 'logintime';
  public static userPrefrence = 'userprefrence';
  public static modifiedTime = 'modifiedtime';
  public static viewedFabrics = 'viewedfabrics';
  public static validUser = 'validuser';
  public static windowCloseTime = 'windowclosetime';
  public static entityFilter = 'entityfilter';
  public static isLicenceAccepted = 'islicenceaccepted';
  public static syncFrequency = 'syncfrequency';
  public static deviceInfo = 'deviceinfo';
  public static lastLogin = 'lastlogin';
  public static application = 'application';
  public static payload = 'payload';
}

/** Construction Webapp Constants **/
export const VirtualKey: string = 'virtualKey';
export const Properties: string = 'properties';
export const Route: string = "route";
export const Forms = "forms";
export const SCHEMA_PAGE = "schemapage";
export const FILTER = "filter";
export const READ_DATA = "readdata";
export const READ_TABLE_DATA = "readtabledata";
export const UserSettingsSync = 'UserSettings_SyncTimeList';
export const UserSettingsTable = 'usersettings';
export const UserSetting = 'usersettings';
export const UserSettings = 'UserSettings_usersettings';

export const FilterTable = "filter";
export const ApprovalsSSRSDoc = "SSRSDOCPAGE";
export const ReviewAndApproval = "Review & Approvals";
export const Reviewers = "reviewers";
export const Approval = "approval";
export const Turnover = "Turnover";

export class FormOperationTypes {
  public static Add = 'add';
  public static Update = 'update';
}

export class SQLiteCommonColumns {
  public static readonly EntityId: string = 'entityid';
  public static readonly EntityName: string = 'entityname';
  public static readonly EntityType: string = 'entitytype';
  public static readonly Application: string = 'application';
  public static readonly Parent: string = 'parent';
  public static readonly ParentEntityId: string = 'parententityid';
  public static readonly UUID: string = 'uuid';
  public static readonly CreatedBy: string = 'createdby';
  public static readonly Payload: string = 'payload';
  public static readonly Image: string = 'image';
  public static readonly Thumbnail: string = 'thumbnail';
  public static readonly Altitude: string = 'altitude';
  public static readonly Latitude: string = 'latitude';
  public static readonly Longitude: string = 'longitude';
  public static readonly Type: string = 'type';
  public static readonly ModifiedBy: string = 'modifiedby';
  public static readonly TenantId: string = 'tenantid';
  public static readonly TenantName: string = 'tenantname';
  public static readonly ModifiedDateTime: string = 'modifieddatetime';
  public static readonly SyncDateTime: string = 'syncdatetime';
  public static readonly CreatedDateTime: string = 'createddatetime';
  public static readonly CreateStatus: string = 'createstatus';
  public static readonly UpdateStatus: string = 'updatestatus';
  public static readonly DeleteStatus: string = 'deletestatus';
  public static readonly SyncStatus: string = 'syncstatus';
  public static readonly IsDeleted: string = 'isdeleted';
  public static readonly EntityTimeZone: string = 'entitytimezone';
  public static readonly IconFileName: string = 'iconfilename';
  public static readonly ID: string = 'id';
  public static readonly SchemaPage: string = 'schemapage';
  public static readonly LeftMenu: string = 'leftmenu';
  public static readonly RightMenuID: string = 'rightmenuid';
  public static readonly UserName: string = 'username';
  public static readonly EmailId: string = 'emailid';
  public static readonly UserId: string = 'userid';
  public static readonly LogInTime: string = 'logintime';
  public static readonly DefaultColumnNames = [SQLiteCommonColumns.Parent, SQLiteCommonColumns.ParentEntityId, SQLiteCommonColumns.UUID, SQLiteCommonColumns.Payload, SQLiteCommonColumns.Type, SQLiteCommonColumns.TenantId, SQLiteCommonColumns.TenantName, SQLiteCommonColumns.CreatedBy, SQLiteCommonColumns.ModifiedBy, SQLiteCommonColumns.CreatedDateTime, SQLiteCommonColumns.ModifiedDateTime, SQLiteCommonColumns.EntityTimeZone, SQLiteCommonColumns.CreateStatus, SQLiteCommonColumns.UpdateStatus, SQLiteCommonColumns.DeleteStatus, SQLiteCommonColumns.SyncStatus]

}

export class ApplicationTheme {
  public static dark = "dark";
  public static light = "light";
}

//Security Group Constant
export const PREMICO = "Premico";
export const ENTITIES = "Entities";
export const PEOPLEANDCOMPANIES = "People&Companies";

// state variables for query
export const STATE_FILTER ='$state:filter'
export const STATE_TABLE_FILTER_WITH_WHERE ='$state:tablefilterWithWhere'
export const STATE_TABLE_FILTER ='$state:tablefilter'
export const STATE_LIMIT_WITH_OFFSET ='$state:limitWithOffset'
/* Metrics */
export const METRIC = 'metric';
export const US_CUSTOMARY = 'us customary';
export const US_CUSTOMARY_COLUMN = 'uscustomary';
