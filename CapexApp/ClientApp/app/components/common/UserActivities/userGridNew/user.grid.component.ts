import { Component, EventEmitter, HostListener, Input, OnDestroy, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { CommonService } from '../../../globals/CommonService';


@Component({
  selector: 'userprofilegrid',
  templateUrl: './user.grid.component.html',
  styleUrls: ['./user.grid.component.scss']
})
/* class GridTypeComponent */
export class UserGridComponentNew implements OnDestroy {
  @Input() gridinputdata;
  @Input() myForm;
  @Input() EntityId;
  @Input() properties;

  @Output() AddClick = new EventEmitter();
  @Output() Click = new EventEmitter();
  noofcols;
  colsdata;
  initialized = false;
  imgSrc;
  width = 60;
  uploadBtnSrc = "UPLOAD PICTURE";//"wwwroot/images/usersettings/Upload Picture button v1.svg";
  constructor(private sanitizer: DomSanitizer, public commonservice: CommonService, public dialog: MatDialog) {
  }
  ngOnInit() {
    this.colsdata = this.gridinputdata;
    this.noofcols = this.gridinputdata.numberOfCols?this.gridinputdata.numberOfCols:1;
    let imgControlProperties=this.gridinputdata[0];

    if(this.properties && this.properties["UserImage"]){
      let property = this.properties["UserImage"];
      let label = property["label"];
      let typ = property["type"];
      imgControlProperties['type']=typ;
      imgControlProperties['displayLabel']=label;
      imgControlProperties['conditional']={};
    }
    else {
      imgControlProperties['type']="img";
      imgControlProperties['displayLabel']="User Image";
      imgControlProperties['conditional']={};
    }

    this.colsdata = [imgControlProperties];

    if (this.colsdata.length != 0) {
      this.initialized = true;
    }
    if (this.myForm.controls["UserImage"]) {
      this.uploadBtnSrc = this.myForm.controls["UserImage"].value ? 'UPLOAD PICTURE' : 'UPLOAD LOGO';
     
    this.imgSrc = this.myForm.controls["UserImage"].value ? this.myForm.controls["UserImage"].value : false;
    }
    // if (this.colsdata.length != 0) {
    //   this.colsdata.forEach(cols => {
    //     if (cols.type == "select" ) {
    //       if (this.myForm.controls[cols.actualname] && this.myForm.controls[cols.actualname].value == "") {
    //         this.myForm.controls[cols.actualname].setValue(null, { emitEvent: false });
    //       }
    //     }
    //   });
    // }
    // this.getParent();
  
  }

  @HostListener('window:resize')
  onResize() {
    this.width = (window.innerWidth / 1400);
  }


  readFile(event) {
    let files = event.target.files;
    if (files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(files[0]);
      setTimeout(() => {
        let fileData: any = reader.result;
        if (fileData && fileData != '') {
          this.imgSrc = this.sanitizer.bypassSecurityTrustResourceUrl(fileData);
          this.myForm.controls["UserImage"].setValue(fileData, { emitEvent: false });
          this.uploadBtnSrc = this.myForm.controls["UserImage"].value ? 'UPLOAD PICTURE' : 'UPLOAD LOGO';
        }
      }, 500);
    }
  }
 
  getDataValidation(key) {
   // this.commonservice.onBlurEvent.next(key);
  }

  DataClicking(data) {
    try {
      this.Click.emit(data);
    }
    catch (e) {
      console.error('Exception in DataClicking() of grid-type.component in grid-type at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  ngOnDestroy() {
  }
  
}
