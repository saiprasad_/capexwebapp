import { ChangeDetectorRef, Component, ComponentFactoryResolver, Inject, Input, OnDestroy, OnInit, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FABRICS, MessageKind } from 'ClientApp/app/components/globals/Model/CommonModel';
import { EntityTypes } from 'ClientApp/app/components/globals/Model/EntityTypes';
import { Observable, Subject } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/internal/operators/distinctUntilChanged';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { PopUpAlertComponent } from '../../../common/PopUpAlert/popup-alert.component';
import { CommonService } from '../../../globals/CommonService';
import { PopupOperation } from '../../../globals/Model/AlertConfig';
import { Routing } from '../../../globals/Model/Message';
import { DialogAlertComponent } from '../../../template/LayoutComponents/DialogAlert/dialog-alert.component';
import { CONTACT_SUPPORT, DISCARD_CHANGES, FILL_REQUIRED_FIELDS, ONLY_NUMBERS, SOMETHING_WRONG, TEN_DIGIT_NUMBERS, VALIDATION_FAILED } from '../../Constants/AttentionPopupMessage';
import { FDCFormGeratorCommon } from '../../services/FDCFormGeneratorCommonService/FDCFormGeratorCommon';

@Component({
  selector: 'user-settings',
  templateUrl: './user-setting.component.html',
  styleUrls: ['./user-setting.component.styles.scss']
})
export class FormSettingComponent implements OnInit, OnDestroy {

  Operation = 'UPDATE'
  userName;

  entityName: string;
  entityType: string;
  tenantName: string;
  tenantId: string;
  entityID: string;

  properties: any;


  intialized: boolean;
  isSubmit: boolean = false;
  isValidTrue = true;
  themechangestatus: boolean = false;
  themedata: any;
  myForm: FormGroup;

  buttonMode = this.commonService.tabletMode ? "On" : "Off";
  keyPadMode = this.commonService.tabletMode ? "On" : "Off";
  themeType = this.commonService.themeDark ? "Light" : "Dark";
  tabletType = this.commonService.tabletMode ? "On" : "Off";


  filteredOptions: Observable<any[]>;
  @Input() ApplicationZoom = 0;

  headerConfig = [
    {
      'source': 'V3 General',
      'title': 'User Settings',
      'routerLinkActive': false,
      'id': 'title',
      'class': 'title',
      'float': 'left',
      'type': 'title',
      'show': true,
      'uppercase': 'Isuppercase'
    },
    {
      'source': 'wwwroot/images/V3 Icon set part1/V3 CloseCancel.svg',
      'title': 'Close',
      'routerLinkActive': false,
      'id': 'close',
      'class': 'icon21-21',
      'float': 'right',
      'type': 'icon',
      'show': true
    },
    {
      'source': 'wwwroot/images/AlThing UI Icon-Button Set v1/FDC_Checkmark_Icon_Idle.svg',
      'title': 'save',
      'routerLinkActive': false,
      'id': 'save',
      'class': 'icon21-21',
      'float': 'right',
      'type': 'icon',
      'show': true
    }
  ];

  parent: any;
  parentType: any;
  UserSettingSchema;
  PersonPayloadData;
  EntityType = "UserSetting";
  EntityName = "UserSetting";
  imgElements = [
    {
      "label": "UserImage",
      "col": 1,
      "colspan": 4,
      "type": "img",
      "displayLabel": "User Image",
      "conditional": {}
    }
  ];
  constructor(public FDCFormGeratorCommonService: FDCFormGeratorCommon, public dialogRef: MatDialogRef<FormSettingComponent>,
    @Inject(MAT_DIALOG_DATA) public datas: any, public commonService: CommonService, public dialog: MatDialog,
    private formBuilder: FormBuilder, public router: Router, public componentFactoryResolver: ComponentFactoryResolver) {

    this.commonService.isUserSettingForm = false;
    this.tenantId = this.commonService.tenantID;
    this.tenantName = this.commonService.tenantName;
    this.entityType = "Person";
    this.entityID = this.commonService.currentUserId;

    this.commonService.userSettingForm.pipe(this.compUntilDestroyed()).subscribe((res: any) => {

      if (res) {
        let payload = JSON.parse(res['Payload'])
        this.UserSettingSchema = JSON.parse(payload['Schema'])
        this.PersonPayloadData = JSON.parse(payload["Data"]);
        let persondetails = JSON.parse(this.PersonPayloadData.Payload)
        let parentdetails = this.commonService.getEntitiesById(this.PersonPayloadData.Parent);
        persondetails.person.company = parentdetails.EntityName;
        this.PersonPayloadData.Payload = JSON.stringify(persondetails);
        this.UserSettingSchema.layout[0].items[0].type = "div";
        this.UserSettingSchema.layout[0].items[0].title = "";

        let hideItems = {
          "options": {
            "md": "0 0 calc(33%-10px)",
            "sm": "0 0 calc(33%-10px)",
            "lg": "0 0 calc(33%-10px)"
          },
          "title": "",
          "type": "number",
          "key": "person.hide1"
        };

        let newArray = [];
        if(this.PersonPayloadData.UserImage){
          this.myForm.controls['UserImage'].setValue(this.PersonPayloadData.UserImage,{emitEvent:false});
          this.commonService.userProfilePicture = this.PersonPayloadData.UserImage;
        }
        this.UserSettingSchema.layout[0].items[0].items[0].items.forEach((item, index, arr) => {
          newArray.push(item)
          if (item.title == "Business") {
            newArray.push(hideItems);
          }
          if (item.title == "Email") {
            hideItems.key = "person.hide2"
            newArray.push(hideItems)
            hideItems.key = "person.hide3"
            newArray.push(hideItems)
          }
        })
        this.UserSettingSchema.layout[0].items[0].items[0].items = newArray;


      }

      if (this.UserSettingSchema && this.PersonPayloadData) {
        this.userName = this.getEntityNameFromPayload(this.PersonPayloadData);
        this.commonService.getCompaniesFromStore$().first().subscribe((res: any) => {
          if (res && res.length) {
            let payload = JSON.parse(this.PersonPayloadData.Payload);
            let companyId = payload.company;
            let companyObject = res.filter(item => item.entityid == companyId);
            if (companyObject.length) {
              payload.company = companyObject[0].entityname;
              this.PersonPayloadData.Payload = JSON.stringify(payload);
            }
          }
        });
        this.intialized = true;
      }
      else {
        this.openDialog(new Array(SOMETHING_WRONG, CONTACT_SUPPORT));
      }
    });

    if (this.commonService.ZoomPercentage) { }
    else
      this.commonService.ZoomPercentage = "100%";
  }

  ngOnInit(): void {
    this.myForm = this.formBuilder.group({});
    this.myForm.addControl("UserImage", this.formBuilder.control(''))
  }

  getEntityNameFromPayload(peopleData) {
    let payload = JSON.parse(peopleData.Payload);
    this.entityName = payload['person'].EntityName;
    return this.entityName;
  }

  ClickEvent(ev) {
    switch (ev.id) {
      case 'close':
        this.CloseDialog();
        break;
      case 'save':
        this.validateForm(ev.id)
        break;
    }
  }


  CloseDialog(): void {
    if (this.myForm.dirty) {
      this.openDialogRef();
    }
    else
      if (this.themechangestatus) {
        this.themedata.checked = !this.themedata.checked;
        this.themechangestatus = false;
      }
    if (!this.myForm.dirty) {
      this.dialogRef.close();
      //this.headerContentGridFDCState("Close");
    }
  }

  openAlertDialog(message, submessage?): void {
    let config = new MatDialogConfig()
    config.data = { 'header': 'Attention!', 'content': message, 'subContent': submessage };
    config.width = '500px';
    this.dialog.open(DialogAlertComponent, config);
  }


  ngOnDestroy() {
    try {
      this.properties = null;
      this.commonService.isUserSettingForm = true;
      this.takeUntilDestroyObservables.next();
      this.takeUntilDestroyObservables.complete();
    } catch (e) {
      console.error('Exception in ngOnDestory() of ngOnDestroy at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in ngOnDestory() of ngOnDestroy at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  takeUntilDestroyObservables = new Subject();
  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables);
  }

  eventsToUserFormGenerator: Subject<void> = new Subject<void>();
  validateForm(id) {
    if (this.isValidTrue && this.myForm.valid) {
      this.eventsToUserFormGenerator.next(id);
    }
    else {
      let error = [];
      error.push(FILL_REQUIRED_FIELDS)
      this.openDialog(VALIDATION_FAILED);
    }
  }

  eventFromUserFormGenerator(event) {
    if (event.FormEvent) {
      switch (event.FormEvent) {
        case "submit":
          this.submitForm(event);
          break;
      }
    }
  }

  themeChange(data) {
    this.themechangestatus = true;
    this.themedata = data;
    if (data.checked) {
      this.themeType = "Light";
    }
    else {
      this.themeType = "Dark";
    }
    this.commonService.changeTheme.next(data);
  }

  changeTabLetMode(data) {
    if (data.checked) {
      this.tabletType = "On";
      this.buttonMode = "On";
      this.keyPadMode = "On";
      // this.commonService.isUserSettingFormTabletMode = true;
      // this.commonService.AppendToBodyObservable$.next({ "Mode": "TabletMode", "EventFrom": "TabletMode", "Value": this.commonService.tabletMode });
    }
    else {
      // this.commonService.isUserSettingFormTabletMode = false;

      this.tabletType = "Off";
    }
    this.commonService.tabletMode = !this.commonService.tabletMode;
    this.commonService.tableButtonMode = this.commonService.tabletMode;
    this.commonService.tabletKeyPadMode = this.commonService.tabletMode;
    // if(this.commonService.tabletMode){
    //   this.ZoomPercentage='100%'
    // }
    this.commonService.zoomApplicationForTabletMode$.next({ "ZoomType": "Default", "Value": this.commonService.tabletMode, "Bydefault": true });
    let IncrementOrDecrement: string = parseInt(this.commonService.ZoomPercentage) === 100 ? "Default" : parseInt(this.commonService.ZoomPercentage) > 100 ? "Increment" : "Decrement";
    if (this.tabletType == "On")
      this.commonService.zoomApplicationForTabletMode$.next({ "ZoomType": IncrementOrDecrement, "Value": parseInt(this.commonService.ZoomPercentage) + '%', "Bydefault": true });
    else
      this.commonService.zoomApplicationForTabletMode$.next({ "ZoomType": "Default", "Value": "100%" });

    //this.commonService.tabletModeObservable$.next({ "TypeOfData": "UserSettingsData", "Mode": "TabletMode", "EventFrom": "TabletMode", "Value": this.commonService.tabletMode });
    // if (this.commonService.tabletMode) {
    //   this.commonService.isMinimizeKeypad = true;
    // }
  }
  changeButtonMode(data) {
    if (data.checked)
      this.buttonMode = "On";
    else
      this.buttonMode = "Off";
    this.commonService.tableButtonMode = !this.commonService.tableButtonMode;
    //this.commonService.tabletModeObservable$.next({ "TypeOfData": "UserSettingsData", "Mode": "TabletMode", "EventFrom": "ButtonMode", "Value": this.commonService.tableButtonMode });

  }
  changeKeyPadMode(data) {
    if (data.checked)
      this.keyPadMode = "On";
    else
      this.keyPadMode = "Off";
    this.commonService.tabletKeyPadMode = !this.commonService.tabletKeyPadMode;
    // this.commonService.tabletModeObservable$.next({ "TypeOfData": "UserSettingsData", "Mode": "TabletMode", "EventFrom": "NumberOrKeyPadMode", "Value": this.commonService.tabletKeyPadMode });
  }

  decreaseZoom() {
    this.commonService.ZoomPercentage = parseInt(this.commonService.ZoomPercentage) - 10 + '%';
    let dataToSend: any = { "ZoomType": "Decrement", "Value": this.commonService.ZoomPercentage };
    this.commonService.zoomApplicationForTabletMode$.next(dataToSend);

    //this.commonService.updateIndexDBUserPrefrence(null);
  }

  increaseZoom() {
    this.commonService.ZoomPercentage = parseInt(this.commonService.ZoomPercentage) + 10 + '%';
    let dataToSend: any = { "ZoomType": "Increment", "Value": this.commonService.ZoomPercentage };
    this.commonService.zoomApplicationForTabletMode$.next(dataToSend);
    //this.commonService.updateIndexDBUserPrefrence(null);
  }

  openDialog(messageToshow): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: false,
        content: [messageToshow],
        subContent: [],
        operation: PopupOperation.Attention
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      this.dialog.open(PopUpAlertComponent, matConfig);
    }
    catch (ex) {
      console.error(ex);
    }
  }

  submitForm(event) {
    var fabric = this.commonService.GetFabricName('/' + this.commonService.lastOpenedFabric);
    var payload = {};

    let eventData = event && event.data ? event.data['person'] : null;
    eventData["UserImage"] = this.myForm.value['UserImage'];
    if (eventData) {
      eventData["EntityType"] = "Person";
      eventData["EntityId"] = this.commonService.currentUserId;
      var messageKind = MessageKind.UPDATE;
      eventData["Parent"] = eventData["company"];//company id
      eventData["EntityName"] = eventData["firstname"] + " " + eventData["lastname"];


      payload = {
        "EntityId": eventData.EntityId,
        "TenantName": this.commonService.tenantName,
        "TenantId": this.commonService.tenantID,
        ModifiedBy: this.commonService.CurrentUserEntityName,
        ModifiedDateTime: new Date().toISOString().slice(0, 19).replace("T", " "),
        CreatedDateTime: new Date().toISOString().slice(0, 19).replace("T", " "),
        CreatedBy: this.commonService.currentUserName,
        "EntityType": eventData.EntityType,
        "Fabric": this.commonService.lastOpenedFabric.toUpperCase(),
        "Payload": "",
        "Info": "Admin",
        "Parent": eventData["Parent"],
        "ParentType": "Company",
        "EntityName": eventData["EntityName"],
        "EntityInfoJson":this.myForm.value,
        Type: ""
      };



      event.data["Parent"] = payload["Parent"];
      event.data["ParentType"] = payload["ParentType"];

      payload["Payload"] = JSON.stringify(event.data);

      if (this.commonService.CurrentUserEntityName != eventData["EntityName"]) {
        this.commonService.CurrentUserEntityName = eventData["EntityName"]
      }

      if (eventData["UserImage"]) {
        this.commonService.userProfilePicture = eventData["UserImage"];
      }

      if (this.themeType && this.themeType == "Light")
        this.commonService.updateIndexDBUserPrefrence(null, true);

      if (this.themeType && this.themeType == "Dark")
        this.commonService.updateIndexDBUserPrefrence(null, false);

      var payloadString = JSON.stringify(payload);
      this.commonService.sendMessageToServer(
        payloadString,
        FABRICS.PEOPLEANDCOMPANIES,
        eventData.EntityId,
        eventData.EntityType,
        messageKind,
        Routing.AllButOriginSession,
        "Details",
        FABRICS.HOME
      );

      this.isSubmit = false;
      this.dialogRef.close();
    }  
  }

  openDialogRef() {
    let message = DISCARD_CHANGES;
    try {
      let config = {
        header: 'Attention!',
        isSubmit: true,
        content: [message],
        subContent: [],
        operation: PopupOperation.AlertConfirm,
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      matConfig.id = message;
      if (!this.dialog.getDialogById(message)) {
        const dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);

        dialogRef.componentInstance.emitResponse.subscribe((res) => {
          this.isSubmit = false;
          this.dialogRef.close();
        });
      }

    }
    catch (e) {
      console.error('Exception in openDialog() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  eventHandlerSubscribe(event) {
  }

}





