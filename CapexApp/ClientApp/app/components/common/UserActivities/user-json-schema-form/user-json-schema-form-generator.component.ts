import { Component, OnInit, Input, OnDestroy, ViewChild, EventEmitter, Output } from '@angular/core';
import { JsonSchemaFormComponent } from '@visur/formgenerator-core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subject, Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { takeUntil, filter } from 'rxjs/operators';
import { CommonService } from '../../../globals/CommonService';
import { untilDestroyed } from 'ngx-take-until-destroy';
//import { PeopleActivitiesService } from 'ClientApp/app/components/fabrics/PeopleFabric/services/people-activities-service';


const defaultOptions: any = {
    addSubmit: false, // Add a submit button if layout does not have one
    debug: false, // Don't show inline debugging information
    loadExternalAssets: true, // Load external css and JavaScript for frameworks
    returnEmptyFields: false, // Don't return values for empty input fields
    setSchemaDefaults: true, // Always use schema defaults for empty fields
    defautWidgetOptions: { feedback: true,hideRequired:true}, // Show inline feedback icons,
    framework: 'material-design',
    selectedLanguage: 'en',
    navigateBackOnSubmit: true,
    ngSelectDefaultSearchable: true,
    floatlabel:true
   
};

@Component({
    selector: 'user-json-schema-form-generator',
    templateUrl: './user-json-schema-form-generator.component.html',
    styleUrls: ['./user-json-schema-form-generator.component.scss']
})
export class UserJsonSchemaFormGeneratorComponent implements OnInit, OnDestroy {
    @Input() header: any;
    @Input() layout: any;
    @Input() data: any;
    @Input() options: any;
    @Input() schema: any;
    @Input() roles: any;
    @Input() images:any;
    @Input() userRoles: any;
    @Input() operationType;
    @Input() schemaPage = null;
    @Input() tableName = null;
    @Input() nestedSchema;
    @Input() search;
    @Input() parentPage;
    @Input() readDataFromOtherSource;
    @Input() queries;
    @Input() updateData;
    @Input() isCompleted = false;
    @Input() BACKEND_SCHEMA_JSON;
    @Input() BACKEND_JSON_DATA;
    @Input() breadcrumbDisplayName;
    @Input() formulas
    @Input() title;
    @Input() $state;
    @Input() jsonData;
    @Input() IsEntityManagementForm;
    @Input() tab;
    @Input() EntityType;
    @Input() EntityId;
    @Input() EntityName;
    @Input() myForm;
    @Input() lookup;
    @Output() applicationEvent = new EventEmitter();

    @ViewChild(JsonSchemaFormComponent) formGenerator: JsonSchemaFormComponent;

    takeUntilDestroyObservables = new Subject();
    popupHeader: any;
    popupFormType: string;
    livedata: any;
    expanded = false;
    parentEntityId;
    isPopupForm = false;
    /// assgin value to menuid. if the page is root/first page/initial page
    menuId;
    previousRoute: string;
    headerControl = 'headerSelect';
    dialogRef: MatDialogRef<any, any>;
    adminSchema;
    childRouteParameters: any;
    @Input() parentEvents: Observable<void>;
    constructor(private route: ActivatedRoute, private commonService: CommonService, public dialog: MatDialog, private matDialog: MatDialog) {
        try {
            this.commonService.triggerSubmitOnFormGenerator$.pipe(this.compUntilDestroyed()).subscribe((res) => {
                if (!this.isCompleted) {
                    this.formGenerator.submitForm();
                }
            });

        }
        catch (e) {
            console.error('Exception in constructor of user-json-schema-form-generator in People at time ' + new Date().toString() + '. Exception is : ' + e);
        }
    }

    ngOnInit() {
        this.parentEvents.pipe(this.compUntilDestroyed()).subscribe(event=>{
            if(event){
                switch(event){
                    case 'save':
                    this.formGenerator.submitForm();    
                    break;
                }
            }
        })
        this.filterOptionsFormFormGenerator();
    }
    ngAfterViewInit() {
    }

    compUntilDestroyed(): any {
        return takeUntil(this.takeUntilDestroyObservables);
    }

    filterOptionsFormFormGenerator() {
        try {
            this.options = defaultOptions;
            this.adminSchema = this.BACKEND_SCHEMA_JSON;
            //  this.adminSchema=JSON.parse(this.adminSchema);
            if (this.adminSchema) {
                this.layout = this.adminSchema.layout;
                this.schema = this.adminSchema.schema;
                this.roles = this.adminSchema.roles;
                this.images = this.adminSchema.images;
                this.options = this.getDefaultOptions(this.adminSchema.options);
                this.nestedSchema = this.adminSchema.nestedSchema;
                this.tableName = this.adminSchema.tableName;
                this.readDataFromOtherSource = this.adminSchema.readDataFromOtherSource;
                this.parentPage = this.adminSchema.parentPage;
                this.queries = this.adminSchema.queries;
                this.formulas = this.adminSchema.formulas
                this.breadcrumbDisplayName = this.adminSchema.breadcrumbDisplayName;
                this.lookup=this.adminSchema.lookup;
                this.title = this.adminSchema.title ? this.adminSchema.title.toUpperCase() : this.adminSchema.title;
                this.$state = this.adminSchema.$state;
                if (this.BACKEND_JSON_DATA) {

                    if (Object.keys(this.BACKEND_JSON_DATA).length > 0) {
                        this.data = this.createTabDataPayload();//listData[0];
                    } else {
                        this.data = {};
                    }
                }
                let uom = this.adminSchema.unitsOfMeasurement;
            }
        } catch (e) {
            console.error(e);
        }
    }

    createTabDataPayload() {
        let payload =  JSON.parse(this.BACKEND_JSON_DATA.Payload);
        return payload;
    }

    getDefaultOptions(options: any) {
        if (options) {
            for (const key of Object.keys(options)) {
                this.options[key] = options[key];
            }
        }
        return this.options;
    }
    onChange(event) {
        console.log('onChange() :: FormGenerator :: event :: ', event);
    }
    async click(event) {
        let buttonEvent: string = event.buttonEvent ? event.buttonEvent.toLowerCase() : null;
        let jsonPayload = {};

        switch (buttonEvent) {
            // case "PopUp":
            //   // case FilterEvent:
            //   this.commonService.presentStatusMessageAsync(Loading);
            //   let obs$ = this.formService.openPopup(this.nestedSchema, event, this.schema, this.wrapper.EntityID, this.schemaPage);
            //   obs$.subscribe(dialogRef => {
            //     this.dialogRef = dialogRef;
            //     this.dialogRef.afterClosed().pipe(
            //       finalize(() => this.dialogRef = undefined)
            //     );
            //   });
            //   break;

            case "close":
                this.closePopup(this.dialogRef);
                break;

            case "inline":
                this.addInlineRow(event);
                break;

            case "submit":
                //this.formService.updateFormValueBasedOnSelectedRecord(this.search, this.livedata, this.parentPage, this.schemaPage, this.schema);
                this.closePopup(this.dialogRef);
                break;

            case "search":
                jsonPayload['parentPage'] = this.parentPage;
                jsonPayload['schemaPage'] = this.schemaPage;
                jsonPayload['data'] = this.livedata;
                jsonPayload['search'] = this.search;

                // //this.formService.openSearch(event, jsonPayload).pipe(untilDestroyed(this)).subscribe((res) => {
                //   //this.updateData = res;
                // });
                break;

            // case BARCODESCAN:
            //   const node: any = getNode(event.dataPointer, this.schema);
            //   this.search = this.nestedSchema[node.page];
            //   // this.formService.updateFormValueBasedOnSelectedRecord(this.search, this.livedata, this.parentPage, this.schemaPage, this.schema);
            //   break;

            // case BARCODESEARCH:
            //   this.formService.bindBarcodeSearchData(event, this.schema, this.nestedSchema, this.schemaPage, this.livedata);
            //   break;

            case "route":
            case "routing":
                // for add popup form, on routing closing the popup..
                this.closePopup(this.dialogRef);

                // send page id as menu id if page is first/inital page
                //      let id = this.wrapper.EntityID ? this.wrapper.EntityID : this.menuId;
                //let id = this.wrapper.EntityID ? this.wrapper.EntityID : "0";

                // for AddPopup page message kind will come as create
                // if (this.wrapper.MessageKind == MessageKinds.CREATE) {
                //   id = this.parentEntityId ? this.parentEntityId : "0";
                // }
                // let breadCrumbjson = this.formService.getbreadcrumbPayloadJson(this.route);
                // breadCrumbjson['breadcrumbDisplayName'] = this.breadcrumbDisplayName;
                // this.formService.route(event, this.schema, this.livedata, id, this.parentEntityId, this.wrapper, breadCrumbjson);
                break;

            case "copy previous":
                // this.formService.copyPrevious(event, this.schema, this.livedata, this.queries).pipe(untilDestroyed(this)).subscribe(data => {
                //   if (data) {
                //     //this.updateData[data.datapointer] = data.items;
                //     data.items = modifiedExistingArraydata(data.type, data.items);
                //     let dataPointer = getInlineDataPointer(this.schema, event.dataPointer);
                //     this.formGenerator.insertItems(dataPointer, data.items);
                //   }
                //   else {
                //     //popup alert need to implement
                //   }
                // });
                break;

            // case FAVORITE:
            //   // this.formService.updateFavoriteColumnAsync(event, this.schema)
            //   //   .then(() => 'Success');
            //   break;

            case "back":
                // this.formService.routeTo(this.previousRoute);
                break;

            // case INLINE_GPS:
            //   this.commonService.presentStatusMessageAsync(Loading);
            //   const tempData: any = await this.formService.updateDefaultGPS(event, this.schema);
            //   this.formGenerator.insertItems(tempData.dataPointer, tempData.data);
            //   this.commonService.dismissStausMessage();
            //   break;

            // case CAMERA:
            //   this.commonService.presentStatusMessageAsync(Loading);
            //   const temp: any = await this.formService.updateDefaultImage(event, this.schema);
            //   this.formGenerator.insertItems(temp.dataPointer, temp.data);
            //   this.commonService.dismissStausMessage();
            //   break;

            default:
                this.emitOutputEvent("click", event);
                break;
        }
    }
    emitOutputEvent(type: string, data: any) {
        this.applicationEvent.emit({ "type": type, data: data });
    }
    closePopup(dialogRef: MatDialogRef<any, any>, event?) {
        if (dialogRef != null) {
            dialogRef.close();
        }
        else {
            this.matDialog.closeAll();
        }
        switch (this.popupFormType) {
            case 'filter':
                break;

        }
        //this.logger.debug(event);
    }
    addInlineRow(event: any) {
        //let dataPointer = this.formService.getInlineDataPointer(this.schema, event.dataPointer);
        //this.formGenerator.insertItems(dataPointer, []);
    }

    @Output()
    userFormGeneratorEvents:EventEmitter<any>=new EventEmitter();
    onSubmitFn(event) {
        let data = event ? event.data : null;
        if (data) {
            //clean up data
            for (let key of Object.keys(data)) {
                if (!data[key]) {
                    delete data[key];
                }
            }
            // delete json data if we are reading data from other tables..
            if (this.readDataFromOtherSource) {
                let keys = Object.keys(this.readDataFromOtherSource);
                for (let key of keys) {
                    key = key.toLowerCase();
                    if (data[key]) {
                        delete data[key];
                    }
                }
            }
            event.FormEvent="submit";
            this.userFormGeneratorEvents.emit(event);
        }
    }

    
    lookUpQuery(event) {
        let dataPointer = event.dataPointer //: "/person/company"
        let lookup = event.lookup ;//: {displayName: "entityname", name: "company", bindValue: "entityid", callback: BehaviorSubject, processing: true}
        let lookupName = event.lookupName; //: "company"
        this.bindPeopleDropDowns(lookup, lookupName,this.tab);
        }
        
        bindPeopleDropDowns(event,column,tab){
        switch(column){
        case "company":
        this.commonService.getCompaniesFromStore$().first().subscribe((res:any) => {
        if (res && res.length) {
            res.sort ((firstEntity, secondEntity) =>firstEntity["entityname"].toLowerCase() > secondEntity["entityname"].toLowerCase() ? 1 : -1)
        event.callback.next(res);
        }
        });
        break;
        case "timezone":
        let timezoneObj = this.commonService.getAllTimeZone();
        let timeZoneList: any[] = [];
        for (const key in timezoneObj) {
        if (timezoneObj.hasOwnProperty(key)) {
        const tempData: any = {};
        tempData.entityid = timezoneObj[key].name + ' (' + timezoneObj[key].utcOffsetStr + ')';
        tempData.entityname = tempData.entityid;
        
        timeZoneList.push(tempData);
        timeZoneList.sort((firstEntity, secondEntity) =>firstEntity["entityname"].toLowerCase() > secondEntity["entityname"].toLowerCase() ? 1 : -1)
        }
        }
        timezoneObj = Object.assign({}, timeZoneList);
        event.callback.next(timeZoneList)
        break;
        }
        }


    enumValueUpdate(event) {
        //let schemaPage = getTableName(this.schemaPage);
        //this.formService.enumValueUpdate(schemaPage, event);
    }
    getTableName() {
        return this.schemaPage.replace(/ /g, '');
    }
    onToggleChange(data) {
        console.log(data);
        switch (data.buttonEvent) {
            case 'themeChange':
                //this.document.body.classList.toggle('dark', data.event.checked);
                break;
        }
    }
    treeAction(event) {
        // this.formService.treeAction(event).pipe(untilDestroyed(this)).subscribe(data => {
        //   setTimeout(() => {
        //     event.callback.next(data);
        //     (<Subject<any>>event.callback).complete();
        //   }, 0
        //   );
        // });
    }
    setData(data) {
        if (data != null && Object.keys(data).length > 0) {
            //this.updateData = setData(data, this.livedata);
            //this.formService.updateState(this.$state, this.schemaPage, this.updateData);
        }
        //this.commonService.dismissStausMessage();
    }
    // submitForm() {
    //   this.formGenerator.submitForm();
    // }
    async onFilterFn(event) {
        const data: any = JSON.parse(JSON.stringify(event));
        let schemaPage = this.getTableName();
        data.schemapage = schemaPage;
        //await this.formService.filterUpsertAsync(data);
    }

    isValid(event) {
        console.log("isValid method");
        console.log(event);
      }
      validationErrors(event) {
        console.log("In Valida Fields :");
        console.log(event);
      }

    ngOnDestroy() {
        this.takeUntilDestroyObservables.next();
        this.takeUntilDestroyObservables.complete();

    }
}
