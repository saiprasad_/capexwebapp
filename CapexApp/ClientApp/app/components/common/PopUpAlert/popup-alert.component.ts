import { ChangeDetectorRef, Component, EventEmitter, Inject, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { merge, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { CommonService } from '../../globals/CommonService';
import { PopupOperation } from '../../globals/Model/AlertConfig';
import { FABRICS, FabricsNames, TypeOfEntity } from '../../globals/Model/CommonModel';
import { EntityTypes } from '../../globals/Model/EntityTypes';
import { ATTENTION, DELETE_PRODUCTION_RECORD, DOWNSTREAM_CHANGE, DO_NOT_DISCONNECT, Hierarchy_CHANGE, NESTED_UNDER_CHANGE } from '../Constants/AttentionPopupMessage';
import { FDCStateQuery } from '../../common/state/KeyValueState/fdc.state.query';
import { FDCStateConstantsKeys } from '../../common/state/KeyValueState/fdc.state.model';
import { filter } from 'rxjs/operators';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AppInsightsService } from '../app-insight/appinsight-service';

@Component({
  selector: 'popup-alert',
  templateUrl: 'popup-alert.component.html',
  styleUrls: ['./popup-alert.component.styles.scss']
})
export class PopUpAlertComponent implements OnInit, OnDestroy {


  @Output() CloseResponce = new EventEmitter<any>(true);
  @Output() emitResponse = new EventEmitter<any>(true);
  @Output() emitCancelResponse = new EventEmitter<any>(true);


  isConfigDataDelete: boolean = false;
  isProdectionDataDelete: boolean = false;
  loading: boolean = false;
  Meterpurpose;
  MultiSelectOption: any = [];
  infoData: any;
  Capabilities:any =[]
  header = '';
  content = [];
  testFormContent = {};
  OldMeterPurpose;
  isSubmit: boolean = false;
  subContent = [];
  operation;
  subOperation;
  EMMutialFabricDeleteMsg;
  deleteConfig = { 'DeleteConfigRecord': this.isConfigDataDelete, 'DeleteProdectionRecord': this.isProdectionDataDelete };
  meterPurpose = {  };
  contextmenuEntityName;
  OperationType;
  headerConfig: any = [];

  entityIcon = "Success"
  Successtrue: boolean = false;
  downUpstreamLocationOrnestedUnderData: any;
  bindLabel = 'EntityName';
  DownStreamLocationsChildren = [];
  // showDownstreamLocation: boolean = false;
  showDownstreamLocationmessge: any = "";
  ShowHideToggle = "No";
  EntityId
  myForm: FormGroup;
  downStream;
  nestedUnder;
  nestedUnderchildren;
  datetime: any;
  deletechangemessage: any = "";
  downstreamOrnestedunderLabel: any = "";
  property: any = "";
  FacilityCodeproperty: any = "";
  FacilityCodepropertylabel: any = "";
  maxDate;
  minDate: Date;
  IsUseing;
  FacilityCodeOptionsUsingtime:any = [];
  EntityType;
  nesteddelete: boolean = true;
  showNestedUnderFacilityCode;
  FacilityCodeChange;
  desktopSwitchUser: boolean = false;
  nestedUnderLabel: string;
  nestedUnderProperty: string;
  downstreamfacilitycode: any;
  HierarchyName: any = "";
  HierarchyDropDownData:any=[];
  selectedHierarchyDropDownData:any=[];

  constructor(public dialogRef: MatDialogRef<PopUpAlertComponent>, public fdcStateQuery: FDCStateQuery, public dialog: MatDialog, private cdRef: ChangeDetectorRef, @Inject(MAT_DIALOG_DATA) public data: any, public commonService: CommonService, private formBuilder: FormBuilder  ,public route: ActivatedRoute,public router: Router,private logger: AppInsightsService) {
    this.maxDate = this.commonService.maxDate;
    if (data.operation == "DeleteDownStreamLocation") {
      this.commonService.showDownstreamLocation = false;
      this.deletechangemessage = [DOWNSTREAM_CHANGE]
      this.downstreamOrnestedunderLabel = "Downstream Location";
      this.property = "DownstreamLocation"
      this.nestedUnderLabel = "Nested Under";
      this.nestedUnderProperty = "NestedUnder"
      this.FacilityCodeproperty = "FacilityCode";
      this.FacilityCodepropertylabel = "Facility Code";
      // this.showDownstreamLocationmessge = [SHOW_DOWNSTREAMLOCATION_UNDER_POPUP_MESSAGE]
      this.myForm = this.formBuilder.group({});
    }
    else if (data.operation == "DeleteNestedUnderFacilityCodeIsUsing" || data.operation == "DeleteFacilityCodeIsUsing") {
      this.deletechangemessage = [NESTED_UNDER_CHANGE]
      this.downstreamOrnestedunderLabel = "Nested Under";
      this.property = "NestedUnder";
      this.FacilityCodeproperty = "FacilityCode";
      this.FacilityCodepropertylabel = "Facility Code";
      // this.showDownstreamLocationmessge = [SHOW_NESTEDUNDER_FACILITYCODE_POPUP_MESSAGE]
      this.myForm = this.formBuilder.group({});
      let groupfromdate = this.formBuilder.control(null, Validators.required);
      let groupfacilitycode = this.formBuilder.control(null, Validators.required);
      this.myForm.addControl('FromDate', groupfromdate);
      this.myForm.addControl('FacilityCode', groupfacilitycode);
      this.myForm.markAsTouched();
      if (data.IsUseing) {
        
      }
    }
    else if (data.operation == "DeleteNestedUnderFacilityCode") {
      // this.showDownstreamLocationmessge = [SHOW_NESTEDUNDER_FACILITYCODE_POPUP_MESSAGE]
      this.deletechangemessage = [NESTED_UNDER_CHANGE];
      this.downstreamOrnestedunderLabel = "Nested Under";
      this.property = "NestedUnder";

      this.myForm = this.formBuilder.group({});
      let group = this.formBuilder.control({});
      group = this.formBuilder.control(null, Validators.required);
      // this.myForm.addControl('FromDate', group);
      this.myForm.markAsTouched();
    }
    else if (data.operation == 'updateVesselsLocationField') {
      this.myForm = this.formBuilder.group({});
      let group1 = this.formBuilder.control(null, Validators.required);
      this.myForm.addControl('NewLocation', group1);
      let group2 = this.formBuilder.control(null, Validators.required);
      this.myForm.addControl('NewLocationFromDate', group2);
      this.myForm.markAsTouched();
    }
    else if (data.operation == PopupOperation.DeletePeople) {
      this.myForm = this.formBuilder.group({});
    }
    else if (data.operation == PopupOperation.AlertConfirm) {
      this.myForm = this.formBuilder.group({});
      if (document && document.getElementsByClassName("cdk-overlay-container") && document.getElementsByClassName("cdk-overlay-container").length
        && document.getElementsByClassName("cdk-overlay-container")[0]["style"]) {
        document.getElementsByClassName("cdk-overlay-container")[0]["style"].setProperty('z-index', 1200);
      }
    }
    else if ( data.operation == PopupOperation.DeleteBasedonEffectiveDate) {
      this.deletechangemessage = data.subType ? [data.subType] : [DELETE_PRODUCTION_RECORD];
      this.commonService.showDownstreamLocation = false;
      this.myForm = this.formBuilder.group({});
    }else if(data.operation == PopupOperation.DeleteHierarachy){
      this.HierarchyName=data.EntityType;
      this.ShowHideToggle = "No"
      this.commonService.ShowHierarchyName = false;
      this.myForm = this.formBuilder.group({});
      this.HierarchyDropDownData=[];
      this.deletechangemessage = [Hierarchy_CHANGE(this.HierarchyName)]
      this.myForm.markAsTouched();
    }
    if (data && data.nesteddelete) {
      this.nesteddelete = data.nesteddelete;
    }
    if (data.Data) {
      this.EntityType = data.Data.EntityType;
      this.contextmenuEntityName = data.Data.EntityName;
    }
    else if (data.EntityName) {
      this.contextmenuEntityName = data.EntityName;
    }
    this.header = data.header;
    this.EntityId=data.EntityId;
    this.operation = data.operation;
    this.IsUseing = data.IsUseing;
    this.content = data.content;
    this.testFormContent = data.testFormContent;
    this.subContent = data.subContent;
    this.isSubmit = data.isSubmit;
    this.loading = data.loading;
    this.infoData = data.data;
    this.Capabilities=data.Capabilities;
    this.EMMutialFabricDeleteMsg=data.EMMutialFabricDeleteMsg;
    this.subOperation=data.subOperation;
    this.Meterpurpose = data.Element;
    this.OldMeterPurpose = data.OldMeterPurpose
    this.MultiSelectOption = data.MultiSelectOption;
    if (data.callFrom && data.callFrom == "DesktopSwitchUser") {
      this.desktopSwitchUser = true;
    }

    if (data.operation === ATTENTION && data.content && data.content[0] === DO_NOT_DISCONNECT) {
      setTimeout(() => {
        this.close();
      }, 10000);
    }

    if (data.operation == PopupOperation.Attention) {
      if (document && document.getElementsByClassName("cdk-overlay-container") && document.getElementsByClassName("cdk-overlay-container").length
        && document.getElementsByClassName("cdk-overlay-container")[0]["style"]) {
        document.getElementsByClassName("cdk-overlay-container")[0]["style"].setProperty('z-index', 1200);
      }
    }

    if (data.MinDate) {
      this.minDate = new Date(data.MinDate);
    }

    this.commonService.crudResponse.pipe(this.compUntilDestroyed()).subscribe((res: string) => {
      if (res == "SUCCESS") {
        if (this.commonService.deletedEntityName != null && this.headerConfig.length >= 0 && this.headerConfig[0]['title']) {
          this.headerConfig[0]['title'] = this.commonService.deletedEntityName + " Delete Entity.";
          this.commonService.deletedEntityName = null;
        }
        this.operation = "SUCCESS"
        this.loading = false;
        this.subContent = ['SUCCESS!'];
        this.entityIcon = "Success";
        this.Successtrue = true;

        // if (data.Data && data.Data.EntityType && this.commonService.isCreateNewEntity && this.operation == "DeleteConfirm") {
        //   this.commonService.deleteContextData.next(this.operation);
        // }

        setTimeout(() => { this.close() })
        this.cdRef.detectChanges();

      }
      else if (res == "ERROR" && this.operation != PopupOperation.Attention) {
        if (this.commonService.deletedEntityName != null && this.headerConfig.length >= 0 && this.headerConfig[0]['title']) {
          this.headerConfig[0]['title'] = this.commonService.deletedEntityName + " deletion failed.";
          this.commonService.deletedEntityName = null;
        }
        this.loading = false;
        this.subContent = ['Sorry! Operation Failed...'];
        setTimeout(() => { this.close() }, 3000)
      }
    });


    this.commonService.closeAllPopupForms$.pipe(this.compUntilDestroyed()).subscribe((res: string) => {
      if (this.commonService.currentRoutingSource == "V3 Home") {
        this.close();
      }
    });

    this.commonService.onSessionExpirePopupAlert$.pipe(this.compUntilDestroyed()).subscribe((res: any) => {
      this.openDialog(res, PopupOperation.AlertConfirm);
    })

  }
  operations = [];

  ngOnInit() {
    this.maxDate = this.commonService.maxDate;
    // this.commonService.IAlertPopupInitiAlize = true;


    this.headerConfig = [
      {
        'source': 'V3 CloseCancel',
        'title': this.header,
        'routerLinkActive': false,
        'id': 'title',
        'class': '',
        'float': 'left',
        'type': 'title',
        'show': true,
        'uppercase': 'Isuppercase'
      },
      {
        'source': 'wwwroot/images/V3 Icon set part1/V3 CloseCancel.svg',
        'title': 'Close',
        'routerLinkActive': false,
        'id': 'close',
        'class': 'icon21-21',
        'float': 'right',
        'type': 'icon',
        //'show': true
        'show': this.commonService.sessionSucess ? false : true
      },
      {
        'source': 'wwwroot/images/AlThing UI Icon-Button Set v1/FDC_Checkmark_Icon_Idle.svg',
        'title': 'Agree',
        'routerLinkActive': false,
        'id': 'save',
        'class': 'icon21-21',
        'float': 'right',
        'type': 'icon',
        'show': this.isSubmit
      }
    ];

    if (this.operation == 'updateVesselsLocationField') {
      this.bindDownUpStreamLocation([]);
      if (this.myForm && this.myForm.controls['NewLocation'] && this.infoData && this.infoData.NewLocation) {
        var namedata = this.downUpstreamLocationOrnestedUnderData.filter(obj => obj.EntityId == this.infoData.NewLocation);
        if (namedata && namedata.length > 0) {
          this.myForm.controls['NewLocation'].setValue(namedata[0].EntityName, { emitEvent: false });
          this.myForm.controls['NewLocation'].setErrors(null);
          this.myForm.markAsTouched();
        }
      }
    }
  }
  indexOf(children, Id) {
    for (let i = 0; i < children.length; i++) {
      if (children[i] === Id)
        return i;
    }
    return -1;

  }
  checkedChildren(event, operation) {
    if (event.checked) {
      this.operations.push(operation)
    }
    else {
      var indexof = this.indexOf(this.operations, operation);
      this.operations.splice(indexof, 1);
    }
  }
  closeSuccessPopup() {
    this.Successtrue = false;
    this.close();
  }
  


  submit(event?) {
    try {
      switch (this.operation) {
        case 'Attention':
          this.dialogRef.close(this.deleteConfig);
          break;
        case 'DeleteAll':
          this.headerConfig = this.headerConfig.filter(item => item.id != "save");
          this.deleteConfig.DeleteConfigRecord = this.deleteConfig.DeleteProdectionRecord = true;
          this.sendResponse(this.deleteConfig);
          this.isSubmit = true; //false;
          this.dialogRef.close(this.deleteConfig);
        
          break;

        case 'Delete':
        case 'DeleteConfirm':
          this.headerConfig = this.headerConfig.filter(item => item.id != "save");
          let obj={OperationType:this.operation,Capabilities:this.Capabilities}
          this.sendResponse(obj);
          this.dialogRef.close(this.deleteConfig);
          break;
         case PopupOperation.DeletePeople:
            if (this.myForm.valid) {
              this.sendResponse({UpdateChildren:this.ShowHideToggle,data:this.myForm.value});
              this.dialogRef.close(this.deleteConfig);
            }
            break;
        case 'AlertConfirm':
          this.headerConfig = this.headerConfig.filter(item => item.id != "save");
          this.isSubmit = true;//false;
          this.deleteConfig.DeleteConfigRecord = this.deleteConfig.DeleteProdectionRecord = true;

          if (this.Meterpurpose && event != "close") {
            this.dialogRef.close(this.deleteConfig);
            this.sendResponse(this.deleteConfig);
          } else if (this.Meterpurpose && event == "close") {
            this.dialogRef.close(this.meterPurpose);
            this.sendResponse(this.meterPurpose);
          }
          else if (event == 'save') {
            if (Object.keys(this.myForm.value as object).length === 0) {
              this.sendResponse(true);
            } else {
              this.sendResponse({data:this.myForm.value});
            }
            this.dialogRef.close(this.deleteConfig);
          }
          else {
            this.close();
          }
          break;
        case 'PermissionAlertConfirm':
          this.headerConfig = this.headerConfig.filter(item => item.id != "save");
          this.isSubmit = true;//false;
          this.deleteConfig.DeleteConfigRecord = this.deleteConfig.DeleteProdectionRecord = true;
          this.sendResponse(this.data);
          if (this.Meterpurpose) {
            this.dialogRef.close(this.deleteConfig);
          }
          else {
            this.close();
          }
          break;
      }
    } catch (e) {
      console.error('Exception in logout() of UseractionComponent  at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  _keyPress(event) {
    try {
      const pattern = /^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$/;
      if (!pattern.test(event.target.value)) {
        event.target.value = "";
        this.myForm.controls.FromDate.setValue(null ,{emitEvent : false})
        this.myForm.controls.FromDate.setErrors({ "invalid": true });
      }
    }
    catch (e) {
      console.error("Exception in Date Field KeyPress in PopUpAlertComponent at time:" + new Date().toString() + ".Exception is :" + e);
    }
  }


  hasSubArray(array, subarray) {
    return subarray.every((i => v => i = array.indexOf(v, i) + 1)(0));
  }
  sendResponse(data: any) {
    this.emitResponse.emit(data);

  }
  sendCancelResponse(res?) {
    this.emitCancelResponse.emit(res);
  }

  headerClickEvent(event) {
    this.data["eventId"] = event.id;
    if (event.id == 'close') {
      this.commonService.capabilityFabricId = this.commonService.getCapabilityId(this.commonService.lastOpenedFabric);
      this.close();
    }
    else if (event.id == 'save') {
      this.submit('save');
    }
  }
  close(): void {
    this.Successtrue = false;
    this.sendCancelResponse(this.data);
    if (this.Meterpurpose) {
      this.dialogRef.close(this.OperationType);
    }
    this.dialogRef.close(this.OperationType);
    // if (this.commonService.Step2Disable == false) {
    //   this.commonService.Step2Disable = true;
    // }
    // if (this.commonService.lastOpenedFabric == "bifabric") {
    //   // this.commonService.RenameAlert = false;
    // }
  }
  showCompanyAndOfficesField(event: any) {
    if (event.checked) {
      this.ShowHideToggle = "Yes"
      this.data.payload.forEach(element => {

        if(element.actualname==EntityTypes.COMPANY){
          this.bindOptionsForPeople(element)
        }

        let propertyName=element.actualname
        let group = this.formBuilder.control({});
        group = this.formBuilder.control(null, Validators.required);
        this.myForm.addControl(propertyName, group);
        group.valueChanges.pipe(debounceTime(500)).pipe(distinctUntilChanged()).subscribe(propertyVal => {
          try {
            if (propertyVal) {
             if(propertyName==EntityTypes.COMPANY){
              // this.myForm.controls['Office'].setValue(null,{emitEvent:false})
              this.data.payload.forEach(element => {
                if(element.actualname==EntityTypes.OFFICE)
                this.bindOptionsForPeople(element,propertyVal.EntityId)
              });
             }
            }
          }
          catch (e) {
            console.error('Exception in Popup=alert.component() of ValuesChanges subscriber of popup-alert.component in people delete at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
          }
        })
      });
      this.myForm.markAsTouched();
      this.bindDownUpStreamLocation([]);
    }
    else {
      this.ShowHideToggle = "No"
      this.myForm = this.formBuilder.group({});
    }
  }

  bindOptionsForPeople(element,propertyVal?){
    this.commonService.getPeopleEntitiesFromStoreOrBackend$("EntityType", [element.actualname],d=>true, d => {
      return { "EntityId": d.EntityId, "EntityType": d.EntityType, "EntityName": d.EntityName,Parent:d.Parent }
    }, FABRICS.PEOPLEANDCOMPANIES).pipe(this.compUntilDestroyed()).subscribe((res:any) => {
      if (res) {
        if(propertyVal)
        res=res.filter((d:any) => d.Parent&&d.Parent[0]&&d.Parent[0].EntityId==propertyVal)
        res=res.filter((d:any)=>d.EntityId!=this.EntityId)
        element.options=res;
      }
    });
  }
  showDownstreamLocationField(event: any) {
    if (event.checked) {
      this.ShowHideToggle = "Yes"
      this.commonService.showDownstreamLocation = true;

      let group1 = this.formBuilder.control({});
      group1 = this.formBuilder.control(null, Validators.required);
      this.myForm.addControl('FromDate', group1);

      let group2 = this.formBuilder.control({});
      group2 = this.formBuilder.control(null, Validators.required);
      this.myForm.addControl(this.property, group2);
      if (this.nestedUnderProperty) {
        let group3 = this.formBuilder.control({});
        group3 = this.formBuilder.control(null, Validators.required);
        this.myForm.addControl(this.nestedUnderProperty, group3);
      }
      if (this.FacilityCodeproperty) {
        let group4 = this.formBuilder.control({});
        group4 = this.formBuilder.control(null, Validators.required);
        this.myForm.addControl(this.FacilityCodeproperty, group4);
      }
      this.myForm.markAsTouched();
      this.bindDownUpStreamLocation([]);
    }
    else {
      this.ShowHideToggle = "No"
      this.commonService.showDownstreamLocation = false;
      this.myForm = this.formBuilder.group({});
    }
  }

  showFromDateField(event: any) {
    if (event.checked) {
      this.ShowHideToggle = "Yes"
      this.commonService.showDownstreamLocation = true;

      let group1 = this.formBuilder.control({});
      group1 = this.formBuilder.control(null, Validators.required);
      this.myForm.addControl('FromDate', group1);
    }
    else {
      this.ShowHideToggle = "No"
      this.commonService.showDownstreamLocation = false;
      this.myForm = this.formBuilder.group({});
    }
  }

  showNestedUnderfacilityCode(event: any) {
    if (event.checked) {
      this.ShowHideToggle = "Yes"
      this.showNestedUnderFacilityCode = true;
      let group = this.formBuilder.control({});
      group = this.formBuilder.control(null, Validators.required);
      this.myForm.addControl(this.property, group);
      this.myForm.markAsTouched();
      this.bindDownUpStreamLocation([]);
    } else {
      this.ShowHideToggle = "No"
      this.showNestedUnderFacilityCode = false;
      if (this.myForm && this.myForm.controls && this.myForm.controls.hasOwnProperty(this.property)) {
        this.myForm.removeControl(this.property);
      }
    }

  }
  facilityCodeNestedChildren(data) {
    if (data && data.children)
      data.children.forEach(element => {
        this.nestedUnderchildren.push(element);
        this.facilityCodeNestedChildren(element)
      });

  }

  bindDownUpStreamLocation(typeOf) {
    
  }

  selectedLocationAndDate(event, type) {
    if (event && type == 'NewLocation') {
      this.infoData.NewLocation = event.EntityId;
    }
    if (event && type == 'FromDate') {
      this.infoData.NewLocationFromDate = event.target.value;
    }
  }

  selectedDownstreamLocationName(event) {
    if (event)
      this.downStream = event;
    else if (event == undefined)
      this.downStream = null;

    if (this.downStream == null) {
      this.FacilityCodeOptionsUsingtime = [];
      this.myForm.controls[this.FacilityCodeproperty].setValue(null, { emitEvent: false });
    }else{
      this.nestedUnder = this.downStream;
      let entitydata = this.commonService.getEntitiesById(this.downStream.EntityId)
      this.downstreamfacilitycode = entitydata ? entitydata.FacilityCode : null;
      let dacdata = null;
      // this.UpdateDownstreamFacilityCodeOptions(this.downStream.EntityId);
      if (this.FacilityCodeOptionsUsingtime){
        let options = this.FacilityCodeOptionsUsingtime;
        dacdata = options.filter(d => d.EntityType == EntityTypes.FACILITYCODE && d.EntityId == this.downstreamfacilitycode);
        this.FacilityCodeOptionsUsingtime = [];
        options.forEach(data => {
          let isExist = this.FacilityCodeOptionsUsingtime.filter(d => d && d.EntityId == data.EntityId);
          if (isExist == undefined || isExist.length == 0) {
            this.FacilityCodeOptionsUsingtime.push(data);
          }
        });
      }

      if (this.myForm.controls[this.nestedUnderProperty])
        this.myForm.controls[this.nestedUnderProperty].setValue(this.nestedUnder.EntityName, { emitEvent: false });
      if (this.myForm.controls[this.FacilityCodeproperty] && dacdata[0] && dacdata[0].EntityName)
        this.myForm.controls[this.FacilityCodeproperty].setValue(dacdata[0].EntityName, { emitEvent: false });
    }
  }

  

  selectedNestedUnderField(event) {
    if (event)
      this.nestedUnder = event;
    else if (event == undefined)
      this.nestedUnder = null;
  }

  selectedNestedUnder(event) {
    if (event && event.EntityType == EntityTypes.FACILITYCODE) {
      this.nestedUnder = event;
    } else {
      this.nestedUnder = null
    }
  }

  selectedfacilitycodeWithisUsing(event) {
    if (event && event.EntityType == EntityTypes.FACILITYCODE) {
      this.FacilityCodeChange = event;
    } else {
      this.FacilityCodeChange = null;
    }
  }

  // PushDownstreamLocations(node) {
  //   var entity = { "EntityId": node.EntityId, "EntityType": node.EntityType, "EntityName": node.EntityName, };
  //   var index = this.DownStreamLocationsChildren.findIndex(element => element.EntityId == node.EntityId);
  //   if (index == -1)
  //     this.DownStreamLocationsChildren.push(entity);
  //   // if (node.children && node.children.length > 0) {
  //   //   node.children.forEach(obj => this.PushDownstreamLocations(obj));
  //   // }
  // }

  /**
   *
   * Cleanup just before Angular destroys the directive/component.
   * Unsubscribe Observables and detach event handlers to avoid memory leaks
   *
   * @example example of ngOnDestroy() method
   *  ngOnDestroy(){
   *     //todo
   *    }
   */
  ngOnDestroy() {
    // this.commonService.IAlertPopupInitiAlize = false;
    //modified cdk z-index
    if (document && document.getElementsByClassName("cdk-overlay-container") && document.getElementsByClassName("cdk-overlay-container").length
      && document.getElementsByClassName("cdk-overlay-container")[0]["style"]) {
      document.getElementsByClassName("cdk-overlay-container")[0]["style"].setProperty('z-index', 1000);
    }

    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }

  takeUntilDestroyObservables=new Subject();
      compUntilDestroyed():any {
          return takeUntil(this.takeUntilDestroyObservables);
      }

  openDialog(messageToshow, Operation?): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: Operation != undefined ? true : false,
        content: [messageToshow],
        subContent: [],
        operation: Operation != undefined ? Operation : PopupOperation.Attention
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.id = messageToshow;
      matConfig.disableClose = true;

      this.commonService.sessionSucess = true;
      this.dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);

      this.dialogRef.componentInstance.emitResponse.subscribe((res) => {
        this.commonService.logout();
      })
    }
    catch (e) {
      console.error('Exception in openDialog() of messagingservice at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  getCSSClass() {
    if (this.loading) {
      return "loaderStyling";
    } else if (this.desktopSwitchUser) {
      return "desktopSwitchUser";
    } else {
      return "dialogBox";
    }
  }

  public getHeaderSwitchUserCSSClass(): string {
    if (this.desktopSwitchUser) {
      return "header-popup-SwitchUser";
    } else {
      return "header-popup";
    }
  }
    /**
   * NG-Select Options select and Highlight
   * @param $event 
   */
  onOptionClick($event) {
    if ($event && $event.target && $event.target.select) {
      $event.target.select();
    }
  }
  

  toggleOperation(event: any) {
    if (event.checked) {
      this.ShowHideToggle = "Yes"
      this.commonService.ShowHierarchyName = true;

      let group1 = this.formBuilder.control({});
      group1 = this.formBuilder.control(null, Validators.required);
      this.myForm.addControl(this.data.EntityType, group1);
      this.myForm.markAsTouched();
      this.binDropDownForHierarchy(this.data.EntityType);
    }
    else {
      this.ShowHideToggle = "No"
      this.commonService.ShowHierarchyName = false;
      this.myForm = this.formBuilder.group({});
      this.HierarchyDropDownData=[];
      this.selectedHierarchyDropDownData=undefined;
    }
  }


  selectedDistrict(event) {
    if (event)
      this.selectedHierarchyDropDownData = event;
    else if (event == undefined)
      this.selectedHierarchyDropDownData = null;
  }

  binDropDownForHierarchy(propertyName) {
    try {

      this.commonService.getEntititesFromStore$('TypeOf',
      [propertyName],
      d => true,
      d => { return { "EntityId": d.EntityId, "EntityType": d.EntityType, "EntityName": d.EntityName } })
      .pipe(this.compUntilDestroyed())
      .subscribe((entities: any) => {
        let options = entities.filter(data => data['EntityId'] != this.data.EntityId);
        this.HierarchyDropDownData = options;
        this.cdRef.markForCheck();
      });

      // switch (propertyName) {
      //   case EntityTypes.DISTRICT:
      //     this.commonService.getEntititesFromStore$('TypeOf',
      //       [propertyName],
      //       d => true,
      //       d => { return { "EntityId": d.EntityId, "EntityType": d.EntityType, "EntityName": d.EntityName } })
      //       .pipe(this.compUntilDestroyed())
      //       .subscribe((entities: any) => {
      //         this.HierarchyDropDownData = entities;
      //         this.cdRef.markForCheck();
      //       });
      //     break;
      //     case EntityTypes.AREA:
      //       break;
      //       case EntityTypes.FIELD:
      //       break;
      //   default:
      //     break;
      // }
    }
    catch (e) {
      console.error('Exception in binDropDownForHierarchy() of popup-alert at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }
}

