import { NgModule } from '@angular/core';
import { AngularMaskDirective } from './mask.directive'
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [AngularMaskDirective],
  exports: [AngularMaskDirective],
  imports: [CommonModule]
})
export class AngularMaskModule { }
