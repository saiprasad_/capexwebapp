import { MatDialogConfig } from '@angular/material/dialog';
import { PopUpAlertComponent } from '../../../components/common/PopUpAlert/popup-alert.component';
import { PopupOperation } from '../../../components/globals/Model/AlertConfig';

export class alertPopUp {

  public openDialog(messageToshow, dialog): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: false,
        content: [messageToshow],
        subContent: [],
        operation: PopupOperation.Attention
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      let dialogRef = dialog.open(PopUpAlertComponent, matConfig);

      dialogRef.afterClosed().subscribe((status) => {
        if (status) {
        }
      });
    }
    catch (ex) {
      console.error(ex);
    }
  }
}
