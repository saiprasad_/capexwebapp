import { RouterModule, Routes } from '@angular/router';
import { AddNewEntityDetailsComponent } from './AddNewEntityDetials/add-new-entity-details-component';
import { AddNewEntityDeactivateGaurdService } from './services/add-new-entity-can-deactivate-guard.service';

const routes: Routes = [
  {
    path: '', component: AddNewEntityDetailsComponent, canDeactivate:[AddNewEntityDeactivateGaurdService], children: [
    ]
  }
];
export const AddnewRouting = RouterModule.forChild(routes);
