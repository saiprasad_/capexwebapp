// AoT compilation doesn't support 'require'.
import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { UserStateConstantsKeys } from 'ClientApp/webWorker/app-workers/commonstore/user/user.state.model';
import { Subject, Observable, of } from "rxjs";
import { takeUntil, filter } from "rxjs/operators";
import { UsersStateQuery } from '../../../../../webWorker/app-workers/commonstore/user/user.state.query';
import { AppConsts } from '../../../common/Constants/AppConsts';
import { FILL_REQUIRED_FIELDS, LOADING_MESSAGE } from '../../../common/Constants/AttentionPopupMessage';
import { PopUpAlertComponent } from '../../../common/PopUpAlert/popup-alert.component';
import { PeopleActivitiesService } from '../../../fabrics/PeopleFabric/services/people-activities-service';
import { CommonService } from '../../../globals/CommonService';
import { PopupOperation } from '../../../globals/Model/AlertConfig';
import { FABRICS, FabricsNames, MessageKind, Datatypes } from '../../../globals/Model/CommonModel';
import { AppInsightsService } from 'ClientApp/app/components/common/app-insight/appinsight-service';
import { IModelMessage, MessageModel } from 'ClientApp/app/components/globals/Model/Message';
import { guid } from '@datorama/akita';
// import { type } from 'os';
import { WORKER_TOPIC } from 'ClientApp/webWorker/app-workers/shared/worker-topic.constants';

@Component({
  selector: 'add-new-entity-details',
  templateUrl: './add-new-entity-details-component.html',
  styleUrls: ['./add-new-entity-details-component.styles.scss'],
})

export class AddNewEntityDetailsComponent implements OnDestroy, OnInit {

  NewEntityId;
  ParentData: any = { isLoaded: false }
  securityAdmin: boolean = false;
  closeFormDelete: boolean = false;
  dialogRef
  parentList: any = [];
  fabricName;
  messageData;
  @Input() myForm: FormGroup;
  listOfChildren: any = [];
  @Input() schema: any;
  @Input() data: any;
  @Input() properties: any;
  @Input() EntityType;
  roleSchema: any;
  HeaderSchema = null;
  isHeaderFormValid: boolean;
  EntitySchema: any;
  IsFormValid
  formName
  isNewEntity
  FormDataOutPut: any = {};
  headerConfig = [
    {
      'source': 'V3 General',
      'title': 'Create New Entity',
      'routerLinkActive': false,
      'id': 'title',
      'class': 'icon21-21',
      'float': 'left',
      'type': 'title',
      'show': true,
      'uppercase': 'Isuppercase'
    }, {
      'source': 'V3 CloseCancel',
      'title': 'Close',
      'routerLinkActive': false,
      'id': 'close',
      'class': 'icon21-21',
      'float': 'right',
      'type': 'icon',
      'show': true
    },
    {
      'source': 'FDC_Checkmark_Icon_Idle',
      'title': 'Save',
      'routerLinkActive': false,
      'id': 'save',
      'class': 'icon21-21',
      'float': 'right',
      'type': 'icon',
      'show': true
    }
  ];

  RouteParam: any = {};
  Type
  NewProperties = {}
  formGroupControl;
  
  takeUntilDestroyObservables = new Subject();
  constructor(public commonService: CommonService, public usersStateQuery: UsersStateQuery, public router: Router, public formBuilder: FormBuilder,
    public PeopleService: PeopleActivitiesService, public cdRef: ChangeDetectorRef, public dialog: MatDialog,
    public route: ActivatedRoute, public logger: AppInsightsService) {
    this.myForm = this.formBuilder.group({});
    this.commonService.popupformclose = false;
    this.NewEntityId = this.commonService.GetNewUUID();//this.fDCService.newEntityId;
    this.commonService.resetControl.pipe(this.compUntilDestroyed()).subscribe((res) => {
      this.myForm.reset();
    })
  }

  ngOnInit() {
    try {
      setTimeout(() => {
        this.commonService.layoutJSON.RightHeader = false;
      });
      this.commonService.isCreateNewEntity = true;
      this.router.events.filter((event: any) => event && event instanceof NavigationEnd).pipe(this.compUntilDestroyed()).subscribe((event: any) => {
        try {
          let url = event.url;
          this.fabricName = this.commonService.getFabricNameByUrl(url);
          if (this.fabricName == FabricsNames.ENTITYMANAGEMENT) {
            this.fabricName = this.commonService.getFabricNameByTab(this.route.queryParams['_value'].tab);

          }
        }
        catch (e) {
          console.error(e);
        }
      });
      this.Type = this.route.queryParams['_value']['Type']

      this.route.queryParams.pipe(this.compUntilDestroyed()).subscribe((param: any) => {
        this.fabricName = this.commonService.getFabricNameByUrl(this.router.url);
        if (this.fabricName == FabricsNames.ENTITYMANAGEMENT) {
          if (Object.keys(this.RouteParam).length > 0 && this.RouteParam.tab && param.tab && this.RouteParam.tab != param.tab) {
            this.RouteParam.tab = param.tab
            this.CloseForm();
          }
          this.RouteParam = JSON.parse(JSON.stringify(param));
          this.fabricName = this.commonService.getFabricNameByTab(this.route.queryParams['_value'].tab);
        }
      });

    }
    catch (e) {
      console.error('Exception in ngOnInit() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
      this.commonService.appLogException(new Error('Exception in  ngOnInit() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  ClickEvent(ev) {
    try {
      switch (ev.id) {
        case 'save':
          if (!this.myForm.invalid) {
            if (this.formGroupControl.controls[this.NewEntityId] && !this.ParentData["isLoaded"]) {
              var message = LOADING_MESSAGE;
              this.openAlertDialog(message, "", false);
            }
            else {
              this.Submit();
            }
          } else {
            var message = FILL_REQUIRED_FIELDS;
            this.openDialog(message);
          }
          break;
        case 'close':
          // this.closebutton = true;
          this.CloseForm();
          break;
        case 'ChangeFieldInNewEntityForm':
          break;
      }
    }
    catch (e) {
      console.error(e);
    }
  }

  openDialog(messageToshow, Operation?): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: Operation != undefined ? true : false,
        content: [messageToshow],
        subContent: [],
        operation: Operation != undefined ? Operation : PopupOperation.Attention
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.id = messageToshow;
      matConfig.disableClose = true;
      if (!this.dialog.getDialogById(messageToshow)) {
        this.dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
        this.dialogRef.componentInstance.emitCancelResponse.subscribe((res) => {

        })
      }
    }
    catch (e) {
      console.error(e);
    }
  }

  openAlertDialog(messageToshow, addnew?, issubmit = true): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: issubmit ? true : false,
        content: [messageToshow],
        subContent: [],
        operation: PopupOperation.AlertConfirm
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      this.commonService.RerouteToAddNew = false;

      this.dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
      this.dialogRef.componentInstance.emitResponse.subscribe((data: any) => {
        this.commonService.popupformclose = false;
        if (addnew == 'addnew') {
          this.listOfChildren = [];
          if (this.commonService.createbyContextmenu) {
            this.commonService.isPlusAddForm = false;


          }
          else {
            if (this.formGroupControl && this.formGroupControl.controls && this.formGroupControl.controls[this.NewEntityId]) {
              this.formGroupControl.controls[this.NewEntityId].markAsUntouched();
              this.formGroupControl.controls[this.NewEntityId].markAsPristine();
            }
          }
        }
      });
      this.dialogRef.componentInstance.emitCancelResponse.subscribe((data: any) => {

        this.usersStateQuery.add(UserStateConstantsKeys.ContextMenuData, null);

      });
    }
    catch (e) {
      console.error('Exception in openAlertDialog() of create-entity.component at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  eventFromChild(event) {
    try {
      this.logger.logEvent('Form Event', this.myForm.value);
      // let dataValue = this.myForm.value;
      // if (this.fabricName == FABRICS.PEOPLEANDCOMPANIES) {
      //   //get formschema
      //   this.EntityType = dataValue["EntityType"].Value;
      //   this.isHeaderFormValid = true
      //   this.commonService.isNewEntityType = true;
      //   this.commonService.isCreateNewEntity = true;
      //   this.isNewEntity = true
      //   this.formName = "People";
      //   this.getEntityTypeSchema();
      // }
      if (event) {
        if (this.listOfChildren.length > 0 && (this.myForm.value["EntityType"] == null || this.listOfChildren[0].EntityType != this.myForm.value["EntityType"]["Value"] || this.myForm.value["Parent"] == null)) {
          this.listOfChildren = [];
          //this.myForm.removeControl(this.NewEntityId);
        }
      }
      else {
        // this.listOfChildren = []; // for Parent Changing Restting form
        this.createNestedForm();
      }
    }
    catch (e) {
      console.error(e);
    }
  }
  getEntityTypeSchema() {
    let imessageModel = this.PeopleService.payloadForReadingOptionsData("PeopleSchema", this.EntityType, this.fabricName);
    imessageModel.DataType = "People";
    let payload = { "EntityType": this.EntityType, "EntityTyId": imessageModel.EntityID, "FormType": "Configuration", "FormCapability": "People", "Info": "", "Fabric": "EntityManagement" }
    imessageModel.Payload = JSON.stringify(payload);
    // this.commonService.readAddNewSchema(this.fabricName).pipe(this.compUntilDestroyed()).filter((d: any) => d).subscribe(res => {
    //   console.log(res);
    //   this.EntitySchema = res;
    //   //this.addNewEntityReceiveBackend(res);
    // });
    this.commonService.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, imessageModel).subscribe((res: any) => {
      if (res["Data"]) {
        if (res["Data"]["Data"] == "Unauthorized") {
          this.logger.logException(res);
        }
      } else {
        this.messageData = typeof res === 'string' ? JSON.parse(res) : res;
        console.log(this.messageData);
        let schemaData = JSON.parse(this.messageData["Payload"]);
        this.EntitySchema = JSON.parse(schemaData["Schema"].toString());
      }
    });
    // this.commonService.getFormSchemaDataFromBackend(imessageModel).subscribe((schema) => {
    //   console.log('CompanySchema:', schema);

    //   this.EntitySchema = JSON.parse(schema["Schema"]);
    //   console.log(schema["Schema"]["layout"]);
    //   console.log("EntitySchema : ", this.EntitySchema);

    // });
    // this.commonService.getFormSchemaDataFromBackend(imessageModel).subscribe((schema) => {
    //   console.log('CompanySchema:', schema);
    //   this.EntitySchema = schema;
    // });

  }
  isFormValidEvent(event) {
    this.IsFormValid = event;
  }
  submitForm(ev) {
    //this.commonService.visurformgenSubs$.next();
    if (ev.id == "close") {
      if(this.listOfChildren.length==0){
          this.CloseForm();
      }
        this.commonService.isclose = true;
    } //else {
      this.commonService.triggerSubmitOnFormGenerator$.next();
     // this.CloseForm();
    //}
  }
 
  CloseForm() {
    try {
      this.commonService.switchButton = undefined;
      if (this.commonService.getFabricNameByUrl(this.router.url) == FabricsNames.ENTITYMANAGEMENT)
        this.router.navigate(['../../'], { relativeTo: this.route, queryParams: { 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
      else
        this.router.navigate(['../../../'], { relativeTo: this.route, queryParams: { 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });

    }
    catch (e) {
      console.error('Exception in CloseForm() of AddNewComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in CloseForm() of AdddNewComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  getAddNewHeaderSchema() {
    this.commonService.readAddNewSchema(this.fabricName).pipe(this.compUntilDestroyed()).filter((d: any) => d).subscribe(res => {
      console.log(res);
      this.HeaderSchema = res;
    })
  }

  debug(data) {
    let fabric = FABRICS.PEOPLEANDCOMPANIES.toLowerCase() + "_em_person_schedule";
    let msgFabric = data.Fabric.toLowerCase().toString();

    if (msgFabric == fabric.toString()) {
      return true;
    }
    return false;
  }
  recieveMessageFromMessagingService(message: MessageModel) {
    try {
      console.log(message);
      this.HeaderSchema = message.Payload;
    }
    catch (e) {
      console.log("Exception : " + e)
    }
  }
  Submit() {
    try {
      this.commonService.popupformclose = false;
      this.commonService.triggerSubmitOnFormGenerator$.next();
      if (this.formGroupControl.valid) {
        this.formGroupControl.markAsUntouched();
        this.formGroupControl.markAsPristine();
      }
    }
    catch (e) {
      console.error(e);
    }
  }

  createNestedForm() {
    try {
      let dataValue = this.myForm.value;
      if (this.listOfChildren.length > 0 && (this.myForm.value["EntityType"] == null || this.listOfChildren[0].EntityType != this.myForm.value["EntityType"]["Value"])) {
        this.listOfChildren = [];
        this.myForm.removeControl(this.NewEntityId);
      }
      else if (this.listOfChildren.length > 0 && (this.fabricName == FABRICS.PEOPLEANDCOMPANIES)) {
        if (this.myForm.value["Parent"]["id"] != null && (this.listOfChildren[0].Parent["id"] != this.myForm.value["Parent"]["id"])) {
          this.listOfChildren = [];
          this.myForm.removeControl(this.NewEntityId);
        }
      }
      else if (this.listOfChildren.length > 0) {
        return;
      }

      switch (this.fabricName) {
        case FABRICS.PEOPLEANDCOMPANIES:
          this.commonService.isNewEntityType = true;
          this.commonService.isCreateNewEntity = true;
          let entitytype = dataValue["EntityType"].Value;
          this.listOfChildren.push({ "EntityName": '', "EntityType": entitytype, "EntityId": this.NewEntityId });
          this.formGroupControl = this.createfullGroup();

          break;
      }
    } catch (e) {
      console.error(e);
    }
  }

  createfullGroup() {
    try {
      const group = this.formBuilder.group({});
      this.listOfChildren.forEach(control => group.addControl(control.EntityId, this.formBuilder.group(control, { updateOn: "blur" })));
      return group;
    } catch (e) {
      console.error(e);
    }
  }

  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables);
  }

  canDeactivate(): Observable<boolean> {
    return of(true);
  }

  ngOnDestroy() {
    try {

      this.commonService.isPlusAddForm = false;
      this.commonService.switchButton = undefined;
      this.commonService.isCreateNewEntity = false;
      this.commonService.Addformclose = false;
      this.commonService.popupformclose = false;
      this.closeFormDelete = false;

      if (this.myForm) {
        this.commonService.allExpandState = false;
        if (this.data) {
          if (this.data.EntityType) { this.data.EntityType.Value = ""; }
          if (this.data.Name) { this.data.Name.Value = ""; }
        }
        this.listOfChildren = [];
        this.commonService.FormSchemaData$ = undefined;
      }
      this.takeUntilDestroyObservables.next();
      this.takeUntilDestroyObservables.complete();
    } catch (e) {
      console.error(e);
    }
  }
}