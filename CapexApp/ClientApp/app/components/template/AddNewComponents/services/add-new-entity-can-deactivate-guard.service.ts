import { Injectable } from "@angular/core";
import { CanDeactivate } from "@angular/router";
import { Observable } from "rxjs";
import { AddNewEntityDetailsComponent } from "../AddNewEntityDetials/add-new-entity-details-component";

@Injectable()
export class AddNewEntityDeactivateGaurdService implements CanDeactivate<AddNewEntityDetailsComponent>{
     canDeactivate(component: AddNewEntityDetailsComponent):Observable<boolean>{
       if (component.Type != "facilitycode" && (component.myForm.dirty || component.commonService.popupformclose) && (!component.closeFormDelete)){
             return component.canDeactivate()
         }
         else
             return Observable.of(true);
     }
}
