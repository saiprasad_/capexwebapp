import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DESKTOP_LOGOUT_HEADERTEXT, WEB_LOGOUT } from 'ClientApp/app/components/common/Constants/AttentionPopupMessage';
import { CommonService } from '../../../globals/CommonService';

@Component({
  selector: 'dialog-alert',
  templateUrl: 'dialog-alert.component.html',
  styleUrls: ['./dialog-alert.component.styles.scss']
})
export class DialogAlertComponent {
  header;
  headerConfig;
  content = [];
  isLogout = false;
  subContent;
  ArrayContent = [];

  constructor(public dialogRef: MatDialogRef<DialogAlertComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public commonService: CommonService) {
    try {
      this.header = data.header;
      this.content = data.content;
      this.subContent = data.subContent;
      this.ArrayContent = data.ArrayContent;
      this.isLogout = this.header == 'Logout' ? true : false;

      this.headerConfig = [
        {
          'source': '',
          'title': this.header,
          'routerLinkActive': false,
          'id': 'general',
          'class': 'title',
          'float': 'left',
          'type': 'title',
          'show': true,
          'uppercase':'Isuppercase'
        },
        {
          'source': 'wwwroot/images/V3 Icon set part1/V3 CloseCancel.svg',
          'title': 'Close',
          'routerLinkActive': false,
          'id': 'close',
          'class': 'icon21-21',
          'float': 'right',
          'type': 'icon',
          'show': true
        },
        {
          'source': 'wwwroot/images/AlThing UI Icon-Button Set v1/FDC_Checkmark_Icon_Idle.svg',
          'title': 'Agree',
          'routerLinkActive': false,
          'id': 'submit',
          'class': 'save',
          'float': 'right',
          'type': 'icon',
          'show': true
        }
      ];
    }
    catch (e) {
      console.error('Exception in constructor of dialog.alert.component  at DialogAlert time ' + new Date().toString() + '. Exception is : ' + e);
    }
   
  }

  submit() {
    try {
      if (this.header == WEB_LOGOUT || this.header == DESKTOP_LOGOUT_HEADERTEXT){
        this.commonService.logout();
        this.close();
      }
        

    } catch (e) {
      console.error('Exception in logout() of DialogBoxComponent i.e. (dialog-alert.component.ts)  at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  clickEvent(data) {
    try {
      switch (data.id) {
        case 'submit':
          this.submit();
          break;
        case 'close':
          this.close();
          break;
      }
    } catch (e) {
      console.error('Exception in ClickEvent() of DialogBoxComponent at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }
  close(): void {
    this.dialogRef.close(false);
  }
}
