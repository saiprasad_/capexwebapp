import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConsts, FormOperationTypes, FORMS, ROUTE } from 'ClientApp/app/components/common/Constants/AppConsts';
import { FABRICS, FabricsNames, TreeOperations, TreeTypeNames } from 'ClientApp/app/components/globals/Model/CommonModel';
import { EntityTypes } from 'ClientApp/app/components/globals/Model/EntityTypes';
import { FabricRouter } from 'ClientApp/app/components/globals/Model/FabricRouter';
import { AppConfig } from 'ClientApp/app/components/globals/services/app.config';
import { produce } from 'immer';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { DISCARD_CHANGES } from '../../../../common/Constants/AttentionPopupMessage';
import { PopUpAlertComponent } from '../../../../common/PopUpAlert/popup-alert.component';
import { CommonService } from '../../../../globals/CommonService';
import { PopupOperation } from '../../../../globals/Model/AlertConfig';
import { ConstructionRightMenus } from '../../../../globals/Model/CommonModel';
@Component({
  selector: 'side-header',
  templateUrl: './side-header.component.html',
  styleUrls: ['./side-header.component.scss']
})
export class SideHeaderComponent implements OnInit, OnDestroy {

  @ViewChild(MatMenuTrigger, { static: false }) contextMenu: MatMenuTrigger;
  // isSubHeaderEnable = false;
  queryParams: any;
  fabricName: any;
  isDifferentTab = false;
  subscription;
  showSGContextMenu: boolean = true;
  LIST_CONSTANT_CONTEXTMENU = 'View Lists';
  DEFAULT_CONSTANT_CONTEXTMENU = 'Set as Default';
  ENTITIES_CONSTANT_CONTEXTMENU = 'View Entities';
  contextMenuOptions: string[] = [];
  ConstructionContextMenus: string[] = [ConstructionRightMenus.Schedule, ConstructionRightMenus.TaggedItems, ConstructionRightMenus.PhysicalInstances, ConstructionRightMenus.Materials, ConstructionRightMenus.Documents, ConstructionRightMenus.Turnover];

  /**
   * dialogRef
   */
  dialogRef: any;
  takeUntilDestroyObservables = new Subject();

  constructor(public commonService: CommonService, public appConfig: AppConfig, public dialog: MatDialog, private route: ActivatedRoute, private router: Router) {
    this.commonService.openListTab
      .pipe(debounceTime(400), distinctUntilChanged())
      .pipe(this.compUntilDestroyed()).subscribe((data) => {
        this.onClick(data);
      });

    this.commonService.changeSecurityLeftTreeBasedOnMode
      .pipe(debounceTime(400), distinctUntilChanged())
      .pipe(this.compUntilDestroyed()).subscribe((data) => {
        this.onClick(data);
      });
  }

  ngOnInit(): void {
    try {
      this.route.queryParams.pipe(this.compUntilDestroyed()).subscribe((params: any) => {
        this.isDifferentTab = false;
        if (this.queryParams && this.queryParams.tab != params.tab) {
          this.isDifferentTab = true;
        }
        this.queryParams = JSON.parse(JSON.stringify(params));
        setTimeout(() => {
          this.switchQueryParams(this.queryParams);
        });
        this.fabricName = this.commonService.getFabricNameByUrl(this.router.url);
        this.commonService.menuFavIconsHide.next();
        //context menu options for securitygroups
        let mode = this.commonService.getQueryParamValue('enabledmode');
        if (mode == 'securitygroups') {
          this.contextMenuOptions = [this.LIST_CONSTANT_CONTEXTMENU]; //"Enable Lists";
          this.showSGContextMenu = params.leftnav;
        }
        else if (!mode || mode == 'roles') {
          this.showSGContextMenu = true;
          this.contextMenuOptions = [this.DEFAULT_CONSTANT_CONTEXTMENU];
        }
      });
    }
    catch (ex) {
      console.error(ex);
    }
  }

  switchQueryParams(queryParams) {
    try {
      if (queryParams != null && Object.keys(queryParams).length > 0) {
        let leftnav = JSON.parse(queryParams.leftnav);
        this.commonService.leftHeaderConfig.filter((list) => {
          if (list.title == queryParams.tab && leftnav) {
            // this.queryParams.leftnav = 'true';
            list.routerLinkActive = true;
            this.commonService.treeIndexLabel = list.title;
          }
          else
            list.routerLinkActive = false;
        });
        this.commonService.isLeftTreeEnable = leftnav;
        this.commonService.layoutJSON.LeftTree = leftnav;
      }
    }
    catch (e) {
      console.error('Exception in switchQueryParams() of tree-activity.component  at TreeActivity time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  onClick(head: any) {
    try {
      // if (this.commonService.lastOpenedFabric == "Security" && this.commonService.isFormModified && this.commonService.treeIndexLabel != head.title) {
      //   this.openPopUpDialog(DISCARD_CHANGES, true, head);
      //   return;
      // }
      let tabChangeFlag = false;
      let params = JSON.parse(JSON.stringify(this.queryParams));
      if (head.title == this.queryParams.tab) {
        let status = !JSON.parse(this.queryParams.leftnav);
        this.queryParams['leftnav'] = status;
        params.leftnav = status;
        this.commonService.layoutJSON.LeftTree = status;
        // if(this.commonService.lastOpenedFabric != "Security"){
        //head.routerLinkActive = status;
        this.commonService.isLeftHeaderActive = status;
        /**i am commenting this initialization plz before uncommenting let me know */
        // this.commonService.treeIndexLabel = null;
        // }
      }
      else {
        tabChangeFlag = true;
        params.leftnav = 'true';
        this.subscription = this.route.queryParams.pipe(this.compUntilDestroyed()).subscribe((params: any) => {
          if (params.tab && this.isDifferentTab) {
            this.commonService.isLeftHeaderActive = true;
            this.commonService.isLeftTreeEnable = true;
            this.commonService.CreatenewList = false;
            this.commonService.treeIndexLabel = params.tab;
            this.queryParams['leftnav'] = true;
            this.commonService.layoutJSON.LeftTree = true;
            if (this.subscription) {
              this.subscription.unsubscribe();
            }
          }
        });
      }
      params.tab = head.title;
      if (this.commonService.getFabricNameByUrl(this.router.url) == FABRICS.ENTITYMANAGEMENT && tabChangeFlag) {
        let fabric = this.commonService.getFabricNameByTab(params.tab);
        let Previousfabric = this.commonService.getFabricNameByTab(this.queryParams.tab);
        params.leftnav = 'true';
        if (AppConsts.mutualFabric.includes(fabric) && AppConsts.mutualFabric.includes(Previousfabric) && params.EntityID) {
          if (this.checkEntityExistOrNot(params)) {
            params.EMTab = params.tab;
            this.router.navigate([FABRICS.ENTITYMANAGEMENT, 'Settings', params.EntityID, params.tab, params.EMTab], { queryParams: params });
          }
          else {
            this.commonService.IsEntityExistEMCapability = { isNotExist: true, capabilityName: params.EMTab };
            delete params.EntityID;
            delete params.EMTab;
            delete params.physicalItem;

            this.router.navigate([FABRICS.ENTITYMANAGEMENT], { queryParams: params });
          }
        }
        else {
          delete params.EntityID;
          delete params.EMTab;
          delete params.physicalItem;

          this.router.navigate([FABRICS.ENTITYMANAGEMENT], { queryParams: params });
        }
      }
      else if (this.commonService.getFabricNameByUrl(this.router.url) == FabricsNames.SECURITY) {
        if (this.route.snapshot.queryParamMap.get('tab') == params.tab) {
          this.router.navigate([], { relativeTo: this.route, queryParams: params });
        }
        else {
          let currenttab = this.route.snapshot.queryParamMap.get('tab');
          let activeId = this.route.snapshot.queryParamMap.get('leftnaventityid');
          if (currenttab && activeId) {
            delete params.leftnaventityid;
            this.commonService.sendDataToAngularTree1(currenttab, TreeOperations.deactivateSelectedNode, activeId)
          }
          this.router.navigate([FABRICS.SECURITY], { queryParams: params });
        }
      }
      else if (this.commonService.getFabricNameByUrl(this.router.url) == FabricsNames.CONSTRUCTION) {
        if (params.leftnav && params.formId) {
          let entityDetails = this.commonService.getEntitiesById(params.formId);
          params.entityType = EntityTypes.Project;
          params.entityId = params.formId;
          params.schema = EntityTypes.Project;
          params.entityName = entityDetails ? entityDetails.EntityName : '';
          this.router.navigate([FabricRouter.CONSTRUCTION_FABRIC, FORMS, ROUTE, params.tab, '0', params.entityType, FormOperationTypes.Update, params.entityId], { queryParams: params });
        }
        else {
          this.router.navigate([], { relativeTo: this.route, queryParams: params });
        }
      }
      else {
        this.router.navigate([], { relativeTo: this.route, queryParams: params });
      }

      // this.commonService.updateLayout.next();
      // this.commonService.isLeftHeaderClick = true;
    }
    catch (e) {
      console.error('Exception in onClick() of side-header.component  at side-Header time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  checkEntityExistOrNot(params) {
    let entityData: any = this.commonService.getEntitiesById(params.EntityID);
    let capabilityName = this.commonService.getCapabilityNamesBy(entityData.Capabilities);
    let fabricName = this.commonService.getFabricNameByTab(params.tab);
    return capabilityName.includes(fabricName);
  }

  openPopUpDialog(messageToshow, isSave, head?): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: isSave,
        content: [messageToshow],
        subContent: [],
        operation: PopupOperation.AlertConfirm
      };

      let matConfig = new MatDialogConfig();
      matConfig.data = config;
      matConfig.width = '600px';
      matConfig.disableClose = true;

      this.dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
      this.dialogRef.componentInstance.emitResponse.subscribe((data: any) => {
        this.commonService.isFormModified = false;
        this.onClick(head);
      });
    }
    catch (ex) {
      console.error('Exception in openPopUpDialog() of SecurityRolesComponent at time ' + new Date().toString() + '. Exception is : ' + ex);
    }
  }

  contextMenuPosition = { x: '0px', y: '0px', data: null };
  onContextMenu(event: any, listNode) {
    this.contextMenuOptions = [this.DEFAULT_CONSTANT_CONTEXTMENU];
    if (this.commonService.leftHeaderConfig && this.commonService.leftHeaderConfig.length != 1) {
      let isFavList = false;
      this.commonService.faviconList.forEach((items) => { if (items.id == listNode.id) isFavList = true; });
      event.preventDefault();
      this.contextMenuPosition.x = event.clientX + 'px';
      this.contextMenuPosition.y = event.clientY + 'px';
      this.contextMenuPosition.data = listNode;

      if (this.fabricName == FabricsNames.SECURITY) {
        let mode = this.commonService.getQueryParamValue('enabledmode');
        // if (mode == 'securitygroups' && this.commonService.treeIndexLabel == listNode.title && this.commonService.treeIndexLabel == TreeTypeNames.Construction) {
        //       this.contextMenuOptions = this.ConstructionContextMenus;
        //       this.contextMenu.openMenu();
        // }
        if (mode == 'securitygroups') {
          this.contextMenu.openMenu();
        }
      }
      else if (this.fabricName == FABRICS.ENTITYMANAGEMENT && listNode.id == FABRICS.CONSTRUCTION) {
        if (listNode.title == this.queryParams.tab) {
          this.contextMenuOptions = this.ConstructionContextMenus;
        }
        this.contextMenu.openMenu();
      }
      else {
        this.contextMenu.openMenu();
      }
    }
  }

  onMenuSelection(oprtionValue) {
    try {
      if (oprtionValue == this.LIST_CONSTANT_CONTEXTMENU || oprtionValue == this.ENTITIES_CONSTANT_CONTEXTMENU) {
        this.enableDisableListTab();
      }
      else if (this.fabricName == FABRICS.ENTITYMANAGEMENT && this.ConstructionContextMenus.includes(oprtionValue)) {
        const params = JSON.parse(JSON.stringify(this.queryParams));
        delete params.EntityID;
        delete params.EMTab;
        params['physicalItem'] = oprtionValue;
        this.router.navigate([FABRICS.ENTITYMANAGEMENT], { queryParams: params });
      }
      else {
        let currentList = this.contextMenuPosition.data;
        this.fabricName = this.commonService.getFabricNameByUrl(this.router.url);
        this.appConfig.AllHeaderRouting.forEach((list) => {
          if (list.Fabric == this.fabricName) {
            list.queryParameter['tab'] = currentList.title;
            list.queryParameter['leftnav'] = 'true';
          }
        });

        this.commonService.faviconList.forEach((list) => produce(list, draft => {
          if (draft.Fabric == this.fabricName) {
            if (draft.queryParameter) {
              draft.queryParameter['tab'] = currentList.title;
              draft.queryParameter['leftnav'] = 'true';
            }
            else {
              draft.queryParameter = {};
              draft.queryParameter['tab'] = currentList.title;
              draft.queryParameter['leftnav'] = 'true';
            }
          }
        }));

        //set as default to state
        let fabric = this.commonService.getSetAsDefaultFabricNameBy(this.fabricName);
        if (fabric != "") {
          this.commonService.setAsDefaultToState(fabric, currentList.title);
        }
      }
    }
    catch (e) {
      console.error("Exception at setDefaultTab,error:" + e);
    }
  }

  enableDisableListTab() {
    let currentList = this.contextMenuPosition.data;
    this.commonService.permissionListTab$.next({ queryParams: this.queryParams, tabData: currentList, enableList: this.contextMenuOptions.includes(this.LIST_CONSTANT_CONTEXTMENU) });
    this.contextMenuOptions[0] = this.contextMenuOptions[0] == this.LIST_CONSTANT_CONTEXTMENU ? this.ENTITIES_CONSTANT_CONTEXTMENU : this.LIST_CONSTANT_CONTEXTMENU;
  }

  applayCSSStyles(head) {
    let fabricName = this.commonService.getFabricNameByUrl(this.router.url);
    if (fabricName == FabricsNames.SECURITY) {
      return { 'visibility': head.show ? 'visible' : 'hidden', 'position': head.show ? 'relative' : 'absolute' };
    }
  }

  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables);
  }

  ngOnDestroy() {
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
