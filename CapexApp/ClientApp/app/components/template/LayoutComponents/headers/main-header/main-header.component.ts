import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { Title } from '@angular/platform-browser';
import { NavigationEnd, Router } from '@angular/router';
import { ConstructionService } from 'ClientApp/app/components/fabrics/construction/services/construction.service';
import { AngularTreeMessageModel, Datatypes, FABRICS, FabricsNames, FabricsPath, TreeOperations } from 'ClientApp/app/components/globals/Model/CommonModel';
import { FabricRouter } from 'ClientApp/app/components/globals/Model/FabricRouter';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { ATTENTION, CANT_ACCESS_CAPABILITY, DESKTOP_LOGOUT, DESKTOP_LOGOUT_CONTENTTEXT, DESKTOP_LOGOUT_HEADERTEXT, DESKTOP_SYNC_STATUS, DO_NOT_DISCONNECT, ENTITY_NOT_EXIST, INITIAL_OFFLINE_DATA_LOADING, LOGOUT, NETWORK_CONNECTION_ESTABLISHED, OFFLINE_DATA_LOADING, OFFLINE_DATA_REMOVED, OFFLINE_WARNING, ONLINE_DATA_LOADING, SYSTEM_OFFLINE, SYSTEM_ONLINE, WEB_LOGOUT } from '../../../../common/Constants/AttentionPopupMessage';
import { PopUpAlertComponent } from '../../../../common/PopUpAlert/popup-alert.component';
import { FormSettingComponent } from '../../../../common/UserActivities/userSettings/user-setting.component';
import { CommonService } from '../../../../globals/CommonService';
import { GlobalIcons } from '../../../../globals/icons';
import { PopupOperation } from '../../../../globals/Model/AlertConfig';
import { EntityTypes } from '../../../../globals/Model/EntityTypes';
import { MessageType, Routing } from '../../../../globals/Model/Message';
import { AppConfig } from '../../../../globals/services/app.config';
import { DialogAlertComponent } from '../../DialogAlert/dialog-alert.component';
@Component({
  selector: 'mainheader',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss']
})
export class MainHeaderComponent implements OnInit, OnDestroy {

  public CustomerAppsettings = AppConfig.AppMainSettings;
  isdesktop: boolean = true;
  fabricTitle;
  public themeWrapper = document.querySelector('body');
  public toggleBtnView: boolean = false;
  // isFdcAdmin:boolean=false;
  isSubHeaderEnable: boolean = false;
  isFabricAdmin = false;
  menuFavIcons: boolean = false;
  menuCapabilityIcon: string = 'V3 Menu';
  // searchSource = 'https://althingblob.blob.core.windows.net/althing-v3/form-header-icon/V3 LeftPaneSearch.svg';
  fabricName;
  SignalCellular = GlobalIcons["Online_Green"];
  private desktopOnline$ = Observable.fromEvent(window, 'online');
  private desktopOffline$ = Observable.fromEvent(window, 'offline');
  isNetworkConnected = false;
  ConnectionEstablishedText;
  spinnerImage = GlobalIcons["Desktop_Spinner"];
  conditionalToolTipForSyncing;
  desktopSyncStatusText;
  NetworkSignalStatus = SYSTEM_ONLINE;
  showSynctext: boolean = false;

  web_logout = WEB_LOGOUT;
  desktop_logout = DESKTOP_LOGOUT;
  desktop_logout_headertext = DESKTOP_LOGOUT_HEADERTEXT;
  desktop_logout_contenttext = DESKTOP_LOGOUT_CONTENTTEXT;
  onlineoffline: string;
  takeUntilDestroyObservables = new Subject();

  constructor(public constructionservice: ConstructionService, public commonService: CommonService, public appConfig: AppConfig, public dialog: MatDialog, public router: Router, private titleService: Title) {
    if (this.CustomerAppsettings.env.UseAsDesktop) {
      //to make open in new tab disable
      this.isdesktop = false;
      this.conditionalToolTipForSyncing = INITIAL_OFFLINE_DATA_LOADING;
      this.desktopSyncStatusText = DESKTOP_SYNC_STATUS;
    }
    //   this.commonService.getOPFRightTree.pipe(this.compUntilDestroyed()).subscribe(res => {
    //   this.getOperationalFormComplianceAdmin();
    // });

    this.commonService.menuFavIconsHide.pipe(this.compUntilDestroyed()).subscribe(res => {
      this.menuFavIcons = false;
    })

    this.commonService.changeTheme.pipe(this.compUntilDestroyed()).subscribe((data: any) => {
      this.themeChange(data);
    });

    this.commonService.onSessionExpirePopupAlert$.pipe(this.compUntilDestroyed()).subscribe((res: any) => {
      // if(this.CustomerAppsettings.env.UseAsDesktop == true && this.commonService.onlineOffline){
      //   this.commonService.desktopSessionOutStatus = true; 
      //   this.commonService.logout();      
      // } else{
      //   this.OpenSessionExpireDialog(res, PopupOperation.AlertConfirm);
      // }
      this.OpenSessionExpireDialog(res, PopupOperation.AlertConfirm);
    })
    this.desktopOnline$.pipe(this.compUntilDestroyed()).subscribe(e => {
      this.conditionalToolTipForSyncing = OFFLINE_DATA_LOADING;
      this.NetworkSignalStatus = SYSTEM_ONLINE;
      this.commonService.isOnlineDataSyncDone = false;
      // repeat with the interval of 1 second
      let timerId = setInterval(() => this.setTimeOutMethod(), 1000);

      // after 7 seconds stop the setInterval
      setTimeout(() => { clearInterval(timerId); this.isNetworkConnectionInitiated(); }, 7000);
    });

    this.desktopOffline$.pipe(this.compUntilDestroyed()).subscribe(e => {
      this.conditionalToolTipForSyncing = ONLINE_DATA_LOADING;
      this.NetworkSignalStatus = SYSTEM_OFFLINE;
      this.isNetworkConnected = false;
      this.SignalCellular = GlobalIcons["Offline_Red"];
    });


    this.commonService.userProfileMenuRefreshOnBrowserBackFowardButton.pipe(this.compUntilDestroyed()).subscribe((response: any) => {
      if (response) {
        this.profileSettingMenu(response);
      }
    })
    this.commonService.userSettingForm.pipe(this.compUntilDestroyed()).subscribe((res: any) => {
      if (res) {
        let payload = JSON.parse(res['Payload'])
        let usersettingData = JSON.parse(payload["Data"]);
        if (usersettingData.UserImage) {
          this.commonService.userProfilePicture = usersettingData.UserImage;
        }
      }
    })
  }

  items = [
    { id: 1, name: 'Item 1' },
    { id: 2, name: 'Item 2' },
    { id: 3, name: 'Item 3' }
  ];

  @ViewChild(MatMenuTrigger, { static: false })
  contextMenu: MatMenuTrigger;

  contextMenuPosition = { x: '0px', y: '0px' };

  onContextMenu(event: any, list) {
    if (!this.commonService.isContextMenuHeader) {
      event.preventDefault();
      this.contextMenuPosition.x = event.currentTarget.offsetLeft + 'px';

      if (this.CustomerAppsettings.env.UseAsDesktop) {
        this.contextMenuPosition.y = 72 + 'px';
      } else {
        this.contextMenuPosition.y = 42 + 'px';
      }

      this.contextMenu.menuData = list;
      this.contextMenu.openMenu();
    }
  }

  desktopSyncMessagesUpdate(data: any) {
    switch (data.OperationType) {

      case "DeleteAndResyncData":
        this.desktopSyncStatusText = OFFLINE_DATA_REMOVED;
        this.commonService.isOnlineDataSyncDone = true;
        break;
      case "DataLoadCompleted":
        this.desktopSyncStatusText = DESKTOP_SYNC_STATUS;
      default:
        break;
    }
    setTimeout(() => { this.commonService.showSyncStatusText = false; }, 3000);

  }
  setTimeOutMethod() {
    this.SignalCellular = GlobalIcons["Flashing_Grey"];
    setTimeout(() => {
      this.SignalCellular = GlobalIcons["Online_Green"];
    }, 1500);

  }
  isNetworkConnectionInitiated() {
    console.log("NetWork Flash Stopped");
    this.ConnectionEstablishedText = NETWORK_CONNECTION_ESTABLISHED;
    this.isNetworkConnected = true;
    setTimeout(() => {
      this.isNetworkConnected = false;
    }, 2000);
  }
  onContextMenuList(event: any, list) {
    if (!this.commonService.isContextMenuHeader) {
      event.preventDefault();
      this.contextMenuPosition.x = 192 + 'px';
      this.contextMenuPosition.y = (Math.trunc(event.y / 42) * 42) + 'px';
      this.contextMenu.menuData = list;
      this.contextMenu.openMenu();
    }
  }

  onContextMenuUnpin() {
    this.unSetFavHeaderIcon(this.contextMenu.menuData);
  }

  onContextMenuOpenNew() {
    let list = this.contextMenu.menuData;
    if (list['routerLink'] && list["Fabric"] == FabricsNames.HOME) {
      let link = list['routerLink'].substr(1, list['routerLink'].length);
      window.open(this.commonService.baseUrl + link, '_blank');
    }
    else if (list['routerLink'] && list.queryParameter && list["Fabric"].toLowerCase() == FABRICS.SECURITY.toLowerCase()) {
      let link = list['routerLink'].substr(1, list['routerLink'].length) + '?tab=' + list.queryParameter.tab + '&leftnav=' + list.queryParameter.leftnav + '&enabledmode=' + list.queryParameter.enabledmode
      let ldata = this.commonService.ResetheaderData(list);
      if (ldata && ldata.queryParameter && !ldata.queryParameter.tab) {
        link = list['routerLink'].substr(1, list['routerLink'].length) + '?leftnav=false' + '&enabledmode=' + list.queryParameter.enabledmode;
      }
      window.open(this.commonService.baseUrl + link, '_blank');
    }
    else if (list['routerLink'] && list.queryParameter) {
      let link = list['routerLink'].substr(1, list['routerLink'].length) + '?tab=' + list.queryParameter.tab + '&leftnav=' + list.queryParameter.leftnav
      window.open(this.commonService.baseUrl + link, '_blank');
    } else {
      if (list['routerLink']) {
        const link = list['routerLink'].substr(1, list['routerLink'].length);
        window.open(this.commonService.baseUrl + link, '_blank');
      }
    }
  }

  defaultmenuUserList = [
    { 'id': 'settings', 'label': 'Settings', 'show': true },
    { 'id': 'constructionadmin', 'label': 'Construction Admin', 'show': false },
    { 'id': 'EMGlobalAdmin', 'label': 'Entity Mgmt Admin', 'show': false },
    { 'id': 'opcomplianceadmin', 'label': 'OP Compliance Admin', 'show': false },
  ];
  menuUserList = []
  ClickEvent(list) {
    try {
      this.commonService.isUserMenuHeader = false
      this.commonService.latlngFinderDivision = false;
      switch (list.id) {
        case 'settings':
          this.openDialog();
          break;

        case 'constructionadmin':
          this.constructionservice.readConstructionAdmin$.next(list);
          // this.router.navigate([FabricRouter.CONSTRUCTION_FABRIC, 'constructionadmin', { EntityType: "constructionadmin", "Expand": false }], { queryParams: { 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
          break;

        // case 'opcomplianceadmin':
        //   if (this.CustomerAppsettings.env.UseAsDesktop == true && this.commonService.onlineOffline == false) {
        //     this.commonAppService.openofflineDialog(new Array(OFFLINE_WARNING));
        //   }
        //   else {
        //     this.getOperationalFormComplianceAdmin();
        //   }
        //   break;
        case 'EMGlobalAdmin':
          // if (!this.router.url.includes('/EMGlobalAdmin')) {
          //   if (this.CustomerAppsettings.env.UseAsDesktop == true && this.commonService.onlineOffline == false) {
          //     this.commonAppService.openofflineDialog(new Array(OFFLINE_WARNING));
          //   } else {
          //     // this.sendDataToAngularTree(this.commonService.treeIndexLabel, TreeOperations.deactivateSelectedNode, '');
          //     this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC, 'EMGlobalAdmin', { EntityType: "EMGlobalAdmin", "Expand": false }], { queryParams: { 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
          //   }
          //   break;
          // }
          break;
      }
    }
    catch (e) {
      console.error('Exception in ClickEvent() of main-header.component  at main-header time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }


  // openofflineDialog(messageToshow): void {
  //   try {
  //     let config = {
  //       header: 'Attention!',
  //       isSubmit: false,
  //       content: [messageToshow],
  //       subContent: [],
  //       operation: PopupOperation.Attention
  //     };
  //     let matConfig = new MatDialogConfig()
  //     matConfig.data = config;
  //     matConfig.width = '500px';
  //     matConfig.disableClose = true;
  //     this.dialog.open(PopUpAlertComponent, matConfig);
  //   }
  //   catch (ex) {
  //     console.log(ex);
  //   }
  // }



  OpenSessionExpireDialog(messageToshow: any, Operation?: any): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: Operation != undefined ? true : false,
        content: [messageToshow],
        subContent: [],
        operation: Operation != undefined ? Operation : PopupOperation.Attention
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.id = messageToshow;
      matConfig.disableClose = true;

      this.commonService.sessionSucess = true;
      this.dialog.open(PopUpAlertComponent, matConfig).componentInstance.emitResponse.subscribe((res: any) => {
        this.commonService.logout();
      })
    }
    catch (e) {
      console.error('Exception in openDialog() of main-header.component at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }


  // getOperationalFormComplianceAdmin() {
  //   try {
  //     this.commonService.isUserMenuHeader = false;
  //     var scheduleForm = this.router.url.includes('schedule');
  //     if (this.commonService.sideHeaderView != 'FabricAdmin') {
  //       this.commonService.sideHeaderView = 'FabricAdmin';
  //       this.commonService.OpFormRightPannel = true;
  //       this.commonService.updateRightSideHeader$.next('OperationalFormComplianceAdmin');
  //       if(scheduleForm){
  //         this.operationalService.scheduleEntityObservable$.next('rightTreeCollapse');
  //       }
  //     }
  //     else {
  //       if(scheduleForm){
  //         this.operationalService.scheduleEntityObservable$.next('rightTreeCollapse');
  //       }else{
  //       this.commonService.sideHeaderView = 'Default';
  //       this.commonService.OpFormRightPannel = false;
  //       this.commonService.updateRightSideHeader$.next(FabricRouter.OPERATIONALFORM);
  //       this.operationalService.scheduleDeleteObservable$.next(FabricRouter.OPERATIONALFORM);
  //       }
  //     }
  //   }
  //   catch (e) {
  //     console.error('Exception in getOperationalFormComplianceAdmin() of MainHeaderComponent at time ' + new Date().toString() + '. Exception is : ' + e);
  //   }
  // }


  ngOnInit(): void {
    try {
      if (this.appConfig.AllHeaderRoutingList)
        this.appConfig.AllHeaderRoutingList.sort((a, b) => { return a.order - b.order });
      this.appConfig.AllHeaderRoutingList.reverse();

      this.router.events.pipe(filter(event => event instanceof NavigationEnd)).pipe(this.compUntilDestroyed()).subscribe((event: NavigationEnd) => {
        this.onRoutechange(event);
      });
      this.fabricName = this.commonService.getFabricNameByUrl(this.router.url);

      if (this.appConfig.AllHeaderRoutingList && this.fabricName) {
        this.appConfig.AllHeaderRoutingList.forEach((list) => {
          if (list.Fabric && this.fabricName == list.Fabric) {
            this.commonService.currentRoutingSource = list.sourceSub;
          }
        });
      }

      if (this.CustomerAppsettings.env.UseAsDesktop && this.commonService.onlineOffline) {
        this.commonService.isOnlineDataSyncDone = false;
        this.openAlertDialog(DO_NOT_DISCONNECT);
      }
      if (this.CustomerAppsettings.env.UseAsDesktop && !this.commonService.onlineOffline) {
        this.commonService.isOnlineDataSyncDone = true;
        this.SignalCellular = GlobalIcons["Offline_Red"];
      }
      this.onRoutechange(this.router);
      if (this.commonService.getFromFDCStateByKey("TabletModebuttonStatus")) {
        this.commonService.onTabletMode();
        this.commonService.tabletKeyPadMode = true;
        // this.commonService.AppendToBodyObservable$.next({ "Mode": "TabletMode", "EventFrom": "TabletMode", "Value": this.commonService.tabletMode });
      }
    }
    catch (e) {
      console.error('Exception in ngOnInit() of main-header.component  at main-header time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  openAlertDialog(message): void {
    try {
      const config = {
        header: ATTENTION + '!',
        isSubmit: false,
        content: [message],
        subContent: [],
        operation: PopupOperation.Attention
      };
      const matConfig: MatDialogConfig<any> = new MatDialogConfig();
      matConfig.data = config;
      matConfig.width = 'max-content';
      matConfig.disableClose = true;

      this.dialog.open(PopUpAlertComponent, matConfig);
    } catch (error) {
      console.error(error);
    }
  }

  onRoutechange(event) {
    try {
      this.fabricName = this.commonService.getFabricNameByUrl(event.url);
      // this.commonService.productManagementContextClick = false;

      // this.appConfig.AllHeaderRoutingList=JSON.parse(JSON.stringify(this.appConfig.AllHeaderRoutingList))
      this.appConfig.AllHeaderRoutingList.forEach((list) => {
        if (list.Fabric && this.fabricName == list.Fabric) {
          this.commonService.currentRoutingSource = list.sourceSub;
          this.fabricTitle = list.tooltip.toUpperCase();
          this.titleService.setTitle(list.tooltip);
          list.routerLinkActive = true;
          this.commonService.isUpdateUserpreferencePinnedTab = false;
          this.commonService.changeFavIcon(list.Fabric);
          if (this.commonService.navigateMutualFabric && this.commonService.entityExist) {
            // this.commonAppService.openofflineDialog(ENTITY_NOT_EXIST(list.tooltip));
            this.commonService.loadingBarAndSnackbarStatus("", "");
            this.commonService.entityExist = false;
          }
        } else {
          list.routerLinkActive = false;
        }
      })

      for (var data of this.appConfig.AllHeaderRoutingListService) {
        if (data.Fabric == this.fabricName) {
          this.commonService.faviconList.forEach((head: any) => {
            if (data.Fabric == this.fabricName && head.Fabric == data.Fabric) {
              data.routerLinkActive = true;
              data = this.commonService.ResetheaderData(data);
            }
            else {
              //data.routerLinkActive = false;
            }
          });
          this.isFabricAdmin = data.admin;
          break;
        }
        else {
          this.isFabricAdmin = false;
        }
      }
    }
    catch (e) {
      console.error('Exception in onRoutechange() of main-header.component  at main-header time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }

  // onClick(data) {
  //   try {
  //     if (data.id && data.id == 'menu') {
  //       this.commonService.isSubHeaderEnable = !this.commonService.isSubHeaderEnable;
  //     }
  //     if (data.id && data.id == 'profile') {
  //       this.commonService.isUserMenuHeader = !this.commonService.isUserMenuHeader;
  //     }
  //   }
  //   catch (e) {
  //     console.error('Exception in onClick() of main-header.component  at main-header time ' + new Date().toString() + '. Exception is : ' + e);
  //   }

  // }

  onRouteClick(data) {
    try {
      let fabric = this.commonService.getFabricNameByUrl(this.router.url)
      this.commonService.isUserMenuHeader = false;
      this.commonService.favIconsClicked = true;
      this.commonService.switchButton = data.id;
      if (this.commonService.onlineOffline || (this.commonService.onlineOffline == false && (data.Fabric.toUpperCase() == FABRICS.HOME))) {
        if (fabric == data.Fabric) {
          this.commonService.isContextMenuHeader = false;
          return;
        }

        // this.authorizationService.securityGroupsData = [];
        this.commonService.isContextMenuHeader = false;
        this.commonService.capabilityFabricId = data.entityId;
        // this.authorizationService.securityAdminEnabledFabrics = [];
        data = this.commonService.ResetheaderData(data);
        this.commonService.routeToFabrics(data)
      } else {
        //var FabricName = this.getFabricName(data.Fabric);
        // if (data && data.tooltip)
        //   this.commonAppService.openofflineDialog(new Array(CANT_ACCESS_CAPABILITY(data.tooltip)));
      }
    }
    catch (e) {
      console.error('Exception in onRouteClick() of main-header.component  at main-header time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }

  unSetFavHeaderIcon(data) {
    try {
      let lists = [];
      this.commonService.isContextMenuHeader = false;
      this.commonService.faviconList.forEach((list: any) => {
        if (list.Fabric != data.Fabric)
          lists.push(list);
      });
      this.commonService.faviconList = lists;
      let faviconList = this.commonService.faviconList;
      this.commonService.updateIndexDBUserPrefrence(null, null, faviconList);
      this.appConfig.AllHeaderRoutingList.forEach((list) => {
        if (data.Fabric == list.Fabric) {
          list.isFavIcon = false;
        }
      });
    }
    catch (e) {
      console.error('Exception in unSetFavHeaderIcon() of main-header.component  at main-header time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }


  sort(cols) {
    try {
      (<Array<any>>cols).sort((leftSide, rightSide) => {
        if (leftSide.order < rightSide.order)
          return -1;
        if (leftSide.col > rightSide.col)
          return 1;

        return 0;
      });
    }
    catch (e) {
      console.error('Exception in sort() of main-header.component  at main-header time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }

  onRightClick(event, fabricTitle?: any) {
    try {
      if (fabricTitle && fabricTitle === 'HOME') {
        this.commonService.isContextMenuHeader = false;
      }
    }
    catch (e) {
      console.error('Exception in onRightClick() of main-header.component  at main-header time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }
  closeContextMenu(id) {
    if (id == 'homeIcon') {

      // this.authorizationService.securityGroupsData = [];
      this.commonService.isContextMenuHeader = false;
      this.commonService.capabilityFabricId = "home";
      // this.authorizationService.securityAdminEnabledFabrics = [];
    }
    else if (id == 'fabricIcon') {
      this.commonService.isContextMenuHeader = false;
    }
  }

  subHeaderViewToggle() {
    try {
      this.commonService.isSubHeaderEnable = !this.commonService.isSubHeaderEnable;
      this.commonService.isUserMenuHeader = false;
      this.commonService.isContextMenuHeader = false;
      if (this.commonService.isSubHeaderEnable == false) {
        this.commonService.isSubHeaderEnable = true;
        this.commonService.subHeaderDisable = true;
      }
      if (this.commonService.tabletMode) {
        setTimeout(() => {
          this.commonService.subheadertoggleMode$.next();
        });
      }
      if (this.commonService.rightSideCapabilityList.length != 0) {
        this.commonService.rightSideCapabilityList.forEach(data => {
          if (data["routerLinkActive"])
            data["routerLinkActive"] = false;
          this.commonService.isRightTreeEnable = false;
          this.commonService.layoutJSON.RightTree = false;
        })
      }
    }
    catch (e) {
      console.error('Exception in subHeaderViewToggle() of main-header.component  at main-header time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }
  profileSettingMenu(response?) {
    try {
      this.menuUserList = JSON.parse(JSON.stringify(this.defaultmenuUserList))
      if (response && response == "BrowserBackForwardButton")
        this.commonService.isUserMenuHeader = true;
      else
        this.commonService.isUserMenuHeader = !this.commonService.isUserMenuHeader;

      this.commonService.isSubHeaderEnable = false;
      this.commonService.isContextMenuHeader = false;
      this.commonService.subHeaderDisable = false;
      for (var data of this.appConfig.AllHeaderRoutingListService) {
        if (data.Fabric == this.fabricName) {
          this.isFabricAdmin = data.admin;
          break;
        }
        else {
          this.isFabricAdmin = false;
        }
      }
      if (this.commonService.isUserMenuHeader) {
        for (var list of this.menuUserList) {
          switch (this.fabricName) {
            case FabricRouter.ENTITYMANAGEMENT_FABRIC:
              if (list["id"] == "EMGlobalAdmin")
                if (this.isFabricAdmin && this.commonService.getAdminFabriclist().length)
                  list["show"] = true;
                else
                  list["show"] = false;
              break;
            case FabricRouter.CONSTRUCTION_FABRIC:
              if (list["id"] == "constructionadmin")
                if (this.isFabricAdmin && this.commonService.getAdminFabriclist().length)
                  list["show"] = true;
                else
                  list["show"] = false;
              break;
            default:
              break;
          }
        }
      }
    }
    catch (e) {
      console.error('Exception in profileSettingMenu() of main-header.component  at main-header time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }


  openDialog(): void {
    try {
      this.commonService.loadingBarAndSnackbarStatus("", "Reading Data...");
      let config = new MatDialogConfig()
      config.disableClose = true;
      config.width = '1020px';
      config.height = '550px';
      this.GetUserSettingsForm();
      this.dialog.open(FormSettingComponent, config);
    }
    catch (e) {
      console.error('Exception in openDialog() of main-header.component  at main-header time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }


  ngAfterViewInit() {
    setTimeout(() => {
      if (this.commonService.userProfilePicture == "")
        this.GetUserSettingsForm();
    }, 1000);
  }

  GetUserSettingsForm() {
    try {
      // this.commonService.uservar = true;
      var payload = { "EntityType": EntityTypes.PERSON, "Fabric": "People", "UserSettings": "yes", DataType: EntityTypes.EntityInfo };
      var message = this.commonService.getMessageDefaultProperties();
      message.CapabilityId = this.commonService.getCapabilityId(FABRICS.PEOPLEANDCOMPANIES)
      message.Payload = JSON.stringify(payload);
      message.DataType = FABRICS.PEOPLEANDCOMPANIES + "_UserSetting";
      message.EntityID = this.commonService.currentUserId;
      message.EntityType = EntityTypes.PERSON;
      message.MessageKind = MessageType.READ;
      message.Routing = Routing.OriginSession;
      message.Type = "Details";
      message.Fabric = FABRICS.PEOPLEANDCOMPANIES;
      this.commonService.sendMessageToBackend(message);

      this.commonService.isUserMenuHeader = false;
    } catch (e) {
      console.error('Exception in GetUserSettingsForm() of UseractionComponent  at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }
  GetFDCAdminForm() {
    try {

    } catch (e) {
      console.error('Exception in GetFDCAdminForm() of UseractionComponent  at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  // sendDataToAngularTree(treeName: string, treeOperationType: string, payload: any) {
  //   var treeMessage: AngularTreeMessageModel = {
  //     "fabric": FABRICS.TASKFABRIC.toLocaleUpperCase(),
  //     "treeName": [treeName],
  //     "treeOperationType": treeOperationType,
  //     "treePayload": payload.children
  //   }
  //   this.commonService.sendDataToAngularTree.next(treeMessage);
  // }

  logoutUser() {
    let headerText: string = WEB_LOGOUT;
    let contentText: string = LOGOUT;
    let configWidth: string = 'max-content';

    try {
      switch (this.CustomerAppsettings.env.UseAsDesktop) {
        case true:
          headerText = DESKTOP_LOGOUT_HEADERTEXT;
          contentText = DESKTOP_LOGOUT_CONTENTTEXT;
          configWidth = '600px';
          break;
      }

      this.commonService.isUserMenuHeader = false;
      let config = new MatDialogConfig()
      config.data = { 'header': headerText, 'content': new Array(contentText) };
      config.width = configWidth;
      config.disableClose = true;
      this.dialog.open(DialogAlertComponent, config);
    } catch (e) {
      console.error('Exception in logoutUser() of UseractionComponent  at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  themeChange(data) {
    try {
      this.commonService.themeDark = !this.commonService.themeDark;
      console.log("themeChange main-header.component....." + this.commonService.themeDark);
      if (this.commonService.themeDark) {
        for (var key in this.commonService.lightTheme) {
          if (this.commonService.lightTheme.hasOwnProperty(key)) {
            var val = this.commonService.lightTheme[key];
            this.themeWrapper.style.setProperty(key, val);
          }
        }
      }
      else {
        for (var key in this.commonService.darkTheme) {
          if (this.commonService.darkTheme.hasOwnProperty(key)) {
            var val = this.commonService.darkTheme[key];
            this.themeWrapper.style.setProperty(key, val);
          }
        }
      }
    }
    catch (e) {
      console.error('Exception in themeChange() of main-header.component  at main-header time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }


  tabletModeChange(ev) {
    this.commonService.tabletMode = !this.commonService.tabletMode;
    this.commonService.tabletKeyPadMode = this.commonService.tabletMode;
    this.commonService.tableButtonMode = this.commonService.tabletMode;
    // this.commonService.tabletModeObservable$.next({ "TypeOfData": "UserSettingsData", "Mode": "TabletMode", "EventFrom": "TabletMode", "Value": this.commonService.tabletMode });
    this.commonService.zoomApplicationForTabletMode$.next({ "ZoomType": "Default", "Value": this.commonService.tabletMode, "Bydefault": true });
    //this.commonService.AppendToBodyObservable$.next({ "Mode": "TabletMode", "EventFrom": "TabletMode", "Value": this.commonService.tabletMode });
    this.commonService.setToFDCStateByKey("TabletModebuttonStatus", this.commonService.tabletMode);

    // if (this.commonService.tabletMode) {
    //   this.commonService.isMinimizeKeypad = true;
    // }
  }

  // TreeTypeNamesForAll = ["All", "List", "Locations"]
  contextMenuClick(data, id, callFrom?) { }

  // exitSecurityAdmin() {

  // }


  // ClickDesktopApp() {

  // }
  // openofflineDialogopformsconfig(messageToshow): void {
  //   try {
  //     let config = {
  //       header: 'Attention!',
  //       isSubmit: false,
  //       content: [messageToshow],
  //       subContent: [],
  //       operation: PopupOperation.Attention
  //     };
  //     let matConfig = new MatDialogConfig()
  //     matConfig.data = config;
  //     matConfig.width = '500px';
  //     matConfig.disableClose = true;
  //     this.dialog.open(PopUpAlertComponent, matConfig);
  //   }
  //   catch (ex) {
  //     console.error(ex);
  //   }
  // }
  // ClickSupport() {
  //   console.log('Support : ')
  // }
  // getFabricName(fabric){
  //   try {
  //     switch (fabric) {
  //       case "taskfabric":
  //         return "Tasks";
  //       case "drawingsfabric":
  //         return "Ideas";
  //       case "People":
  //         return "People & Companies";
  //       case "geo-analytics Geo Analytics":
  //         return "geo-analytics";
  //       case "BI":
  //         return "Business Intelligence";
  //       case "ActivityCenter":
  //         return "Activity Center";
  //       case "FieldDataCapture":
  //         return "Field Data Capture";
  //       case "OperationalForm":
  //         return "Operational Forms";
  //       case "Workflow":
  //         return "workflowfabric";
  //       case "Home":
  //         return "Home";
  //       case "AssetManagement":
  //         return "Asset Management";
  //       case "Security":
  //         return "Security";
  //       case "BigData":
  //         return "Big Data";
  //       default:
  //         return fabric;

  //     }
  //   }
  //   catch (e) {
  //     console.log(new Error('Exception in GetFabricname() of commonservice at time ' + new Date().toString() + '. Exception is : ' + e));
  //   }
  // }

  onLineOfflineStatus() {
    if (this.commonService.onlineOffline) {
      this.onlineoffline = 'ONLINE';
      return "online_text_color";
    } else {
      this.onlineoffline = 'OFFLINE';
      return "offline_text_color";
    }
  }



  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables);
  }

  ngOnDestroy() {
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }
}