﻿/// <reference path="../../../../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { FdcHeaderPopupTsComponent } from './fdc-header-popup-ts.component';

let component: FdcHeaderPopupTsComponent;
let fixture: ComponentFixture<FdcHeaderPopupTsComponent>;

describe('FdcHeaderPopup.ts component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ FdcHeaderPopupTsComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(FdcHeaderPopupTsComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});