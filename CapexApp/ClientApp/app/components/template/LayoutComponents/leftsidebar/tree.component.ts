import { Component, ElementRef, OnDestroy, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatExpansionPanel } from '@angular/material/expansion';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
// import { CommonInputPopupComponent } from 'ClientApp/app/components/common/commonInputPopup/common-InputPopup.component';
import { FOUR_CHARACTERS_REQUIRED } from 'ClientApp/app/components/common/Constants/AttentionPopupMessage';
import { ITreeConfig } from 'ClientApp/app/components/globals/Model/EntityTypes';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { alertPopUp } from '../../../common/AttentionPopUp/popup';
import { AppConsts } from '../../../common/Constants/AppConsts';
import { PopUpAlertComponent } from '../../../common/PopUpAlert/popup-alert.component';
import { PopupOperation } from '../../../globals/Model/AlertConfig';
import { AngularTreeEventMessageModel, AngularTreeMessageModel, FABRICS, FabricsNames, FabricsPath, TreeOperations, TreeTypeNames } from '../../../globals/Model/CommonModel';
import { CommonService } from './../../../globals/CommonService';
import { CommonEntityFilter } from './CommonEntityFilter/common-entity-filter.component';
export const debounced = (cb, time) => {
  const db = new Subject();
  const sub = db.pipe(debounceTime(time)).subscribe(cb);
  const func = v => db.next(v);
  func.unsubscribe = () => sub.unsubscribe();
  return func;
};
@Component({
  selector: 'left-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.styles.scss']
})
export class TreeComponent implements OnDestroy {
  debouncedClick;
  // public CustomerAppsettings = AppConfig.AppMainSettings;
  // public onlineOffline: boolean = navigator.onLine;
  // entiyFilter;
  // activeTab;
  isSearchText: boolean = false;
  filterText;
  fabric;
  createPermission = false;
  @ViewChild('inputSearch', { static: false })
  filterFocus: ElementRef;
  @ViewChildren("treeareas") treeareas: QueryList<ElementRef>
  @ViewChildren(CommonEntityFilter) commonEntityFilter: QueryList<CommonEntityFilter>
  // queryParams;
  isExpand = false;
  tabList;
  activenode: boolean = false;
  isSingleClick: Boolean = true;
  ListQueryParmas;
  isListTab;
  diableFilter = false;
  // ListCreatDataModel = '[{"label":"Favorites","expand":false,"payload":[]},{"label":"Conversations","expand":false,"payload":[]}]';

  ListCreatData = [];
  // ShowAdd: boolean = false;
  takeUntilDestroyObservables = new Subject();
  constructor(public commonService: CommonService, public router: Router, private route: ActivatedRoute, public dialog: MatDialog) {
    this.debouncedClick = debounced($event => this.searchEntity($event), 400);
    this.commonService.opendialogboxObs.pipe(this.compUntilDestroyed()).subscribe((res) => {
      this.openDialog(res);
    })
    this.commonService.getAngularTreeEvent$
      .filter((msg:AngularTreeEventMessageModel) => msg.treeOperationType==TreeOperations.ClearSearch)
      .pipe(this.compUntilDestroyed())
      .subscribe((data: AngularTreeEventMessageModel) => {
        try {
          if(this.filterText){
            this.filterText = '';
            if (this.commonService.leftHeaderSearchObject && this.commonService.leftHeaderSearchObject[this.tabList])
            this.commonService.leftHeaderSearchObject[this.tabList]["text"] = '';
            this.isSearchText = false;
            if (this.commonService.leftHeaderSearchObject && this.commonService.leftHeaderSearchObject[this.commonService.treeIndexLabel])
              this.commonService.leftHeaderSearchObject[this.commonService.treeIndexLabel]["isActive"] = false;
            this.setFocus()
          }
        }
        catch (e) {
          console.error('Exception in constructor of sendDataToAngularTree  subscriber of capability.tree.component  at CapabilityTree. time ' + new Date().toString() + '. Exception is : ' + e);
        }

      });
  }
  refreshtree: boolean = false;
  currentRouteFabric = "";
  filter: any;
  treeInitilize: boolean = true;
  ngOnInit() {
    try {
      let ListCreatDataModel = '[]'
      this.router.events.filter((evt: any) => evt && evt instanceof NavigationEnd).pipe(this.compUntilDestroyed()).subscribe((event) => {
        try {
          if (event instanceof NavigationEnd) {
            let url = event.url;
            let physiclaflow = url.includes('PhysicalFlow');
            if ((url.includes('EntityID') || url.includes('EntityId')) && !physiclaflow) {
              // if(this.filterText)
              // this.clearFilter()
              this.activenode = true;
            }
            else {
              this.activenode = false;
            }
            let fabricName = this.commonService.getFabricNameByUrl(url);
            if (this.fabric == undefined) {
              this.fabric = fabricName;
            }
            if (this.fabric != fabricName) {
              this.storeLeftHeaderSearchObject();
              this.fabric = fabricName;
              if (this.fabric == FabricsNames.SECURITY && this.commonService.getRolesOrSecurityGroupsMode() == "roles") {
                this.isExpand = true;
              } else {
                this.isExpand = false;
              }
              this.treeInitilize = false;
              this.isSearchText = false;
              this.ListCreatData = [];
              setTimeout(() => {
                this.treeInitilize = true;
                this.ListCreatData = JSON.parse(ListCreatDataModel);
              }, 100);
            }
            if (!this.commonService.leftHeaderSearchObject)
              this.storeLeftHeaderSearchObject();
          }

          if (this.commonService.leftHeaderSearchObject && this.commonService.leftHeaderSearchObject[this.tabList]) {
            this.isSearchText = this.commonService.leftHeaderSearchObject[this.tabList]["isActive"];
            this.filterText = this.commonService.leftHeaderSearchObject[this.tabList]["text"];
          }


          this.commonService.userProfileMenuRefreshOnBrowserButtons();
        }
        catch (e) {
          console.error('Exception in ngOnInit() of router.events subscriber of tree-activity.component  at TreeActivity time ' + new Date().toString() + '. Exception is : ' + e);
        }

      });


      if (this.commonService.lastOpenedFabric == 'ActivityCenterFabric') {
        ListCreatDataModel = '[{"label":"Favorites","expand":false,"payload":[]},{"label":"Conversations","expand":false,"payload":[]}]';
        this.ListCreatData = JSON.parse(ListCreatDataModel);
      }

      this.commonService.addNew$.pipe(this.compUntilDestroyed()).subscribe((res) => {
        try {

          if (this.commonService.getFabricNameByUrl(this.router.url) == FabricsNames.ENTITYMANAGEMENT) {
            this.createPermission = true;
          }
          else if (this.commonService.lastOpenedFabric == FABRICS.BUSINESSINTELLIGENCE) {
            if (this.commonService.isCapbilityAdmin()) {
              this.createPermission = true;
            }
            else if (res && res["createpermission"] && res["createpermission"].length == 0) {
              this.createPermission = false;
            }
            else if (res && res["createpermission"] && res["createpermission"].length > 0) {
              this.createPermission = res["createpermission"][0]["Create"];
            }
          } else if (this.commonService.getFabricNameByUrl(this.router.url) == FabricsNames.SECURITY || this.commonService.lastOpenedFabric == FabricsPath.HOME) {
            this.createPermission = false;
          }
          else {
            this.createPermission = true;
          }
        }
        catch (e) {
          console.error('Exception in ngOnInit() of addNew$ subscriber of tree-activity.component  at TreeActivity time ' + new Date().toString() + '. Exception is : ' + e);
        }
      });
    }
    catch (e) {
      console.error('Exception in ngOnInit() of tree-activity.component  at TreeActivity time ' + new Date().toString() + '. Exception is : ' + e);
    }

    this.route.queryParams.pipe(this.compUntilDestroyed()).subscribe((queryParams:any) => {
      try {

        // this.queryParams = JSON.parse(JSON.stringify(queryParams));
        // this.commonService.queryParamsList =  this.queryParams;
       this.diableFilter = this.commonService.getFabricNameByUrl(this.router.url) == FabricsNames.SECURITY ? true : false;
       if (queryParams.tab && queryParams.tab == "Lists") {
          this.isListTab = true;
        }
        else {
          this.isListTab = false;
        }
       let fabricName= this.commonService.getFabricNameByUrl(this.router.url)
        if (this.ListQueryParmas && (this.ListQueryParmas.tab != queryParams.tab || this.commonService.getFabricNameByUrl(this.router.url).toUpperCase() != fabricName.toUpperCase())) {
          this.isSearchText = false;
           if(this.filterText)
           this.clearFilter();
          if(queryParams.leftnav == "false")
          this.commonService.leftHeaderSearchObject[this.tabList]["text"] = '';
        }
        if (this.ListQueryParmas && this.ListQueryParmas.hasOwnProperty('leftnav') &&!JSON.parse(this.ListQueryParmas.leftnav)&&this.ListQueryParmas.leftnav != queryParams.leftnav){
          this.treeInitilize = false;
          setTimeout(() => {
            this.treeInitilize = true;
          },100);
        }
       this.ListQueryParmas =JSON.parse(JSON.stringify(queryParams))
        if (this.commonService.lastOpenedFabric == FABRICS.ENTITYMANAGEMENT) {
          this.createPermission = true;
      }
       if(this.commonService.getFabricNameByUrl(this.router.url)=="Security"){
          this.createPermission = false;
      }
        this.commonService.queryParamsList = JSON.parse(JSON.stringify(queryParams));

        if (this.tabList != queryParams['tab']) {
          this.tabList = queryParams['tab'];
          if (this.fabric == "Security" && this.commonService.getRolesOrSecurityGroupsMode() == "roles") {
            this.isExpand = true;
          } else {
            this.isExpand = false;
          }
        }
      }
      catch (e) {
        console.error('Exception in route.queryParams subscriber of tree-activity.component  at TreeActivity time ' + new Date().toString() + '. Exception is : ' + e);
      }

    });
  }

  switchQueryParams(queryParams) {
    try {
      this.commonService.leftHeaderConfig.filter((list) => {
        if (list.title == queryParams.tab || (queryParams.tab == "List" && list.title == TreeTypeNames.LIST)) {
          list.routerLinkActive = true;
          this.commonService.treeIndexLabel = list.title;
        }
        else
          list.routerLinkActive = false;
        return list;
      });
    }
    catch (e) {
      console.error('Exception in switchQueryParams() of tree-activity.component  at TreeActivity time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  storeLeftHeaderSearchObject() {
    this.commonService.leftHeaderSearchObject = {};
    this.commonService.leftHeaderConfig.forEach(header => {
      this.commonService.leftHeaderSearchObject[header.title] = { "isActive": false, "text": "" };
    })
    this.filterText = '';
  }

  getEventFromTree(msg: AngularTreeMessageModel){
    switch(msg.treeOperationType){
      case TreeOperations.NodeOnClick:
        if(this.filterText)
        this.clearFilter();
        break;
    }
  }

  Enablefilter(event) {
    this.commonService.isFilterIconActive = true
    // this.fdcService.onRefreshUrl = false;
    this.commonEntityFilter['_results'].filter(d => d.TreeName == this.commonService.treeIndexLabel)[0].Enablefilter()
  }
  openDialogs(messageToshow, Operation?, Data?): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: Operation != undefined ? true : false,
        content: [messageToshow],
        subContent: [],
        operation: Operation != undefined ? Operation : PopupOperation.Attention
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;

      let dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
      dialogRef.componentInstance.emitResponse.subscribe((Type: any) => {
        if (this.commonService.Addformclose != true) {
          this.commonService.Addformclose = true;
          this.commonService.createbyContextmenu = false;
          if (this.commonService.lastOpenedFabric == "assetsfabric") {
            this.commonService.doActivity$.next({ "Event": "NEW", "Value": "" });
          }
          else if (this.commonService.lastOpenedFabric) {
            this.commonService.addNewEntity$.next();
          }
          this.commonService.Addformclose = false;
        }
      });
    }
    catch (e) {
      console.error('Exception in openDialog() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  expandButtonShowHide() {
    let fabric = this.router.url.split('/')[1]
    if (fabric.includes('?'))
      fabric = fabric.split('?')[0]
    if (AppConsts.mutualFabric.includes(fabric) && this.commonService.treeIndexLabel == 'All')
      return false;
    else
      return true;
  }

  openofflineDialog(messageToshow): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: false,
        content: [messageToshow],
        subContent: [],
        operation: PopupOperation.Attention
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      this.dialog.open(PopUpAlertComponent, matConfig);
    }
    catch (ex) {
      console.log(ex);
    }
  }


  AddEntity() {
    try {
      this.commonService.isPlusAddForm = true;
      this.commonService.latlngFinderDivision = false;
      this.commonService.switchButton = 'AddNewEntity';
      if (this.commonService.CustomerAppsettings.env.UseAsDesktop == true && this.commonService.onlineOffline == false) {
        this.openofflineDialog(new Array("Oops - it seems you are offline currently. You can create entities only in online mode."));
      } else {
        if (this.commonService.Addformclose != true) {
          this.commonService.Addformclose = true;
          this.commonService.createbyContextmenu = false;
          this.commonService.CreatedByAddIcon = true;
          if (this.commonService.lastOpenedFabric == "assetsfabric") {
            this.commonService.doActivity$.next({ "Event": "NEW", "Value": "" });
          }
          else if (this.commonService.lastOpenedFabric) {
            this.commonService.addNewEntity$.next();
            switch (this.commonService.lastOpenedFabric.toLowerCase()) {
              case FABRICS.BUSINESSINTELLIGENCE.toLowerCase():
                break;
              case FABRICS.ENTITYMANAGEMENT.toLowerCase():
                if (this.tabList != TreeTypeNames.BusinessIntelligence)
                  this.sendDataToAngularTree(TreeOperations.deactivateSelectedNode, '');
                break;
              default:
                this.sendDataToAngularTree(TreeOperations.deactivateSelectedNode, '');
                break;
            }
          }
          this.commonService.Addformclose = false;
        }
      }
    } catch (e) {
      console.error('Exception in AddEntity() of tree-activity.component  at TreeActivity time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  // private openDialogAttention(messageToshow: any, Operation?: any, Data?: any): void {

  //   try {

  //     const config = {
  //       header: 'Attention!',
  //       isSubmit: Operation !== undefined ? true : false,
  //       content: [messageToshow],
  //       subContent: [],
  //       operation: Operation !== undefined ? Operation : PopupOperation.Attention
  //     };

  //     const matConfig = new MatDialogConfig();
  //     matConfig.data = config;
  //     matConfig.width = '500px';
  //     matConfig.disableClose = true;

  //     const dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
  //     dialogRef.componentInstance.emitResponse.subscribe((Type: any) => {
  //       console.log(Type);
  //     });

  //     dialogRef.afterClosed().subscribe((status: string) => {
  //       console.log(status);
  //       if (status === 'save') {
  //         this.commonService.addNewEntity$.next();
  //         this.sendDataToAngularTree(TreeOperations.deactivateSelectedNode, '');
  //         this.commonService.islistformValidation = false;
  //       }
  //     });

  //   } catch (e) {
  //     console.error('Exception in openDialog() of create-entity.component in create-entity-dialog at time '
  //       + new Date().toString() + '. Exception is : ' + e + new Error().stack);
  //   }

  // }

  searchIconClick(event) {
    try {
      this.isSearchText = !this.isSearchText;

      if (!this.isSearchText && this.commonService.leftHeaderSearchObject && this.commonService.leftHeaderSearchObject[this.tabList]) {
        this.commonService.leftHeaderSearchObject[this.tabList]["isActive"] = this.isSearchText;
        if(this.filterText)
        this.clearFilter();
        this.commonService.searchActive = false;
      }
      else {
        if (this.commonService.leftHeaderSearchObject && this.commonService.leftHeaderSearchObject[this.tabList])
          this.commonService.leftHeaderSearchObject[this.tabList]["isActive"] = this.isSearchText;
        this.setFocus()
        this.commonService.searchActive = true;
      }

    }
    catch (e) {
      console.error('Exception in onClickFilter() of tree-activity.component  at TreeActivity time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  setFocus() {
    if (this.filterFocus && this.filterFocus.nativeElement)
      setTimeout(() => { this.filterFocus.nativeElement.focus(); }, 0)
  }

  searchEntity(event,keypadResonse?) {
    try {
      let value;
      if(event)
      value = event.target.value ? event.target.value : '';
      else
      value=keypadResonse;

      let Fabric = this.commonService.getFabricNameByUrl(this.router.url);
      if ([FabricsNames.SECURITY].includes(Fabric)) {
        if (event && event.keyCode == 13 && value.length < 4) {
          new alertPopUp().openDialog(FOUR_CHARACTERS_REQUIRED, this.dialog);
        }
         else if (value.length > 3) {
          this.sendDataToAngularTree(TreeOperations.entityFilter, value);
          // this.commonService._getLeftTreeSearchValue.next(value);
          this.commonService.leftHeaderSearchObject[this.tabList]["text"] = this.filterText = value;
        }
         else if (value.length <= 3) {
          this.sendDataToAngularTree(TreeOperations.entityFilter, '');
          // this.commonService._getLeftTreeSearchValue.next(null);
        }
      }
      else {
        var message: AngularTreeMessageModel = {
          "fabric": this.commonService.lastOpenedFabric,
          "treeName": [this.commonService.treeIndexLabel],
          "treeOperationType": TreeOperations.entityFilter,
          "treePayload": value
        };
        if (event && event.keyCode == 13 && value.length < 4) {
          new alertPopUp().openDialog(FOUR_CHARACTERS_REQUIRED, this.dialog);
        }
        else if (value.length > 3 && this.commonService.leftHeaderSearchObject && this.commonService.leftHeaderSearchObject[this.tabList]) {
          this.commonService.leftHeaderSearchObject[this.tabList]["text"] = this.filterText = value;
          this.commonService.sendDataFromAngularTreeToFabric.next(message);
        }
        else if ((value.length <= 3) && this.commonService.leftHeaderSearchObject && this.commonService.leftHeaderSearchObject[this.tabList]) {
          message.treePayload = ''
          this.commonService.leftHeaderSearchObject[this.tabList]["text"] = '';
          this.commonService.sendDataFromAngularTreeToFabric.next(message);
          this.isExpand=true
          this.ExpandCollapse()
        }
      }
    }
    catch (e) {
      console.error('Exception in searchEntity() of tree-activity.component  at TreeActivity time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }



  ExpandCollapse() {
    this.isExpand = !this.isExpand;
    this.UpdateExpandAndCollapse();
  }

  UpdateExpandAndCollapse() {
    var configdata: ITreeConfig = { "treeExpandAll": this.isExpand };
    this.sendDataToAngularTree(TreeOperations.treeConfig, configdata);
  }
  UpdateActiveExpandAndCollapse() {
    var configdata: ITreeConfig = { 'treeActiveOnClick': this.activenode };
    this.sendDataToAngularTree(TreeOperations.treeConfig, configdata);
  }

  sendDataToAngularTree(treeOperationType: string, payload: any) {
    try {
      var treeMessage: AngularTreeMessageModel = {
        "fabric": this.fabric,
        "treeName": [this.commonService.treeIndexLabel],
        "treeOperationType": treeOperationType,
        "treePayload": payload,
      }
      this.commonService.sendDataToAngularTree.next(treeMessage);
    }
    catch (e) {
      console.error('exception in sendDataToAngularTree() of Tree Component at time ' + new Date().toString() + '. exception is : ' + e);
    }
  }

  clearFilter() {
    try {
      if(this.filterText){
      this.filterText = '';
      if (this.commonService.leftHeaderSearchObject && this.commonService.leftHeaderSearchObject[this.tabList])
        this.commonService.leftHeaderSearchObject[this.tabList]["text"] = '';
      let Fabric = this.commonService.getFabricNameByUrl(this.router.url);
      if ([FabricsNames.SECURITY].includes(Fabric)) {
        this.sendDataToAngularTree(TreeOperations.entityFilter, '');
        if(this.commonService.getRolesOrSecurityGroupsMode() == 'securitygroups'){
          var configdata: ITreeConfig = { "treeExpandAll": false };
          this.sendDataToAngularTree(TreeOperations.treeConfig, configdata);
        }
      }
      else{
        var message: AngularTreeMessageModel = {
          "fabric": this.commonService.lastOpenedFabric,
          "treeName": [this.commonService.treeIndexLabel],
          "treeOperationType": TreeOperations.entityFilter,
          "treePayload": ''
        };
        this.commonService.sendDataFromAngularTreeToFabric.next(message);
        this.commonService.searchActive = false;

        // if(this.router && this.router.url && this.router.url.includes('EntityID')){
        //   // this.route.queryParams.pipe(untilDestroyed(this)).subscribe((params:any)=>{
        //     let entityId=this.commonService.getEntityIDFromUrl(this.router.url);
        //     this.sendDataToAngularTree(TreeOperations.ActiveSelectedNode, entityId);
        //   // })
        // }
      }
    }
      // this.commonService._getLeftTreeSearchValue.next(null);
      this.isSearchText = false;
      if (this.commonService.leftHeaderSearchObject && this.commonService.leftHeaderSearchObject[this.commonService.treeIndexLabel])
        this.commonService.leftHeaderSearchObject[this.commonService.treeIndexLabel]["isActive"] = false;
      this.setFocus()


    }
    catch (e) {
      console.error('Exception in clearFilter() of tree-activity.component  at TreeActivity time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  searchTextFieldClick($event){

  }

  method1CallForClick(){
        if(this.isExpand==true){
            this.ExpandCollapse()
        }
        else{
          this.isExpand=false
          this.ExpandCollapse()
        }
  }
  // method2CallForDblClick(){
  //   this.isSingleClick = false;
  //   this.isExpand=false
  //   this.ExpandCollapse()
  // }
  matExpnasionOnOpen(eve, objData) {
    objData.expand = false;
  }
  matExpnasionOnClose(eve, objData) {
    objData.expand = false;
  }
  IconExpand(eve, objData) {
    if (objData.expand) {
      objData.expand = false;
    }
    else {
      objData.expand = true;
    }
  }
  expandPanel(matExpansionPanel: MatExpansionPanel, event: Event, colDat) {
    event.stopPropagation();
  }

  ngOnDestroy() {
    this.commonService.isPlusAddForm = false;
    // this.commonService.isCopyToEntityForm = false;
    // this.commonService.isCreateNewEquipmentForm = false;
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }
  compUntilDestroyed():any {
    return takeUntil(this.takeUntilDestroyObservables);
    }

  openDialog(messageToshow): void {
    // try {
    //   let matConfig = new MatDialogConfig()
    //   matConfig.data = messageToshow;
    //   matConfig.width = '500px';
    //   matConfig.disableClose = true;
    //   matConfig.id = messageToshow["header"];
    //   if(this.commonService.tabletMode)
    //   matConfig.hasBackdrop=false;
    //   let dialogref = this.dialog.open(CommonInputPopupComponent, matConfig);
    //   dialogref.componentInstance.emitResponse.subscribe((res) => {
    //     let value = res;
    //     if (this.commonService.righttreeContextClick) {
    //       this.commonService.righttreeContextClick = false;
    //       // this.commonService.sendListNameToRightSideTree.next(this.commonService.nameoflist);
    //     } else {
    //       this.commonService.ValidateListForm(value);
    //       this.commonService.sendCreatedListOrFormOpen$.next();
    //     }
    //   })
    // }
    // catch (e) {
    //   console.error('Exception in openDialog() of tree-activity.component  at TreeActivity time ' + new Date().toString() + '. Exception is : ' + e);
    // }
  }
  leftTreeChangeOnFilterOnOrOFF(event) {
    let filterResult = this.commonEntityFilter['_results'].filter(d => d.TreeName == this.commonService.treeIndexLabel)[0];

    if (filterResult) {
        filterResult.closeOptions();
    }
    this.treeareas.forEach(element => {
      if (event) {
        element.nativeElement.style.top = '70px';
        element.nativeElement.style.height = 'calc(100% - 70px)';
      }
      else {
        element.nativeElement.style.top = '42px';
        element.nativeElement.style.height = 'calc(100% - 48px)';
      }
    })

  }

  getTreeLabel() {
    const urlParams = new URLSearchParams(this.router.url);
    const emTab = urlParams.get('EMTab');
    const physicalItem = urlParams.get('physicalItem');

    let fabric = this.commonService.getFabricNameByUrl(this.router.url);
    let treeLabel = "";
    switch (fabric) {
      case FabricsNames.ENTITYMANAGEMENT:
        switch (this.commonService.treeIndexLabel) {
          case "Production Management":
            treeLabel = "PRODUCTION MGMT";
            break;

          case "Business Intelligence":
            treeLabel = "Analytics";
            break;

          case "Operational Forms":
          case "People & Companies":
          case "Pigging":
            treeLabel = this.commonService.treeIndexLabel;
            break;

          default:
            treeLabel = physicalItem ? physicalItem : emTab ? emTab : this.commonService.treeIndexLabel;
            break;
        }
        break;

      //add other fabric here
      default:
        treeLabel = this.commonService.treeIndexLabel;
        break;
    }
    if (treeLabel.includes(" Role")) {
      treeLabel = treeLabel.replace(" Role", "");
    }
    return treeLabel;
  }

  applayCssStyles(list) {
    let fabricName = this.commonService.getFabricNameByUrl(this.router.url);
    if (fabricName == FABRICS.SECURITY) {
      if (this.commonService.treeIndexLabel === list.title && list.show) {
        return { 'visibility': 'visible', 'z-index': '1' }
      } else {
        return { 'visibility': 'hidden', 'z-index': '0' }
      }
    } else {
      if (this.commonService.treeIndexLabel === list.title) {
        return { 'visibility': 'visible', 'z-index': '1' }
      } else {
        return { 'visibility': 'hidden', 'z-index': '0' }
      }
    }
  }

}
