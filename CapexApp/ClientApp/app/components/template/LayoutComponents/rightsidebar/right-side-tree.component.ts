import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AppConsts } from 'ClientApp/app/components/common/Constants/AppConsts';
import { PopUpAlertComponent } from 'ClientApp/app/components/common/PopUpAlert/popup-alert.component';
import { PopupOperation } from 'ClientApp/app/components/globals/Model/AlertConfig';
import { EntityTypes, ITreeConfig } from 'ClientApp/app/components/globals/Model/EntityTypes';
import { Subject } from 'rxjs';
import { takeUntil } from "rxjs/operators";
import { alertPopUp } from '../../../common/AttentionPopUp/popup';
import { DISCARD_CHANGES, FOUR_CHARACTERS_REQUIRED } from '../../../common/Constants/AttentionPopupMessage';
// import { GisEventMessage } from '../../../common/GisMapComponent/model/map.models';
// import { MapService } from '../../../common/GisMapComponent/services/map.service';
import { CommonService } from '../../../globals/CommonService';
import { AngularTreeMessageModel, CAPABILITY_NAME, FABRICS, FabricsNames, RightSideClickTreeEventModel, TreeOperations, TreeTypeNames } from '../../../globals/Model/CommonModel';
import { FabricRouter } from '../../../globals/Model/FabricRouter';
import { Routing } from '../../../globals/Model/Message';

@Component({
  selector: 'right-tree',
  templateUrl: 'right-side-tree.component.html',
  styleUrls: ['./right-side-tree.component.styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class RightSideTreeComponent implements OnInit, OnDestroy {

  myForm: FormGroup;
  // flexMode = ['FFVGROUP', 'FFVEVENT', 'OPFORMS', 'SECURITYROLES'];
  overLay = ["areas", "production", "layers", "views", "securityGroupsTreeForPermissions", "rolesTreeForPermissions", "peopleTreeForPermissions", "rolesTreeForPermissions"]
  excluedFabric = [FABRICS.ENTITYMANAGEMENT, FABRICS.HOME, FABRICS.SECURITY];
  public static clientXValue: any;
  public static clientYValue: any;
  contextMenuOption = [{
    displayName: 'RightTree Context Menu',
    children: [
      {
        displayName: 'Add to list',
        children: [{ displayName: 'Create New List', children: [] }]
      }]
  }]
  entitydata: any
  schema;
  datas;
  isSerachText: boolean = false;
  @ViewChild('inputSearch', { static: false })
  filterFocus: ElementRef;
  @ViewChild(MatMenuTrigger, { static: false }) trigger: MatMenuTrigger;
  leftPanelTab = "";
  leftMenuCapabilities: any;
  activeHeader: any;
  headerId: any;
  isFabricAdmin = false;
  ListQueryParmas: any = {};
  layerViewPermission: boolean = false;
  ListChild: any = {};
  navjsonTemplateConfig = {
    'Home': {
      "Default": {
        LEFTNAVBAR: [],
        RIGHTNAVBAR: []

      }
    },
    'Entity Management': {
      "Default": {
        LEFTNAVBAR: [{ 'Label': TreeTypeNames.CONSTRUCTION, 'Status': true }, { 'Label': TreeTypeNames.TaggedItems, 'Status': true }, { 'Label': TreeTypeNames.Schedule, 'Status': true }, { 'Label': TreeTypeNames.Documents, 'Status': true }, { 'Label': TreeTypeNames.Materials, 'Status': true }, { 'Label': TreeTypeNames.PhysicalInstances, 'Status': true }, { 'Label': TreeTypeNames.Maps, 'Status': true }],
        RIGHTNAVBAR: []
      }
    },
    'Construction': {
      "Default": {
        LEFTNAVBAR: [{ 'Label': TreeTypeNames.Schedule, 'Status': true }, { 'Label': TreeTypeNames.Approvals, 'Status': true }, { 'Label': TreeTypeNames.TaggedItems, 'Status': true }, { 'Label': TreeTypeNames.PhysicalInstances, 'Status': true }, { 'Label': TreeTypeNames.Materials, 'Status': true }, { 'Label': TreeTypeNames.Documents, 'Status': true }, { 'Label': TreeTypeNames.Directory, 'Status': true }, { 'Label': TreeTypeNames.Procedures, 'Status': true }, { 'Label': TreeTypeNames.Turnover, 'Status': true }],
        RIGHTNAVBAR: []
      }
    },
    "People": {
      "Default": {
        LEFTNAVBAR: [{ 'Label': 'ALL', 'Status': true }],
        RIGHTNAVBAR: []
      }
    },
    "Security": {
      "Default": {
        LEFTNAVBAR: [{ 'Label': TreeTypeNames.BusinessIntelligence, 'Status': true },{ 'Label': TreeTypeNames.CONSTRUCTION, 'Status': true }, { 'Label': TreeTypeNames.PROJECTS, 'Status': true }, { 'Label': TreeTypeNames.FABRICATION, 'Status': true }, { 'Label': TreeTypeNames.POSSIBLEPIPELINEINTEGRITY, 'Status': true }, { 'Label': TreeTypeNames.REPORTING, 'Status': true },{ 'Label': TreeTypeNames.BusinessIntelligence + "_Role", 'Status': true },{ 'Label': TreeTypeNames.CONSTRUCTION + "_Role", 'Status': true }, { 'Label': TreeTypeNames.PROJECTS + "_Role", 'Status': true }, { 'Label': TreeTypeNames.FABRICATION + "_Role", 'Status': true }, { 'Label': TreeTypeNames.POSSIBLEPIPELINEINTEGRITY + "_Role", 'Status': true }, { 'Label': TreeTypeNames.REPORTING + "_Role", 'Status': true }],
        RIGHTNAVBAR: [{ 'Label': 'SECURITYROLESADDNEW', 'Status': true }, { 'Label': 'SECURITYGROUPS', 'Status': true }, { 'Label': 'PEOPLE', 'Status': true }, { 'Label': 'SECURITYROLES', 'Status': false }]
      }
    },
    "Business Intelligence": {
      "Default": {
        LEFTNAVBAR: [{ 'Label': 'ALL', 'Status': true }, { 'Label': 'LIST', 'Status': true }, { 'Label': 'SEARCH', 'Status': false }],
        RIGHTNAVBAR: []
      }
    },
    "Maps": {
      "Default": {
        LEFTNAVBAR: [{ 'Label': 'ALL', 'Status': true }],
        RIGHTNAVBAR: []
      }
    },
    "Reporting": {
      "Default": {
        LEFTNAVBAR: [{ 'Label': 'ALL', 'Status': true }],
        RIGHTNAVBAR: []
      }
    }
  }


  HeaderConfigAll: any =
    {
      'LICENSEE': {
        'source': 'Licencee',
        'id': 'licensee',
        'routerLinkActive': false,
        'title': 'Licensee',
        'alise': 'Licensee',
        'class': 'icon21-21',
        'type': 'tree',
        'add': true,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'SCHEDULES': {
        'source': 'V3 Schedulers',
        'id': 'schedules',
        'routerLinkActive': false,
        'title': 'Schedules',
        'alise': 'Schedules',
        'class': 'icon21-21',
        'type': 'tree',
        'add': true,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'AREAS': {
        'source': 'areas',
        'id': 'areas',
        'routerLinkActive': false,
        'title': 'Areas',
        'alise': 'Areas',
        'class': 'icon21-21',
        'type': 'form',
        'add': false,
        'search': false,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'PRODUCTION': {
        'source': 'production',
        'id': 'production',
        'routerLinkActive': false,
        'title': 'Production',
        'alise': 'Production',
        'class': 'icon21-21',
        'type': 'form',
        'add': false,
        'search': false,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'OPFORMS': {
        'source': 'V3 Forms',
        'id': 'opforms',
        'routerLinkActive': false,
        'title': 'OP Forms',
        'alise': 'OP Forms',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true

      },
      'LAYERS': {
        'source': 'V3 Layers',
        'id': 'layers',
        'routerLinkActive': false,
        'title': 'LOCATIONS',
        'alise': 'LOCATIONS',
        'class': 'icon21-21',
        'type': 'form',
        'add': false,
        'search': false,
        'addtooltip': 'ADD',
        'isExpand': true
      },

      'PHYSICAL FLOW': {
        'source': 'V3 Hierarchy',
        'id': 'hierarchy',
        'routerLinkActive': false,
        'title': 'PHYSICAL FLOW',
        'alise': 'Hierarchy',
        'class': 'icon21-21',
        'type': 'tree',
        'add': true,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },

      'FACILITY CODE': {
        'source': 'V3 FacilityCode',
        'id': 'FacilityCode',
        'routerLinkActive': false,
        'title': 'FACILITY CODE',
        'alise': 'FacilityCode',
        'class': 'icon21-21',
        'type': 'tree',
        'add': true,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'VIEWS': {
        'source': 'V3 Views',
        'id': 'views',
        'routerLinkActive': false,
        'title': 'Views',
        'alise': 'Views',
        'class': 'icon21-21',
        'type': 'form',
        'add': true,
        'search': false,
        'addtooltip': 'SAVE AS VIEW',
        'isExpand': true
      },
      'ASSET TYPES': {
        'source': 'AssetType',
        'id': 'AssetTypes',
        'routerLinkActive': false,
        'title': 'ASSET TYPES',
        'alise': 'ASSET TYPES',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'SECURITYGROUPS': {
        'source': 'V3 SecurityGroups',
        'id': 'securityGroupsTreeForPermissions',
        'routerLinkActive': false,
        'title': 'Security Groups',
        'alise': 'Security Groups',
        'class': 'icon21-21',
        'type': 'tree',
        'add': true,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'SECURITYROLES': {
        'source': 'V3 Roles',
        'id': 'rolesTreeForPermissionsRead',
        'routerLinkActive': false,
        'title': 'SECURITY ROLES',
        'alise': 'SECURITY ROLES',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'FFVGROUP': {
        'source': 'FFVGroupTreeIcon',
        'id': 'ffvGroup',
        'routerLinkActive': false,
        'title': 'FFV Group',
        'alise': 'FFV Group',
        'class': 'icon21-21',
        'type': 'tree',
        'add': true,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'FFVEVENT': {
        'source': 'FFVEVENT',
        'id': 'ffvEvent',
        'routerLinkActive': false,
        'title': 'FFV Event',
        'alise': 'FFV Event',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'PEOPLE': {
        'source': 'V3 PeepsAndComps',
        'id': 'peopleTreeForPermissions',
        'routerLinkActive': false,
        'title': 'People & Companies',
        'alise': 'People & Companies',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': false
      },
      'ROLES': {
        'source': 'V3 Roles',
        'id': 'rolesTreeForPermissionsRead',
        'routerLinkActive': false,
        'title': 'Roles',
        'alise': 'Roles',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'DATASETS': {
        'source': 'V3 DataSets',
        'id': 'BigDataTree',
        'routerLinkActive': false,
        'title': 'DataSets',
        'alise': 'DataSets',
        'class': 'icon21-21',
        'type': 'tree',
        'add': true,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'ALL': {
        'source': 'V3 All',
        'id': 'all',
        'alise': 'All',
        'routerLinkActive': false,
        'title': 'All',
        'class': 'icon16-21',
        'add': false,
        'search': false,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'LIST': {
        'source': 'V3 Lists',
        'id': 'list',
        'routerLinkActive': false,
        'title': 'Lists',
        'alise': 'List',
        'class': 'icon16-21',
        'add': false,
        'search': false,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'SEARCH': {
        'source': 'V3 Search',
        'id': 'search',
        'routerLinkActive': false,
        'title': 'Search',
        'alise': 'Search',
        'class': 'icon16-21',
        'add': false,
        'search': false,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'FDCSCHEDULE': {
        'source': 'V3 Schedulers',
        'id': 'Schedule',
        'routerLinkActive': false,
        'title': 'Schedule',
        'alise': 'Schedule',
        'class': 'icon16-21',
        'type': 'tree',
        'add': true,
        'search': false,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'LOCATIONS': {
        'source': 'V3 Locations',
        'id': 'layers',
        'routerLinkActive': false,
        'title': 'Hierarchy',
        'alise': 'Locations',
        'class': 'icon21-21',
        'add': false,
        'search': false,
        'addtooltip': 'ADD',
        'isExpand': true
      },

      'HIERARCHY': {
        'source': 'V3 Hierarchy',
        'id': 'hierarchy',
        'routerLinkActive': false,
        'title': 'Hierarchy',
        'alise': 'Hierarchy',
        'class': 'icon21-21',
        'type': 'tree',
        'add': true,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'PROPERTIES': {
        'source': 'propeties',
        'id': 'properties',
        'routerLinkActive': false,
        'title': 'properties',
        'alise': 'properties',
        'class': 'icon21-21',
        'type': 'form',
        'add': false,
        'search': false,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'Production Management_RIGHT': {
        'source': 'AssetType',
        'id': 'Production Management_RIGHT',
        'routerLinkActive': false,
        'title': 'ENTITY TYPES',
        'alise': 'ENTITY TYPES',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'People & Companies_RIGHT': {
        'source': 'AssetType',
        'id': 'People & Companies_RIGHT',
        'routerLinkActive': false,
        'title': 'ENTITY TYPES',
        'alise': 'ENTITY TYPES',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'Business Intelligence_RIGHT': {
        'source': 'AssetType',
        'id': 'Business Intelligence_RIGHT',
        'routerLinkActive': false,
        'title': 'ENTITY TYPES',
        'alise': 'ENTITY TYPES',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'Operational Forms_RIGHT': {
        'source': 'AssetType',
        'id': 'Operational Forms_RIGHT',
        'routerLinkActive': false,
        'title': 'ENTITY TYPES',
        'alise': 'ENTITY TYPES',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'Pigging_RIGHT': {
        'source': 'AssetType',
        'id': 'Pigging_RIGHT',
        'routerLinkActive': false,
        'title': 'Pigging TYPES',
        'alise': 'Pigging TYPES',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'Security_RIGHT': {
        'source': 'AssetType',
        'id': 'Security_RIGHT',
        'routerLinkActive': false,
        'title': 'Entity Types',
        'alise': 'Entity Types',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'SECURITYROLESADDNEW': {
        'source': 'V3 RolesAdd',
        'id': 'rolesTreeForPermissions',
        'routerLinkActive': false,
        'title': 'SECURITY ROLES',
        'alise': 'SECURITY ROLES',
        'class': 'icon21-21',
        'type': 'tree',
        'add': true,
        'search': true,
        'addtooltip': 'ADD',
        'isExpand': true
      },
      'Business Intelligence': {
        'source': 'V3 BusinessIntelligence',
        'id': 'bi',
        'routerLinkActive': false,
        'title': 'Business Intelligence',
        'alise': 'Business Intelligence',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': true,
        'isExpand':true
      },
      'Construction': {
        'source': 'Construction_white_outlined',
        'id': 'construction',
        'routerLinkActive': false,
        'title': 'Construction',
        'alise': 'Construction',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': true,
        'isExpand':true
      },
      'Tagged Items': {
        'source': 'TaggedItemsTreeIcon',
        'id': 'taggedItems',
        'routerLinkActive': false,
        'title': 'Tagged Items',
        'alise': 'Tagged Items',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': true,
        'isExpand': true
      },
      'Turnover': {
        'source': 'TurnoverTreeIcon',
        'id': 'turnover',
        'routerLinkActive': false,
        'title': 'Turnover',
        'alise': 'Turnover',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': true,
        'isExpand': true
      },
      'Physical Instances': {
        'source': 'PhysicalInstancesTreeIcon',
        'id': 'physicalInstances',
        'routerLinkActive': false,
        'title': 'Physical Instances',
        'alise': 'Physical Instances',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': true,
        'isExpand': true
      },
      'Schedule': {
        'source': 'ScheduleTreeIcon',
        'id': 'schedule',
        'routerLinkActive': false,
        'title': 'Schedule',
        'alise': 'Schedule',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': true,
        'isExpand': true
      },
      'Directory': {
        'source': 'DirectoryTreeIcon',
        'id': 'Directory',
        'routerLinkActive': false,
        'title': 'Directory',
        'alise': 'Directory',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': true,
        'isExpand': true
      },
      'Procedures': {
        'source': 'ProceduresTreeIcon',
        'id': 'Procedures',
        'routerLinkActive': false,
        'title': 'Procedures',
        'alise': 'Procedures',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': true,
        'isExpand': true
      },
      'Approvals': {
        'source': 'ApprovalsTreeIcon',
        'id': 'approvals',
        'routerLinkActive': false,
        'title': 'Approvals',
        'alise': 'Approvals',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': true,
        'isExpand': true
      },
      'Materials': {
        'source': 'MaterialsTreeIcon',
        'id': 'materials',
        'routerLinkActive': false,
        'title': 'Materials',
        'alise': 'Materials',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': true,
        'isExpand': true
      },
      'Documents': {
        'source': 'DocumentsTreeIcon',
        'id': 'documents',
        'routerLinkActive': false,
        'title': 'Documents',
        'alise': 'Documents',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': true,
        'isExpand': true
      },
      'Projects': {
        'source': 'Projects_white_outlined',
        'id': 'Projects',
        'routerLinkActive': false,
        'title': 'Projects',
        'alise': 'Projects',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': true,
        'isExpand':true
      },
      'Fabrication': {
        'source': 'Fabrication_white_outlined',
        'id': 'Fabrication',
        'routerLinkActive': false,
        'title': 'Fabrication',
        'alise': 'Fabrication',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': true,
        'isExpand':true
      },
      'Possible Pipeline Integrity': {
        'source': 'Pipeline_white_outlined',
        'id': 'Possible Pipeline Integrity',
        'routerLinkActive': false,
        'title': 'Possible Pipeline Integrity',
        'alise': 'Possible Pipeline Integrity',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': true,
        'isExpand':true
      },
      'Reporting': {
        'source': 'Reporting_white_outlined',
        'id': 'Reporting',
        'routerLinkActive': false,
        'title': 'Reporting',
        'alise': 'Reporting',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': true,
        'isExpand':true
      },
      'Business Intelligence_Role': {
        'source': 'V3 BusinessIntelligence',
        'id': 'bi',
        'routerLinkActive': false,
        'title': 'Business Intelligence Role',
        'alise': 'Business Intelligence',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': false,
        'isExpand':true
      },
      'Construction_Role': {
        'source': 'Construction_white_outlined',
        'id': 'Construction',
        'routerLinkActive': false,
        'title': 'Construction Role',
        'alise': 'Construction',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': false,
        'isExpand':true
      },
      'Projects_Role': {
        'source': 'Projects_white_outlined',
        'id': 'Projects',
        'routerLinkActive': false,
        'title': 'Projects Role',
        'alise': 'Projects',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': false,
        'isExpand':true
      },
      'Fabrication_Role': {
        'source': 'Fabrication_white_outlined',
        'id': 'Fabrication',
        'routerLinkActive': false,
        'title': 'Fabrication Role',
        'alise': 'Fabrication',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': false,
        'isExpand':true
      },
      'Possible Pipeline Integrity_Role': {
        'source': 'Pipeline_white_outlined',
        'id': 'Possible Pipeline Integrity',
        'routerLinkActive': false,
        'title': 'Possible Pipeline Integrity Role',
        'alise': 'Possible Pipeline Integrity',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': false,
        'isExpand':true
      },
      'Reporting_Role': {
        'source': 'Reporting_white_outlined',
        'id': 'Reporting',
        'routerLinkActive': false,
        'title': 'Reporting Role',
        'alise': 'Reporting',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': false,
        'isExpand':true
      }
    };

  treeDefaultExpandStatus = {
    'schedules': { isExpand: true },
    'securityGroupsTreeForPermissions': { isExpand: true },
    'rolesTreeForPermissionsRead': { isExpand: true },
    'ffvEvent': { isExpand: true },
    'ffvGroup': { isExpand: true },
    'Production Management_RIGHT': { isExpand: true },
    'People & Companies_RIGHT': { isExpand: true },
    'Business Intelligence_RIGHT': { isExpand: true },
    'Operational Forms_RIGHT': { isExpand: true },
    'Pigging_RIGHT': { isExpand: true },
    'Security_RIGHT': { isExpand: true },
    'rolesTreeForPermissions': { isExpand: true },
    'peopleTreeForPermissions': { isExpand: false },
  };

  fabric;
  filterText;
  treeIndexLabel
  private searchValue: string = undefined;
  takeUntilDestroyObservables = new Subject();
  FFVGroupDataRead: boolean = true;


  filterFunc(rightClickModel: RightSideClickTreeEventModel) {
    if (!rightClickModel.capability) {
      return true;
    }
    return false;
  }
  constructor(public dialog: MatDialog, public commonService: CommonService, public router: Router,
    public route: ActivatedRoute, private formBuilder: FormBuilder, private cdref: ChangeDetectorRef, private elementRef: ElementRef) {
    try {
      //this.commonService.rightTreeClick.pipe(this.compUntilDestroyed()).subscribe(head => {
      //  if (head && head != null)
      //    this.headerClickEvent(head);
      //});

      this.commonService.rightTreeClick.filter((res: RightSideClickTreeEventModel) => this.filterFunc(res)).pipe(this.compUntilDestroyed()).subscribe(head => {
        if (head && head != null)
          this.headerClickEvent(head['head']);
      });





      this.commonService.updateRightSideHeader$.pipe(this.compUntilDestroyed()).subscribe((message: any) => {
        try {
          this.commonService.isRightTreeEnable = false;
          this.commonService.layoutJSON.RightTree = false;
          this.activeHeader = null;
          this.datas = null;
          this.schema = null;
          this.commonService.treeIndexLabelRight = null;
          this.updateRightTreeHeader(this.fabric);
          // this.rightsidetreehiding();
          this.cdref.markForCheck();
        }
        catch (e) {
          console.error('Exception in constructor of updateRightSideHeader$ subscriber of RightSideTreeComponent at time ' + new Date().toString() + '. Exception is : ' + e);
          this.commonService.appLogException(new Error('Exception in constructor of updateRightSideHeader$ subscriber of RightSideTreeComponent at time ' + new Date().toString() + '. Exception is : ' + e));
        }

      });




      // this.commonService.sendListNameToRightSideTree.pipe(this.compUntilDestroyed()).subscribe((res) => {
      //   try {
      //     this.AddToListEvent(res);
      //   }
      //   catch (e) {
      //   }
      // });
    }
    catch (e) {
      console.error('Exception in constructor of RightSideTreeComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in constructor of RightSideTreeComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }


  ngOnInit() {
    try {
      this.myForm = new FormGroup({});
      let fabricName = this.commonService.getFabricNameByUrl(this.router.url);
      this.fabric = fabricName;
      this.activeHeader = null;
      this.datas = null;
      this.schema = null;
      this.commonService.treeIndexLabelRight = null;

      this.commonService.sideHeaderView = 'Default';

      this.updateRightTreeHeader(this.fabric);
      this.cdref.markForCheck();
      setTimeout(() => {
        // this.rightsidetreehiding();
      }, 0);

      this.router.events.filter((event: any) => event && event instanceof NavigationEnd).pipe(this.compUntilDestroyed()).subscribe((event: any) => {
        try {
          //if (event instanceof NavigationEnd) {
          let url = event.url;
          let fabricName = this.commonService.getFabricNameByUrl(url);
          for (var data of this.commonService.appConfig.AllHeaderRoutingListService) {
            if (data.Fabric == fabricName && data.admin == true && fabricName.toLowerCase() != FABRICS.SECURITY.toLowerCase()) {
              // this.rightsidetreehiding();
              this.isFabricAdmin = true;
              break;
            }
            else {
              this.isFabricAdmin = false;

              // Bug#56286: [New User] When opened any form must not display the right tree as expanded mode.
              if (data.Fabric == fabricName && data.admin == false && fabricName.toLowerCase() != FABRICS.SECURITY.toLowerCase()) {
                // this.rightsidetreehiding();
              }

            }
          }

          if (this.fabric != fabricName) {
            this.commonService.leftHeaderConfig = [];
            this.headerId = undefined;
            this.fabric = fabricName;
            this.activeHeader = null;
            this.datas = null;
            this.schema = null;
            this.commonService.treeIndexLabelRight = null;
            this.commonService.isRightTreeEnable = false;
            this.commonService.layoutJSON.RightTree = false;
            this.commonService.sideHeaderView = 'Default';
            setTimeout(() => {
              this.updateRightTreeHeader(this.fabric);
              // this.rightsidetreehiding();
              this.commonService.leftHeaderConfig.filter((list) => {
                if (list.title == this.ListQueryParmas["tab"] || (this.ListQueryParmas["tab"] == "List" && list.title == TreeTypeNames.LIST)) {
                  list.routerLinkActive = true;
                  this.commonService.treeIndexLabel = list.title;
                }
                else
                  list.routerLinkActive = false;
              });
              this.cdref.markForCheck();
            }, 0);
            this.isSerachText = false;
            this.clearFilter();
          }
          else if (this.isSerachText && this.filterText != '') {
            this.isSerachText = false;
            this.clearFilter();
          }
          // }
          this.commonService.updateLayout.next();
        }
        catch (e) {
          console.error('Exception in ngOnInit() of router.events subscriber of RightSideTreeComponent at time ' + new Date().toString() + '. Exception is : ' + e);
          this.commonService.appLogException(new Error('Exception in ngOnInit() of router.events subscriber of RightSideTreeComponent at time ' + new Date().toString() + '. Exception is : ' + e));
        }
      });


      this.route.queryParams.pipe(this.compUntilDestroyed()).subscribe((param: any) => {
        this.ListQueryParmas = JSON.parse(JSON.stringify(param));
        let index = this.commonService.rightSideCapabilityList.indexOf(this.HeaderConfigAll.FFVGROUP);
        if (param && param['FormType'] && param['FormType'] == 'DailyFFVAssignment') {
          if (index == -1) {
            this.commonService.rightSideCapabilityList = [];
            this.commonService.rightSideCapabilityList.push(this.HeaderConfigAll['FFVGROUP']);
            this.commonService.rightSideCapabilityList.push(this.HeaderConfigAll['FFVEVENT']);
            this.HeaderConfigAll['FFVGROUP']['routerLinkActive'] = false;
            this.FFVGroupDataRead = false;
            this.headerClickEvent(this.HeaderConfigAll['FFVGROUP']);
          }
        }
        else {
          if (index != -1) {
            this.commonService.rightSideCapabilityList = [];
            this.updateRightTreeHeader(this.fabric);
            if (this.activeHeader && (this.activeHeader.id == 'ffvGroup' || this.activeHeader.id == 'ffvEvent'))
              this.commonService.isRightTreeEnable = false;
            this.commonService.layoutJSON['RightTree'] = false;
          }
        }
        if (this.commonService.getFabricNameByUrl(this.router.url) == FabricsNames.ENTITYMANAGEMENT && param.tab && param.EntityID && ![TreeTypeNames.PeopleAndCompanies, TreeTypeNames.Construction].includes(param.tab) && !param.physicalItem) {
          let index = this.commonService.rightSideCapabilityList.filter(d => d.id == param.EMTab + '_RIGHT');
          if (index.length == 0) {
            this.commonService.rightSideCapabilityList = [];
            this.commonService.rightSideCapabilityList.push(this.HeaderConfigAll[param.EMTab + '_RIGHT']);
            this.HeaderConfigAll[param.EMTab + '_RIGHT']['routerLinkActive'] = false;
            this.headerClickEvent(this.HeaderConfigAll[param.EMTab + '_RIGHT']);
          }
          else {
            this.sendDataToAngularTree(param.EMTab + '_RIGHT', TreeOperations.deactivateSelectedNode, '');
          }
        }
        if (this.leftPanelTab != this.ListQueryParmas["tab"]) {
          this.leftPanelTab = this.ListQueryParmas["tab"];
        }
        if (this.isSerachText && (this.commonService.lastOpenedFabric == FabricsNames.SECURITY)) {
          if (this.filterText != '') {
            this.isSerachText = false;
            this.clearFilter();
          }
        }
        this.cdref.markForCheck();
      });

      // this.commonService.EntityManagementCommonFormObservable.pipe(this.compUntilDestroyed()).subscribe((res: any) => {
      //   let param=this.route.queryParams['_value'];
      //   if (this.commonService.getFabricNameByUrl(this.router.url) == FabricsNames.ENTITYMANAGEMENT&&param.tab!=TreeTypeNames.PeopleAndCompanies) {
      //     if (res && res.treeName) {
      //       if(param && param.EntityID)
      //       this.commonService.layoutJSON.RightHeader = true;
      //       this.commonService.rightSideCapabilityList = [];
      //       this.commonService.rightSideCapabilityList.push(this.HeaderConfigAll[res.treeName]);
      //       this.HeaderConfigAll[res.treeName]['routerLinkActive'] = false;
      //       this.headerClickEvent(this.HeaderConfigAll[res.treeName]);
      //     } else {
      //       this.commonService.layoutJSON.RightHeader = false;
      //     }
      //   }
      // });
    }
    catch (e) {
      console.error('Exception in ngOnInit() of RightSideTreeComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in ngOnInit() of RightSideTreeComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }




  onClickFilter() {
    try {
      this.isSerachText = !this.isSerachText;
      if (!this.isSerachText) {
        this.clearFilter();
        this.commonService.searchActive = false;
      }
      else {
        this.setFocus();
        this.commonService.searchActive = true;
      }
    }
    catch (e) {
      console.error('Exception in onClickFilter() of tree-activity.component  at TreeActivity time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }

  searchEntity(event) {
    try {
      let value = event.target.value ? event.target.value : '';
      this.searchValue = value;
      var message: AngularTreeMessageModel = {
        "fabric": this.commonService.lastOpenedFabric,
        "treeName": [this.headerId],
        "treeOperationType": TreeOperations.entityFilter,
        "treePayload": value
      };
      if (event.keyCode == 13 && value.length < 4)
        new alertPopUp().openDialog(FOUR_CHARACTERS_REQUIRED, this.dialog);
      else if (value.length > 3) {
        this.commonService.sendDataFromAngularTreeToFabric.next(message);
      }
      else if ((value.length == 3) && ((event.keyCode === 8 || event.keyCode === 46) || (event.ctrlKey === true && event.keyCode === 86))) {
        this.resetTree(message);
      } else if ((value.length < 3) && (event.ctrlKey === true && event.keyCode === 86)) {
        this.resetTree(message);
      }
    }
    catch (e) {
      console.error('Exception in searchEntity() of tree-activity.component  at TreeActivity time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  private resetTree(message) {
    try {
      message.treePayload = '';
      this.commonService.sendDataFromAngularTreeToFabric.next(message);
      this.expandTree();
    } catch (error) {
      console.error(error);
    }
  }

  clearFilter() {
    try {
      this.filterText = '';

      if (this.searchValue !== '' && this.searchValue !== undefined) {
        this.searchValue = undefined;

        let Fabric = this.commonService.getFabricNameByUrl(this.router.url);
        var message: AngularTreeMessageModel = {
          "fabric": this.commonService.lastOpenedFabric,
          "treeName": [this.headerId],
          "treeOperationType": TreeOperations.entityFilter,
          "treePayload": ''
        };
        this.commonService.sendDataFromAngularTreeToFabric.next(message);

        if (Fabric === FabricsNames.ENTITYMANAGEMENT) {
          this.expandTree();
        }
      }

      this.isSerachText = false;
      this.setFocus();
    }
    catch (e) {
      console.error('Exception in clearFilter() of tree-activity.component  at TreeActivity time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  private expandTree(): void {
    try {
      const configdata: ITreeConfig = { 'treeExpandAll': true };
      const treeMessage: AngularTreeMessageModel = {
        'fabric': this.commonService.lastOpenedFabric,
        'treeName': [this.headerId],
        'treeOperationType': TreeOperations.treeConfig,
        'treePayload': configdata
      }

      this.commonService.sendDataToAngularTree.next(treeMessage);
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  setFocus() {
    setTimeout(() => {
      if (this.filterFocus && this.filterFocus.nativeElement)
        this.filterFocus.nativeElement.focus();
    }, 0);
  }


  sendDataToAngularTree(treeName: string, treeOperationType: string, payload: any) {
    try {
      var treeMessage: AngularTreeMessageModel = {
        "fabric": this.commonService.lastOpenedFabric,
        "treeName": [treeName],
        "treeOperationType": treeOperationType,
        "treePayload": payload
      }
      this.commonService.sendDataToAngularTree.next(treeMessage);
    }
    catch (e) {
      console.error('Exception in sendDataToAngularTree() of tree-activity.component  at TreeActivity time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  updateRightTreeHeader(fabric) {
    try {
      let rightMenuCapabilities = this.commonService.rightSideCapabilityList = [];
      if (this.fabric && fabric != null) {
        rightMenuCapabilities = this.navjsonTemplateConfig[fabric][this.commonService.sideHeaderView]['RIGHTNAVBAR'];
        this.leftMenuCapabilities = this.navjsonTemplateConfig[fabric][this.commonService.sideHeaderView]['LEFTNAVBAR'];
      }
      if (rightMenuCapabilities) {
        rightMenuCapabilities.forEach((res) => {
          if (this.HeaderConfigAll[res['Label']]['title'] == this.commonService.treeIndexLabelRight)
            this.HeaderConfigAll[res['Label']]['routerLinkActive'] = true;
          else
            this.HeaderConfigAll[res['Label']]['routerLinkActive'] = false;
          if (res['Status'])
            this.commonService.rightSideCapabilityList.push(this.HeaderConfigAll[res['Label']]);
        })
      }
      if (this.headerId != 'ffvGroup' && this.headerId != 'ffvEvent')
        this.commonService.leftHeaderConfig = [];
      if (this.leftMenuCapabilities) {
        if (fabric == FABRICS.ENTITYMANAGEMENT) {
          this.commonService.appConfig.AllHeaderRoutingList.forEach(item => {
            let itemConfig = JSON.parse(JSON.stringify(item));
            if (itemConfig.admin && (this.excluedFabric.indexOf(item.id) == -1 && fabric == FABRICS.ENTITYMANAGEMENT)) {
              this.commonService.leftHeaderConfig.push(itemConfig);
              this.storeLeftHeaderSearchObject();
            }
          });
        }
        else {
          this.leftMenuCapabilities.forEach((res) => {
            if (res['Status'])
              this.commonService.leftHeaderConfig.push({ ...this.HeaderConfigAll[res['Label']] });
            this.storeLeftHeaderSearchObject();
          })
        }
        if (fabric == FABRICS.BUSINESSINTELLIGENCE) {
          this.HeaderConfigAll["LOCATIONS"]["routerLinkActive"] = false;
        }
        else if (fabric == FabricsNames.SECURITY) {
          this.commonService.leftHeaderConfig = this.commonService.leftHeaderConfig.filter(item =>
            this.commonService.appConfig.AllHeaderRoutingList.findIndex(subitem => (item.title.includes(subitem.title) && subitem.admin)) >= 0);
          if (this.commonService.leftHeaderConfig.length == 0) {
            this.commonService.layoutJSON.RightHeader = false;
            this.commonService.rightSideCapabilityList = [];
          } else {
            this.commonService.layoutJSON.RightHeader = true;
          }
        }
      }
      setTimeout(() => {
        this.commonService.layoutJSON.LeftHeader = this.commonService.leftHeaderConfig.length > 0;
        if (this.commonService.layoutJSON.RightHeader && fabric != FabricsNames.SECURITY) {
          this.commonService.layoutJSON.RightHeader = this.commonService.rightSideCapabilityList.length > 0
        }
        this.commonService.updateLayout.next();
      }, 0);
    }
    catch (e) {
      console.log(e);
    }
  }

  storeLeftHeaderSearchObject() {
    this.commonService.leftHeaderSearchObject = {};
    this.commonService.leftHeaderConfig.forEach(header => {
      this.commonService.leftHeaderSearchObject[header.title] = { "isActive": false, "text": "" };
    })
  }

  clickEvent() {
    let url = '';
    switch (this.activeHeader.id) {
      case 'securityGroupsTreeForPermissions':
      case 'rolesTreeForPermissions':
        this.AddNewSecurityRoleOrGroup(this.activeHeader);
        break;
      // case 'schedules':
      //   this.commonService.isNewScheduleGroup = true;
      //   url = '/' + FabricRouter.OPERATIONALFORM + '/schedule/edit';
      //   this.operationalService.LocationId = null;
      //   this.sendDataToAngularTree(TreeTypeNames.schedules, TreeOperations.deactivateSelectedNode, '');
      //   if (this.commonService.treeIndexLabel != null) {
      //     this.router.navigate([url], { queryParams: { "EntityID": this.commonService.GetNewUUID(), "EntityName": '', 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.checkTreeActive, 'rightTree' : this.commonService.OpFormRightPannel, 'NewSchedule' : this.commonService.isNewScheduleGroup} });
      //   }
      //   else {
      //     this.router.navigate([url], { queryParams: { "EntityID": this.commonService.GetNewUUID(), "EntityName": '', 'leftnav': false, 'rightTree' : this.commonService.OpFormRightPannel,'NewSchedule' :this.commonService.isNewScheduleGroup} });
      //   }

      //   break;
      case 'ffvGroup':
        // if (this.commonService.isFFVGroupValidation) {
        //   this.openAlertDialog(DISCARD_CHANGES);
        // } else {
        //   // this.fDCService.isFFvGroupForm = false;
        //   this.commonService.isNewFFVGroup = true;
        //   this.ListQueryParmas['EntityId'] = this.commonService.GetNewUUID();
        //   this.ListQueryParmas['EntityName'] = '';
        //   this.sendDataToAngularTree(TreeTypeNames.ffvGroup, TreeOperations.deactivateSelectedNode, '');
        //   this.router.navigate(['dailyassignment'], { relativeTo: this.route, queryParams: this.ListQueryParmas });
        // }
        break;
    }
  }
  openAlertDialog(messageToshow, res?): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: true,
        content: [messageToshow],
        subContent: [],
        operation: PopupOperation.AlertConfirm
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      this.commonService.popupformclose = false;

      let dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
      dialogRef.componentInstance.emitResponse.subscribe((data: any) => {
        // if (res) {
        //   this.commonService.isFFVGroupValidation = false;
        //   this.ListQueryParmas["EntityId"] = res.treePayload.data.EntityId;
        //   this.ListQueryParmas["EntityName"] = res.treePayload.data.EntityName;
        //   this.router.navigate(['dailyassignment'], { relativeTo: this.route, queryParams: this.ListQueryParmas });
        // }
        // else {
        //   this.commonService.isNewFFVGroup = true;
        //   this.commonService.isFFVGroupValidation = false;
        //   this.ListQueryParmas['EntityId'] = this.commonService.GetNewUUID();
        //   this.ListQueryParmas['EntityName'] = '';
        //   this.sendDataToAngularTree(TreeTypeNames.ffvGroup, TreeOperations.deactivateSelectedNode, '');
        //   this.router.navigate(['dailyassignment'], { relativeTo: this.route, queryParams: this.ListQueryParmas });
        // }
      });
      dialogRef.componentInstance.emitCancelResponse.subscribe((data: any) => {
        // if (this.commonService.isNewFFVGroup) {
        //   this.sendDataToAngularTree(TreeTypeNames.ffvGroup, TreeOperations.deactivateSelectedNode, '');
        // }
        // else {
        //   this.sendDataToAngularTree(TreeTypeNames.ffvGroup, TreeOperations.ActiveSelectedNode, this.commonService.queryParamsList["EntityId"]);
        // }
      });
    }
    catch (ex) {
      console.error('Exception in openPopUpDialog() of FFVGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + ex);
    }
  }

  AddNewSecurityRoleOrGroup(data) {
    this.commonService.AddNewSecurityRoleOrGroup$.next(data.title);
  }


  headerClickEvent(head) {
    try {
      if (head != null) {
        this.headerId = head.id;
        if (head.routerLinkActive) {
          head.routerLinkActive = false;
          if (this.treeDefaultExpandStatus[head.id]) {
            head.isExpand = this.treeDefaultExpandStatus[head.id].isExpand
          }
          this.commonService.rightSideCapabilityList.forEach((data: any) => {
            if (data.id == head.id)
              data.routerLinkActive = false;
          })
          this.commonService.isRightTreeEnable = false;
          this.commonService.layoutJSON.RightTree = false;
          let queryParams = JSON.parse(JSON.stringify(this.route.snapshot.queryParams));
          if(queryParams.hasOwnProperty('rightnav')){
            queryParams['rightnav'] = false;
            this.router.navigate([], { relativeTo: this.route, queryParams: queryParams });
          }
        }
        else {
          this.commonService.isRightTreeEnable = true;
          this.commonService.layoutJSON.RightTree = true;
          this.commonService.treeIndexLabelRight = head.title;
          this.commonService.rightSideCapabilityList.forEach((data: any) => {
            if (data.id == head.id)
              data.routerLinkActive = true;
            else
              data.routerLinkActive = false;
          });
          //if (((head.id == 'roles' || head.id == 'rolesTreeForPermissions') && this.commonService.lastOpenedFabric == FabricsNames.SECURITY) || (AppConsts.treeNameForPermission.includes(head.id) && this.commonService.isSecurityAdmin)) {
          //  if (head.id == 'rolesTreeForPermissions' && this.router && this.router.url &&  this.commonService.lastOpenedFabric == FabricsNames.SECURITY){
          //    head.add = this.router.url.includes('tab');
          //    if(this.router.url.includes('tab')){
          //      this.authorizationService.ShowPermissionTree(head);
          //    }
          //  }
          //  else{
          //    this.authorizationService.ShowPermissionTree(head);
          //  }
          //}
          if (AppConsts.treeNameForPermission.includes(head.id)) {
            if (AppConsts.permissionRightSideAddNew.includes(head.id)) {
              head.add = true;
            }
            else {
              head.add = false;
            }
            this.commonService.ShowPermissionTree$.next({ head: head, value: true });
          }
          else if (head.id == 'areas') {
            let payload = JSON.stringify({ "EntityId": null, "TenantName": null, "TenantId": null, "EntityType": "Schema", "Fabric": "FDC", "EntityInfoJson": null, "Info": "areas" });
          } else if (head.id == 'production') {
            let payload = JSON.stringify({ "EntityId": null, "TenantName": null, "TenantId": null, "EntityType": "Schema", "Fabric": "FDC", "EntityInfoJson": null, "Info": "production" });

            this.commonService.sendMessageToServer(payload, 'FDC', 'production', 'ProductionSchema', 'READ', Routing.OriginSession, 'production', 'FDC')
          } else if (head.id == 'properties') {
            let payload = JSON.stringify({ "EntityId": null, "TenantName": null, "TenantId": null, "EntityType": "Schema", "Fabric": "FDC", "EntityInfoJson": null, "Info": "properties" });

            this.commonService.sendMessageToServer(payload, 'FDC', 'properties', 'PropertiesSchema', 'READ', Routing.OriginSession, 'properties', 'FDC')
          } else if (head.id == 'layers') {
            let payload = JSON.stringify({ "EntityId": null, "TenantName": null, "TenantId": null, "EntityType": "Schema", "Fabric": "FDC", "EntityInfoJson": null, "Info": "layers" });

            this.commonService.sendMessageToServer(payload, 'FDC', 'layers', 'LayerSchema', 'READ', Routing.OriginSession, 'layers', 'FDC')

          } else if (head.id == 'views') {
            let payload = JSON.stringify({ "EntityId": null, "TenantName": null, "TenantId": null, "EntityType": "Schema", "Fabric": "FDC", "EntityInfoJson": null, "Info": "views" });
          } else if (head.id == 'ffvGroup' && this.FFVGroupDataRead) {
            let payload = JSON.stringify({ "EntityId": null, "TenantName": null, "TenantId": null, "EntityType": "ffvGroup", "Fabric": "FDC", "EntityInfoJson": null, "Info": "ffvGroup", "CapabilityId": this.commonService.capabilityFabricId });
            this.commonService.sendMessageToServer(payload, "FDC", null, "ffvGroup", "READ", "OriginSession", "ffvGroup", "FDC");
          } else if (head.id == 'ffvEvent') {
            let payload = JSON.stringify({ "EntityId": null, "TenantName": null, "TenantId": null, "EntityType": "ffvEvent", "Fabric": "FDC", "EntityInfoJson": null, "Info": "ffvEvent", "CapabilityId": this.commonService.capabilityFabricId });
            this.commonService.sendMessageToServer(payload, "FDC", null, "ffvEvent", "READ", "OriginSession", "ffvEvent", "FDC");
          }
          //  else if (head.id == 'schedules') {
          //   let payload = JSON.stringify({ "EntityId": null, "TenantName": null, "TenantId": null, "EntityType": "schedules", "Fabric": this.commonService.lastOpenedFabric, "EntityInfoJson": null, "Info": "schedules", "CapabilityId": this.commonService.capabilityFabricId });
          //   this.commonService.sendMessageToServer(payload, 'OpData', 'schedules', "schedules", "READ", "OriginSession", "schedules", this.commonService.lastOpenedFabric);
          // }else if(head.id == "opforms"){
          //  this.operationalService.getRightSideFormsTree();
          // }
          else if (this.commonService.getFabricNameByUrl(this.router.url) == FabricsNames.ENTITYMANAGEMENT) {
            var message: AngularTreeMessageModel = {
              "fabric": this.commonService.lastOpenedFabric,
              "treeName": [head.id],
              "treeOperationType": 'createRightTree',
              "treePayload": ''
            };
            this.commonService.sendDataFromAngularTreeToFabric.next(message)
          }
        }


        this.commonService.layoutJSON.OverLayRightTree = true;
        this.FFVGroupDataRead = true;
        this.isSerachText = false;
        this.schema = undefined;
        this.myForm = new FormGroup({});
        this.clearFilter();
        this.activeHeader = {};
        this.cdref.markForCheck();
        if (head.routerLinkActive) {
          this.activeHeader = head;
        }
        setTimeout(() => {
          this.cdref.markForCheck();
        }, 10);
      }
    }
    catch (e) {
      console.error('Exception in headerClickEvent() of RightSideTreeComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in headerClickEvent() of RightSideTreeComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  createGroup() {
    try {
      const group = this.formBuilder.group({});
      this.schema.forEach(control => {
        group.addControl(control.title, this.formBuilder.group({}));
        group.controls[control.title].valueChanges.subscribe((res) => {
          if (this.datas && Object.keys(this.datas).indexOf(control.title) != -1) {
            this.datas[control.title] = res;
            // var msgData: GisEventMessage = {
            //   messageType: "UpdateUserprefrenceMap",
            //   payload: this.datas
            // }
            // this.mapService.sendDataToGisMap.next(msgData);
          }
        })
      });
      return group;
    }
    catch (e) {
      console.error('Exception in createGroup() of RightSideTreeComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in createGroup() of RightSideTreeComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }

  submit() {
  }

  // rightSideTreeToggle() {
  //   try {
  //     this.commonService.rightSideCapabilityList.forEach((data: any) => {
  //       if (this.headerId) {
  //         if (data.id == this.headerId)
  //           if (!this.commonService.isRightTreeEnable) {
  //             data.routerLinkActive = false;
  //           } else {
  //             data.routerLinkActive = true;
  //           }
  //       }
  //     });

  //     if (this.commonService.rightSideCapabilityList.length == 0) {
  //       this.commonService.isRightTreeEnable = false;
  //     }

  //     if (this.headerId == undefined && this.commonService.rightSideCapabilityList.length > 0) {
  //       this.commonService.rightSideCapabilityList[0]["routerLinkActive"] = true;
  //       this.activeHeader = this.commonService.rightSideCapabilityList[0];
  //       this.headerId = this.activeHeader.id;
  //       this.commonService.treeIndexLabelRight = this.activeHeader.title;
  //       if (this.activeHeader.id == 'roles' && this.commonService.lastOpenedFabric == FabricsNames.SECURITY) {
  //         this.commonService.ShowPermissionTree$.next({ head: this.activeHeader });
  //       }
  //       else if (this.activeHeader.id == 'layers') {
  //         let payload = JSON.stringify({ "EntityId": null, "TenantName": null, "TenantId": null, "EntityType": "Schema", "Fabric": "FDC", "EntityInfoJson": null, "Info": "layers" });
  //         this.commonService.sendMessageToServer(payload, 'FDC', 'layers', 'LayerSchema', 'READ', Routing.OriginSession, 'layers', 'FDC')
  //       } else if (this.activeHeader.id == 'views') {
  //         let payload = JSON.stringify({ "EntityId": null, "TenantName": null, "TenantId": null, "EntityType": "Schema", "Fabric": "FDC", "EntityInfoJson": null, "Info": "views" });
  //         this.commonService.sendMessageToServer(payload, 'FDC', 'views', 'ViewSchema', 'READ', Routing.OriginSession, 'views', 'FDC')
  //       }
  //     }
  //     this.commonService.updateLayout.next();
  //   }
  //   catch (e) {
  //     console.error('Exception in rightSideTreeToggle() of RightSideTreeComponent at time ' + new Date().toString() + '. Exception is : ' + e);
  //     this.commonService.appLogException(new Error('Exception in rightSideTreeToggle() of RightSideTreeComponent at time ' + new Date().toString() + '. Exception is : ' + e));
  //   }
  // }

  ngOnDestroy() {
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }

  openmenulist(data, event) {
    if (data.title == 'LOCATIONS' && this.entitydata != undefined) {
      RightSideTreeComponent.clientXValue = event.clientX;
      RightSideTreeComponent.clientYValue = event.clientY;
      if (this.contextMenuOption.length > 0) {
        setTimeout(() => {
          if (this.commonService.activeContextMenuNodeId && this.commonService.activeContextMenuNodeId != "")
            document.getElementById(this.commonService.activeContextMenuNodeId).style.background = "var(--sideNode-LightBackGround)";
          this.createContextmenue();
        }, 10);
      }

    }
  }

  getselectedEntity(event) {
    this.entitydata = event;
  }

  createContextmenue() {
    try {
      var dd = this.elementRef.nativeElement.querySelector('.capMenu');
      dd.style.left = RightSideTreeComponent.clientXValue + "px";
      dd.style.top = RightSideTreeComponent.clientYValue + "px";
      this.trigger.openMenu();
      this.cdref.detectChanges();
      document.getElementsByClassName('cdk-overlay-backdrop')[0].addEventListener('contextmenu', (offEvent: any) => {
        offEvent.preventDefault();
        this.trigger.closeMenu();
      });
    }
    catch (e) {
      console.error("Exception in createContextmenu() . Exception Is : " + e);
    }
  }

  menuClose() {
    if (this.commonService.activeContextMenuNodeId && this.commonService.activeContextMenuNodeId != "")
      document.getElementById(this.commonService.activeContextMenuNodeId).style.removeProperty("background");
    this.commonService.activeContextMenuNodeId = "";
  }


  contextOptionClick($event) {
    try {
      this.cdref.detectChanges();
      if ($event.displayName == "Add To List" || $event.displayName == "Create New List") {
        this.commonService.righttreeContextClick = true;
        this.commonService.createnewlist();
      }
    }
    catch (e) {
    }

  }


  // AddToListEvent(nameOfList) {

  // }


  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables);
  }

  method1CallForClick() {
    if (this.activeHeader.isExpand == true) {
      this.ExpandCollapse()
    }
    else {
      this.activeHeader.isExpand = false
      this.ExpandCollapse()
    }
  }

  ExpandCollapse() {
    this.activeHeader.isExpand = !this.activeHeader.isExpand;
    this.UpdateExpandAndCollapse();
  }

  UpdateExpandAndCollapse() {
    var configdata: ITreeConfig = { "treeExpandAll": this.activeHeader.isExpand };
    this.sendDataToAngularTree(this.activeHeader.id, TreeOperations.treeConfig, configdata);
  }

}
