import { Injectable, EventEmitter, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'ClientApp/app/components/globals/CommonService';
import { EntityTypes, ITreeConfig } from '../../../globals/Model/EntityTypes';
import { MessageModel, Routing } from '../../../globals/Model/Message';
import { AngularTreeMessageModel, FABRICS, MessageKind, AngularTreeEventMessageModel, Datatypes } from '../../../globals/Model/CommonModel';
import { filter, map, takeUntil } from 'rxjs/operators';
import { TreeTypeNames, TreeOperations } from 'ClientApp/app/components/globals/Model/CommonModel';

import { UsersStateQuery } from '../../../../../webWorker/app-workers/commonstore/user/user.state.query';
import { UserStateConstantsKeys } from '../../../../../webWorker/app-workers/commonstore/user/user.state.model';
import { StateRecords } from '../../../common/state/common.state';
import { WORKER_TOPIC } from '../../../../../webWorker/app-workers/shared/worker-topic.constants';
import { EntityCommonStoreQuery } from '../../../common/state/entity.state.query';
import { FabricRouter } from '../../../globals/Model/FabricRouter';
import { EntityFilterStoreQuery } from '../../../common/state/EntityFilter/entityfilter.query';
import { MatDialog } from '@angular/material/dialog';
import { AppConfig } from '../../../globals/services/app.config';
import { getDataFromJson, getNode, getTableNames, replaceAll, isNullOrEmptyString, generatePrimaryTableQuery } from 'ClientApp/app/components/globals/helper.functions';
import { FormOperationTypes, FORMS, ROUTE, READ_DATA, UserSettings, UserSettingsSync, READ_TABLE_DATA, FilterTable, FILTER, SQLiteCommonColumns, IMAGES, Forms, Route, SCHEMA_PAGE, AppConsts, SCHEMA, ApprovalsSSRSDoc, ReviewAndApproval, STATE_FILTER } from 'ClientApp/app/components/common/Constants/AppConsts';
import { combineLatest, forkJoin, Observable, of, Subject } from 'rxjs';
import { TreeNode } from '@circlon/angular-tree-component/lib/defs/api';

@Injectable()
export class ConstructionService implements OnDestroy {
  IsEntityMgmtDrag;
  EntityType;
  EntityId;
  ContextMenuData: any;
  routeParam: any = {};
  turnoverdata: any = [];
  public onTreeEventChanges$ = new Subject<any>();
  public constructionCommonObservable$: Subject<any> = new Subject<any>();
  takeUntilDestroyObservables$ = new Subject();
  public sendDataToComponent$ = new Subject<any>();
  public readturnover = new Subject<any>();
  public readConstructionAdmin$ = new Subject<any>();

  constructor(private route: ActivatedRoute, private commonService: CommonService, public usersStateQuery: UsersStateQuery, public stateRecords: StateRecords, public entityCommonStoreQuery: EntityCommonStoreQuery, public router: Router, public entityFilterStoreQuery: EntityFilterStoreQuery, public dialog: MatDialog, public appConfig: AppConfig) {
    this.workerInit();
    this.initializeTreeWorkersObservables();
    this.ReadDefaultTreeData();
  }

  workerInit(): void {

    //tree messages
    this.commonService.sendDataFromAngularTreeToFabric
      .pipe(filter(message => this.commonService.lastOpenedFabric && this.commonService.lastOpenedFabric.toLocaleUpperCase() == FABRICS.CONSTRUCTION.toLocaleUpperCase()))
      .subscribe((msg: AngularTreeMessageModel) => {
        try {
          this.GetEventFromTree(msg);
        }
        catch (e) {
          console.error('Exception in sendDataFromAngularTreeToFabric subscriber of ConstructionService at time ' + new Date().toString() + '. Exception is : ' + e);
        }
      });

    //socket messages
    this.commonService.worker_WS$
      .filter(data => data && data.Fabric && data.Fabric.toLowerCase() == FABRICS.CONSTRUCTION.toLowerCase())
      .subscribe((response) => {
        try {
          this.recieveMessageFromMessagingService(response);
        }
        catch (e) {
          console.log(e);
        }
      });

    //from services and components messages
    this.commonService.sendMessagesToFabricServices
      .pipe(filter((message: any) => message.Fabric.toUpperCase() == FABRICS.CONSTRUCTION.toUpperCase() || this.debug(message)))
      .subscribe(async (message: MessageModel) => {
        this.recieveMessageFromMessagingService(message);
      });

    this.readConstructionAdmin$
      .pipe(filter((message: any) => message.id == "constructionadmin"))
      .subscribe(async (message) => {
        if (this.ContextMenuData && this.ContextMenuData.EntityType == EntityTypes.Project)
          this.router.navigate([FabricRouter.CONSTRUCTION_FABRIC, 'constructionadmin', { EntityType: "constructionadmin", "Expand": false, parentId: 0, operationType: "update", schema: this.ContextMenuData.EntityType }], { queryParams: { 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable, 'expand': false, entityName: this.ContextMenuData.EntityName, entityType: this.ContextMenuData.EntityType, operationType: "update", schema: this.ContextMenuData.EntityType } });
      });

    // this.route.queryParams
    //   .pipe(this.compUntilDestroyed())
    //   .subscribe((params: any) => {
    //     const tempTab = params.tab;
    //     if (Object.keys(this.routeParam).length <= 0 || (this.routeParam.tab != tempTab) || params.leftnav != this.routeParam.leftnav) {
    //       if (tempTab) {
    //         this.routeParam = JSON.parse(JSON.stringify(params));
    //         //this.commonService.updateViewedFabric(this.commonService.getFabricNameByTab(params.tab));
    //         // const tab = params.tab;
    //         this.ReadTreeData(tempTab);
    //       }
    //     }
    //     this.routeParam = JSON.parse(JSON.stringify(params));
    //   });
  }

  recieveMessageFromMessagingService(message: MessageModel) {
    try {
      if (message != null && (message.Fabric.toLowerCase() == FABRICS.CONSTRUCTION.toLowerCase() || this.debug(message))) {
        console.log("Response People Fabric : " + message);
        if (message.DataType == "Error") {
          if (message.MessageKind == MessageKind.CREATE) {
            console.log("Error :" + message.Payload);
          }
        }
        else if (message.MessageKind.toUpperCase() == MessageKind.READ) {
          this.constructionCommonObservable$.next(message);
        }
        else if ([MessageKind.CREATE, MessageKind.UPDATE, MessageKind.DELETE, MessageKind.PARTIALUPDATE].includes(message.MessageKind.toUpperCase()) && message.Payload.toLowerCase() != 'success') {
          this.constructionCommonObservable$.next(message);
        }
      }

      if (message.DataType != 'Error') {
        setTimeout(() => {
          this.commonService.loadingBarAndSnackbarStatus("complete", "");
        }, 2000);
      }
      else {
        this.commonService.loadingBarAndSnackbarStatus("complete", message.DataType);
        this.commonService.errorDetails = message.Payload;
        console.error(message);
      }
    }
    catch (e) {
      console.log("Exception : " + e);
    }
  }

  initializeTreeWorkersObservables() {
    try {
      this.commonService.initializeTreeWorkersObservablesConstruction();
    }
    catch (e) {
      console.error(e);
    }
  }

  ReadTreeData(treeName) {
    switch (treeName) {
      case TreeTypeNames.TaggedItems:
      case TreeTypeNames.PhysicalInstances:
      case TreeTypeNames.Materials:
      case TreeTypeNames.Documents:
      case TreeTypeNames.Turnover:
        this.CheckLeftTreeDatainState(Datatypes.CONSTRUCTION, treeName.replace(/ /g, '') + 'Entities', FABRICS.CONSTRUCTION, '', '', FABRICS.CONSTRUCTION, treeName);
        break;

      case TreeTypeNames.Schedule:
        this.CheckLeftTreeDatainState(Datatypes.CONSTRUCTION, TreeTypeNames.Construction + 'Entities', FABRICS.CONSTRUCTION, '', '', FABRICS.CONSTRUCTION, TreeTypeNames.Schedule);
        break;

      default:
        break;
    }
  }

  ReadDefaultTreeData() {
    this.CheckLeftTreeDatainState(Datatypes.CONSTRUCTION, TreeTypeNames.Schedule + 'Entities', FABRICS.CONSTRUCTION, '', '', FABRICS.CONSTRUCTION, TreeTypeNames.Schedule);
    this.CheckLeftTreeDatainState(Datatypes.CONSTRUCTION, TreeTypeNames.Approvals + 'Entities', FABRICS.CONSTRUCTION, '', '', FABRICS.CONSTRUCTION, TreeTypeNames.Approvals);
    this.CheckLeftTreeDatainState(Datatypes.CONSTRUCTION, TreeTypeNames.TaggedItems.replace(/ /g, '') + 'Entities', FABRICS.CONSTRUCTION, '', '', FABRICS.CONSTRUCTION, TreeTypeNames.TaggedItems);
    this.CheckLeftTreeDatainState(Datatypes.CONSTRUCTION, TreeTypeNames.PhysicalInstances.replace(/ /g, '') + 'Entities', FABRICS.CONSTRUCTION, '', '', FABRICS.CONSTRUCTION, TreeTypeNames.PhysicalInstances);
    this.CheckLeftTreeDatainState(Datatypes.CONSTRUCTION, TreeTypeNames.Materials.replace(/ /g, '') + 'Entities', FABRICS.CONSTRUCTION, '', '', FABRICS.CONSTRUCTION, TreeTypeNames.Materials);
    this.CheckLeftTreeDatainState(Datatypes.CONSTRUCTION, TreeTypeNames.Documents.replace(/ /g, '') + 'Entities', FABRICS.CONSTRUCTION, '', '', FABRICS.CONSTRUCTION, TreeTypeNames.Documents);
    this.CheckLeftTreeDatainState(Datatypes.CONSTRUCTION, TreeTypeNames.Procedures.replace(/ /g, '') + 'Entities', FABRICS.CONSTRUCTION, '', '', FABRICS.CONSTRUCTION, TreeTypeNames.Procedures);
    this.CheckLeftTreeDatainState(Datatypes.CONSTRUCTION, TreeTypeNames.Directory.replace(/ /g, '') + 'Entities', FABRICS.CONSTRUCTION, '', '', FABRICS.CONSTRUCTION, TreeTypeNames.Directory);
    this.CheckLeftTreeDatainState(Datatypes.CONSTRUCTION, TreeTypeNames.Turnover.replace(/ /g, '') + 'Entities', FABRICS.CONSTRUCTION, '', '', FABRICS.CONSTRUCTION, TreeTypeNames.Turnover);
  }

  CheckLeftTreeDatainState(DataType, EntityID, EntityType, Fabric, EntityArray?, capabality?, stateStatusFabric?) {
    var stateLastModifiedUTCMS = this.usersStateQuery.getStateKey(UserStateConstantsKeys.modifiedTime);
    var isdateTime = false;
    var currentTimeUTCMS = Math.floor((new Date()).getTime() / 1000);

    if (stateLastModifiedUTCMS) {
      var windowCloseUTCMS = this.usersStateQuery.getStateKey(UserStateConstantsKeys.windowCloseTime);

      if (windowCloseUTCMS && windowCloseUTCMS > stateLastModifiedUTCMS) {//resetting store
        var closeAndModifiedTimeDifference = currentTimeUTCMS - windowCloseUTCMS;
        if (closeAndModifiedTimeDifference > 6000) {
          //clear all store except users store
          this.stateRecords.reSetStore();
          this.usersStateQuery.add(UserStateConstantsKeys.viewedFabrics, []);// Reset Viewed Fabrics
        }
        else {
          //based on datetime fetch latest entities
          isdateTime = true;
        }
      }
      else {
        //based on datetime fetch latest entities
        isdateTime = true;
      }
    }
    else {
      isdateTime = true;
    }
    let isEntitiesExist = this.commonService.getViewedFabricsAndCheckStatus(capabality);
    // if (!isEntitiesExist) { //we need toimplement logic reading entity based on last read
    this.commonService.leftTreeForConstruction(DataType, EntityArray, capabality, null, EntityType, EntityID, stateStatusFabric);
    // }
    // else if (isdateTime) {
    //   this.dateTimeBasedReadTreeBackend(currentTimeUTCMS, DataType, EntityID, EntityType, Fabric, EntityArray, capabality);
    // }
  }


  expandTree(treeNameForExpanding) {
    var configdata: ITreeConfig = {
      "treeActiveOnClick": true,
      "treeExpandAll": true
    };
    this.sendDataToAngularTree(treeNameForExpanding, TreeOperations.treeConfig, configdata);
  }



  dateTimeBasedReadTreeBackend(currentTimeUTCMS: number, DataType, EntityID, EntityType, Fabric, EntityArray?, capabality?) {
    try {
      let isSubscribe = true;
      let fabricName = "";

      if (capabality)
        fabricName = capabality;
      else if (Fabric)
        fabricName = Fabric;

      this.entityCommonStoreQuery
        .selectAll({
          filterBy: (entity: any) => entity && entity.SG && (<Array<any>>entity.SG).indexOf(fabricName) >= 0
        })
        .filter(data => data.length != 0 && isSubscribe)
        .map(data => JSON.parse(JSON.stringify(data)))
        .shareReplay(1).first().subscribe(res => {
          var date = this.commonService.maxModifiedDateTimeStateObject(res);

          var last_utc = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
            date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()) / 1000;

          if (last_utc != NaN && currentTimeUTCMS - last_utc > 60) {
            var dateTime = new Date(last_utc * 1000).toISOString().slice(0, 19).replace('T', ' ');
            this.commonService.leftTreeForConstruction(DataType, EntityArray, capabality, dateTime, EntityType, EntityID);
          }
          isSubscribe = false;
        });
    }
    catch (e) {
      this.commonService.appLogException(new Error("ConstructionService dateTimeBasedReadTreeBackend: " + e));
    }
  }

  debug(data) {
    let fabric = FABRICS.CONSTRUCTION.toLowerCase() + "_em_person_schedule";
    let msgFabric = data.Fabric.toLowerCase().toString();

    if (msgFabric == fabric.toString()) {
      return true;
    }
    return false;
  }

  saveIntoDB(event, type?) {
    try {
      let entityData = event && event.data && event.data.EntityType ? event.data[event.data.EntityType.toLowerCase()] : null;
      if (event.EntityType == ReviewAndApproval || event.data.EntityType == ReviewAndApproval) {
        entityData = JSON.parse(JSON.stringify(event.data));
        // if(accordionData){
        //   if(accordionData.status){
        //     entityData= event.data["status"];
        //   }
        //   if( accordionData.reviewers &&accordionData.reviewers.length!=0){
        //     if(accordionData.reviewers[0]["uuid"])
        //     delete accordionData.reviewers[0]["uuid"];
        //     Object.assign(entityData,accordionData.reviewers[0]);
        //   }if(accordionData.approval){
        //     Object.assign(entityData,accordionData.approval);
        //   }
        // }

      }
      if (entityData) {
        entityData["EntityType"] = event.data.EntityType;
        entityData["EntityId"] = event.data.EntityId;
        entityData["EntityName"] = event.data.EntityName;
        var messageKind = event.data.MessageKind;




        entityData = this.initializeCommonFields(entityData);

        let jsonString = JSON.stringify(entityData);
        let payload = {
          "EntityId": entityData.EntityId, "TenantName": this.commonService.tenantName,
          "TenantId": this.commonService.tenantID, "EntityType": entityData.EntityType,
          "Fabric": this.commonService.lastOpenedFabric.toUpperCase(), "Payload": jsonString, "Info": "Admin",
          ProductionDay: entityData.ProductionDay, Type: type ? type : ""
        };

        let message = this.commonService.getMessageModel(JSON.stringify(payload), Datatypes.REVIEWANDAPPROVALS, entityData.EntityId, entityData.EntityType, messageKind, Routing.AllFrontEndButOrigin, "Details", FABRICS.CONSTRUCTION);
        // if (entityData.status.approvalstatus == "Approved")
        message.Type = entityData.status.approvalstatus ? entityData.status.approvalstatus.toUpperCase() : message.Type;
        this.commonService.statusCheck(message)

        this.commonService
          .httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
          .map(res => {
            //var messageData = JSON.parse(res);
            // let data = JSON.parse(messageData["Payload"]);
            this.commonService.loadingBarAndSnackbarStatus("complete", "Entity Updated...");
            setTimeout(() => {
              this.commonService.loadingBarAndSnackbarStatus("", "");
            }, 1000);
            return res;
          }).subscribe(data => {
            if (data) {
              console.log("data");
            }
          });

      }
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in SaveIntoDb() of ConstructionService in EntityMgmt Construction at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  initializeCommonFields(entityData) {
    entityData.TenantName = this.commonService.tenantName;
    entityData.TenantId = this.commonService.tenantID;
    entityData.ModifiedBy = this.commonService.CurrentUserEntityName;
    entityData.ModifiedDateTime = new Date().toISOString().slice(0, 19).replace("T", " ");
    entityData.SyncDateTime = new Date().toISOString().slice(0, 19).replace("T", " ");

    if (!entityData.CreatedDateTime)
      entityData.CreatedDateTime = entityData.ModifiedDateTime;
    if (!entityData.CreatedBy) {
      entityData.CreatedBy = this.commonService.currentUserName;
    }
    if (!entityData.ModifiedBy) {
      entityData.ModifiedBy = this.commonService.currentUserName;
    }
    return entityData;
  }


  getAngularTreeEvent(treeName: Array<string>, treeOperationType: string, payload?): EventEmitter<any> {
    let eventModel: AngularTreeEventMessageModel = {
      fabric: FABRICS.CONSTRUCTION,
      treeName: treeName,
      treeOperationType: treeOperationType,
      treePayload: payload ? payload : '',
      Event: new EventEmitter()
    };
    setTimeout(() => {
      this.commonService.getAngularTreeEvent$.next(eventModel);
    });
    return eventModel.Event;
  }

  activateEntityWhenSearchActive(params) {
    this.commonService.searchActive = false;
    var configdata: ITreeConfig = { "treeExpandAll": false };
    this.sendDataToAngularTree(params.tab, TreeOperations.treeConfig, configdata);
  }

  sendDataToAngularTree(treeName: string[], treeOperationType: string, payload: any, treeConfig?) {
    var treeMessage: AngularTreeMessageModel = {
      fabric: FABRICS.CONSTRUCTION.toLocaleUpperCase(),
      treeName: treeName,
      treeOperationType: treeOperationType,
      treePayload: payload,
      treeConfig: treeConfig
    };
    this.commonService.sendDataToAngularTree.next(treeMessage);
  }

  TreeOnFirstTimeLoad(treename: string, tree: object) {
    try {
      var jsonObject = { TreeType: treename, MessageData: tree };
      this.onTreeEventChanges$.next(jsonObject);
    }
    catch (e) {
      console.error('Exception in TreeOnFirstTimeLoad() of EntityMgt in  EntityMgt/service at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  GetEventFromTree(data: AngularTreeMessageModel) {
    try {
      let treeName = data.treeName[0];
      if (data.treePayload.data) {
        this.ContextMenuData = data.treePayload.data;
        this.commonService.ContextMenuData = data.treePayload.data;
      }

      switch (treeName) {
        case TreeTypeNames.Schedule:
        case TreeTypeNames.TaggedItems:
        case TreeTypeNames.PhysicalInstances:
        case TreeTypeNames.Materials:
        case TreeTypeNames.Documents:
        case TreeTypeNames.Approvals:
        case TreeTypeNames.Directory:
        case TreeTypeNames.Procedures:
        case TreeTypeNames.Turnover:
          switch (data.treeOperationType) {
            case TreeOperations.FullTreeOnCreation:
              this.TreeOnFirstTimeLoad(treeName, data.treePayload);
              break;
            default:
              this.DocumentTreeOperations(data);
              break;
          }
          break;
        case TreeTypeNames.TurnoverApproveTree:
          this.ReadDataForFormOnNodeClickForTurnover(data);
          break;
        default:
          break;
      }
    }
    catch (ex) {
      console.log(ex);
    }
  }

  ActivateNode(entityId, currentTab) {
    if (currentTab && entityId) {
      this.sendDataToAngularTree([currentTab], TreeOperations.ActiveSelectedNode, entityId);
    }
  }
  ReadDataForFormOnNodeClickForTurnover(data: AngularTreeMessageModel) {
    var payloadData = data.treePayload.data;
    this.constructionCommonObservable$.next(payloadData);
  }
  taggedItemsTreeOperations(data: any) {
    switch (data.treeOperationType) {
      case TreeOperations.NodeOnClick:
        console.log(data);
        this.click(data);
        break;
    }
  }
  MaterialsTreeOperations(data: any) {
    switch (data.treeOperationType) {
      case TreeOperations.NodeOnClick:
        console.log(data);
        this.click(data);
        break;
    }
  }

  scheduleTreeOperations(data: any) {
    switch (data.treeOperationType) {
      case TreeOperations.NodeOnClick:
        console.log(data);
        this.onNodeOnClick(data.treePayload);
        break;
    }
  }
  turnoverTreeOperations(data: any) {
    switch (data.treeOperationType) {
      case TreeOperations.NodeOnClick:
        console.log(data);
        this.onNodeOnClick(data.treePayload);
        break;
    }
  }
  DocumentTreeOperations(data: any) {
    switch (data.treeOperationType) {
      case TreeOperations.NodeOnClick:
        console.log(data);
        this.onNodeOnClick(data.treePayload);
        break;
    }
  }

  PhysicalInstanceTreeOperations(data: any) {
    switch (data.treeOperationType) {
      case TreeOperations.NodeOnClick:
        console.log(data);
        this.click(data);
        break;
    }
  }

  click(treedata) {
    if (this.commonService.rightSideCapabilityList.length != 0) {
      this.commonService.rightSideCapabilityList.forEach(data => {
        if (!data["routerLinkActive"]) {
          let clickModel = this.commonService.getRightClickEventModelObject(data);
          this.commonService.rightTreeClick.next(clickModel);
        }
      });
    }

    if (treedata && treedata.treePayload && treedata.treePayload.data) {
      let expand = false;
      let formName = this.getFormName();

      if (formName == 'Settings') {
        if (this.route.queryParams && this.route.queryParams['_value']['expand'])
          expand = this.route.queryParams['_value']['expand'];
      }

      this.router.navigate([FabricRouter.CONSTRUCTION_FABRIC, 'Settings', treedata.treePayload.data.EntityId, this.commonService.treeIndexLabel], { queryParams: { 'EntityID': treedata.treePayload.data.EntityId, 'EntityName': treedata.treePayload.data.EntityName, 'schema': treedata.treePayload.data.EntityType, 'operationType': 'READ', 'expand': expand, 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
    }
  }

  onNodeOnClick(treePayload) {

    try {
      if (treePayload) {
        const entityType: string = treePayload?.data?.EntityType;
        const entityName: string = treePayload?.data?.EntityName;
        let entityId: string = treePayload?.data?.EntityId;
        const parentNode: TreeNode = treePayload?.parent;
        let parentId = '0';
        // set parent id in url if not root node
        if (parentNode && parentNode.data) {
          if (!parentNode.data.virtual) {
            parentId = parentNode.data.entityid;
            if (!parentNode.data.entityId)
              parentId = parentNode.data.EntityId;
            // if child nodes are static then pass parent node id as entityid
            if (entityType == 'fixedmenu') {
              entityId = parentId;
            }
          }
        }

        if (entityType && entityId) {
          // let payloadJson = {}
          // payloadJson['schemaName'] = SCHEMA_PAGE;
          // payloadJson['entityType'] = entityType;
          // payloadJson['entityId'] = entityId;
          // payloadJson['entityName'] = entityName;
          // this.commonService.readDataFromApiCall(payloadJson, SCHEMA_PAGE, entityId, entityType, FABRICS.CONSTRUCTION)
          //   .subscribe(response => {
          //     const schemaPage: string = response[0][SCHEMA_PAGE]
          //     if (schemaPage) {
          const currentUrl: string = this.router.url.slice(0, this.router.url.lastIndexOf(`?`));
          const queryParam = this.router.parseUrl(this.router.url).queryParams;
          const queryParam1 = JSON.parse(JSON.stringify(queryParam));
          queryParam1['operationType'] = FormOperationTypes.Update;
          queryParam1['entityType'] = entityType;
          queryParam1['schema'] = entityType;
          queryParam1['entityId'] = entityId;
          queryParam1['entityName'] = entityName;
          queryParam1['formId'] = entityId;
          this.router.navigate([FabricRouter.CONSTRUCTION_FABRIC, FORMS, ROUTE, queryParam1.tab, parentId, entityType, FormOperationTypes.Update, entityId], { queryParams: queryParam1 });
          //   }
          // });
        }
      }
    }
    catch (error) {
      this.commonService.appLogException(new Error('Exception in onNodeOnClick() :: LayoutComponent :: Exception :: ' + error));
    }

  }

  //Persistance Query service
  readData$(schemaPage: string, schemaObject: any, entityId: string, parentEntityId: string, entityType: string) {
    let tablesProps = getTableNames(schemaPage, null, schemaObject);
    let tables = tablesProps.tables;
    let formTypeTables = {};
    let arrayTypeTables = {}

    Object.keys(tables).forEach(key => {
      let properties = tables[key];
      if (properties.type == "object") {
        let tableName = properties.tableName;
        if (!formTypeTables[tableName]) {
          formTypeTables[tableName] = {}
          formTypeTables[tableName]['props'] = properties;
          formTypeTables[tableName]['fieldNames'] = {}
        }
        formTypeTables[tableName]['fieldNames'][key] = properties.fieldNames
      } else if (properties.type == "array") {
        arrayTypeTables[key] = properties;
      }
    });
    let obs = [...this.getFormTypeObservables(formTypeTables, entityId, parentEntityId, entityType), ...this.getArrayTypeObservables(arrayTypeTables, entityId, parentEntityId, entityType)]
    return obs;
  }
  getFormTypeObservables(formTypeTables, entityId, parentEntityId, entityType) {
    let observables$ = []
    for (let tableName of Object.keys(formTypeTables)) {
      let fieldNames = formTypeTables[tableName].fieldNames;
      let table = formTypeTables[tableName]['props'];
      let dataPayload = this.getJsonPayload(table.tableName, READ_DATA, null, entityId, table.filterBy, table.primary, parentEntityId, table.type);
      let apiCall = this.commonService.readDataFromApiCall(dataPayload, READ_DATA, entityId, entityType, FABRICS.CONSTRUCTION);
      observables$.push(combineLatest([of(table), apiCall, of(fieldNames)])
        .pipe(map(data => {
          const tableName: string = data[0].tableName;
          const fields: string = data[2];

          if (data[1]) {
            if (data[1].values && data[1].values.length > 0) {
              let result: any[] = [];
              for (const value of data[1].values) {
                if (value.payload) {
                  let payload = JSON.parse(value.payload);
                  payload = this.getUpdatePayloadBasedonTableName(tableName, payload, value);
                  result.push(payload);
                }
                else if (tableName == UserSettings || tableName == UserSettingsSync) {
                  result.push(value);
                }
              }
              let jsonOutput = result[0];
              let resultData = {}
              let keys = Object.keys(fields);
              for (let key of keys) {
                let fieldNames: Array<any> = fields[key]
                let result = {}
                for (let prop of Object.keys(jsonOutput)) {
                  let d = fieldNames.find(fieldName => fieldName == prop)
                  if (d != null) {
                    result[prop] = jsonOutput[d];
                  }
                }
                resultData[key] = result
              }
              return resultData;
            }
          }
          return null;
        })));
    }
    return observables$;
  }

  readDataForTable$(key, tableName, entityType, sourceInfo, type, entityId, entityName, projectid, query: string, queryParam: {}, filterData?) {

    let fieldMapping = sourceInfo["fieldMapping"];
    let primaryTable = sourceInfo["primaryTable"];

    console.log("query primary", query);
    if (!query) {
      query = generatePrimaryTableQuery(fieldMapping, primaryTable, entityId);
      if (type == "object") {
        query += ` where uuid='${entityId}'`;
      }
      else if (type == "array") {
        if (entityId) {
          query += ` where parententityid ='${entityId}'`;
        }
        else {
          query += ` where (parententityid is null or parententityid = '')`;
        }

        if (filterData?.length > 0) {
          let data = JSON.parse(filterData[0]?.payload);
          let filterQuery = data.sqllitequery;
          if (filterQuery)
            query += filterQuery;
        }
      }

    } else {
      query = replaceAll(query, 'json_extract', 'json_value');
      for (let key of Object.keys(queryParam)) {
        if (queryParam[key].toLowerCase() == "parententityid" || queryParam[key].toLowerCase() == "entityid") {
          query = replaceAll(query, key, entityId);
        }
        else if (queryParam[key].toLowerCase() == "projectid") {
          query = replaceAll(query, key, projectid);
        } else {
          query = replaceAll(query, key, queryParam[key]);

        }
      }
      let filterQuery = "";
      if (filterData?.length > 0) {
        let data = JSON.parse(filterData[0]?.payload);
        if (data.sqllitequery) filterQuery = data.sqllitequery;
      }
      if (query.indexOf(STATE_FILTER) >= 0)
        query = replaceAll(query, STATE_FILTER, filterQuery);

    }

    let tablePayload = {};
    tablePayload['key'] = key;
    tablePayload['tableName'] = tableName;
    tablePayload['sourceInfo'] = JSON.stringify(sourceInfo);
    tablePayload['type'] = type;
    tablePayload['entityId'] = entityId;
    tablePayload['query'] = query;
    tablePayload['queryParam'] = JSON.stringify(queryParam);
    tablePayload['filterData'] = JSON.stringify(filterData);
    return this.commonService.readDataFromApiCall(tablePayload, READ_TABLE_DATA, entityId, entityType, FABRICS.CONSTRUCTION, AppConsts.GETDATAFROMBACKEND).pipe(map(data => {
      let output = []
      let result = {};
      if (data && data.length) {
        output = data
      }
      result[key.toLowerCase()] = output;
      return result;
    }));;


  }


  executePrepopulateQueries(queries: Array<any>, schema: any, parentEntityId) {
    let queries$: Array<Observable<any>> = [];

    if (queries && queries.length > 0) {
      let fileteredQueries = queries.filter(query => query.prepopulate);
      for (let queryNode of fileteredQueries) {
        let data = {};
        let property = queryNode.property ? queryNode.property.toLowerCase() : null;
        let query: string = queryNode.query;
        let sheetName = queryNode.sheetName ? queryNode.sheetName.toLowerCase() : null;
        let queryParameter: string = queryNode.queryParameter;
        if (queryParameter) {
          let params = queryParameter.split(";");
          for (let param of params) {
            let fields = param.split("=");
            if (fields.length == 2) {
              if (fields[1].toLowerCase() == "parententityid") {
                //query = replaceAll(query, fields[0], parentEntityId);
              }
            }
          }
        }
        data[sheetName] = {};
        // let query$ = this.queryService.queryRecords$(query)
        //   .pipe(map((d: any) => {
        //     return d[Values];
        //   })).pipe(map(jsonData => {
        //     if (jsonData != null && jsonData.length > 0) {
        //       data[sheetName] = jsonData[0];
        //       return data;
        //     } else {
        //       return null;
        //     }
        //   }));
        // queries$.push(query$);
      }
    }
    return forkJoin(queries$);
  }

  getDefaultFilterData$(key) {
    let filterPayload = this.getJsonPayload(FilterTable, FILTER, key);
    let filterApiCall = this.commonService.readDataFromApiCall(filterPayload, FILTER, null, null, FABRICS.CONSTRUCTION);

    return filterApiCall;
  }

  dismissStausMessage() {
    this.commonService.dismissStausMessage();
  }
  getArrayTypeObservables(arrayTypeTables, entityId, parentEntityId, entityType) {
    let observables$ = []
    for (let key of Object.keys(arrayTypeTables)) {
      let table = arrayTypeTables[key];
      table["key"] = key;

      let dataPayload = this.getJsonPayload(table.tableName, READ_DATA, null, entityId, table.filterBy, table.primary, parentEntityId, table.type);
      let filterPayload = this.getJsonPayload(FilterTable, FILTER, key, entityId, table.filterBy, table.primary, parentEntityId, table.type);
      let readDataApiCall = this.commonService.readDataFromApiCall(dataPayload, READ_DATA, entityId, entityType, FABRICS.CONSTRUCTION)
      let filterApiCall = this.commonService.readDataFromApiCall(filterPayload, FILTER, entityId, entityType, FABRICS.CONSTRUCTION)

      observables$.push(combineLatest([of(table), readDataApiCall, filterApiCall])
        .pipe(map(data => {
          const tableName: string = data[0].tableName;
          const filterPayload: any = data[2];
          const key: string = data[0].key;

          if (data[1]) {
            if (data[1].values && data[1].values.length > 0) {
              let result: any[] = [];
              for (const value of data[1].values) {
                if (value.payload) {
                  let payload = JSON.parse(value.payload);
                  payload = this.getUpdatePayloadBasedonTableName(tableName, payload, value);
                  result.push(payload);
                }
              }

              let output = result;
              const finalOutput: any = {}
              finalOutput[data[0].key] = output;
              return finalOutput;
            }
          }
          return null;
        })));
    }
    return observables$;
  }

  getJsonPayload(tableName = null, dataType = null, key = null, entityId = null, filterBy = null, primary = null, parentEntityId = null, type = null) {
    let payloadJson = {};
    payloadJson['tableName'] = tableName;
    payloadJson['dataType'] = dataType;
    payloadJson['entityId'] = entityId;
    payloadJson['filterBy'] = filterBy;
    payloadJson['primary'] = primary;
    payloadJson['key'] = key;
    payloadJson['parentEntityId'] = parentEntityId;
    payloadJson['type'] = type;
    return payloadJson;
  }

  getUpdatePayloadBasedonTableName(tableName, payload, value) {
    payload[SQLiteCommonColumns.UUID] = value.uuid;
    payload[SQLiteCommonColumns.Parent] = value.parent;

    switch (tableName) {
      case IMAGES:
        payload[SQLiteCommonColumns.Thumbnail] = value.thumbnail;
        payload[SQLiteCommonColumns.Altitude] = value.altitude;
        payload[SQLiteCommonColumns.Latitude] = value.latitude;
        payload[SQLiteCommonColumns.Longitude] = value.longitude;
        break;
      default:
        break;
    }
    return payload;
  }


  //==============================================================================================
  routeUrl(event, schema, data, parentIdForChildForm: string, parentEntityId: string, wrapper: MessageModel, schemaPage, tableName) {
    let dataPointer: string = event.dataPointer;
    let node = getNode(dataPointer, schema);

    if (node != null && node.length == 1) {
      let schemaRoute = node[0].schemaRoute;
      let page = node[0].page;
      let operationType: string = node[0].operationType;

      //get breadcrumb display label
      // let dispalyLabel = breadCrumjson.schemaPage;
      // if (breadCrumjson.id && breadCrumjson.breadcrumbDisplayName) {
      //   let formField = breadCrumjson.breadcrumbDisplayName.split(":");  // Header:DailyReportNumber
      //   let field = formField[1];     // DailyReportNumber
      //   let fieldIn = formField[0];  //Header
      //   let fieldInNode = jsonPath.query(data, fieldIn.toLowerCase(), 1);
      //   let entitydata = fieldInNode ? fieldInNode.find((item: any) => item.uuid == breadCrumjson.id) : null;
      //   if (entitydata) {
      //     dispalyLabel = entitydata[field.toLowerCase()];
      //   }
      // }
      // add popup
      if (page == null && wrapper.MessageKind == MessageKind.CREATE) {
        page = event.data.name;
        operationType = FormOperationTypes.Add;
      }

      if (operationType && operationType.toLowerCase() == FormOperationTypes.Add) {
        this.routing(parentIdForChildForm, schemaPage, schemaPage, page, FormOperationTypes.Add, null, null, tableName);
        return;
      }

      let entityId = null;
      let entityName = null;
      if (event.data) {
        entityId = event.data.entityid ? event.data.entityid : event.data.uuid;
        //will take entityname nee to do schema and data model cleanup
        if (page == 'Tagged Item') {
          entityName = event.data.tag ? event.data.tag : page;
        }
        else if (page == 'Materials') {
          entityName = event.data.materialname ? event.data.materialname : page;
        }
        else if (page == ReviewAndApproval) {
          if (event.data.pagetype) {
            entityName = event.data.pagetype;
          } else if (event.data.journalnumber) {
            entityName = event.data.journalnumber;
          } else {
            entityName = page;

          }

        }
        else {
          entityName = event.data.entityname ? event.data.entityname : page;
        }
      } else if (event.widgetType = "button") {
        entityId = getDataFromJson(dataPointer, data);
        parentIdForChildForm = entityId;
      }
      if (schemaRoute) {
        if (!page) {
          // get schema name based on input parameters

            let formField = schemaRoute.split(":");
            let field = formField[1];     // type
            let fieldIn = formField[0];  //header
            page = event.data[field];
          }
        

        if (page != null) {
          this.routing(parentIdForChildForm, schemaPage, schemaPage, page, FormOperationTypes.Update, entityId, entityName, tableName);
        }
        else {
          this.commonService.appLogException(new Error('Exception in routeUrl() of ConstructionService in EntityMgmt Construction at time ' + new Date().toString() + '. Exception is : '));

        }
      }
    }
  }

  routing(parentIdForChildForm, parentPage, dispalyLabel, page, formType, entityid, entityname, tableName) {
    try {
      const currentUrl: string = this.router.url.slice(0, this.router.url.lastIndexOf(`/${Forms}`));
      //let routeData = this.commonService.getUrlfromActviatedRoute(this.route);
      // let binddata = parentIdForChildForm + ',' + dispalyLabel + ',' + parentPage;
      //let newRouteData = parentPage ? (routeData ? routeData + '#' + binddata : binddata) : routeData;
      //const currentUrl: string = this.router.url.slice(0, this.router.url.lastIndexOf(`?`));
      const queryParam = this.router.parseUrl(this.router.url).queryParams;
      const queryParam1 = JSON.parse(JSON.stringify(queryParam));
      queryParam1['operationType'] = FormOperationTypes.Update;
      queryParam1['entityType'] = page;
      queryParam1['schema'] = page;
      queryParam1['entityId'] = entityid;
      queryParam1['entityName'] = entityname;
      queryParam1['tableName'] = tableName;
      // newRouteData.EntityName='';
      if (page == ReviewAndApproval) {

        let url = this.router.createUrlTree([currentUrl, Forms, Route, queryParam1.tab, parentIdForChildForm, page, formType], { queryParams: queryParam1 }).toString();
        if (url.startsWith('/') && this.commonService.baseUrl.endsWith('/'))
          url = url.replace(url.charAt(0), "");

        let newTabUrl = this.commonService.baseUrl + url;
        window.open(newTabUrl, '_blank');
      } else {
        if (formType == FormOperationTypes.Add) {
          ///this.router.navigate([currentUrl, Forms, Route, parentIdForChildForm, page, formType], { queryParams: { 'data': newRouteData } });

          this.router.navigate([currentUrl, Forms, Route, queryParam1.tab, parentIdForChildForm, page, formType], { queryParams: queryParam1 });
        }
        else {
          this.router.navigate([currentUrl, Forms, Route, queryParam1.tab, parentIdForChildForm, page, formType, entityid], { queryParams: queryParam1 });
        }
      }

    }
    catch (ex) {
      console.log(ex);
    }

  }

  readSchema$(schemaPage, entityType, entityId, entityName) {
    let apiCall;
    let payloadJson = {}
    payloadJson['entityType'] = entityType ? entityType : null;
    payloadJson['entityId'] = entityId ? entityId : null;
    payloadJson['entityName'] = entityName ? entityName : null;
    payloadJson['schema'] = schemaPage ? schemaPage : null;
    payloadJson['dataType'] = SCHEMA;
    let readDataFromApi = this.commonService.readDataFromApiCall(payloadJson, SCHEMA, payloadJson['entityId'], payloadJson['entityType'], FABRICS.CONSTRUCTION);
    let filterJson: any = JSON.parse(JSON.stringify(payloadJson));
    filterJson['dataType'] = FILTER;
    // let readfilterDataFromApi = this.commonService.readDataFromApiCall(filterJson, SCHEMA_PAGE, entityId, entityType, FABRICS.CONSTRUCTION);
    //  return combineLatest(readDataFromApi, readfilterDataFromApi);
    return combineLatest(readDataFromApi);

    // apiCall = Observable.combineLatest([
    //   this.commonService.readDataFromApiCall(payloadJson, SCHEMA_PAGE, entityId, entityType, FABRICS.CONSTRUCTION);
    //   this.commonService.readDataFromApiCall(filterJson, SCHEMA_PAGE, entityId, entityType, FABRICS.CONSTRUCTION);    ]).first().map(([schemapage, filterpage]) => {

    //   return apiCall;
    // });
  }
  getFormName() {
    try {
      let formType = '';
      let urltree = this.router.parseUrl(this.router.url);
      if (urltree.root.children && urltree.root.children.primary && urltree.root.children.primary.segments && urltree.root.children.primary.segments.length > 0) {
        let segments = urltree.root.children.primary.segments;
        for (let index = 0; index < segments.length; index++) {
          const element = segments[index];
          if (element.path == 'Settings') {
            formType = "Settings";
            break;
          }
          else if (element.path == 'addnewentity') {
            formType = "addnewentity";
            break;
          }
        }
      }
      return formType;
    }
    catch (error) {

    }
  }

  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables$);
  }

  readData(schemaPage: string, schemaObject: any, entityId: string, parentEntityId: string) {
    let tablesProps = getTableNames(schemaPage, null, schemaObject);
    let tables = tablesProps.tables;
    let formTypeTables = {};
    let arrayTypeTables = {}

    Object.keys(tables).forEach(key => {
      let properties = tables[key];
      if (properties.type == "object") {
        let tableName = properties.tableName;
        if (!formTypeTables[tableName]) {
          formTypeTables[tableName] = {}
          formTypeTables[tableName]['props'] = properties;
          formTypeTables[tableName]['fieldNames'] = {}
        }
        formTypeTables[tableName]['fieldNames'][key] = properties.fieldNames
      } else if (properties.type == "array") {
        arrayTypeTables[key] = properties;
      }
    });
  }

  readDefaultSignData(key: string) {
    console.log("read user default signature details from backend");
    let payload = {};
    let EntityId = this.commonService.currentUserId;
    let EntityType = EntityTypes.Construction;
    let messageKind = MessageKind.READ;
    let message = this.commonService.getMessageModel(JSON.stringify(payload), Datatypes.USERSIGNATURE, EntityId, EntityType, messageKind, Routing.AllFrontEndButOrigin, "Details", FABRICS.CONSTRUCTION);

    return this.commonService.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .map(res => {
        this.commonService.loadingBarAndSnackbarStatus("complete", "Entity Updated...");
        setTimeout(() => {
          this.commonService.loadingBarAndSnackbarStatus("", "");
        }, 1000);
        return res;
      })
    //return "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAVUAAADXCAYAAACqA8JJAAAVvUlEQVR4Xu2dW8h0VRnH9SZC7YDSRWU2WlFplh0gI8wxKikyi44g5AjZRV2oRWAh+X4XQReBdsTA6q0ugrxIQ+qiwFGSEhItUOhgjZRe9EUWWIE39fw/97L17fbM7Jn97L3XWvu3YDHvzOz9rGf9nvX+Z5323ieeQIIABCAAATcCJ7pZwhAEIAABCJyAqNIIIAABCDgSQFQdYWIKAhCAAKJKG4AABCDgSABRdYSJKQhAAAKIKm0AAhCAgCMBRNURJqYgAAEIIKq0AQhAAAKOBBBVR5iYggAEIICo0gYgAAEIOBJAVB1hYgoCEIAAokobgAAEIOBIAFF1hIkpCEAAAogqbQACEICAIwFE1REmpiAAAQggqrQBCEAAAo4EEFVHmJiCAAQggKjSBiAAAQg4EkBUHWFiCgIQgACiShuAAAQg4EgAUXWEiSkIQAACiCptAAIQgIAjAUTVESamIAABCCCqtAEIQAACjgQQVUeYmIIABCCAqNIGIAABCDgSQFQdYWIKAhCAAKJKG4AABCDgSABRdYSJKQhAAAKIKm0AAhCAgCMBRNURJqYgAAEIIKq0AQhAAAKOBBBVR5iYggAEIICo0gYgAAEIOBJAVB1hYgoCEIAAokobgAAEIOBIAFF1hIkpCEAAAogqbQACEICAIwFE1REmpiAAAQggqrQBCEAAAo4EEFVHmJiCAAQggKjSBiAAAQg4EkBUHWFiCgIQgACiShuAAAQg4EgAUXWEiSkIQAACiCptAAIQgIAjAUTVESamIAABCCCqtAEIQAACjgQQVUeYmIIABCCAqNIGIAABCDgSQFQdYWIKAhCAAKJKG4AABCDgSABRdYSJKQhAAAKIKm0AAhCAgCMBRNURZqamTjW/v2b5HMu3Wb4u03rgNgSSIICoJhGG0Zx4v5V8xPLLKw/+Za8nj+YNBUOgAAKIagFB3KMKZ9s5B5YlqnF6zN6o50qCAAT2JICo7gku09OebX7/2PL5Df4ftc++a/mTmdYNtyGQBAFENYkwDOLEzEr5geXzGkr7gH12yyBeUAgECieAqBYe4Kp6ElIJqoS1nhDUabQBajkQAUR1INAjFiNBvcOyhv71dKV9cPOIvlE0BIojgKgWF9LjKrRJUC+yI5dlV5/aQWB4Aojq8MyHKhFBHYo05UAgIoColtkc5lYtzaE2DfnpoZYZc2qVCAFENZFAOLqxTlD/YWW8myG/I2lMQaCBAKJaVrOQoGpRqp4Q1LLiTG0SJoCoJhycHV3bNIf6arN1/472OBwCENiDAKK6B7QET1knqOqhqveKoCYYNFwqkwCimn9cEdT8Y0gNCiKAqOYdzJm5f5/l+io/PdS844r3GRNAVPMNnoRUi1L1a/kR1HxjiucFEEBU8w2ieqhNgsq2qXxjiucFEEBU8wzit8ztRYPrrPLnGU+8LogAoppfMA/M5esb3L7CPjvMrzp4DIGyCCCqecVTvVP1UusJQc0rjnhbMAFENZ/grts6haDmE0M8nQABRDWPIK9b6UdQ84gfXk6IAKKaR7B1xymt6sfp2/ZG0wEkCEAgIQKIakLBWOPKgX1eX5hCUNOPGx5OlACimnbg5+Ze/a5Tv7LP9Pnf03Yd7yAwTQKIarpxn5lr9UtQEdR044VnEDhGAFFNtyGoh6oeaUgPV+9X6bqMZxCAAKKaZhu40dy6KnJN1/NrSxWCmma88AoCTxFAVNNrDFrl12p/nLj8NL044REEGgkgqmk1jJm5E8+j8hiUtOKDNxDYSgBR3YposAO0wV+CKmEN6T32x62DeUBBEIBAZwKIameEbgbqG/yvMcuaWyVBAAIZEUBU0wjW1ebGDZErXH6aRlzwAgI7E0BUd0bmfsIHzeI3LJ9cWf6ivUpkSRCAQIYEENVxg6Z51Icsn1q5weWn48aD0iHQmQCi2hlhJwNahLq0snCXvV7YyRonQwACoxNAVMcLwcKKDjec1uWn9edNjecZJUMAAnsTQFT3RtfpxJmdHe9HvcjeLztZ5GQIQCAJAojqOGG434p9VVX0EXs9GMcNSoUABLwJIKreRLfbi6/rv9MOn28/hSMgAIFcCCCqw0ZKAhruj8pNUoZlT2kQGIQAojoI5mOFaPvUH6tXvecS1OHYUxIEBiOAqA6G+tglp+F2frfZ3/VnTg3nCSVBAAK9EUBUe0N7nOH6sH9m3/I4lGHYUwoEBiWAqA6De2nFhI39bJ8ahjmlQGAUAohq/9jjXiqr/f3zpgQIjEoAUe0ff3wp6plW3Kr/IikBAhAYiwCi2i95zZ1qxV+Jm6X0yxrrEEiCAKLabxgOzfzlVRH0UvtljXUIJEEAUe0vDPG+VHqp/XHGMgSSIoCo9heO+G7+PA21P85YhkBSBBDV/sLxiJl+nmVW/PtjjGUIJEcAUe0nJLpaSg/yU7rE8u39FINVCEAgNQKIaj8RWZnZF1p+2PKsnyKwmhiBF5g/L7L8HMt/sPwMy8vEfMSdAQggqv6Q414qN03x55uCxbk58SzLelrDWy2fZvnpDT+g2qOsNkCaEAFE1T/YKzNJL9Wf6xgWJZ66mbjEc1a9aldHU3rQPvyL5RdbPj06YAr/Y+qdHx0jQCmWOYWAD8l9YYWF505xjf+Q5LuXJdGUgGqkIRFd98ww3QdXT25YVflxe9WWuSAqc/v7JssvrVx6nb3e2929ZCycZJ6ca/ntlp9r+S2Wz6q8O8de9eMy6YSo+oY/PCaFuVRfrn1Yk4jqJjcSQWW9j1MQT8U0zm18+aYddEV14BF7PWhzUsLHXGy+Kb/ZcngMUJO7JdS1cxgQ1c4InzKgno0e5qekf6hDP9NYciCgYbseB76wXB+iy7yeaLu0vKuA1l37qH3w9ejDHEcsEtA3WH6t5ddY1tbAbUk9VPVUJ58QVb8mEG5CrR7Ounk3v9Kw1IaAfugkpGFIH58TRFRCquxxf9uF2QnTPypLW+m0pS71NKs4ze31HZaf1uCwpjCesKxdDT+y/JDlMK2hKZDfpF7JofxDVP1Ir8yUFqi4q78f010tSRzCkF5CGv+4aUpmaVkr8hpR6L1nkiBpb3Jc5ufs/XWehTjZijnJb72P05/tze8tf8/ybytuTkWXbwZR9YmxGmW4G9U19rd6raRhCKgnKmFQri8u6Wo2iejSsob1faV46ieUod6benWpJP3IhB+cOif9wIiPOPXNKhUevfmBqPqgvdrM3FCZ4jp/H6brrEgQJA4SCQlpnOLeqMTBY0i/rTbyR0/IrU/5jD2XOjOfwg+OWMVJU1RBQPWjs9pWSb5vTwBRbc9q05FqmGrASjD1YRpbCfOic/tQYhFSEIfQGx1aHCSk8RNyg19jXPQhX95rWYtL2u7UxCkIaZ+9dv/oZ2YRAfAJ2H8qMxpu6h+f1I1AWKlXD6veywoLTIf23ZjiIB/VQ60PpYfa+SHRVI9d5avN1f0InMIPTreIcHZrAohqa1RrD/yQfaMJfSVNAXyiu8lJWpBIxD3SACHujUoghhjStwnAshK1+Ng+59Ml4vG+2qZ50V/aMb+2/B3LqzaV4Bh/Aohqd6bxvsQrzdzN3U1OxkIQ0oXVOBYJCakENOSUgEjc9OMpn+Okq6rqn3X1O/DZNH8scVdGRLvSdjofUe0OUvsSwz9TaZckdqfz/xYkFB+zrBuRxEKqRaYgohKJFNMQgjq3ioceu1iFFPfYNe0x5tRHirFJxidEtXsodPWMeqtKL7Gs/X2k4wmsmyMNQnqYgUioDtqHKtGLU9ceamAju+qR6n1IzItm+J+EqHYP2s/MxBsrM/D8H891Qqoe1y8sX5uBkIbazOwPLUrpNSTV42rLh3s0Idlpmj+WqbC3Vr321R62OWVkAohA9wCElX9ZmjrPTWKhK80kQBKLnNIF5uwPLcc9SPWw1avcZQh+mR2v6SGdJ04hhfnjZcUmlYW4nGKUlK9TFwGPYOgemrqf5FRFNd6IX1+RlpCGedIcxWJh/n/Z8ilVfCWAh5bVQ92W4p66LgTQTa1DYli/jV7G3yOq3YOnYeG8MjP2VTTda7PdwqY5QJ2du5AGAhLOcJWcPtMNRN5neVPvdGbfrxvW64Yjn7e8tLzajpkjciWAqHaPXLyl6h4zd353k8lZ2NQbLW342rTCv2kxSr1z8VlYjnvqKW8LS66BleQQoto9mtoe9NXITAlM46Hr3OoWzyeqqmHVXr2u3OZIN0W8aYX/iJ1wUDtJ4nm55fr8aE67Gbq3fCw0EihBAMYOrURHUwAh5cp0ZhUIN3Guz42qbhrWBxFdjQ29h/JVf22ZiuseX8Ov7yWkC8v6O6RULpvtAQkm9yGQqwDsU9c+z9HlgbqRhdLLLOdyw951PS7Vo9TeaFM7mFeCGnrkGrrrs1UkpLHYSkgPLbPtqc//qkxtI6o+gXvAzJxdmWoaLvqU0t3KzEyoN/ouyxKJU2smJRZhtX6X7ULdPRvPQl1Q9WOim0vrDvga3ofE0H68GGVVMqLqE676vGoqwhoWUSQcyvW5UdW+9GH9pgjXV/j1RNR/Wz6jOiksNh3a+6VPU8FK6QQQVb8Ix1ur/mlmX2F55Wd+q6WZHaEnXUo8JaZ6bUoSirst/9zyVyznuH90K4wWB0goNUfalHRVk75Xr32qfFog5JAmAoiqX7uQiMULVnoo2octez8HXb1Niaaeh6XXTQIaajel+dFtERW/n1jW1U1xEiMJqfJqmxG+h8A6Aoiqb9vQUyZ11/WQJKi3WF5WeZfSmsRzZgaU2yT1ttTTUtlTmR/dxEU8P2P5Ksvx00LVa/9CxaoNV46BwEYCiKp/A4lvBRhb14PgdAcrDSdXVa6XHoRUz1p/5o6uSUQlnhJRZYatTwLUwtynLb++xvNRe68fQN3UmQQBNwKIqhvK4wytE1aP0jRMlShLOPUa/vawXYqNmVWkaU9pqJ+ukNIiFT88pUQ8oXogqv0FQ9uWLrasnQH7pCfspJ9a/p1l9UARz+0Ug5DOo0Mfsb+fX73vcru+7aVzBASMAKI6TDN4pxVzpuXTLKsXpayka8aVQu8zCOeyEtFhvMu7FLHUPOnCcrx5X88NO8vy2yLG2nfK/HLe8U7ee0Q1+RDh4BoCTb3SsBVKP0rxTaUZ7tOMBiOAqA6GmoIcCKhXKjHVfGjcKz209zdWvfu5veoafn3PcN8BOiZ2I4Co7saLo8chIKHUED++bFSX1EpIJaghfdb+0NVsSvp+YZnh/jgxm2ypiOpkQ59FxZuG+BrKS0iXtRpIYCW8SjdZ1jYqVvezCHNZTiKqZcWzlNpITA8sz6oKhceYhCF+XE8dE27Zx3C/lBaQcT0Q1YyDV5jrmgPVRn0JZ5gvDZeO6rOmXue8ElQdr2NZ3S+sUeRYHUQ1x6iV5bMEMfRMYzFVT/VwQ1W1WBWeIaVVfwkqw/2y2kaWtUFUswxbEU5LQDUHGq/kq7e5TUx1nq5YC4tWWpjSOSQIJEEAUU0iDJNyoklMw/7STT1TQZpZjudPF/b+1knRo7LJE0BUkw9RMQ6uE1P1MpctaikB1XBfdtgu1QIYh4xDAFEdh/uUSn2lVVYP0IuH+eqZthVTsVIPNtxQmqujptR6Mqwropph0DJwOazkS0jPi/zdVUx1ruZPg41r7G/tBCBBIFkCiGqyocnSsYV5rW1RYREpVOIe++Nay8sdahWv7jPc3wEch45LAFEdl38Jpc+sEvW7RKle4aF5B/b3aoeK1lf39WBCiTXbpXaAyKHjEUBUx2Ofe8nqjUpM57WKSEw1RF+3YX9TvWVTw30Jq+xIkBnu595SJuY/ojqxgDtUtw8xlVsSz3Dtvob7KmeXHq5D1TABge4EENXuDKdiQYtF2tLk2TMVO9nV3tNZBZLN/FNpUYXWE1EtNLDO1Yp7kbHprtub4sUort13DhrmxiGAqI7DPZdS1StV7zTeFiXfu67G1xejuopzLjzxcwIEENUJBHnPKq7rnXYdnn+kEupT7FW904Xl5Z4+choEkiOAqCYXktEdmpkH4fr62BmP3ql6vRJRJbZKjR5qHOiDAKLaB9V8bUrwwvX1cS26Ds+1ki+7EmxuJJ1v+8DzFgQQ1RaQJnCIxE6iV78SSgIood33TlBNG/m1OLWaAFOqOFECiOpEAx9Ve13vVNfpS2T3vZKJ3ilta5IEENVJhv1YpdWLjOc4A4lwRdTBnmjone4JjtPKIIColhHHXWsxtxO0GCUBjJN6pxqe37+rwer4uNfbdepgTxc4DQLjEkBUx+U/Runac3q35ZNqhWur1D7X68vMzHI8J8vK/hiRpcwkCCCqSYRhMCckfvfVeqjqnaqHudrDC/V04+dMsbK/B0ROKYsAolpWPDfVRgIoQZWwhnS7/XHJngjmdp7uKBXsdd12tacbnAaBtAggqmnFoy9vJKh3WI4vN9UQvb6Fqk35EtF4qK+LAjQPu2xzMsdAoHQCiGrpEX5yMapJUBf2+S7bpRjql99WqKEDAUTVAWLCJpoEVXOo8x19lgDHV1p1WdTasWgOh0BeBBDVvOK1i7cegirx1VzpGVXBXRa1dvGdYyGQLQFENdvQbXV8aUdcGB21Sw91VvVMw5zro/b+MsuySYIABDYQQFTLbB6HVq3Lo6q13Tca5k0PqnN1az7tXVUmQQACLQggqi0gZXbI3PzVwlRIbXuoWsG/3nK4yootUpkFHnfTIICophEHTy+WZiwM+7XdSSK7aZV/UYnprHKi66WqnnXBFgSyI4CoZheyjQ7Xe6ln2tGrhjPUG73U8oHlIKYa6qu3emtZSKgNBIYlgKgOy7vv0g6tgDCXquG7eqFxqu811XcSU4mrziVBAAIdCSCqHQEmdLoE87HIn3ov9S777oLoe8Q0oeDhSjkEENVyYqmhuzboK8WXoOr6/DdZPqv6DjEtJ+bUJEECiGqCQdnTpT/ZeadX515pr7pj1PcjW3+1vz9l+XBP+5wGAQi0IICotoCUwSHapK+bTjelB+3Dj1teZlAPXIRA9gQQ1exDeKwCTaJ6r32uW/t9yfLfyqgmtYBA+gQQ1fRj1NZDCeu5lnXP1MctP2D5aNuTOQ4CEPAhgKj6cMQKBCAAgWMEEFUaAgQgAAFHAoiqI0xMQQACEEBUaQMQgAAEHAkgqo4wMQUBCEAAUaUNQAACEHAkgKg6wsQUBCAAAUSVNgABCEDAkQCi6ggTUxCAAAQQVdoABCAAAUcCiKojTExBAAIQQFRpAxCAAAQcCSCqjjAxBQEIQABRpQ1AAAIQcCSAqDrCxBQEIAABRJU2AAEIQMCRAKLqCBNTEIAABBBV2gAEIAABRwKIqiNMTEEAAhBAVGkDEIAABBwJIKqOMDEFAQhAAFGlDUAAAhBwJICoOsLEFAQgAAFElTYAAQhAwJEAouoIE1MQgAAEEFXaAAQgAAFHAoiqI0xMQQACEEBUaQMQgAAEHAn8FxJP+PYrx0/XAAAAAElFTkSuQmCC";
  }


  ngOnDestroy(): void {
    this.takeUntilDestroyObservables$.next();
    this.takeUntilDestroyObservables$.complete();
  }

}
