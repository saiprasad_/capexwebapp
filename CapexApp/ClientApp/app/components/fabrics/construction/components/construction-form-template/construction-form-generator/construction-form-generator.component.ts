import { Component, EventEmitter, Inject, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'ClientApp/app/components/globals/CommonService';
import { MessageModel } from 'ClientApp/app/components/globals/Model/Message';
import { BehaviorSubject, of, Subject, combineLatest, Observable } from 'rxjs';
import { takeUntil, filter, debounceTime, delay, first, scan, switchMap, finalize, concatMap, map } from 'rxjs/operators';
import { MessageKind, FABRICS, Datatypes, TreeTypeNames, MessagesCount, SERVICETYPE, ReportType } from '../../../../../globals/Model/CommonModel';
import { EntityTypes } from '../../../../../globals/Model/EntityTypes';
import { ConstructionService } from '../../../services/construction.service';
import { Routing } from '../../../../../globals/Model/Message';
import { setData, getInlineDataPointer, getFormVariables, getTableName, getNode, getQuery, getQueryParam, getKeyValuePair, getDataPointer, getSearchlistBasedOnType, getLowerCase, generateUUID, isNullOrEmptyString, updateUOM } from '../../../../../globals/helper.functions';
import { UUIDGenarator } from 'visur-angular-common';
import { CLOSE, INLINE, SUBMIT, SEARCH, ROUTE, ROUTING, COPY_PREVIOUS, BACK, THEME_CHANGE, PHYSICAL, AppConsts, TREE_ACTION, ENITTY_ID, FormOperationTypes, MENU_ID, OPERATION_TYPE, PARENT_ID, SCHEMA, ApprovalsType, ApprovalsSSRSDoc, ReviewAndApproval, Approval, Reviewers, DELETEROW } from '../../../../../common/Constants/AppConsts';
import { WORKER_TOPIC } from 'ClientApp/webWorker/app-workers/shared/worker-topic.constants';
import { FilterDialogComponent } from 'ClientApp/app/components/common/filter-dialog/filter-dialog.component';

import { JsonSchemaFormComponent } from '@visur/formgenerator-core';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { isNullOrUndefined } from 'util';
import { testData } from '../../../../../temp';
import { saveAs } from "file-saver";
import { ApplicationState } from 'ClientApp/app/components/common/Constants/application.state';

const defaultOptions: any = {
  addSubmit: false, // Add a submit button if layout does not have one
  debug: false, // Don't show inline debugging information
  loadExternalAssets: true, // Load external css and JavaScript for frameworks
  returnEmptyFields: false, // Don't return values for empty input fields
  setSchemaDefaults: true, // Always use schema defaults for empty fields
  defautWidgetOptions: { feedback: true }, // Show inline feedback icons,
  framework: 'material-design',
  selectedLanguage: 'en',
  navigateBackOnSubmit: true,
  ngSelectDefaultSearchable: false,
  timeFormat: 24,
  dataTableHeaderHeight: 40,
  dataTableRowHeight: 40,
  togglePosition: 'before',
  formReadonly: false
};

@Component({
  selector: 'app-construction-form-generator',
  templateUrl: './construction-form-generator.component.html',
  styleUrls: ['./construction-form-generator.component.scss']
})
export class ConstructionFormGeneratorComponent implements OnInit, OnDestroy {
  @Input() userId;
  @Input() header: any;
  @Input() layout: any;
  @Input() data: any;
  @Input() options: any;
  @Input() schema: any;
  @Input() roles: any;
  @Input() images: any;
  @Input() userRoles: any;
  @Input() operationType;
  @Input() schemaPage = null;
  @Input() tableName = null;
  @Input() nestedSchema;
  @Input() search;
  @Input() parentPage;
  @Input() readDataFromOtherSource;
  @Input() queries;
  @Input() updateData;
  @Input() isCompleted = false;
  @Input() SCHEMA_JSON: any = {};
  @Input() JSON_DATA;
  @Input() breadcrumbDisplayName;
  @Input() formulas;
  @Input() title;
  // @Input() $state;
  @Input() state: ApplicationState;

  @Input() jsonData;
  @Input() IsEntityManagementForm;
  @Input() tab;
  @Input() EntityType: string;
  @Input() EntityId;
  @Input() EntityName;
  @Input() IsEntityMgmtDrag;
  @Input() commonInputVariable;
  @Input() isNewEntity;
  @Input() LocationData;
  @Input() myForm;
  @Input() payloadData: any;
  @Input() queryParams: any = {};
  @Input() tableNameOrType;
  @Output() applicationEvent = new EventEmitter();
  @ViewChild(JsonSchemaFormComponent) formGenerator: JsonSchemaFormComponent;
  @Output() commonOutputEmitter = new EventEmitter();

  takeUntilDestroyObservables = new Subject();
  popupHeader: any;
  popupFormType: string;
  livedata: any;
  @Input()
  set expanded(value) {
    this.setExpandCollapse(value);
  }
  parentEntityId;
  projectId = null

  isPopupForm = false;
  /// assgin value to menuid. if the page is root/first page/initial page
  menuId;
  previousRoute: string;
  headerControl = 'headerSelect';
  dialogRef: MatDialogRef<any, any>;
  //adminSchema;
  childRouteParameters: any;
  wrapper: MessageModel = new MessageModel();
  schemaData: any;
  initializingData = false;
  isFilterActive = false;
  activeFilterName: string;
  filterQyery: string;
  isInitilize = false;
  turnoverTreeData: any;
  constructor(public constructionService: ConstructionService, private route: ActivatedRoute, private commonService: CommonService, public dialog: MatDialog, public router: Router) {
    // this.commonService.triggerSubmitOnFormGenerator$
    //   .pipe(this.compUntilDestroyed())
    //   .subscribe((res) => {
    //     if (!this.isCompleted) {
    //       this.formGenerator.submitForm();
    //     }
    //   });

    // this.constructionService.constructionCommonObservable$
    //   .pipe(filter((data: any) => data.MessageKind == MessageKind.READ && this.EntityId == data.EntityID))
    //   .pipe(this.compUntilDestroyed())
    //   .subscribe((message: MessageModel) => {
    //     this.landingPageSchemaSubscription(message);
    //   });

    //   this.constructionService.sendDataToComponent$
    //   .pipe(filter((data: any) => data.id == 'searchinput'))
    //   .pipe(this.compUntilDestroyed())
    //   .subscribe((message:any) => {
    //     let entityType = this.getEntityType(this.EntityType);
    //     let searchlist = this.search ? this.search : getSearchlistBasedOnType(entityType);
    //     let searchmodel = [];
    //     searchlist.forEach(element =>{
    //       let property = element.split(':');
    //       searchmodel.push({ SearchValue : (message.value ? message.value : null),SearchField : property[1],AccordianName:property[0]})
    //     });
    //     //let schemaPage = (this.tableName != null && this.tableName != '') ? this.tableName : this.schemaPage;
    //     //schemaPage = getTableName(schemaPage);
    //     //let entityid = this.EntityId;
    //     //let observables = this.constructionService.readData(schemaPage, this.schema, entityid, this.parentEntityId)
    //     this.readDataFromBackend(message,Datatypes.SEARCHDATA,searchmodel );
    //   });

    //   this.constructionService.sendDataToComponent$
    //   .pipe(filter((data: any) => data.id == 'ngselect'))
    //   .pipe(this.compUntilDestroyed())
    //   .subscribe((message:any) => {
    //     let entityType = this.getEntityType(this.EntityType);
    //     console.log("Ng select");
    //     this.readSchema(message.value,this.tab);
    //   });


    this.constructionService.sendDataToComponent$
      .pipe(filter((data: any) => data.id == 'searchinput'))
      .pipe(this.compUntilDestroyed())
      .subscribe((message: any) => {
        let entityType = this.getEntityType(this.EntityType);
        let searchlist = this.search ? this.search : getSearchlistBasedOnType(entityType);
        let searchmodel = [];
        searchlist.forEach(element => {
          let property = element.split(':');
          let searchObj = {};
          searchObj['SearchValue'] = message.value ? message.value : null;
          searchObj['SearchField'] = property[1];
          searchObj['AccordianName'] = property[0];
          if (this.filterQyery && !searchObj['SearchValue']) {
            searchObj['FilterQuery'] = this.filterQyery;
          }
          searchmodel.push(searchObj);
        });
        this.readDataFromBackend(message, Datatypes.SEARCHDATA, searchmodel);
      });

    this.constructionService.sendDataToComponent$
      .pipe(filter((data: any) => data.id == 'filter'))
      .pipe(this.compUntilDestroyed())
      .subscribe((message: any) => {
        //will take datapointer from form
        let entityType = getLowerCase(this.getEntityType(this.EntityType));
        let datapointer = "/" + entityType + "panel" + "/" + entityType;
        if (this.isFilterActive) {
          this.activateDeactivateFilter({ dataPointer: datapointer }, null, null, null, "200px", null);
        }
        else {
          this.filterOperation({ dataPointer: datapointer });
        }
      });

    this.constructionService.sendDataToComponent$
      .pipe(filter((data: any) => data.id == 'ngselect'))
      .pipe(this.compUntilDestroyed())
      .subscribe((message: any) => {
        let entityType = this.getEntityType(this.EntityType);
        console.log("Ng select");
        //this.readSchema(message.value,this.tab);
        //this.livedata=null;
        //this.updateData=null;
        this.initializeSchema(message.value);
      });

    this.constructionService.sendDataToComponent$
      .pipe(filter((data: any) => data.id == 'save'))
      .pipe(this.compUntilDestroyed())
      .subscribe((message: any) => {
        let entityType = this.getEntityType(this.EntityType);
        if (this.formGenerator)
          this.formGenerator.submitForm();

      });

    this.constructionService.readturnover
      .pipe(filter((data: any) => data.id == 'button'))
      .pipe(this.compUntilDestroyed())
      .subscribe((message: any) => {
        let turnover: any = [];
        this.constructionService.turnoverdata.forEach(element => {
          let data = { EntityName: element.entityname ? element.entityname : element.EntityName, reporttype: element.reporttype, taggeditem: element.taggeditem, createddate: element.createddate, lastmodifiedby: element.lastmodifiedby };
          turnover.push(data);
        });
        this.constructionService.turnoverdata = turnover
        this.readTurnoverData(message, Datatypes.REPORTDATA, this.constructionService.turnoverdata);
      });
    this.constructionService.constructionCommonObservable$
      .pipe(filter((data: any) => data.EntityType == 'TurnoverApprove'))
      .pipe(this.compUntilDestroyed())
      .subscribe((message: any) => {
        this.turnoverTreeData = message;
        this.initializeSchema();
      });
  }

  ngOnInit(): void {

    this.route.queryParams
      .filter(d => this.commonService.getFabricNameByUrl(this.router.url) == FABRICS.CONSTRUCTION)
      .pipe(this.compUntilDestroyed())
      .subscribe((queryParams) => {
        try {
          let queryParameters = JSON.parse(JSON.stringify(queryParams));
          this.combineParamAndQueryParam(queryParameters);
          if (queryParameters) {
            this.EntityType = queryParameters.entityType;
            this.EntityId = queryParameters.entityId;
            this.EntityName = queryParameters.entityName;
            this.tab = queryParameters.tab;
            this.tableNameOrType = queryParameters.tableName;
          }
        }
        catch (e) {
          this.commonService.appLogException(new Error('Exception inroute.queryParams subscriber of ngOnInit of ConstructionFormGeneratorComponent at time ' + new Date().toString() + '. Exception is : ' + e));
        }
      });
    // this.constructionService.constructionCommonObservable$
    //   .pipe(filter((data: any) => this.data.treeOperationType == "NodeOnClick"))
    //   .pipe(this.compUntilDestroyed())
    //   .subscribe((message: any) => {
    //     let turnover: any = [];
    //   });
    this.route.params
      .filter(d => this.commonService.getFabricNameByUrl(this.router.url) == FABRICS.CONSTRUCTION)
      .pipe(this.compUntilDestroyed())
      .subscribe((params) => {
        try {
          this.isInitilize = false;
          this.childRouteParameters = JSON.parse(JSON.stringify(params));
          this.combineParamAndQueryParam(this.childRouteParameters);
          if (((this.tab != TreeTypeNames.Approvals) || (this.tab == TreeTypeNames.Approvals && (this.EntityType == ReviewAndApproval))) && (this.tab != "Turnover" || this.childRouteParameters.EntityType == "constructionadmin")) {
            this.initializeSchema();
          }
        }
        catch (e) {
          this.commonService.appLogException(new Error('Exception inroute.queryParams subscriber of ngOnInit of ConstructionFormGeneratorComponent at time ' + new Date().toString() + '. Exception is : ' + e));
        }
      });

    //this.readSchema();

    // if(this.tab!=TreeTypeNames.Approvals){
    //   this.initializeSchema();
    // }
    // if(this.tab==TreeTypeNames.Approvals && (this.EntityType==ReviewAndApproval)){

    //    this.initializeSchema();
    // }
    // this.initializeApplicationEvents();
  }

  combineParamAndQueryParam(data) {
    Object.keys(data).forEach(key => {
      this.queryParams[key.toLowerCase()] = data[key];
    });
  }

  loadData() {
    let schemaPage = (this.tableName != null && this.tableName != '') ? this.tableName : this.schemaPage;
    schemaPage = getTableName(schemaPage);
    let observables$ = [];
    if (this.wrapper.MessageKind != MessageKind.CREATE) {
      let entityid = this.wrapper.EntityID;
      observables$ = observables$.concat(this.constructionService.readData$(schemaPage, this.schema, entityid, this.parentEntityId, this.EntityType));
    }
    else {
      // set entityid if create operation
      this.wrapper.EntityID = this.commonService.GetNewUUID();
      this.executePropulateQueries();
      this.constructionService.dismissStausMessage();
    }

    if (this.readDataFromOtherSource) {
      observables$ = observables$.concat(this.loadDataFromOtherSource(schemaPage, this.readDataFromOtherSource, false));
    }
    let messagesSource$ = null;
    if (this.wrapper.MessageKind != MessageKind.CREATE)
      messagesSource$ = this.dataLoadStatusChecker(observables$.length);
    else {
      this.resetInitialization();
    }
    for (let obs of observables$) {
      obs.pipe(untilDestroyed(this))
        .subscribe(data => {
          this.setData(data);
          if (messagesSource$) messagesSource$.next(new MessagesCount(observables$.length, 1));
        });
    }

  }
  dataLoadStatusChecker(length: number) {
    let messagesSource$ = new BehaviorSubject<MessagesCount>(new MessagesCount(length, 0));
    let messages$ = messagesSource$.asObservable()
      .pipe(scan((acc, curr) => new MessagesCount(acc.totalCount, acc.curentCount + curr.curentCount)));
    console.log(`${this.schemaPage} Data load starts at:  ${new Date().toUTCString()}`);
    messages$.pipe(untilDestroyed(this))
      .subscribe(d => {
        if (d.totalCount > 0) {
          console.log(`${this.schemaPage} Data loading at:  ${new Date().toUTCString()}`);
          if (d.totalCount == d.curentCount) {
            console.log(`${this.schemaPage} Data loading completed at:  ${new Date().toUTCString()}`);
            setTimeout(() => this.resetInitialization(), 2000);
            //this.updateRecentActivity(this.wrapper.MessageKind, this.livedata);
          }
        }
      });
    return messagesSource$;
  }
  loadDataFromOtherSource(schemaPage: string, otherSources: any, filter = true) {
    let entityid = this.EntityId;
    let entityName = this.EntityName;
    let keys = Object.keys(otherSources);
    let obs$ = [];
    for (let key of keys) {
      let sourceInfo = otherSources[key];
      let dataPointer: string = getDataPointer(key, this.schema);
      let node = getNode(dataPointer, this.schema);

      if (node && node.length == 1) {
        let type = node[0].type;
        let query = getQuery(this.queries, node[0].query);
        let queryParameter = getQueryParam(this.queries, node[0].query);
        let queryParam = {};//getKeyValuePair(this.queryParams, null);
        if (queryParameter) {
          let params = getKeyValuePair(queryParameter, null);
          for (let key of Object.keys(params)) {
            queryParam[key] = params[key];
          }
        }
        if (filter) {
          obs$.push(this.constructionService.getDefaultFilterData$(key).pipe(map(d => d.values)).pipe(concatMap((response: any) => {
            console.log(`${this.schemaPage} Data loading filter ends and read data ${key} starts at:  ${new Date().toUTCString()}`);
            return this.constructionService.readDataForTable$(key, schemaPage, this.getEntityType(this.EntityType), sourceInfo, type, entityid, entityName, this.queryParams['formid'], query, queryParam, response)
          })))
        } else {
          obs$.push(this.constructionService.readDataForTable$(key, schemaPage, this.getEntityType(this.EntityType), sourceInfo, type, entityid, entityName, this.queryParams['formid'], query, queryParam));
        }
      }
    }
    return obs$;
  }

  executePropulateQueries() {
    this.constructionService.executePrepopulateQueries(this.queries, this.schema, this.parentEntityId).subscribe(d => {
      let data = {};
      if (d && d instanceof Array) {
        for (let d1 of d) {
          if (!isNullOrUndefined(d1) && d1 instanceof Object) {
            let keys = Object.keys(d1);
            for (let key of keys) {
              data[key] = d1[key];
            }
          }
        }
      } else if (d && Object.keys(d).length > 0) {
        data = d;
      }
      this.setData(data);
    });
  }
  resetInitialization() {
    this.initializingData = false;
  }
  readPDFDocument() {
    let payload: any = {};
    payload.ReportName = this.getTableNameOrType(this.tableNameOrType) + '-' + this.EntityName;
    payload.UUID = this.EntityId;
    payload.enittyName = null;
    payload.createdBy = null;
    payload.ReportType = this.getTableNameOrType(this.tableNameOrType);
    payload.ReportSubType = '';
    payload.StartDate = null;
    payload.EndDate = null;


    let message = this.commonService.getMessageModel(
      JSON.stringify(payload),
      Datatypes.PDFDOC, this.EntityId,
      Datatypes.PDFDOC, MessageKind.READ, Routing.OriginSession,
      ReportType.SINGLE,
      FABRICS.CONSTRUCTION);
    this.commonService
      .httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .map(res => {
        var messageData = JSON.parse(res);
        let data = messageData.EntityType != AppConsts.ERROR ? this.isPayloadValid(messageData) : {};
        return data;
      }).subscribe(data => {
        if (data) {
          this.commonOutputEmitter.emit({ id: 'pdfBase64Src', value: data.PDFBase64Data });
        }
      });
  }

  isPayloadValid(message) {
    try {
      return JSON.parse(message["Payload"]);
    }
    catch (e) {
      return {};
    }
  }

  getTableNameOrType(Type) {
    let tableName = "";
    switch (Type) {
      case EntityTypes.Exhibits:
        tableName = "exhibit";
        break;
      case EntityTypes.Journals:
        tableName = "dailyjournal";
        break;
      default:
        tableName = Type.toLowerCase();
        break;
    }
    return tableName;
  }
  initializeSchema(defaultOption?) {
    //this.formService.presentStatusMessageAsync(LOADING);
    //this.isDarkTheme = this.formService.getTheme();
    // make form read only
    defaultOptions.formReadonly = AppConsts.FromReadOnly[this.route.snapshot.queryParams.tab] === true;
    this.options = { ...defaultOptions };
    // let schema$ = !this.getSchemaFromInput$() ? this.getSchemaFromUrl$() : this.getSchemaFromInput$();

    let schema$ = this.getSchemaFromUrl$(defaultOption);

    this.route.queryParams.pipe(untilDestroyed(this), debounceTime(200), delay(500))
      .subscribe(res => {
        this.triggerTableSizeRecalculation();
      });

    schema$.pipe(untilDestroyed(this))
      .subscribe((schemaData: any) => {
        if (schemaData && schemaData.length > 0) {
          if (schemaData[0][0].hasOwnProperty('Schema')) {
            this.schemaData = JSON.parse(schemaData[0][0].Schema);
          }
          else if (schemaData[0][0].hasOwnProperty('schema')) {
            this.schemaData = JSON.parse(schemaData[0][0].schema);
          }
          // if(schemaData[0].hasOwnProperty('data')){
          //   this.data = JSON.parse(JSON.stringify( schemaData[0].data))
          // }
          // else if(schemaData[0].hasOwnProperty('Data')){
          // this.data = JSON.parse(JSON.stringify( schemaData[0].Data))
          // }
          let d = getFormVariables(this.schemaData, this.options);
          for (let key of Object.keys(d)) {
            this[key] = d[key];
          }
          //let uom = this.schemaData.unitsOfMeasurement;
          this.readData(defaultOption);

          if (!this.projectId) {
            this.projectId = this.route.snapshot.queryParams.formId;
          }
          this.commonService.sendDataToAngularTree1(this.childRouteParameters.tab,'ActiveSelectedNode',this.projectId);
          this.getUIState(this.projectId).subscribe(state => {
            this.state = state;
            if (this.state.projectUOM && this.schema) {
              this.schema = updateUOM(this.schema, this.state.projectUOM.toLowerCase());
            }
            this.setData(this.data);

          });

          this.isInitilize = true;

          // if (uom && uom != null && uom != "") {
          //   this.metric = this.formService.getUOMValue(uom);
          // }
          // this.state = this.getUIState();
          // if (schemaData.isPopup) {
          //   let d = getPopupFormVariables(schemaData);
          //   for (let key of Object.keys(d)) {
          //     this[key] = d[key];
          //   }

          //   if (this.operationType) {
          //     this.wrapper.MessageKind = this.operationType.toLowerCase() == FormOperationTypes.Add ? MessageKinds.CREATE : MessageKinds.UPDATE;
          //   }
          // }
          // this.initializingData = true;
          // this.loadData();
          // this.previousRoute = this.formService.getPreviousRoute();
        } else {
          // let err = new Error();
          // err.name = INVALID_SCHEMA;
          // err.message = INVALID_PAGE(this.schemaPage);
          // let message = { "type": ERR, "message": err }
          // this.logFormGeneratorEvents(message) //3480
        }
      });

  }

  getUIState(projectId) {
    return this.commonService.getUIProjectState(projectId);
  }

  // getSchemaFromInput$() {
  //   let isPopupForm = false;
  //   if (this.dialogData instanceof Array)
  //     isPopupForm = this.dialogData.length > 0;
  //   else if (this.dialogData instanceof Object)
  //     isPopupForm = this.dialogData && Object.keys(this.dialogData).length > 0;
  //   // initializing the layout/options after the popupformgenerator initialized
  //   return isPopupForm ? of(this.dialogData).pipe(delay(100)) : null;
  // }
  getSchemaFromUrl$(defaultOption?) {
    // schema initialized using selector then read only data else read both schema and data
    return this.route.params.pipe(first(),
      filter((d: any) => d.schema),
      switchMap((params) => {
        // this.formService.debug('route.params :: FormGeneratorComponent :: ' + JSON.stringify(params));
        if (params[`${OPERATION_TYPE}`]) {
          this.wrapper.EntityID = params[`${ENITTY_ID}`];
          this.parentEntityId = params[`${PARENT_ID}`];
          this.parentEntityId = (this.parentEntityId == '0') ? null : this.parentEntityId;
        }
        else {
          if (!params.entityId) {
            // this.wrapper.EntityID = params[`${MENU_ID}`];/// we should not set entity value for construction rightmenu
            this.menuId = params[`${MENU_ID}`];
            this.wrapper.MessageKind = MessageKind.UPDATE;

          }
        }
        // FOR MENUS FORM UPDATE OPERATION
        if (!this.wrapper.MessageKind) {
          this.wrapper.MessageKind = params[`${OPERATION_TYPE}`] == FormOperationTypes.Add ? MessageKind.CREATE : MessageKind.UPDATE;
        }

        this.schemaPage = params[`${SCHEMA}`];
        // this.formService.readSchema$(params[`${SCHEMA}`]);
        let schemaPage = this.getSchemaNameByEntityType(params[`${SCHEMA}`], defaultOption);
        return this.constructionService.readSchema$(schemaPage, this.EntityType, this.EntityId, this.EntityName);

        // return "";
      }));
  }
  // initializeApplicationEvents() {
  //   // refresh if any data changes
  //   this.constructionService.subscribeToApplicationEvent()
  //     .pipe(filter(message => [REFRESH_DATA, UPDATEFORM_FROM_CHILDPAGE, THEME_CHANGE].indexOf(message.event) >= 0), delay(500))
  //     .pipe(untilDestroyed(this))
  //     .subscribe((res: any) => {
  //       try{
  //       const parentPointer: string = (<string>res.data.parentPage);
  //       if (parentPointer) {
  //         const parentPointers: any[] = parentPointer.split('.');
  //         const parentPage: string = parentPointers[0];

  //         switch (res.event) {
  //           case UPDATEFORM_FROM_CHILDPAGE:
  //             if (this.schemaPage == parentPage) {
  //               this.setData(res.data['data']);
  //             }
  //             break;

  //           case REFRESH_DATA:
  //             if (this.schemaPage == parentPage) {
  //               this.refreshParentTable(parentPointers);
  //             }
  //             break;
  //           case THEME_CHANGE:
  //             this.isDarkTheme = res.data
  //           default:
  //             break;
  //         }
  //       } else {
  //         this.formService.error("On refreshing parentPage is null \n" + res, new Error("NUll reference"))
  //       }
  //     }catch(e){
  //       console.error(e);
  //     }

  //     });
  // }



  // readSchema(inputEntityType?, subType?) {
  //   let payload: any = {};
  //   let entityType = inputEntityType ? inputEntityType : this.getEntityType(this.EntityType);
  //   payload.EntityId = this.EntityId;
  //   payload.EntityType = entityType;
  //   payload.DataType = EntityTypes.EntityInfo;
  //   payload.FormCapability = entityType;
  //   payload.subType = this.EntityType;
  //   payload.EntityName = this.EntityName;
  //   if (this.tab == TreeTypeNames.Approvals) {
  //     payload.FormName = this.getSchemaNameByEntityType(entityType);
  //   }
  //   else {
  //     payload.FormName = this.EntityType != EntityTypes.Project ? this.getSchemaNameByEntityType(entityType) : entityType;
  //   }

  //   let capabilityId = this.commonService.getCapabilityId(FABRICS.CONSTRUCTION);
  //   payload.CapabilityId = capabilityId ? capabilityId : "";

  //   if (this.isNewEntity || this.IsEntityMgmtDrag) {
  //     payload.isNewEntity = this.isNewEntity;
  //     payload.Info = "AddNewForm";
  //   }
  //   payload.FormType = "Configuration";
  //   //   this.expandCollapse = true;
  //   payload.Fabric = this.commonService.lastOpenedFabric;
  //   this.commonService.sendMessageToServer(JSON.stringify(payload), Datatypes.LOADFORMDATA, this.EntityId, entityType, MessageKind.READ, Routing.OriginSession, payload.Info, FABRICS.CONSTRUCTION);
  // }

  setExpandCollapse(value) {
    if (this.formGenerator) {
      let data = { "type": "expandCollapseAll", "value": value };
      this.formGenerator.setLayoutOptions(data);
    }
  }

  onChange(event) {
    this.livedata = event;
    console.log('onChange() :: FormGenerator :: event :: ', event);
  }

  isValidFn(event) {
    console.log("isValid Fn", event);
  }

  validationErrors(event) {
    console.log("In Valida Fields :");
    console.log(event);
  }

  enumValueUpdate(event) {
    //let schemaPage = getTableName(this.schemaPage);
    //this.formService.enumValueUpdate(schemaPage, event);
  }

  // landingPageSchemaSubscription(message: MessageModel) {
  //   let schemaData = JSON.parse(message.Payload ? message.Payload.toString() : "{}");
  //   this.SCHEMA_JSON = JSON.parse(schemaData.Schema ? schemaData.Schema.toString() : "{}");
  //   this.SCHEMA_JSON.data = JSON.parse(schemaData.Data ? JSON.stringify(schemaData.Data) : "{}");

  //   // this.SCHEMA_JSON.layout[0].items[0].items.forEach(item => {
  //   //   item["disabled"] = false;
  //   // });
  //   this.initializeFormGenerator();

  //   // this.route.params
  //   //   .pipe(this.compUntilDestroyed(), debounceTime(200), delay(500))
  //   //   .subscribe(d => {
  //   //     this.triggerTableSizeRecalculation();
  //   //   });
  // }

  triggerTableSizeRecalculation() {
    if (this.formGenerator)
      this.formGenerator.resizeTable();
  }

  setData(data) {
    if (data != null && Object.keys(data).length > 0) {
      data = (data && Object.keys(data).length > 0) ? this.updateDataProperties(data) : {};
      //data['extra'] = this.payloadData ? this.generateDataForExtraTable() : [];
      this.updateData = setData(data, this.livedata);
    }
    //this.changeDetectorRef.detectChanges();
  }

  updateDataProperties(data: any) {
    let tempData: any = {};
    const keys: string[] = Object.keys(data);
    for (let key of keys) {
      const innerData: any = data[key];
      key = this.getSchemaPageKey(key);
      if (Array.isArray(innerData)) {
        tempData[key] = [];
        innerData.forEach(element => {
          const innerKeys: string[] = Object.keys(element);
          let tempObjData = {};
          for (const innerKey of innerKeys) {
            if (innerKey.toLowerCase() == 'payload') {
              this.payloadData = element[innerKey] ? JSON.parse(element[innerKey]) : {};
              continue;
            }
            tempObjData[innerKey.toLowerCase()] = element[innerKey];
          }
          tempData[key].push(tempObjData);
        });
      }
      else {
        tempData[key] = {};
        const innerKeys: string[] = Object.keys(innerData);

        for (const innerKey of innerKeys) {
          if (innerKey.toLowerCase() == 'payload') {
            this.payloadData = innerData[innerKey] ? JSON.parse(innerData[innerKey]) : {};
            continue;
          }
          tempData[key][innerKey.toLowerCase()] = innerData[innerKey];
        }
      }
    }
    return tempData;
  }

  getSchemaPageKey(key: string) {
    switch (key) {
      case EntityTypes.PhysicalInstances:
        return PHYSICAL;

      default:
        return key.toLowerCase();
    }
  }

  generateDataForExtraTable(payloadData?) {
    payloadData = payloadData ? payloadData : this.payloadData;
    const tableReords: any[] = [];
    if (payloadData && Object.keys(payloadData).length > 0) {
      const keys: string[] = Object.keys(payloadData);
      for (const key of keys) {
        const data: any = {};
        data['label'] = key;
        data['value'] = payloadData[key];
        data['uuid'] = UUIDGenarator.generateUUID();

        tableReords.push(data);
      }
    }
    return tableReords;
  }

  lookUpQuery(event) {
    let filters = event.filters;
    let fields = event.fields;
    let displayLabel = event.displayLabel;
    let label = {};

    if (fields && Array.isArray(fields) && fields.length > 0) {
      for (let field of fields) {
        let formFields = (<string>field).split(":"); //companies:entityname
        if (formFields && formFields.length == 2) {
          let column = formFields[1]; //entityname
          let tableName = formFields[0];//companies
          label[tableName] = column;
          // this.peopleService.bindPeopleDropDowns(event,column);
        }
        else {
        }
      }
    }
  }



  getDefaultOptions(options: any) {
    if (options) {
      for (const key of Object.keys(options)) {
        this.options[key] = options[key];
      }
    }
    return this.options;
  }

  async click(event) {
    let buttonEvent: string = event.buttonEvent ? event.buttonEvent.toLowerCase() : null;
    let jsonPayload = {};

    switch (buttonEvent) {
      // case "PopUp":
      //   // case FilterEvent:
      //   this.commonService.presentStatusMessageAsync(Loading);
      //   let obs$ = this.formService.openPopup(this.nestedSchema, event, this.schema, this.wrapper.EntityID, this.schemaPage);
      //   obs$.subscribe(dialogRef => {
      //     this.dialogRef = dialogRef;
      //     this.dialogRef.afterClosed().pipe(
      //       finalize(() => this.dialogRef = undefined)
      //     );
      //   });
      //   break;
      case "apply-default-sign":
        this.applyDefaultSign(event);
        break;

      case CLOSE:
        this.closePopup(this.dialogRef);
        break;

      case TREE_ACTION:
        if (event.data.treeToRelation && event.data.row && event.data.row[event.data.treeToRelation]) {
          let searchmodel = [];
          searchmodel.push({ SearchValue: event.data.row[event.data.treeToRelation], SearchField: event.data.treeFromRelation, AccordianName: this.getEntityType(this.EntityType) });
          this.readDataFromBackend(event, Datatypes.LOADDATA, searchmodel);
        }
        break;


      case INLINE:
        this.addInlineRow(event);
        break;

      case SUBMIT:
        //this.formService.updateFormValueBasedOnSelectedRecord(this.search, this.livedata, this.parentPage, this.schemaPage, this.schema);
        this.closePopup(this.dialogRef);
        break;

      case SEARCH:
        jsonPayload['parentPage'] = this.parentPage;
        jsonPayload['schemaPage'] = this.schemaPage;
        jsonPayload['data'] = this.livedata;
        jsonPayload['search'] = this.search;

        // //this.formService.openSearch(event, jsonPayload).pipe(untilDestroyed(this)).subscribe((res) => {
        //   //this.updateData = res;
        // });
        break;

      // case BARCODESCAN:
      //   const node: any = getNode(event.dataPointer, this.schema);
      //   this.search = this.nestedSchema[node.page];
      //   // this.formService.updateFormValueBasedOnSelectedRecord(this.search, this.livedata, this.parentPage, this.schemaPage, this.schema);
      //   break;

      // case BARCODESEARCH:
      //   this.formService.bindBarcodeSearchData(event, this.schema, this.nestedSchema, this.schemaPage, this.livedata);
      //   break;

      case ROUTE:
      case ROUTING:
        if ((event.widgetType == "table-row" && event.data != undefined) || event.widgetType == "button") {
          // for add popup form, on routing closing the popup..
          this.closePopup(this.dialogRef);

          // send page id as menu id if page is first/inital page
          //let id = this.wrapper.EntityID ? this.wrapper.EntityID : this.menuId;
          let id = this.childRouteParameters.EntityID ? this.childRouteParameters.EntityID : "0";

          //for AddPopup page message kind will come as create
          if (this.wrapper.MessageKind == MessageKind.CREATE) {
            id = this.childRouteParameters.parentId ? this.childRouteParameters.parentId : "0";
          }
          if (event.data && this.childRouteParameters && this.childRouteParameters.tab == "Turnover") {
            let contain = false;
            if(this.constructionService.turnoverdata.length>0){
              this.constructionService.turnoverdata.forEach(element => {
                if(element.entityname == event.data.entityname)
                    contain = true;
              });
              if(!contain){
                this.constructionService.turnoverdata.push(event.data);
              }else{
                contain = false;
              }
            }else{
            this.constructionService.turnoverdata.push(event.data);
            }
          }
          // let breadCrumbjson = this.formService.getbreadcrumbPayloadJson(this.route);
          // breadCrumbjson['breadcrumbDisplayName'] = this.breadcrumbDisplayName;
          this.constructionService.routeUrl(event, this.schema, this.livedata, id, this.childRouteParameters.parentId, this.wrapper, this.childRouteParameters.schema, this.tableNameOrType);

        }
        else if(event.widgetType == "table-row-select"){
          if (event.data && this.childRouteParameters && this.childRouteParameters.tab == "Turnover") {
            event.data.forEach(ele => {
              if(this.constructionService.turnoverdata.length>0){
                this.constructionService.turnoverdata.forEach(element => {
                  if(element.entityname != ele.entityname)
                     this.constructionService.turnoverdata.push(ele);
                });
              }else{
              this.constructionService.turnoverdata.push(ele);
              }
            });
          }
        }
        break;
      case COPY_PREVIOUS:
        // this.formService.copyPrevious(event, this.schema, this.livedata, this.queries).pipe(untilDestroyed(this)).subscribe(data => {
        //   if (data) {
        //     //this.updateData[data.datapointer] = data.items;
        //     data.items = modifiedExistingArraydata(data.type, data.items);
        //     let dataPointer = getInlineDataPointer(this.schema, event.dataPointer);
        //     this.formGenerator.insertItems(dataPointer, data.items);
        //   }
        //   else {
        //     //popup alert need to implement
        //   }
        // });
        break;

      // case FAVORITE:
      //   // this.formService.updateFavoriteColumnAsync(event, this.schema)
      //   //   .then(() => 'Success');
      //   break;

      case BACK:
        // this.formService.routeTo(this.previousRoute);
        break;

      // case INLINE_GPS:
      //   this.commonService.presentStatusMessageAsync(Loading);
      //   const tempData: any = await this.formService.updateDefaultGPS(event, this.schema);
      //   this.formGenerator.insertItems(tempData.dataPointer, tempData.data);
      //   this.commonService.dismissStausMessage();
      //   break;

      // case CAMERA:
      //   this.commonService.presentStatusMessageAsync(Loading);
      //   const temp: any = await this.formService.updateDefaultImage(event, this.schema);
      //   this.formGenerator.insertItems(temp.dataPointer, temp.data);
      //   this.commonService.dismissStausMessage();
      //   break;
      case DELETEROW:
        if(event.data && event.data.row && Object.keys(event.data.row).length>0){
          this.deleteReviewRow(event);

        }
         break;
      default:
        this.emitOutputEvent("click", event);
        break;
    }
  }

  applyDefaultSign(event) {
    let dataPointer: string = event.dataPointer;
    const tempArray: string[] = dataPointer.split('/').filter((d) => !isNullOrEmptyString(d));
    const signData = this.constructionService.readDefaultSignData(event.applyTo);
    signData.subscribe(res => {
      if (res) {
        var message = JSON.parse(res);
        var payload = JSON.parse(message.Payload);
        var signatureArray = JSON.parse(payload.Signature);
        if (signatureArray.length) {
          let signature = signatureArray[0].signature;
          const data: any = {};
          data[tempArray[tempArray.length - 1]] = signature;
          this.formGenerator.upsertItem(dataPointer, data);
        }
      }
    });
  }
 
deleteReviewRow(event){
  this.commonService.loadingBarAndSnackbarStatus("start", "Deleting Review...");
  setTimeout(() => {
    this.commonService.loadingBarAndSnackbarStatus("", "");
}, 2000);
   let payload: any = {};
   let entityType = this.getEntityType(this.EntityType);
   let queryparams = this.route.snapshot.queryParams;
   let tablename = this.gettablename(queryparams.tableName);
   payload.EntityType = entityType;
   payload.UUID = event.data.row.uuid;
   payload.FormName = this.EntityType != EntityTypes.Project ? this.getSchemaNameByEntityType(entityType) : entityType;
   payload.ReviewersData = event.data.row;
   let capabilityId = this.commonService.getCapabilityId(FABRICS.CONSTRUCTION);
   payload.CapabilityId = capabilityId ? capabilityId : "";
   payload.Fabric = this.commonService.lastOpenedFabric;
    let message = this.commonService.getMessageModel(JSON.stringify(payload), Datatypes.REVIEWANDAPPROVALS, this.EntityId, entityType, MessageKind.DELETE, Routing.OriginSession, tablename, FABRICS.CONSTRUCTION);
    this.formGenerator.removeItem(event);
    this.commonService
      .httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .subscribe(message => {
        message = JSON.parse(message)
        let payload = JSON.parse(message.Payload)
        if(payload && payload.DataType == "SUCCESS"){
        this.commonService.loadingBarAndSnackbarStatus("start", "Reviewer Deleted...");
      setTimeout(() => {
        this.commonService.loadingBarAndSnackbarStatus("", "");
    }, 1000);
  }
 })
}
gettablename(table){
  switch(table){
    case EntityTypes.Journals:
      table = "dailyjournal_review";
      break;
    case EntityTypes.Exhibits:
      table = "exhibit_review";
      break;
    case EntityTypes.Environment:
      table = "environment_review";
      break;
    default:
     break;

  }
  return table;
}

  readTurnoverData(event: any, dataType: string, turnovermodel: string | any[], defaultApprovalType?: undefined) {
    let payload: any = {};
    let type: any;
    let entityType = defaultApprovalType ? defaultApprovalType : this.getEntityType(this.EntityType);
    payload.EntityId = this.EntityId;
    if (event == "TurnoverApprove")
      payload.EntityType = event;
    else
    payload.EntityType = entityType;
    payload.DataType = dataType;
    payload.FormCapability = entityType;
    payload.subType = this.EntityType;
    payload.EntityName = this.EntityName;
    payload.FormName = this.EntityType != EntityTypes.Project ? this.getSchemaNameByEntityType(entityType) : entityType;
    payload.TurnOverModel = turnovermodel;
    let capabilityId = this.commonService.getCapabilityId(FABRICS.CONSTRUCTION);
    payload.CapabilityId = capabilityId ? capabilityId : "";
    payload.Fabric = this.commonService.lastOpenedFabric;
    if (event == "TurnoverApprove")
      type = "READDATA";
    else
      type = "APPROVEDREAD";
    let message = this.commonService.getMessageModel(JSON.stringify(payload), Datatypes.REPORTDATA, this.EntityId, entityType, MessageKind.READ, Routing.OriginSession, type, FABRICS.CONSTRUCTION);

    this.commonService
      .httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .subscribe(res => {
        if (res) {
          var messageData = JSON.parse(res);
          let data = JSON.parse(messageData["Payload"]);
          //let download = JSON.parse(data.Payload);
          if (message.Type == "READDATA") {
            //var tempData = download.Data;
            // this.data = tempData;
            this.initializeSchema();
          }
          else {
            const byteArray = new Uint8Array(atob(data.DownloadData).split('').map(char => char.charCodeAt(0)));
            if (turnovermodel.length == 1)
              saveAs(new Blob([byteArray], { type: 'application/pdf' }), data.FileName);
            else
              saveAs(new Blob([byteArray], { type: 'application/zip' }), data.FileName);
          }
        }
      });
  }

  readDataFromBackend(event, dataType, searchmodel, defaultApprovalType?) {
    let payload: any = {};
    let type: any;
    let tempDatatype: any;
    let entityType = defaultApprovalType ? defaultApprovalType : this.getEntityType(this.EntityType);
    payload.EntityId = this.EntityId;
    payload.EntityType = entityType;
    payload.DataType = dataType;
    payload.FormCapability = entityType;
    payload.subType = this.EntityType;
    payload.EntityName = this.EntityName;
    payload.FormName = this.EntityType != EntityTypes.Project ? this.getSchemaNameByEntityType(entityType) : entityType;
    payload.SearchModel = searchmodel;
    payload.SecurityGroupsId = this.commonService.getSecurityGroupIds(FABRICS.CONSTRUCTION);
    let capabilityId = this.commonService.getCapabilityId(FABRICS.CONSTRUCTION);
    payload.CapabilityId = capabilityId ? capabilityId : "";
    payload.Fabric = this.commonService.lastOpenedFabric;
    if (this.turnoverTreeData && this.turnoverTreeData.EntityType == "TurnoverApprove") {
      type = "READDATA";
      tempDatatype = Datatypes.REPORTDATA;
    }
    else {
      type = payload.Info;
      tempDatatype = Datatypes.LOADFORMDATA;
    }
    let message = this.commonService.getMessageModel(JSON.stringify(payload), tempDatatype, this.EntityId, entityType, MessageKind.READ, Routing.OriginSession, type, FABRICS.CONSTRUCTION);
    this.commonService
      .httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .map(res => {
        var messageData = JSON.parse(res);
        let data = JSON.parse(messageData["Payload"]);
        return data;
      }).subscribe(data => {
        if (data) {
          let tempdata = (data.Data && Object.keys(data.Data).length > 0) ? this.updateDataProperties(data.Data) : {};
          if (dataType == Datatypes.LOADDATA) {
            Object.keys(tempdata).forEach(element => {
              event.data.callback.next(tempdata[element]);
            });
          }
          else if (dataType == Datatypes.SEARCHDATA) {
            this.updateData = setData(tempdata, this.livedata);
          }
        }
      });
  }

  emitOutputEvent(type: string, data: any) {
    this.applicationEvent.emit({ "type": type, data: data });
  }

  closePopup(dialogRef: MatDialogRef<any, any>, event?) {
    if (dialogRef != null) {
      dialogRef.close();
    }
    else {
      this.dialog.closeAll();
    }
    switch (this.popupFormType) {
      case 'filter':
        break;

    }
    //this.logger.debug(event);
  }

  addInlineRow(event: any) {
    let dataPointer = getInlineDataPointer(this.schema, event.dataPointer);
    if (this.EntityType == ReviewAndApproval && event.title == "REVIEWS") {
      this.formGenerator.insertItems(dataPointer, { parent: this.EntityId });
    }
    else
      this.formGenerator.insertItems(dataPointer, []);
  }

  onToggleChange(data) {
    console.log(data);
    switch (data.buttonEvent) {
      case THEME_CHANGE:
        //this.document.body.classList.toggle('dark', data.event.checked);
        break;
    }
  }

  onSubmitFn(event) {
    let data = event ? event.data : null;
    if (data) {
      //clean up data
      for (let key of Object.keys(data)) {
        if (!data[key]) {
          delete data[key];
        }
      }
      // delete json data if we are reading data from other tables..
      if (this.readDataFromOtherSource) {
        let keys = Object.keys(this.readDataFromOtherSource);
        for (let key of keys) {
          key = key.toLowerCase();
          if (data[key]) {
            delete data[key];
          }
        }
      }

      if (!this.EntityType) {
        this.EntityType = this.myForm.getRawValue().EntityType;
      }

      data["EntityType"] = this.getEntityType(this.EntityType);
      data["EntityId"] = this.EntityId;
      data["EntityName"] = this.EntityName ? this.EntityName : null;
      if (this.isNewEntity) {
        data["MessageKind"] = MessageKind.CREATE;
      }
      else {
        data["MessageKind"] = MessageKind.UPDATE;
      }

      this.constructionService.saveIntoDB(event, this.tableNameOrType);
    }
  }

  treeAction(event) {
    // this.formService.treeAction(event).pipe(untilDestroyed(this)).subscribe(data => {
    //   setTimeout(() => {
    //     event.callback.next(data);
    //     (<Subject<any>>event.callback).complete();
    //   }, 0
    //   );
    // });
  }

  async onFilterFn(event) {
    const data: any = JSON.parse(JSON.stringify(event));
    let schemaPage = this.getTableName();
    data.schemapage = schemaPage;
    //await this.formService.filterUpsertAsync(data);
  }

  getTableName() {
    return this.schemaPage.replace(/ /g, '');
  }

  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables);
  }


  getSchemaNameByEntityType(entityType, defaultApprovalType?) {
    let schemaName = entityType;
    if (this.queryParams.entitytype != "constructionadmin") {
      if (entityType == EntityTypes.Project && this.tab == TreeTypeNames.Schedule) {
        schemaName = 'Tasks';
      }
      else if (entityType == EntityTypes.Project && this.tab == TreeTypeNames.TaggedItems) {
        schemaName = EntityTypes.TaggedItems;
      }
      else if (entityType == EntityTypes.Project && this.tab == TreeTypeNames.Materials) {
        schemaName = 'MaterialList';
      }
      else if (entityType == EntityTypes.Project && this.tab == TreeTypeNames.PhysicalInstances) {
        schemaName = EntityTypes.PhysicalInstances;
      }
      else if (entityType == EntityTypes.Project && this.tab == TreeTypeNames.Documents) {
        schemaName = 'Documents-Nav'//EntityTypes.Documents;
      }
      else if (entityType == EntityTypes.Project && this.tab == TreeTypeNames.Approvals) {
        schemaName = TreeTypeNames.Approvals + defaultApprovalType;
      }
      else if (entityType == EntityTypes.Project && this.tab == TreeTypeNames.Turnover) {
        schemaName = TreeTypeNames.Turnover;
      }
    }
    else if (this.queryParams.entitytype == "constructionadmin")
      schemaName = this.queryParams.entitytype;

    return schemaName;
  }


  getEntityType(entityType) {
    let eType = entityType;
    let url = decodeURIComponent(this.router.url);
    if (entityType == EntityTypes.Project && url.toUpperCase()
      .includes(TreeTypeNames.Schedule.toUpperCase())) {
      eType = 'Tasks';
    }
    else if (entityType == EntityTypes.Project && url.toUpperCase()
      .includes(TreeTypeNames.TaggedItems.toUpperCase())) {
      eType = EntityTypes.TaggedItems;
    }
    else if (entityType == EntityTypes.Project && url.toUpperCase()
      .includes(TreeTypeNames.Materials.toUpperCase())) {
      eType = EntityTypes.Materials;
    }
    else if (entityType == EntityTypes.Project && url.toUpperCase()
      .includes(TreeTypeNames.PhysicalInstances.toUpperCase())) {
      eType = EntityTypes.PhysicalInstances;
    }
    else if (entityType == EntityTypes.Project && url.toUpperCase()
      .includes(TreeTypeNames.Documents.toUpperCase())) {
      eType = EntityTypes.Documents;
    }
    else if (entityType == EntityTypes.Project && url.toUpperCase()
      .includes(TreeTypeNames.Turnover.toUpperCase())) {
      eType = EntityTypes.Turnover;
    }
    else if (entityType == "Tagged Item") {
      eType = EntityTypes.TaggedItems;
    }
    return eType;
  }

  filterOperation(event) {
    let eventPointer: string = event.dataPointer;
    let pointer = eventPointer.substring(1, eventPointer.length);
    let obs$: Observable<any> = null;
    const parentPointer: string = (<string>pointer);
    const parents: any[] = parentPointer.split('/');
    let parentPage: string = this.schemaPage + "." + parents[1];
    let otherSources = this.readDataFromOtherSource ? this.readDataFromOtherSource : {};
    let keys = Object.keys(otherSources);
    let isNonInlineTable = keys.find(key => key == parents[1]);
    if (isNonInlineTable) {
      const otherSource: any = {};
      otherSource[parents[1]] = this.readDataFromOtherSource[parents[1]];
      //obs$ = this.loadDataFromOtherSource(parents[0], otherSource)[0];
    } else {
      obs$ = of(this.livedata);
    }
    let inline = isNonInlineTable ? false : true;

    this.getFilterSchema(event, this.schema, parentPage, inline).subscribe(result => {
      this.activateDeactivateFilter(event, result, parentPage, inline);
    });
  }

  activateDeactivateFilter(event, result, parentPage, inline, height?, width?) {
    this.dialogRef = this.filterOpenDialog(result, this.EntityId, this.schemaPage, this.tableName, null, event.dataPointer, parentPage, inline, height, width);
    this.dialogRef.afterClosed().pipe(finalize(() => this.dialogRef = undefined)).subscribe(filtetData => {
      if (filtetData) {
        this.isFilterActive = filtetData.activateFilter;
        this.activeFilterName = filtetData.filterName;
        this.filterQyery = filtetData.nonsqllitequery;
        let entityType = this.getEntityType(this.EntityType);
        let emitData = { id: 'updateFormHeader', data: { 'opacity': false, headId: 'filter' } };
        let searchmodel = [];
        if (filtetData.activateFilter) {
          emitData.data.opacity = true;
          searchmodel.push({ SearchValue: null, SearchField: null, AccordianName: getLowerCase(entityType), FilterQuery: filtetData.nonsqllitequery });
        }
        else if (filtetData.deActivateFilter) {
          emitData.data.opacity = false;
          searchmodel.push({ AccordianName: getLowerCase(entityType) });
        }
        this.commonOutputEmitter.emit(emitData);
        this.readDataFromBackend(filtetData, Datatypes.SEARCHDATA, searchmodel);
      }
    });
  }

  getFilterSchema(event, schema, parentPage, inline) {
    let dataPointer: string = event.dataPointer;
    let filterinputSchema: Observable<any> = null;
    if (!inline) {
      //filterinputSchema = this.queryService.readSchemaByParentPage$(parentPage);
    } else {
      if (dataPointer) {
        let node = getNode(dataPointer, schema);
        if (node.length == 1) {
          filterinputSchema = of(node[0]);
        }
      }
    }
    return filterinputSchema;
  }

  filterOpenDialog(schema: any, entityId, parentId, tableName, data, dataPointer, parentPage, inline, height, width) {
    let jsonSchema = {};
    jsonSchema["entityId"] = entityId;
    jsonSchema["TableData"] = data;
    jsonSchema["TableName"] = tableName;
    jsonSchema["PrentId"] = parentId;
    jsonSchema["dataPointer"] = dataPointer;
    jsonSchema["parentPage"] = parentPage;
    jsonSchema["inline"] = inline;
    jsonSchema["schema"] = schema;
    jsonSchema["clearFilter"] = this.isFilterActive;
    jsonSchema["activeFilterName"] = this.activeFilterName;
    jsonSchema["applicationId"] = this.commonService.getCapabilityId(this.commonService.getFabricNameByUrl(this.router.url));
    width = width ? width : "450px";
    height = height ? height : "550px";
    const dialogRef = this.dialog.open(FilterDialogComponent, {
      width: width,
      height: height,
      disableClose: true,
      data: jsonSchema
    });
    return dialogRef;
  }

  readData(defaultApprovalType?) {
    if (this.EntityType == EntityTypes.Project) {
      let entityType = defaultApprovalType ? defaultApprovalType : this.getEntityType(this.EntityType);
      let searchmodel = [];
      if (this.turnoverTreeData && this.turnoverTreeData.EntityType == "TurnoverApprove") {
        searchmodel.push(this.turnoverTreeData);
        this.constructionService.turnoverdata = [];
      }
      searchmodel.push({ AccordianName: entityType.toLowerCase() });
      this.readDataFromBackend(null, Datatypes.SEARCHDATA, searchmodel, defaultApprovalType);
    }
    else if (this.tab == TreeTypeNames.Approvals && (this.EntityType == ReviewAndApproval)) {
      this.readPDFDocument();
      this.readReviewAndApprovalsData();
    }
    else {
      this.readEntityData();
      let observables$ = [];
      if (this.readDataFromOtherSource) {
        observables$ = observables$.concat(this.loadDataFromOtherSource(this.schemaPage, this.readDataFromOtherSource, false));
      }
      for (let obs of observables$) {
        obs.pipe(untilDestroyed(this))
          .subscribe(data => {
            if (data['extra'] && data['extra'].length) {
              data['extra'] = this.generateDataForExtraTable(data['extra'][0]);
            }
            this.setData(data);
          });
      }
    }
  }

  readReviewAndApprovalsData() {
    let payload: any = {};
    let entityType = this.tableNameOrType;
    payload.EntityId = this.EntityId;
    payload.EntityType = entityType;
    payload.DataType = ReviewAndApproval;
    payload.FormCapability = entityType;
    payload.subType = this.tableNameOrType;
    payload.EntityName = this.EntityName;
    payload.FormName = this.EntityType != EntityTypes.Project ? this.getSchemaNameByEntityType(entityType) : entityType;
    let searchmodel = [];
    searchmodel.push({ AccordianName: entityType.toLowerCase() });
    payload.SearchModel = searchmodel;
    let capabilityId = this.commonService.getCapabilityId(FABRICS.CONSTRUCTION);
    payload.CapabilityId = capabilityId ? capabilityId : "";
    payload.Fabric = this.commonService.lastOpenedFabric;
    let message = this.commonService.getMessageModel(JSON.stringify(payload), Datatypes.LOADFORMDATA, this.EntityId, entityType, MessageKind.READ, Routing.OriginSession, payload.Info, FABRICS.CONSTRUCTION);
    this.commonService
      .httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .map(res => {
        var messageData = JSON.parse(res);
        let data = JSON.parse(messageData["Payload"]);
        return data;
      }).subscribe(data => {
        if (data) {
          // if (data.Data) {
          //   data.Data[Reviewers] = data.Data.status ? JSON.parse(JSON.stringify(data.Data.status)) : null;
          //   data.Data.status = data.Data.status && data.Data.status[0] ? data.Data.status[0] : null;
          //   data.Data[Approval] = data.Data.status ? JSON.parse(JSON.stringify(data.Data.status)) : null;
          // }
          //this.data=null;
          // this.livedata=null;
          //this.updateData=null;
          if (data.Data) {
            if (data.Data[Reviewers].length == 1 && !data.Data[Reviewers][0].uuid) {
              data.Data[Reviewers][0].uuid = generateUUID();
              data.Data[Reviewers][0].parent = data.Data[Approval].uuid;
            }
          }

          let tempdata = (data.Data && Object.keys(data.Data).length > 0) ? this.updateDataProperties(data.Data) : {};
          this.updateData = setData(tempdata, this.livedata);
        }
      });

  }
  readEntityData() {
    let payload: any = {};
    let entityType = this.getEntityType(this.EntityType);
    payload.EntityId = this.EntityId;
    payload.EntityType = entityType;
    payload.DataType = Datatypes.ENTITYDATA;
    payload.FormCapability = entityType;
    payload.subType = this.EntityType;
    payload.EntityName = this.EntityName;
    let capabilityId = this.commonService.getCapabilityId(FABRICS.CONSTRUCTION);
    payload.CapabilityId = capabilityId ? capabilityId : "";
    payload.FormType = "Configuration";
    payload.Fabric = this.commonService.lastOpenedFabric;
    let message = this.commonService.getMessageModel(JSON.stringify(payload), Datatypes.CONSTRUCTION, this.EntityId, Datatypes.CONSTRUCTION, MessageKind.READ, Routing.OriginSession, payload.Info, FABRICS.CONSTRUCTION);
    this.commonService
      .httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .map(res => {
        var messageData = JSON.parse(res);
        let data = JSON.parse(messageData["Payload"]);
        return data;
      }).subscribe(data => {
        if (data) {
          let tempdata = (data.Data && Object.keys(data.Data).length > 0) ? this.updateDataProperties(data.Data) : {};
          if (this.payloadData) {
            tempdata['extra'] = this.generateDataForExtraTable();
          }
          this.updateData = setData(tempdata, this.livedata);
        }
      });
  }

  ngOnDestroy() {
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }
}
