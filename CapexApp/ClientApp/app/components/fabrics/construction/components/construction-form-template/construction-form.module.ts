import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule1 } from '../../../../shared/shared1.module';
import { ConstructionFormTemplateComponent } from './construction-form-template.component';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';

import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: ConstructionFormTemplateComponent,
    children: []
  }
];

//export const routing = RouterModule.forChild(routes);

@NgModule({
  declarations: [
    ConstructionFormTemplateComponent
  ],
  imports: [
    CommonModule,
    SharedModule1,
    NgxExtendedPdfViewerModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]

})
export class ConstructionFormModule {

}
