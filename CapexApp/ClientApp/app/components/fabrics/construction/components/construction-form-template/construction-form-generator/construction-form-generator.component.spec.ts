import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstructionFormGeneratorComponent } from './construction-form-generator.component';

describe('ConstructionFormGeneratorComponent', () => {
  let component: ConstructionFormGeneratorComponent;
  let fixture: ComponentFixture<ConstructionFormGeneratorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConstructionFormGeneratorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstructionFormGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
