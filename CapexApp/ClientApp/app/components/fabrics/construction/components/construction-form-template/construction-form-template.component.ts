import { Component, OnInit, OnDestroy, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { Subject, combineLatest } from 'rxjs';
import { ConstructionService } from '../../services/construction.service';
import { CommonService } from '../../../../globals/CommonService';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { takeUntil } from 'rxjs/operators';
import { AngularTreeMessageModel, Datatypes, MessageKind, TreeOperations, TreeTypeNames } from 'ClientApp/app/components/globals/Model/CommonModel';
import { FabricRouter } from '../../../../globals/Model/FabricRouter';
import { TreeModel } from '@circlon/angular-tree-component';
import { TreeDataEventResponseModel, AngularTreeEventMessageModel, FABRICS } from '../../../../globals/Model/CommonModel';
import { AppConsts, ApprovalsType, ReviewAndApproval, Turnover } from 'ClientApp/app/components/common/Constants/AppConsts';
import { getNewUUID, getqueryParam, getShowStatus } from 'ClientApp/app/components/globals/helper.functions';
import { ApplicationTheme } from '../../../../common/Constants/AppConsts';
import { EntityTypes } from 'ClientApp/app/components/globals/Model/EntityTypes';
import { ResizeEvent } from 'angular-resizable-element';
import { AppInsightsService } from 'ClientApp/app/components/common/app-insight/appinsight-service';
import { ConstructionFormGeneratorComponent } from './construction-form-generator/construction-form-generator.component';
import { Routing } from 'ClientApp/app/components/globals/Model/Message';
import { WORKER_TOPIC } from 'ClientApp/webWorker/app-workers/shared/worker-topic.constants';
import { guid } from '@datorama/akita';
export const PREVIOUS = 'PREVIOUS';
export const NEXT = 'NEXT';
export const CHANGE = 'CHANGE';

@Component({
  selector: 'app-construction-form-template',
  templateUrl: './construction-form-template.component.html',
  styleUrls: ['./construction-form-template.component.scss']
})
export class ConstructionFormTemplateComponent implements OnInit, OnDestroy {
  @ViewChild('sideHeaderElement', { static: false }) sideHeaderElement: ElementRef;
  headerConfig = [];
  subheaderConfig = [];
  ReviewheaderConfig = [];
  TuronverheaderConfig = [];
  TurnoverLeftheaderConfig = [];
  locationName;
  minWidth = 234;
  maxWidth = 362;
  leftTreeWidth = 234;
  listOfChildren = [];
  myForm: FormGroup;
  tab = '';
  adminData;
  schema;
  entityData;
  //routeParam: any = {};
  nodeId = [];
  dialogRef;
  isForm: boolean;
  commonInputVariable = { "updated": true };
  treeIndexTab = "";
  activeroute = false;
  formInitialized: boolean = false;
  isPdfFound: boolean = false;
  isheaderinitialize: boolean = false;
  isTab: boolean = true;
  userId;
  param;
  expanded;
  takeUntilDestroyObservables$ = new Subject();
  formHeaderNgSelectFormControl = new FormControl();
  currentEntityType;
  reviewType = ReviewAndApproval;
  turoverType = Turnover;
  base64PdfSrc = "";

  darkLightTheme;

  entityturnover: string;

  turnovarjson: any[];

  constructor(public constructionService: ConstructionService, public commonService: CommonService, private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder, public dialog: MatDialog, private logger: AppInsightsService) {

    if (!this.commonService.themeDark)
      this.darkLightTheme = ApplicationTheme.dark;
    else
      this.darkLightTheme = ApplicationTheme.light;

    let tempSubscription = this.constructionService.onTreeEventChanges$
      .filter(obj => obj.TreeType == this.commonService.treeIndexLabel)
      .subscribe(data => {
        try {
          setTimeout(() => {  // to highlight the activeSelected node after reloading
            // this.creatEntityMangementForm();
          }, 2500);

          if (this.entityData && this.router.url && this.router.url.includes(this.entityData.EntityId)) {
            this.constructionService.sendDataToAngularTree([this.param.tab], TreeOperations.ActiveSelectedNode, this.entityData.EntityId);
          }
          tempSubscription.unsubscribe();
        }
        catch (e) {
          this.commonService.appLogException(new Error('Exception in onTreeEventChanges$ subscriber of constructor of Contruction.formtemplate.component' + new Date().toString() + '. Exception is : ' + e));
        }
      });

    this.formHeaderNgSelectFormControl.valueChanges.pipe(this.compUntilDestroyed()).subscribe(value => {
      this.constructionService.sendDataToComponent$.next({ id: 'ngselect', value: value });
    })
  }

  ngOnInit(): void {
    //   this.router.routeReuseStrategy.shouldReuseRoute = function(){
    //     return false;
    //  }
    //setTimeout(() => {
    this.creatEntityMangementForm();

    //}, 0);
    // this.setHeader(params);
    // if (params && params.hasOwnProperty('expand')) {
    //   this.expanded = JSON.parse(params.expand);
    // }
  }

  creatEntityMangementForm() {
    combineLatest([this.route.params, this.route.queryParams])
      .filter(d => this.commonService.getFabricNameByUrl(this.router.url) == FABRICS.CONSTRUCTION)
      .pipe(this.compUntilDestroyed())
      .subscribe(([params, queryParams]) => {
        try {
          params = JSON.parse(JSON.stringify(params));
          params.expand = queryParams.expand;
          params.leftnav = queryParams.leftnav;
          params.schema = queryParams.schema;
          params.entityName = queryParams.entityName;
          params.operationType = queryParams.operationType;
          params.entityType = queryParams.entityType;
          this.locationName = params.entityName ? params.entityName : params.schema;
          params.tab = queryParams.tab;
          this.currentEntityType = params.entityType ? params.entityType : null;
          this.isheaderinitialize = params.entityType == EntityTypes.Project ? true : false;
          this.isTab = params.tab == TreeTypeNames.Turnover ? false : true;
          if (params.tab == this.turoverType && params.entityType && params.entityType == "Project") {
            this.entityturnover = this.turoverType;
            this.readTurnoverDataFromADL(params);
          } else {
            this.entityturnover = null;
          }
          this.setHeader(params);
          this.ActivateTreeNode();
          // if (params && params.hasOwnProperty('expand')) {
          //   this.expanded = JSON.parse(params.expand);
          // }
        }
        catch (e) {
          this.commonService.appLogException(new Error('Exception inroute.queryParams subscriber of constructor of Construction.formtemplate.component at time ' + new Date().toString() + '. Exception is : ' + e));
        }
      });
  }

  readTurnoverDataFromADL(params) {
    let payload: any = {};
    let type: any;
    payload.EntityType = params.entityType;
    payload.EntityId = getNewUUID();
    payload.DataType = Datatypes.REPORTDATA
    payload.FormCapability = params.entityType;
    payload.ProjectName = params.entityName;
    let capabilityId = this.commonService.getCapabilityId(FABRICS.CONSTRUCTION);
    payload.CapabilityId = capabilityId ? capabilityId : "";
    payload.Fabric = this.commonService.lastOpenedFabric;
    type = "LEFTTREEDATA";
    let message = this.commonService.getMessageModel(JSON.stringify(payload), Datatypes.REPORTDATA, payload.EntityId, params.entityType, MessageKind.READ, Routing.OriginSession, type, FABRICS.CONSTRUCTION);

    this.commonService
      .httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .subscribe(resData => {
        if (resData) {
          var turnOverTreedata = JSON.parse(resData);
          var payloadData = JSON.parse(turnOverTreedata["Payload"]);
          //var turnoverNodeData = JSON.parse(payloadData["Payload"]);
          var nodedata = this.commonService.flatToHierarchyState(payloadData);
          setTimeout(() => {
            this.sendDataToAngularTree("turnoverApproveTree", TreeOperations.createFullTree, nodedata);
          }, 0);
        }
      });
  }

  ActivateTreeNode() {
    const queryParam = this.router.parseUrl(this.router.url).queryParams;
    this.constructionService.ActivateNode(queryParam.formId, queryParam.tab)
  }
  sendDataToAngularTree(treeName: string, treeOperationType: string, payload: any) {
    try {
      var treeMessage: AngularTreeMessageModel = {
        "fabric": this.commonService.getFabricNameByUrl(this.router.url),
        "treeName": [treeName],
        "treeOperationType": treeOperationType,
        "treePayload": payload
      }
      this.commonService.sendDataToAngularTree.next(treeMessage);
    } catch (e) {
      console.error('exception in sendDataToAngularTree() of AuthorizationService at time ' + new Date().toString() + '. exception is : ' + e);
      this.logger.logException(new Error('Exception in sendDataToAngularTree() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  setHeader(RouteParam) {
    this.headerConfig = [
      {
        'source': RouteParam.expand && JSON.parse(RouteParam.expand) ? 'V3 CollapsAll' : 'V3 ExpandAll',
        'title': RouteParam.expand && JSON.parse(RouteParam.expand) ? 'Collapse All' : 'Expand All',
        'routerLinkActive': false,
        'id': 'expandcollapse',
        'class': 'icon21-21',
        'float': 'left',
        'type': 'icon',
        'show': true,
        'opacity': true
      },
      {
        'source': 'V3 CloseCancel',
        'title': 'Close',
        'routerLinkActive': false,
        'id': 'close',
        'class': 'icon21-21',
        'float': 'right',
        'type': 'icon',
        'show': true,
        'opacity': true
      },
      {
        'source': 'V3 LeftPaneFilter',
        'title': 'Filter',
        'routerLinkActive': false,
        'id': 'filter',
        'class': 'icon21-21',
        'float': 'right',
        'type': 'icon',
        'show': false,
        'opacity': false
      },
      {
        'source': 'MainSearch',
        'title': 'Search',
        'routerLinkActive': false,
        'id': 'search',
        'class': 'icon21-21',
        'float': 'right',
        'type': 'icon',
        'show': false,
        'opacity': false
      },
      {
        'source': '',
        'title': 'Search Input',
        'routerLinkActive': false,
        'id': 'searchinput',
        'class': 'icon21-21',
        'float': 'right',
        'type': 'searchinput',
        'show': false
      },
      {
        'source': 'V3 PeepsAndComps',
        'title': this.locationName,
        'routerLinkActive': false,
        'id': 'location',
        'class': 'icon21-21',
        'float': 'left',
        'type': 'select',
        'show': true,
        'uppercase': 'Isuppercase'
      },
      {
        'source': '',
        'title': 'Entity Type',
        'routerLinkActive': false,
        'id': 'ngselect',
        'class': 'icon21-21',
        'float': 'left',
        'type': 'ngselect',
        'show': false,
        'options': [],
        'defaultValue': this.formHeaderNgSelectFormControl
      }
    ];

    this.subheaderConfig = [
      {
        'source': 'V3 LeftPaneFilter',
        'title': 'Filter',
        'routerLinkActive': false,
        'id': 'filter',
        'class': 'icon21-21',
        'float': 'left',
        'type': 'icon',
        'show': getShowStatus('filter', RouteParam.entityType, this.getCurrentTab()),
        'opacity': false,
      },
      {
        'source': 'MainSearch',
        'title': 'Search',
        'routerLinkActive': false,
        'id': 'search',
        'class': 'icon21-21',
        'float': 'left',
        'type': 'icon',
        'show': getShowStatus('search', RouteParam.entityType, this.getCurrentTab()),
        'opacity': false
      },
      {
        'source': '',
        'title': 'REFRESH TABLES',
        'routerLinkActive': false,
        'id': 'button',
        'class': 'icon21-21',
        'float': 'right',
        'type': 'button',
        'show': (RouteParam.entityType == EntityTypes.Project)
      },
      {
        'source': '',
        'title': 'Search Input',
        'routerLinkActive': false,
        'id': 'searchinput',
        'class': 'icon21-21',
        'float': 'left',
        'type': 'searchinput',
        'show': false
      },
      {
        'source': '',
        'title': 'Entity Type',
        'routerLinkActive': false,
        'id': 'ngselect',
        'class': 'icon21-21',
        'float': 'left',
        'type': 'ngselect',
        'show': getShowStatus('ngselect', RouteParam.entityType, this.getCurrentTab()),
        'options': [],
        'defaultValue': this.formHeaderNgSelectFormControl
      }
    ];

    this.ReviewheaderConfig = [
      {
        'source': RouteParam.expand && JSON.parse(RouteParam.expand) ? 'V3 CollapsAll' : 'V3 ExpandAll',
        'title': RouteParam.expand && JSON.parse(RouteParam.expand) ? 'Collapse All' : 'Expand All',
        'routerLinkActive': false,
        'id': 'expandcollapse',
        'class': 'icon21-21',
        'float': 'left',
        'type': 'icon',
        'show': true,
        'opacity': true
      },
      {
        'source': 'FDC_Checkmark_Icon_Idle',
        'title': 'Save',
        'routerLinkActive': false,
        'id': 'save',
        'class': 'icon21-21',
        'float': 'right',
        'type': 'icon',
        'show': true
      },
      {
        'source': 'V3 General',
        'title': RouteParam.entityType,
        'routerLinkActive': false,
        'id': 'title',
        'class': 'icon21-21',
        'float': 'left',
        'type': 'title',
        'show': true,
        'uppercase': 'Isuppercase'
      }

    ];
    this.TurnoverLeftheaderConfig = [
      {
        'source': RouteParam.expand && JSON.parse(RouteParam.expand) ? 'V3 CollapsAll' : 'V3 ExpandAll',
        'title': RouteParam.expand && JSON.parse(RouteParam.expand) ? 'Collapse All' : 'Expand All',
        'routerLinkActive': false,
        'id': 'expandcollapse',
        'class': 'icon21-21',
        'float': 'left',
        'type': 'icon',
        'show': false,
        'opacity': true
      },
      {
        'source': 'V3 General',
        'title': "APPROVED DOCUMENTS",
        'routerLinkActive': false,
        'id': 'title',
        'class': 'icon21-21',
        'float': 'left',
        'type': 'title',
        'show': true,
        'uppercase': 'Isuppercase'
      }

    ];
    this.TuronverheaderConfig = [
      {
        'source': 'V3 LeftPaneFilter',
        'title': 'Filter',
        'routerLinkActive': false,
        'id': 'filter',
        'class': 'icon21-21',
        'float': 'left',
        'type': 'icon',
        'show': getShowStatus('filter', RouteParam.entityType, this.getCurrentTab()),
        'opacity': false,
      },
      {
        'source': 'MainSearch',
        'title': 'Search',
        'routerLinkActive': false,
        'id': 'search',
        'class': 'icon21-21',
        'float': 'left',
        'type': 'icon',
        'show': getShowStatus('search', RouteParam.entityType, this.getCurrentTab()),
        'opacity': false
      },
      {
        'source': '',
        'title': 'DOWNLOAD SELECTED',
        'routerLinkActive': false,
        'id': 'button',
        'class': 'btn-color',
        'float': 'right',
        'type': 'button',
        'show': (RouteParam.entityType == EntityTypes.Project)
      },
      {
        'source': '',
        'title': 'Search Input',
        'routerLinkActive': false,
        'id': 'searchinput',
        'class': 'icon21-21',
        'float': 'left',
        'type': 'searchinput',
        'show': getShowStatus('search', RouteParam.entityType, this.getCurrentTab())
      }
    ];

    if (RouteParam.tab == TreeTypeNames.Approvals && RouteParam.entityType != ReviewAndApproval) {
      this.subheaderConfig.forEach((headerObject: any) => {
        if (headerObject.id == "ngselect") {
          headerObject.title = RouteParam.tab + " Type";
          headerObject.options = ApprovalsType;
          setTimeout(() => {
            this.formHeaderNgSelectFormControl.setValue(ApprovalsType[0]);
          }, 10);
        }
      });


    }

  }
  onResizeEnd(ev: ResizeEvent) {
    if (this.commonService.isUserSettingForm) {
      if (ev.rectangle.right <= this.maxWidth && ev.rectangle.right >= (this.minWidth + 42))
        this.leftTreeWidth = ev.rectangle.right - 42;
    }
  }
  ngAfterViewChecked() {
    this.commonService.sideHeaderElement = this.sideHeaderElement;
  }
  // getListOfChildrens(entityId) {
  //   let entityData: any = this.commonService.getEntitiesById(entityId);
  //   let capabilityName: any = [];

  //   this.listOfChildren = [];
  //   this.listOfChildren.push({ "EntityName": this.routeParam.EntityName, "EntityType": this.routeParam.schema, "EntityId": this.routeParam.EntityID, "LocationId": entityData.EntityId, TypeOfEntity: entityData.TypeOf, "Enable": true, tab: this.tab, "expand": false });

  //   this.addlistOfChildren(this.routeParam.EntityID, this.routeParam.tab, this.routeParam.schema);
  //   this.isForm = true;

  //   if (this.treeIndexTab) {
  //     this.constructionService.sendDataToAngularTree([this.routeParam.tab], TreeOperations.deactivateSelectedNode, "");
  //     if (this.commonService.searchActive) {
  //       this.constructionService.activateEntityWhenSearchActive(this.routeParam);
  //     }
  //     else {
  //       this.constructionService.sendDataToAngularTree([this.routeParam.tab], TreeOperations.ActiveSelectedNode, this.routeParam.EntityID);
  //     }
  //   }

  //   if (capabilityName.length > 0 && !capabilityName.includes(this.commonService.getCapabilityFormNameBasedOnTab(this.treeIndexTab))) {
  //     this.closeForm();
  //   }
  // }

  addlistOfChildren(EntityId, tab, EntityType) {
    let entityData: any = this.commonService.getEntitiesById(EntityId);

    this.myForm = this.createGroup();
  }

  createGroup() {
    var group = this.formBuilder.group([]);
    try {
      let controlarray = this.formBuilder.array([], { updateOn: "blur" });
      group.addControl('listOfChildren', controlarray);
      const control = group.controls['listOfChildren'] as FormArray;

      this.listOfChildren.forEach((obj: any) => {
        control.push(this.formBuilder.group(obj));
      });
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in createGroup() of Construction.formtemplate.component at time ' + new Date().toString() + '. Exception is : ' + e));
    }
    return group;
  }

  private closeForm() {
    const queryParam = this.router.parseUrl(this.router.url).queryParams;
    let currentEntityId = queryParam['formId'];
    delete queryParam['operationType'];
    delete queryParam['entityType'];
    delete queryParam['schema'];
    delete queryParam['entityId'];
    delete queryParam['entityName'];
    delete queryParam['formId'];
    let params = JSON.parse(JSON.stringify(queryParam));
    delete params.EntityID;
    this.constructionService.sendDataToAngularTree(AppConsts.ConstructionTreeList, TreeOperations.deactivateSelectedNode, currentEntityId);
    this.router.navigate([FabricRouter.CONSTRUCTION_FABRIC], { queryParams: params });
  }

  ChangeLocation(event) {
    try {
      switch (event.opreation) {
        case PREVIOUS:
          this.getPrevLocation();
          break;

        case NEXT:
          this.getNextLocation();
          break;

        case CHANGE:
          break;
      }

      if (event && event.opreation && (event.opreation === PREVIOUS || event.opreation === NEXT)) {
        this.commonService.isNextOrPreviousClicked = true;
      }
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in ChangeLocation() of Construction.formtemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  getPrevLocation() {
    /**
     * new logic started
     * this logic is written to get prev locations
     */
    this.commonService.event = "drillUp";
    const queryParam = this.router.parseUrl(this.router.url).queryParams;
    var treeName = queryParam.tab;

    this.constructionService.getAngularTreeEvent([treeName], TreeOperations.GetTreeInstance).
      first().
      subscribe((response: TreeDataEventResponseModel) => {
        let treeNames: Array<string> = response.treeName;
        let tree = response.treePayload;

        if (tree && treeNames.indexOf(treeName) > -1) {
          var tempdata = this.commonService.previousLocationData;
          var model: TreeModel = tree.treeModel;
          model.focusPreviousNode();
        }

        let RouteParam = JSON.parse(JSON.stringify(queryParam));
        RouteParam.EntityID = this.commonService.previousLocationData.EntityId;

        if (treeName) {
          this.constructionService.sendDataToAngularTree([treeName], 'ActiveSelectedNode', RouteParam.EntityID);
        }
        this.navigateBasedOnQueryParams(RouteParam);
      });
  }

  getNextLocation() {
    try {
      this.commonService.event = "drillDown";
      const queryParam = this.router.parseUrl(this.router.url).queryParams;
      var treeName = queryParam.tab;
      this.constructionService.getAngularTreeEvent([treeName], TreeOperations.GetTreeInstance)
        .first()
        .subscribe((response: TreeDataEventResponseModel) => {
          let treeNames: Array<string> = response.treeName;
          let tree = response.treePayload;
          if (tree && treeNames.indexOf(treeName) > -1) {
            var tempdata = this.commonService.previousLocationData;
            var model: TreeModel = tree.treeModel;
            model.focusNextNode();
            //this logic is added to get next location
            if (tempdata && tempdata.children && tempdata.children.length != 0) {
              this.commonService.previousLocationData = tempdata.children[0];
            }

            if (this.commonService.previousLocationData) {
              this.nodeId.push(this.commonService.previousLocationData.EntityId);
            }

            if (this.commonService.previousLocationData && this.commonService.previousLocationData.children && this.commonService.previousLocationData.children.length != 0) {
              for (var a in this.nodeId) {
                if (this.nodeId[a] == this.commonService.previousLocationData.EntityId) {
                  model.isNodeFocused;
                }
              }
            }

            /**  next locatiion logic end  */
            if (!this.commonService.previousLocationData) {
              let data = this.getFirstChildLocation(model.getFirstRoot());
              if (data) {
                this.commonService.previousLocationData = data.data;
              }
            }

            let RouteParam = JSON.parse(JSON.stringify(queryParam));
            RouteParam.EntityID = this.commonService.previousLocationData.EntityId;
            if (treeName) {
              this.constructionService.sendDataToAngularTree([treeName], 'ActiveSelectedNode', RouteParam.EntityID);
            }

            this.navigateBasedOnQueryParams(RouteParam);
          }
        });
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in getNextLocation() of Construction.formtemplate.component at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  commonOutputEmitter(event) {
    switch (event.id) {
      case "updateFormHeader":
        this.headerConfig.forEach((headerObject: any) => {
          if (headerObject.id == event.data.headId) {
            if (event.data.hasOwnProperty('opacity')) {
              headerObject.opacity = event.data.opacity;
            }
          }
        });
        break;
      case "pdfBase64Src":
        if (!event.value) {
          this.isPdfFound = true;
        } else {
          this.isPdfFound = false;
        }
        this.base64PdfSrc = event.value ? event.value : '';
        break;
    }
  }

  navigateBasedOnQueryParams(RouteParam) {
    this.router.navigate([FabricRouter.CONSTRUCTION_FABRIC, 'Settings', RouteParam.EntityID, RouteParam.tab], { queryParams: RouteParam });
  }

  getFirstChildLocation(dist) {
    try {
      var temp = dist.getFirstChild();
      if (temp) {
        this.getFirstChildLocation(temp);
      }
      if (temp.data.typeOfField == "Location") {
        return temp;
      }
    }
    catch (e) {
      console.warn("first location doesnt exist");
    }
  }

  ClickEvent(data) {
    const queryParam = this.router.parseUrl(this.router.url).queryParams;
    let RouteParam = JSON.parse(JSON.stringify(queryParam));
    if (data && data.id) {
      try {
        switch (data.id) {
          case "close":
            this.closeForm();
            break;

          case 'expandcollapse':
            RouteParam.expand = !JSON.parse(this.route.queryParams['_value'].expand);
            this.listOfChildren.forEach(element => {
              element.expand = RouteParam.expand;
            });
            this.navigateBasedOnQueryParams(RouteParam);
            break;

          case "save":
            this.constructionService.sendDataToComponent$.next(data);
            break;

          case "searchinput":
          case "ngselect":
          case "filter":
            this.constructionService.sendDataToComponent$.next(data);
            break;
          case "button":
            if (data.title == "DOWNLOAD SELECTED") {
              this.constructionService.readturnover.next(data);

            }
            break;
        }
      }
      catch (e) {
        this.commonService.appLogException(new Error('Exception in getNextLocation() of Construction.formtemplate.component at time ' + new Date().toString() + '. Exception is : ' + e));
      }
    }
  }

  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables$);
  }


  ngOnDestroy(): void {
    this.commonService.entityData = undefined;
    this.takeUntilDestroyObservables$.next();
    this.takeUntilDestroyObservables$.complete();
  }

  getCurrentTab() {
    return getqueryParam(this.router)['tab'];
  }
}
