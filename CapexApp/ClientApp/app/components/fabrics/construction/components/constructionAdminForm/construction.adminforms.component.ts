import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, Pipe, PipeTransform } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { TreeModel } from '@circlon/angular-tree-component';
import { AppConsts, CHANGE } from 'ClientApp/app/components/common/Constants/AppConsts';
import { NEXT, PREVIOUS } from 'ClientApp/app/components/common/form-header/form-header.component';
import { FabricRouter } from 'ClientApp/app/components/globals/Model/FabricRouter';
import { Observable, Subject, combineLatest } from 'rxjs';
import { takeUntil } from "rxjs/operators";
import { CommonService } from '../../../../globals/CommonService';
import { PopupOperation } from '../../../../globals/Model/AlertConfig';
import { FABRICS, TreeDataEventResponseModel, TreeOperations, TreeTypeNames } from '../../../../globals/Model/CommonModel';
import { ConstructionService } from '../../services/construction.service';

@Component({
    selector: 'constructionadminforms',
    templateUrl: './construction.adminforms.component.html',
    styleUrls: ['./construction.adminforms.component.styles.scss'],
    changeDetection: ChangeDetectionStrategy.Default
})

export class ConstructionAdminFormsComponent implements OnInit, OnDestroy {
    EntityId;
    myForm: FormGroup;
    EntityType: any;
    locationName: any;
    ListsofData: any = [];
    ListQueryParmas = {};
    queryParams = [];
    headerConfig = [];
    nodeId = [];
    dialogRef;
    expandCollapsebooleanValue: boolean = true;
    isExpand: boolean = true;
    intialized: boolean = false;
    formHeaderNgSelectFormControl = new FormControl();
    takeUntilDestroyObservables = new Subject();
    constructor(public commonService: CommonService, public router: Router, public dialog: MatDialog,
        private cdRef: ChangeDetectorRef,
        private formBuilder: FormBuilder, private route: ActivatedRoute, public constructionservice: ConstructionService) {
        try {
            //   this.constructionservice.IsFdcAdmin = false;

            console.log("Admin forms");
        } catch (e) {
            console.error('Exception in constructor of fdc.adminforms.component in FDCAdminForm at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
            this.commonService.appLogException(new Error('Exception in constructor of fdc.adminforms.component in FDCAdminForm at time ' + new Date().toString() + '. Exception is : ' + e));
        }
    }
    ngOnInit() {
        try {
            this.creatEntityMangementForm();
            console.log("Admin forms");
        } catch (e) {
            console.error('Exception in ngOnInit() of ConstructionAdminFormsComponent  at time ' + new Date().toString() + '. Exception is : ' + e);
            this.commonService.appLogException(new Error('Exception in ngOnInit() of ConstructionAdminFormsComponent  at time ' + new Date().toString() + '. Exception is : ' + e));
        }
    }
    creatEntityMangementForm() {
        combineLatest([this.route.params, this.route.queryParams])
            .filter(d => this.commonService.getFabricNameByUrl(this.router.url) == FABRICS.CONSTRUCTION)
            .pipe(this.compUntilDestroyed())
            .subscribe(([params, queryParams]) => {
                try {
                    params = JSON.parse(JSON.stringify(params));
                    params.expand = queryParams.expand;
                    params.leftnav = queryParams.leftnav;
                    params.schema = queryParams.schema;
                    params.entityName = queryParams.entityName;
                    params.operationType = queryParams.operationType;
                    params.entityType = queryParams.entityType;
                    this.locationName = params.entityName ? params.entityName : params.schema;
                    params.tab = queryParams.tab;
                    this.setHeader(params);
                    this.ActivateTreeNode()
                }
                catch (e) {
                    this.commonService.appLogException(new Error('Exception inroute.queryParams subscriber of constructor of Construction.formtemplate.component at time ' + new Date().toString() + '. Exception is : ' + e));
                }
            });
    }
    ActivateTreeNode() {
        const queryParam = this.router.parseUrl(this.router.url).queryParams;
        this.constructionservice.ActivateNode(queryParam.formId, queryParam.tab)
    }
    setHeader(RouteParam) {
        this.headerConfig = [
            {
                'source': RouteParam.expand && JSON.parse(RouteParam.expand) ? 'V3 CollapsAll' : 'V3 ExpandAll',
                'title': RouteParam.expand && JSON.parse(RouteParam.expand) ? 'Collapse All' : 'Expand All',
                'routerLinkActive': false,
                'id': 'expandcollapse',
                'class': 'icon21-21',
                'float': 'left',
                'type': 'icon',
                'show': true,
                'opacity': true
            },
            {
                'source': 'V3 CloseCancel',
                'title': 'Close',
                'routerLinkActive': false,
                'id': 'close',
                'class': 'icon21-21',
                'float': 'right',
                'type': 'icon',
                'show': true,
                'opacity': true
            },
            {
                'source': 'V3 LeftPaneFilter',
                'title': 'Filter',
                'routerLinkActive': false,
                'id': 'filter',
                'class': 'icon21-21',
                'float': 'right',
                'type': 'icon',
                'show': false,
                'opacity': false
            },
            {
                'source': 'MainSearch',
                'title': 'Search',
                'routerLinkActive': false,
                'id': 'search',
                'class': 'icon21-21',
                'float': 'right',
                'type': 'icon',
                'show': false,
                'opacity': false
            },
            {
                'source': '',
                'title': 'Search Input',
                'routerLinkActive': false,
                'id': 'searchinput',
                'class': 'icon21-21',
                'float': 'right',
                'type': 'searchinput',
                'show': false
            },
            {
                'source': 'V3 PeepsAndComps',
                'title': this.locationName,
                'routerLinkActive': false,
                'id': 'location',
                'class': 'icon21-21',
                'float': 'left',
                'type': 'select',
                'show': true,
                'uppercase': 'Isuppercase'
            },
            {
                'source': '',
                'title': 'Entity Type',
                'routerLinkActive': false,
                'id': 'ngselect',
                'class': 'icon21-21',
                'float': 'left',
                'type': 'ngselect',
                'show': false,
                'options': [],
                'defaultValue': this.formHeaderNgSelectFormControl
            }
        ];
    }
    ClickEvent(data) {
        try {
            // this.navButton = data.id;
            let url = '';
            switch (data.id) {
                case 'close':
                    this.expandCollapsebooleanValue = true;
                    this.closeForm();
                    break;
                case 'expandcollapse':
                    {
                        this.expandCollapsebooleanValue = false;
                        //url = '/'+ FabricRouter.FDC +'/Map/fdcadminform';
                        data.source = !this.isExpand ? 'V3 ExpandAll' : 'V3 CollapsAll';
                        data.title = this.isExpand ? 'Expand All' : 'Collapse All'
                        this.isExpand = !this.isExpand;
                        this.router.navigate([url, { EntityType: "fdcglobaladmin", Expand: this.isExpand }], { queryParams: this.queryParams });
                    }
            }
        }
        catch (e) {
            console.error('Exception in ClickEvent() of ConstructionAdminFormsComponent  at time ' + new Date().toString() + '. Exception is : ' + e);
            this.commonService.appLogException(new Error('Exception in ClickEvent() of ConstructionAdminFormsComponent  at time ' + new Date().toString() + '. Exception is : ' + e));
        }
    }
    private closeForm() {
        const queryParam = this.router.parseUrl(this.router.url).queryParams;
        let currentEntityId = queryParam['formId'];
        delete queryParam['operationType'];
        delete queryParam['entityType'];
        delete queryParam['schema'];
        delete queryParam['entityId'];
        delete queryParam['entityName'];
        delete queryParam['formId'];
        let params = JSON.parse(JSON.stringify(queryParam));
        delete params.EntityID;
        this.constructionservice.sendDataToAngularTree(AppConsts.ConstructionTreeList, TreeOperations.deactivateSelectedNode, currentEntityId);
        this.router.navigate([FabricRouter.CONSTRUCTION_FABRIC], { queryParams: params });
    }
    ChangeLocation(event) {
        try {
            switch (event.opreation) {
                case PREVIOUS:
                    this.getPrevLocation();
                    break;

                case NEXT:
                    this.getNextLocation();
                    break;

                case CHANGE:
                    break;
            }

            if (event && event.opreation && (event.opreation === PREVIOUS || event.opreation === NEXT)) {
                this.commonService.isNextOrPreviousClicked = true;
            }
        }
        catch (e) {
            this.commonService.appLogException(new Error('Exception in ChangeLocation() of Construction.formtemplate at time ' + new Date().toString() + '. Exception is : ' + e));
        }
    }
    getPrevLocation() {
        /**
         * new logic started
         * this logic is written to get prev locations
         */
        this.commonService.event = "drillUp";
        const queryParam = this.router.parseUrl(this.router.url).queryParams;
        var treeName = queryParam.tab;

        this.constructionservice.getAngularTreeEvent([treeName], TreeOperations.GetTreeInstance).
            first().
            subscribe((response: TreeDataEventResponseModel) => {
                let treeNames: Array<string> = response.treeName;
                let tree = response.treePayload;

                if (tree && treeNames.indexOf(treeName) > -1) {
                    var tempdata = this.commonService.previousLocationData;
                    var model: TreeModel = tree.treeModel;
                    model.focusPreviousNode();
                }

                let RouteParam = JSON.parse(JSON.stringify(queryParam));
                RouteParam.EntityID = this.commonService.previousLocationData.EntityId;

                if (treeName) {
                    this.constructionservice.sendDataToAngularTree([treeName], 'ActiveSelectedNode', RouteParam.EntityID);
                }
                this.navigateBasedOnQueryParams(RouteParam);
            });
    }
    getNextLocation() {
        try {
            this.commonService.event = "drillDown";
            const queryParam = this.router.parseUrl(this.router.url).queryParams;
            var treeName = queryParam.tab;
            this.constructionservice.getAngularTreeEvent([treeName], TreeOperations.GetTreeInstance)
                .first()
                .subscribe((response: TreeDataEventResponseModel) => {
                    let treeNames: Array<string> = response.treeName;
                    let tree = response.treePayload;
                    if (tree && treeNames.indexOf(treeName) > -1) {
                        var tempdata = this.commonService.previousLocationData;
                        var model: TreeModel = tree.treeModel;
                        model.focusNextNode();
                        //this logic is added to get next location
                        if (tempdata && tempdata.children && tempdata.children.length != 0) {
                            this.commonService.previousLocationData = tempdata.children[0];
                        }

                        if (this.commonService.previousLocationData) {
                            this.nodeId.push(this.commonService.previousLocationData.EntityId);
                        }

                        if (this.commonService.previousLocationData && this.commonService.previousLocationData.children && this.commonService.previousLocationData.children.length != 0) {
                            for (var a in this.nodeId) {
                                if (this.nodeId[a] == this.commonService.previousLocationData.EntityId) {
                                    model.isNodeFocused;
                                }
                            }
                        }

                        /**  next locatiion logic end  */
                        if (!this.commonService.previousLocationData) {
                            let data = this.getFirstChildLocation(model.getFirstRoot());
                            if (data) {
                                this.commonService.previousLocationData = data.data;
                            }
                        }

                        let RouteParam = JSON.parse(JSON.stringify(queryParam));
                        RouteParam.EntityID = this.commonService.previousLocationData.EntityId;
                        if (treeName) {
                            this.constructionservice.sendDataToAngularTree([treeName], 'ActiveSelectedNode', RouteParam.EntityID);
                        }

                        this.navigateBasedOnQueryParams(RouteParam);
                    }
                });
        }
        catch (e) {
            this.commonService.appLogException(new Error('Exception in getNextLocation() of Construction.formtemplate.component at time ' + new Date().toString() + '. Exception is : ' + e));
        }
    }
    getFirstChildLocation(dist) {
        try {
            var temp = dist.getFirstChild();
            if (temp) {
                this.getFirstChildLocation(temp);
            }
            if (temp.data.typeOfField == "Location") {
                return temp;
            }
        }
        catch (e) {
            console.warn("first location doesnt exist");
        }
    }
    navigateBasedOnQueryParams(RouteParam) {
        this.router.navigate([FabricRouter.CONSTRUCTION_FABRIC, 'Settings', RouteParam.EntityID, RouteParam.tab], { queryParams: RouteParam });
    }
    compUntilDestroyed(): any {
        return takeUntil(this.takeUntilDestroyObservables);
    }
    ngOnDestroy() {
        try {
            this.commonService.enableTableModeButton();
            this.ListsofData = [];
            this.commonService.globalfdcAdmin = false;
            // this.fDCService.mesurementstoggle = false;
            this.commonService.loadingBarAndSnackbarStatus("", "");
            // this.commonService.globalfdcAdmin = false;
            this.myForm = null;
            this.takeUntilDestroyObservables.next();
            this.takeUntilDestroyObservables.complete();

        } catch (e) {
            console.error('Exception in ngOnDestroy() of FdcAdminFormsComponent  at time ' + new Date().toString() + '. Exception is : ' + e);
            this.commonService.appLogException(new Error('Exception in ngOnDestroy() of FdcAdminFormsComponent  at time ' + new Date().toString() + '. Exception is : ' + e));
        }
    }
    canDeactivate(): Observable<boolean> {
        var response;
        // if(this.navButton != "expandcollapse" && !this.commonService.logoutbtn){
        // let message = 'Are you sure want to discard your changes ?';
        if (this.myForm.invalid && !this.commonService.logoutbtn) {
            //  let message = Mandatory_Message;
            //   this.openDialog(message, PopupOperation.AlertConfirm);
            response = this.dialogRef.componentInstance.emitResponse.map(res => { return true; });
            //   this.fDCService.fdcRecalculationFormStatus = true;
        }
        else {
            response = true;
        }
        return response;

    }
}