import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule1 } from '../../../shared/shared1.module';
import { ConstructionService } from '../services/construction.service';
import { ConstructionComponent } from '../components/construction/construction.component';
import { routing } from './construction.routing';



@NgModule({
  declarations: [
    ConstructionComponent
  ],
  imports: [
    CommonModule,
    SharedModule1,
    routing
  ],
  providers: [
    ConstructionService
  ]
})
export class ConstructionModule {

}
