import { RouterModule, Routes } from '@angular/router';
import { HOME, APP, CAPABILITY, MENU_ID, FORMS, SCHEMA, ROUTE, PARENT_ID, OPERATION_TYPE, ENITTY_ID } from 'ClientApp/app/components/common/Constants/AppConsts';
import { ConstructionComponent } from '../components/construction/construction.component';

const routes: Routes = [
  {
    path: '',
    component: ConstructionComponent,
    children: [
      // {
      //   path: 'addnewentity',
      //   loadChildren: () => import('../../../template/AddNewComponents/addnew.form.module').then(m => m.AddNewFormModule)
      // },
      // {
      //   path: 'Settings/:EntityID/:tab',
      //   loadChildren: () => import('../components/construction-form-template/construction-form.module').then(m => m.ConstructionFormModule)
      // },

      {
        path: 'Settings',
        loadChildren: () => import(`../components/construction-form-template/construction-form.module`).then(m => m.ConstructionFormModule)
      },
      {
        path: 'Settings/:EntityID/:tab',
        loadChildren: () => import(`../components/construction-form-template/construction-form.module`).then(m => m.ConstructionFormModule)
      },
      {
        path: 'Settings/:EntityID/:tab/forms/:schema',
        loadChildren: () => import(`../components/construction-form-template/construction-form.module`).then(m => m.ConstructionFormModule)
      },
      {
        path: 'Settings/:EntityID/:tab/forms/route/:parentId/:schema',
        loadChildren: () => import(`../components/construction-form-template/construction-form.module`).then(m => m.ConstructionFormModule)
      },
      {
        path: 'Settings/:EntityID/:tab/forms/route/:parentId/:schema/:operationType',
        loadChildren: () => import(`../components/construction-form-template/construction-form.module`).then(m => m.ConstructionFormModule)
      },
      {
        path: 'Settings/:EntityID/:tab/forms/route/:parentId/:schema/:operationType/:EntityID',
        loadChildren: () => import(`../components/construction-form-template/construction-form.module`).then(m => m.ConstructionFormModule)
      }


      ,
      {
        path: 'forms/:schema',
        loadChildren: () => import(`../components/construction-form-template/construction-form.module`).then(m => m.ConstructionFormModule)
      },
      {
        path: 'forms/route/:tab/:parentId/:schema',
        loadChildren: () => import(`../components/construction-form-template/construction-form.module`).then(m => m.ConstructionFormModule)
      },
      {
        path: 'forms/route/:tab/:parentId/:schema/:operationType',
        loadChildren: () => import(`../components/construction-form-template/construction-form.module`).then(m => m.ConstructionFormModule)
      },
      {
        path: 'forms/route/:tab/:parentId/:schema/:operationType/:EntityID',
        loadChildren: () => import(`../components/construction-form-template/construction-form.module`).then(m => m.ConstructionFormModule)
      },
      {
        path: "constructionadmin",
        loadChildren: () => import(`../components/constructionAdminForm/construction.adminforms.module`).then(m => m.ConstructionAdminFormsModule)
      }
    ]
  }
];

export const routing = RouterModule.forChild(routes);

