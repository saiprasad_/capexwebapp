export const ENABLEDEMODE = 'enabledmode';
export const TAB = 'tab';
export const LEFTNAV = 'leftnav';
export const ENTITYID = 'EntityId';

export const SECURITYGROUP_EDIT_PATH = 'editSecuritygroup';
