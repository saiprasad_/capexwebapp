import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { TreeModel } from '@circlon/angular-tree-component';
import { ALLENTITYTYPES, AppConsts, BIentitiesarray, DefaultPeopleSortingOrderArray, PEOPLEentitiesarray} from 'ClientApp/app/components/common/Constants/AppConsts';
import { CANT_MODIFY_GROUP, CONTEXT_MENU, DELETE_ROLE, DELETE_SECURITY_GROUP, DISCARD_CHANGES, DOESNT_HAVE_SCHEMA, REMOVE_ALL_SELECTED_ENTITIES, REMOVE_ENTITY1, REMOVE_PEOPLE_ENTITIES, REMOVE_ROLE, REMOVE_SELECTED_PEOPLE_ENTITIES, REMOVE_SELECTED_ROLES, SELECT_CAPABILITY, SELECT_ENTITY } from 'ClientApp/app/components/common/Constants/AttentionPopupMessage';
import { Observable, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { UserStateConstantsKeys } from '../../../../../webWorker/app-workers/commonstore/user/user.state.model';
import { WORKER_TOPIC } from '../../../../../webWorker/app-workers/shared/worker-topic.constants';
import { AppInsightsService } from '../../../common/app-insight/appinsight-service';
import { PopUpAlertComponent } from '../../../common/PopUpAlert/popup-alert.component';
import { EntityCommonStoreQuery } from '../../../common/state/entity.state.query';
import { AuthorizationStateConstantsKeys, SecurityGroupsEntitiesModel, SecurityGroupsFabricsModel } from '../../../fabrics/Security/components/store/authorization.state.model';
import { AuthorizationStateQuery } from '../../../fabrics/Security/components/store/authorization.state.query';
import { CommonService } from '../../../globals/CommonService';
import { WebWorkerService } from '../../../globals/components/ngx-web-worker/web-worker.service';
import { PopupOperation } from '../../../globals/Model/AlertConfig';
import { AngularTreeMessageModel, Datatypes, FABRICS, FabricsNames, FabricsPath, MessageKind, RightSideClickTreeEventModel, TreeDataEventResponseModel, TreeOperations, TreeTypeNames, TypeOfEntity } from '../../../globals/Model/CommonModel';
import { EntityTypes, ITreeConfig } from '../../../globals/Model/EntityTypes';
import { MessageModel, MessageType, Routing, IModelMessage, MessageDataTypes } from '../../../globals/Model/Message';
import { AppConfig } from '../../../globals/services/app.config';
import { MessageEntityTypes, DataTypes } from '../services/constants/message.constants';
import { ROLES, SECURITYGROUPS, SECURITY_GROUPS, SECURITY_ROOTNODE, PARENTTREENODE } from '../services/constants/module.constants';
import { TreeModelObject } from '../models/formobject.model';
import { IFormSchemaNameModel } from '../models/formschema-name.model';
import { RoleInfoModel } from '../models/role-info.model';
import { getNewUUID } from 'ClientApp/app/components/globals/helper.functions';
@Injectable()
export class AuthorizationService {
  public AuthorizationCommonObservable = new Subject();
  DestroyPermissionComponent = new Subject();
  DestroyEntityPermissionComponent = new Subject();
  public CreateGroupsComponent$ = new Subject();
  public DestroyGroupsComponent = new Subject();
  CreatePermissionsComponent$ = new Subject();
  public updateNewGroup = new Subject();
  DeleteRole$ = new Subject();
  public componentGenerateinFdc$ = new Subject();
  public removeUsersFromSecurityGroup$ = new Subject();
  public showSecurityGroupForm$ = new Subject();
  public removeEntitiesFromSecurityGroupForm$ = new Subject();
  public AddEntityInSecurityGroup = new Subject();
  public securityRoleData = [];
  public showRoleForm$ = new Subject();
  isTreeGraphToggleIdeas: boolean = true;
  listOfEntityForSecurityGroupForm = [];
  currentHoverData;
  permissionsExpandStatus: boolean = false;
  globalVarForUserPermission;
  groupValue = null;
  permissionValue = null;
  public EntityCapability;
  editmode: boolean = true;
  newGroupDivValues: any = { "headerName": "SecurityGroup", "placeHolder": "Group Name" };
  isRoleDelete = false;
  isPermissionTreeOpened: boolean = true;
  permVariable: string;
  isPermissionTree: boolean = false;
  allAvailableGroups: any;
  usersData: any = [];
  roleDetailsForUI: any = {};
  newArrayForSelectedGroups: any = [];
  newArrayForSelectedUsers: any = [];
  saaampDeleteCheckArray = [];
  callFromGeneral;
  entityName;
  securityRoleId = "";
  treeNodeName = "";
  capabilityName: string;
  activatedTab: string;
  fabricIdForSecurity: string;
  isRoleFormModified: boolean = false;
  roleInfoActive;
  roleEntityId;
  roleEntityName;
  roleCurrentForms;
  newRoleActive;
  //securitygroup variables
  //isSecurityGroupModified: boolean = false;
  securityGroupName: string;
  //securityGroupEntityId;
  lastOpenSecurityGroupEntityId;
  isLeftHeaderClick: boolean = false;
  //isNewSecurityGroupCreated: boolean = false;
  securityGroupsData = [];
  securityGroupIdToDelete;

  //mat dialog reference
  dialogRef;
  nodeToDelete;

  //Observables
  roleInfoObservable$: Subject<RoleInfoModel> = new Subject<RoleInfoModel>();
  securityRoleName;
  securityGroupId = "";
  jsonForWholeSecurityTree: any = {};
  securityAdminEnabledFabrics = [];
  isClickLocked: boolean = false;
  listtabActive = false;
  queryParams:any = {};
  /**
  *
  *lastOpenSecurityGroupId
  *
  */
  lastOpenSecurityGroupId = "";
  /**

  /**
  *
  *securityGroupData
  *
  *
  *namingPattern
  *
  */
  namingPattern = /([A-Z](?=[a-z]+)|[A-Z]+(?![a-z])|[& /])/g;

  /**
  *
  *sGroupParentNodeId
  *
  */
  sGroupParentNodeId;
  /**
 *
 *securityGroupFormData
 *
 */
  securityRoleOrGroupFormData: any;
  /**

  *
  *roleInfoActive
  *
  *currentForms
  *
  */
  public currentForms = [];


  /**
  *
  *getFormFieldData$
  *
  */
  public getFormFieldData$ = new Subject();
  /**
  *
  *newlyCreatedRole
  *
  */
  newlyCreatedRole: { "EntityId": string; "EntityName": string; "EntityType": string; "uid": string; };
  /**
  *
  *rolesTreeData
  *
  */
  rolesTreeData: any;
  /**
  *
  *treeNameForPermission
  *
  */
  // treeNameForPermission = [TreeTypeNames.peopleTreeForPermissions, TreeTypeNames.rolesTreeForPermissions, TreeTypeNames.securityGroupsTreeForPermissions];
  /**
  *
  *treeNameForAuthorization
  *
  */
  /**
  *
  *FormData
  **storing the received fields data *
  */
  FormData = [];

  fabricName;

  rightSideRoleArray;
  Activetab = "";
  ActiveMode = "roles";
  FabricTreeInstance: Map<string, object> = new Map<string, object>();

  public CustomerAppsettings = AppConfig.AppMainSettings;


  /**
   * This is the example to create Constructor for AuthorizationService
   * @param commonService Injects common service like common observables ,variables or any services can be shared among all components
   * @param router Injects angular routing service with which routing or navigation can be done
   *
   * @example example of constructor
   *
   * constructor(public commonService: CommonService, public router: Router){
   *  // Todo
   * }
   */
  constructor(public entityCommonStoreQuery: EntityCommonStoreQuery, public dialog: MatDialog, private logger: AppInsightsService, public authorizationStateQuery: AuthorizationStateQuery, private _webWorkerService: WebWorkerService, private http: HttpClient, public commonService: CommonService, public router: Router, private route: ActivatedRoute, public appConfig: AppConfig) {
    try {
      this.workerInit();
      this.initializeTreeWorkersObservables();
      this.ReadTreeData();
      this.commonService.AddNewSecurityRoleOrGroup$
        .pipe(filter((message) => this.getCurrentFabric() == FabricsNames.SECURITY))
        .subscribe((res) => {
          if (res == "Security Groups") {
            this.addNewSecurityGroup();
          }
          else if (res == "SECURITY ROLES") {
            this.AddNewRole();
          }
        },
          error => {
            console.error('Exception in addNewSecurityGroup$ observable of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + error.message);
            this.logger.logException(new Error('Exception in addNewSecurityGroup$ observable of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + error.message));
          });


      this.commonService.sendMessagesToFabricServices
        .pipe(filter((res: any) => (res.DataType == 'Authorization' || res.DataType == 'SecurityGroups' || res.DataType == 'DataModel' || res.DataType == "SUCCESS" || res.Fabric == 'SECURITY') && res.EntityType != "Error"))
        .subscribe((message: any) => {
          this.recieveMessageHandler(message);
        },
          error => {
            console.error('Exception in sendMessagesToFabricServices observable in AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + error.message);
            this.logger.logException(new Error('Exception in sendMessagesToFabricServices observable in AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + error.message));
          });


      this.commonService.sendDataFromAngularTreeToFabric
        .pipe(filter((message: any) => AppConsts.treeNameForAuthorization.includes(message.treeName[0]) && (this.commonService.getFabricNameByUrl(this.router.url) == FabricsNames.SECURITY)))
        .subscribe((msg: AngularTreeMessageModel) => {
          this.GetEventFromTree(msg);
        },
          error => {
            console.error('Exception in sendDataFromAngularTreeToFabric observable in AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + error.message);
            this.logger.logException(new Error('Exception in sendDataFromAngularTreeToFabric observable in AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + error.message));
          });

      this.commonService.ShowPermissionTree$
        .subscribe((msg: any) => {
          this.ShowPermissionTree(msg.head, msg.value);
        },
          error => {
            console.error('Exception in sendDataFromAngularTreeToFabric observable in AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + error.message);
            this.logger.logException(new Error('Exception in sendDataFromAngularTreeToFabric observable in AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + error.message));
          });


      this.commonService.rightTreeClick.filter((res: RightSideClickTreeEventModel) => this.filterFunc(res)).subscribe(head => {
        if (head && head != null)
          this.onRightHeaderClickEvent(head.head);
      });

      this.commonService.permissionListTab$.subscribe(info => {
        if (info && info != null) {
          this.onEnabdleDisableListTab(info);
        }
      });
    }
    catch (e) {
      console.error('Exception in constructor() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in constructor() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }
  filterFunc(rightClickModel: RightSideClickTreeEventModel) {
    if (rightClickModel.capability && rightClickModel.capability == FabricsNames.SECURITY) {
      return true;
    }
    return false;
  }


  onRightHeaderClickEvent(head: any) {

   // if (this.commonService.isFormModified || this.isSecurityGroupModified) {
    if (this.commonService.isFormModified) {

      let enabledMode = this.commonService.getQueryParamValue("enabledmode");
      if (enabledMode == 'roles' && head.id != 'rolesTreeForPermissions' && this.commonService.isFormModified) {

        this.openPopUpDialog(DISCARD_CHANGES, true, PopupOperation.AlertConfirm, 'Attention!', 'rolesrightclick', head);
        return;
      }
      else if (enabledMode == 'securitygroups' && head.id == 'rolesTreeForPermissions') {
        this.openPopUpDialog(DISCARD_CHANGES, true, PopupOperation.AlertConfirm, 'Attention!', 'securitygroupsrightclick', head);
        return;
      }
      else {
        this.publishToRightTreeClickEvent(head);
      }
    } else {
      this.publishToRightTreeClickEvent(head);

    }

  }
  publishToRightTreeClickEvent(head) {
    let modelObj = this.commonService.getRightClickEventModelObject(head);
    this.commonService.rightTreeClick.next(modelObj);
  }

  onEnabdleDisableListTab(info: any) {
    let isenableList = info['enableList'];
    this.listtabActive = isenableList;
    if (isenableList) {
      this.readListData(info);
    }
    else {
      let tab = info.queryParams && info.queryParams.tab ? info.queryParams.tab : null;
      if (tab)
        this.commonService.permissionEntitiesTab$.next({ tab: tab, fabirc: FabricsNames.SECURITY });
    }
  }

  readListData(info: any) {
    let fabric = this.returnListfabricName();
    let tab = info.queryParams && info.queryParams.tab ? info.queryParams.tab : null;
    let listStatus: boolean = this.commonService.checkListStateStatus("EntityType", "List", fabric);
    if (!listStatus) {
      this.ListTreeSendBackEnd(tab);
    }
    else {
      this.QueryListDataFromState(tab);
    }
  }

  returnListfabricName(tabvalue?) {
    let tab = tabvalue ? tabvalue : this.getRolesOrSecurityGroupstab();
    switch (tab) {
      case TreeTypeNames.BusinessIntelligence: {
        return FABRICS.BUSINESSINTELLIGENCE;
      }
    }
  }

  returnPayloadfabricName(tabvalue?) {
    let tab = tabvalue ? tabvalue : this.getRolesOrSecurityGroupstab();
    switch (tab) {
      case TreeTypeNames.BusinessIntelligence: {
        return FABRICS.BUSINESSINTELLIGENCE.toUpperCase();
      }
    }
  }

  /******************************************* STATE MANAGEMENT ********************************************************/

  getSecurityGroupsAndEntities() {
    try {
      let payload = {
        "EntityType": "UserSecurityGroupsEntities",
        "DataType": "UserSecurityGroupsEntities",
        "Fabric": this.getCurrentFabric(),
        "Info": "Admin",
        "CapabilityId": this.commonService.capabilityFabricId
      };

      this.commonService.sendMessageToServer(JSON.stringify(payload),
        "UserSecurityGroupsEntities",
        "UserSecurityGroupsEntities",
        "UserSecurityGroupsEntities",
        MessageKind.READ,
        Routing.OriginSession,
        "Details",
        this.getCurrentFabric());
    } catch (e) {
      console.error('Exception in getSecurityGroupsAndEntities of AuthorizationService  at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in getSecurityGroupsAndEntities of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  addSecurityGroupsEntitiesToState(message: MessageModel) {
    try {
      if (this.commonService.onlineOffline != false) {
        if (message.Payload != '' && message.Payload != '[]' && message.Payload != '{}') {
          let payload = JSON.parse(message.Payload);
          if (payload.length > 0) {
            payload.forEach(tempElement => {
              let stateData = [];
              let securityGroupArray = tempElement.SecurityGroups;
              let storeName = tempElement.Fabric;

              securityGroupArray.forEach((element) => {
                let tempModel: SecurityGroupsEntitiesModel = {
                  EntityId: element.EntityId,
                  EntityName: element.EntityName,
                  Entities: []
                };
                if (element.Entities && element.Entities.length > 0) {
                  element.Entities.forEach(innerElement => {
                    tempModel.Entities.push(innerElement.EntityId);
                  });
                }
                stateData.push(tempModel);
              });
              let securityGroupData: SecurityGroupsFabricsModel = {
                [storeName]: stateData
              };
              this.authorizationStateQuery.addToState(AuthorizationStateConstantsKeys.SecurityGroups, securityGroupData);
            });
          }
        }
      }
    } catch (e) {
      console.error('Exception in addSecurityGroupsEntitiesToState() method of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in addSecurityGroupsEntitiesToState() method of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  

  addOrUpdateSecurityGroupToAuthorizationState(securityGroupName: string, payload: any, fabric: string, messageKind: string) {
    try {
      let isExist = this.checkIfUserExistInSecurityGroup(payload);
      if (isExist) {
        if (payload.hasOwnProperty('Entities')) {
          let stateObj: SecurityGroupsEntitiesModel = {
            EntityId: payload.SecurityGroupId,
            EntityName: securityGroupName,
            Entities: payload.Entities
          };
          if (messageKind == MessageKind.CREATE) {
            this.authorizationStateQuery.addSecurityGroupToState(stateObj, fabric);
          }
          else {
            this.authorizationStateQuery.updateSecurityGroupToState(stateObj, fabric);
          }
        }
        if (payload.hasOwnProperty('RemovedEntities')) {
          payload.RemovedEntities.forEach(element => {
            this.authorizationStateQuery.deleteEntityFromSecurityGroup(payload.SecurityGroupId, fabric, element);
          });
        }
      }
    }
    catch (e) {
      console.error('Exception in addOrUpdateSecurityGroupToAuthorizationState() method of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in addOrUpdateSecurityGroupToAuthorizationState() method of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  checkIfUserExistInSecurityGroup(payload: any): boolean {
    try {
      var tree: any = this.FabricTreeInstance.get('peopleTreeForPermissions');
      let isUserExist = false;
      let currentUserId = this.commonService.currentUserId;

      if (payload.hasOwnProperty('PeopleCompanies') && tree) {
        payload['PeopleCompanies'].forEach(entityId => {
          if (entityId == currentUserId) {
            isUserExist = true;
          }
          else {
            let node = this.commonService.getNodeByEntityId(entityId, tree.treeModel);
            if (node) {
              if (node.hasChildren) {
                node.data.children.forEach(innerElement => {
                  if (innerElement.EntityId == currentUserId) {
                    isUserExist = true;
                    return;
                  }
                  else {
                    if (innerElement.hasOwnProperty('children')) {
                      innerElement.children.forEach(innerChildElement => {
                        if (innerChildElement.EntityId == currentUserId) {
                          isUserExist = true;
                          return;
                        }
                      });
                    }
                  }
                });
              }
            }
          }
        });
      }
      return isUserExist;
    }
    catch (e) {
      console.error('Exception in checkIfUserExistInSecurityGroup() method of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in checkIfUserExistInSecurityGroup() method of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  deleteSecurityGroupFromState(securityGroupId: string) {
    try {
      let fabric = this.getCurrentFabric();
      this.authorizationStateQuery.deleteSecurityGroup(securityGroupId, fabric);
    }
    catch (e) {
      console.error('Exception in deleteSecurityGroupFromState() method of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in deleteSecurityGroupFromState() method of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  /*********************************************************************************************************************/

  updateOtherSessionPermission(message) {
    var payLoad = JSON.parse(message.Payload);
    if (message.Type == "AddRole") {
      let sampObje = { "Forms": payLoad.RoleInfo[0].Forms };
      message.Payload = JSON.stringify(sampObje);
      message.MessageKind = MessageKind.UPDATE;
      this.commonService.publishedToCloseTheDeletedFrom$.next(message);
    }
    else if (message.Type == "DeleteRole") {
      let sampObje = { "RemovedForms": payLoad.RoleInfo[0].Forms };
      message.Payload = JSON.stringify(sampObje);
      message.MessageKind = MessageKind.UPDATE;
      this.commonService.publishedToCloseTheDeletedFrom$.next(message);
    }
  }

  async refreshUserHomePage() {
    try {
      this.appConfig.loadHead = [];
      this.commonService.isSubHeaderEnable = false;
      this.commonService.isUserMenuHeader = false;
      this.appConfig.GetCapabilities();
      this.commonService.currentRoutingSource = "V3 Home";
      this.commonService.closeAllPopupForms$.next();
      this.appConfig.AllHeaderRoutingList.forEach((head: any, index) => {
        if (head.id == "home") {
          head.routerLinkActive = true;
        }
        else
          head.routerLinkActive = false;
      });

      await this.UpdateUserPreferanceRead();

      this.routerNavigate(['/Home'])
    } catch (e) {
      console.error('Exception in refreshUserHomePage() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in refreshUserHomePage() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  UpdateUserPreferanceRead() {
    let userpreferanceCapability = { "Fabric": null, "UserPinnedTabs": null, "Theme": null };
    this.appConfig.AllHeaderRoutingListService.forEach((element: any) => {
      if (element.title)
        userpreferanceCapability[element.title] = null;
    })
    var userPayload = { "email": this.commonService.username, "cacheName": "TenantDetails", "Permission": userpreferanceCapability };
    this.commonService.sendMessageToServer(JSON.stringify(userPayload), EntityTypes.UserPreference, this.commonService.currentUserName, EntityTypes.UserPreference, MessageType.READ, Routing.OriginSession, '', FABRICS.COMMON.toUpperCase())
  }

  peopleHierarchyTree(peopleFormData) {
    var treeData = peopleFormData;
    var peopleDataForLeftTree = [];
    for (let index in treeData) {
      if (treeData[index].length != 0) {
        for (let nodeIndex in treeData[index]) {
          let nodeData = {};
          nodeData["EntityName"] = treeData[index][nodeIndex]["EntityName"];
          nodeData["EntityType"] = treeData[index][nodeIndex]["EntityType"];
          nodeData["EntityId"] = treeData[index][nodeIndex]["EntityId"];
          nodeData['children'] = [];
          if (treeData[index][nodeIndex]["Parent"] != undefined) {
            nodeData["Parent"] = { 'EntityId': treeData[index][nodeIndex]["Parent"][0]["EntityId"], 'id': treeData[index][nodeIndex]["Parent"][0]["EntityId"], 'entityType': treeData[index][nodeIndex]["Parent"][0]["EntityType"] };
          }
          peopleDataForLeftTree.push(nodeData);
        }
      }
    }
    if (peopleDataForLeftTree) {
      peopleDataForLeftTree = this.sortingTreeeForPermissions(peopleDataForLeftTree);
      this.flatToJsonPeople(peopleDataForLeftTree);
    }
  }
  workerInit(): void {
    this.commonService.worker_WS$
      .pipe(filter((res: any) => (res.DataType == 'Authorization' || res.DataType == 'SecurityGroups' || res.DataType == 'DataModel' || res.DataType == "SUCCESS" || res.Fabric == 'SECURITY') && res.EntityType != "Error"))
      .subscribe((response) => {
        try {
          this.recieveMessageHandler(response);
        } catch (e) {
          console.error(e);
        }
      });

  }
  recieveMessageHandler(message) {
    try {
      let tempPayloadData;
      switch (message.EntityID) {
        case EntityTypes.People:
          if (message.EntityType == EntityTypes.People) {
            let peopleFormData = JSON.parse(message['Payload']);
            if (peopleFormData) {
              this.peopleHierarchyTree(peopleFormData);
            }
          } else {
            this.commonService.sendMessageToServer(null, EntityTypes.People, EntityTypes.People, EntityTypes.People, MessageKind.READ, Routing.OriginSession, 'Details', FABRICS.PEOPLEANDCOMPANIES);
          }
          break;
        case 'formfields':
          var fieldJson = JSON.parse(message.Payload);
          if (fieldJson["fields"] != undefined && fieldJson["fields"].length > 0) {
            this.FormData = fieldJson["fields"][0];
            this.getFormFieldData$.next(message);
          }
          else {
            let formName = this.replaceFormName(this.currentForms[0].EntityName);
            this.currentForms.splice(0, 1);
            this.openPopUpDialog([DOESNT_HAVE_SCHEMA(formName)], false, PopupOperation.AlertConfirm, 'Attention!');
          }
          break;
        case 'roles':
          let roleData = JSON.parse(message.Payload);
          if (roleData.Roles.length == 0) {
            this.rightSideRoleArray = roleData.Roles;
            this.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissions, TreeOperations.createFullTree, roleData.Roles);
          } else {
            var Payload = roleData.Roles[0];
            this.securityRoleData = Payload.role;
            if (Payload.role.length > 0) {
              let obj: any = {};
              let roleArray = [];
              Payload.role.forEach(item => {
                obj = { "EntityName": item.EntityName, "SchemaId": item.SchemaId, "EntityType": EntityTypes.Role, "children": [], "EntityId": item.EntityId, "uid": item.uid };
                roleArray.push(obj);
              });
              roleArray = this.sortingTreeeForPermissions(roleArray);
              obj = { "EntityName": "Roles", "SchemaId": "SchemaId", "EntityType": "PermissionsListHeader", "children": roleArray, "EntityId": "Roles", "uid": "uid" };
              roleArray = [];
              roleArray.push(obj);
              this.rightSideRoleArray = roleArray;
              let mode = this.getRolesOrSecurityGroupsMode();
              if (mode == "roles") {
                this.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissions, TreeOperations.createFullTree, roleArray);
                this.expandTree(TreeTypeNames.rolesTreeForPermissionsRead);
                if (this.securityRoleId != undefined && this.securityRoleId != "") {
                  this.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissions, TreeOperations.ActiveSelectedNode, this.securityRoleId);
                }
              }
              else { //in Security mode
                this.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissionsRead, TreeOperations.createFullTree, roleArray);
                this.expandTree(TreeTypeNames.rolesTreeForPermissionsRead);
              }
            }
            else {
              this.rightSideRoleArray = Payload.role;
              this.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissions, TreeOperations.createFullTree, Payload.role);
            }
            let fabricid = this.commonService.getCapabilityidbasedontabforSecurity();
            var getmodel = this.getSecurityGroupsEntitiesModel(fabricid, "Roles", this.rightSideRoleArray);
            this.updateAurthState(getmodel, fabricid + "Roles");
          }
          break;


        case 'securitygroups':
          let tempSecurityGroupsData = [];
          this.sGroupParentNodeId = this.commonService.GetNewUUID();
          let rootNode: any = {};
          rootNode.EntityId = this.sGroupParentNodeId;
          rootNode.EntityType = 'SecurityRootNode';
          rootNode.children = [];
          rootNode.EntityName = 'Security Groups'

          tempPayloadData = JSON.parse(message['Payload']);
          tempPayloadData['SecurityGroups'] = this.sortingTreeeForPermissions(tempPayloadData['SecurityGroups']);
          this.securityGroupsData = JSON.parse(JSON.stringify(tempPayloadData['SecurityGroups']));

          var globalSecurityAdmin = {
            "EntityId": "AdminiStratorSecurityGroups", "EntityName": "Security Admin",
            "EntityType": "PermissionsListHeader", 'children': []
          };
          var capabilitySecurityAdmin = {
            "EntityId": "Security Groups", "EntityName": "Security Groups",
            "EntityType": "PermissionsListHeader", "children": []
          };
          var adminGroups = this.getAdmins(tempPayloadData['SecurityGroups']);
          globalSecurityAdmin.children = adminGroups;
          rootNode.children = [globalSecurityAdmin, capabilitySecurityAdmin]
          capabilitySecurityAdmin.children = tempPayloadData['SecurityGroups'];
          // rootNode.children = tempPayloadData['SecurityGroups'];
          tempSecurityGroupsData.push(rootNode);
          this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.createFullTree, rootNode.children);
          this.expandTree(TreeTypeNames.rolesTreeForPermissionsRead);
          let fabricid = this.commonService.getCapabilityidbasedontabforSecurity();
          var getmodel = this.getSecurityGroupsEntitiesModel(fabricid, "SecurityGroup", rootNode.children);
          this.updateAurthState(getmodel, fabricid + "SecurityGroup");
          if (this.securityGroupId) {
            this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.ActiveSelectedNode, this.securityGroupId);
          }
          break;

        case 'securitygroupinfo': {
          this.securityRoleOrGroupFormData = message;
          if (message && message.Payload) {
            var SgroupDetails = JSON.parse(message.Payload).SgroupDetails;
            if (SgroupDetails && SgroupDetails.length) {
              if (SgroupDetails[0].EntityId == this.getRolesOrSecurityGroupsEntityId()) {
                this.showSecurityGroupForm$.next(message);
              }
            }
          }
        }
          break;

        case 'roleinfo': {
          if (this.getCurrentFabric() == FabricsNames.SECURITY) {
            this.securityRoleOrGroupFormData = message['Payload'];
            this.showRoleForm$.next(message['Payload']);
          }
          else {
            message.EntityType = EntityTypes.SecurityGroup;
            this.updateOtherSessionPermission(message);
          }
        }
          break;
        case 'forms':
          let payLoad = JSON.parse(message.Payload);
          if (payLoad.forms.length > 0) {
            let i = 1;
            let obj: any = {};
            let samArray = [];
            payLoad.forms.forEach(item1 => {
              if (item1.FormType != undefined || item1.TypeOf != undefined) {
                obj = { "EntityName": this.replaceFormName(item1.Name), "SchemaId": item1.SchemaId, "EntityType": "PermissionsList", "FormType": item1.FormType, "TypeOf": item1.TypeOf, "children": [], "EntityId": i, "uid": item1.uid, "parent": { "id": "Forms", "text": "Forms" }, "ExistingParent": { "id": "Forms", "text": "Forms" } };
              } else {
                obj = { "EntityName": this.replaceFormName(item1.Name), "SchemaId": item1.SchemaId, "EntityType": "PermissionsList", "children": [], "EntityId": i, "uid": item1.uid, "parent": { "id": "Forms", "text": "Forms" }, "ExistingParent": { "id": "Forms", "text": "Forms" } };
              }
              samArray.push(obj);
              i++
            });
            obj = { "EntityName": "Forms", "SchemaId": "SchemaId", "EntityType": "PermissionsListHeader", "children": samArray, "EntityId": "Forms", "uid": "uid", "parent": { "id": "lkjhgfdsa098765", "text": "lkjhgfdsa098765" }, "ExistingParent": { "id": "lkjhgfdsa098765", "text": "lkjhgfdsa098765" } };
            samArray = [];
            samArray.push(obj);
            this.sendDataToAngularTree(this.treeNodeName, TreeOperations.createFullTree, samArray);
            let fabricid = this.commonService.getCapabilityidbasedontabforSecurity();
            var getmodel = this.getSecurityGroupsEntitiesModel(fabricid, "Forms", samArray);
            this.updateAurthState(getmodel, fabricid + "Forms");
            this.expandTree(this.treeNodeName);
          }
          else {
            this.sendDataToAngularTree(this.treeNodeName, TreeOperations.createFullTree, payLoad.forms);
          }
          this.isClickLocked = false;
          break;
      }

      if (message.DataType == "SUCCESS" && message.EntityType == "UserPreference")
        this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, "");

      this.sessionAndPermissionUpdate(message);
    }
    catch (e) {
      console.error('Exception in recieveMessageHandler() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in recieveMessageHandler() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  getSecurityGroupsEntitiesModel(EntityId, EntityName, Entities) {
    var SecurityGroupsEntitiesModel: SecurityGroupsEntitiesModel = {
      EntityId: EntityId,
      EntityName: EntityName,
      Entities: Entities
    };
    return SecurityGroupsEntitiesModel;
  }
  updateAurthState(SecurityGroupsEntitiesModel: SecurityGroupsEntitiesModel, fabric) {
  }

  getAurthState(fabric) {
    return this.authorizationStateQuery.getStateKey(fabric);
  }

  sessionAndPermissionUpdate(message) {
    try {
      if (message.Type == "UserAdmin") {
        if (message.EntityType == EntityTypes.SecurityGroup && message.CapabilityId == this.commonService.capabilityFabricId) {

          if (message.MessageKind == MessageKind.CREATE) {
            this.commonService.loadingBarAndSnackbarStatus("", "Entity Created");
            let payload = JSON.parse(message['Payload']);
            let createdSecurityGroup: any = {
              'InsertAtPosition': "Security Groups",
              'NodeData': {
                EntityId: payload.SecurityGroupId,
                EntityName: payload.SecurityGroupName,
                EntityType: EntityTypes.SecurityGroup,
              }
            }
            this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.AddNewNode, createdSecurityGroup);
            this.addOrUpdateSecurityGroupToAuthorizationState(payload.SecurityGroupName, payload, this.getCurrentFabric(), message.MessageKind);
          }

          if (message.MessageKind == MessageKind.DELETE) {
            this.commonService.loadingBarAndSnackbarStatus("", "Entity Deleted");
            this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.deleteNodeById, message.EntityID);
            if (this.securityGroupId == message.EntityID) {
              this.closeForm();
            }
            this.deleteSecurityGroupFromState(message.EntityID);
          }

          if (message.MessageKind == MessageKind.UPDATE) {
            this.commonService.loadingBarAndSnackbarStatus("", "Entity Updated");
            let payload = JSON.parse(message['Payload']);
            if (payload['SecurityGroupName']) {
              let nodeData = {
                "EntityId": message.EntityID,
                "EntityName": payload['SecurityGroupName'],
                "EntityType": EntityTypes.SecurityGroup,
                "uid": ""
              };
              this.securityGroupName = payload['SecurityGroupName'];
              let updatedNode = { "EntityId": message.EntityID, "NodeData": nodeData };
              this.sendDataToAngularTree(TreeTypeNames
                .securityGroupsTreeForPermissions, TreeOperations.updateNodeById, updatedNode);

              this.addOrUpdateSecurityGroupToAuthorizationState(payload.SecurityGroupName, payload, this.getCurrentFabric(), message.MessageKind);
            }
            if (message.EntityID == this.lastOpenSecurityGroupId) {
              this.securityRoleOrGroupFormData = message;
              this.showSecurityGroupForm$.next(message);
            }
          }

        } else if (message.EntityType == EntityTypes.Role) {
          if (message.MessageKind == MessageKind.CREATE) {
            this.commonService.loadingBarAndSnackbarStatus("", "Entity Created");
            let payload = JSON.parse(message['Payload']);
            if (payload.CapabilityId == this.fabricIdForSecurity) {
              let createRole: any = {
                'InsertAtPosition': "Roles",
                'NodeData': {
                  EntityId: payload.RoleId,
                  EntityName: payload.RoleName,
                  EntityType: EntityTypes.Role,
                }
              }
              this.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissions, TreeOperations.AddNewNode, createRole);
            }
          }
          else if (message.MessageKind == MessageKind.DELETE) {
            this.commonService.loadingBarAndSnackbarStatus("", "Entity Deleted");
            this.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissions, TreeOperations.deleteNodeById, message.EntityID);
            this.sendDataToAngularTree(TreeTypeNames.RoleTree, TreeOperations.deleteNodeById, message.EntityID);
            if (this.getCurrentFabric() == FabricsNames.SECURITY && message.EntityID == this.securityRoleId) {
              let queryParams = JSON.parse(JSON.stringify(this.route.snapshot.queryParams))
              queryParams = this.getQueryParams(queryParams.leftnav,queryParams.tab,queryParams.rightnav,queryParams.righttab,queryParams.enabledmode,queryParams.subtype,queryParams.leftnaventityid);
              this.routerNavigate(['Security'],queryParams)
            }
          }
          else if (message.MessageKind == MessageKind.UPDATE) {
            let payload = JSON.parse(message['Payload']);
            this.commonService.loadingBarAndSnackbarStatus("", "Entity Updated");
            let nodeData = {
              "EntityId": message.EntityID,
              "EntityName": payload['RoleInfo'][0]['EntityName'],
              "EntityType": EntityTypes.Role,
              "uid": ""
            };
            let updatedNode = { "EntityId": message.EntityID, "NodeData": nodeData };
            this.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissions, TreeOperations.updateNodeById, updatedNode);
            this.sendDataToAngularTree(TreeTypeNames.RoleTree, TreeOperations.updateNodeById, updatedNode);

            if (message.EntityID == this.securityRoleId) {
              this.securityRoleOrGroupFormData = message['Payload'];
              this.showRoleForm$.next(message['Payload']);
            }
          }
        }
      }
      else if (message.EntityType == EntityTypes.Role && !this.commonService.isCapbilityAdmin()) {
        if (message.MessageKind == MessageKind.UPDATE || message.MessageKind == MessageKind.DELETE) {
          this.commonService.loadingBarAndSnackbarStatus("", "Entity Updated");
          this.commonService.publishedToCloseTheDeletedFrom$.next(message);
        }
      }
      else if (message.EntityType == EntityTypes.SecurityGroup && message.EntityID != "roleinfo" && message.Routing != Routing.OriginSession) {
        let payload = JSON.parse(message.Payload);
        let isUserPermissionRefresh = false;
        if (message.MessageKind == MessageKind.UPDATE || message.MessageKind == MessageKind.CREATE) {
          payload.PeopleCompanies.forEach(element => {
            if (element == this.commonService.currentUserId) {
              isUserPermissionRefresh = true;
            }
          });
          payload.RemovedPeopleCompanies.forEach(element => {
            if (element["EntityId"] == this.commonService.currentUserId) {
              isUserPermissionRefresh = true;
            }
          });

          if (isUserPermissionRefresh) {
            isUserPermissionRefresh = false;
            this.refreshUserHomePage();
          }
          else if (payload.Roles.length > 0 || payload.RemovedRoles.length > 0 || payload.Entities.length > 0 || payload.RemovedEntities.length > 0) {
            this.commonService.publishedToCloseTheDeletedFrom$.next(message);
          }
        }
        else if (message.MessageKind == MessageKind.DELETE) {
          if (message.UserID == this.commonService.currentUserId) {
            this.refreshUserHomePage();
          }
        }
      }
      else if (message.EntityType == EntityTypes.PERSON) {
        if (message.MessageKind == MessageKind.CREATE && this.commonService.isCapbilityAdmin()) {
          let payload = JSON.parse(message.Payload);
          let entityInfoJson = JSON.parse(payload.EntityInfoJson);
          payload["EntityName"] = entityInfoJson.EntityName.Value;
          payload["Icon"] = "";
          payload["children"] = [];
          if (this.commonService.sideHeaderView == 'Permission') {
            let InsertAtPosition;
            if (payload.Parent == null || payload.Parent.id == '' || payload.Parent.id == null || payload.Parent.id == 'lkjhgfdsa098765') {
              InsertAtPosition = "althingTreeRoot";
            }
            else {
              InsertAtPosition = payload.Parent["id"];
            }
            let nodeData = { "InsertAtPosition": InsertAtPosition, "NodeData": payload };
            this.sendDataToAngularTree(TreeTypeNames.peopleTreeForPermissions, TreeOperations.AddNewNode, nodeData);
          }
        }
        else if (message.MessageKind == MessageKind.DELETE && this.commonService.isCapbilityAdmin()) {
          this.sendDataToAngularTree(TreeTypeNames.peopleTreeForPermissions, TreeOperations.deleteNodeById, message.EntityID);
        }
        else if (message.MessageKind == MessageKind.DELETE && !this.commonService.isCapbilityAdmin()) {
          this.appConfig.AllHeaderRoutingList.forEach((head: any, index) => {
            if (head.id == "home") {
              head.routerLinkActive = true;
            }
            else {
              head.routerLinkActive = false;
              this.appConfig.AllHeaderRoutingList.splice(1, index);
            }
            this.commonService.isclickoncreatelist = false;
          });
          this.commonService.currentRoutingSource = "V3 Home";
          this.routerNavigate(['/Home']);
        }
      }
      if (message.DataType == "SUCCESS" && (message.EntityType == EntityTypes.SecurityGroup || message.EntityType == EntityTypes.Role) && this.commonService.isCapbilityAdmin()) {
        if (message.MessageKind == MessageKind.DELETE) {
          this.commonService.loadingBarAndSnackbarStatus("", "Entity Deleted");
        }
        else if (message.MessageKind == MessageKind.UPDATE) {
          this.commonService.loadingBarAndSnackbarStatus("", "Entity Updated");
        }
        else if (message.MessageKind == MessageKind.CREATE) {
          this.commonService.loadingBarAndSnackbarStatus("", "Entity Created");
        }
      }
    }
    catch (e) {
      console.error('Exception in sessionAndPermissionUpdate() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in sessionAndPermissionUpdate() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  promise
  flatToJsonPeople(d) {
    try {
      var self = this;
      var alltreeData = d;

      this._webWorkerService.terminate(this.promise);
      this.promise = this._webWorkerService.run(this.flatToHierarchyPeople, alltreeData);
      this.promise.then(function (response) {
        self.sendDataToAngularTree(TreeTypeNames.peopleTreeForPermissions, TreeOperations.createFullTree, response);
        var getmodel = this.getSecurityGroupsEntitiesModel(TreeTypeNames.peopleTreeForPermissions, TreeTypeNames.peopleTreeForPermissions, response);
        self.updateAurthState(getmodel, TreeTypeNames.peopleTreeForPermissions + TreeTypeNames.peopleTreeForPermissions);
      }).catch((e) => { console.log("Web Worker Exception : " + e) });
    } catch (e) {
      console.error(e);
    }
  }

  flatToHierarchyPeople(list) {
    let roots = [];
    let all = {};
    if (!list.length) {
      return;
    }
    list.forEach(Element => {
      all[Element.EntityId] = Element;
    });
    Object.keys(all).forEach(Element => {
      let item = all[Element];
      if (item.Parent == null) {
        item.Parent = { id: "", text: "" }
      }
      if (item.Parent.id == "" || item.Parent.id == null) {
        roots.push(item);
      }
      else if (item.Parent.id in all) {
        let p = all[item.Parent.id];
        if (!p.children) {
          p.children = [];
        }
        p.children.push(item);
      }
    });
    return roots;
  }




  getAdmins(securityGroups) {
    var admins = [];
    for (var i = 0; i < securityGroups.length; i++) {
      var group = securityGroups[i]["EntityName"];
      if (group == "Admin" || group == "FDCAdmin" || group == "PiggingAdmin" || group == "AdminiStratorSecurityGroup" || group == "AssetAdmin" || group == "OperationalFormsAdmin" || group == "BIAdmin") {
        //var node=securityGroups.splice(i,1);
        admins.push(securityGroups[i]);
        securityGroups.splice(i, 1);
        i--;
      }
    }
    return admins;
  }
  /**
    *
    * Used to sort tree data for permission trees, 
    * @example example of sortingTreeeForPermissions() method
    * @param {array } treedata: is array of node which will used to generate tree.
    * 
    *  sortingTreeeForPermissions(){
    *     //todo
    *    }
    */
  sortingTreeeForPermissions(treedata) {
    try {
      var treeData = treedata.sort((node1, node2) => {
        var name1 = node1['EntityName'] ? node1['EntityName'].toLowerCase() : "";
        var name2 = node2['EntityName'] ? node2['EntityName'].toLowerCase() : "";
        if (name1 > name2) { return 1; }
        if (name1 < name2) { return -1; }
        return 0;
      });

      return treeData;
    }
    catch (e) {
      console.error('Exception in sortingTreeeForPermissions() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in sortingTreeeForPermissions() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  sortingTreeeBasedonEntityTypeForPermissions(treedata) {
    try {
      let SortArray = [];
      DefaultPeopleSortingOrderArray.map((item) => {
        treedata.forEach(element => {
          if (element.EntityType == item || element.Type == item) {
            SortArray.push(element);
          }
        });
      });
      return SortArray;
    }
    catch (e) {
      console.error('Exception in sortingTreeeForPermissions() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in sortingTreeeForPermissions() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }


  /**
  *
  * Used to create a new Security group, It opens an empty Security Groups form, by routing to Security Groups component.
  * @example example of addNewSecurityGroup() method
  *
  *  addNewSecurityGroup(){
  *     //todo
  *    }
  */
  addNewSecurityGroup() {
    try {
      if (this.commonService.treeIndexLabelRight == 'Security Groups') {
        let activeLeftNavEntityId = this.route.snapshot.queryParams.leftnaventityid;
        let allowCreate = (this.getRolesOrSecurityGroupstab() != FABRICS.CONSTRUCTION) || (this.getRolesOrSecurityGroupstab() == FABRICS.CONSTRUCTION && activeLeftNavEntityId) ? true : false;
        //if (!this.isSecurityGroupModified && allowCreate) {
        if (allowCreate) {
          this.securityGroupName = "";
          this.listOfEntityForSecurityGroupForm = [];
          this.securityGroupId = "";
          this.lastOpenSecurityGroupId = "";

          let url = this.getDefaultUrl();
          this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.deactivateSelectedNode, "");

          setTimeout(() => {
            let queryParams = JSON.parse(JSON.stringify(this.route.snapshot.queryParams))
            queryParams = this.getQueryParams(queryParams.leftnav,queryParams.tab,queryParams.rightnav,queryParams.righttab,queryParams.enabledmode,null,queryParams.leftnaventityid);
            let newSecurityGroupId = getNewUUID();
            let url = [FABRICS.SECURITY,EntityTypes.SECURITYGROUP,'New',newSecurityGroupId]                  
            if(queryParams.leftnaventityid){
              url = [FABRICS.SECURITY,EntityTypes.SECURITYGROUP,queryParams.leftnaventityid,'New',newSecurityGroupId]                  
            }
            this.routerNavigate(url, queryParams);          
          }, 1);
        }
        else if(!allowCreate){
          this.openPopUpDialog([SELECT_ENTITY], false, PopupOperation.PermissionAlertConfirm, 'Attention!', 'newGroupSide');
        }
        else {
          this.openPopUpDialog([DISCARD_CHANGES], true, PopupOperation.PermissionAlertConfirm, 'Attention!', 'newGroupSide');
        }
      }
      // else{
      //   this.openPopUpDialog(new Array("Creating " + this.commonService.treeIndexLabelRight + " type entity from here is not allowed.!!!"), false, PopupOperation.AlertConfirm, 'Attention!');
      // }
    }
    catch (e) {
      console.error('Exception in addNewSecurityGroup() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in addNewSecurityGroup() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  /**
   * AddNewRole() method called on clicking the plus icon of permission right side bar to display new role form.
   */
  AddNewRole() {
    try {
      if (this.getCurrentFabric() == FabricsNames.SECURITY && this.commonService.isLeftHeaderActive) {
        if (!this.commonService.isFormModified) {
          this.roleInfoActive = false;
          this.currentForms = [];
          this.securityRoleId = "";
          //this.securityRoleName = "";
          let url = FABRICS.SECURITY;
          this.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissions, TreeOperations.deactivateSelectedNode, "");

          setTimeout(() => {
            this.newRoleActive = true;
            let queryParams = JSON.parse(JSON.stringify(this.route.snapshot.queryParams))
            queryParams = this.getQueryParams(queryParams.leftnav,queryParams.tab,queryParams.rightnav,queryParams.righttab,queryParams.enabledmode,queryParams.subtype,queryParams.leftnaventityid);
            this.routerNavigate([url + '/NewRole'],  queryParams);
          }, 1);
        }
        else {
          this.openPopUpDialog([DISCARD_CHANGES], true, PopupOperation.PermissionAlertConfirm, 'Attention!', 'newRoleSide');
        }
      } else {
        this.openPopUpDialog([SELECT_CAPABILITY], false, PopupOperation.AlertConfirm, 'Attention!');
      }
    }
    catch (e) {
      console.error('Exception in AddNewRole() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in AddNewRole() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  /*  *  * Used to open a confirmation dialog pop up to confirm with user about the current action.
    * @param {object } message: is array of string which will shown in confirmation pop up as message.
    * @example example of openPopUpDialog(message) method
    *  openPopUpDialog(message){
    *
    *  //todo
    *
    *  }
    * */
  openPopUpDialog(messageToshow, isSubmit, operationType, headerType, callFrom?, node?): void {
    try {
      let config = {
        header: headerType,
        isSubmit: isSubmit,
        content: [messageToshow],
        subContent: [],
        operation: operationType,
        callfrom: callFrom
      };

      let matConfig = new MatDialogConfig();
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;

      this.dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
      this.dialogRef.componentInstance.emitResponse.subscribe((data: any) => {
        if (data.operation && data.operation == "PermissionAlertConfirm") {
          switch (data.callfrom) {
            case 'roleSide': {
              // this.commonService.isFormModified = false;
              // if (this.securityRoleId == node) {
              //   this.showRoleForm$.next(this.securityRoleOrGroupFormData);
              // } else {
              //   this.getRoleDetails(node);
              // }
              break;
            }
            case 'groupSide': {

              break;
            }
            case 'newGroupSide': {
              this.addNewSecurityGroup();
              break;
            }
            case 'newRoleSide': {
              this.commonService.isFormModified = false;
              this.AddNewRole();
              break;
            }
          }
        }
        else if (callFrom == 'rolesrightclick') {
           // this.commonService.isFormModified = false;
           // this.publishToRightTreeClickEvent(node);
        }
        else if (callFrom == 'securitygroupsrightclick') {
          // this.isNewSecurityGroupCreated = false;
          // this.isSecurityGroupModified = false; 
          // this.publishToRightTreeClickEvent(node);
        }
        else if (data) {
          if (this.nodeToDelete) {
            this.onConfirmDelete(this.nodeToDelete);
          }
        }
      });    

    }
    catch (ex) {
      console.error('Exception in openPopUpDialog() of authorization service at time ' + new Date().toString() + '. Exception is : ' + ex);
    }
  }
  closeLoaderStatus(loadingStatus: string, status?: string, duration?) {
    this.commonService.loadingBarAndSnackbarStatus(loadingStatus, status, duration);
  }
  /**
  *
  * Used to handle the even occures on angular tree like click event on node.
  * @param {object } data: is event data which comes after an operation on angular tree.
  * @example example of GetEventFromTree(data) method
  *
  *  GetEventFromTree(data){
  *     //todo
  *    }
  */
  GetEventFromTree(data: AngularTreeMessageModel) {
    let treeName = data.treeName[0];
    switch (treeName) {
      case TreeTypeNames.rolesTreeForPermissions:
        {
          switch (data.treeOperationType) {
            case TreeOperations.NodeOnClick:
              {
                this.singleClick(data);
                break;
              }
            case TreeOperations.NodeOnContextClickBeforeInit: {
                var name = data.treePayload.data.EntityName;
                this.createContextMenu(data);
                break;
              }
            case TreeOperations.NodeContextOptionClicked: {
                this.nodeToDelete = data;
                this.openPopUpDialog([DELETE_ROLE(data.treePayload.nodeData.data.EntityName)], true, PopupOperation.DeleteAll, " Delete " + data.treePayload.nodeData.data.EntityName);
                break;
              }          
            case TreeOperations.entityFilter:{
              this.sendDataToAngularTree(data.treeName[0], data.treeOperationType, data.treePayload);
              break;
            }
          }
          break;
        }
      case TreeTypeNames.securityGroupsTreeForPermissions:
        {
          switch (data.treeOperationType) {
            case TreeOperations.NodeOnClick:
              {
                this.singleClick(data);
                break;
              }
            case TreeOperations.NodeOnContextClickBeforeInit: {
              var name = data.treePayload.data.EntityName;
              let entityType = data.treePayload.data.EntityType;
              if(entityType == EntityTypes.SECURITYGROUP){
               this.createContextMenu(data);
              }
              break;
            }
            case TreeOperations.NodeContextOptionClicked: {
              this.nodeToDelete = data;
               this.openPopUpDialog([DELETE_SECURITY_GROUP(data.treePayload.nodeData.data.EntityName)], true, PopupOperation.DeleteAll, " Delete " + data.treePayload.nodeData.data.EntityName);
              break;
            }
            case TreeOperations.entityFilter:{
              this.sendDataToAngularTree(data.treeName[0], data.treeOperationType, data.treePayload);
              break;
            }
          }
          break;
        }
      case TreeTypeNames.SecurityGroupsPeopleTree:
      case TreeTypeNames.SecurityGroupsEntitiesTree: {
        switch (data.treeOperationType) {
          case TreeOperations.NodeOnContextClickBeforeInit: {
            this.createContextMenu(data);
            break;
          }
          case TreeOperations.NodeContextOptionClicked: {
            this.nodeToDelete = data;
            this.onConfirmDelete(data);
            break;
          }
          case TreeOperations.entityFilter:
            this.sendDataToAngularTree(data.treeName[0], data.treeOperationType, data.treePayload);
            break;
        }
        break;
      }
      case TreeTypeNames.peopleTreeForPermissions: {
        switch (data.treeOperationType) {
          case TreeOperations.entityFilter:
            this.sendDataToAngularTree(data.treeName[0], data.treeOperationType, data.treePayload);
            break;
        }
        break;
      }
      case FabricsNames.SECURITY:
      case TreeTypeNames.BusinessIntelligence:
          {
            switch (data.treeOperationType) {
              case TreeOperations.NodeOnContextClickBeforeInit: {
                this.createContextMenu(data);
                break;
              }
              case TreeOperations.NodeContextOptionClicked: {
                this.AddEntityInSecurityGroup.next(data);
                break;
              }
            }
            break;
          }
      case TreeTypeNames.Construction:
              {
                switch (data.treeOperationType) {
                  case TreeOperations.NodeOnClick: {
                    this.queryParams.leftnaventityid = data.treePayload.data.EntityId;
                    let queryParams = JSON.parse(JSON.stringify(this.route.snapshot.queryParams))
                    queryParams = this.getQueryParams(queryParams.leftnav,queryParams.tab,queryParams.rightnav,queryParams.righttab,queryParams.enabledmode,null,data.treePayload.data.EntityId);
                    this.routerNavigate([FABRICS.SECURITY], queryParams );
                    this.getSecuritygroups(null);
                    break;
                  }
                }
                break;
      }
    }
  }

  RemoveFilter(treeName, nodeid?) {
    this.commonService.searchActive = false;
    this.sendDataToAngularTree(treeName, TreeOperations.RemoveFilter, '');
    var configdata: ITreeConfig = { "treeExpandAll": treeName == TreeTypeNames.securityGroupsTreeForPermissions ? true : false };
    this.sendDataToAngularTree(treeName, TreeOperations.treeConfig, configdata);
    setTimeout(() => {
      if (nodeid)
        this.sendDataToAngularTree(treeName, TreeOperations.ActiveSelectedNode, nodeid);
    }, 0);
  }
  TreeOnFirstTimeLoad(treename: string, tree: object) {
    this.FabricTreeInstance.set(treename, tree);
  }


  onConfirmDelete(data) {
    let treeName = data.treeName[0];
    if (data.treePayload.nodeData.data.EntityId == this.roleEntityId) {
      this.newRoleActive = false;
      this.roleInfoActive = false;
      this.commonService.isFormModified = false;
    }
    if (treeName == TreeTypeNames.securityGroupsTreeForPermissions) {
      this.onDeleteSecurityGroup(data);
    }
    else if ([TreeTypeNames.SecurityGroupsEntitiesTree,TreeTypeNames.SecurityGroupsPeopleTree].indexOf(treeName) >= 0) {
      this.removeEntitiesFromSecurityGroupForm$.next(data);
    }
    else if (treeName == TreeTypeNames.rolesTreeForPermissions && data.treePayload.nodeData.data.EntityType == EntityTypes.Role) {
      try {
        switch (treeName) {
          case TreeTypeNames.rolesTreeForPermissions: {
            this.sendDataToAngularTree(treeName, TreeOperations.deleteNodeById, data.treePayload.nodeData.data.EntityId);
            let payload = { RoleId: data.treePayload.nodeData.data.EntityId,
                            RoleName: data.treePayload.nodeData.data.EntityName,
                            CapabilityId: this.commonService.getCapabilityidbasedontabforSecurity(),
                            Capability: this.commonService.getCapabilityNamebasedontabforSecurity()
                          };
            let imessageModel: IModelMessage = this.commonService.getMessageWrapper( {DataType : DataTypes.FORMSCHEMAROLES,
              EntityType : MessageEntityTypes.ROLESCHEMAPERMISSIONS,
              MessageKind : MessageKind.DELETE,
              Payload : JSON.stringify(payload)});
            this.commonService.postDataToBackend(imessageModel, AppConsts.DELETING).subscribe(response => {
              console.log(response);
              this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, AppConsts.DELETED, AppConsts.LOADER_MESSAGE_DURATION);
              this.securityRoleData.forEach(role => {
                if (role.EntityName == data.treePayload.nodeData.data.EntityName) {
                  let index = this.securityRoleData.indexOf(role);
                  this.securityRoleData.splice(index, 1);
                }
              });
            });
            break;
          }
        }
      }
      catch (e) {
        console.error("Exception in onConfirmDelete in authorizationservice at time:" + new Date().toString() + ".Exception is :" + e);
      }
    }
    this.nodeToDelete = undefined;
  }
  onDeleteSecurityGroup(data) {
    let nodeData = data.treePayload.nodeData.data;
    this.securityGroupIdToDelete = nodeData.EntityId;
    this.securityGroupsData = this.securityGroupsData.filter(item => item.EntityId != nodeData.EntityId);
    //If form Open close the form
    if (nodeData.EntityId == this.lastOpenSecurityGroupEntityId) {
      //this.isSecurityGroupModified = false;
      this.closeForm();
    }
    let payload = {
      "Capability": {
        "CapabilityId": this.commonService.getCapabilityidbasedontabforSecurity(),
        "CapabilityName": this.commonService.getCapabilityNamebasedontabforSecurity()
      },
      'SecurityGroupId': nodeData.EntityId,
    };
    let imessageModel: IModelMessage = this.commonService.getMessageWrapper({DataType : MessageDataTypes.SECURITYGROUPS,
      EntityType : EntityTypes.SecurityGroup,
      MessageKind : MessageKind.DELETE,
      EntityID : nodeData.EntityId,
      Payload : JSON.stringify(payload)});
    this.commonService.postDataToBackend(imessageModel,AppConsts.DELETING).subscribe(data => {
      this.logger.logEvent('Deleted');
      this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, AppConsts.DELETED, AppConsts.LOADER_MESSAGE_DURATION);
    });
    this.sendDataToAngularTree(data.treeName[0], TreeOperations.deleteNodeById, nodeData.EntityId);

  }

  nodeOptionClickedForPermissionTrees(data) {
    try {
      let treeName = data.treeName[0];
      let selectedTreeNodes = [];
      let selectedMultiNode = false;
      let messageForSingleNodeSelect = "";
      let messageForMultiNodeSelect = "";
      if (treeName == TreeTypeNames.SecurityGroupsPeopleTree) {
        messageForSingleNodeSelect = REMOVE_SELECTED_PEOPLE_ENTITIES;
        messageForMultiNodeSelect = REMOVE_PEOPLE_ENTITIES(data.treePayload.nodeData.data.EntityName);
      }
      else if (treeName == TreeTypeNames.SecurityGroupsEntitiesTree) {
        messageForSingleNodeSelect = REMOVE_ALL_SELECTED_ENTITIES;
        messageForMultiNodeSelect = REMOVE_ENTITY1(data.treePayload.nodeData.data.EntityName);
      }
      else {
        messageForSingleNodeSelect = REMOVE_SELECTED_ROLES;
        messageForMultiNodeSelect = REMOVE_ROLE(data.treePayload.nodeData.data.EntityName);
      }

      this.nodeToDelete = data;
      for (var key in data.treePayload.nodeData.treeModel.activeNodeIds) {
        if (data.treePayload.nodeData.treeModel.activeNodeIds[key]) {
          selectedTreeNodes.push(key);
        }
        if (selectedTreeNodes.length > 1) {
          selectedMultiNode = true;
          break;
        }
      }
      selectedTreeNodes = [];
      if (selectedMultiNode) {
        this.openPopUpDialog([messageForSingleNodeSelect], true, PopupOperation.AlertConfirm, 'Attention!');
      }
      else {
        this.openPopUpDialog([messageForMultiNodeSelect], true, PopupOperation.AlertConfirm, 'Attention!');
      }
    }
    catch (e) {
      console.error("Exception in onConfirmDelete in authorizationservice at time:" + new Date().toString() + ".Exception is :" + e);
    }
  }
  /**
  *
  * Used to handle the right click event on tree node.
  * @param {object } data: is event data which comes after an click event on angular tree.
  * @example example of onContextClick(data) method
  *
  *  onContextClick(data){
  *     //todo
  *    }
  */
  onContextClick(data) {
    try {
      //let url = this.getDefaultUrl() + '/securitygroups/edit';
      let fabName = this.commonService.getCapabilityNamebasedontabforSecurity();
      let nodeData = data.treePayload.nodeData.data;
      this.securityGroupIdToDelete = nodeData.EntityId;
      let payload = {
        "CapabilityId": this.commonService.getCapabilityidbasedontabforSecurity(),
        "SecurityGroupId": nodeData.EntityId,
        "SecurityGroupUid": nodeData.uid,
        "capabilityName": fabName.toUpperCase()
      };
      if (nodeData.EntityId == this.lastOpenSecurityGroupId) {
        //this.isSecurityGroupModified = false;
        this.closeForm();
      }
      this.commonService.sendMessageToServer(JSON.stringify(payload), 'Authorization', nodeData.EntityId, EntityTypes.SecurityGroup, MessageKind.DELETE, Routing.AllFrontEndButOrigin, null, this.getCurrentFabric());
      this.securityGroupsData = this.securityGroupsData.filter(item => item.EntityId != nodeData.EntityId);
      this.deleteSecurityGroupFromState(nodeData.EntityId);

      this.sendDataToAngularTree(data.treeName[0], TreeOperations.deleteNodeById, nodeData.EntityId);

    }
    catch (e) {
      console.error('exception in onContextClick() of AuthorizationService at time ' + new Date().toString() + '. exception is : ' + e);
      this.logger.logException(new Error('Exception in onContextClick() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  /**
*
* Used to close the currently new/open Security Groups form.
* @example example of closeForm() method
*
*  closeForm(){
*     //todo
*    }
*/
  closeForm() {
    let url = FABRICS.SECURITY;
    let queryParams = JSON.parse(JSON.stringify(this.route.snapshot.queryParams))
    queryParams = this.getQueryParams(queryParams.leftnav,queryParams.tab,queryParams.rightnav,queryParams.righttab,queryParams.enabledmode,queryParams.subtype,queryParams.leftnaventityid);
    this.routerNavigate([url], queryParams);
  }

  /**
  *
  * Used to handle the right click event to show context menu on tree node.
  * @param {object } data: is event data which comes after an click event on angular tree.
  * @example example of createContextMenu(data) method
  *
  *  createContextMenu(data){
  *     //todo
  *    }
  */
  createContextMenu(data) {
    try {
      let treeName = data.treeName[0];
      let contextMenuOption = [];
      let showDeleteHierarchyOption = false;
      if(data.treePayload && data.treePayload.treeModel){
        for (var key in data.treePayload.treeModel.activeNodeIds) {
          if (data.treePayload.treeModel.activeNodeIds[key]) {
            let node = data.treePayload.treeModel.getNodeById(key);
            if(node && node.children &&  node.children.length){
               showDeleteHierarchyOption = true ;
               break;
            }
          }
        }
      }
      switch (treeName) {
        case TreeTypeNames.SecurityGroupsEntitiesTree:
        case TreeTypeNames.SecurityGroupsPeopleTree:
          {
            contextMenuOption = [{
              displayName: CONTEXT_MENU,
              children: [
                {
                  displayName: 'Remove Entity',
                  children: []
                }
              ]
            }];
            if((data.treePayload && data.treePayload.data && data.treePayload.data.children && data.treePayload.data.children.length) || showDeleteHierarchyOption){
              contextMenuOption[0].children.push( {
                displayName: 'Remove Hierarchy',
                children: []
              });
            }
            break;
          }
        case TreeTypeNames.securityGroupsTreeForPermissions: {
          contextMenuOption = [{
            displayName: CONTEXT_MENU,
            children: [
              {
                displayName: 'Delete',
                children: []
              }
            ]
          }];
          break;
        }
        case TreeTypeNames.rolesTreeForPermissions: {
          if (data.treePayload.data.EntityType == "Role") {
            contextMenuOption = [{
              displayName: 'Roles Context Menu',
              children: [
                {
                  displayName: 'Delete',
                  children: []
                }
              ]
            }];
            break;
          }
        }
        case TreeTypeNames.Construction: {
            if (ALLENTITYTYPES.includes(data.treePayload.data.EntityType)  && (this.securityGroupId)) {
              contextMenuOption = [{
                displayName: 'Entities Context Menu',
                children: [
                  {
                    displayName: 'Add To Security Group',
                    children: []
                  }
                ]
              }];
            }
            break;
          }
      }
      this.sendDataToAngularTree(treeName, TreeOperations.treeContextMenuOption, contextMenuOption);
    }
    catch (e) {
      this.logger.logEvent('exception in createContextMenu() of AuthorizationService at time ' + new Date().toString() + '. exception is : ' + e);
    }
  }

  /**
  *
  * Used to handle the single click event on tree node.
  * @param {object } data: is event data which comes after an click event on angular tree.
  * @example example of singleClick(data) method
  *
  *  singleClick(data){
  *     //todo
  *    }
  */
  singleClick(data) {
    try {
      let activesecurityId = this.lastOpenSecurityGroupId ? this.lastOpenSecurityGroupId : this.securityRoleId;
      switch (data.treeName[0]) {
        case TreeTypeNames.rolesTreeForPermissions: {
          if (data.treePayload.data.EntityType == EntityTypes.Role && data.treePayload.data.EntityId != activesecurityId) {
            //this.getRoleDetails(data.treePayload.data.EntityId, data.treePayload.data.EntityName);
            this.securityRoleId = data.treePayload.data.EntityId;
            this.getRoleDetails(data.treePayload.data.EntityName);
          }
          else if (data.treePayload.data.EntityType == "PermissionsListHeader") {
            this.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissions, TreeOperations.deactivateSelectedNode, data.treePayload.data.EntityId);
            this.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissions, TreeOperations.ActiveSelectedNode, activesecurityId);
          }
          break;
        }
        case TreeTypeNames.securityGroupsTreeForPermissions:
          if (data.treePayload.data.EntityType == EntityTypes.SECURITYGROUP && data.treePayload.data.EntityId != activesecurityId) {
            this.securityGroupName = data.treePayload.data.EntityName;
            this.getSecurityGroupDetails(data.treePayload.data);
          }
          else {
            this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.deactivateSelectedNode, data.treePayload.data.EntityId);
            if(activesecurityId){
              this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.ActiveSelectedNode, activesecurityId);
            }
          }
          break;
      }

    }
    catch (e) {
      console.error('exception in singleclick() of AuthorizationService at time ' + new Date().toString() + '. exception is : ' + e);
      this.logger.logException(new Error('Exception in singleclick() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  /**
  *
  * Used to show a role form with data after clicking on particuar node of roles tree in Security fabric.
  * @param {object } node: is node data on which click event occured.
  * @example example of getRoleDetails(node) method
  *
  *  getRoleDetails(node){
  *     //todo
  *    }
  */
  getRoleDetails(name: string) {
    let payload = { Capability: this.commonService.getCapabilityNamebasedontabforSecurity(),
      CapabilityId : this.commonService.getCapabilityidbasedontabforSecurity(),RoleName: name};
    let imessageModel: IModelMessage = this.commonService.getMessageWrapper( { DataType : DataTypes.FORMSCHEMAROLES,EntityType : MessageEntityTypes.ROLESCHEMAPERMISSIONS,MessageKind : MessageKind.READ,Payload : JSON.stringify(payload)});
    this.commonService.getFormSchemaDataFromBackend(imessageModel, AppConsts.loaderStart).subscribe(data => {
      console.log('RoleFormSchema', data);
      this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, '', AppConsts.LOADER_MESSAGE_DURATION);
      this.onRoleFormSchemaDetailsReceive(data, name);
    });
    //route to security role component
    let url = FABRICS.SECURITY + '/editRole';
    let queryParams = JSON.parse(JSON.stringify(this.route.snapshot.queryParams))
    queryParams = this.getQueryParams(queryParams.leftnav,queryParams.tab,queryParams.rightnav,queryParams.righttab,queryParams.enabledmode,queryParams.subtype,queryParams.leftnaventityid);
    this.routerNavigate([url], queryParams);
  }


  /**
  *
  * Used to show a Security group form with data after clicking on particuar node of Security Groups tree.
  * @param {object } node: is node data on which click event occured.
  * @example example of getSecurityGroupDetails(node) method
  *
  *  getSecurityGroupDetails(node){
  *     //todo
  *    }
  */
  getSecurityGroupDetails(node:any) {
    try {
      let nodeEntityId = node.EntityId;
      let nodeEntityName = node.EntityName;
      let nodeSubType = node.SubType; 
      //if (!this.isSecurityGroupModified && nodeEntityId != this.securityGroupId) {
      if (nodeEntityId != this.securityGroupId) {
        this.isLeftHeaderClick = false;
        let queryParams = JSON.parse(JSON.stringify(this.route.snapshot.queryParams))
        queryParams = this.getQueryParams(queryParams.leftnav,queryParams.tab,queryParams.rightnav,queryParams.righttab,queryParams.enabledmode,nodeSubType,queryParams.leftnaventityid);   
        let url = [FABRICS.SECURITY,EntityTypes.SECURITYGROUP,'Edit',nodeEntityId]                  
        if(queryParams.leftnaventityid){
          url = [FABRICS.SECURITY,EntityTypes.SECURITYGROUP,queryParams.leftnaventityid,'Edit',nodeEntityId]                  
        }
        this.routerNavigate(url, queryParams);
      }
      // else if (this.isSecurityGroupModified) {
      //   if (this.securityGroupId) {
      //     this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.ActiveSelectedNode, this.securityGroupId);
      //   }
      //   else {
      //     this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.deactivateSelectedNode, "");
      //   }
      //   this.openPopUpDialog([DISCARD_CHANGES], true, PopupOperation.PermissionAlertConfirm, 'Attention!', 'groupSide', nodeEntityId);
      // }

    }
    catch (e) {
      console.error('Exception in getSecurityGroupDetails() of AuthorizationService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in getSecurityGroupDetails() of AuthorizationService at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e));
    }
  }


  /**
  *
  * Used to create payload, which can be send to AngularTreeCoponent to perform various operation on tree nodes, like create full tree, append new node to the existing tree.
  * @param {string } treeName: is the name of angular tree.
  * @param {string} treeOperationType: is the type of operation, needs to perform on tree node like Create full tree, Add New Node.
  * @param {object} payload: is an object which may contain the whole tree data or a single node, based on operation type.
  * @example example of createEntitiesTree(payloadJson) method
  *
  *  sendDataToAngularTree(treeName, treeOperationType, payload){
  *     //todo
  *    }
  */
  sendDataToAngularTree(treeName: string, treeOperationType: string, payload: any) {
    try {
      var treeMessage: AngularTreeMessageModel = {
        "fabric": this.getCurrentFabric(),
        "treeName": [treeName],
        "treeOperationType": treeOperationType,
        "treePayload": payload
      }
      this.commonService.sendDataToAngularTree.next(treeMessage);
    } catch (e) {
      console.error('exception in sendDataToAngularTree() of AuthorizationService at time ' + new Date().toString() + '. exception is : ' + e);
      this.logger.logException(new Error('Exception in sendDataToAngularTree() of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  /**
  *
  * Used to show People tree, roles tree and Security Groups tree in the right side bar.
  * @param {object } event: is click event occurs on rightsidebrar, based on that right side tree creates.
  * @example example of ShowPermissionTree(event) method
  *
  *  ShowPermissionTree(event){
  *     //todo
  *    }
  */
  ShowPermissionTree(event, isChangeRouting?: boolean) {
    try {
      if (isChangeRouting) {
        this.changeRouting(event);
      }
      if (this.getRolesOrSecurityGroupsleftnav() == 'true' || (isChangeRouting && this.getRolesOrSecurityGroupstab())) {
        switch (event.id) {
          case TreeTypeNames.peopleTreeForPermissions:
            this.getSecurityPeople(null);
            break;
          case TreeTypeNames.rolesTreeForPermissions:
          case TreeTypeNames.rolesTreeForPermissionsRead:
            this.getRolesBasedOnCapability(null);
            break;
          case TreeTypeNames.securityGroupsTreeForPermissions:
            this.getSecuritygroups(null);
            break;
        }
      }
    }
    catch (e) {
      console.error('Exception in ShowPermissionTree() of AuthorizationService  at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in ShowPermissionTree() of AuthorizationService  at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }
  getSecurityPeople(capability: string) {
    let payload = { Capability: FABRICS.CONSTRUCTION };
    let imessageModel: IModelMessage = this.commonService.getMessageWrapper({DataType : "People",EntityType : "People",MessageKind : MessageKind.READ,Payload : JSON.stringify(payload)});
    this.commonService.postDataToBackend(imessageModel, AppConsts.READING).subscribe(data => {
      console.log('PeopleTreePermissionData', data);
      this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, '', AppConsts.LOADER_MESSAGE_DURATION);
      this.onPeopleCompaniesDataReceive(data);
    });
  }
  getRolesBasedOnCapability(capabilityId: string) {

    let imessageModel: IModelMessage = this.commonService.getMessageWrapper( {DataType : DataTypes.FORMSCHEMAROLES,
      EntityType : MessageEntityTypes.ALLROLES,
      MessageKind : MessageKind.READ,
      Payload : JSON.stringify({ Capability: this.commonService.getCapabilityNamebasedontabforSecurity(),
        CapabilityId : this.commonService.getCapabilityidbasedontabforSecurity()})});
    this.commonService.getFormSchemaDataFromBackend(imessageModel, AppConsts.READING).subscribe(data => {
      console.log('Roles', data);
      this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, '', AppConsts.LOADER_MESSAGE_DURATION);
      this.onRolesDataReceive(data);
    });
  }

  getSecuritygroups(capability: string) {
    capability = this.commonService.getCapabilityNamebasedontabforSecurity();
    let activeLeftNavEntityId = this.queryParams.leftnaventityid;
    let payload = { Capability: capability,
      CapabilityId : this.commonService.getCapabilityidbasedontabforSecurity(),EntityId: (activeLeftNavEntityId ? activeLeftNavEntityId : '')};
    let imessageModel: IModelMessage = this.commonService.getMessageWrapper( {DataType : MessageDataTypes.SECURITYGROUPS,EntityType : MessageEntityTypes.SECURITYGROUPS,MessageKind : MessageKind.READ,Payload : JSON.stringify(payload)});
    this.commonService.postDataToBackend(imessageModel, AppConsts.READING).subscribe(data => {
      console.log('SecurityGroups', data);
      this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, '', AppConsts.LOADER_MESSAGE_DURATION);
      this.onSecurityGroupsDataReceive(data,capability);
    });
  }
  onPeopleCompaniesDataReceive(data) {
    this.logger.logEvent('People&Companies data receisve', data); 
    data = JSON.parse(JSON.stringify(data));
    let people_compData = [];
    let flattenData = [...data.Companies,...data.Persons]
    people_compData = this.commonService.GetParentChildRelationship(flattenData);
    people_compData = this.sortingTreeeForPermissions(people_compData);
    this.sendDataToAngularTree(TreeTypeNames.peopleTreeForPermissions, TreeOperations.createFullTree, people_compData);
    this.expandTree(TreeTypeNames.securityGroupsTreeForPermissions);
    this.sendDataToAngularTree(TreeTypeNames.peopleTreeForPermissions, TreeOperations.updatedropPointer, {dropDataPointer:'people&companies'});
  }
  onRolesDataReceive(rolesData: Array<any>) {
    let rolesArray: Array<TreeModelObject> = [];
    rolesData.forEach(role => {
      let roleObj = new TreeModelObject(role.EntityName, role.uid, role.EntityType, [], role.EntityId + '')
      rolesArray.push(roleObj);
    });
    rolesArray = this.sortingTreeeForPermissions(rolesArray);
    let parentObj = new TreeModelObject('Roles', 'SchemaId', 'PermissionsListHeader', [...rolesArray], 'Roles');
    this.securityRoleData = rolesArray;
    rolesArray = [];
    rolesArray.push(parentObj);
    this.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissions, TreeOperations.createFullTree, rolesArray);
    this.expandTree(TreeTypeNames.rolesTreeForPermissions);

  }
  onFormsDataReceive(formsData: Array<IFormSchemaNameModel>) {
    let id = 1;
    let formsArray: Array<TreeModelObject> = [];
    formsData.forEach(form => {
      let formObj = new TreeModelObject(form.SchemaName, form.SchemaId, 'PermissionsList', [], id + '', { "id": "Forms", "text": "Forms" }, { "id": "Forms", "text": "Forms" })
      formsArray.push(formObj);
      id++;
    });
    formsArray = this.sortingTreeeForPermissions(formsArray);
    let parentObj = new TreeModelObject('Forms', 'SchemaId', 'PermissionsListHeader', [...formsArray], 'Forms', { "id": "lkjhgfdsa098765", "text": "lkjhgfdsa098765" }, { "id": "lkjhgfdsa098765", "text": "lkjhgfdsa098765" });
    formsArray = [];
    formsArray.push(parentObj);
    this.sendDataToAngularTree(this.treeNodeName, TreeOperations.createFullTree, formsArray);
    this.expandTree(this.treeNodeName);


  }
  onRoleFormSchemaDetailsReceive(permissionsData, roleName: string) {
    let roleInfo: RoleInfoModel = { RoleName: roleName, RolePermissions: permissionsData };
    console.log('RoleInfoModel', roleInfo);
    this.roleInfoObservable$.next(roleInfo);
  }
  onSecurityGroupsDataReceive(data,capability) {
    this.logger.logEvent('securitygroups data receisve', data);
    let securitygroups = this.securityGroupsData = data ? data['SGroups']:[];
    securitygroups = this.sortingTreeeForPermissions(securitygroups);
    let parentObj = this.getTreeModel(securitygroups,capability);
    this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.createFullTree, [parentObj]);
    this.expandTree(TreeTypeNames.securityGroupsTreeForPermissions);

  }
  onPeopleCompaniesLeftNavDataReceive(data) {
    this.logger.logEvent('People&Companies data receive', data);
    var tempData = JSON.stringify(data)
    data = JSON.parse(tempData);
    let people_compData = [];
    for (var tmpdata in data) {
      var tempArrObj = data[tmpdata];
      for (var tempd in tempArrObj)
        people_compData.push(tempArrObj[tempd])
    }
    this.sendDataToAngularTree(this.treeNodeName, TreeOperations.createFullTree, people_compData);
    this.expandTree(TreeTypeNames.securityGroupsTreeForPermissions);
  }
  onEntitiesTreeDataReceive(entitiesData: Array<any>) {
    this.logger.logEvent('Construction Data', entitiesData);
    entitiesData.forEach(data => {
      var string = JSON.stringify(data)
      let taskData = JSON.parse(string);
      if (taskData['tasks'].length > 0) {
        let entities = taskData['tasks'].find(obj => obj.EntityName == 'Construction');
        if (entities) {
          this.logger.logEvent('filtered entities', entities);
          this.sendDataToAngularTree(this.treeNodeName, TreeOperations.createFullTree, entities['children']);
          this.expandTree(this.treeNodeName);
        }
      }
    })
  }
  /**
  *
  * Used to fetch the default url for every fabric based on fabric name.
  * @example example of getDefaultUrl(fabricName) method
  *
  *  getDefaultUrl(fabricName){
  *     //todo
  *    }
  */
  getDefaultUrl() {
    let res = this.router.url.toString().split("/");
    let fabricName = res[1];
    if (fabricName.includes('?'))
      fabricName = fabricName.split("?")[0]
    let defaultUrl = '';
    switch (fabricName) {
      case FABRICS.BUSINESSINTELLIGENCE:
        defaultUrl = '/bifabric';
        break;
      case FABRICS.PEOPLEANDCOMPANIES:
        defaultUrl = '/' + FABRICS.PEOPLEANDCOMPANIES + '/entities';
        break;
      case FABRICS.SECURITY:
        defaultUrl = '/' + fabricName;
        break;
    }
    return defaultUrl;
  }

  changeRoutingForUsers() {
    var url = this.getDefaultUrl();
    let queryParams = JSON.parse(JSON.stringify(this.route.snapshot.queryParams))
    queryParams = this.getQueryParams(queryParams.leftnav,queryParams.tab,queryParams.rightnav,queryParams.righttab,queryParams.enabledmode,queryParams.subtype,queryParams.leftnaventityid);                 
    this.routerNavigate([url], queryParams);
  }

  changeRouting(head: any) {
    let enabledMode = null;
    let mode = this.getRolesOrSecurityGroupsMode();
    if (mode) {
      if (mode == ROLES) {
        if (head.id != TreeTypeNames.rolesTreeForPermissions) {
          //route to securitygroups
          enabledMode = SECURITYGROUPS;
          this.changeLeftSideHeaderConfig();
        }
      }
      else {
        if (head.id == TreeTypeNames.rolesTreeForPermissions) {
          //route to roles
          enabledMode = ROLES;
          this.changeLeftSideHeaderConfig();
        }
      }
    }
    let queryParams = JSON.parse(JSON.stringify(this.route.snapshot.queryParams));
    if (enabledMode != null) {
      let url = 'Security'
      if(queryParams.leftnaventityid){
        this.sendDataToAngularTree(queryParams.tab,TreeOperations.deactivateSelectedNode,queryParams.leftnaventityid)
      }
      queryParams = this.getQueryParams(queryParams.leftnav,queryParams.tab,queryParams.rightnav,head.title,enabledMode,queryParams.subtype,null);
      this.routerNavigate([url], queryParams );
    }
    else if(queryParams.righttab != head.title){
      queryParams['righttab'] = head.title;
      queryParams['rightnav'] = true;
      this.router.navigate([], { relativeTo: this.route, queryParams: queryParams });
    }
    else if(queryParams.righttab == head.title && queryParams['rightnav'] === "false"){
      queryParams['rightnav'] = true;
      this.router.navigate([], { relativeTo: this.route, queryParams: queryParams });
    }

  }

  changeLeftSideHeaderConfig() {
    this.commonService.leftHeaderConfig.forEach(element => {
      element.show = !element.show;
    });
    let mode = this.getRolesOrSecurityGroupsMode();
    let tab = (mode == ROLES ? this.getRolesOrSecurityGroupstab().replace(" Role", "") : (this.getRolesOrSecurityGroupstab() + " Role"));
    let getCurrentTabData = this.commonService.leftHeaderConfig.find(item => item.title == tab);
    if (getCurrentTabData) {
      this.commonService.changeSecurityLeftTreeBasedOnMode.next(getCurrentTabData);
    }
    console.log(this.commonService.leftHeaderConfig);
  }

  getRolesOrSecurityGroupsMode(): string {
    return this.route.snapshot.queryParamMap.get('enabledmode');
  }

  getRolesOrSecurityGroupstab(): string {
    return this.route.snapshot.queryParamMap.get('tab');
  }

  getRolesOrSecurityGroupsleftnav(): string {
    return this.route.snapshot.queryParamMap.get('leftnav');
  }

  getRolesOrSecurityGroupsEntityId(): string {
    return this.route.snapshot.queryParamMap.get('EntityId');
  }

  getCurrentFabric(): string {
    return this.commonService.getFabricNameByUrl(this.router.url);
  }

  popupFormNameComparision(FieldName, popupFormName) {
    try {
      if (FieldName == "FactorsOrRatios" && popupFormName == "ComponentRatio")
        return true;
      else if (FieldName == "FluidLvl" && popupFormName == "Fluidlevel")
        return true;
      else if ((FieldName == "Test" || FieldName == "ViewTest") && popupFormName == "TestDataEntry")
        return true;
      else if (FieldName == "Downtime" && popupFormName == "DownTimeReason")
        return true;
      else if (FieldName == "Telemetry" && popupFormName == "DataIntegrations")
        return true;
      else if (FieldName == "ProductTolerance" && popupFormName == "ProductOfTolerance")
        return true;
      else
        return false;
    } catch (e) {
      console.error('Exception in popupFormNameComparision() of securityrole Component at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  expandTree(treeNameForExpanding) {
    var configdata: ITreeConfig = {
      "treeActiveOnClick": true,
      "treeExpandAll": true
    }
    this.sendDataToAngularTree(treeNameForExpanding, TreeOperations.treeConfig, configdata);
  }

  replaceFormName(Name) {
    try {
      if (Name) {
        Name = Name.replace(this.namingPattern, ' $1').trim();
      }
      return Name;
    }
    catch (ex) {

    }
  }

  //-----------------------------------------------Tree Data---------------------------------------------
  //-------------------------------------------------------------------------------------------------------


  initializeTreeWorkersObservables(fabric?) {
    try {
      this.commonService.initializeTreeWorkersObservablesPeople();
      this.commonService.initializeTreeWorkersObservablesBI();
      this.commonService.initializeTreeWorkersObservablesConstruction();
    } catch (e) {
      console.error(e);
    }

  }

  subscribetree(obs: any, treeName) {
    obs.subscribe((data: any) => {
      this.commonService.getAngularTreeEvent(FabricsNames.SECURITY, [treeName], TreeOperations.GetTreeInstance).
        first().
        subscribe((response: TreeDataEventResponseModel) => {
          let treeNames: Array<string> = response.treeName;
          let tree = response.treePayload;
          if (tree && treeNames.indexOf(treeName) > -1) {
            var model: TreeModel = tree.treeModel;
            if (model && model.nodes && (model.nodes.length == 0 || model.nodes.length != data.length)) {
              let nodedata = [];
              data.forEach(item => {
                nodedata.push({ EntityName: item.EntityName, EntityId: item.EntityId, EntityType: item.EntityType, children: item.children })
              });
              this.sendDataToAngularTree(treeName, TreeOperations.createFullTree, nodedata);
            }
          }
        })
    });
  }

  ReadTreeData() {
    this.CheckLeftTreeDatainState(FABRICS.BUSINESSINTELLIGENCE, "", "", "", BIentitiesarray, FABRICS.BUSINESSINTELLIGENCE, FABRICS.BUSINESSINTELLIGENCE);
    this.CheckLeftTreeDatainState(EntityTypes.People, "", "", "", PEOPLEentitiesarray, FABRICS.PEOPLEANDCOMPANIES, FABRICS.PEOPLEANDCOMPANIES);
    this.CheckLeftTreeDatainState(Datatypes.CONSTRUCTION, TreeTypeNames.Construction + 'Entities', FABRICS.CONSTRUCTION, '', '', FABRICS.CONSTRUCTION, TreeTypeNames.Schedule);
    this.CheckLeftTreeDatainState(Datatypes.CONSTRUCTION, TreeTypeNames.TaggedItems.replace(/ /g, '') + 'Entities', FABRICS.CONSTRUCTION, '', '', FABRICS.CONSTRUCTION, TreeTypeNames.TaggedItems);
    this.CheckLeftTreeDatainState(Datatypes.CONSTRUCTION, TreeTypeNames.Materials.replace(/ /g, '') + 'Entities', FABRICS.CONSTRUCTION, '', '', FABRICS.CONSTRUCTION, TreeTypeNames.Materials);
  }

  CheckLeftTreeDatainState(DataType, EntityID, EntityType, Fabric, EntityArray?, capabality?, stateStatusFabric?) {
    var stateLastModifiedUTCMS = this.commonService.usersStateQuery.getStateKey(UserStateConstantsKeys.modifiedTime);
    var isdateTime = false;
    var currentTimeUTCMS = Math.floor((new Date()).getTime() / 1000);

    if (stateLastModifiedUTCMS) {
      var windowCloseUTCMS = this.commonService.usersStateQuery.getStateKey(UserStateConstantsKeys.windowCloseTime);

      if (windowCloseUTCMS && windowCloseUTCMS > stateLastModifiedUTCMS) {//resetting store
        var closeAndModifiedTimeDifference = currentTimeUTCMS - windowCloseUTCMS;
        if (closeAndModifiedTimeDifference > 6000) {
          //clear all store except users store
          this.commonService.stateRecords.reSetStore();
          this.commonService.usersStateQuery.add(UserStateConstantsKeys.viewedFabrics, []);// Reset Viewed Fabrics
        }
        else {
          //based on datetime fetch latest entities
          isdateTime = true;
        }
      }
      else {
        //based on datetime fetch latest entities
        isdateTime = true;
      }
    }
    else {
      isdateTime = true;
    }
    let isEntitiesExist = this.commonService.getViewedFabricsAndCheckStatus(FABRICS.ENTITYMANAGEMENT + "_" + stateStatusFabric);

    if (!isEntitiesExist) { //we need toimplement logic reading entity based on last read
      if (DataType == EntityTypes.People) {
        this.lefttreeforBIandPeople(DataType, EntityArray, capabality);
        this.readCompanyTypes();
      }
      else if (DataType == FABRICS.BUSINESSINTELLIGENCE) {
        this.lefttreeforBIandPeople(DataType, EntityArray, capabality);
      }
      else if(DataType == Datatypes.CONSTRUCTION){
        this.commonService.leftTreeForConstruction(DataType, EntityArray, capabality, null, EntityType, EntityID, stateStatusFabric);
      }
      else {
        this.Readcapabilitytreedata(DataType, EntityID, EntityType, Fabric);
      }
    }
    else if (isdateTime) {
      this.dateTimeBasedReadTreeBackend(currentTimeUTCMS, DataType, EntityID, EntityType, Fabric, EntityArray, capabality);
      if (DataType == EntityTypes.People)
        this.readCompanyTypes();
    }
  }

  lefttreeforBIandPeople(EntityTypeORDataTypeORFabric, EntityArray, capabality, dateTime?) {

    if (this.commonService.capabilityFabricId == null || this.commonService.capabilityFabricId == undefined) {
      this.commonService.capabilityFabricId = this.commonService.FabricCapabilityIdAtLoginTime;
    }
    let Payload;
    if (capabality == FABRICS.BUSINESSINTELLIGENCE)
      Payload = { "EntityType": EntityTypeORDataTypeORFabric, "DataType": EntityTypeORDataTypeORFabric, "Fabric": EntityTypeORDataTypeORFabric, "Info": "General", "CapabilityId": this.commonService.getCapabilityId(capabality) };
    else
      Payload = {};

    if (dateTime && Object.keys(Payload).length == 0) {
      Payload = {};
      Payload["ModifiedDateTime"] = dateTime;
    } else if (dateTime) {
      Payload["ModifiedDateTime"] = dateTime;
    }


    let message = this.commonService.getMessage();
    message.Payload = JSON.stringify(Payload);
    message.DataType = EntityTypeORDataTypeORFabric;
    message.EntityID = EntityTypeORDataTypeORFabric;
    message.EntityType = EntityTypeORDataTypeORFabric;
    message.MessageKind = MessageKind.READ;
    message.Routing = Routing.OriginSession;
    message.CapabilityId = this.commonService.getCapabilityId(capabality);
    message.Type = "Details";
    //this.commonService.statusCheck(message);
    message.Fabric = EntityTypeORDataTypeORFabric;

    this.commonService
      .httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .filter(data => message.CapabilityId).map(res => {
        var messageData = JSON.parse(res);
        let data = JSON.parse(messageData["Payload"]);
        return data;
      })
      .subscribe(data => {
        this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, '');
        this.filterforBIandPeople(data, EntityArray, capabality);

      });
  }

  filterforBIandPeople(data, array, capabality) {
    var Entitiesdata = [];
    for (const key in data) {
      if (data.hasOwnProperty(key) && Array.isArray(data[key])) {
        Entitiesdata = [...data[key], ...Entitiesdata];
      }
    }
    var entities = Entitiesdata;

    let existingEntities = [];
    let newEntities = [];
    let parentEntities = [];
    for (var index in entities) {
      if (entities[index]) {
        if (entities[index].CompanyType && entities[index].CompanyType != "" && entities[index].CompanyType.length > 0)
          entities[index].CompanyType = entities[index].CompanyType[0]//local dev use
        let entityId = entities[index].EntityId;
        let entityData = this.commonService.entityCommonStoreQuery.getEntity(entityId);
        if (entityData) {
          // if BI not exist in SG then add into existing entities list.
          if (entityData["SG"] != undefined) {
            if (!((<Array<any>>entityData["SG"]).indexOf(capabality) >= 0)) {
              existingEntities.push(entityId);
            }
            continue;
          }

          if (capabality == FABRICS.PEOPLEANDCOMPANIES) {
            if ((entityData["Parent"] == undefined || entityData["Parent"] == "") && entities[index]["Parent"]) {
              parentEntities.push(entities[index]);
            }
            // if People not exisit in SG then add into existing entities list.
            if (entityData["SG"] == undefined) {
              existingEntities.push(entityId);
              continue;
            }
          }

        }
        if (capabality == FABRICS.PEOPLEANDCOMPANIES) {
          entities[index]["SG"] = [capabality];
          newEntities.push(entities[index]);
        } else {
          entities[index]["SG"] = [capabality];
          entities[index]["TypeOf"] = entities[index]["EntityType"];
          if (entities[index]["EntityType"] == "DashBoard") {
            entities[index]["Dashboardurl"] = entities[index]["DashBoardURL"];
            delete entities[index]["DashBoardURL"];
          }
          newEntities.push(entities[index]);
        }

      }
    }
    if (existingEntities.length > 0)
      this.commonService.entityCommonStoreQuery.UpdateArrayValue(existingEntities, "SG", capabality);

    if (newEntities.length > 0)
      this.commonService.entityCommonStoreQuery.AddAll(newEntities);

    if (parentEntities.length > 0)
      this.commonService.entityCommonStoreQuery.UpdateProperties("Parent", parentEntities);

    if (this.getCurrentFabric().toLowerCase() == "Security") {
      this.commonService.setToFDCStateByKey(FABRICS.SECURITY + "_" + capabality, newEntities)
    }
    this.commonService.updateViewedFabric(FABRICS.ENTITYMANAGEMENT + "_" + capabality);
  }
  readCompanyTypes() {
 
  }
  dateTimeBasedReadTreeBackend(currentTimeUTCMS: number, DataType, EntityID, EntityType, Fabric, EntityArray?, capabality?) {
    try {
      let isSubscribe = true;
      let fabricName = "";

      if (capabality)
        fabricName = capabality;
      else if (Fabric)
        fabricName = Fabric;
      this.commonService.entityCommonStoreQuery
        .selectAll({
          filterBy: (entity: any) => entity && entity.SG && (<Array<any>>entity.SG).indexOf(fabricName) >= 0
        })
        .filter(data => data.length != 0 && isSubscribe)
        .map(data => JSON.parse(JSON.stringify(data)))
        .shareReplay(1).first().subscribe(res => {
          var date = this.commonService.maxModifiedDateTimeStateObject(res);

          var last_utc = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
            date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()) / 1000;

          if (last_utc != NaN && currentTimeUTCMS - last_utc > 60) {
            var dateTime = new Date(last_utc * 1000).toISOString().slice(0, 19).replace('T', ' ');
            if (capabality == FABRICS.BUSINESSINTELLIGENCE || capabality == FABRICS.PEOPLEANDCOMPANIES){
              this.lefttreeforBIandPeople(DataType, EntityArray, capabality, dateTime);
            }
            else if(capabality == FABRICS.CONSTRUCTION){
                this.commonService.leftTreeForConstruction(DataType, EntityArray, capabality, dateTime, EntityType, EntityID);
            }
            else
              this.Readcapabilitytreedata(DataType, EntityID, EntityType, Fabric, dateTime);
          }
          isSubscribe = false;
        });
    }
    catch (e) {
      this.commonService.appLogException(new Error("authorizationService dateTimeBasedReadTreeBackend: " + e));
    }
  }

  Readcapabilitytreedata(DataType, EntityID, EntityType, Fabric, dateTime?) {
    var capabality;
    let d = new Date,
      dformat = [d.getFullYear(), d.getMonth() + 1, d.getDate()].join('-') + ' ' + [d.getHours(), d.getMinutes(), d.getSeconds()].join(':');

    if (this.commonService.capabilityFabricId == null || this.commonService.capabilityFabricId == undefined) {
      this.commonService.capabilityFabricId = this.commonService.FabricCapabilityIdAtLoginTime;
    }

    capabality = Fabric;

    let payload = { "EntityType": EntityType, "DataType": DataType, "Fabric": Fabric, "Info": "Admin", "CurrentDate": dformat, "CapabilityId": this.commonService.getCapabilityId(capabality) }

    if (dateTime) {
      payload["ModifiedDateTime"] = dateTime;
    }

    var PayLoad = JSON.stringify(payload);
    let message = this.commonService.getMessage();
    message.Payload = PayLoad;
    message.DataType = DataType;
    message.EntityID = EntityID;
    message.EntityType = payload.EntityType;
    message.MessageKind = MessageKind.READ;
    message.Routing = Routing.OriginSession;
    message.CapabilityId = this.commonService.getCapabilityId(capabality);
    message.Type = "Details";
    this.commonService.statusCheck(message);
    message.Fabric = Fabric;
    Observable.combineLatest([
      this.commonService.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message),
    ]).filter(data => message.CapabilityId).map(([treeData]) => {
      var messageData = JSON.parse(treeData);
      let treeEntity = JSON.parse(messageData["Payload"]);
      this.commonService.setTypeOfField(treeEntity);
      let data = [treeEntity]
      return data;
    })
      .subscribe(([treeData]) => {
        this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, '');
        this.formatinglefttree(treeData, capabality);
      });
  }
  formatinglefttree(data, capabality) {
    var entities = [...data.areas,
    ...data.district,
    ...data.facilities,
    ...data.fields,
    ...data.headers,
    ...data.equipments,
    ...data.wells,
    ...data.facilitycode
    ];
    let existingEntities = [];
    let newEntities = []
    for (var index in entities) {
      if (entities[index]) {
        let entityId = entities[index].EntityId;
        let entityData = this.commonService.entityCommonStoreQuery.getEntity(entityId);
        if (entityData) {
          // if fdc not exisit in SG then add into existing entities list.
          if (entityData["SG"] != undefined) {
            if (!((<Array<any>>entityData["SG"]).indexOf(capabality) >= 0)) {
              existingEntities.push(entityId);
            }
            continue;
          }
        }
        else {
          //add new node
          entities[index]["SG"] = [capabality];
          newEntities.push(entities[index]);
        }

      }
    }
    if (existingEntities.length > 0)
      this.commonService.entityCommonStoreQuery.UpdateArrayValue(existingEntities, "SG", capabality);

    if (newEntities.length > 0)
      this.commonService.entityCommonStoreQuery.AddAll(newEntities);
    if (this.getCurrentFabric().toLowerCase() == "Security") {
      this.commonService.setToFDCStateByKey(FABRICS.SECURITY + "_" + capabality, newEntities)
    }
    this.commonService.updateViewedFabric(FABRICS.ENTITYMANAGEMENT + "_" + capabality);
  }

  SecurityTreeInitilization() {
    try {
      this.capabilityName = this.getRolesOrSecurityGroupstab();
      let mode = this.getRolesOrSecurityGroupsMode();
      if (this.capabilityName != undefined && !this.commonService.isFormModified) {
        if (this.getRolesOrSecurityGroupsleftnav() == 'true') {
          this.readFormTreeData(this.capabilityName);
        }
      }
      else if (this.commonService.isFormModified && this.Activetab == this.capabilityName && mode == 'roles') {
        var tabHeaderData: any = {};
        if (this.commonService.leftHeaderConfig) {
          this.commonService.leftHeaderConfig.forEach(head => {
            if (head.title == this.capabilityName) {
              tabHeaderData = head;
            }
          });
        }
        this.sendRequestToReadFormsInSecurityCapability(tabHeaderData);
      }
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in ngOnInit() method of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  resetEntitiestree() {
    this.commonService.permissionListTab$.next({ queryParams: { tab: this.Activetab }, tabData: { tab: this.Activetab }, enableList: false });
  }
  readFormTreeData(capabilityName: string) {
    try {
      this.treeNodeName = capabilityName;
      let tabHeaderData: any = {};
      if (this.commonService.leftHeaderConfig) {
        this.commonService.leftHeaderConfig.forEach(head => {
          if (head.title == capabilityName) {
            tabHeaderData = head;
          }
        });
      }

      let mode = this.getRolesOrSecurityGroupsMode();


      if (this.Activetab != tabHeaderData.title) {
        this.Activetab = tabHeaderData.title;
        let righttreeMode = { id: "" };
        if (mode == ROLES) { //get forms data
          this.getFormsBasedOnCapability(capabilityName);
        }
        if (this.commonService.treeIndexLabelRight && this.commonService.treeIndexLabelRight.toLowerCase() == "SECURITY GROUPS".toLowerCase()) {
          righttreeMode.id = TreeTypeNames.securityGroupsTreeForPermissions;
        } else if (this.commonService.treeIndexLabelRight && this.commonService.treeIndexLabelRight.toLowerCase() == "SECURITY ROLES".toLowerCase() && mode == 'roles') {
          righttreeMode.id = TreeTypeNames.rolesTreeForPermissions
        }
        if (righttreeMode.id && this.ActiveMode == this.getRolesOrSecurityGroupsMode()) {
          this.ShowPermissionTree(righttreeMode);
        } else {
          this.ActiveMode = this.getRolesOrSecurityGroupsMode()
        }
        // if(righttreeMode.id && mode == SECURITYGROUPS){
        //   this.getEntitiesTreeData(capabilityName);
        // }
      } else if (this.Activetab == tabHeaderData.title && mode == ROLES) {
        this.getFormsBasedOnCapability(capabilityName);
      }

    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in readFormTreeData() method of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }


  getFormsBasedOnCapability(capabilityId: string) {

    let imessageModel: IModelMessage = this.commonService.getMessageWrapper({ DataType : DataTypes.FORMSCHEMAROLES,EntityType : MessageEntityTypes.ALLFORMSCHEMANAMES,MessageKind : MessageKind.READ,
    Payload : JSON.stringify({ Capability: this.commonService.getCapabilityNamebasedontabforSecurity(),
      CapabilityId : this.commonService.getCapabilityidbasedontabforSecurity()})
    });
    this.commonService.getFormSchemaDataFromBackend(imessageModel, AppConsts.READING).subscribe(data => {
      console.log('FormsSchenaNames', data);
      this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, '', AppConsts.LOADER_MESSAGE_DURATION);
      let mode = this.getRolesOrSecurityGroupsMode();
      if(mode == ROLES){
      this.onFormsDataReceive(data);
      }
    });
  }



  sendRequestToReadFormsInSecurityCapability(tabHeader) {
    try {
      if (tabHeader.title) {
        tabHeader.entityId = this.commonService.getCapabilityidbasedontabforSecurity();
        this.treeNodeName = tabHeader.title;
        let message = this.commonService.getMessage();
        message.EntityID = "forms";
        message.EntityType = "FabricCapabilityForm";
        message.Fabric = tabHeader.title.replace(" Role", "");
        message.Payload = tabHeader.entityId;
        message.DataType = "Authorization";
        message.MessageKind = "Read";
        message.Routing = Routing.OriginSession;
        switch (message.Fabric) {
          case TreeTypeNames.PeopleAndCompanies:
            message.Fabric = "People";
            break;
        }
        this.fabricIdForSecurity = tabHeader.entityId;
        this.commonService.sendMessageToSocket_Backend(message);
      }
    } catch (e) {
      this.commonService.appLogException(new Error('Exception in sendRequestToReadFormsInSecurityCapability() method of AuthorizationService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }


  ListTreeSendBackEnd(tab: string) {
    let payloadFabricName = this.returnPayloadfabricName(tab);
    let stateFabricName = this.returnListfabricName(tab);
    let payloadCapabilityId = this.commonService.getCapabilityId(stateFabricName);
    var userPayload = {
      "CapabilityId": payloadCapabilityId,
      "Info": "UserList"
    };

    let message = this.commonService.getMessage();
    //capabilityId changes
    message.Payload = JSON.stringify(userPayload);
    message.DataType = EntityTypes.List;
    message.EntityID = EntityTypes.UserList;
    message.EntityType = EntityTypes.UserList;
    message.MessageKind = MessageKind.READ;
    message.Routing = Routing.OriginSession;
    message.Type = "Details";
    this.commonService.statusCheck(message);
    message.Fabric = payloadFabricName;
    message.CapabilityId = payloadCapabilityId;
    console.log("Get Lists tree send backend", new Date().getMinutes() + ":" + new Date().getSeconds())

    this.commonService
      .httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .map(res => {
        this.commonService.loadingBar.complete();
        console.log("Get Lists tree received backend", new Date().getMinutes() + ":" + new Date().getSeconds())
        let messageData = JSON.parse(res);
        let data = JSON.parse(messageData["Payload"]);
        return data;
      })
      .do(d => console.log("Get Lists tree after received map", new Date().getMinutes() + ":" + new Date().getSeconds()))
      .subscribe((data: any) => {
        console.log(data);
        this.commonService.addListEntitiesToState(data, stateFabricName);
        this.QueryListDataFromState(tab);
      });
  }

  QueryListDataFromState(tab: string) {
    let stateFabricName = this.returnListfabricName(tab);
    let entities$ = this.entityCommonStoreQuery
      .selectAll({
        filterBy: (entity: any) =>
          entity && ([].indexOf(entity.TypeOf) >= 0) && entity.SG && (<Array<any>>entity.SG).indexOf(stateFabricName) >= 0
      })
      .debounceTime(150)
      .filter(data => data.length != 0)
      .map(data => JSON.parse(JSON.stringify(data)))
      .shareReplay(1);

    let listEntities$ = this.entityCommonStoreQuery
      .selectAll({
        filterBy: (entity: any) =>
          entity && [EntityTypes.List, EntityTypes.Personal, EntityTypes.Shared].indexOf(entity.EntityType) >= 0 && entity.SG && (<Array<any>>entity.SG).indexOf(stateFabricName) >= 0
      })
      .debounceTime(150)
      .filter(data => data.length != 0)
      .map(data => JSON.parse(JSON.stringify(data)))
      .shareReplay(1);
    let lists$ = listEntities$
      .combineLatest(entities$.startWith([]))
      .map(d =>
        this.commonService.createListEntities(d[0], stateFabricName)
      ).subscribe(data => {
        console.log("List Data", data);
        //send data to angular tree
        this.sendDataToAngularTree(tab, TreeOperations.createFullTree, data);
      });

  }

  getTreeModel(securitygroups:Array<any>,capability:string):TreeModelObject{
    let parentObj:TreeModelObject;
    switch(capability){
      case FABRICS.CONSTRUCTION:
        parentObj = new TreeModelObject(SECURITY_GROUPS, 'SchemaId', SECURITY_ROOTNODE, [], SECURITY_GROUPS);
        AppConsts.ConstructionSGTreeList.forEach(element =>{
          let entities = securitygroups.filter(item => item.SubType == element);
          parentObj.children.push( new TreeModelObject(element, element, element, [...entities], element));
        });
        break;
        default:
          parentObj = new TreeModelObject(SECURITY_GROUPS, 'SchemaId', SECURITY_ROOTNODE, [...securitygroups], SECURITY_GROUPS);
          break;
    }
    return parentObj;
  }

  routerNavigate(urllist:Array<string>,queryParam?:any){
    if(queryParam){
    this.router.navigate(urllist, { queryParams: queryParam})
    }
    else{
      this.router.navigate(urllist)

    }
  }

  getQueryParams(leftNav:string,tab:string,rightNav:string,rightTab:string,enabledMode:string,SubType:string=null,leftNavEntityId:string=null){
    let queryParams = {leftnav:'',tab:'',rightnav:'',righttab:'',enabledmode:'',subtype:'',leftnaventityid:''};
    tab ? queryParams.tab = tab : queryParams.tab;
    leftNav ? queryParams.leftnav = leftNav : queryParams.leftnav;
    rightNav ? queryParams.rightnav = rightNav : queryParams.rightnav;
    rightTab ? queryParams.righttab = rightTab : queryParams.righttab;
    enabledMode ? queryParams.enabledmode = enabledMode : queryParams.enabledmode;
    SubType ? queryParams.subtype = SubType : delete queryParams.subtype;
    leftNavEntityId ? queryParams.leftnaventityid = leftNavEntityId : delete queryParams.leftnaventityid;
    return queryParams;
  }
  /**
   * Cleanup just before Angular destroys the directive/component.
   * Unsubscribe Observables and detach event handlers to avoid memory leaks
   */
  ngOnDestroy() {

  }
}
