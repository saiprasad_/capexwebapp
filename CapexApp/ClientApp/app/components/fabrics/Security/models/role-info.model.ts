export class RoleInfoModel {
    RoleName: string;
    RolePermissions: Array<RoleFormPermissions>;

}

export class RoleFormPermissions {
    SchemaName: string;
    SchemaId: string;
    Permissions: RolePermissions;
    // form: globalThis.Permissions;
    // form: globalThis.Permissions;
    // form: globalThis.Permissions;

}

export class RolePermissions {
    create: boolean;
    read: boolean;
    update: boolean;
    delete: boolean;
}