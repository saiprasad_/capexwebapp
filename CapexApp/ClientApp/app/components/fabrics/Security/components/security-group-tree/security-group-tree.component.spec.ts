import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SecurityGroupTreeComponent } from './security-group-tree.component';

describe('SecurityGroupTreeComponent', () => {
  let component: SecurityGroupTreeComponent;
  let fixture: ComponentFixture<SecurityGroupTreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SecurityGroupTreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SecurityGroupTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
