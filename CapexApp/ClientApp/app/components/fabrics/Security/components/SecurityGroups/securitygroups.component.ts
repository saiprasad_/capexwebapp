import { Component, Input, ComponentFactoryResolver, ChangeDetectorRef, OnInit, OnDestroy } from '@angular/core';
import { CommonService } from '../../../../globals/CommonService';
import { Observable, Subject, of } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { AppInsightsService } from '../../../../common/app-insight/appinsight-service';
import { AuthorizationService } from './../../services/authorization.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { FABRICS, MessageKind, AngularTreeMessageModel, TreeTypeNames, TreeOperations, FabricsNames, TreeDataEventResponseModel, FabricsPath } from '../../../../globals/Model/CommonModel';
import { ITreeConfig, EntityTypes } from './../../../../globals/Model/EntityTypes'
import { Routing, IModelMessage, MessageDataTypes } from '../../../../globals/Model/Message';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PopupOperation } from 'ClientApp/app/components/globals/Model/AlertConfig';
import { PopUpAlertComponent } from 'ClientApp/app/components/common/PopUpAlert/popup-alert.component';
import { AppConfig } from '../../../../globals/services/app.config';
import { AppConsts, ALLENTITYTYPES, NEW_ROWS, DELETE_ROWS, ENTITIES } from 'ClientApp/app/components/common/Constants/AppConsts';
import { DISCARD_CHANGES, CANT_DRAG_SECURITYGROUPS, CANT_DRAG_FOLDER, SECURITYGROUPS_EMPTY_NAME, ALREADY_EXIST_GROUP, ALREADY_EXIST, SOME_ALREADY_EXIST, SOMETHING_WRONG, VALID_SECURITYGROUPS_NAME } from '../../../../common/Constants/AttentionPopupMessage';
import { PARENTTREENODE } from '../../services/constants/module.constants';
import { MessageEntityTypes } from '../../services/constants/message.constants';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'securitygroups',
  templateUrl: 'securitygroups.component.html',
  styleUrls: ['./securitygroups.component.scss']
})


export class SecurityGroupsComponent implements OnInit, OnDestroy {

  peopleCompanies = [];

  /**
  * entityDetails
  */
  // entityDetails = {
  //   entities: [],
  //   peopleEntities: [],
  //   roles: []
  // };
  entityDetails  = {
    entities: [],
    peopleEntities: []
  };
  fabricName;
  /**
  * draggedEntities
  */
  draggedEntities = [];
  /**
  * draggedPeopleEntities
  */
  draggedPeopleEntities = [];
  /**
   * removedEntities
   */
  removedEntities = [];
  /**
 * removedPeopleEntities
 */
  removedPeopleEntities = [];
  /**
   * securityGroupsDeatils
   */
  securityGroupsDeatils: any;

  /**
  * 
  *href
  *
  */
  public href: any;

  /**
 * TableList
 */
  TableList = [];
  /**
 * FieldsList
 */
  FieldsList = [];
  /**
 * HeaderList
 */
  HeaderList = ['Entities', 'People & Companies', 'Roles'];
  /**
   * ListArray
   */
  ListArray = [];
  /**
  * currentGroupName
  */
  currentGroupName: string = "";
  /**
  * isGroupNameExist
  */
  isGroupNameExist: boolean = false;
  AddedHaulLocation: boolean = false;
  /**
  * dialogRef
  */
  dialogRef: any;


  params:any = {};
  /**
  * formType
  */
  formType: string = "";
  /**
  * 
  *securityGroupName
  *
  */
  @Input()
  securityGroupName: string;

  enable = false;
  selectedEntitiesByUser = [];
  securityGroupTreeName = "";
  emitEventsToChild: Subject<any> = new Subject<any>();
  isAddNewSecurityGroup:boolean = true;
  leftNavEntityId:string;
  constructionSGForm:any = {
    "head": [
      {
        "source": "wwwroot/images/V3 Icon set part1/V3 General.svg",
        "title": "Enter name",
        "routerLinkActive": false,
        "id": "input",
        "class": "title",
        "float": "left",
        "type": "input",
        "event": {},
        "show": true,
        "uppercase": "Isuppercase"
      },
      {
        "source": "wwwroot/images/V3 Icon set part1/V3 CloseCancel.svg",
        "title": "Close",
        "routerLinkActive": false,
        "id": "close",
        "class": "icon21-21",
        "float": "right",
        "type": "icon",
        "show": true
      },
      {
        "source": "wwwroot/images/V3 Icon set part1/V3 Help.svg",
        "title": "Entity Admin",
        "routerLinkActive": false,
        "id": "save",
        "class": "icon21-21",
        "float": "right",
        "type": "icon",
        "show": true
      },
      {
        "source": "",
        "title": "Entity Type",
        "routerLinkActive": false,
        "id": "ngselect",
        "class": "icon21-21",
        "float": "left",
        "type": "ngselect",
        "show": true,
        "readOnly": false,
        "options": [],
        "defaultValue": "Schedule"
      }
    ],
    "body": [
      {
        "events": {},
        "schemaPage": "Premico",
        "type": "form",
        "title": "Premico",
        "class": "",
        "width":"50",
        "treeName": "SecurityGroupsPremicoTree",
        "id": "Premico",
        "showFilter":true,
        "showSearch":true,
        "showExpandAll":false
      },
      {
        "events": {},
        "schemaPage": "Entities",
        "type": "form",
        "title": "Entities",
        "class": "",
        "width":"25",
        "treeName": "SecurityGroupsEntitiesTree",
        "id": "Entities",
        "showFilter":false,
        "showSearch":true,
        "showExpandAll":false
      },
      {
        "events": {},
        "schemaPage": "People&Companies",
        "type": "form",
        "title": "People & Companies",
        "class": "",
        "width":"25",
        "treeName": "SecurityGroupsPeopleTree",
        "id": "People&Companies",
        "showFilter":false,
        "showSearch":true,
        "showExpandAll":false
      }
    ]
  }

  defaultSGForm:any = {
    "head": [
      {
        "source": "wwwroot/images/V3 Icon set part1/V3 General.svg",
        "title": "Enter name",
        "routerLinkActive": false,
        "id": "input",
        "class": "title",
        "float": "left",
        "type": "input",
        "event": {},
        "show": true,
        "uppercase": "Isuppercase"
      },
      {
        "source": "wwwroot/images/V3 Icon set part1/V3 CloseCancel.svg",
        "title": "Close",
        "routerLinkActive": false,
        "id": "close",
        "class": "icon21-21",
        "float": "right",
        "type": "icon",
        "show": true
      },
      {
        "source": "wwwroot/images/V3 Icon set part1/V3 Help.svg",
        "title": "Entity Admin",
        "routerLinkActive": false,
        "id": "save",
        "class": "icon21-21",
        "float": "right",
        "type": "icon",
        "show": true
      }
    ],
    "body": [
      {
        "events": {},
        "schemaPage": "Entities",
        "type": "tree",
        "title": "Entities",
        "class": "",
        "width":"50",
        "treeName": "SecurityGroupsEntitiesTree",
        "id": "Entities",
        "showFilter":false,
        "showSearch":true,
        "showExpandAll":true
      },
      {
        "events": {},
        "schemaPage": "People&Companies",
        "type": "tree",
        "title": "People & Companies",
        "class": "",
        "width":"50",
        "treeName": "SecurityGroupsPeopleTree",
        "id": "People&Companies",
        "showFilter":false,
        "showSearch":true,
        "showExpandAll":true
      }
    ]
  }

  sgForm:any = this.defaultSGForm;
 



  /**
  * 
  *entityTypesPeopleAndCompnay
  *
  */
  entityTypesPeopleAndCompnay = [EntityTypes.PERSON, EntityTypes.COMPANY, EntityTypes.OFFICE];
  /**
  * 
  *entityTypesRole
  *
  */
  entityTypesRole = [EntityTypes.Role];
  takeUntilDestroyObservables = new Subject();
  formSelectFormControl:FormControl = new FormControl();
  selectOptions:Array<string>;
  /**
   * This is the example to create Constructor for Security group component
   * @param commonService Injects common service like common observables ,variables or any services can be shared among all components
   * @param router Injects angular routing service with which routing or navigation can be done
   * 
   * @example example of constructor
   * 
   * constructor(public commonService: CommonService, public fdcService: FDCService, public router: Router){
   *  // Todo 
   * }
   */
  constructor(public dialog: MatDialog, public appConfig: AppConfig, private ref: ChangeDetectorRef, private route: ActivatedRoute, public router: Router, private logger: AppInsightsService, public commonService: CommonService, public componentFactoryResolver: ComponentFactoryResolver, public authorizationService: AuthorizationService) {
    try {

      this.securityGroupName = authorizationService.securityGroupName;
      this.currentGroupName = this.securityGroupName;
      this.commonService.layoutJSON.OverLayRightTree = true;
      this.createEntityTypeControl();
      /**
      * To show already created Security groups 
      */
      this.authorizationService.showSecurityGroupForm$.pipe(this.compUntilDestroyed()).subscribe((res: any) => {
        this.entityDetails = {
          entities: [],
          peopleEntities: []
        };
        this.draggedEntities = [];
        this.draggedPeopleEntities = [];
        this.removedEntities = [];
        this.removedPeopleEntities = [];
        this.ListArray = [];
        this.authorizationService.listOfEntityForSecurityGroupForm = [];

        if (res['Payload'] != null && res['Payload'] != "") {
          let payloadJson = JSON.parse(res['Payload']);
          if (payloadJson['SgroupDetails'].length != 0) {
            payloadJson = payloadJson['SgroupDetails']['0'];
            this.authorizationService.securityGroupName = payloadJson["EntityName"];
            this.securityGroupName = this.authorizationService.securityGroupName;
            this.currentGroupName = this.securityGroupName;
            if (!payloadJson.entities) {
              payloadJson.entities = [];
            }
            if (!payloadJson.people) {
              payloadJson.people = [];
            }
          }
          else {
            payloadJson = {
              entities: [],
              people: []
            };
          }


          if (payloadJson['entities']) {
            payloadJson['entities'].forEach(element => {
              if (element["EntityName"] != undefined) {
                //this.entityDetails.entities.push(element);
                this.getChilddata([element], this.entityDetails.entities)
              }
            });
          }
          if (payloadJson['listentities']) {
            payloadJson['listentities'].forEach(element => {
              if (element["EntityName"] != undefined) {
                //this.entityDetails.entities.push(element);
                this.getChilddata([element], this.entityDetails.entities)
              }
            });
          }
          //sorting for securitygroup entities 
          var sortedEntities = this.authorizationService.sortingTreeeForPermissions(this.entityDetails.entities);
          let fabric = this.commonService.getFabricNameByTab(this.authorizationService.getRolesOrSecurityGroupstab());
          switch (fabric) {
            case FABRICS.BUSINESSINTELLIGENCE:
              this.createEntitiesTreeBasedOnALLTree(fabric, sortedEntities.reverse(), TreeTypeNames.SecurityGroupsEntitiesTree).pipe(this.compUntilDestroyed()).subscribe((response: Array<any>) => {
                this.entityDetails.entities = response;
              });
              break;
            case FABRICS.PEOPLEANDCOMPANIES:
              let rootNode = this.commonService.flatToPeopleAllTree(this.entityDetails.entities);
              this.commonService.sendDataToAngularTree1(TreeTypeNames.SecurityGroupsEntitiesTree, 'createTree', rootNode);
              break;
            default:
              this.commonService.getEntititesByTypeOf$("EntityType").pipe(this.compUntilDestroyed()).subscribe(data => {
                let entities = [];
                let listentitiles = [];
                this.entityDetails.entities.forEach(item => {
                  let entity = this.commonService.getEntitiesById(item.EntityId);
                  if (entity && item.EntityType != EntityTypes.FACILITYCODE && item.EntityType != "List") {
                    entities.push(entity)
                  }
                  else if (item.EntityType == "List") {
                    listentitiles.push(item)
                  }
                });
                let Fabric = null;
             
                this.commonService.getObserveable(TreeTypeNames.PHYSICALFLOW, entities, Fabric).subscribe((msg: any) => {
                  let rootNode = msg;
                  listentitiles.forEach(element => {
                    switch (element.EntityType) {
                      case "List":
                        let ele = element;
                        ele["children"] = element["list.entity"] ? element["list.entity"] : [];
                        ele["children"] = ele["children"].filter(item => item["EntityName"] != undefined);
                        ele["children"].forEach(item => {
                          item["Parent"] = [{ EntityId: ele.EntityId, EntityName: ele.EntityName }]
                        });
                        delete element["list.entity"];
                        rootNode.push(ele);
                        break;
                    }
                  });
                  rootNode.forEach((item, index) => {
                    if (item.EntityType == EntityTypes.HaulLocation && item.children) {
                      if (item.children.length == 0) {
                        rootNode.splice(index, 1)
                      } else {
                        this.AddedHaulLocation = true;
                      }
                    }
                  });
                  rootNode = this.commonService.sortNodesByOrder(rootNode, AppConsts.SGEntitySortingOrderArray)
                  this.commonService.sendDataToAngularTree1(TreeTypeNames.SecurityGroupsEntitiesTree, 'createTree', rootNode);
                });
              });
              break;
          }




        }
        else {
          console.warn('Invalid JSON.');
        }
      },
        error => {
          console.error('Exception in showSecurityGroupForm$ observable of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + error.message);
          this.logger.logException(new Error('Exception in showSecurityGroupForm$ observable of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + error.message));
        });

      this.authorizationService.removeEntitiesFromSecurityGroupForm$.pipe(this.compUntilDestroyed()).subscribe((Data: any) => {
        let treeName = Data.treeName[0];
        if ([TreeTypeNames.SecurityGroupsEntitiesTree, TreeTypeNames.SecurityGroupsPeopleTree].includes(treeName)) {
          let index;
          let eNode = null;
          let selectedTreeNodes = [];
          //this.authorizationService.isSecurityGroupModified = true;
          for (let key in Data.treePayload.nodeData.treeModel.activeNodeIds) {
            if (Data.treePayload.nodeData.treeModel.activeNodeIds[key]) {
              selectedTreeNodes.push(key);
            }
          }
          if (selectedTreeNodes.length <= 1) {
            eNode = Data.treePayload.nodeData.data;
            selectedTreeNodes = [];
          }
          let operationType =  Data.treePayload.optionSelected;
          switch (treeName) {
            case TreeTypeNames.SecurityGroupsEntitiesTree: {
              selectedTreeNodes.forEach(item => {
                let node = Data.treePayload.nodeData.treeModel.getNodeById(item);
                if (node) {
                  let nodechild = [];
                  if (node.data.children && node.data.children.length && operationType != "Remove Hierarchy") {
                    nodechild = node.data.children;
                    node.data.children = [];
                  }
                  if (this.isAddNewSecurityGroup) {
                    index = this.findIndexToDelete(this.entityDetails.entities, node.data.EntityId);
                    if (index != -1) {
                      this.entityDetails.entities.splice(index, 1);
                    }
                  }
                  index = this.findIndexToDelete(this.draggedEntities, node.data.EntityId);
                  if (index == -1) {
                    //this.removedEntities.push(node.data);
                    this.getChilddata([node.data], this.removedEntities)
                  }
                  else {
                    this.draggedEntities.splice(index, 1);
                  }
                  this.sendDataToAngularTree(treeName, TreeOperations.deleteNodeById, node.data.EntityId);
                  if (node.data.EntityId == EntityTypes.HaulLocation) {
                    this.AddedHaulLocation = false;
                  }
                  nodechild.forEach(item => { this.insertNode(treeName, item) });
                }
              });
              if (eNode) {
                let nodechild = [];
                if (eNode.children && eNode.children.length  && operationType != "Remove Hierarchy") {
                  nodechild = eNode.children;
                  eNode.children = [];
                }
                if (this.isAddNewSecurityGroup) {
                  index = this.findIndexToDelete(this.entityDetails.entities, eNode.EntityId);
                  if (index != -1) {
                    this.entityDetails.entities.splice(index, 1);
                  }
                }
                index = this.findIndexToDelete(this.draggedEntities, eNode.EntityId);
                if (index == -1) {
                  //this.removedEntities.push(eNode);
                  this.getChilddata([eNode], this.removedEntities)

                }
                else {
                  this.draggedEntities.splice(index, 1);
                }
                this.sendDataToAngularTree(treeName, TreeOperations.deleteNodeById, eNode.EntityId);
                // if (eNode.EntityId == EntityTypes.HaulLocation) {
                //   this.AddedHaulLocation = false;
                // }
                nodechild.forEach(item => { this.insertNode(treeName, item); });
              }

              break;
            }
            case TreeTypeNames.SecurityGroupsPeopleTree: {
              selectedTreeNodes.forEach(item => {
                let node = Data.treePayload.nodeData.treeModel.getNodeById(item);
                if (node) {
                  let nodechild = [];
                  if (node.data.children && node.data.children.length  && operationType != "Remove Hierarchy") {
                    nodechild = node.data.children;
                    node.data.children = [];
                  }
                  if (this.isAddNewSecurityGroup) {
                    index = this.findIndexToDelete(this.entityDetails.peopleEntities, node.data.EntityId);
                    if (index != -1) {
                      this.entityDetails.peopleEntities.splice(index, 1);
                    }
                  }
                  index = this.findIndexToDelete(this.draggedPeopleEntities, node.data.EntityId);
                  if (index == -1) {
                    //this.removedPeopleEntities.push(node.data);
                    this.getChilddata([node.data], this.removedPeopleEntities)
                  }
                  else {
                    this.draggedPeopleEntities.splice(index, 1);
                  }
                  this.sendDataToAngularTree(treeName, TreeOperations.deleteNodeById, node.data.EntityId);
                  nodechild.forEach(item => { this.insertNode(treeName, item); });
                }
              });
              if (eNode) {
                let nodechild = [];
                if (eNode.children && eNode.children.length && operationType != "Remove Hierarchy") {
                  nodechild = eNode.children;
                  eNode.children = [];
                }
                if (this.isAddNewSecurityGroup) {
                  index = this.findIndexToDelete(this.entityDetails.peopleEntities, eNode.EntityId);
                  if (index != -1) {
                    this.entityDetails.peopleEntities.splice(index, 1);
                  }
                }
                index = this.findIndexToDelete(this.draggedPeopleEntities, eNode.EntityId);
                if (index == -1) {
                  //this.removedPeopleEntities.push(eNode);
                  this.getChilddata([eNode], this.removedPeopleEntities)
                }
                else {
                  this.draggedPeopleEntities.splice(index, 1);
                }
                this.sendDataToAngularTree(treeName, TreeOperations.deleteNodeById, eNode.EntityId);
                nodechild.forEach(item => { this.insertNode(treeName, item); });
              }
              break;
            }
          }
          Data.treePayload.nodeData.treeModel.activeNodeIds = {};
        }
      },
        error => {
          console.error('Exception in deleteSecurityGroup$ observable of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + error.message);
          this.logger.logException(new Error('Exception in deleteSecurityGroup$ observable of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + error.message));
        });

      this.authorizationService.AddEntityInSecurityGroup.pipe(this.compUntilDestroyed()).subscribe((Data: any) => {
        if (Data && Data.treePayload && Data.treePayload.nodeData && Data.treePayload.nodeData.data) {
          let nodedata = JSON.parse(JSON.stringify(Data.treePayload.nodeData.data));
          if (nodedata.children && nodedata.children.length) {
            nodedata.children = [];
          }
          let event = { element: { data: { EntityType: nodedata.EntityType } } }
          this.addEntitiesInSecurityGroup([nodedata], event);
        }
      },
        error => {
          console.error('Exception in deleteSecurityGroup$ observable of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + error.message);
          this.logger.logException(new Error('Exception in deleteSecurityGroup$ observable of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + error.message));
        });
    }
    catch (e) {
      console.error('Exception in constructor() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in constructor() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  insertNode(localTreeName, NodeData) {
    try {
      let parentlink = PARENTTREENODE;
      let payload = { "InsertAtPosition": parentlink, "NodeData": NodeData };
      let tab = this.authorizationService.getRolesOrSecurityGroupstab();
      payload["SortByName"] = true;
      this.sendDataToAngularTree(localTreeName, TreeOperations.AddNewNode, payload);
    } catch (ex) {

    }
  }
 




  /**
*
* Used to create payload, which can be send to AngularTreeCoponent to perform various operation on tree nodes, like create full tree, append new node to the existing tree.
* @param {string } treeName: is the name of angular tree.
* @param {string} treeOperationType: is the type of operation, needs to perform on tree node like Create full tree, Add New Node.
* @param {object} payload: is an object which may contain the whole tree data or a single node, based on operation type.
* @example example of createEntitiesTree(payloadJson) method
* 
*  sendDataToAngularTree(treeName, treeOperationType, payload){
*     //todo
*    }
*/
  // sendDataToAngularTree(treeName: string, treeOperationType: string, payload: any) {
  //   try {
  //     var treeMessage: AngularTreeMessageModel = {
  //       "fabric": this.commonService.lastOpenedFabric,
  //       "treeName": [treeName],
  //       "treeOperationType": treeOperationType,
  //       "treePayload": payload,
  //     }
  //     this.commonService.sendDataToAngularTree.next(treeMessage);
  //   } catch (e) {
  //     console.error('exception in sendDataToAngularTree() of SecurityGroupsComponent at time ' + new Date().toString() + '. exception is : ' + e);
  //     this.logger.logException(new Error('Exception in sendDataToAngularTree() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e));
  //   }
  // }


  /**
  *
  * Used to handle node drop event from left/right side tree into Security groups component, and send that node to append in particular tree based on entity type like entities tree, people tree of roles tree.
  * @param {object } event: is dropped node into Security group component.
  * @example example of onDrop(event) method
  * 
  *  onDrop(event){
  *     //todo
  *    }
  */
  onDrop(event) {
    console.log('On Droped Event', event);
    try {
      if (event.element && event.element.treeModel.options.rootId != TreeTypeNames.RoleTree && event.element.treeModel.options.rootId != TreeTypeNames.SecurityGroupsEntitiesTree && event.element.treeModel.options.rootId != TreeTypeNames.SecurityGroupsPeopleTree) {
        let sampObj = [];
        var selectedTreeValues = Object.keys(event.element.treeModel.activeNodeIds);
        if (selectedTreeValues.length > 1) {
          selectedTreeValues.forEach(item => {
            let node = event.element.treeModel.getNodeById(item);
            if (node && node.isActive) {
              sampObj.push(node.data);
            }
          })
          if (sampObj.length == 0) {
            this.sendDataToAngularTree(event.element.treeModel.options.rootId, TreeOperations.deactivateAllAvtiveNode, "");
          }
        } else {
          sampObj.push(event.element.data);
        }
        this.sendDataToAngularTree(event.element.treeModel.options.rootId, TreeOperations.deactivateSelectedNode, "");
        if (sampObj.length > 0) {
          this.addEntitiesInSecurityGroup(sampObj, event);
        }
      }
    }
    catch (e) {
      console.error('Exception in onDrop() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in onDrop() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }


  addEntitiesInSecurityGroup(obj, dropEvent) {
    try {
      console.log(obj, dropEvent);
      let localTreeName;
      for (let i = 0; i < obj.length; i++) {
        let payload = {};
        let draggedNodeData = {};
        draggedNodeData['EntityId'] = obj[i].EntityId;
        draggedNodeData['EntityName'] = obj[i].EntityName;
        draggedNodeData['EntityType'] = obj[i].EntityType;
        draggedNodeData['children'] = obj[i].children && obj[i].children.length ? obj[i].children : [];
        let flag = false;
        let parentlink = PARENTTREENODE;
        if (ALLENTITYTYPES.includes(obj[i].EntityType)) {
          //this.authorizationService.isSecurityGroupModified = true;
          localTreeName = TreeTypeNames.SecurityGroupsEntitiesTree;
          this.formType = ENTITIES;
          if (this.draggedEntities.findIndex(d => d.EntityId == draggedNodeData["EntityId"]) == -1) {
            this.getChilddata([draggedNodeData], this.draggedEntities)
          }

          payload = { "InsertAtPosition": parentlink, "NodeData": draggedNodeData };
          this.sendDataToAngularTree(localTreeName, TreeOperations.AddNewNode, payload);

        }
        else if (obj[i].EntityType == "Person" || obj[i].EntityType == "Company") {
          //this.authorizationService.isSecurityGroupModified = true;
          localTreeName = TreeTypeNames.SecurityGroupsPeopleTree;
          this.formType = ENTITIES;
          if (this.draggedPeopleEntities.findIndex(d => d.EntityId == draggedNodeData["EntityId"]) == -1) {
            this.getChilddata([draggedNodeData], this.draggedPeopleEntities)
          }

          payload = { "InsertAtPosition": parentlink, "NodeData": draggedNodeData };
          this.sendDataToAngularTree(localTreeName, TreeOperations.AddNewNode, payload);
        }
      }


    }
    catch (e) {
      console.error('Exception in addEntititesRolePeopleInSecurityGroup() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in addEntititesRolePeopleInSecurityGroup() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

 

  checkIfAvailable(treeName, draggedNodeData) {
    try {
      var a = this.checkForDuplicateEntity(treeName, draggedNodeData.EntityId);
      if (a) {
        return true;
      }
      else {
        if (draggedNodeData.children && draggedNodeData.children.length > 0) {
          this.deleteOldChildNodes(treeName, draggedNodeData.children);
        }
        return false;
      }
    }
    catch (e) {
      console.error('Exception in checkIfAvailable() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in checkIfAvailable() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  deleteOldChildNodes(treeName, draggedNodeDataArray) {
    try {
      draggedNodeDataArray.forEach(item => {
        if (treeName == TreeTypeNames.SecurityGroupsEntitiesTree) {
          let index = this.entityDetails.entities.findIndex(index => index.EntityId === item.EntityId);
          let index1 = this.draggedEntities.findIndex(index => index.EntityId === item.EntityId);
          if (index >= 0) {
            if (!this.isAddNewSecurityGroup && index1 == -1) {
              //this.removedEntities.push(this.entityDetails.entities[index]);
              this.getChilddata([this.entityDetails.entities[index]], this.removedEntities)
            }
            if (index1 != -1) {
              this.draggedEntities.splice(index1, 1);
            }
            this.entityDetails.entities.splice(index, 1);
            this.sendDataToAngularTree(treeName, TreeOperations.deleteNodeById, item.EntityId);
          }
        }
        else {
          let index = this.entityDetails.peopleEntities.findIndex(index => index.EntityId === item.EntityId);
          let index1 = this.draggedPeopleEntities.findIndex(index => index.EntityId === item.EntityId);
          if (index >= 0) {
            if (!this.isAddNewSecurityGroup && index1 == -1) {
              //this.removedPeopleEntities.push(this.entityDetails.peopleEntities[index]);
              this.getChilddata([this.entityDetails.peopleEntities[index]], this.removedPeopleEntities)
            }
            if (index1 != -1) {
              this.draggedPeopleEntities.splice(index1, 1);
            }
            this.entityDetails.peopleEntities.splice(index, 1);
            this.sendDataToAngularTree(treeName, TreeOperations.deleteNodeById, item.EntityId);
          }
        }
        if (item.children && item.children.length > 0) {
          this.deleteOldChildNodes(treeName, item.children);
        }
      })
    }
    catch (e) {
      console.error('Exception in deleteOldChildNodes() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in deleteOldChildNodes() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  canDeactivate(): Observable<boolean> {
    try {
      var response;
      let message = DISCARD_CHANGES;
      this.commonService.isSubHeaderClick = false;
      this.openPopUpDialog(message, PopupOperation.AlertConfirm);
      response = this.dialogRef.componentInstance.emitResponse.map(res => {
        //this.authorizationService.isSecurityGroupModified = false;
        this.commonService.isSubHeaderClick = true;
        this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.deactivateSelectedNode, "");
        return true;
      })
      return response;
    }
    catch (e) {
      console.error('Exception in canDeactivate() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in canDeactivate() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  callAlertPopUp() {
    try {
      this.formType = !this.formType ? ENTITIES : this.formType;
      if (this.selectedEntitiesByUser.length == 1) {
        this.openPopUpDialog(ALREADY_EXIST(this.selectedEntitiesByUser[0], this.formType), false, "SecurityGroupPopUpForm");
      } else {
        this.openPopUpDialog(SOME_ALREADY_EXIST(this.formType.toLowerCase(), this.formType), false, "SecurityGroupPopUpForm");
      }
    }
    catch (e) {
      console.error('Exception in callAlertPopUp() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in callAlertPopUp() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  /**
  *
  * Used to handle click event on form header.
  * @param {object } data: is event which is occured on click on form header.
  * @example example of clickEvent(data) method
  * 
  *  clickEvent(data){
  *     //todo
  *    }
  */
  clickEvent(data) {
    try {
      switch (data) {
        case 'close': {
          // if (this.authorizationService.isSecurityGroupModified || (this.currentGroupName != this.securityGroupName)) {
          //   this.openPopUpDialog(DISCARD_CHANGES, true, "SecurityGroupForm");
          // } else {
          //   this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.deactivateSelectedNode, "");
          //   this.cancel();
          // }
          this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.deactivateSelectedNode, "");
          this.cancel();
        }
          break;
      }
    }
    catch (e) {
      console.error('Exception in ClickEvent() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in clickEvent() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  /**
  *
  * Used to find the duplicate entity if already exist in the entities array or peopleentities array and return boolean value based on that.
  * @param {object } entities: is the entities array which is containing all the entities 
  * @param {object } draggedNodeData: is the entity for that we will find the duplicate entity in array of entities.
  * @example example of checkForDuplicateEntity(entities) method
  * 
  *  checkForDuplicateEntity(entities){
  *     //todo
  *    }
  */
  checkForDuplicateEntity(treeName, entityId) {
    try {
      var tree: any = this.authorizationService.FabricTreeInstance.get(treeName);
      let node: any = undefined
      if (tree) {
        node = this.commonService.getNodeByEntityId(entityId, tree.treeModel);
      }
      return node;
    }
    catch (e) {
      console.error('Exception in checkForDuplicateEntity() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in checkForDuplicateEntity() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }


  /**
  * Initialize the directive/component after Angular first displays the data-bound properties
  * Indicates that Angular is finished creating the component & sets the directive/component's input properties
  * here we are using For downtime reasons.
  * @example example of ngOnInit() method
  *
  *  ngOnInit(){
  *     //todo
  *    }
  */
  ngOnInit() {
    
    this.route.params.pipe(this.compUntilDestroyed()).subscribe((params) => {
      try {
        this.params = JSON.parse(JSON.stringify(params));
        let queryParams = this.route.snapshot.queryParams;
        this.fabricName = this.router ? this.commonService.getFabricNameByUrl(this.router.url) : FABRICS.SECURITY;
        this.leftNavEntityId = this.params.LeftNavEntityId;
        this.isAddNewSecurityGroup = this.params && this.params.OperationType == 'Edit' ? false : true;
        if(queryParams.tab == FABRICS.CONSTRUCTION){
          this.sgForm = this.constructionSGForm;
          let entitydata = this.commonService.getEntitiesById(this.leftNavEntityId);
          this.sgForm.body.forEach(item => {
            if(item.id == 'Premico' && entitydata){
              item.title = entitydata.EntityName;
            }
          })
          this.enable = true;
          let subtype = queryParams.subtype ? queryParams.subtype : null;
          this.formSelectFormControl.setValue(subtype,{emitEvent:false});
        }
        if(!this.isAddNewSecurityGroup){
          this.authorizationService.securityGroupId = this.params.SecurityGroupID;
          this.getSecurityGroupDetails();
        }
      }
      catch (e) {
        console.error('Exception in route.params  of securityGroupComponent at time ' + new Date().toString() + '. Exception is : ' + e);
        this.logger.logException(new Error('Exception in route.params  of securityGroupComponent at time ' + new Date().toString() + '. Exception is : ' + e));
      }
    });
    this.route.queryParams.subscribe(queryParams =>{
      this.activateTreeNode(queryParams.tab);
    });
   }

   activateTreeNode(tab){
    setTimeout(() => {
      if(this.params){
        let EntityId = this.params.LeftNavEntityId;
        let SecurityGroupId = this.params.SecurityGroupID;
        if(SecurityGroupId){
          this.authorizationService.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions,TreeOperations.ActiveSelectedNode,SecurityGroupId);
        }
        if(EntityId){
          this.authorizationService.sendDataToAngularTree(tab,TreeOperations.ActiveSelectedNode,EntityId);
        }
      }
    }, 1500);
  }

  /**
  * To initialize the component and Respond after Angular initializes the component's views and child views
  * @example example of ngAfterViewInit() method
  * 
  * ngAfterViewInit(){
   *  // Todo
   * }
   */
  ngAfterViewInit() {
    this.ref.detectChanges();
  }

  createEntityTypeControl(){
    let capability = this.commonService.getCapabilityNamebasedontabforSecurity();
    this.formSelectFormControl.valueChanges.pipe(this.compUntilDestroyed()).subscribe((value:string)=>{
       this.emitEventToChild({id:'ngselect',value:value})
    })
    if(capability == FABRICS.CONSTRUCTION)
    this.selectOptions = AppConsts.ConstructionSGTreeList;
  }

  findIndexToDelete(treeEntities, nodeToDalete) {
    try {
      let index = treeEntities.findIndex(index => index.EntityId === nodeToDalete);
      return index;
    }
    catch (e) {
      console.error('Exception in findIndexToDelete() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in findIndexToDelete() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }


  sendRequestToReadSecuritygGroupInfo() {
    try {
      this.isAddNewSecurityGroup = false;
      this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.deactivateSelectedNode, "");
      this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.ActiveSelectedNode, this.authorizationService.securityGroupId);
      this.authorizationService.lastOpenSecurityGroupId = this.authorizationService.securityGroupId;
      let payload = { "EntityId": this.authorizationService.securityGroupId, "CapabilityId": this.commonService.getCapabilityidbasedontabforSecurity(), "capabilityName": this.commonService.getCapabilityNamebasedontabforSecurity() };
      this.commonService.sendMessageToServer(JSON.stringify(payload), 'Authorization', 'securitygroupinfo', EntityTypes.SecurityGroup, MessageKind.READ, Routing.OriginSession, null, this.commonService.lastOpenedFabric);
    }
    catch (e) {
      console.error('Exception in sendRequestToReadSecuritygGroupInfo() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in sendRequestToReadSecuritygGroupInfo() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  /**
 *
 * Used to validate existing group name and duplicate group name in  Security group.
 * @example example of geoupNameValidation() method
 * 
 *  geoupNameValidation(){
 *     //todo
 *    }
 */

  groupNameValidation(groupName) {
    try {
      let index;
      if (this.isGroupNameExist) {
        this.isGroupNameExist = false;
      } else if (this.authorizationService.securityGroupsData) {
        this.authorizationService.securityGroupsData.forEach(group => {
          if (group.EntityName == groupName) {
            this.isGroupNameExist = true;
          } else if (group.EntityName == this.currentGroupName && this.currentGroupName != this.securityGroupName && this.currentGroupName != "") {
            index = this.authorizationService.securityGroupsData.indexOf(group);
            group.EntityName = this.securityGroupName;
          }
        })
      }
      if (this.isGroupNameExist && index != undefined && index != -1) {
        this.authorizationService.securityGroupsData[index]["EntityName"] = this.currentGroupName;
      }
    } catch (e) {
      console.error('Exception in groupNameValidation() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in groupNameValidation() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  /**
  *
  * Used to create a new Security group or update an existing Security group.
  * @example example of createOrUpdateSecurityGroup() method
  * 
  *  createOrUpdateSecurityGroup(){
  *     //todo
  *    }
  */

  createOrUpdateSecurityGroup() {
    try {
      if (!this.securityGroupName || this.securityGroupName.trim() == "") {
        this.openPopUpDialog([SECURITYGROUPS_EMPTY_NAME], false);
        return;
      }
      else if (this.currentGroupName != this.securityGroupName) {
        this.groupNameValidation(this.securityGroupName);
        if (this.isGroupNameExist) {
          this.isGroupNameExist = false;
          this.openPopUpDialog([ALREADY_EXIST_GROUP(this.securityGroupName)], false);
          return;
        }
      }
      if (this.securityGroupName) {
        let entities = [];
        let peopleEntities = [];
        let removedPeopleEntities = [];
        let removedEntities = [];
        let securitygroupId;

        this.getChildId(this.draggedEntities, entities);

        this.getChildId(this.draggedPeopleEntities, peopleEntities, "PeopleEntities");

        this.getChildId(this.removedEntities, removedEntities);

        this.getChildId(this.removedPeopleEntities, removedPeopleEntities, "PeopleEntities");

        let payload:any;
        let messageKind:string;
        let startStatus:string;
        let endStatus:string;
        if (this.isAddNewSecurityGroup) {
          if(this.leftNavEntityId){
            entities.push(this.leftNavEntityId);
          }
          messageKind = MessageKind.CREATE;
          startStatus = AppConsts.CREATING;
          endStatus = AppConsts.CREATED;
          //this.authorizationService.isNewSecurityGroupCreated = true;
          securitygroupId = this.params.SecurityGroupID;
          payload = this.getSecurityGroupPayload(securitygroupId, entities, peopleEntities, removedEntities, removedPeopleEntities);
          payload['SecurityGroupName'] = this.securityGroupName;
        }
        else {
          messageKind = MessageKind.UPDATE;
          startStatus = AppConsts.UPDATING;
          endStatus = AppConsts.UPDATED;
          securitygroupId = this.authorizationService.securityGroupId;
          payload = this.getSecurityGroupPayload(securitygroupId, entities, peopleEntities, removedEntities, removedPeopleEntities);
          if (this.securityGroupName != this.authorizationService.securityGroupName) { //changed sgroupname
            this.authorizationService.securityGroupName = this.securityGroupName;
            payload['SecurityGroupName'] = this.securityGroupName;

          }
        }
        if(this.formSelectFormControl.value){
          payload['SubType'] = this.formSelectFormControl.value;
        }
        let imessageModel: IModelMessage = this.commonService.getMessageWrapper({Payload:JSON.stringify(payload),DataType:MessageDataTypes.SECURITYGROUPS,EntityType:EntityTypes.SECURITYGROUP,EntityID:securitygroupId,MessageKind:messageKind});
        this.commonService.postDataToBackend(imessageModel,startStatus ).subscribe(data => {
          this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, endStatus, AppConsts.LOADER_MESSAGE_DURATION);
          if(messageKind == MessageKind.CREATE){
            let node = {EntityId:payload['SecurityGroupId'],EntityName:payload['SecurityGroupName'],SubType:payload['SubType']};
            this.authorizationService.getSecurityGroupDetails(node);
          }
        });

      
        let newSecurityGroup: any = {};
        newSecurityGroup.EntityId = securitygroupId;
        newSecurityGroup.EntityName = this.securityGroupName;
        newSecurityGroup.EntityType = EntityTypes.SecurityGroup;
        if (imessageModel.MessageKind == MessageKind.CREATE) {
          let parentId =  "Security Groups";
          if(this.formSelectFormControl.value){
             parentId = this.formSelectFormControl.value;
             newSecurityGroup.SubType = this.formSelectFormControl.value;
          }
          let newPayload = { "InsertAtPosition": parentId, "NodeData": newSecurityGroup };
          this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.AddNewNode, newPayload);
          this.authorizationService.securityGroupsData.push(newSecurityGroup);
          //activate created node
          this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.ActiveSelectedNode, securitygroupId);
        }
        else if (imessageModel.MessageKind == MessageKind.UPDATE) {
          let newPayload = { "EntityId": securitygroupId, "NodeData": newSecurityGroup };
          this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.updateNodeById, newPayload);
        }
        //this.authorizationService.isSecurityGroupModified = false;
        this.currentGroupName = this.securityGroupName
        this.isAddNewSecurityGroup = false;
        this.emptyLocalVariables();
      }
    }
    catch (e) {
      console.error('Exception in CreateSecurityGroup() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in CreateSecurityGroup() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  emptyLocalVariables() {

    this.draggedEntities = [];
    this.draggedPeopleEntities = [];
    this.removedEntities = [];
    this.removedPeopleEntities = [];
  }
  getSecurityGroupPayload(securitygroupId: string, entities, peopleEntities, removedEntities, removedPeopleEntities) {
    let payload = { 
      "Capability": {
        "CapabilityId": this.commonService.getCapabilityidbasedontabforSecurity(),
        "CapabilityName": this.commonService.getCapabilityNamebasedontabforSecurity()
      },
      'SecurityGroupId': securitygroupId,
      'Entities': entities,
      'PeopleCompanies': peopleEntities,
      'RemovedEntities': removedEntities,
      'RemovedPeopleCompanies': removedPeopleEntities
    };
    return payload;
  }
  getSecurityGroupDetails() {
    this.isAddNewSecurityGroup = false;
    this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.deactivateSelectedNode, "");
    this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.ActiveSelectedNode, this.authorizationService.securityGroupId);
    this.authorizationService.lastOpenSecurityGroupEntityId = this.authorizationService.securityGroupId;
    //payload 
    let imessageModel: IModelMessage = this.commonService.getMessageWrapper({DataType:MessageEntityTypes.SECURITYGROUPS,EntityType:EntityTypes.SECURITYGROUP,EntityID:this.authorizationService.securityGroupId,MessageKind:MessageKind.READ});
    this.commonService.postDataToBackend(imessageModel, AppConsts.READING).subscribe(data => {
      console.log('SecurityGroup Details', data);
      this.onSecurityGroupDetailsReceive(data);
      this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, '', AppConsts.LOADER_MESSAGE_DURATION);
    });

  }
  onSecurityGroupDetailsReceive(data) {
    console.log('SecurityGroup Details', data);
    this.entityDetails = {
      entities: [],
      peopleEntities: []
    };
    this.draggedEntities = [];
    this.draggedPeopleEntities = [];
    this.removedEntities = [];
    this.removedPeopleEntities = [];
    this.authorizationService.listOfEntityForSecurityGroupForm = [];
    let sGroupDetails = data['SGroupDetails'][0];
    this.authorizationService.securityGroupName = this.securityGroupName = this.currentGroupName = sGroupDetails['EntityName'];
    this.constructSecurityGroupEntitiesTree(sGroupDetails['Entities']);
    this.constructSecurityGroupPeopleTree(sGroupDetails['People']);


  }
  constructSecurityGroupEntitiesTree(entities: Array<any>) {
    let entitiesArray = entities ? entities : [];
    entitiesArray = this.updateEntitiesTreeData(entitiesArray);
    this.sendDataToAngularTree(TreeTypeNames.SecurityGroupsEntitiesTree, TreeOperations.createFullTree, entitiesArray);

  }

  updateEntitiesTreeData(entitiesArray:Array<any>){
    let entities = entitiesArray;
    entities.forEach(item =>{ 
      if(item.NestedUnder){
        item.Parent = [];
        item.Parent.push({EntityId:item.NestedUnder,EntityName:item.NestedUnder});
        delete item.NestedUnder;
      }
     });
    return this.commonService.GetParentChildRelationship(entities);
  }

  constructSecurityGroupPeopleTree(peopleEntities: Array<any>) {
    let rootNode = [];
    if (peopleEntities) {
      rootNode = peopleEntities;
      rootNode.forEach(item => {
        if (item.hasOwnProperty("OfficeParent")) {
          item["Parent"] = item["OfficeParent"];
          delete item["OfficeParent"];
        } else if (item.hasOwnProperty("PersonParent")) {
          item["Parent"] = item["PersonParent"];
          delete item["PersonParent"];
        }
      });
      rootNode = this.commonService.GetParentChildRelationship(rootNode);
      this.entityDetails.peopleEntities = rootNode;
    }
    this.sendDataToAngularTree(TreeTypeNames.SecurityGroupsPeopleTree, TreeOperations.createFullTree, rootNode);
  }

  getChildId(list: Array<any>, idlist: Array<any>, typeOf?: string) {

    list.forEach(item => {
      if (item.EntityType == EntityTypes.List) {
        idlist.push(item.EntityId)
      }
      else if (item.children) {
        if (typeOf && typeOf == "PeopleEntities") {
          idlist.push({ "EntityId": item.EntityId, "EntityType": item.EntityType })
        }
        else {
          idlist.push(item.EntityId)
        }
        // else if (item['EntityId'] != EntityTypes.HaulLocation) {
        //   idlist.push(item.EntityId)
        // }
        this.getChildId(item.children, idlist, typeOf)
      }
      else {
        if (typeOf && typeOf == "PeopleEntities") {
          idlist.push({ "EntityId": item.EntityId, "EntityType": item.EntityType })
        }
        else {
          idlist.push(item.EntityId)
        }
      }
    })
  }

  getChilddata(list: Array<any>, entitylist: Array<any>) {
    list = JSON.parse(JSON.stringify(list));
    list.forEach(item => {
      if (item.children && item.EntityType != EntityTypes.List) {
        let children = item.children;
        item.children = [];
        if (!this.findEntity(entitylist, item['EntityId'])) {
            entitylist.push(item)
        }
        this.getChilddata(children, entitylist)
      }
      else if (!this.findEntity(entitylist, item['EntityId'])) {
        entitylist.push(item)
      }
    })
  }

  findEntity(entitylist, EntityId) { return entitylist.findIndex(item => item.EntityId == EntityId) >= 0 };

  checkForDuplicateEntityForCreateNewSGroup(eId, removedEntities) {
    try {
      let isEntityAvailable: boolean = false;
      removedEntities.forEach(item => {
        if (item == eId) {
          isEntityAvailable = true;
        }
      })
      return isEntityAvailable;
    }
    catch (e) {
      console.error('Exception in checkForDuplicateEntityForCreateNewSGroup() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in checkForDuplicateEntityForCreateNewSGroup() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }



  /**
  *
  * Used to open a confirmation dialog pop up to confirm with user about the current action.
  * @param {object } message: is array of string which will shown in confirmation pop up as message. 
  * @example example of openPopUpDialog(message) method
  * 
  *  openPopUpDialog(message){
  *     //todo
  *    }
  */
  openPopUpDialog(messageToshow, isSave, callingFrom?): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: isSave,
        content: [messageToshow],
        subContent: [],
        operation: PopupOperation.PermissionAlertConfirm,
        callingFrom: callingFrom
      };

      let matConfig = new MatDialogConfig();
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      matConfig.id = messageToshow;
      if (!this.dialog.getDialogById(messageToshow)) {
        this.dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
        this.dialogRef.componentInstance.emitResponse.pipe(this.compUntilDestroyed()).subscribe((data: any) => {
          if (data.eventId == 'save' && data.callingFrom == "SecurityGroupForm") {
            this.sendDataToAngularTree(TreeTypeNames.securityGroupsTreeForPermissions, TreeOperations.deactivateSelectedNode, "");
            this.cancel();
          }
        });
        this.dialogRef.componentInstance.emitCancelResponse.pipe(this.compUntilDestroyed()).subscribe((data: any) => {
          if (data.eventId == 'close' && data.callingFrom == "SecurityGroupPopUpForm") {
            this.sendDataToAngularTree(this.securityGroupTreeName, TreeOperations.deactivateSelectedNode, "");
            this.selectedEntitiesByUser = [];
          }
        });
      }
    }
    catch (ex) {
      console.error('Exception in openPopUpDialog() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + ex);
      this.logger.logException(new Error('Exception in openPopUpDialog() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + ex));
    }
  }

  /**
  *
  * Used to close the currently new/open Security Groups form.
  * @example example of cancel() method
  * 
  *  cancel(){
  *     //todo
  *    }
  */
  cancel() {
    try {
      this.authorizationService.lastOpenSecurityGroupId = "";
      //this.authorizationService.isSecurityGroupModified = false;
      this.authorizationService.securityGroupId = '';
      let url = FABRICS.SECURITY;
      let queryParams = JSON.parse(JSON.stringify(this.route.snapshot.queryParams))
      queryParams = this.authorizationService.getQueryParams(queryParams.leftnav,queryParams.tab,queryParams.rightnav,queryParams.righttab,queryParams.enabledmode,queryParams.subtype,queryParams.leftnaventityid);
      this.authorizationService.routerNavigate([url], queryParams)
      // this.draggedEntities = [];
      // this.draggedPeopleEntities = [];
      // this.removedEntities = [];
      // this.removedPeopleEntities = [];
      // this.TableList = [];
      // this.authorizationService.listOfEntityForSecurityGroupForm = [];
      // this.ListArray = [];
      // this.commonService.layoutJSON.OverLayRightTree = false;
      // this
      this.isAddNewSecurityGroup = false;
      this.authorizationService.closeForm();
    }
    catch (e) {
      console.error('Exception in cancel() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in cancel() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }

  createEntitiesTreeBasedOnALLTree(fabric, entitiesPayload, treeName): Observable<any[]> {
    try {
      let rootNode = [];
      let treeEvent = this.commonService.getAngularTreeEvent("Security", [TreeTypeNames.BusinessIntelligence], TreeOperations.GetTreeInstance).map((responseModel: TreeDataEventResponseModel) => {
        let treeNames: Array<string> = responseModel.treeName;
        let tree = responseModel.treePayload;
        entitiesPayload.forEach(element => {
          switch (element.EntityType) {
            case "List":
              let ele = element;
              ele["children"] = element["list.entity"] ? element["list.entity"] : [];
              ele["children"] = ele["children"].filter(item => item["EntityName"] != undefined)
              delete element["list.entity"];
              rootNode.push(ele);
              break;
          }
        });
        if (treeNames.indexOf(TreeTypeNames.BusinessIntelligence) > -1 && tree && tree.treeModel && tree.treeModel.nodes) {
          entitiesPayload.forEach(element => {
            switch (element.TypeOf) {
              case "Category":
              case "DashBoard":
                let entitydata = this.commonService.getEntitiesById(element.EntityId);
                if (entitydata) {
                  rootNode.push(entitydata);
                }
                break;
            }
          });
          rootNode = this.commonService.GetParentChildRelationship(rootNode);
          entitiesPayload = rootNode;
        }
        if (rootNode) {
          rootNode.forEach(item => {
            item["isParent"] = true;
          })
        }
        this.commonService.sendDataToAngularTree1(treeName, 'createTree', rootNode.reverse());
        return rootNode;
      })

      return rootNode.length ? of(rootNode) : treeEvent;
    }
    catch (e) {
      console.error('Exception in createEntitiesTreeBasedOnALLTree() of CommonserService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in createEntitiesTreeBasedOnALLTree() of CommonserService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }



  /**
   * Cleanup just before Angular destroys the directive/component.
   * Unsubscribe Observables and detach event handlers to avoid memory leaks
   */
  ngOnDestroy() {
    try {
      this.entityDetails = {
        entities: [],
        peopleEntities: []
      };
      this.draggedEntities = [];
      this.draggedPeopleEntities = [];
      this.removedEntities = [];
      this.removedPeopleEntities = [];
      this.TableList = [];
      this.authorizationService.listOfEntityForSecurityGroupForm = [];
      this.ListArray = [];
      this.authorizationService.securityGroupId = "";
      this.authorizationService.securityGroupName = "";
      this.takeUntilDestroyObservables.next();
      this.takeUntilDestroyObservables.complete();
    }
    catch (e) {
      console.error('Exception in ngOnDestroy() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in ngOnDestroy() of SecurityGroupsComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  sendDataToAngularTree(treeName: string, treeOperationType: string, payload: any) {
    try {
      var treeMessage: AngularTreeMessageModel = {
        "fabric": this.commonService.lastOpenedFabric,
        "treeName": [treeName],
        "treeOperationType": treeOperationType,
        "treePayload": payload,
      }
      this.commonService.sendDataToAngularTree.next(treeMessage);
    } catch (e) {
      console.error('exception in sendDataToAngularTree() of SecurityGroupsComponent at time ' + new Date().toString() + '. exception is : ' + e);
    }
  }
  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables);
  }

  emitEventToChild(event) {
    this.emitEventsToChild.next(event);
  }

  OutputEmitter(event){
    let buttonEvent: string = event.buttonEvent ? event.buttonEvent.toLowerCase() : null;
    switch(buttonEvent){
      case NEW_ROWS:
        this.addEntities(event.data,event.schemaPage);
        break;
      case DELETE_ROWS:  
      this.deleteEntities(event.data,event.schemaPage);
        break;
    }
  }

  addEntities(data:any,schemaPage:string){
    if(schemaPage == ENTITIES){
       let rows:Array<any> = data.rows;
       rows.forEach(row => {
        let existIndex = this.draggedEntities.findIndex(item => item.EntityId == row.EntityId);
        if(existIndex == -1){
          this.draggedEntities.push(row);
        }
        let removeIndex = this.removedEntities.findIndex(item => item.EntityId == row.EntityId);
        if(removeIndex >= 0){
          this.removedEntities.splice(removeIndex, 1);
        }
      });
    }
    else if(schemaPage == "People&Companies"){
      let rows:Array<any> = data.rows;
      rows.forEach(row => {
        let existIndex = this.draggedPeopleEntities.findIndex(item => item.EntityId == row.EntityId);
        if(existIndex == -1){
          this.draggedPeopleEntities.push(row);
        }
        let removeIndex = this.removedPeopleEntities.findIndex(item => item.EntityId == row.EntityId);
        if(removeIndex >= 0){
          this.removedPeopleEntities.splice(removeIndex, 1);
        }
      });
    }
  }

  deleteEntities(data:any,schemaPage:string){
    if(schemaPage == ENTITIES){
      let rows:Array<any> = data.rows;
      rows.forEach(row => {
        let existIndex = this.removedEntities.findIndex(item => item.EntityId == row.EntityId);
        if(existIndex == -1){
          this.removedEntities.push(row);
        }
        let removeIndex = this.draggedEntities.findIndex(item => item.EntityId == row.EntityId);
        if(removeIndex >= 0){
          this.draggedEntities.splice(removeIndex, 1);
        }
      });    }
    else if(schemaPage == "People&Companies"){
      let rows:Array<any> = data.rows;
      rows.forEach(row => {
        let existIndex = this.removedPeopleEntities.findIndex(item => item.EntityId == row.EntityId);
        if(existIndex == -1){
          this.removedPeopleEntities.push(row);
        }
        let removeIndex = this.draggedPeopleEntities.findIndex(item => item.EntityId == row.EntityId);
        if(removeIndex >= 0){
          this.draggedPeopleEntities.splice(removeIndex, 1);
        }
      });    }
  }

}
