import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { PopUpAlertComponent } from 'ClientApp/app/components/common/PopUpAlert/popup-alert.component';
import { FDCIcons, PiggingIcons, IconJson } from '../../../globals/icons';
import { CommonService } from '../../../globals/CommonService';
//constants and models class imports
import { FABRICS, FabricsNames, TreeOperations, TreeTypeNames } from '../../../globals/Model/CommonModel';
//services imports
import { AuthorizationService } from './../services/authorization.service';
import { Subject } from 'rxjs';
import { Routing } from '../../../globals/Model/Message';
import { ROLES } from '../services/constants/module.constants';



@Component({
  selector: 'Security-fabric',
  templateUrl: './Security-fabric.component.html',
  styleUrls: ['./Security-fabric.component.scss']
})
export class SecurityFabricComponent implements OnInit {
  capability = FABRICS.SECURITY;
  leftSideIconsData = [];
  /**
 * queryParams
 */
  queryParams: any;
  /**
 * capabilityName
 */
  capabilityName: any;
  /**
 * activeSideHeader
 */
  activeSideHeader = "";
  takeUntilDestroyObservables = new Subject();

  constructor(public authorizationService: AuthorizationService, public sanitizer: DomSanitizer, public commonService: CommonService, public router: Router, public iconRegistry: MatIconRegistry, public dialog: MatDialog, private route: ActivatedRoute) {
    this.commonService.lastOpenedFabric = FABRICS.SECURITY;
    this.checkAssignCapabilityId();
    this.route.queryParams.subscribe(queryParams => {
      this.authorizationService.queryParams = JSON.parse(JSON.stringify(queryParams));
      setTimeout(() => {
          this.authorizationService.SecurityTreeInitilization();
          if(queryParams.rightnav === "true" && queryParams.righttab == 'Security Groups'){
            let rightTab = this.commonService.rightSideCapabilityList.find(item => item.title == queryParams.righttab && !item.routerLinkActive );
            if(rightTab){
              this.commonService.enablerightTreeTab.next(rightTab);
              if(queryParams.tab && queryParams.leftnaventityid){
                this.authorizationService.sendDataToAngularTree(queryParams.tab,TreeOperations.ActiveSelectedNode,queryParams.leftnaventityid);
              }
            }
          }
      },1000);
    });
  }


  checkAssignCapabilityId() {

    if (this.commonService.capabilityFabricId == null || this.commonService.capabilityFabricId == undefined) {
      this.commonService.capabilityFabricId = this.commonService.FabricCapabilityIdAtLoginTime;
      if (this.commonService.FabricCapabilityIdAtLoginTime == null || this.commonService.FabricCapabilityIdAtLoginTime == undefined) {
      }
    }
  }

  registerIcon() {
    var Icons = Object.assign(FDCIcons, PiggingIcons, IconJson);
    for (let element in Icons) {
      this.iconRegistry.addSvgIcon(
        element,
        this.sanitizer.bypassSecurityTrustResourceUrl(Icons[element]));
    }
  }


  ngOnInit() {
    this.registerIcon()
    this.commonService.changeFavIcon(FABRICS.SECURITY);
    let mode = this.authorizationService.getRolesOrSecurityGroupsMode();
    if (mode == ROLES) {
      this.commonService.leftHeaderConfig.forEach(element => {
        element.show = !element.show;
      });
    }
  }

  
  ngOnDestroy(): void {
    this.authorizationService.Activetab = '';
  }

}
