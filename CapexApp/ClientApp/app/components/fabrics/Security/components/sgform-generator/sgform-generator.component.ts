
import { Component, EventEmitter, Inject, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'ClientApp/app/components/globals/CommonService';
import { IModelMessage, MessageModel } from 'ClientApp/app/components/globals/Model/Message';
import { BehaviorSubject, of, Subject, combineLatest, Observable } from 'rxjs';
import { takeUntil, filter, debounceTime, delay, first, scan, switchMap, finalize } from 'rxjs/operators';
import { MessageKind, FABRICS, Datatypes, TreeTypeNames, MessagesCount, SERVICETYPE, ReportType } from '../../../../globals/Model/CommonModel';
import { EntityTypes } from '../../../../globals/Model/EntityTypes';
import { Routing } from '../../../../globals/Model/Message';
import { setData, getInlineDataPointer, getFormVariables, getTableName, getNode, getQuery, getQueryParam, getKeyValuePair, getDataPointer, getSearchlistBasedOnType, getLowerCase } from '../../../../globals/helper.functions';
import { UUIDGenarator } from 'visur-angular-common';
import { CLOSE, SEARCH, THEME_CHANGE, PHYSICAL, AppConsts, TREE_ACTION, ENITTY_ID, FormOperationTypes, MENU_ID, OPERATION_TYPE, PARENT_ID, SCHEMA, SEARCHINPUT, FILTER, SELECT, SAVE, ROWS_DROPPED, DELETE, DELETE_ALL, EXPAND_ALL, CONTEXTMENU, NEW_ROWS, DELETE_ROWS, PREMICO, ENTITIES, PEOPLEANDCOMPANIES } from '../../../../common/Constants/AppConsts';
import { WORKER_TOPIC } from 'ClientApp/webWorker/app-workers/shared/worker-topic.constants';
import { FilterDialogComponent } from 'ClientApp/app/components/common/filter-dialog/filter-dialog.component';
import { SgformGeneratorService } from './sgform-generator.service'
import { JsonSchemaFormComponent } from '@visur/formgenerator-core';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { isNullOrUndefined } from 'util';
import { MessageEntityTypes } from '../../services/constants/message.constants';
const defaultOptions: any = {
  addSubmit: false, // Add a submit button if layout does not have one
  debug: false, // Don't show inline debugging information
  loadExternalAssets: true, // Load external css and JavaScript for frameworks
  returnEmptyFields: false, // Don't return values for empty input fields
  setSchemaDefaults: true, // Always use schema defaults for empty fields
  defautWidgetOptions: { feedback: true }, // Show inline feedback icons,
  framework: 'material-design',
  selectedLanguage: 'en',
  navigateBackOnSubmit: true,
  ngSelectDefaultSearchable: false,
  timeFormat: 24,
  dataTableHeaderHeight: 42,
  dataTableRowHeight: 42,
  togglePosition: 'before',
  selectionType: 'multi',
  allowContextMenu: true
};



@Component({
  selector: 'app-sgform-generator',
  templateUrl: './sgform-generator.component.html',
  styleUrls: ['./sgform-generator.component.scss']
})
export class SGFormGeneratorComponent implements OnInit {

    @Input() userId;
    @Input() header: any;
    @Input() layout: any;
    @Input() data: any;
    @Input() options: any;
    @Input() schema: any;
    @Input() roles: any;
    @Input() images: any;
    @Input() userRoles: any;
    @Input() operationType;
    @Input() schemaPage = null;
    @Input() tableName = null;
    @Input() nestedSchema;
    @Input() search;
    @Input() parentPage;
    @Input() readDataFromOtherSource;
    @Input() queries;
    @Input() updateData;
    @Input() isCompleted = false;
    @Input() SCHEMA_JSON: any = {};
    @Input() JSON_DATA;
    @Input() breadcrumbDisplayName;
    @Input() formulas;
    @Input() title;
    @Input() $state;
    @Input() jsonData;
    @Input() IsEntityManagementForm;
    @Input() tab;
    @Input() EntityType: string;
    @Input() EntityId;
    @Input() SecurityGroupId;
    @Input() EntityName;
    @Input() IsEntityMgmtDrag;
    @Input() commonInputVariable;
    @Input() isNewEntity;
    @Input() LocationData;
    @Input() myForm;
    @Input() payloadData: any;
    @Input() queryParams;
    @Input() tableNameOrType;
    @Input() events: Observable<any>;
    @Input() droppedRowsMapping:Array<any>;
    @Output() applicationEvent = new EventEmitter();
    @ViewChild(JsonSchemaFormComponent) formGenerator: JsonSchemaFormComponent;
    @Output() commonOutputEmitter = new EventEmitter();
    dataBufferObj:any ={};
    takeUntilDestroyObservables = new Subject();
    popupFormType: string;
    livedata: any;
    @Input()
    set expanded(value) {
      this.setExpandCollapse(value);
    }
    parentEntityId;
    isPopupForm = false;
    /// assgin value to menuid. if the page is root/first page/initial page
    menuId;
    previousRoute: string;
    dialogRef: MatDialogRef<any, any>;
    //adminSchema;
    childRouteParameters: any;
    wrapper: MessageModel = new MessageModel()
    schemaData: any;
    queryParameters: any;
    isFilterActive = false;
    activeFilterName:string;
    filterQyery:string;
  
  constructor(private sgformGeneratorService : SgformGeneratorService ,private route: ActivatedRoute, private commonService: CommonService, public dialog: MatDialog, public router: Router) {

  }

  ngOnInit(): void {
    this.ObservablesSubscription();
    this.routeSubscription();
  }

  ObservablesSubscription(){
    if (this.events) {
      this.events.pipe(this.compUntilDestroyed()).subscribe((event) => this.manualEvent(event));
    }
  }

  routeSubscription(){
    this.route.queryParams
      .filter(d => this.commonService.getFabricNameByUrl(this.router.url) == FABRICS.SECURITY)
      .pipe(this.compUntilDestroyed())
      .subscribe((queryParams) => {
        try {
          this.queryParameters = JSON.parse(JSON.stringify(queryParams));;
          if (this.queryParameters) {
            this.EntityType = this.removeSpace(this.queryParameters.subtype);
            this.schemaPage = this.getSchemaPage(this.EntityType,this.schemaPage);
          }
        }
        catch (e) {
          this.commonService.appLogException(new Error('Exception inroute.queryParams subscriber of ngOnInit of ConstructionFormGeneratorComponent at time ' + new Date().toString() + '. Exception is : ' + e));
        }
      });

    this.route.params
      .filter(d => this.commonService.getFabricNameByUrl(this.router.url) == FABRICS.SECURITY)
      .pipe(this.compUntilDestroyed())
      .subscribe((params) => {
        try {
          this.layout = null;
          this.childRouteParameters = JSON.parse(JSON.stringify(params));;
          this.EntityId = this.childRouteParameters.LeftNavEntityId
          this.SecurityGroupId = this.childRouteParameters.SecurityGroupID;
          this.isFilterActive = false;
          // new form default will be empty
          if(this.childRouteParameters.OperationType == "New"){
            this.EntityType = null; 
          }
          else if (this.EntityType) {
            this.initializeSchema();
          }
        }
        catch (e) {
          this.commonService.appLogException(new Error('Exception inroute.queryParams subscriber of ngOnInit of ConstructionFormGeneratorComponent at time ' + new Date().toString() + '. Exception is : ' + e));
        }
      });

  }

  initializeSchema(defaultOption?) {

    this.options = { ...defaultOptions };
    let schema$ = this.getSchemaFromUrl$();

    this.route.queryParams.pipe(untilDestroyed(this), debounceTime(200), delay(500))
      .subscribe(res => {
        this.triggerTableSizeRecalculation();
      });

    schema$.pipe(untilDestroyed(this))
      .subscribe((schemaData: any) => {
        if (schemaData && schemaData.length > 0) {
          if (schemaData[0][0].hasOwnProperty('Schema')) {
            this.schemaData = JSON.parse(schemaData[0][0].Schema);
          }
          else if (schemaData[0][0].hasOwnProperty('schema')) {
            this.schemaData = JSON.parse(schemaData[0][0].schema);
          }
          this.readData(defaultOption)
          let d = getFormVariables(this.schemaData, this.options);
          for (let key of Object.keys(d)) {
            this[key] = d[key];
          }
        } else {
          this.layout = null;
        }
      });

  }

  getSchemaFromUrl$() {
    // schema initialized using selector then read only data else read both schema and data
    return this.route.params.pipe(first(),
      // filter((d: any) => d.schema),
      switchMap((params) => {
        if (params[`${OPERATION_TYPE}`]) {
          this.wrapper.EntityID = params[`${ENITTY_ID}`];
          this.parentEntityId = params[`${PARENT_ID}`];
          this.parentEntityId = (this.parentEntityId == '0') ? null : this.parentEntityId;
        }
        else {
          if (!params.entityId) {
            this.menuId = params[`${MENU_ID}`];
            this.wrapper.MessageKind = MessageKind.UPDATE;
          }
        }
        // FOR MENUS FORM UPDATE OPERATION
        if (!this.wrapper.MessageKind) {
          this.wrapper.MessageKind = params[`${OPERATION_TYPE}`] == FormOperationTypes.Add ? MessageKind.CREATE : MessageKind.UPDATE;
        }

        let schemaPage = this.getSchemaNameByEntityType(this.EntityType);
        return this.sgformGeneratorService.readSchema$(schemaPage, this.EntityType, this.EntityId, this.EntityName);

        // return "";
      }));
  }


  setExpandCollapse(value) {
    if (this.formGenerator) {
      let data = { "type": "expandCollapseAll", "value": value };
      this.formGenerator.setLayoutOptions(data);
    }
  }

  onChange(event) {
    this.livedata = event;
    console.log('onChange() :: FormGenerator :: event :: ', event);
  }

  isValidFn(event) {
    console.log("isValid Fn", event);
  }

  validationErrors(event) {
    console.log("In Valida Fields :");
    console.log(event);
  }

  enumValueUpdate(event) {

  }


  triggerTableSizeRecalculation() {
    if (this.formGenerator)
      this.formGenerator.resizeTable();
  }


  updateDataProperties(data: any) {
    let tempData: any = {};
    const keys: string[] = Object.keys(data);
    for (let key of keys) {
      const innerData: any = data[key];
      key = this.getSchemaPageKey(key);
      if (Array.isArray(innerData)) {
        tempData[key] = [];
        innerData.forEach(element => {
          const innerKeys: string[] = Object.keys(element);
          let tempObjData = {}
          for (const innerKey of innerKeys) {
            if (innerKey.toLowerCase() == 'payload') {
              this.payloadData = element[innerKey] ? JSON.parse(element[innerKey]) : {};
              continue;
            }
            tempObjData[innerKey.toLowerCase()] = element[innerKey];
          }
          tempData[key].push(tempObjData);
        });
      }
      else {
        tempData[key] = {};
        const innerKeys: string[] = Object.keys(innerData);

        for (const innerKey of innerKeys) {
          if (innerKey.toLowerCase() == 'payload') {
            this.payloadData = innerData[innerKey] ? JSON.parse(innerData[innerKey]) : {};
            continue;
          }
          tempData[key][innerKey.toLowerCase()] = innerData[innerKey];
        }
      }
    }
    return tempData;
  }

  getSchemaPageKey(key: string) {
    switch (key) {
      case 'tasks':
      case 'taggeditems':
      case 'materials':
        return this.schemaPage.toLowerCase();
      default:
        return key.toLowerCase();
    }
  }

  generateDataForExtraTable() {
    const tableReords: any[] = [];
    if (this.payloadData && Object.keys(this.payloadData).length > 0) {
      const keys: string[] = Object.keys(this.payloadData);
      for (const key of keys) {
        const data: any = {};
        data['label'] = key;
        data['value'] = this.payloadData[key];
        data['uuid'] = UUIDGenarator.generateUUID();

        tableReords.push(data);
      }
    }
    return tableReords;
  }

  lookUpQuery(event) {
    let filters = event.filters;
    let fields = event.fields;
    let displayLabel = event.displayLabel;
    let label = {};

    if (fields && Array.isArray(fields) && fields.length > 0) {
      for (let field of fields) {
        let formFields = (<string>field).split(":"); //companies:entityname
        if (formFields && formFields.length == 2) {
          let column = formFields[1]; //entityname
          let tableName = formFields[0];//companies
          label[tableName] = column;
          // this.peopleService.bindPeopleDropDowns(event,column);
        }
        else {
        }
      }
    }
  }

  getDefaultOptions(options: any) {
    if (options) {
      for (const key of Object.keys(options)) {
        this.options[key] = options[key];
      }
    }
    return this.options;
  }

  async click(event) {
    let buttonEvent: string = event.buttonEvent ? event.buttonEvent.toLowerCase() : null;

    switch (buttonEvent) {
      case CLOSE:
        this.closePopup(this.dialogRef);
        break;
      case TREE_ACTION:
        this.treeActionOperation(event);
        break;
      case ROWS_DROPPED:
        this.rowDroppedOperation(event);
        break;
      case DELETE:
        this.deleteOperation(event);
        break;
      case DELETE_ALL:
        this.deleteAllOperation(event);
        break;
      case EXPAND_ALL:
       this.expandAllOperation(event);
        break;
      default:
        this.emitOutputEvent("click", event);
        break;
    }
  }

  async manualEvent(event) {
    let buttonEvent: string = event.id ? event.id.toLowerCase() : null;
    let entityType = this.getEntityType(this.EntityType);
    switch (buttonEvent) {
      case SEARCHINPUT:
        this.searchOperation(event,entityType);
        break;
      case FILTER:
       this.filterManualOperation(this.schemaPage);
        break;
      case SELECT:
       this.selectOperation(event);
        break;
      case SAVE:
        if (this.formGenerator) {
          this.formGenerator.submitForm();
        }
        break;
    }
  }

  readDataFromBackend(event, dataType, searchmodel, defaultApprovalType?) {
    let payload: any = {};
    let entityType = defaultApprovalType ? defaultApprovalType : this.getEntityType(this.EntityType);
    payload.EntityId = this.EntityId;
    payload.EntityType = entityType;
    payload.DataType = dataType;
    payload.FormCapability = entityType;
    payload.subType = this.EntityType;
    let entitiydetails = this.commonService.getEntitiesById(this.EntityId);
    payload.EntityName = entitiydetails ? entitiydetails.EntityName : null;
    payload.SearchModel = searchmodel;
    let capabilityId = this.commonService.getCapabilityId(FABRICS.CONSTRUCTION);
    payload.CapabilityId = capabilityId ? capabilityId : "";
    payload.Fabric = this.commonService.lastOpenedFabric;
    let message = this.commonService.getMessageModel(JSON.stringify(payload), Datatypes.LOADFORMDATA, this.EntityId, entityType, MessageKind.READ, Routing.OriginSession, payload.Info, FABRICS.CONSTRUCTION);
    this.commonService
      .httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .map(res => {
        var messageData = JSON.parse(res);
        let data = JSON.parse(messageData["Payload"]);
        return data;
      }).subscribe(data => {
        if (data) {
          let tempdata = (data.Data && Object.keys(data.Data).length > 0) ? this.updateDataProperties(data.Data) : {};
          if (dataType == Datatypes.LOADDATA) {
            Object.keys(tempdata).forEach(element => {
              event.data.callback.next(tempdata[element]);
            });
          }
          else if (dataType == Datatypes.SEARCHDATA) {
            this.updateData = setData(tempdata, this.livedata);
          }
        }
      });
  }

  emitOutputEvent(type: string, data: any) {
    this.applicationEvent.emit({ "type": type, data: data });
  }

  closePopup(dialogRef: MatDialogRef<any, any>, event?) {
    if (dialogRef != null) {
      dialogRef.close();
    }
    else {
      this.dialog.closeAll();
    }
    switch (this.popupFormType) {
      case 'filter':
        break;

    }
    //this.logger.debug(event);
  }


  onToggleChange(data) {
    console.log(data);
    switch (data.buttonEvent) {
      case THEME_CHANGE:
        //this.document.body.classList.toggle('dark', data.event.checked);
        break;
    }
  }

  onSubmitFn(event) {
    let data = event ? event.data : null;
    if (data) {
      //clean up data
      for (let key of Object.keys(data)) {
        if (!data[key]) {
          delete data[key];
        }
      }
      // delete json data if we are reading data from other tables..
      if (this.readDataFromOtherSource) {
        let keys = Object.keys(this.readDataFromOtherSource);
        for (let key of keys) {
          key = key.toLowerCase();
          if (data[key]) {
            delete data[key];
          }
        }
      }

      if (!this.EntityType) {
        this.EntityType = this.myForm.getRawValue().EntityType;
      }

      data["EntityType"] = this.getEntityType(this.EntityType);
      data["EntityId"] = this.EntityId;
      if (this.isNewEntity) {
        data["MessageKind"] = MessageKind.CREATE;
      }
      else {
        data["MessageKind"] = MessageKind.UPDATE;
      }

      //this.sgformGeneratorService.saveIntoDB(event);
    }
  }

  treeAction(event) {
    // this.formService.treeAction(event).pipe(untilDestroyed(this)).subscribe(data => {
    //   setTimeout(() => {
    //     event.callback.next(data);
    //     (<Subject<any>>event.callback).complete();
    //   }, 0
    //   );
    // });
  }

  async onFilterFn(event) {
    const data: any = JSON.parse(JSON.stringify(event));
    let schemaPage = getTableName(this.schemaPage);
    data.schemapage = schemaPage;
    //await this.formService.filterUpsertAsync(data);
  }

  removeSpace(value) {
    return value ? value.replace(/ /g, '') : value;
  }

  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables);
  }


  getSchemaNameByEntityType(entityType) {
    let schemaPage = this.schemaPage ? this.schemaPage : entityType;
    if (this.schemaPage == ENTITIES || this.schemaPage == PEOPLEANDCOMPANIES || this.schemaPage == (entityType+PREMICO)) {
      schemaPage = EntityTypes.SECURITYGROUP + this.schemaPage;
    }
    return schemaPage;
  }


  getEntityType(entityType) {
    let eType = entityType;
    let url = decodeURIComponent(this.router.url);
    if (entityType == TreeTypeNames.Schedule) {
      eType = 'Tasks';
    }
    return eType;
  }

  filterOperation(event) {
    let eventPointer: string = event.dataPointer;
    let pointer = eventPointer.substring(1, eventPointer.length);
    let obs$: Observable<any> = null;
    const parentPointer: string = (<string>pointer);
    const parents: any[] = parentPointer.split('/');
    let parentPage: string = this.schemaPage + "." + parents[1];
    let otherSources = this.readDataFromOtherSource ? this.readDataFromOtherSource : {};
    let keys = Object.keys(otherSources);
    let isNonInlineTable = keys.find(key => key == parents[1]);
    if (isNonInlineTable) {
      const otherSource: any = {};
      otherSource[parents[1]] = this.readDataFromOtherSource[parents[1]];
    } else {
      obs$ = of(this.livedata);
    }
    let inline = isNonInlineTable ? false : true

    this.getFilterSchema(event, this.schema, parentPage, inline).subscribe(result => {
      this.activateDeactivateFilter(event, result, parentPage, inline);
    })
  }

  activateDeactivateFilter(event, result, parentPage, inline, height?, width?) {
    this.dialogRef = this.filterOpenDialog(result, this.EntityId, this.schemaPage, this.tableName, null, event.dataPointer, parentPage, inline, height, width)
    this.dialogRef.afterClosed().pipe(finalize(() => this.dialogRef = undefined)).subscribe(filtetData => {
      if (filtetData) {
        this.isFilterActive = filtetData.activateFilter;
        this.activeFilterName = filtetData.filterName;
        this.filterQyery = filtetData.nonsqllitequery;
        let entityType = this.getEntityType(this.EntityType);
        let emitData = { id: 'updateFormHeader', data: { 'opacity': false, headId: 'filter' } }
        let searchmodel = [];
        if (filtetData.activateFilter) {
          emitData.data.opacity = true;
          searchmodel.push({ SearchValue: null, SearchField: null, AccordianName: getLowerCase(entityType), FilterQuery: filtetData.nonsqllitequery })
        }
        else if (filtetData.deActivateFilter) {
          emitData.data.opacity = false;
          searchmodel.push({ AccordianName: getLowerCase(entityType) })
        }
        this.commonOutputEmitter.emit(emitData);
        this.readDataFromBackend(filtetData, Datatypes.SEARCHDATA, searchmodel);
      }
    });
  }

  getFilterSchema(event, schema, parentPage, inline) {
    let dataPointer: string = event.dataPointer;
    let filterinputSchema: Observable<any> = null;
    if (!inline) {
      //filterinputSchema = this.queryService.readSchemaByParentPage$(parentPage);
    } else {
      if (dataPointer) {
        let node = getNode(dataPointer, schema);
        if (node.length == 1) {
          filterinputSchema = of(node[0]);
        }
      }
    }
    return filterinputSchema;
  }

  filterOpenDialog(schema: any, entityId, parentId, tableName, data, dataPointer, parentPage, inline, height, width) {
    let jsonSchema = {};
    jsonSchema["entityId"] = entityId;
    jsonSchema["TableData"] = data;
    jsonSchema["TableName"] = tableName;
    jsonSchema["PrentId"] = parentId;
    jsonSchema["dataPointer"] = dataPointer;
    jsonSchema["parentPage"] = parentPage;
    jsonSchema["inline"] = inline;
    jsonSchema["schema"] = schema;
    jsonSchema["clearFilter"] = this.isFilterActive;
    jsonSchema["activeFilterName"] = this.activeFilterName;
    jsonSchema["applicationId"] = this.commonService.getCapabilityId(this.commonService.getFabricNameByUrl(this.router.url));
    width = width ? width : "450px";
    height = height ? height : "550px";
    const dialogRef = this.dialog.open(FilterDialogComponent, {
      width: width,
      height: height,
      disableClose: true,
      data: jsonSchema
    });
    return dialogRef;
  }

  readData(defaultApprovalType?) {
    let entityType = this.getEntityType(this.EntityType);
    let searchmodel = [];
    searchmodel.push({ AccordianName: entityType.toLowerCase() })
    if (this.schemaPage == (this.EntityType+PREMICO))
      this.readDataFromBackend(null, Datatypes.SEARCHDATA, searchmodel, defaultApprovalType);
    else {
      this.getSecurityGroupDetails()
    }
  }

  getSecurityGroupDetails() {
    let imessageModel: IModelMessage = this.commonService.getMessageWrapper({ DataType: MessageEntityTypes.SECURITYGROUPS, EntityType: EntityTypes.SECURITYGROUP, EntityID: this.SecurityGroupId, MessageKind: MessageKind.READ });
    this.commonService.postDataToBackend(imessageModel, AppConsts.READING).subscribe(data => {
      console.log('SecurityGroup Details', data);
      this.onSecurityGroupDetailsReceive(data);
      this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, '', AppConsts.LOADER_MESSAGE_DURATION);
    });

  }

  onSecurityGroupDetailsReceive(data) {
      console.log('SecurityGroup Details', data);
      let sGroupDetails = data['SGroupDetails'][0];
      if(sGroupDetails && this.schemaPage == ENTITIES){
        let entities =sGroupDetails[ENTITIES] ? sGroupDetails[ENTITIES] : []
        this.constructSecurityGroupEntitiesTree(entities);
      }
      else if(sGroupDetails && this.schemaPage == PEOPLEANDCOMPANIES){
        let peopleandCompanies =sGroupDetails['People'] ? sGroupDetails['People'] : []
       this.constructSecurityGroupPeopleTree(peopleandCompanies);
      }
  }

  constructSecurityGroupEntitiesTree(entitiesArray: Array<any>) {
    let entities = entitiesArray;
    entities.forEach(item => {
      if (item.NestedUnder) {
        item.ParentEntityId = item.NestedUnder;
        delete item.NestedUnder;
      }
      else {
        item.ParentEntityId = null;
      }
      item['loaded'] = true;
      item.uuid = item.EntityId;
    });
    this.dataBufferObj[this.schemaPage] = entities;
    let tempdata = this.updateDataProperties({ [this.schemaPage]: entities });
    this.updateData = setData(tempdata, this.livedata);
  }

  constructSecurityGroupPeopleTree(peopleEntities: Array<any>) {
    let rootNode = [];
    if (peopleEntities) {
      rootNode = peopleEntities;
      rootNode.forEach(item => {
        item.uuid = item.EntityId;
        item['loaded'] = true;
        if (item.hasOwnProperty("OfficeParent")) {
          item.ParentEntityId = item.OfficeParent.EntityId;
          delete item.OfficeParent;
        } else if (item.hasOwnProperty("PersonParent")) {
          item.ParentEntityId = item.PersonParent.EntityId;
          delete item.PersonParent;
        }
      });
    }
    this.dataBufferObj[this.schemaPage] = rootNode;
    let tempdata = this.updateDataProperties({ [this.schemaPage]: rootNode });
    this.updateData = setData(tempdata, this.livedata);
  }

  getAllChildUUID(fromData:Array<any>,uuid:string,treeFromRelation:string,uuidList:Array<string>,formatedRows:Array<any>) {
    let childRows = fromData.filter(item => item[treeFromRelation] == uuid);
    childRows.forEach(item => {
      uuidList.push(item.uuid);
      this.updateDataBuffer(item,'delete');
      formatedRows.push({EntityId:item.uuid});
      this.getAllChildUUID(fromData,item.uuid,treeFromRelation,uuidList,formatedRows)
    })
  }

  deleteOperation(event){
    if(event && event.data && event.data.content){
      let rowlist:Array<any> = event.data.content
      let formatedRows:Array<any> = [];
      rowlist.forEach(row =>{
        if(row.uuid){
         this.updateDataBuffer(row,'delete');
         this.formGenerator.removeTableItemByUUID(event.data.dataPointer,[row.uuid]);
         formatedRows.push({EntityId:row.uuid});
        }
      });
      this.commonOutputEmitter.emit({buttonEvent:DELETE_ROWS,id:DELETE_ROWS,schemaPage:this.schemaPage,data:{rows:formatedRows}});
    }
  }

  deleteAllOperation(event){
    if(event && event.data){
      let eventData = event.data;
      let rows:Array<any> = eventData.content ? eventData.content : [];
      let tempList:Array<any> =eventData.dataPointer ? eventData.dataPointer.split('/') : [];
      let formData:Array<any> = tempList.length ? this.livedata[tempList[tempList.length-1]] : [] ;
      let formatedRows:Array<any> = [];
      rows.forEach(row =>{
        if(row.uuid){
          let uuidlist = [row.uuid];
          this.updateDataBuffer(row,'delete');          
          formatedRows.push({EntityId:row.uuid}); 
          this.getAllChildUUID(formData,row.uuid,eventData.treeFromRelation,uuidlist,formatedRows);
          this.formGenerator.removeTableItemByUUID(eventData.dataPointer,uuidlist);
        }
      });
      this.commonOutputEmitter.emit({buttonEvent:DELETE_ROWS,id:DELETE_ROWS,schemaPage:this.schemaPage,data:{rows:formatedRows}});
    }
  }

  treeActionOperation(event){
    if (event.data.treeToRelation && event.data.row && event.data.row[event.data.treeToRelation]) {
      let searchmodel = [];
      searchmodel.push({ SearchValue: event.data.row[event.data.treeToRelation], SearchField: event.data.treeFromRelation, AccordianName: this.getEntityType(this.EntityType) })
      this.readDataFromBackend(event, Datatypes.LOADDATA, searchmodel)
    }
  }

  expandAllOperation(event){
    if (event.data.treeToRelation && event.data.content && event.data.content[event.data.treeToRelation]) {
      let searchmodel = [];
      searchmodel.push({ SearchValue: event.data.content[event.data.treeToRelation], SearchField: event.data.treeFromRelation, AccordianName: this.getEntityType(this.EntityType) })
      this.readDataFromBackend(event, Datatypes.LOADDATA, searchmodel)
    }
  }

  rowDroppedOperation(event){
    if(event && event.data && event.data.droppedRows){
      let droppedRows:Array<any> = event.data.droppedRows;
      let formatedRows:Array<any> = [];
      droppedRows.forEach(item =>{
        this.updateDataBuffer(item,'add');
        formatedRows.push({EntityId:item.uuid})
      });
      this.commonOutputEmitter.emit({buttonEvent:NEW_ROWS,id:NEW_ROWS,schemaPage:this.schemaPage,data:{rows:formatedRows}});
    }
  }

  searchOperation(event,entityType){
    if(this.schemaPage == ENTITIES || this.schemaPage == PEOPLEANDCOMPANIES){
      let searchValue = event.value ? event.value : null;
      let tempdata = this.updateDataProperties(this.dataBufferObj);
      let schemaPage = this.schemaPage.toLowerCase();
      let data = tempdata[schemaPage] ? (searchValue ? tempdata[schemaPage].filter(item => item.entityname && item.entityname.includes(searchValue)):tempdata[schemaPage]):[];
      this.updateData = setData({[schemaPage]:data}, this.livedata);    
    }
    else{
      let searchlist = this.search ? this.search : getSearchlistBasedOnType(entityType);
      let searchmodel = [];
      searchlist.forEach(element => {
        let property = element.split(':');
        let searchObj = {};
        searchObj['SearchValue'] = event.value ? event.value : null;
        searchObj['SearchField'] = property[1];
        searchObj['AccordianName'] = property[0];
        if (this.filterQyery && !searchObj['SearchValue']) {
          searchObj['FilterQuery'] = this.filterQyery;
        }
        searchmodel.push(searchObj)
      });
      this.readDataFromBackend(event, Datatypes.SEARCHDATA, searchmodel);
    }
  }

  filterManualOperation(entityType){
     //will take datapointer from form
     entityType = getLowerCase(entityType);
     let datapointer = "/" + entityType + "panel" + "/" + entityType;
     if (this.isFilterActive) {
       this.activateDeactivateFilter({ dataPointer: datapointer }, null, null, null, "200px", null);
     }
     else {
       this.filterOperation({ dataPointer: datapointer });
     }
  }

  selectOperation(event){
    if (event.value) {
      this.EntityType = this.removeSpace(event.value);
      this.schemaPage = this.getSchemaPage(this.EntityType,this.schemaPage);
      this.isFilterActive = false;
      this.initializeSchema();
    }
    else {
      this.layout = null;
    }
  }

  getSchemaPage(entityType,schemaPage){
    if(schemaPage && schemaPage.includes(PREMICO) && entityType){
      return entityType + PREMICO;
    }else{
      return schemaPage;
    }
  }

  ngOnDestroy() {
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }

  updateDataBuffer(row:any,operation){
    if(operation == 'add' && this.dataBufferObj[this.schemaPage]){
      this.dataBufferObj[this.schemaPage].push(row);
    }
    else if(operation == 'delete' && this.dataBufferObj[this.schemaPage]){
      this.dataBufferObj[this.schemaPage] = this.dataBufferObj[this.schemaPage].filter(item => item.uuid != row.uuid);
    }
  }

}
