import { TestBed } from '@angular/core/testing';

import { SgformGeneratorService } from './sgform-generator.service';

describe('SgformGeneratorService', () => {
  let service: SgformGeneratorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SgformGeneratorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
