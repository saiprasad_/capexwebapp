import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { StateRecords } from 'ClientApp/app/components/common/state/common.state';
import { EntityCommonStoreQuery } from 'ClientApp/app/components/common/state/entity.state.query';
import { EntityFilterStoreQuery } from 'ClientApp/app/components/common/state/EntityFilter/entityfilter.query';
import { CommonService } from 'ClientApp/app/components/globals/CommonService';
import { AppConfig } from 'ClientApp/app/components/globals/services/app.config';
import { UsersStateQuery } from 'ClientApp/webWorker/app-workers/commonstore/user/user.state.query';
import { combineLatest, Subject,forkJoin, Observable, of } from 'rxjs';
import { MessageModel } from 'ClientApp/app/components/globals/Model/Message';
import { FABRICS, MessageKind } from 'ClientApp/app/components/globals/Model/CommonModel';
import { getDataFromJson, getNode, getTableNames } from 'ClientApp/app/components/globals/helper.functions';
import { FormOperationTypes, READ_TABLE_DATA, FILTER,  SCHEMA, ReviewAndApproval, Forms, Route, UserSettings, UserSettingsSync, READ_DATA, FilterTable, SQLiteCommonColumns, IMAGES } from 'ClientApp/app/components/common/Constants/AppConsts';
import { map } from 'rxjs/operators';

@Injectable()
export class SgformGeneratorService {

  constructor(private route: ActivatedRoute, private commonService: CommonService, public usersStateQuery: UsersStateQuery, public stateRecords: StateRecords, public entityCommonStoreQuery: EntityCommonStoreQuery, public router: Router, public entityFilterStoreQuery: EntityFilterStoreQuery, public dialog: MatDialog, public appConfig: AppConfig) { }

  dismissStausMessage() {
    this.commonService.dismissStausMessage();
  }

  readSchema$(schemaPage, entityType, entityId, entityName) {
    let apiCall;
    let payloadJson = {}
    payloadJson['entityType'] = entityType ? entityType : null;
    payloadJson['entityId'] = entityId ? entityId : null;
    payloadJson['entityName'] = entityName ? entityName : null;
    payloadJson['schema'] = schemaPage ? schemaPage : null;
    payloadJson['dataType'] = SCHEMA;
    let readDataFromApi = this.commonService.readDataFromApiCall(payloadJson, SCHEMA, payloadJson['entityId'], payloadJson['entityType'], FABRICS.CONSTRUCTION);
    let filterJson: any = JSON.parse(JSON.stringify(payloadJson));
    filterJson['dataType'] = FILTER;
    return combineLatest(readDataFromApi);

  }

  routeUrl(event, schema, data, parentIdForChildForm: string, parentEntityId: string, wrapper: MessageModel, schemaPage,tableName) {
    let dataPointer: string = event.dataPointer;
    let node = getNode(dataPointer, schema);

    if (node != null && node.length == 1) {
      let schemaRoute = node[0].schemaRoute;
      let page = node[0].page;
      let operationType: string = node[0].operationType;

      //get breadcrumb display label
      // let dispalyLabel = breadCrumjson.schemaPage;
      // if (breadCrumjson.id && breadCrumjson.breadcrumbDisplayName) {
      //   let formField = breadCrumjson.breadcrumbDisplayName.split(":");  // Header:DailyReportNumber
      //   let field = formField[1];     // DailyReportNumber
      //   let fieldIn = formField[0];  //Header
      //   let fieldInNode = jsonPath.query(data, fieldIn.toLowerCase(), 1);
      //   let entitydata = fieldInNode ? fieldInNode.find((item: any) => item.uuid == breadCrumjson.id) : null;
      //   if (entitydata) {
      //     dispalyLabel = entitydata[field.toLowerCase()];
      //   }
      // }
      // add popup
      if (page == null && wrapper.MessageKind == MessageKind.CREATE) {
        page = event.data.name;
        operationType = FormOperationTypes.Add;
      }

      if (operationType && operationType.toLowerCase() == FormOperationTypes.Add) {
        this.routing(parentIdForChildForm, schemaPage, schemaPage, page, FormOperationTypes.Add, null,null,tableName);
        return;
      }

      let entityId = null;
      let entityName = null;
      if (event.data) {
        entityId = event.data.entityid ? event.data.entityid : event.data.uuid ;
        //will take entityname nee to do schema and data model cleanup
        if(page == 'Tagged Item'){
          entityName = event.data.tag ? event.data.tag : page;
        }
        else if(page == 'Materials'){
          entityName = event.data.materialname ? event.data.materialname : page;
        }
        else if(page==ReviewAndApproval){
          if(event.data.pagetype){
            entityName = event.data.pagetype;
          }else if(event.data.journalnumber){
            entityName = event.data.journalnumber;
          }else{
            entityName = page;

          }

        }
        else{
          entityName = event.data.entityname ? event.data.entityname : page;
        }
      } else if (event.widgetType = "button") {
        entityId = getDataFromJson(dataPointer, data);
        parentIdForChildForm = entityId;
      }

      if (!page) {
        // get schema name based on input parameters
        let formField = schemaRoute.split(":");
        let field = formField[1];     // type
        let fieldIn = formField[0];  //header
        page = event.data[field];
      }

      if (page != null) {
        this.routing(parentIdForChildForm, schemaPage, schemaPage, page, FormOperationTypes.Update, entityId,entityName,tableName);
      }
      else {
        this.commonService.appLogException(new Error('Exception in routeUrl() of ConstructionService in EntityMgmt Construction at time ' + new Date().toString() + '. Exception is : '));

      }
    }

  }
  
  readDataForTable$(key, tableName, entityType, sourceInfo, type, entityId, query: string, queryParam: {}, filterData?) {
    let tablePayload = {};
    tablePayload['key'] = key;
    tablePayload['tableName'] = tableName;
    tablePayload['sourceInfo'] = JSON.stringify(sourceInfo);
    tablePayload['type'] = type;
    tablePayload['entityId'] = entityId;
    tablePayload['query'] = query;
    tablePayload['queryParam'] = JSON.stringify(queryParam);
    tablePayload['filterData'] = JSON.stringify(filterData);
    return this.commonService.readDataFromApiCall(tablePayload, READ_TABLE_DATA, entityId, entityType, FABRICS.CONSTRUCTION);

    let fieldMapping = sourceInfo["fieldMapping"];
    let primaryTable = sourceInfo["primaryTable"];
    if (!query) {
      //query = this.queryBuilder.generatePrimaryTableQuery(fieldMapping, primaryTable, entityId);
    }

    console.log("query primary", query);
    if (Object.keys(queryParam).length == 0) {
      if (type == "object") {
        query += ` where uuid='${entityId}'`;
      }
      else if (type == "array") {
        if (entityId)
          query += ` where parententityid ='${entityId}'`;
        else {
          query += ` where parententityid is null`;
        }
        if (filterData?.length > 0) {
          let data = JSON.parse(filterData[0]?.payload);
          let filterQuery = data.query;
          if (filterQuery)
            query += filterQuery;
        }
      }

    } else {
      for (let key of Object.keys(queryParam)) {
        if (queryParam[key].toLowerCase() == "parententityid") {
          //query = replaceAll(query, key, entityId);
        } else {
          //query = replaceAll(query, key, queryParam[key]);

        }
      }
      if (filterData?.length > 0) {
        let data = JSON.parse(filterData[0]?.payload);
        let filterQuery = data.query;
        if (filterQuery)
          query += filterQuery;
      }
    }

    // return this.queryRecords$(query).pipe(map(data => {
    //   let output = []
    //   let result = {};
    //   if (data) {
    //     if (data.values && data.values.length > 0) {
    //       let values = data.values
    //       if (type == "object") {
    //         output = data.values[0];
    //       } else {
    //         output = values
    //       }
    //     }
    //   }
    //   result[key.toLowerCase()] = output;
    //   return result;
    // }));
  }

    //Persistance Query service
    readData$(schemaPage: string, schemaObject: any, entityId: string, parentEntityId: string, entityType: string) {
      let tablesProps = getTableNames(schemaPage, null, schemaObject);
      let tables = tablesProps.tables;
      let formTypeTables = {};
      let arrayTypeTables = {}
  
      Object.keys(tables).forEach(key => {
        let properties = tables[key];
        if (properties.type == "object") {
          let tableName = properties.tableName;
          if (!formTypeTables[tableName]) {
            formTypeTables[tableName] = {}
            formTypeTables[tableName]['props'] = properties;
            formTypeTables[tableName]['fieldNames'] = {}
          }
          formTypeTables[tableName]['fieldNames'][key] = properties.fieldNames
        } else if (properties.type == "array") {
          arrayTypeTables[key] = properties;
        }
      });
      let obs = [...this.getFormTypeObservables(formTypeTables, entityId, parentEntityId, entityType), ...this.getArrayTypeObservables(arrayTypeTables, entityId, parentEntityId, entityType)]
      return obs;
    }

    executePrepopulateQueries(queries: Array<any>, schema: any, parentEntityId) {
      let queries$: Array<Observable<any>> = [];
  
      if (queries && queries.length > 0) {
        let fileteredQueries = queries.filter(query => query.prepopulate);
        for (let queryNode of fileteredQueries) {
          let data = {};
          let property = queryNode.property ? queryNode.property.toLowerCase() : null;
          let query: string = queryNode.query;
          let sheetName = queryNode.sheetName ? queryNode.sheetName.toLowerCase() : null;
          let queryParameter: string = queryNode.queryParameter;
          if (queryParameter) {
            let params = queryParameter.split(";");
            for (let param of params) {
              let fields = param.split("=");
              if (fields.length == 2) {
                if (fields[1].toLowerCase() == "parententityid") {
                  //query = replaceAll(query, fields[0], parentEntityId);
                }
              }
            }
          }
          data[sheetName] = {};
          // let query$ = this.queryService.queryRecords$(query)
          //   .pipe(map((d: any) => {
          //     return d[Values];
          //   })).pipe(map(jsonData => {
          //     if (jsonData != null && jsonData.length > 0) {
          //       data[sheetName] = jsonData[0];
          //       return data;
          //     } else {
          //       return null;
          //     }
          //   }));
          // queries$.push(query$);
        }
      }
      return forkJoin(queries$);
    }

    getFormTypeObservables(formTypeTables, entityId, parentEntityId, entityType) {
      let observables$ = []
      for (let tableName of Object.keys(formTypeTables)) {
        let fieldNames = formTypeTables[tableName].fieldNames;
        let table = formTypeTables[tableName]['props'];
        let dataPayload = this.getJsonPayload(table.tableName, READ_DATA, null, entityId, table.filterBy, table.primary, parentEntityId, table.type);
        let apiCall = this.commonService.readDataFromApiCall(dataPayload, READ_DATA, entityId, entityType, FABRICS.CONSTRUCTION);
        observables$.push(combineLatest([of(table), apiCall, of(fieldNames)])
          .pipe(map(data => {
            const tableName: string = data[0].tableName;
            const fields: string = data[2];
  
            if (data[1]) {
              if (data[1].values && data[1].values.length > 0) {
                let result: any[] = [];
                for (const value of data[1].values) {
                  if (value.payload) {
                    let payload = JSON.parse(value.payload);
                    payload = this.getUpdatePayloadBasedonTableName(tableName, payload, value);
                    result.push(payload);
                  }
                  else if (tableName == UserSettings || tableName == UserSettingsSync) {
                    result.push(value);
                  }
                }
                let jsonOutput = result[0];
                let resultData = {}
                let keys = Object.keys(fields);
                for (let key of keys) {
                  let fieldNames: Array<any> = fields[key]
                  let result = {}
                  for (let prop of Object.keys(jsonOutput)) {
                    let d = fieldNames.find(fieldName => fieldName == prop)
                    if (d != null) {
                      result[prop] = jsonOutput[d];
                    }
                  }
                  resultData[key] = result
                }
                return resultData;
              }
            }
            return null;
          })));
      }
      return observables$;
    }

    routing(parentIdForChildForm, parentPage, dispalyLabel, page, formType, entityid,entityname,tableName) {
      try {
        const currentUrl: string = this.router.url.slice(0, this.router.url.lastIndexOf(`/${Forms}`));
        //let routeData = this.commonService.getUrlfromActviatedRoute(this.route);
        // let binddata = parentIdForChildForm + ',' + dispalyLabel + ',' + parentPage;
        //let newRouteData = parentPage ? (routeData ? routeData + '#' + binddata : binddata) : routeData;
        //const currentUrl: string = this.router.url.slice(0, this.router.url.lastIndexOf(`?`));
        const queryParam = this.router.parseUrl(this.router.url).queryParams;
        const queryParam1 = JSON.parse(JSON.stringify(queryParam));
        queryParam1['operationType'] = FormOperationTypes.Update;
        queryParam1['entityType'] = page;
        queryParam1['schema'] = page;
        queryParam1['entityId'] = entityid;
        queryParam1['entityName'] = entityname;
        queryParam1['tableName']=tableName;
        // newRouteData.EntityName='';
        if(page==ReviewAndApproval){
          
          let url = this.router.createUrlTree([currentUrl, Forms, Route,queryParam1.tab, parentIdForChildForm, page, formType], { queryParams: queryParam1 }).toString();
          if(url.startsWith('/') && this.commonService.baseUrl.endsWith('/'))
           url = url.replace(url.charAt(0), "");
  
          let newTabUrl =this.commonService.baseUrl + url;
          window.open(newTabUrl, '_blank');
        }else{
          if (formType == FormOperationTypes.Add) {
            ///this.router.navigate([currentUrl, Forms, Route, parentIdForChildForm, page, formType], { queryParams: { 'data': newRouteData } });
    
            this.router.navigate([currentUrl, Forms, Route,queryParam1.tab, parentIdForChildForm, page, formType], { queryParams: queryParam1 });
          }
          else {
            this.router.navigate([currentUrl, Forms, Route,queryParam1.tab, parentIdForChildForm, page, formType, entityid], { queryParams: queryParam1 });
          }
        }
  
      }
      catch (ex) {
        console.log(ex);
      }
  
    }

    getArrayTypeObservables(arrayTypeTables, entityId, parentEntityId, entityType) {
      let observables$ = []
      for (let key of Object.keys(arrayTypeTables)) {
        let table = arrayTypeTables[key];
        table["key"] = key;
  
        let dataPayload = this.getJsonPayload(table.tableName, READ_DATA, null, entityId, table.filterBy, table.primary, parentEntityId, table.type);
        let filterPayload = this.getJsonPayload(FilterTable, FILTER, key, entityId, table.filterBy, table.primary, parentEntityId, table.type);
        let readDataApiCall = this.commonService.readDataFromApiCall(dataPayload, READ_DATA, entityId, entityType, FABRICS.CONSTRUCTION)
        let filterApiCall = this.commonService.readDataFromApiCall(filterPayload, FILTER, entityId, entityType, FABRICS.CONSTRUCTION)
  
        observables$.push(combineLatest([of(table), readDataApiCall, filterApiCall])
          .pipe(map(data => {
            const tableName: string = data[0].tableName;
            const filterPayload: any = data[2];
            const key: string = data[0].key;
  
            if (data[1]) {
              if (data[1].values && data[1].values.length > 0) {
                let result: any[] = [];
                for (const value of data[1].values) {
                  if (value.payload) {
                    let payload = JSON.parse(value.payload);
                    payload = this.getUpdatePayloadBasedonTableName(tableName, payload, value);
                    result.push(payload);
                  }
                }
  
                let output = result;
                const finalOutput: any = {}
                finalOutput[data[0].key] = output;
                return finalOutput;
              }
            }
            return null;
          })));
      }
      return observables$;
    }

    getJsonPayload(tableName = null, dataType = null, key = null, entityId = null, filterBy = null, primary = null, parentEntityId = null, type = null) {
      let payloadJson = {};
      payloadJson['tableName'] = tableName;
      payloadJson['dataType'] = dataType;
      payloadJson['entityId'] = entityId;
      payloadJson['filterBy'] = filterBy;
      payloadJson['primary'] = primary;
      payloadJson['key'] = key;
      payloadJson['parentEntityId'] = parentEntityId;
      payloadJson['type'] = type;
      return payloadJson;
    }

    getUpdatePayloadBasedonTableName(tableName, payload, value) {
      payload[SQLiteCommonColumns.UUID] = value.uuid;
      payload[SQLiteCommonColumns.Parent] = value.parent;
  
      switch (tableName) {
        case IMAGES:
          payload[SQLiteCommonColumns.Thumbnail] = value.thumbnail;
          payload[SQLiteCommonColumns.Altitude] = value.altitude;
          payload[SQLiteCommonColumns.Latitude] = value.latitude;
          payload[SQLiteCommonColumns.Longitude] = value.longitude;
          break;
        default:
          break;
      }
      return payload;
    }
}
