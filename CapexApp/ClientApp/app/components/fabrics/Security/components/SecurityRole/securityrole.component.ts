import { Component, ChangeDetectorRef, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonService } from './../../../../globals/CommonService';
import { MessagingService } from './../../../../globals/services/MessagingService';
import { Router, ActivatedRoute } from '@angular/router';
import { AppInsightsService } from './../../../../common/app-insight/appinsight-service';
import { Observable, Subject } from "rxjs";
import { UUIDGenarator } from "visur-angular-common";
import { AuthorizationService } from './../../services/authorization.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AppConfig } from './../../../../globals/services/app.config';
import { MessageKind, AngularTreeMessageModel, TreeTypeNames, TreeOperations, FabricsNames, FabricsPath, FABRICS } from 'ClientApp/app/components/globals/Model/CommonModel';
import { Routing, IModelMessage } from 'ClientApp/app/components/globals/Model/Message';
import { PopupOperation } from './../../../../globals/Model/AlertConfig';
import { PopUpAlertComponent } from './../../../../common/PopUpAlert/popup-alert.component';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { AppConsts, PermissionFormListForDeleteButtonDisable } from 'ClientApp/app/components/common/Constants/AppConsts';
import { EntityTypes } from 'ClientApp/app/components/globals/Model/EntityTypes';
import { FORM_ALREADY_EXIST_ROLE, CANT_DRAG_FOLDER, CANT_DRAG_ROLE, EMPTY_ROLE, ALREADY_EXIST_ROLE, DISCARD_CHANGES, FORM_ARCHIVED, VALID_ROLE_NAME, DUPLICATE_DROP, DELETE_ROLE, DELETE_FORM } from 'ClientApp/app/components/common/Constants/AttentionPopupMessage';
import { filter, takeUntil } from 'rxjs/operators';
import { RoleFormPermissions, RoleInfoModel, RolePermissions } from '../../models/role-info.model';
import { DataTypes, MessageEntityTypes } from '../../services/constants/message.constants';
import { alertPopUp } from 'ClientApp/app/components/common/AttentionPopUp/popup';
@Component({
  selector: 'securityrole',
  templateUrl: './securityrole.component.html',
  styleUrls: ['./securityrole.component.scss'],

})

export class SecurityRoleComponent implements OnInit, OnDestroy {

  currentIndex = 0;
  timeOutId;

  isLinear = true;
  public href: any;

  panelOpenState = false;
  entityList = [];
  fields = [];
  lists = [];
  svgIconList: any = {};
  /**
  * dialogRef
  */
  dialogRef: any;

  /**
   * json
   */
  json;
  /**
   * formFieldData
   */
  formFieldData = [];
  /**
   * popForms
   */
  popForms = [];
  /**
   * formFieldData
   */
  popUpFieldData = [];
  /**
   * HeaderData
   */
  HeaderData = [];
  /**
   * treeJsonData
   */
  treeJsonData: JSON;
  /**
   * currentWidth
   */
  currentWidth: number;
  /**
   * currentHeight
   */
  currentHeight: number;
  /**
   * margin
   */
  margin = { top: 5, right: 20, bottom: 30, left: 0 };
  /**
   * width
   */
  width: number;
  /**
   * height
   */
  height: number;
  /**
   * barCount
   */
  barCount: number = 3;
  /**
   * barHeight
   */
  barHeight: number = 0;
  /**
   * barWidth
   */
  barWidth: number = 0;
  /**
   * globalheight
   */
  globalheight: number = 0;
  /**
   * duration
   */
  duration = 400;
  /**
   * root
   */
  root;
  /**
   * tree
   */
  tree;
  /**
   * svg
   */
  svg;
  /*
  *expandAll
  */
  expandAll: boolean = false;
  /*
  *expandCollapseTitle
  */
  expandCollapseTitle: string = "Expand All";
  /*
   *expandCollapseIcon
   */
  expandCollapseIcon = 'V3 CollapsAll';
  /*
   *queryParams
   */
  queryParams: any = {};
  enabledisable = true;
  /*
  *expandAll
  */
  myPanel: boolean = true;
  // /*
  // *headerConfig
  // */
  // headerConfig;
  /*
  *popup
  */
  popup = [];
  /**
   * delete
   */
  delete = false;
  /**
*  ExpandSingle_Icon_Idle
*/
  ExpandSingle_Icon_Idle = "V3 ToggleDown";
  /**
  * CollapsSingle_Icon_Idle
  */
  CollapsSingle_Icon_Idle = "ExpandAccordion";

  /*
  *isSaveOrUpdate
  */
  isSaveOrUpdate = false;

  /*
  *deleteFormData
  */
  deleteFormData: any;
  /*
 *deleteFormData
 */
  isDeleteForm = false;


  myForm: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  colorcode = '#398dd3';
  mode = 'determinate';
  value = 25;
  bufferValue = 0;
  forms = this.authorizationService.currentForms;

  listOfChildren = [];
  schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "from": "FDC",
    "type": "object",
    "title": "NewEntity",
    "additionalProperties": false,
    "required": ["Role Name"],
    "properties": {
      "Role Name": {
        "type": "string",
      }
    }
  };
  data = {
    "Role Name": {
      "type": "string",
      "Value": null
    }
  }
  allOperation = [{ Name: "create", Value: false }, { Name: "read", Value: false }, { Name: "update", Value: false }, { Name: "delete", Value: false }]
  permissionsProperty = [{ Name: "create", Value: false }, { Name: "read", Value: false }, { Name: "update", Value: false }, { Name: "delete", Value: false }]

  roleSavedInBackend: boolean = false;

  copyConfigChildren: Array<any> = [];
  //htmlFormSchema: Array<any>;
  SchemaJson;
  NameOfTheEntity;
  entityType;
  entityId;
  childrenEntityList: any;
  ParentNameOftheEntity;
  IconName;
  IsChildrenExist: boolean = false;
  childrenEntityDetails: any;
  isThisStepDone: boolean = false;
  CanProceedFuther: boolean = false;
  Capability;
  Category;
  NameList = [];
  deleteIcon;
  Readchecked = false;
  Writechecked = false;
  Createchecked = false;
  Deletechecked = false;
  Operations = ['Read', 'Update']
  AllOperations = ['create', 'read', 'update', 'delete']
  rolePermissiondata = [];
  updatedForms = [];
  NewForms = [];
  deletedForms = [];
  entityTypelist = [];
  roleSavedInBackendProperly = false;
  displayRoleForm$;



  roleDetails = {oldRoleName: "", newRoleName: "" }
  updatedFormPermissions: Array<RoleFormPermissions> = [];
  newFormsAdded: Array<RoleFormPermissions> = [];
  roleInfoDetails: RoleInfoModel;

  createupdatedRoleJson = {};
  takeUntilDestroyObservables = new Subject();
  constructor(private _formBuilder: FormBuilder, public appConfig: AppConfig, private route: ActivatedRoute, private ref: ChangeDetectorRef, public dialog: MatDialog, public authorizationService: AuthorizationService, private logger: AppInsightsService, public router: Router, public commonService: CommonService, public messagingService: MessagingService,) {
    this.width = this.currentWidth = 255;
    this.height = this.currentHeight = 2500;
  }

  keydownevent(e) {
    if (e.which == 9) {
      e.preventDefault();
    }
  }



  ngOnInit() {
    this.authorizationService.roleInfoObservable$.subscribe(roleInfo => {
      if (roleInfo) {
        this.roleInfoDetails = this.deepCloneObject(roleInfo);
        this.roleDetails.oldRoleName = roleInfo.RoleName;
        this.roleDetails.newRoleName = roleInfo.RoleName;
        if (roleInfo.RolePermissions && Array.isArray(roleInfo.RolePermissions)) {
          roleInfo.RolePermissions.forEach(item => {
            if (this.rolePermissiondata.findIndex(element => element.SchemaName == item.SchemaName) == -1) {

              this.rolePermissiondata.push(item);
            }
            
          });
        }

        this.roleInfoDetails.RolePermissions = this.rolePermissiondata;
        this.checkAllFormtoggleStatus(this.rolePermissiondata);
      }
    });
  }






  deepCloneObject(obj: any): any {
    return JSON.parse(JSON.stringify(obj));
  }

  setRoleName() {
    let getRoleName = this.authorizationService.authorizationStateQuery.getStateKey("RoleName");
    if (getRoleName && getRoleName.EntityName && !this.authorizationService.newRoleActive) {
      this.roleDetails.oldRoleName = getRoleName.EntityName;
      this.roleDetails.newRoleName = getRoleName.EntityName;
    }
    this.allOperation = [{ Name: "Create", Value: false }, { Name: "Read", Value: false }, { Name: "Update", Value: false }, { Name: "Delete", Value: false }]
  }

  operationType(formname) {
    try {
      var obj = ['create', 'read', 'update', 'delete'];
      return obj;

    } catch (e) {
      console.error('exception in operationType() of SecurityRolesComponent at time ' + new Date().toString() + '. exception is : ' + e);
      this.logger.logException(new Error('Exception in operationType() of SecurityRoleComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  /**
   * sendDataToAngularTree() To send the data to angular tree component for creating the role tree.
   * @param treeName
   * @param treeOperationType
   * @param payload
   */
  sendDataToAngularTree(treeName: string, treeOperationType: string, payload: any) {
    try {
      var treeMessage: AngularTreeMessageModel = {
        "fabric": this.authorizationService.getCurrentFabric(),
        "treeName": [treeName],
        "treeOperationType": treeOperationType,
        "treePayload": payload,
      }
      this.commonService.sendDataToAngularTree.next(treeMessage);
    } catch (e) {
      console.error('exception in sendDataToAngularTree() of SecurityRolesComponent at time ' + new Date().toString() + '. exception is : ' + e);
      this.logger.logException(new Error('Exception in sendDataToAngularTree() of SecurityRoleComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  /**
   * updateRoleTree() method will update the role after create new role or updating existing role.
   */
  updateRoleTree(nodeData, operationType) {
    try {
      if (!this.commonService.isRightTreeEnable && this.authorizationService.rightSideRoleArray.length != 0 && this.authorizationService.rightSideRoleArray[0]["children"].length != 0) {
        if (operationType == "Update") {
          this.authorizationService.rightSideRoleArray[0]["children"].forEach((roleNode) => {
            if (roleNode.EntityId == nodeData.EntityId) {
              roleNode.EntityName = nodeData.EntityName;
            }
          });
        }
        else if (operationType == "Create") {
          this.authorizationService.rightSideRoleArray[0]["children"].push(nodeData);
        }
      }

    } catch (e) {
      console.error('exception in sendDataToAngularTree() of SecurityRolesComponent at time ' + new Date().toString() + '. exception is : ' + e);
      this.logger.logException(new Error('Exception in sendDataToAngularTree() of SecurityRoleComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  /**
   * onDrop() method will call on dropping the dragged from the left tree in to the role form.
   */
  onDrop(treeNodeData) {
    if (treeNodeData && treeNodeData.element) {
      var activatedNodes = treeNodeData.element.treeModel && treeNodeData.element.treeModel.activeNodes &&
        treeNodeData.element.treeModel.activeNodes.length ? treeNodeData.element.treeModel.activeNodes : [treeNodeData.element];
      activatedNodes.forEach(node => {
        if (node.data && node.data.EntityType != "PermissionsListHeader") {
          var form: RoleFormPermissions = new RoleFormPermissions();
          form.SchemaName = node.data.EntityName;
          form.SchemaId = node.data.SchemaId;
          form.Permissions = new RolePermissions();
          form.Permissions.create = false;
          form.Permissions.read = false;
          form.Permissions.update = false;
          form.Permissions.delete = false;
          if (this.rolePermissiondata.findIndex(item => item.SchemaName == form.SchemaName) == -1) {
            this.rolePermissiondata.push(form);
          }
          else {
            for (var i = 0; i < this.rolePermissiondata.length; i++) {
              if (treeNodeData.element.data.EntityName == this.rolePermissiondata[i].SchemaName) {
                new alertPopUp().openDialog(DUPLICATE_DROP, this.dialog);
              }
            }
          }
        }
      });
      this.sendDataToAngularTree(treeNodeData.element.treeModel.options.rootId, TreeOperations.deactivateAllAvtiveNode, "");
      this.checkAllFormtoggleStatus(this.rolePermissiondata);
    }
  }


  deleteForms(form) {

    console.log('deleted form', form);

    let message = DELETE_FORM(form.SchemaName);

    this.openDialog(message,form);
  }


  openDialog(messageToshow, form): void {
    try {
    let config = {
    header: 'Attention!',
    isSubmit: true,
    content: [messageToshow],
    subContent: [],
    operation: PopupOperation.AlertConfirm
    };
    let matConfig = new MatDialogConfig();
    matConfig.data = config;
    matConfig.width = '500px';
    matConfig.disableClose = true;
    matConfig.id = messageToshow;
    if (!this.dialog.getDialogById(messageToshow)) {
    this.dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
    this.dialogRef.componentInstance.emitResponse.subscribe((res) => {
    this.deleteExistingRole(form);
    });
    }
    }
    catch (e) {
    this.commonService.appLogException(new Error('Exception in openDialog() of Securityrolecomponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
    }


    //remove from forms 
  deleteExistingRole(form){

    console.log('deleted form', form);
   //remove from forms
   let index = this.rolePermissiondata.findIndex(roleForm => roleForm.SchemaName == form.SchemaName);
   if (index > -1) {
   this.rolePermissiondata.splice(index, 1);
  }
   //remove from updated forms if updated
   let upindex = this.updatedFormPermissions.findIndex(roleForm => roleForm.SchemaName == form.SchemaName);
   if (upindex > -1) {
   this.updatedFormPermissions.splice(upindex, 1);
  }
  //remove from addnew forms if added
  let aindex = this.newFormsAdded.findIndex(roleForm => roleForm.SchemaName == form.SchemaName);
  if (aindex > -1) {
  this.newFormsAdded.splice(aindex, 1);
     } else {
       this.deletedForms.push(form);
       }
  }

  
  /**
   * deleteRole() Deleting the existing form from the existing or new role form.
   * @param deleteForm
   */
  deleteRole(deleteForm) {
    try {
      //this.commonService.isFormModified = true;
      if (this.authorizationService.newRoleActive) {
        for (var i = 0; i <= this.formFieldData.length - 1; i++) {
          if (deleteForm["Name"] == this.formFieldData[i]["Name"]) {
            this.formFieldData.splice(i, 1);
          }
        }
      }
      else if (this.authorizationService.roleInfoActive) {
        for (var i = 0; i <= this.rolePermissiondata.length - 1; i++) {
          if (deleteForm["Name"] == this.rolePermissiondata[i]["Name"]) {
            var delForm = this.rolePermissiondata.splice(i, 1);
            var isNewForm = this.checkIsNewForm(deleteForm);
            if (!isNewForm) {
              this.checkFormExistInDeleteForms(deleteForm);
            }
          }
        }
        for (var i = 0; i <= this.updatedForms.length - 1; i++) {
          if (deleteForm["Name"] == this.updatedForms[i]["Name"]) {
            var delForm = this.updatedForms.splice(i, 1);
            this.checkFormExistInDeleteForms(deleteForm);
          }
        }
        for (var i = 0; i <= this.NewForms.length - 1; i++) {
          if (deleteForm["Name"] == this.NewForms[i]["Name"]) {
            var delForm = this.NewForms.splice(i, 1);
          }
        }
      }
      if (this.svgIconList && this.svgIconList.hasOwnProperty(deleteForm["Name"])) {
        delete this.svgIconList[deleteForm["Name"]];
      }

      // for(var i=0;i<=this.svgIconList.length-1;i++){
      //   if(deleteForm["Name"]==this.svgIconList[i]["name"]){
      //     var delForm=this.svgIconList.splice(i,1);
      //   }
      // }
      for (var i = 0; i <= this.entityList.length - 1; i++) {
        if (deleteForm["Name"] == this.entityList[i]["EntityName"]) {
          var delForm = this.entityList.splice(i, 1);
        }
      }
      for (var i = 0; i <= this.listOfChildren.length - 1; i++) {
        if (deleteForm["Name"] == this.listOfChildren[i]) {
          var delForm = this.listOfChildren.splice(i, 1);
        }
      }
      this.deleteFormData = null;
      this.checkAllFormtoggleStatus(this.authorizationService.newRoleActive ? this.formFieldData : this.rolePermissiondata);
    }
    catch (e) {
      console.error('Exception in deleteRole() of securityRole Component at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in deleteRole() of SecurityRoleComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  /**
   * checkIsNewForm() Checking if the deleting form is new or already saved form to send status of the form to the deleterole method when removing forms from role form.
   * @param form
   */
  checkIsNewForm(form) {
    try {
      for (var i = 0; i <= this.NewForms.length - 1; i++) {
        if (form["Name"] == this.NewForms[i]["Name"]) {
          return true;
        }
        return false;
      }
    }
    catch (e) {
      console.error('Exception in checkIsNewForm() of securityRole Component at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in checkIsNewForm() of SecurityRoleComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  /**
   * checkFormExistInDeleteForms() this method will check if the removing form is already exist in deletedforms or not.
   * @param data
   */
  checkFormExistInDeleteForms(data) {
    try {
      var exist = true;
      if (this.deletedForms.length != 0) {
        for (var i = 0; i <= this.deletedForms.length - 1; i++) {
          if (data["Name"] == this.deletedForms[i]["Name"]) {
            exist = false;
          }
        }
        if (exist) {
          this.deletedForms.push(data);
        }
      }
      else {
        this.deletedForms.push(data);
      }
    }
    catch (e) {
      console.error('Exception in checkFormExistInDeleteForms() of securityRole Component at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in checkFormExistInDeleteForms() of SecurityRoleComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  /**
   * ClickEvent() for the role form header icons to performs action based on the click on the icon
   * @param data
   */
  clickEvent(head) {
    try {
      switch (head) {
        case 'expandcollapse':
          this.ExpandAllPanel(head);
          break;

        case 'close': {
          // if (this.commonService.isFormModified || this.RoleName != this.currentRoleName) {
          //   this.openPopUpDialog(DISCARD_CHANGES, true);
          // } else {
          //   this.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissions, TreeOperations.deactivateSelectedNode, "");
          //   this.cancel();
          // }
          this.cancel();
          this.authorizationService.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissions, TreeOperations.deactivateSelectedNode, "");
          break;
        }
        // break;

      }
    } catch (e) {
      console.error('Exception in ClickEvent() of SecurityRoleComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in ClickEvent() of SecurityRoleComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }


  expandPannel(form, header, formName?) {
    try {
      if (header == "mainHeader" && this.svgIconList && this.svgIconList.hasOwnProperty(form.name)) {
        this.svgIconList[form.name].bool = !this.svgIconList[form.name].bool;
        this.svgIconList[form.name].matIcon = this.svgIconList[form.name].bool ? this.ExpandSingle_Icon_Idle : this.CollapsSingle_Icon_Idle;
        if (!this.svgIconList[form.name]["enableBody"]) {
          this.svgIconList[form.name]["enableBody"] = true;
        }
        let togglestatus = [];
        Object.keys(this.svgIconList).forEach((item: any) => togglestatus.push(this.svgIconList[item].bool))
        if (togglestatus.every(item => item == this.svgIconList[form.name].bool)) {
          this.expandAll = this.svgIconList[form.name].bool;
          this.expandCollapseIconChange();
        }
      }
      if (header == "popupHeader" && this.svgIconList && this.svgIconList.hasOwnProperty(formName.name)) {
        this.svgIconList[formName.name].popup.forEach((popupOption) => {
          if (popupOption.name == form.name) {
            popupOption.bool = !popupOption.bool;
            popupOption.matIcon = popupOption.bool ? this.ExpandSingle_Icon_Idle : this.CollapsSingle_Icon_Idle;
          }
        });
      }
    } catch (e) {
      console.error('Exception in expandPannel() of securityrole Component at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in expandPannel() of SecurityRoleComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  /**
   * ChangeToggle() On changing the form header toggle values this method will call to change all the field toggle values based on the header
   * header toggle value.
   * @param ope : ope be the name of the toggle
   * @param entityname : entityName will be the name of the form
   * @param value : value will be the value of the ope (true or false)
   */
  changeToggle(operation, formName, value) {
    console.log([operation, formName, value]);
    let clone = this.roleInfoDetails ? this.roleInfoDetails.RolePermissions.find(formObj => formObj.SchemaName == formName) : null;
    clone = this.updateDependentToggle(clone, operation, value, formName);
    if (!clone) { //new forms updating
      let formFind = this.rolePermissiondata.find(form => form.SchemaName == formName);
      formFind = this.updateDependentToggle(formFind, operation, value, formName);
      if (formFind) {
        let addnewFormFind = this.newFormsAdded.find(form => form.SchemaName == formName);
        if (!addnewFormFind) {
          this.newFormsAdded.push(formFind);
        }

      }

    } else { //existing forms update
      let existingFormObj = this.deepCloneObject(clone);
      if (this.updatedFormPermissions.length <= 0) {
        if (Object.keys(existingFormObj).length >= 0) {
          existingFormObj.Permissions[operation] = value;
          this.updatedFormPermissions.push(existingFormObj);
        }

      } else {
        let findFormObj = this.updatedFormPermissions.find(formObj => formObj.SchemaName == formName);
        findFormObj = this.updateDependentToggle(findFormObj, operation, value, formName);
        if (!findFormObj) {
          if (Object.keys(existingFormObj).length >= 0) {
            existingFormObj.Permissions[operation] = value;
            this.updatedFormPermissions.push(existingFormObj);
          }
        }
        else {

          findFormObj.Permissions[operation] = value;
          if (this.comparePermissionObjects(existingFormObj.Permissions, findFormObj.Permissions)) {//if rwo objects are same delete from updated forms array
            let index = this.updatedFormPermissions.findIndex(roleForm => roleForm.SchemaName == formName);
            if (index > -1) {
              this.updatedFormPermissions.splice(index, 1);
            }
          }
        }

      }
    }
    this.ref.detectChanges();
  }
  comparePermissionObjects(obj1, obj2): boolean {
    return JSON.stringify(obj1) === JSON.stringify(obj2)

  }

  updateDependentToggle(form, ope, value, formName) {
    if (form) {
      if (ope == AppConsts.READ) {
        if (value) {
          form.Permissions[AppConsts.READ] = value;
        }
        else {
          form.Permissions[AppConsts.UPDATE] = value;
          form.Permissions[AppConsts.CREATE] = value;
          form.Permissions[AppConsts.READ] = value;
        }
      }
      else if (ope == AppConsts.UPDATE) {
        if (value) {
          form.Permissions[AppConsts.READ] = value;
          form.Permissions[AppConsts.UPDATE] = value;
        }
        else {
          form.Permissions[AppConsts.UPDATE] = value;
          form.Permissions[AppConsts.CREATE] = value;
        }
      }
      else if (ope == AppConsts.CREATE) {
        if (value) {
          form.Permissions[AppConsts.READ] = value;
          form.Permissions[AppConsts.UPDATE] = value;
          form.Permissions[AppConsts.CREATE] = value;
        }
        else {
          form.Permissions[AppConsts.CREATE] = value;
        }
      }
      let index = this.rolePermissiondata.findIndex(element => element.SchemaName == formName);
      if (index >= 0) {
        this.rolePermissiondata[index].Permissions = form.Permissions;
      }
    }
    return form;
  }


  /**
   * toStopPropagation() to stop from holding the toggle
   * @param checkedEvent
   */
  toStopPropagation(checkedEvent) {
    checkedEvent.stopPropagation();
  }


  // /**
  //  * roleSaved() method calls when we click on the success svg to close the success role form.
  //  */
  // roleSaved() {
  //   this.cancel();
  // }
  /**
   * roleNameValidation() method called for validating the role name in the role creation and update form to check
   * if the role name is already exist or not.
   * @param roleName
   */
  roleNameValidation() {
    try {
      let status = false;
      let index = -1;
      if (this.authorizationService.securityRoleData) {
        index = this.authorizationService.securityRoleData.findIndex(element => element.EntityName == this.roleDetails.newRoleName);
        if (index >= 0) {
          status = true;
        }
        else {
          index = this.authorizationService.securityRoleData.findIndex(element => element.EntityName == this.roleDetails.oldRoleName);
          if (index >= 0) {
            this.authorizationService.securityRoleData[index]["EntityName"] = this.roleDetails.newRoleName;
          }
        }
      }
      return status;
    } catch (e) {
      console.error('Exception in roleNameValidation() of securityrole Component at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in roleNameValidation() of SecurityRoleComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }


  createOrUpdateRole() {

    //validate role name
    if (!this.roleDetails.newRoleName || this.roleDetails.newRoleName.trim() == "") {
      this.openPopUpDialog([EMPTY_ROLE], false);
      return;
    }
    else if (this.router.url.includes('NewRole') || this.roleDetails.newRoleName != this.roleDetails.oldRoleName) {
      let status = this.roleNameValidation();
      if (status) {
        this.openPopUpDialog([ALREADY_EXIST_ROLE(this.roleDetails.newRoleName)], false);
        return;
      }
    }

    if (this.router.url.includes('NewRole')) {
      this.saveNewForm();
    }
    else {
      this.saveEditForm();
    }

    this.createupdatedRoleJson = {
      EntityId: this.authorizationService.securityRoleId,
      EntityName: this.roleDetails.newRoleName,
      EntityType: "Role"
    };
  }

  saveNewForm() {
    try {
      let roleId = UUIDGenarator.generateUUID();
      this.authorizationService.securityRoleId = roleId;
      let payload = {
        Capability: this.commonService.getCapabilityNamebasedontabforSecurity(),
        CapabilityId: this.commonService.getCapabilityidbasedontabforSecurity(),
        RoleName: this.roleDetails.newRoleName,
        RoleId: roleId,
        NewSchemas: this.rolePermissiondata
      };
      let imessageModel: IModelMessage = this.commonService.getMessageWrapper({
        DataType: DataTypes.FORMSCHEMAROLES,
        EntityType: MessageEntityTypes.ROLESCHEMAPERMISSIONS,
        MessageKind: MessageKind.CREATE,
        Payload: JSON.stringify(payload)
      });
      this.commonService.postDataToBackend(imessageModel, AppConsts.CREATING).subscribe(response => {
        console.log(response);
        this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, AppConsts.CREATED, AppConsts.LOADER_MESSAGE_DURATION);
        this.updateRoleTree(this.createupdatedRoleJson, "Create");
        var payload = { "InsertAtPosition": 'Roles', "NodeData": this.createupdatedRoleJson };
        this.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissions, TreeOperations.AddNewNode, payload);
        this.cancel();
        setTimeout(() => {
          let entityName = this.createupdatedRoleJson ? this.createupdatedRoleJson["EntityName"] : "";
          this.authorizationService.getRoleDetails(entityName);
        }, 500);
      });
    } catch (ex) {
      console.error('Exception in saveEditForm() of securityrole Component at time ' + new Date().toString() + '. Exception is : ' + ex);
      this.logger.logException(new Error('Exception in saveEditForm() of SecurityRoleComponent at time ' + new Date().toString() + '. Exception is : ' + ex));
    }
  }

  saveEditForm() {
    try {
      let isRoleNameChanged = this.roleDetails.newRoleName != this.roleDetails.oldRoleName;
      let OldRoleName = this.roleDetails.oldRoleName;
      let AllForms = [];
      if (isRoleNameChanged && this.rolePermissiondata.length) {
        AllForms = this.rolePermissiondata;
      }
      //this.roleInfoDetails.RolePermissions = this.updatedFormPermissions;
      let payload = {
        Capability: this.commonService.getCapabilityNamebasedontabforSecurity(),
        CapabilityId: this.commonService.getCapabilityidbasedontabforSecurity(),
        RoleName: this.roleDetails.newRoleName,
        RoleId: this.authorizationService.securityRoleId,
        UpdatedSchemas: this.updatedFormPermissions,
        NewSchemas: this.newFormsAdded,
        DeletedSchemas: this.deletedForms,
        isRoleNameChanged: isRoleNameChanged,
        OldRoleName: OldRoleName,
        AllForms: AllForms
      };
      let imessageModel: IModelMessage = this.commonService.getMessageWrapper({
        DataType: DataTypes.FORMSCHEMAROLES,
        EntityType: MessageEntityTypes.ROLESCHEMAPERMISSIONS,
        MessageKind: MessageKind.UPDATE,
        Payload: JSON.stringify(payload)
      });
      if (this.updatedFormPermissions.length || this.newFormsAdded.length || this.deletedForms.length || isRoleNameChanged) {
        this.commonService.postDataToBackend(imessageModel, AppConsts.UPDATING).subscribe(response => {
          console.log(response);
          this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, AppConsts.UPDATED, AppConsts.LOADER_MESSAGE_DURATION);
          this.updateRoleTree(this.createupdatedRoleJson, "Update");
          let newPayload = { "EntityId": this.createupdatedRoleJson["EntityId"], "NodeData": this.createupdatedRoleJson };
          this.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissions, TreeOperations.updateNodeById, newPayload);
          this.clearExistingValuesAfterUpdate();
        });
      }
    } catch (ex) {
      console.error('Exception in saveEditForm() of securityrole Component at time ' + new Date().toString() + '. Exception is : ' + ex);
      this.logger.logException(new Error('Exception in saveEditForm() of SecurityRoleComponent at time ' + new Date().toString() + '. Exception is : ' + ex));
    }
  }
  clearExistingValuesAfterUpdate() {
    this.updatedFormPermissions = [];
    this.newFormsAdded = [];
    this.deletedForms = [];
    this.roleInfoDetails.RolePermissions = this.rolePermissiondata;
  }
  /**
   * cancel() method is called to close the role form on clicking the close icon of the role form.
   */
  cancel() {
    try {
      this.commonService.isFormModified = false;
      this.authorizationService.roleEntityId = "";
      let url = FABRICS.SECURITY;
      let enabledmode = this.authorizationService.getRolesOrSecurityGroupsMode();
      this.router.navigate([url], { queryParams: { 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable, 'enabledmode': enabledmode } });
    } catch (e) {
      console.error('Exception in cancel() of securityrole Component at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in cancel() of SecurityRoleComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }


  canDeactivate(): Observable<boolean> {
    var response;
    let message = DISCARD_CHANGES;
    this.openPopUpDialog(message, true, PopupOperation.PermissionAlertConfirm);
    response = this.dialogRef.componentInstance.emitResponse.map(res => {
      this.commonService.isFormModified = false;
      this.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissions, TreeOperations.deactivateSelectedNode, "");
      return true;
    })
    return response;
  }


  /**
  *
  * Used to open a confirmation dialog pop up to confirm with user about the current action.
  * @param {object } message: is array of string which will shown in confirmation pop up as message.
  * @example example of openPopUpDialog(message) method
  *
  *  openPopUpDialog(message){
  *     //todo
  *    }
  */
  openPopUpDialog(messageToshow, isOk, operation?): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: isOk,
        content: [messageToshow],
        subContent: [],
        operation: operation != undefined ? operation : PopupOperation.AlertConfirm
      };

      let matConfig = new MatDialogConfig();
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;

      this.dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
      this.dialogRef.componentInstance.emitResponse.subscribe((data: any) => {
        if (this.isDeleteForm && data.operation != PopupOperation.PermissionAlertConfirm) {
          this.isDeleteForm = false;
          this.deleteRole(this.deleteFormData);
        }
        else if (data && data.operation != PopupOperation.PermissionAlertConfirm) {
          this.sendDataToAngularTree(TreeTypeNames.rolesTreeForPermissions, TreeOperations.deactivateSelectedNode, "");
          this.cancel();
        }
      });

    }
    catch (ex) {
      console.error('Exception in openPopUpDialog() of SecurityRolesComponent at time ' + new Date().toString() + '. Exception is : ' + ex);
      this.logger.logException(new Error('Exception in openPopUpDialog() of SecurityRoleComponent at time ' + new Date().toString() + '. Exception is : ' + ex));
    }
  }

  
  /**
 *
 * Used to expandall panel in Security role form.
 * @example example of ExpandAllPanel() method
 *
 *  ExpandAllPanel(){
 *     //todo
 *    }
 */
  ExpandAllPanel(data) {
    try {
      this.expandAll = !this.expandAll;

      if (this.expandAll && this.svgIconList) {
        for (var key in this.svgIconList) {
          if (this.svgIconList.hasOwnProperty(key)) {
            this.svgIconList[key].bool = true;
            this.svgIconList[key].matIcon = this.ExpandSingle_Icon_Idle;
            if (!this.svgIconList[key]["enableBody"]) {
              this.svgIconList[key]["enableBody"] = true;
            }
          }
        }
      }
      else if (!this.expandAll && this.svgIconList) {
        for (var key in this.svgIconList) {
          if (this.svgIconList.hasOwnProperty(key)) {
            this.svgIconList[key].bool = false;
            this.svgIconList[key].matIcon = this.CollapsSingle_Icon_Idle;
          }
        }
      }
      this.expandCollapseIconChange();
    } catch (e) {
      console.error('Exception in ExpandAllPanel() of securityrole Component at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in ExpandAllPanel() of SecurityRoleComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  expandCollapseIconChange() {
    this.enabledisable = false;
    this.ref.detectChanges();
    this.expandCollapseIcon = this.expandAll ? 'V3 ExpandAll' : 'V3 CollapsAll';
    this.expandCollapseTitle = this.expandAll ? 'Collapse All' : 'Expand All';
    this.enabledisable = true;
    this.ref.detectChanges();
  }

  operationtypes(formName) {
    let currenttab = this.authorizationService.getRolesOrSecurityGroupstab();
    if (PermissionFormListForDeleteButtonDisable[currenttab] && PermissionFormListForDeleteButtonDisable[currenttab].findIndex(item => formName.includes(item)) >= 0) {
      let oplist = this.AllOperations.filter(item => item != "Delete");
      oplist.push('');
      return oplist;
    }
    else
      return this.AllOperations;
  }

  checkAllFormtoggleStatus(formdata: any, formName?) {
    this.allOperation.forEach((item, index, array) => {
      switch (item["Name"]) {
        case "delete":
        case "read":
        case "update":
        case "create":
          array[index]["Value"] = formdata.every(form => form['Permissions'] && form['Permissions'][item["Name"]] === true);
          break;
      }
    });
  }



  MainHeaderToggleChange(operationName, value) {
    this.rolePermissiondata.forEach((form, index, array) => {
      array[index]['Permissions'][operationName] = value;
      this.changeToggle(operationName, form.SchemaName, value);
    });
    this.checkAllFormtoggleStatus(this.rolePermissiondata);

  }

  formToggleChange(operation, formName, value) {
    this.changeToggle(operation, formName, value);
    this.checkAllFormtoggleStatus(this.rolePermissiondata);
  }
  /**
   * ngOnDestroy() method will be called for destroying the role forms.
   */
  ngOnDestroy() {
    this.authorizationService.securityRoleId = "";
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }
  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables);
  }
}
