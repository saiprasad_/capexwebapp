import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { AppConsts } from '../../../common/Constants/AppConsts';
import { PopUpAlertComponent } from '../../../common/PopUpAlert/popup-alert.component';
import { RenameComponent } from '../../../common/rename-dialog-form/rename-form.component';
import { CommonService } from '../../../globals/CommonService';
import { PopupOperation } from '../../../globals/Model/AlertConfig';
import { Datatypes, FABRICS, MessageKind, TreeOperations, TreeTypeNames } from '../../../globals/Model/CommonModel';
import { FabricRouter } from '../../../globals/Model/FabricRouter';
import { IModelMessage } from '../../../globals/Model/Message';
import { MapService } from '../services/map.service';
import { NewEntityMapComponent } from './newentitymapdialog/newentitymap.component';
import { NO_LONGER_AVAILABLE,CONTAIN_MAPS } from '../../../common/Constants/AttentionPopupMessage'
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  takeUntilDestroyObservables=new Subject();
  capability = FABRICS.MAPS;
  constructor(private mapService:MapService,private commonService : CommonService,private dialog: MatDialog, public router:Router, public route: ActivatedRoute)  { 
    this.commonService.lastOpenedFabric = FabricRouter.MAPS_FABRIC;
    this.mapService.getLeftTreeData()
    this.commonService.addNewEntity$.pipe(this.compUntilDestroyed()).pipe(filter((data1: any) => this.commonService.lastOpenedFabric == FABRICS.MAPS)).subscribe((data1: any) => {
      try {
        if(this.commonService.treeIndexLabel!=TreeTypeNames.LIST)
          this.openCreateNewForm();
      } catch (e) {
        console.error('Exception in addNewEntity$ observable of MapFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
      }
    });
    this.mapService.MapsCommonObservable.pipe(this.compUntilDestroyed()).subscribe((data: any) => {
      try {
        if (data.optionSelected == "Rename") {
          // var name = message.treePayload.nodeData.data.EntityName;
          let RenameContext = data.nodeData.data.EntityName;
          let alertmessgae = "Rename : " + RenameContext;
          this.openRenameDialog(alertmessgae, data);

        }
        else if (data.optionSelected == "Delete") {
          this.deletedialog(data.data);
        }
        // else if (message.treePayload.optionSelected == "Copy Dashboard") {
        //   let alertmessgae = "Copy Dashboard : " + message.treePayload.nodeData.data.EntityName;
        //   this.openRenameDialog(alertmessgae, message);
        // }
      } catch (e) {
        console.error('Exception in BiCommonObservable observable of MapComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
      }
    });
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.commonService.addNew$.next(this.commonService.lastOpenedFabric);        
     }, 100);
  }
  deletedialog(data) {
    var message = "";
    var submessage = "";
    try {
      if (!(data.children.length > 1 && data.EntityType == 'Category')) {
        message =   NO_LONGER_AVAILABLE(data.EntityName);
      }
      else {
        message =   CONTAIN_MAPS(data.EntityName);
      }
      let config = {
        header: 'Delete ' + data.EntityType,
        isSubmit: true,
        content: [message],
        subContent: [submessage],
        operation: PopupOperation.DeleteAll,

      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      var dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
      dialogRef.componentInstance.emitResponse.subscribe((res: any) => {
        if (res.DeleteConfigRecord && res.DeleteProdectionRecord) {
          this.deleteNode(data)
        }
      });
    }
    catch (e) {
      console.error('Exception in deletedialog() of MapComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }
  deleteNode(data:any) {
    let self = this;
    try {
      this.mapService.DeleteEnttiy(data.EntityId)
      var entityType = data.EntityType;
      var entityID = data.EntityId;
      let payLoad : any={}
      payLoad.EntityId=data.EntityId;
      payLoad.EntityName=data.EntityName;
      payLoad.ModifiedBy=this.commonService.currentUserId;
      payLoad.ModiFiedDateTime=new Date();
      payLoad.IsDeleted=true
      let imessageModel: IModelMessage = self.commonService.getMessageWrapper( {Payload : JSON.stringify(payLoad),DataType : Datatypes.MAPS,EntityType :  entityType,EntityID :  entityID,MessageKind : MessageKind.DELETE});
      self.commonService.postDataToBackend(imessageModel, AppConsts.DELETING).subscribe(response => {
        console.log(response);
        this.commonService.isRightTreeEnable = false;
        this.closeForm(data);
        self.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, AppConsts.DELETED, AppConsts.LOADER_MESSAGE_DURATION);
      });
    }
    catch (e) {
      console.error('Exception in deleteNode() of MapComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }
  openRenameDialog(data, message) {
    try {
      let result = undefined;
      let matConfig = new MatDialogConfig()
      matConfig.width = '500px';
      matConfig.height = '150px';
      matConfig.disableClose = true;
      matConfig.data = { "Header": data, "Operation": message.optionSelected, "ExistingName": message.nodeData.data.EntityName, "Parent": message.nodeData.data.Parent };
      let dialogRef = this.dialog.open(RenameComponent, matConfig);

      dialogRef.componentInstance.emitResponse.subscribe((Name) => {
        if (Name) {
          if (message.optionSelected == "Rename") {
            let data= message.nodeData.data;
            data.EntityName=Name
            let msg = {
              "EntityId": data.EntityId, "NodeData": { "EntityName": data.EntityName }
            };
            this.mapService.sendDataToAngularTree(TreeTypeNames.ALL, TreeOperations.updateNodeById, msg)
            this.mapService.updateNode(data)
            let payLoad : any={}
            payLoad.EntityId=data.EntityId;
            payLoad.EntityName=data.EntityName;
            payLoad.ModifiedBy=this.commonService.currentUserId;
            payLoad.ModiFiedDateTime=new Date();

            let imessageModel: IModelMessage = this.commonService.getMessageWrapper(  {Payload : JSON.stringify(payLoad),DataType : Datatypes.MAPS,EntityType :  message.EntityType,EntityID :  message.EntityID,MessageKind : "UPDATE"});
            this.commonService.postDataToBackend(imessageModel, AppConsts.UPDATING).subscribe(response => {
              console.log(response);
              this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, AppConsts.UPDATED, AppConsts.LOADER_MESSAGE_DURATION);
            });
          }
        }
      });
    }
    catch (e) {
      console.error('Exception in openRenameDialog() of MapComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }
  closeForm(data){
    try {
      let routeUrl = this.commonService.getEntityIDFromUrl(this.router.url)   
      let url = FABRICS.MAPS;
      if(data.EntityId == routeUrl){
        this.router.navigate([url], { queryParams: {'leftnav': this.commonService.isLeftTreeEnable, 'rightnav': this.commonService.isRightTreeEnable, 'tab': TreeTypeNames.ALL } });
      }
    }
    catch (e) {
      console.error('Exception in CloseForm() of MapComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in CloseForm() of MapComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  openCreateNewForm() {
    try {
      let matConfig = new MatDialogConfig()
      matConfig.width = '500px';
      matConfig.disableClose = true;

      const dialogRef = this.dialog.open(NewEntityMapComponent, matConfig);

    }
    catch (ex) {
      console.error(ex);
    }

  }
  ngOnInit(): void {
  }
  compUntilDestroyed():any {
    return takeUntil(this.takeUntilDestroyObservables);
  }
  ngOnDestroy() {
    try {

      this.takeUntilDestroyObservables.next();
      this.takeUntilDestroyObservables.complete();
    }
    catch (e) {
      console.error('Exception in ngOnDestroy() of MapFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }
}
