import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
// import { untilDestroyed } from 'ngx-take-until-destroy';
import { ALREADY_EXIST3, SELECTALL_INPUT_FIELDS } from 'ClientApp/app/components/common/Constants/AttentionPopupMessage';
import { Subject } from "rxjs";
import { takeUntil } from 'rxjs/operators';
import { AppConsts } from '../../../../common/Constants/AppConsts';
import { PopUpAlertComponent } from '../../../../common/PopUpAlert/popup-alert.component';
import { BIFabricService } from '../../../bifabric/services/BIFabricService';
import { CommonService } from '../../../../globals/CommonService';
import { PopupOperation } from '../../../../globals/Model/AlertConfig';
import { FABRICS, FabricsNames,Datatypes, TreeTypeNames, TreeOperations } from '../../../../globals/Model/CommonModel';
import { EntityTypes } from '../../../../globals/Model/EntityTypes';
import { FabricRouter } from '../../../../globals/Model/FabricRouter';
import { IModelMessage, Routing } from '../../../../globals/Model/Message';
import { MessagingService } from '../../../../globals/services/MessagingService';
import { MapService } from '../../services/map.service';

export const _filter = (opt: any, value: any): any => {
  const filterValue = value ? value.toLowerCase() : '';

  return opt.filter(item => item["viewValue"].toLowerCase().indexOf(filterValue) === 0);
};



@Component({
  selector: 'newentitybi',
  templateUrl: 'newentitymap.component.html',
  styleUrls: ['./newentitymap.component.styles.scss'],
})

export class NewEntityMapComponent implements OnDestroy, OnInit {
  @ViewChild(MatAutocompleteTrigger, {static: false}) autocompleteTrigger: MatAutocompleteTrigger;
  /**
 * parentList
 */

 // parentList: any = [];

  /**
   * bindedParentName
   */

  bindedParentName;

  /**
  * declaring input variable myForm using angular @Input() directive
  */

  @Input() myForm: FormGroup;

  /**
* declaring input variable schema using angular @Input() directive
*/

  @Input() schema: any;
  /**
  * declaring input variable data using angular @Input() directive
  */

  @Input() data: any;
  /**
 * observable
 */

  property = { 'Label': "" };
  EntityTypesArray = [];
  datas;
  List2;
  newEntityUpdate = true;
  DisableCategoryProperty: boolean = false;

  headerConfig = [
    {
      'source': 'V3 General',
      'title': 'Create New Entity',
      'routerLinkActive': false,
      'id': 'title',
      'class': 'icon21-21',
      'float': 'left',
      'type': 'title',
      'show': true,
      'uppercase':'Isuppercase'
    },
    {
      'source': 'V3 CloseCancel',
      'title': 'cancel',
      'routerLinkActive': false,
      'id': 'close',
      'class': 'icon21-21',
      'float': 'right',
      'type': 'icon',
      'show': true
    },
    {
      'source': 'FDC_Checkmark_Icon_Idle',
      'title': 'Save',
      'routerLinkActive': false,
      'id': 'save',
      'class': 'icon21-21',
      'float': 'right',
      'type': 'icon',
      'show': true
    }
  ];
 staticSchema="{\"Schema\":\"{\\\"EntityType\\\":{\\\"type\\\":\\\"select\\\",\\\"label\\\":\\\"Entity Type\\\",\\\"actualname\\\":\\\"EntityType\\\",\\\"validation\\\":{\\\"required\\\":true,\\\"minLength\\\":\\\"\\\",\\\"maxLength\\\":\\\"\\\",\\\"pattern\\\":\\\"\\\",\\\"custom\\\":\\\"\\\"},\\\"condition\\\":{\\\"show\\\":true},\\\"disable\\\":false,\\\"bindLabel\\\":\\\"EntityName\\\",\\\"update\\\":true,\\\"options\\\":[{\\\"Value\\\":\\\"Category\\\",\\\"EntityName\\\":\\\"Category\\\"},{\\\"Value\\\":\\\"Map\\\",\\\"EntityName\\\":\\\"Map\\\"}]},\\\"Parent\\\":{\\\"type\\\":\\\"select\\\",\\\"label\\\":\\\"Parent Entity\\\",\\\"actualname\\\":\\\"Parent\\\",\\\"validation\\\":{\\\"required\\\":false,\\\"minLength\\\":\\\"\\\",\\\"maxLength\\\":\\\"\\\",\\\"pattern\\\":\\\"\\\",\\\"custom\\\":\\\"\\\"},\\\"condition\\\":{\\\"show\\\":true},\\\"disable\\\":false,\\\"update\\\":true,\\\"bindLabel\\\":\\\"text\\\",\\\"options\\\":[]},\\\"EntityName\\\":{\\\"type\\\":\\\"string\\\",\\\"label\\\":\\\"Entity Name\\\",\\\"actualname\\\":\\\"EntityName\\\",\\\"validation\\\":{\\\"required\\\":true,\\\"minLength\\\":\\\"\\\",\\\"maxLength\\\":\\\"\\\",\\\"pattern\\\":\\\"\\\",\\\"custom\\\":\\\"\\\"},\\\"disable\\\":false,\\\"condition\\\":{\\\"show\\\":true},\\\"bindLabel\\\":\\\"EntityName\\\",\\\"update\\\":true}}\",\"DataModel\":\"{\\\"Parent Entity\\\":{\\\"type\\\":\\\"singleselect\\\",\\\"Value\\\":null},\\\"EntityType\\\":{\\\"type\\\":\\\"singleselect\\\",\\\"Value\\\":null},\\\"Entity Name\\\":{\\\"type\\\":\\\"string\\\",\\\"Value\\\":null}}\",\"EntityType\":\"AddNewBi\",\"FormName\":\"AddNewBi\",\"Fabric\":null,\"EntityId\":null,\"Data\":null}"
  currentCategoryType: any;
  formGroupControl;
  EntityTypesOptions = [];
  NewProperties = {}
  keys = [];
  Lists = [];
  takeUntilDestroyObservables=new Subject();

  sortByProperty = function (property) {
    return function (x, y) {
      return ((x[property].toLowerCase() === y[property].toLowerCase()) ? 0 : ((x[property].toLowerCase() > y[property].toLowerCase()) ? 1 : -1));
    };
  };

  constructor(public commonService: CommonService, public router: Router, public formBuilder: FormBuilder,
    public messagingService: MessagingService, public cdRef: ChangeDetectorRef,
    public dialog: MatDialog, public MapService: MapService,
    public route: ActivatedRoute, private fb: FormBuilder, public dialogRef: MatDialogRef<NewEntityMapComponent>) {
    dialogRef.disableClose = true;
  }



  createFormGroups() {
    try {
      this.keys.forEach(propertyName => {
        if (propertyName != 'Parent')
          var group = this.formBuilder.control(null, Validators.required);
        else
          var group = this.formBuilder.control(null);
        // subscribing to all the properties for value change event
        this.myForm.addControl(propertyName, group);

        group.valueChanges.subscribe(propertyVal => {

            if (propertyVal) {
              this.createControlAndChangesSubscription(propertyName, propertyVal);
              // this.valueChangesState = true;
              this.cdRef.detectChanges();
            }
        })
      });

    }
    catch (e) {
      console.error('Exception in createFormGroups() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
      this.commonService.appLogException(new Error('Exception in  createFormGroups() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  createControlAndChangesSubscription(name, value) {
    try {
          // if (name == 'EntityType') {
          //   if (value != null) {
          //     //this.BIFabricService.datasourcelist.sort((a, b) => (a.name > b.name) ? 1 : -1)
          //   } else {
          //     this.myForm.controls['DataSource'].setValue(null, { emitEvent: false });
          //   }
          //   if (value.Value == 'Category') {
          //     this.myForm.removeControl("DataSource");
          //   } else if (!this.myForm.contains("DataSource")) {
          //     var group = this.formBuilder.control(null, Validators.required);
          //     this.myForm.addControl("DataSource", group);
          //   }
          // }
    }
    catch (e) {
      console.error('Exception in createControlAndChangesSubscription() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
      this.commonService.appLogException(new Error('Exception in  createControlAndChangesSubscription() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }



  ClickEvent(ev) {
    switch (ev.id) {
      case 'close':
        this.dialogRef.close();
        this.dialog.ngOnDestroy();
        break;
      case 'save':
        this.nextFormShow();
        break;
    }
  }






  filter(filterValue) {
    if (filterValue) {
      var EntityTypeArray = this.EntityTypesArray.filter(option => option.toLowerCase().includes(filterValue.toLowerCase()));
      if (EntityTypeArray.length == 0) {
        filterValue = filterValue.slice(0, filterValue.length - 1);
        EntityTypeArray = this.EntityTypesArray.filter(option => option.toLowerCase().includes(filterValue.toLowerCase()));
      }
      return EntityTypeArray;
    }
    else {
      return this.EntityTypesArray;
    }
  }

  /**
   * @example example of openDialog method
   * openDialog (messageToshow) {
   *
   *          //todo
   * }
   */

  openDialog(messageToshow): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: false,
        content: [messageToshow],
        subContent: [],
        operation: PopupOperation.Attention
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;

      this.dialog.open(PopUpAlertComponent, matConfig);
    }
    catch (ex) {
      console.error(ex);
    }
  }



  ngOnInit() {
    try {
      let fabricName="";
      this.route.params.pipe(this.compUntilDestroyed()).subscribe(params => {
        let url = this.router.url;
        fabricName = this.commonService.getFabricNameByUrl(url);
      });
      this.myForm = this.formBuilder.group({});
      this.formGroupControl = this.formBuilder.group({});
      fabricName = fabricName == FABRICS.ENTITYMANAGEMENT ? FABRICS.BUSINESSINTELLIGENCE : fabricName;
      this.addNewEntitySchemaSubscription(JSON.parse(this.staticSchema));
      // this.commonService.readAddNewSchema(fabricName).filter(d => d).pipe(this.compUntilDestroyed()).subscribe(res => {
       
      //   this.addNewEntitySchemaSubscription(res);
      // })

    } catch (e) {
      console.error('Exception in ngOnInit() of NewEntityMapComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in ngOnInit() of NewEntityMapComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }





  createEntity(data) {
    let date = new Date().toLocaleDateString();
    var entityID = this.commonService.GetNewUUID();
    var entityType = data.EntityType;
    data.TenantName =  this.commonService.tenantName ;
    data.TenantId =  this.commonService.tenantID ;
    data.CreatedBy =  this.commonService.CurrentUserEntityName ;
    data.ModifiedBy =  this.commonService.CurrentUserEntityName ;
    data.Modified =  date ;
    data.Created =  date ;
    data.EntityId =  entityID ;
    data.Dashboardurl =  "" ;
    data.EntityName =  data["Entity Name"] ;
    data.LastChat = this.commonService.localtoTimestamp();
    data.Parent = data["Parent Entity"] ? data["Parent Entity"].id : "";

    if (data["Parent Entity"] == null) {
      data["Parent Entity"] = "";
    }
    var validate = true;

    var found = this.commonService.CheckExistEntityProperty('EntityName', data["Entity Name"], FabricsNames.MAPS);
    if (found) {
      this.commonService.DeleteEntityType = data["Entity Name"];
      this.commonService.TypeVal = entityType;
      validate = false;
    } else validate = true;

    if (!validate) {
      this.openDialog(ALREADY_EXIST3(this.commonService.TypeVal, this.commonService.DeleteEntityType));
    }

    if (validate == true) {
      var fabric = this.commonService.GetFabricName('/' + this.commonService.lastOpenedFabric);
      fabric = FABRICS.MAPS;
      this.MapService.AddEnttiy(data)
      var jsonString = JSON.stringify(data);
      var payload = { "EntityId": entityID, "TenantName": this.commonService.tenantName, "TenantId": this.commonService.tenantID, "EntityType": entityType, "Fabric": fabric, "Payload": jsonString };
      var payloadString = JSON.stringify(payload);

      var message = this.commonService.getMessage();
      message.EntityID = entityID;

      message.EntityType = entityType;
      message.Payload = payloadString;
      message.DataType = Datatypes.MAPS;
      message.MessageKind = "CREATE";
      message.Type = "Details";

      message.Routing = Routing.AllFrontEndButOrigin;

      let imessageModel: IModelMessage = this.commonService.getMessageWrapper({Payload : message.Payload,DataType : Datatypes.MAPS,EntityType :  message.EntityType,EntityID :  message.EntityID,MessageKind : "CREATE"} );
      this.commonService.postDataToBackend(imessageModel, AppConsts.CREATING).subscribe(response => {
        console.log(response);
        this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, AppConsts.CREATED, AppConsts.LOADER_MESSAGE_DURATION);
      });
      this.dialogRef.close();
      if(message.EntityType == EntityTypes.Map){
        this.router.navigate([FabricRouter.MAPS_FABRIC, 'map', data.EntityId], { queryParams: { 'EntityID': data.EntityId,TenantName:this.commonService.tenantName,TenantId:this.commonService.tenantID, 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
      }
      else{
        this.router.navigate([FabricRouter.MAPS_FABRIC],{ queryParams: {'leftnav': this.commonService.isLeftTreeEnable, 'rightnav': this.commonService.isRightTreeEnable, 'tab': TreeTypeNames.ALL } });
      }
      this.MapService.sendDataToAngularTree(TreeTypeNames.ALL, TreeOperations.ActiveSelectedNode, data.EntityId);     
      this.dialog.ngOnDestroy();
    }
  }





  nextFormShow() {

    if (this.myForm.valid) {
      this.data["Entity Name"] = this.myForm.value.EntityName;
      this.data["EntityType"] = this.myForm.value.EntityType.Value;
      this.data["Parent Entity"] = this.myForm.value.Parent;
      this.createEntity(this.data);
    }
    else {
      this.openDialog(SELECTALL_INPUT_FIELDS);
    }    
  }

  

 
 


  ngOnDestroy() {
    try {
      this.commonService.isCreateNewEntity = false;
      this.commonService.Addformclose = false;
      this.DisableCategoryProperty = false;
      this.takeUntilDestroyObservables.next();
      this.takeUntilDestroyObservables.complete();
      if (this.data) {
        this.commonService.allExpandState = false;
        if (this.data.EntityType) { this.data.EntityType = ""; }
        if (this.data["Entity Name"]) { this.data["Entity Name"] = ""; }
        // this.commonService.parentFieldVar = null;
        this.commonService.FormSchemaData$ = undefined;
      }
    } catch (e) {
      console.error('Exception in ngOnDestroy() of NewEntityMapComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in ngOnDestroy() of NewEntityMapComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  compUntilDestroyed():any {
    return takeUntil(this.takeUntilDestroyObservables);
  }

  addNewEntitySchemaSubscription(payloadObj:any){
    try {
      //var payloadObj = JSON.parse(message.Payload);

        var jsonschema = JSON.parse(payloadObj.Schema);
        var datamodel = JSON.parse(payloadObj.DataModel);
        this.commonService.FormSchemaData$ = {
          "Schema": jsonschema, "Data": datamodel
      }
      this.schema = jsonschema;
      this.data = datamodel;
      this.NewProperties = this.schema;

      this.myForm = this.formBuilder.group({});
      this.formGroupControl = this.formBuilder.group({});
      if (!this.commonService.isCapbilityAdmin()) {
        var optionlist = [];

       // this.NewProperties['EntityType']['options'] = optionlist;
      }
      this.EntityTypesOptions = this.NewProperties['EntityType']['options'];
      this.NewProperties['Parent']['options'] = this.commonService.getEntititesBy('EntityType', ['Category'], d => { return { "id": d.EntityId, "text": d.EntityName } },FABRICS.MAPS)
      .map(d => JSON.parse(JSON.stringify(d))).sort(this.sortByProperty('text'));
      this.keys = Object.keys(this.NewProperties);
      this.Lists = [];
      this.keys.forEach((res) => { this.Lists.push(this.NewProperties[res]); });
      this.createFormGroups();

      this.cdRef.detectChanges();

    } catch (e) {
    }
  }
}

