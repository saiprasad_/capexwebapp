import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { CommonModule } from '@angular/common';
import { MapSourceComponent } from './map-source.component';
import { routing } from './map-source.routing';
import { SharedModule1 } from '../../../../shared/shared1.module';
import { AngularMaterialModule } from '../../../../../../app/app.module';
import { HttpClientModule } from '@angular/common/http';

const MAP_SOURCE_COMPONENTS = [MapSourceComponent];

@NgModule({
  imports: [CommonModule, SharedModule1, routing, AngularMaterialModule, HttpClientModule],
  declarations: [MAP_SOURCE_COMPONENTS]
})

export class MapSourceModule { }
