import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { combineLatest, Subject } from 'rxjs';
import { AuthCommonStoreQuery } from '../../../common/state/Auth/auth.state.query';
import { StateRecords } from '../../../common/state/common.state';
import { EntityCommonStoreQuery } from '../../../common/state/entity.state.query';
import { EntityFilterStoreQuery } from '../../../common/state/EntityFilter/entityfilter.query';
import { CommonService } from '../../../globals/CommonService';
import { WebWorkerService } from '../../../globals/components/ngx-web-worker/web-worker';
import { EntityTypes } from '../../../globals/Model/EntityTypes';
import { FABRICS, AngularTreeMessageModel, TreeTypeNames, FabricsNames, MessageKind, TreeOperations, ObservableStateMessage, Datatypes } from '../../../globals/Model/CommonModel';
import { UserStateConstantsKeys } from '../../../../../webWorker/app-workers/commonstore/user/user.state.model';
import { Routing } from '../../../globals/Model/Message';
import { WORKER_TOPIC } from '../../../../../webWorker/app-workers/shared/worker-topic.constants';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { filter } from 'rxjs/operators';
import { FabricRouter } from '../../../globals/Model/FabricRouter';
import { MapsEntities } from '../../../common/Constants/AppConsts';

@Injectable()
export class MapService {

  public onTreeEventChanges$ = new Subject<any>();
  public MapsCommonObservable = new Subject();
  DeleteListDialogEmittor: EventEmitter<any> = new EventEmitter();

  constructor(private http: HttpClient, private router: Router, public commonService: CommonService,public entityFilterStoreQuery:EntityFilterStoreQuery,
    public stateRecords: StateRecords, public entityCommonStoreQuery: EntityCommonStoreQuery, public authCommonStoreQuery: AuthCommonStoreQuery) {
    //this.workerInit();
    this.initializeTreeWorkersObservables();
    this.observablesubscribtion();  
   
  }
  observablesubscribtion() {
    this.commonService.sendDataFromAngularTreeToFabric.pipe(filter((message: any) => this.commonService.lastOpenedFabric == FABRICS.MAPS)).subscribe((message) => {
      try {
        this.GetEventFromTree(message);
      } catch (e) {
        console.error('Exception in sendDataFromAngularTreeToFabric Observable of MapsFabricService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
      }
      
    });
  }
  AddEnttiy(infoJson) {
    try {
      let info = this.getInfoJson(infoJson);
      this.entityCommonStoreQuery.upsert(info.EntityId, info);
      let NewNode: any = {
        'InsertAtPosition': infoJson["Parent"] != "" ? infoJson["Parent"] : "althingTreeRoot",
        'NodeData': info
      }
      this.sendDataToAngularTree(TreeTypeNames.ALL, TreeOperations.AddNewNode, NewNode);

      // this.commonService.AddTemp = false;

    } catch (e) {
      console.error('Exception in AddEnttiy observable  of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }
  DeleteEnttiy(EntityId) {
    try {
      this.entityCommonStoreQuery.deleteNodeById("EntityId", EntityId);
      this.sendDataToAngularTree(TreeTypeNames.ALL, TreeOperations.deleteNodeById, EntityId)
    } catch (e) {
      console.error('Exception in DeleteEnttiy of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }
  getInfoJson(infoJson) {
    try {
      var info: any = new Payload();
      info.Capability = infoJson["Capability"] != null ? infoJson["Capability"] : this.commonService.capabilityFabricId;
      info.children = [];
      info.EntityType = infoJson["EntityType"]
      info.EntityName = infoJson["EntityName"]
      info.EntityId = infoJson["EntityId"]
      info.Parent = [{EntityId:infoJson["Parent"]}];
      info["SG"] = [FabricsNames.MAPS];
      info["Capabilities"] = this.commonService.capabilityFabricId;
      return info;
    }
    catch (ex) {

    }
  }
  GetEventFromTree(data: AngularTreeMessageModel) {
    try {

      let treeName = data.treeName[0];
      if (data.treePayload.nodeData) {
        data.treePayload.data = data.treePayload.nodeData.data;
      }
      if (data.treePayload.data) {
        if (data.treeOperationType != TreeOperations.expanderClick) {
          this.commonService.ContextMenuData = data.treePayload.data;
        }
        if (data.treePayload.data.TypeOf == EntityTypes.List || data.treePayload.data.EntityType == EntityTypes.List) {
          this.commonService.ListContextMenuData = data.treePayload.data;
        }
      }
      if (this.commonService.rightSideCapabilityList && this.commonService.rightSideCapabilityList.length != 0 && ([TreeTypeNames.ALL, TreeTypeNames.LIST].includes(treeName)) && data.treeOperationType == TreeOperations.NodeOnClick) {
        this.commonService.rightSideCapabilityList.forEach(data => {
          if (data["routerLinkActive"])
            data["routerLinkActive"] = false;

          this.commonService.isRightTreeEnable = false;
          this.commonService.layoutJSON.RightTree = false;
        })
      }

      switch (treeName) {
        case TreeTypeNames.ALL:
      {
          switch (data.treeOperationType) {
            case TreeOperations.NodeOnClick: {
              if (data.treePayload.data.EntityType&&data.treePayload.data.EntityType==EntityTypes.Map && data.treePayload.data.EntityId != "HaulLocation")
                this.singleClick(data);
              break;
            }
            case TreeOperations.FullTreeOnCreation: {
              this.TreeOnFirstTimeLoad(treeName, data.treePayload);
              break;
            }
            case TreeOperations.filteroptionjson: {
              this.filteredjsonstructure(treeName);
              break;
            }
            case TreeOperations.selectedfilteroption: {
              this.filterTreeData(data);
              break;
            }
            case TreeOperations.NodeOnContextClickBeforeInit: {
               if (data.treePayload.data.EntityId != "HaulLocation" && data.treePayload.data.EntityType) {
                this.generateContextMenuJson(data.treePayload.data);
              }
              break;
            }
            case TreeOperations.NodeContextOptionClicked:
              this.contextClickEvent(data.treePayload);
              break;
            case TreeOperations.entityFilter:
              this.entityFilter(data);
              break;
            case TreeOperations.RemoveFilter:
              this.RemoveFilterFromTree(data);
              break;
          }

        }
          break;

        case TreeTypeNames.LIST: {
          switch (data.treeOperationType) {
            case TreeOperations.NodeOnClick: {
              this.singleClick(data);
              break;
            }

            // case TreeOperations.NodeContextOptionClicked: {
            //   this.contextClickEvent(data.treePayload, treeName);
            //   break;
            // }
            // case TreeOperations.filteroptionjson: {
            //   this.filteredjsonstructure(treeName);
            //   break;
            // }
            // case TreeOperations.selectedfilteroption: {
            //   this.filterTreeData(data);
            //   break;
            // }
            // case TreeOperations.RemoveFilter:
            //   this.RemoveFilterFromTree(data);
            //   break;
            // case TreeOperations.entityFilter:
            //   this.entityFilter(data);
            //   break;
            // case TreeOperations.NodeOnContextClickBeforeInit: {
            //   break;
            // }
            // case TreeOperations.NodeOnDrop: {
            //   this.commonService.moveNodeFromTo(data.treePayload);
            // }
          }
        }
          break;

      }
    }
    catch (e) {
      console.error('Exception in GetEventFromTree() of Map Service in  Map/service at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in GetEventFromTree() of Map Service in Map/service at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  TreeOnFirstTimeLoad(treename: string, tree: object) {
    try {
      var jsonObject = { TreeType: treename, MessageData: tree };
      this.onTreeEventChanges$.next(jsonObject);
    }
    catch (e) {
      console.error('Exception in TreeOnFirstTimeLoad() of EntityMgt in  EntityMgt/service at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }
  generateContextMenuJson(data) {
    try {
        let contextMenuOption;
        let childlist = [];
          childlist.push({ displayName: 'Rename', children: [] });
          childlist.push({ displayName: 'Delete', children: [] });
        contextMenuOption = [{
          displayName: 'Maps Context Menu',
          children: childlist
        }]
        this.sendDataToAngularTree(TreeTypeNames.ALL, TreeOperations.treeContextMenuOption, contextMenuOption);
        this.commonService.activeContextMenuNodeId = data.EntityId;
    } catch (e) {
      console.error('Exception in createContextMenuToAllTab() of BIFabricService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }
  filteredjsonstructure(treeName) {
    try {
      let key =this.commonService.capabilityFabricId+"_"+treeName
      this.entityFilterStoreQuery
        .selectEntity(key).first()
        .subscribe(res=>{
        if(res){
          var message: AngularTreeMessageModel = {
            "fabric": this.commonService.lastOpenedFabric,
            "treeName": [this.commonService.treeIndexLabel],
            "treeOperationType": TreeOperations.filteroptionjson,
            "treePayload": JSON.parse(JSON.stringify(res))
          };
          this.commonService.sendDataToAngularTree.next(message);
        }
        else{
              let EntityTypeKeyValue={}
              MapsEntities.forEach(d=>{EntityTypeKeyValue[d]=true})
              let data={
                EntityTypes:EntityTypeKeyValue,
              }
                let schema=[
                  {
                    "expand":true,
                    "expandIcon":"V3 ToggleDown",
                    "name": "ENTITY TYPES",
                    "actualName":"EntityTypes",
                    "type": "textWithcross",
                  }
                ]
                var message: AngularTreeMessageModel = {
                  "fabric": this.commonService.lastOpenedFabric,
                  "treeName": [this.commonService.treeIndexLabel],
                  "treeOperationType": TreeOperations.filteroptionjson,
                  "treePayload": {schema: schema,data: data,capability: this.commonService.capabilityFabricId,treeName: treeName
                  }
                };
                this.commonService.sendDataToAngularTree.next(message);
             // })
            //})
           
        }
        });

    
      
    }
    catch (e) {
      console.error('Exception in filteredjsonstructure() of MapService in Map/Service at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }
  entityFilter(Data :AngularTreeMessageModel){
    var types = [EntityTypes.Category,EntityTypes.DashBoard,EntityTypes.Map]
    this.commonService.getEntititesFromStore$("EntityType", types,d=>true,
      d => { return d },
      FABRICS.MAPS)
       .pipe(untilDestroyed(this,'destroy')).first()
       .subscribe((filteredentities: any) => {
        this.sendDataToAngularTree(Data.treeName[0], TreeOperations.entityFilter, {Entities:filteredentities,value:Data.treePayload})
      });
      if (Data.treePayload == "") {
          if (this.router && this.router.url && this.router.url.includes('EntityID')) {
            let entityId = this.commonService.getEntityIDFromUrl(this.router.url);
            this.sendDataToAngularTree(Data.treeName[0], TreeOperations.ActiveSelectedNode, entityId);
          }
      }
  }
  RemoveFilterFromTree(data){
    this.sendDataToAngularTree(data.treeName[0], TreeOperations.RemoveFilter,'');
  }

  filterTreeData(Data){
    let filterData=Data.treePayload;
    let types = [EntityTypes.Category,EntityTypes.DashBoard]
    let EntityType=Object.keys(filterData.EntityTypes).filter(d=>filterData.EntityTypes[d]).reduce((obj, key) => {obj[key] = filterData.EntityTypes[key];return obj;},{});
    let EntityTypeArray=Object.keys(EntityType)
    let filterFunc=d => EntityTypeArray.includes(d.EntityType) ;
    this.commonService.getEntititesFromStore$("EntityType", types,d=>true,
    d => { return d },
    FABRICS.MAPS)
      .pipe(untilDestroyed(this,'destroy')).first()
      .subscribe((filteredentities: any) => {
      this.sendDataToAngularTree(Data.treeName[0], TreeOperations.filterTree, {Entities:filteredentities,filterFunc:filterFunc})
    });
  }
  contextClickEvent(data) {
    if (data.optionSelected != "New Location")
      this.commonService.createbyContextmenu = true;

    switch (data.optionSelected) {
      case "Rename":
      case "Delete": {
        this.MapsCommonObservable.next(data);
        break;
      }
      default:break;
    }
  }
  singleClick(data){
    this.router.navigate([FabricRouter.MAPS_FABRIC, 'map', data.treePayload.data.EntityId], { queryParams: { 'EntityID': data.treePayload.data.EntityId,TenantName:this.commonService.tenantName,TenantId:this.commonService.tenantID, 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
  }
  sendDataToAngularTree(treeName1: string, treeOperationType: string, payload: any) {
    try {
      var treeMessage: AngularTreeMessageModel = {
        "fabric": FABRICS.MAPS.toLocaleUpperCase(),
        "treeName": [treeName1],
        "treeOperationType": treeOperationType,
        "treePayload": payload
      }
      this.commonService.sendDataToAngularTree.next(treeMessage);
    } catch (e) {
      console.error('Exception in sendDataToAngularTree() of BIFabricService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }
  getLeftTreeData() {
    var stateLastModifiedUTCMS = this.commonService.getUsersStateKey(UserStateConstantsKeys.modifiedTime);
    var isdateTime = false;
    var currentTimeUTCMS = Math.floor((new Date()).getTime() / 1000);

    if (stateLastModifiedUTCMS) {
      var windowCloseUTCMS = this.commonService.getUsersStateKey(UserStateConstantsKeys.windowCloseTime);

      if (windowCloseUTCMS && windowCloseUTCMS > stateLastModifiedUTCMS) {//resetting store
        var closeAndModifiedTimeDifference = currentTimeUTCMS - windowCloseUTCMS;
        if (closeAndModifiedTimeDifference > 6000) {
          //clear all store except users store
          this.stateRecords.reSetStore();
          this.commonService.addUserStateKeyValue(UserStateConstantsKeys.viewedFabrics, []);// Reset Viewed Fabrics
        }
        else {
          //based on datetime fetch latest entities
          isdateTime = true;
        }
      }
      else {
        //based on datetime fetch latest entities
        isdateTime = true;
      }
    }
    else {
      isdateTime = true;
    }

    

    setTimeout(() => {
      let isEntitiesExist: boolean = this.commonService.getViewedFabricsAndCheckStatus(FabricsNames.MAPS);
      let listStatus:boolean = this.commonService.checkListStateStatus("EntityType","List",FabricsNames.MAPS);
      
      if (!isEntitiesExist) {
        this.leftTreeSendBackend();
       // this.ListTreeSendBackEnd();
      }
      else if(!listStatus){
        //this.ListTreeSendBackEnd();
      }
      else if (isdateTime) {
       this.dateTimeBasedReadTreeBackend(currentTimeUTCMS);
     }
    }, 10);
  }

  updateNode(data){
    this.entityCommonStoreQuery.upsert(data.EntityId,  data);
  }

  dateTimeBasedReadTreeBackend(currentTimeUTCMS: number) {
    try {
      let isSubscribe = true;
      this.entityCommonStoreQuery
        .selectAll({
          filterBy: (entity: any) => entity && entity.SG && (<Array<any>>entity.SG).indexOf(FabricsNames.MAPS) >= 0
        })
        .filter(data => data.length != 0 && isSubscribe)
        .map(data => JSON.parse(JSON.stringify(data)))
        .shareReplay(1).first().subscribe(res => {
          var date = this.commonService.maxModifiedDateTimeStateObject(res);

          var last_utc = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
            date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()) / 1000;

          if (last_utc != NaN && currentTimeUTCMS - last_utc > 60) {
            var dateTime = new Date(last_utc * 1000).toISOString().slice(0, 19).replace('T', ' ');
            this.leftTreeSendBackend(dateTime);
          }
          isSubscribe = false;
        });
    }
    catch (e) {
      console.error("BIService dateTimeBasedReadTreeBackend: " + e);
    }
  }

  ListTreeSendBackEnd() {
    var capabilityName = this.commonService.getCapabilityNameBasedOnFabric();
    var userPayload = {
      "Info": EntityTypes.UserList, "isContextMenu": false, "tab": TreeTypeNames.LIST, "CapabilityName": capabilityName
    };

    let message = this.commonService.getMessage();
    message.Payload = JSON.stringify(userPayload);
    message.DataType = TreeTypeNames.LIST;
    message.EntityID = this.commonService.currentUserName;
    message.EntityType = EntityTypes.UserList;
    message.MessageKind = MessageKind.READ;
    message.Routing = Routing.OriginSession;
    message.Type = "Details";
    //this.commonService.statusCheck(message);
    message.Fabric = FabricsNames.MAPS.toUpperCase();

    this.commonService
      .httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .map(res => {
        let messageData = JSON.parse(res);
        let data = JSON.parse(messageData["Payload"]);
        return data;
      })
      .subscribe((data: any) => {
        this.commonService.addListEntitiesToState(data, FabricsNames.MAPS);
      });
  }

  leftTreeSendBackend(dateTime?) {
    try {
      if (this.commonService.capabilityFabricId == null || this.commonService.capabilityFabricId == undefined) {
        this.commonService.capabilityFabricId = this.commonService.FabricCapabilityIdAtLoginTime;
      }

      let opsPayload = { "EntityType": FABRICS.MAPS, "DataType": Datatypes.MAPS, "Fabric": FabricsNames.MAPS, "Info": "General", "CapabilityId": this.commonService.capabilityFabricId };
      if (dateTime) {
        opsPayload["ModifiedDateTime"] = dateTime;
      }
      let CaapbilityId = this.commonService.getCapabilityId(FabricsNames.MAPS);
      let message = this.commonService.getMessage();
        message.Payload = JSON.stringify(opsPayload);
        message.DataType =  Datatypes.MAPS;
        message.EntityID = FabricsNames.MAPS;
        message.EntityType = FabricsNames.MAPS;
        message.MessageKind = MessageKind.READ;
        message.Routing = Routing.OriginSession;
        message.CapabilityId = CaapbilityId;
        message.Type = "Details";
       //this.commonService.statusCheck(message);
        message.Fabric = FabricsNames.MAPS;

      this.commonService
        .httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
        .filter(data => CaapbilityId).map(res => {
          var messageData = JSON.parse(res);
          let data = JSON.parse(messageData["Payload"]);
          return data;
        })
        .subscribe(data => {
        //let data:any="{\"categories\":[{\"EntityId\":\"bf6d224e-5ad0-4a01-ad37-7e8cfd8d79d3_3\",\"EntityName\":\"CateGory 001\",\"EntityType\":\"Category\"},{\"EntityId\":\"bf6d224e-5ad0-4a01-ad37-7e8cfd8d79d3_4\",\"EntityName\":\"CateGory 002\",\"EntityType\":\"Category\",\"Parent\":[{\"EntityId\":\"bf6d224e-5ad0-4a01-ad37-7e8cfd8d79d3_3\",\"EntityName\":\"CateGory 001\"}]}],\"maps\":[{\"EntityId\":\"bf6d224e-5ad0-4a01-ad37-7e8cfd8d79d3_5\",\"EntityName\":\"CateGory 002\",\"EntityType\":\"Map\",\"Parent\":[{\"EntityId\":\"bf6d224e-5ad0-4a01-ad37-7e8cfd8d79d3_3\",\"EntityName\":\"CateGory 001\"}]}]}"
        //data=JSON.parse(data)  
        var entities = [...data.categories,
            ...data.maps
          ];
          //let entities=[{"EntityId":"GPS MAP 1","EntityName":"GPS MAP 1","EntityType":"Map"},{"EntityId":"GPS MAP 2","EntityName":"GPS MAP 2","EntityType":"Map"},{"EntityId":"GPS MAP 3","EntityName":"GPS MAP 3","EntityType":"Map"},{"EntityId":"GPS MAP 4","EntityName":"GPS MAP 4","EntityType":"Map"}]
          let existingEntities = [];
          let newEntities = [];
          for (var index in entities) {
            if (entities[index]) {
              let entityId = entities[index].EntityId;
              let entityData = this.entityCommonStoreQuery.getEntity(entityId);
              if (entityData) {
                // if BI not exist in SG then add into existing entities list.
                if (entityData["SG"] != undefined) {
                  if (!((<Array<any>>entityData["SG"]).indexOf(FabricsNames.MAPS) >= 0)) {
                    existingEntities.push(entityId);
                  }
                  continue;
                }
              }
              entities[index]["SG"] = [FabricsNames.MAPS];
              entities[index]["TypeOf"] = entities[index]["EntityType"];
              newEntities.push(entities[index]);
            }
          }

          if (existingEntities.length > 0)
            this.entityCommonStoreQuery.UpdateArrayValue(existingEntities, "SG", FabricsNames.MAPS);

          if (newEntities.length > 0)
            this.entityCommonStoreQuery.AddAll(newEntities);

          this.commonService.updateViewedFabric(FabricsNames.MAPS);
        });
    }
    catch (e) {
      console.error('Exception in leftTreeSendBackend() of BIService at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }
  initializeTreeWorkersObservables() {
    try {

      //bi common Tree
      this.commonService.initializeTreeWorkersObservablesMaps();

      let entities$ = this.entityCommonStoreQuery
        .selectAll({
          filterBy: (entity: any) => entity&&[EntityTypes.Map].indexOf(entity.EntityType) >= 0 && entity.SG && (<Array<any>>entity.SG).indexOf(FabricsNames.MAPS) >= 0
        })
        .debounceTime(150)
        .filter(data => data.length != 0)
        .map(data => JSON.parse(JSON.stringify(data)))
        .shareReplay(1);

      let listEntities$ = this.entityCommonStoreQuery
        .selectAll({
          filterBy: (entity: any) =>
            entity&&(([EntityTypes.List, EntityTypes.Personal, EntityTypes.Shared, EntityTypes.Carousel].indexOf(entity.EntityType) >= 0) ||
              (entity.Parent && entity.Parent.length > 0 && entity.Parent[0].EntityName == EntityTypes.Carousel)) && entity.SG && (<Array<any>>entity.SG).indexOf(FabricsNames.MAPS) >= 0
        })
        .debounceTime(150)
        .filter(data => data.length != 0)
        .map(data => JSON.parse(JSON.stringify(data)))
        .shareReplay(1);

      // let lists$ = listEntities$
      //   .combineLatest(entities$.startWith([]))
      //   .map(d => this.commonService.createListEntities(d[0], FabricsNames.MAPS));
      let lists$ = combineLatest(listEntities$,entities$.startWith([]))
      .map(d => this.commonService.createListEntities(d[0], FabricsNames.MAPS));

      let model:ObservableStateMessage=  { Capability: FabricsNames.MAPS, Tree: TreeTypeNames.LIST, observable: lists$.shareReplay(1) };
      this.commonService.addIntoObservableStateService(model);

    } catch (e) {
      console.error(e);
    }
  }
  destroy() { }

}
class Payload {
  public Capability;
  public children;
  public EntityType;
  public parent;
  public EntityId;
  public EntityName;
  public Icon;

}