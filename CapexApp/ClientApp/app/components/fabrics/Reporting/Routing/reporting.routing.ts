import { Routes, RouterModule } from '@angular/router';
import { ReportingComponent } from './../Components/reporting.component';
const routes: Routes = [
    {
        path: '', component: ReportingComponent,
        children: []
    }
];

export const routing = RouterModule.forChild(routes);
