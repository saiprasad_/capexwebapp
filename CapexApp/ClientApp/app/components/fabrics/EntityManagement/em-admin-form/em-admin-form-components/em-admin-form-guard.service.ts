import { Injectable } from "@angular/core";
import { CanDeactivate } from "@angular/router";
import { Observable } from "rxjs";
import { EmAdminFormComponent } from "./em-admin-form.component";

@Injectable()
export class EMAdminGaurdService implements CanDeactivate<EmAdminFormComponent>{

  canDeactivate(component: EmAdminFormComponent): Observable<boolean> {
    if (component.myForm && component.myForm.invalid && component.entityMgmtCommonInputVariable && component.entityMgmtCommonInputVariable["updated"] && component.expandCollapsebooleanValue) {
      return component.canDeactivate()
    }
    else{
      component.expandCollapsebooleanValue = true;
      return Observable.of(true);
    }
  }
}