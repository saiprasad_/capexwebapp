import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmAdminFormComponent } from './em-admin-form.component';

describe('EmAdminFormComponent', () => {
  let component: EmAdminFormComponent;
  let fixture: ComponentFixture<EmAdminFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmAdminFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmAdminFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
