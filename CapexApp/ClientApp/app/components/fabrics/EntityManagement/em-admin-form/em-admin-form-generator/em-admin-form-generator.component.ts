import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'ClientApp/app/components/globals/CommonService';
import { FABRICS, MessageKind } from 'ClientApp/app/components/globals/Model/CommonModel';
import { EntityTypes } from 'ClientApp/app/components/globals/Model/EntityTypes';
import { MessageModel, Routing } from 'ClientApp/app/components/globals/Model/Message';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { EMGlobalAdmin, IEntityInterface } from '../../entityUtil';
import { EntityManagementService } from '../../services/EntityManagementService';
import { FDCFormGeratorCommon } from 'ClientApp/app/components/common/services/FDCFormGeneratorCommonService/FDCFormGeratorCommon';
import { Mesurements } from '../../../../common/Constants/JsonSchemas';

@Component({
  selector: 'em-admin-form-generator',
  templateUrl: './em-admin-form-generator.component.html',
  styleUrls: ['./em-admin-form-generator.component.scss']
})

export class EmAdminFormGeneratorComponent implements OnInit {
  @Input() EntityName;
  @Input() myForm: FormGroup;
  @Input() EntityType;
  EntityId
  intialized:boolean;
  jsonData :any;
  METER_JSON : any; 
  properties :any;
  headerGrid :any;
  contentGrid:any;
  columnRowSpanCount
  listOfChildren:any =[]
  nestedAccordionIdsList = [];
  headerdata: any = [];
  HierarchyArray:any = ["District", "Area", "Field"];
  contentGrid1 = {
    numberOfCol: 2,
    columnRowSpan: 2
  }
  typeOdNestetAccordian
  expandCollapse:boolean;
  expandShow:boolean=true;
  createDeletePermission = { Create: true, Delete: true, Read: true, Update: true };
  ExpandSingle_Icon_Idle = "V3 ToggleDown";
  CollapsSingle_Icon_Idle = "ExpandAccordion";
  entityUtil
  Params :any={}
  takeUntilDestroyObservables=new Subject();
  constructor(private emService : EntityManagementService,public route : ActivatedRoute,public commonService:CommonService,private cdRef: ChangeDetectorRef,public FDCFormGeratorCommonService: FDCFormGeratorCommon,public formBuilder:FormBuilder, public dialog: MatDialog) {
    this.emService.EntityMgmtCommonObservable.pipe(filter((data: any) =>
    (data.EntityID == this.EntityName && data.MessageKind == MessageKind.READ))).pipe(this.compUntilDestroyed()).subscribe((message: MessageModel) => {
      try {
        switch (message.DataType) {
          case "EMGlobalAdmin":
            if(message.Type!="ShowMore")
              this.globalAdminConfig(message);
            break;
        }
      }
      catch (e) {
        this.commonService.appLogException(new Error('Exception in fieldDataCaptureCommonFormObservable() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
      }
    })
   }

  ngOnInit() {
    this.expandCollapse=true
    this.route.params.pipe(this.compUntilDestroyed()).subscribe((params:any) => {
      if(Object.keys(this.Params).length>0||this.Params.Expand != params.Expand){
        this.expandCollapse=JSON.parse(params.Expand)
      }
      this.Params = JSON.parse(JSON.stringify(params));

    });
    this.EMGlobalAdmin()
  }
  
  EMGlobalAdmin() {
    this.HierarchyArray.forEach(element => {
    this.EntityId = this.commonService.tenantID + "_global";
    var payload1 = { "EntityType": "Schema", "Fabric": "FDC", "Info": this.EntityName ,FormType:this.EntityName,"Type":element};
    let PayLoad = JSON.stringify(payload1);
    //this.commonService.sendMessageToServer(PayLoad, "EMGlobalAdmin", this.EntityName, this.EntityType, MessageKind.READ, Routing.OriginSession,element, FABRICS.FDC.toUpperCase());
  });
    this.cdRef.markForCheck();
}
expandPannel(expand){
  try {
    this.expandCollapse = expand;
  } catch (error) {
    
  }
}
  globalAdminConfig(message) {
    try {
      if (message['Payload'] != "") {
        let payload = JSON.parse(message['Payload']);
        let formSchema = JSON.parse(payload["Schema"]);
        this.jsonData = JSON.parse(payload["Data"]);
        let jsonSchema = JSON.parse(formSchema.Schema);
        this.typeOdNestetAccordian = message.Type;
        this.METER_JSON = jsonSchema['Schema'];
        this.properties = jsonSchema['Properties'];
        this.METER_JSON["Controls"] = [];
        this.InitializeglobaladminSchema();
        if (this.myForm.controls[this.EntityName]) {
          this.myForm.controls[this.EntityName].setValue(this.EntityName, { emitEvent: false });
        }
        this.intialized = true
        // this.cdRef.markForCheck();
        //this.InitializeEntityUtilityHandler();//initialize global admin util
      }
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in globalAdminConfig() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
    /**
 * This is useing for Initialize the Globaladmin Schema
 * */
InitializeglobaladminSchema() {
  try {
    if (this.METER_JSON) {
      this.CommonIterationData();
    }
  } catch (e) {
    this.commonService.appLogException(new Error('Exception in InitializeglobaladminSchema() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
  }
}
CommonIterationData() {
  try {

    this.MeasurementConvertionFunction();
    this.FDCFormGeratorCommonService.setDefaultScope(this.properties, this.commonService.isCapbilityAdmin());
    this.headerGrid = this.METER_JSON['accordion']['header'];
    this.contentGrid = this.METER_JSON['accordion']['content'];
    /**
     * Accordion iteration
     */
    if (this.contentGrid) {
      this.columnRowSpanCount = 0;
      this.adminSchemaAccordionContent();
    }
  }
  catch (e) {
    this.commonService.appLogException(new Error('Exception in CommonIterationData() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
  }
}
MeasurementConvertionFunction() {
  if (this.properties) {
    for (let property in this.properties) {
      if (this.properties[property]['checkboxvalue']) {
        if (this.properties[property]['checkboxvalue'].true && Mesurements[this.properties[property]['checkboxvalue'].true]) {
          this.properties[property]['checkboxvalue'].true = Mesurements[this.properties[property]['checkboxvalue'].true];
        }
        if (this.properties[property]['checkboxvalue'].false && Mesurements[this.properties[property]['checkboxvalue'].false]) {
          this.properties[property]['checkboxvalue'].false = Mesurements[this.properties[property]['checkboxvalue'].false];
        }
      }
      if (this.properties[property]['measures_left']) {
        if (this.properties[property]['measures_left']['true'] && Mesurements[this.properties[property]['measures_left']['true']]) {
          this.properties[property]['measures_left']['true'] = Mesurements[this.properties[property]['measures_left']['true']];
        }
        if (this.properties[property]['measures_left']['false'] && Mesurements[this.properties[property]['measures_left']['false']]) {
          this.properties[property]['measures_left']['false'] = Mesurements[this.properties[property]['measures_left']['false']];
        }
      }
    }
  }
}
adminSchemaAccordionContent() {
  switch (this.contentGrid.type) {
    // case "table":
    //   this.adminSchemaAccordionContentTypeTable();
    //   break;
    case "section":
      this.adminSchemaAccordionContentTypeSection();
      break;
    // case "Multigrid":
    //   this.adminSchemaAccordionContentTypeMultigrid();
    //   break;
    // case "tables":
    //   this.adminSchemaAccordionContentTypeFacilityTables();
    //   break;
    // case "MultiDateViewGrid":
    //   this.adminSchemaAccordionContentMultiDateView();
    //   break;
    // default:
    //   this.adminSchemaAccordionContentTypeOthers();
    //   break;
  }
}
adminSchemaAccordionContentTypeSection() {
  /**
         * Binding the Accordions data to particular accordion in NestedAccordions
         */
  this.nestedAccordionInitialization(this.contentGrid);
  this.createGroupAccordianData(this.myForm);

  /*
   *
   * For Filtering Header MenuItems
   */
  if (!this.commonService.isCapbilityAdmin()) {
    if (this.headerGrid.actionmenu && this.headerGrid.actionmenu.cols) {
      this.FDCFormGeratorCommonService.updateFieldPermission(this.headerGrid.actionmenu, this.createDeletePermission);
      for (let col of this.headerGrid.actionmenu.cols) {
        this.FDCFormGeratorCommonService.objectLoop(col, this.properties)
      }
    }
    if (this.contentGrid.actionmenu && this.contentGrid.actionmenu.cols) {
      this.FDCFormGeratorCommonService.updateFieldPermission(this.contentGrid.actionmenu, this.createDeletePermission);
    }
  }

  /**
   * only headerdata filtering
   */
  for (let col of this.headerGrid.grid.cols) {
    if (col.type == 'grid') {
      this.FDCFormGeratorCommonService.gridLoop(col, this.properties, this.EntityType, this.METER_JSON)
    }
    else {
      this.FDCFormGeratorCommonService.objectLoop(col, this.properties, this.METER_JSON)
    }
  }

  //this.adminSchemaAccordionContentTypeCommon();
  //this.adminSchemaAccordionSetJSONDataControlAndGroup();
}
createGroupAccordianData(myForm) {
  this.listOfChildren.forEach(obj => {
    this.myForm.addControl(obj.EntityId, this.formBuilder.group({}));
  })
}

adminSchemaAccordionSetJSONDataControlAndGroup() {
  //this.createFullGroup();
  //this.FDCFormGeratorCommonService.MakeDetectChanges(this.cdRef);
}
nestedAccordionInitialization(content1, DataType?) {
  let content = JSON.parse(JSON.stringify(content1));
  for (let accordion in content.data.accordion) {
    if (content.data.accordion[accordion]) {
      for (let property in content.data.accordion[accordion].Properties) {
        var properties = content.data.accordion[accordion].Properties
        if (properties[property]['checkboxvalue']) {
          if (properties[property]['checkboxvalue'].true && Mesurements[properties[property]['checkboxvalue'].true]) {
            content.data.accordion[accordion].Properties[property]['checkboxvalue'].true = Mesurements[properties[property]['checkboxvalue'].true];
          }
          if (properties[property]['checkboxvalue'].false && Mesurements[properties[property]['checkboxvalue'].false]) {
            content.data.accordion[accordion].Properties[property]['checkboxvalue'].false = Mesurements[properties[property]['checkboxvalue'].false];
          }
        }
        if (properties[property]['measures_left']) {
          if (properties[property]['measures_left'].true && Mesurements[properties[property]['measures_left'].true]) {
            content.data.accordion[accordion].Properties[property]['measures_left'].true = Mesurements[properties[property]['measures_left'].true];
          }
          if (properties[property]['measures_left'].false && Mesurements[properties[property]['measures_left'].false]) {
            content.data.accordion[accordion].Properties[property]['measures_left'].false = Mesurements[properties[property]['measures_left'].false];
          }
        }
      }
      if(this.listOfChildren.filter(element => element.EntityId == this.typeOdNestetAccordian).length == 0){
      this.listOfChildren.push({ "Schema": content.data.accordion[this.typeOdNestetAccordian], "Data": this.jsonData , "EntityId": this.typeOdNestetAccordian, "EntityName": this.typeOdNestetAccordian, "GlobalParentAccordionName": this.EntityName, "EntityType": this.EntityType });
      this.listOfChildren = this.getSortingListOfChildren(this.listOfChildren)
      } 
    }
    
  }
  if (this.jsonData != null) {
    
  }
}
getSortingListOfChildren(listOfChildren) {
  try {
    let NewSortedListOfChildren = [];
    this.HierarchyArray.map((item) => {
      let SortArray = [];
      listOfChildren.forEach(element => {
        if (element.EntityName == item) {
          SortArray.push(element);
        }
      })
      SortArray.sort((firstEntity, secondEntity) => {
        if (firstEntity['EntityName'] && secondEntity['EntityName'])
          return firstEntity['EntityName'].toLowerCase() > secondEntity['EntityName'].toLowerCase() ? 1 : -1
      })
      NewSortedListOfChildren = [...NewSortedListOfChildren, ...SortArray];
    });
    return NewSortedListOfChildren;
  }
  catch (e) {
    console.error('Exception in getSortingListOfChildren() of EntityManagement.formtemplate.component in EntityManagementFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e);
  }
}
adminSchemaAccordionContentTypeCommon() {
  if (this.contentGrid.section && this.contentGrid.section.type == 'section') {
    for (let accordion in this.contentGrid.section) {
      var nextAcordianData = {};
      if (accordion != "type") {
        if (this.jsonData != null) {
          this.listOfChildren.push({ "Schema": this.contentGrid.section[accordion], "Data": nextAcordianData, "EntityId": accordion, "EntityName": accordion });
        }
        else {
          this.listOfChildren.push({ "Schema": this.contentGrid.section[accordion], "Data": nextAcordianData, "EntityId": accordion, "EntityName": accordion });
        }
      }
    }
    this.createGroupAccordianData(this.myForm);
  }
  this.contentGrid1.columnRowSpan = this.columnRowSpanCount;
  /**
   *  categoring the header related data
   */
  for (let col of this.headerGrid.grid.cols) {
    if (col.type == 'grid' || col.type == 'table') {
      let arrayCopy = col.grid.cols.slice();
      let filteredGridCols = arrayCopy.filter(d => d.type != 'none');
      filteredGridCols.forEach(obj => {
        this.headerdata.push(this.properties[obj.label].label);
      })

    }
    else if (col.elements.length) {
      let filteredElements = col.elements.filter(d => d.type != 'none');
      filteredElements.forEach(obj => {
        this.headerdata.push(this.properties[obj.label].label);
      })
    }
  }
}
InitializeEntityUtilityHandler() {
  try {
    var UtilHandler = this.getDefaultUtils();
    if (UtilHandler) {
      UtilHandler.initializeEntityHandler();
    }
  } catch (e) {
    this.commonService.appLogException(new Error('Exception in InitializeEntityUtilityHandler() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
  }

}
  /*****
   * CREATE Default Utils to Pass to each Location Util's Instance
   */
  getDefaultUtils() {
    try {
      let entityInstance: any = this.getEntityInstance();
      if (entityInstance) {
        return entityInstance;
      }

    } catch (e) {
      this.commonService.appLogException(new Error('Exception in getDefaultUtils() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }
    /**
   * To Create Instance
   */
  getEntityInstance(): IEntityInterface {
    var entityInstance: any;
    if (this.entityUtil == null) {
      switch (true) {
        case this.EntityType==EntityTypes.EMGlobalAdmin:
          entityInstance = new EMGlobalAdmin(this.myForm, this.formBuilder, this.dialog);
          break;
        default:
          break;
      }
    }
    else {
      entityInstance = this.entityUtil;
    }

    if (entityInstance) {
      this.entityUtil = entityInstance;
      return this.entityUtil;

    }

  }


  ReadShowMoreForHierarchyEntities(type) {
    try {
      this.EntityId = this.commonService.tenantID + "_global";
      let ExistingDataListLength = this.myForm.controls[type]["controls"]["ListsData"].length
      let payload1 = { "EntityType": type, "Fabric": "FDC", "Info": this.EntityName, FormType: this.EntityName, "OldConfigEntityId": ExistingDataListLength };
      let PayLoad = JSON.stringify(payload1);
     // this.commonService.sendMessageToServer(PayLoad, "EMGlobalAdmin", this.EntityName, this.EntityType, MessageKind.READ, Routing.OriginSession, "ShowMore", FABRICS.FDC.toUpperCase());
      this.cdRef.markForCheck();
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in ReadShowMoreForHierarchyEntities() of em-admin-form-generator.componenet.ts at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  clickEvent(event) {
    try {
      switch (event) {
        default:
          this.ReadShowMoreForHierarchyEntities(event);
          break;
      }
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in clickEvent() of em-admin-form-generator.componenet.ts at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }


  compUntilDestroyed():any {
    return takeUntil(this.takeUntilDestroyObservables);
    }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.takeUntilDestroyObservables.next();
this.takeUntilDestroyObservables.complete();

  }
}
