import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { routing } from './em-admin-form-routing.module';
import { EmAdminFormComponent } from './em-admin-form-components/em-admin-form.component';
import { SharedModule1 } from 'ClientApp/app/components/shared/shared1.module';
import { EmAdminFormGeneratorComponent } from './em-admin-form-generator/em-admin-form-generator.component';
import { EMAdminGaurdService } from './em-admin-form-components/em-admin-form-guard.service';


@NgModule({
  declarations: [EmAdminFormComponent, EmAdminFormGeneratorComponent],
  imports: [
    CommonModule,SharedModule1,
    routing
  ],
  providers:[EMAdminGaurdService]
})
export class EmAdminFormModule { }
