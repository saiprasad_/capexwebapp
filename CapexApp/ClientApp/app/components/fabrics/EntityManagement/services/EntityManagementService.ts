//external library imports
import { EventEmitter, Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { TreeModel } from '@circlon/angular-tree-component';
import { AppConsts, BIentitiesarray, FilterEntitiesfor_BI, PEOPLEentitiesarray, peopleEntityTypes } from 'ClientApp/app/components/common/Constants/AppConsts';
import { CONTEXT_MENU_ACTIVE_MESSAGE, DELETE_ENTITY, DELETE_LOCATION_PRODUCTION_HISTORY_EQUIPMENT_WILL_DELETE, DELETE_LOCATION_WITH_LINKED_LOCATIONS, DELETE_PEOPLE_WITH_OFFICES_PERSONS, DELETE_PEOPLE_WITH_PERSONS, EM_MUTUAL_FABRIC_DELETE_MSG, SELECT_CHECK_MARK, TENANT_COMPANY_DELETE_MESSAGE, TRANSFEROFFICEPERSON, TRANSFERPERSON } from 'ClientApp/app/components/common/Constants/AttentionPopupMessage';
//js imports
import { CommonJsMethods } from 'ClientApp/app/components/common/library/CommonJavaScriptMethods';
//components imports
import { PopUpAlertComponent } from 'ClientApp/app/components/common/PopUpAlert/popup-alert.component';
import { StateRecords } from 'ClientApp/app/components/common/state/common.state';
import { PeopleCompanyTypes } from 'ClientApp/app/components/common/state/CompanyTypes/companytype.state.model';
import { CompanyModel, EntityStateConstantsKeys, HierarchyModel, OfficeModel, PersonsModel } from 'ClientApp/app/components/common/state/entity.state.model';
import { EntityCommonStoreQuery } from "ClientApp/app/components/common/state/entity.state.query";
//state imports
import { EntityFilterStoreQuery } from 'ClientApp/app/components/common/state/EntityFilter/entityfilter.query';
//common services
import { CommonService } from 'ClientApp/app/components/globals/CommonService';
import { PopupOperation } from 'ClientApp/app/components/globals/Model/AlertConfig';
//constants imports
import { AngularTreeEventMessageModel, AngularTreeMessageModel, CAPABILITY_NAME, ComponentInstanceEventMessageModel, Datatypes, FABRICS, FabricsNames, MessageKind, TreeDataEventResponseModel, TreeOperations, TreeTypeNames, TypeOfEntity } from 'ClientApp/app/components/globals/Model/CommonModel';
import { EntityTypes, ITreeConfig } from 'ClientApp/app/components/globals/Model/EntityTypes';
import { FabricRouter } from 'ClientApp/app/components/globals/Model/FabricRouter';
import { MessageModel, Routing } from 'ClientApp/app/components/globals/Model/Message';
import { AppConfig } from 'ClientApp/app/components/globals/services/app.config';
import { UserStateConstantsKeys } from 'ClientApp/webWorker/app-workers/commonstore/user/user.state.model';
import { UsersStateQuery } from 'ClientApp/webWorker/app-workers/commonstore/user/user.state.query';
import { WORKER_TOPIC } from 'ClientApp/webWorker/app-workers/shared/worker-topic.constants';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { Observable, of, Subject } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';






@Injectable()
export class EntityManagementService {

  //****observables
  EntityMgmtCommonObservable = new Subject();
  public onTreeEventChanges = new Subject<any>();
  public getEMFormComponentInstance$ = new Subject();
  //****boolean
  NonmeterCreationStatus = false;
  deliveredCreationStatus = false;
  isRightTreeActive = false;
  locationTypeForTreeFilterFlag: boolean;
  isSchemaRead: boolean;
  public EMLocationcontextmenueDeleteOperation: boolean = false;

  //****string and any type
  Operational_Form_RightTree_Data;
  ProdMgmt_RightTree_Data;
  createEntityEnableCapabilityList = [];
  //****objects
  RouteParam: any = {};
  isCurrentSession: boolean = true;
  selectParent: boolean;

  //****arrays
  // BIentitiesarray = ["categories", "dashboards"];
  // PEOPLEentitiesarray = ["Companies", "Groups", "Persons"];
  // EntitmanagementCapabilities = [FABRICS.PIGGING];
  // public CustomerAppsettings = AppConfig.AppMainSettings;
  // EntitiesforFDC_OPF_pig = [TypeOfEntity.well, TypeOfEntity.facility, TypeOfEntity.delivered];
  // filterEntitiesfor_BI = [EntityTypes.Category, EntityTypes.DashBoard];
  // filterEntitiesfor_People = [TypeOfEntity.Company, TypeOfEntity.Group, TypeOfEntity.Person];

  constructor(public entityCommonStoreQuery: EntityCommonStoreQuery, public commonService: CommonService, public usersStateQuery: UsersStateQuery, public stateRecords: StateRecords, public router: Router, public route: ActivatedRoute, public entityFilterStoreQuery: EntityFilterStoreQuery, public dialog: MatDialog, public appConfig: AppConfig) {
    this.commonService.lastOpenedFabric = FabricsNames.ENTITYMANAGEMENT;
    this.workerInit();
    this.CheckDataExist();
    this.initializeTreeWorkersObservables();

    this.commonService.sendDataFromAngularTreeToFabric
      .pipe(filter((message: any) => AppConsts.treeNameForEntityManagement.includes(message.treeName[0]) && this.commonService.getFabricNameByUrl(this.router.url) === FabricsNames.ENTITYMANAGEMENT))
      .subscribe((msg: AngularTreeMessageModel) => {
        this.GetEventFromTree(msg);
      },
        error => {
          console.error('Exception in sendDataFromAngularTreeToFabric observable in EntityManagementService at time ' + new Date().toString() + '. Exception is : ' + error.message);
          this.commonService.appLogException(new Error('Exception in sendDataFromAngularTreeToFabric observable in EntityManagementService at time ' + new Date().toString() + '. Exception is : ' + error.message));
        });

    this.route.queryParams
      .pipe(untilDestroyed(this))
      .subscribe(params => {
        const tempTab = params.physicalItem ? params.physicalItem : params.EMTab ? params.EMTab : params.tab;
        if (Object.keys(this.RouteParam).length <= 0 || (this.RouteParam.tab != tempTab && this.RouteParam.physicalItem != tempTab && this.RouteParam.EMTab != tempTab) || params.leftnav != this.RouteParam.leftnav) {
          if (tempTab) {
            this.RouteParam = JSON.parse(JSON.stringify(params));
            //this.commonService.updateViewedFabric(this.commonService.getFabricNameByTab(params.tab));
            // const tab = params.tab;
            this.ReadTreeData(tempTab);
          }
        }
        this.RouteParam = JSON.parse(JSON.stringify(params));
      });

    this.commonService.sendMessagesToFabricServices.pipe(filter((data: any) => this.debug(data) && this.commonService.lastOpenedFabric == FabricsNames.ENTITYMANAGEMENT)).subscribe(async (message: MessageModel) => {
      this.recieveMessageFromMessagingService(message);
    });

    this.getEMCurrentProductionDay();
  }
  debug(data) {
    let fabric = FABRICS.PEOPLEANDCOMPANIES.toLowerCase() + "_em_person_schedule".toString();
    let msgFabric = data.Fabric.toLowerCase().toString();

    if (msgFabric == fabric) {
      return false;
    }
    return true;
  }
  workerInit(): void {
    this.commonService.worker_WS$.filter(data => this.commonService.lastOpenedFabric == FabricsNames.ENTITYMANAGEMENT && data.EntityID != "OfflineData").subscribe((response) => {
      try {
        this.recieveMessageFromMessagingService(response);
      } catch (e) {
        this.commonService.appLogException(new Error('Exception in workerInit() of EntityManagementService at time ' + new Date().toString() + '. Exception is : ' + e));
      }
    });
  }

  CheckDataExist() {

    // this.commonService.getEntititesFromStore$("TypeOf",
    //   [TypeOfEntity.well],
    //   d => true,
    //   d => { return { "EntityId": d.EntityId, "SG": d.SG } },
    //   FABRICS.OPFFabric)
    //   .pipe(untilDestroyed(this)).first()
    //   .subscribe((entities: any) => {
    //     let data = (<any[]>entities).filter(d => d && d["SG"] && ((<Array<any>>d["SG"]).indexOf(FABRICS.OPFFabric) >= 0));
    //     if (data && data.length == 0) {
    //       this.CheckLeftTreeDatainState("OperationEntities", "OperationalForm", "OperationEntities", FABRICS.OPFFabric, '', TreeTypeNames.OperationalForms, FABRICS.OPFFabric);
    //     }
    //   });
    // this.commonService.getEntititesFromStore$("TypeOf",
    //   [TypeOfEntity.well],
    //   d => true,
    //   d => { return { "EntityId": d.EntityId, "SG": d.SG } },
    //   FABRICS.PIGGING)
    //   .pipe(untilDestroyed(this)).first()
    //   .subscribe((entities: any) => {
    //     let data = (<any[]>entities).filter(d => d && d["SG"] && ((<Array<any>>d["SG"]).indexOf(FABRICS.PIGGING) >= 0));
    //     if (data && data.length == 0) {
    //       this.CheckLeftTreeDatainState("PiggingEntities", "PiggingEntities", "PiggingEntities", FABRICS.PIGGING, '', TreeTypeNames.PIGGING, FABRICS.PIGGING);
    //     }
    //   });

  }

  recieveMessageFromMessagingService(message: MessageModel) {
    switch (message.DataType) {
      case 'SUCCESS':
        switch (message.MessageKind) {
          case MessageKind.CREATE:

            if (message.Fabric.toLowerCase() != FABRICS.PEOPLEANDCOMPANIES.toLowerCase())
              this.addEntityIntoLeftTree(message.EntityID);

            switch (message.Fabric.toLowerCase()) {
              case FABRICS.PEOPLEANDCOMPANIES.toLowerCase():
                {
                  this.AddNewNodeLeftTreePeople(message);
                  this.CreateRoutingForPeople(message);
                }
                break;
              case FABRICS.BUSINESSINTELLIGENCE.toLowerCase():
              case "BI":

                break;
              default:
                break;
            }
            break;

          case MessageKind.UPDATE:
            // switch (message.Fabric.toLowerCase()) {
            //   case FABRICS.OPFFabric.toLowerCase():
            //     message.Fabric = "FDC";
            //     this.commonService.loadingBarAndSnackbarStatus("complete", "Entity Updated");
            //     this.commonService.sendMessagesToFabricServices.next(message);
            //     break;

            //   default:
            //     break;
            // }
            break;

          case MessageKind.DELETE:
            this.commonService.loadingBarAndSnackbarStatus("complete", "Entity Deleted");
            switch (message.Fabric.toLowerCase()) {
              case FABRICS.PEOPLEANDCOMPANIES.toLowerCase():
                this.DeleteAndRecreatePeopleNode(message);
                break;
              default:

                break;
            }
            this.EntityMgmtCommonObservable.next(message);
            break;
          case MessageKind.PARTIALUPDATE:
            switch (message.Fabric.toLowerCase()) {
              case "people":
                case FABRICS.PEOPLEANDCOMPANIES.toLowerCase():
                this.peoplePartialuPdate(message, true);
                break;
              default:
                break;
            }
            break;
          default:
            break;
        }
        break;

      default:
        switch (message.MessageKind) {
          case MessageKind.READ:
            this.commonService.loadingBarAndSnackbarStatus("complete", "");
            switch (message.Fabric.toLowerCase()) {
              case FABRICS.PEOPLEANDCOMPANIES.toLowerCase():
                break;
              case FABRICS.BUSINESSINTELLIGENCE.toLowerCase():
              case "BI":
                break;
            }
            break;
          case MessageKind.CREATE:
            // if(message.DataType.toLocaleLowerCase() == FABRICS.FDC.toLocaleLowerCase())
            // this.recievedMessageHandler(message);
            break;
          case MessageKind.UPDATE:
          case MessageKind.DELETE:
            if (message.DataType == "Configuration") {
              this.recievedMessageHandler(message);
            }
            break;
          default:
            break;
        }
        break;
      case "People"://other session partial update
        {
          switch (message.MessageKind) {
            case MessageKind.PARTIALUPDATE:
              let peopleViewedFabric = FABRICS.ENTITYMANAGEMENT + "_" + FABRICS.PEOPLEANDCOMPANIES;
              let isReceivedMessage = true;
              this.usersStateQuery.select().filter(userState => userState && isReceivedMessage).subscribe(res => {
                if (res && res.viewedFabrics && res.viewedFabrics.length && res.viewedFabrics.indexOf(peopleViewedFabric) != -1) {
                  this.peoplePartialuPdate(message, false);
                }
                isReceivedMessage = false;
              });
              break;
          }
        }
        break;
    }
    setTimeout(() => {
      this.commonService.loadingBarAndSnackbarStatus("", "");
    }, 3000);
  }
  CreateRoutingForFDC(message) {
    let payload = JSON.parse(message.Payload);
    if (message.Routing == "OriginSession" && payload.Type == "CopyToNewEntityCreated" ) {
      let payloaddata = JSON.parse(payload.EntityInfoJson);
      //payloaddata.LocationId = message.EntityID=payloaddata.EntityId;

      setTimeout(() => {
        this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC, 'Settings', payloaddata.EntityId, this.commonService.treeIndexLabel, this.commonService.treeIndexLabel], { queryParams: { 'EntityID': payloaddata.EntityId, 'expand': false, 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable, EMTab: this.commonService.treeIndexLabel } });
      }, 0);
    }


  }
  recievedMessageHandler(message: MessageModel) {
    try {

      switch (message.Payload) {
        case "SUCCESS":
          this.commonService.errorDetails = message.Payload;
          break;
        default:
          switch (message.DataType) {
            case "DBError":
              if (message.MessageKind == MessageKind.UPDATE) {
                var Data = JSON.parse(message["Payload"]);
                this.commonService.errorDetails = Data["ExceptionMessage"];
              } else {
                this.commonService.errorDetails = message.Payload;
              }
              this.commonService.DBFailErrorHandling$.next(message);
              break;
            case "Error":
              this.commonService.errorDetails = message.Payload;
              console.error(message);
              break;
            default: break;
          }
          // case FABRICS.FDC.toUpperCase():
          //       case MessageKind.CREATE:
          //         if (message.Payload != MessageKind.CREATE) { }
          //         this.isCurrentSession = false;
          //         var parentId = JSON.parse(JSON.parse(message.Payload)["EntityInfoJson"]).FieldId
          //         if (parentId != null)
          //           this.selectParent = true;
          //         else
          //           this.selectParent = false;
          //         let fabric = this.commonService.getFabricNameByUrl(this.router.url);
          //         if (fabric.toLowerCase() == FabricsNames.ENTITYMANAGEMENT.toLowerCase() && message.Fabric.toLowerCase() != FABRICS.PEOPLEANDCOMPANIES.toLowerCase())
          //           this.addEntityIntoLeftTree(message.EntityID);
          //           this.EntityMgmtCommonObservable.next(message);
          //         break;

          // default:
          //       let fabricName = this.commonService.getFabricNameByUrl(this.router.url);
          //       if (fabricName == FabricsNames.ENTITYMANAGEMENT) {
          //         fabricName = this.route.queryParams['_value']['tab'];
          //       }
          //       if (message.MessageKind == MessageKind.DELETE && message.DataType == "Configuration" && message.EntityType != EntityTypes.FFV) {
          //         //state delete entities for other session
          //         switch (fabricName) {
          //           case FABRICS.PIGGING:
          //            this.sendDataToAngularTree([TreeTypeNames.PIGGING], TreeOperations.deleteNodeById, message.EntityID);
          //             break;
          //           case "Operational Forms":
          //            this.sendDataToAngularTree([TreeTypeNames.OperationalForms], TreeOperations.deleteNodeById, message.EntityID);
          //             break;
          //           default:
          //             this.removeEntityFromLeftTree(TreeTypeNames.PHYSICALFLOW, TreeOperations.deleteNodeById, message.EntityID, message.EntityID);
          //             break;
          //         }
          //         if (AppConsts.Locations.includes(message.EntityType)) {
          //           this.closeLocationForm(message.EntityID);
          //         }
          //       }

          //      //update operation in left side tree in configuration updation time
          //       if (message.MessageKind == MessageKind.UPDATE && message.DataType == "Configuration" ) {
          //         if (message.Payload != "" && message.Payload != null && message.Payload != undefined) {
          //           var ValueChangedFields;
          //           var ConfigPayload = JSON.parse(message.Payload);
          //           var infoJson = ConfigPayload.EntityInfoJson ? JSON.parse(ConfigPayload.EntityInfoJson) : ConfigPayload;
          //           ValueChangedFields = (ConfigPayload.ValueChangedFields && ConfigPayload.ValueChangedFields != null && ConfigPayload.ValueChangedFields != "") ? JSON.parse(ConfigPayload.ValueChangedFields) : (infoJson.ValueChangedFields && infoJson.ValueChangedFields != null && infoJson.ValueChangedFields != "") ? JSON.parse(infoJson.ValueChangedFields) : { 'EntityName': infoJson.EntityName };
          //           this.updateTreeName(infoJson, message, ValueChangedFields);
          //           if (infoJson.NestedUnderName != null && infoJson.NestedUnderName != "" && infoJson.NestedUnderId != null && infoJson.NestedUnderId != "") {
          //             ValueChangedFields = {
          //               'NestedUnderName': infoJson.NestedUnderName,
          //               'NestedUnderId': infoJson.NestedUnderId,
          //               'NestedUnderType': infoJson.NestedUnderType
          //             };
          //             this.updateTreeName(infoJson, false, ValueChangedFields);
          //           }
          //         }
          //       }


          //       break;
          //   }

          break;
      }
    } catch (e) {
      console.error('Exception in sendMessagesToFabricServices subscriber of EM Service in  EM/service at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in sendMessagesToFabricServices subscriber of EMService in EM/service at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  CreateRoutingForPigging(message) {
    let entityInfo = JSON.parse(message.Payload);
  
  }
  CreateRoutingForPeople(message) {
    let entityInfo = JSON.parse(message.Payload);
    let emtab = EntityTypes.PERSON;
    let queryParameterTab = message.EntityType == EntityTypes.PERSON ? emtab : this.commonService.treeIndexLabel;
    if (entityInfo.Type != "Drag" && entityInfo.Type != "AddNewItemToManfacturer")
      this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC, 'Settings', message.EntityID, this.commonService.treeIndexLabel, queryParameterTab], { queryParams: { 'EntityID': message.EntityID, expand: false, 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable, EMTab: emtab } });
  }
  addEntityIntoLeftTree(EntityID) {
    this.entityCommonStoreQuery.selectEntity(EntityID).first().filter((action) => action != undefined).subscribe((Entity: any) => {
      //for(var index in action ){
      //let Entity:any=this.entityCommonStoreQuery.getEntity(action[index])
      let InsertAtPosition = "";
      let treedata = this.getTreePayload(Entity);
      if (peopleEntityTypes.includes(Entity.EntityType)) {
        treedata = JSON.parse(JSON.stringify(Entity));
        if (Entity.Parent)
          InsertAtPosition = Entity.Parent[0].EntityId;
        else
          InsertAtPosition = "althingTreeRoot";
      }

      if (InsertAtPosition != "") {
        var SGarray;
        if (InsertAtPosition != "althingTreeRoot" && InsertAtPosition != "HaulLocation") {
          var data = this.commonService.getEntitiesById(InsertAtPosition);
          SGarray = data.SG;
        }

        let treePayload: any = { "InsertAtPosition": InsertAtPosition, "NodeData": treedata };
        Entity.SG.forEach(element => {
        
          if (element == FABRICS.PEOPLEANDCOMPANIES)
            this.sendDataToAngularTree([TreeTypeNames.PeopleAndCompanies], TreeOperations.AddNewNode, treePayload);
        });
      }


    });

  }
  initializeTreeWorkersObservables() {
    try {
      this.commonService.initializeTreeWorkersObservablesPeople();
      // this.commonService.initializeTreeWorkersObservablesPigging();
      // this.commonService.initializeTreeWorkersObservablesPRODMGMT();
      // this.commonService.initializeTreeWorkersObservablesOPFORMS();
      this.commonService.initializeTreeWorkersObservablesBI();
      this.commonService.initializeTreeWorkersObservablesConstruction();
    } catch (e) {
      console.error(e);
    }

  }

  initializeCommonFields(entityData, properties, bindhierarchy?: boolean, entityName?) {
    entityData.TenantName = this.commonService.tenantName;
    entityData.TenantId = this.commonService.tenantID;
    entityData.ModifiedBy = this.commonService.CurrentUserEntityName;
    entityData.ModifiedDateTime = new Date().toISOString().slice(0, 19).replace("T", " ");
    if (!entityData.CreatedDateTime)
      entityData.CreatedDateTime = entityData.ModifiedDateTime;
    if (!entityData.CreatedBy) {
      entityData.CreatedBy = this.commonService.currentUserName;
    }
    let Data = JSON.parse(JSON.stringify(entityData));
    return Data;
  }

  getCalenderProductionDate() {
    let productionDay = new Date();
    productionDay.setHours(8, 0, 0, 0);
    return productionDay;
  }
  AddOrUpdateHierarchyEntitiesInState(payload) {
    try {
      let statePayload: HierarchyModel = null;
      statePayload["Capabilities"] = this.commonService.getCapabilityIdsBy(statePayload['SG']); this.entityCommonStoreQuery.upsert(payload.EntityId, statePayload);
      this.entityCommonStoreQuery.upsert(payload.EntityId, statePayload);
    }
    catch (e) {
      console.error("AddOrUpdateEntitiesAndSetAsDefaultState " + e);
    }
  }


  DeleteHierarchyEntitiesInState(message) {
    try {

    
    } catch (e) {
      this.commonService.appLogException(new Error('Exception in DeleteHierarchyEntitiesInState() of EntityManagementService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }





  CheckLeftTreeDatainState(DataType, EntityID, EntityType, Fabric, EntityArray?, capabality?, stateStatusFabric?) {
    var stateLastModifiedUTCMS = this.usersStateQuery.getStateKey(UserStateConstantsKeys.modifiedTime);
    var isdateTime = false;
    var currentTimeUTCMS = Math.floor((new Date()).getTime() / 1000);

    if (stateLastModifiedUTCMS) {
      var windowCloseUTCMS = this.usersStateQuery.getStateKey(UserStateConstantsKeys.windowCloseTime);

      if (windowCloseUTCMS && windowCloseUTCMS > stateLastModifiedUTCMS) {//resetting store
        var closeAndModifiedTimeDifference = currentTimeUTCMS - windowCloseUTCMS;
        if (closeAndModifiedTimeDifference > 6000) {
          //clear all store except users store
          this.stateRecords.reSetStore();
          this.usersStateQuery.add(UserStateConstantsKeys.viewedFabrics, []);// Reset Viewed Fabrics
        }
        else {
          //based on datetime fetch latest entities
          isdateTime = true;
        }
      }
      else {
        //based on datetime fetch latest entities
        isdateTime = true;
      }
    }
    else {
      isdateTime = true;
    }
    let isEntitiesExist = this.commonService.getViewedFabricsAndCheckStatus(FABRICS.ENTITYMANAGEMENT + "_" + stateStatusFabric);
    // switch (DataType) {
    //   // case FABRICS.BUSINESSINTELLIGENCE:
    //   //   isEntitiesExist = this.commonService.getViewedFabricsAndCheckStatus(stateStatusFabric);
    //   //   break;
    //   // case EntityTypes.FDC:
    //   //   isEntitiesExist = this.commonService.getViewedFabricsAndCheckStatus(stateStatusFabric);
    //   //   break;
    //   // case "OperationEntities":
    //   // case "PiggingEntities":
    //   //   isEntitiesExist = this.commonService.getViewedFabricsAndCheckStatus(stateStatusFabric);
    //   //   break;
    //   case EntityTypes.People:
    //   isEntitiesExist = this.commonService.checkStateStatus(FABRICS.PEOPLEANDCOMPANIES);
    //   break;
    // }

    if (!isEntitiesExist) { //we need toimplement logic reading entity based on last read
      if (DataType == EntityTypes.People) {
        this.lefttreeforBIPeopleConstruction(DataType, EntityArray, capabality);
        this.readCompanyTypes();
      }
      else if (DataType == FABRICS.BUSINESSINTELLIGENCE || DataType == Datatypes.CONSTRUCTION) {
        this.lefttreeforBIPeopleConstruction(DataType, EntityArray, capabality, null, EntityType, EntityID);
      }
      else {
        this.Readcapabilitytreedata(DataType, EntityID, EntityType, Fabric);
      }
    }
    else if (isdateTime) {
      this.dateTimeBasedReadTreeBackend(currentTimeUTCMS, DataType, EntityID, EntityType, Fabric, EntityArray, capabality);
      if (DataType == EntityTypes.People)
        this.readCompanyTypes();
    }
  }

  readCompanyTypes() {

  }

  dateTimeBasedReadTreeBackend(currentTimeUTCMS: number, DataType, EntityID, EntityType, Fabric, EntityArray?, capabality?) {
    try {
      let isSubscribe = true;
      let fabricName = "";

      if (capabality)
        fabricName = capabality;
      else if (Fabric)
        fabricName = Fabric;
      this.entityCommonStoreQuery
        .selectAll({
          filterBy: (entity: any) => entity && entity.SG && (<Array<any>>entity.SG).indexOf(fabricName) >= 0
        })
        .filter(data => data.length != 0 && isSubscribe)
        .map(data => JSON.parse(JSON.stringify(data)))
        .shareReplay(1).first().subscribe(res => {
          var date = this.commonService.maxModifiedDateTimeStateObject(res);

          var last_utc = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
            date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()) / 1000;

          if (last_utc != NaN && currentTimeUTCMS - last_utc > 60) {
            var dateTime = new Date(last_utc * 1000).toISOString().slice(0, 19).replace('T', ' ');
            if (capabality)
              this.lefttreeforBIPeopleConstruction(DataType, EntityArray, capabality, dateTime, EntityType, EntityID);
            else
              this.Readcapabilitytreedata(DataType, EntityID, EntityType, Fabric, dateTime);
          }
          isSubscribe = false;
        });
    }
    catch (e) {
      this.commonService.appLogException(new Error("EntityManagementService dateTimeBasedReadTreeBackend: " + e));
    }
  }

  ReadTreeData(treeName) {
    switch (treeName) {
      case TreeTypeNames.BusinessIntelligence:
        this.CheckLeftTreeDatainState(FABRICS.BUSINESSINTELLIGENCE, "", "", "", BIentitiesarray, FABRICS.BUSINESSINTELLIGENCE, FABRICS.BUSINESSINTELLIGENCE);
        break;
      case TreeTypeNames.PeopleAndCompanies:
        this.CheckLeftTreeDatainState(EntityTypes.People, "", "", "", PEOPLEentitiesarray, FABRICS.PEOPLEANDCOMPANIES, FABRICS.PEOPLEANDCOMPANIES);
        break;

      default:
        switch (treeName) {
          case TreeTypeNames.TaggedItems:
          case TreeTypeNames.PhysicalInstances:
          case TreeTypeNames.Materials:
          case TreeTypeNames.Documents:
            this.CheckLeftTreeDatainState(Datatypes.CONSTRUCTION, treeName.replace(/ /g, '') + 'Entities', FABRICS.CONSTRUCTION, '', '', FABRICS.CONSTRUCTION, treeName);
            break;

          case TreeTypeNames.Construction:
          case TreeTypeNames.Schedule:
            this.CheckLeftTreeDatainState(Datatypes.CONSTRUCTION, TreeTypeNames.Construction + 'Entities', FABRICS.CONSTRUCTION, '', '', FABRICS.CONSTRUCTION, TreeTypeNames.Construction);
            this.commonService.physicalInstanes$.next({ treeName: TreeTypeNames.Construction, type: TreeTypeNames.Construction });
            break;
        }
        break;
    }
  }

  Readcapabilitytreedata(DataType, EntityID, EntityType, Fabric, dateTime?) {
    var capabality;
    let d = new Date,
      dformat = [d.getFullYear(), d.getMonth() + 1, d.getDate()].join('-') + ' ' + [d.getHours(), d.getMinutes(), d.getSeconds()].join(':');

    if (this.commonService.capabilityFabricId == null || this.commonService.capabilityFabricId == undefined) {
      this.commonService.capabilityFabricId = this.commonService.FabricCapabilityIdAtLoginTime;
    }

    capabality = Fabric;

    let payload = { "EntityType": EntityType, "DataType": DataType, "Fabric": Fabric, "Info": "Admin", "CurrentDate": dformat, "CapabilityId": this.commonService.getCapabilityId(capabality) };

    if (dateTime) {
      payload["ModifiedDateTime"] = dateTime;
    }

    var PayLoad = JSON.stringify(payload);
    let message = this.commonService.getMessage();
    message.Payload = PayLoad;
    message.DataType = DataType;
    message.EntityID = EntityID;
    message.EntityType = payload.EntityType;
    message.MessageKind = MessageKind.READ;
    message.Routing = Routing.OriginSession;
    message.CapabilityId = this.commonService.getCapabilityId(capabality);
    message.Type = "Details";
    var PayLoad = JSON.stringify(payload);
    let OperationalStatusmessage = this.commonService.getMessage();
    OperationalStatusmessage.CapabilityId = this.commonService.getCapabilityId(this.commonService.lastOpenedFabric);
    OperationalStatusmessage.Payload = JSON.stringify({});
    OperationalStatusmessage.DataType = "OperationalStatus";
    OperationalStatusmessage.EntityID = "OperationalStatus";
    OperationalStatusmessage.EntityType = "OperationalStatus";
    OperationalStatusmessage.MessageKind = MessageKind.READ;
    OperationalStatusmessage.Routing = Routing.OriginSession;
    OperationalStatusmessage.Type = "Details";
    this.commonService.statusCheck(message);
    message.Fabric = Fabric;
    Observable.combineLatest([
      this.commonService.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message),
      this.commonService.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, OperationalStatusmessage)
    ]).filter(data => message.CapabilityId).map(([treeData, OperationalStatus]) => {
      var messageData = JSON.parse(treeData);
      var OperationalStatusmessageData = JSON.parse(OperationalStatus);

      // Desktop Initial Sync Request
      // if (this.commonService.isElectron && this.commonService.onlineOffline && !this.commonService.desktopInitialSyncStatus) {
      //   this.commonAppService.DesktopDataReadForOffline();
      //   this.commonService.desktopInitialSyncStatus = true;
      // }

      let treeEntity = JSON.parse(messageData["Payload"]);
      let Removedentities;
      if (treeEntity.hasOwnProperty("Addedentities") && treeEntity.hasOwnProperty("Removedentities")) {
        Removedentities = JSON.parse(treeEntity.Removedentities);
        treeEntity = JSON.parse(treeEntity.Addedentities);
        this.updateRemovedentityinState(Removedentities, capabality);
      }

      this.commonService.setTypeOfField(treeEntity);
      let OperatedStatusData = JSON.parse(OperationalStatusmessageData["Payload"]);
      let data = [treeEntity, OperatedStatusData];
      return data;
    })
      .subscribe(([treeData, OperationalStatus]) => {
        this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, '');
        this.commonService.updateTreeDataBasedOnOperationalStatus(treeData, OperationalStatus);
        this.formatinglefttree(treeData, capabality);
      });
  }

  formatinglefttree(data, capabality) {
    var entities = [...data.areas,
    ...data.district,
    ...data.facilities,
    ...data.fields,
    ...data.headers,
    ...data.equipments,
    ...data.wells,
    ...data.facilitycode
    ];
    let existingEntities = [];
    let newEntities = [];
    for (var index in entities) {
      if (entities[index]) {
        let entityId = entities[index].EntityId;
        let entityData = this.entityCommonStoreQuery.getEntity(entityId);
        if (entityData) {
          // if (data["ModifiedDateTime"]) {
          //   this.updateObjectProperties(entityId, entityData, entities[index], TreeOperations.updateNodeById);
          // }
          // if fdc not exisit in SG then add into existing entities list.
          if (entityData["SG"] != undefined) {
            if (!((<Array<any>>entityData["SG"]).indexOf(capabality) >= 0)) {
              existingEntities.push(entityId);
            }
            continue;
          }
        }
        else {
          //add new node
          entities[index]["SG"] = [capabality];
          //commented to test
          // if (data["ModifiedDateTime"]) {
          //   this.updateObjectProperties(entityId, entityData, entities[index], TreeOperations.AddNewNode);
          // }
          // else {
          //   newEntities.push(entities[index]);
          // }
          newEntities.push(entities[index]);
        }
      }
    }
    if (existingEntities.length > 0)
      this.entityCommonStoreQuery.UpdateArrayValue(existingEntities, "SG", capabality);

    if (newEntities.length > 0)
      this.entityCommonStoreQuery.AddAll(newEntities);

    this.commonService.updateViewedFabric(FABRICS.ENTITYMANAGEMENT + "_" + capabality);
  }

  lefttreeforBIPeopleConstruction(EntityTypeORDataTypeORFabric, EntityArray, capabality, dateTime?, entityType?, entityId?) {
    if (this.commonService.capabilityFabricId == null || this.commonService.capabilityFabricId == undefined) {
      this.commonService.capabilityFabricId = this.commonService.FabricCapabilityIdAtLoginTime;
    }
    let Payload;
    if (capabality == FABRICS.BUSINESSINTELLIGENCE) {
      Payload = { "EntityType": EntityTypeORDataTypeORFabric, "DataType": EntityTypeORDataTypeORFabric, "Fabric": EntityTypeORDataTypeORFabric, "Info": "General", "CapabilityId": this.commonService.getCapabilityId(capabality) };
    }
    else if (entityId == EntityTypes.Construction + 'Entities') {
      Payload = {
        depth: 5,
        isCapitalResult: true,
        entityTypes: [EntityTypes.Project, EntityTypes.Task],
        rightMenus: [{ EntityName: this.commonService.getCapabilityNamebasedontabforSecurity(), EntityId: this.commonService.getCapabilityidbasedontabforSecurity() }]
      };
    }
    else {
      Payload = {};
    }

    if (dateTime && Object.keys(Payload).length == 0) {
      Payload = {};
      Payload["ModifiedDateTime"] = dateTime;
    }
    else if (dateTime) {
      Payload["ModifiedDateTime"] = dateTime;
    }

    let message = this.commonService.getMessage();
    message.Payload = JSON.stringify(Payload);
    message.DataType = EntityTypeORDataTypeORFabric;
    message.EntityID = entityId ? entityId : EntityTypeORDataTypeORFabric;
    message.EntityType = EntityTypeORDataTypeORFabric == Datatypes.CONSTRUCTION ? entityType : EntityTypeORDataTypeORFabric;
    message.MessageKind = MessageKind.READ;
    message.Routing = Routing.OriginSession;
    message.CapabilityId = this.commonService.getCapabilityId(capabality);
    message.Type = "Details";
    //this.commonService.statusCheck(message);
    message.Fabric = EntityTypeORDataTypeORFabric;

    this.commonService
      .httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .map(res => {
        var messageData = JSON.parse(res);
        let data = JSON.parse(messageData["Payload"]);
        return data;
      })
      .subscribe(data => {
        this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, '');
        if (EntityTypeORDataTypeORFabric == Datatypes.CONSTRUCTION) {
          if (entityId == EntityTypes.Construction + 'Entities') {
            this.onEntitiesTreeDataReceive(data);
          }
          else {
            this.filterForPhysicalItems(entityId, data);
          }
        }
        else {
          this.filterforBIPeople(data, EntityArray, capabality);
        }
      });
  }

  onEntitiesTreeDataReceive(entitiesData: Array<any>) {
    let nodes: any[] = [];
    entitiesData.forEach(data => {
      let taskData = JSON.parse(JSON.stringify(data));
      if (taskData['tasks'].length > 0) {
        let entities = taskData['tasks'].find(obj => obj.EntityName == EntityTypes.Construction);
        if (entities) {
          nodes.push(entities['children']);

          // this.sendDataToAngularTree([TreeTypeNames.Construction], TreeOperations.createFullTree, entities['children']);
          // this.expandTree(TreeTypeNames.Construction);
        }
      }
    });

    if (nodes.length > 0) {
      let flattenNodes: any[] = [];
      for (const tempnodes of nodes) {
        this.createFlattenEntitiesList(tempnodes, flattenNodes);
      }
      this.filterForConstruction(flattenNodes);
    }
  }

  createFlattenEntitiesList(nodes: any[], flattenNodes: any[]) {
    const tempNodes: any[] = JSON.parse(JSON.stringify(nodes));
    for (const node of tempNodes) {
      if (node.hasOwnProperty('children')) {
        const tempNode: any = JSON.parse(JSON.stringify(node));

        const children: any[] = tempNode.children;
        delete tempNode.children;
        flattenNodes.push(tempNode);
        this.createFlattenEntitiesList(children, flattenNodes);
      }
      else {
        flattenNodes.push(node);
      }
    }
  }

  expandTree(treeNameForExpanding) {
    var configdata: ITreeConfig = {
      "treeActiveOnClick": true,
      "treeExpandAll": true
    };
    this.sendDataToAngularTree(treeNameForExpanding, TreeOperations.treeConfig, configdata);
  }

  AddorUpdateEntityState(entityId, entityData, fabric: string) {
    try {
      entityData['SG'] = [fabric];
      entityData["ModifiedDateTime"] = new Date().toISOString();
      this.entityCommonStoreQuery.upsert(entityId, entityData);
    }
    catch (e) {
      console.error("AddorUpdateEntitiyState" + e);
    }
  }

  filterForPhysicalItems(entityId, newEntities) {
    this.createTaggedItemsLeftTree(entityId, newEntities);
  }

  createTaggedItemsLeftTree(entitiesName, newEntities, existingEntities?) {
    // if (newEntities && newEntities.length > 0) {
    // switch (entitiesName) {
    //   case EntityTypes.TaggedItems + 'Entities':
    //     this.commonService.sendDataToAngularTree1(TreeTypeNames.Construction, 'createTree', newEntities);
    //     break;
    // }
    newEntities = newEntities ? newEntities : [];
    // this.expandTree(TreeTypeNames.Construction);
    if (newEntities.length > 0) {
      this.filterForConstruction(newEntities);
      this.AddorUpdateEntityState(newEntities[0].EntityId, newEntities[0], FABRICS.CONSTRUCTION);
    }
    this.commonService.physicalInstanes$.next({ treeName: TreeTypeNames.Construction, data: newEntities, type: TreeTypeNames.TaggedItems });
    // }
  }

  filterForConstruction(data) {
    const capabality: string = FABRICS.CONSTRUCTION;
    var entities = JSON.parse(JSON.stringify(data));

    let existingEntities = [];
    let newEntities = [];
    for (var index in entities) {
      if (entities[index]) {
        let entityId = entities[index].EntityId;
        let entityData = this.entityCommonStoreQuery.getEntity(entityId);
        if (entityData) {
          // if (data["ModifiedDateTime"]) {
          //   this.updateObjectProperties(entityId, entityData, entities[index], TreeOperations.updateNodeById);
          // }
          // if fdc not exisit in SG then add into existing entities list.
          if (entityData["SG"] != undefined) {
            if (!((<Array<any>>entityData["SG"]).indexOf(capabality) >= 0)) {
              existingEntities.push(entityId);
            }
            continue;
          }
        }
        else {
          //add new node
          entities[index]["SG"] = [capabality];
          //commented to test
          // if (data["ModifiedDateTime"]) {
          //   this.updateObjectProperties(entityId, entityData, entities[index], TreeOperations.AddNewNode);
          // }
          // else {
          //   newEntities.push(entities[index]);
          // }
          newEntities.push(entities[index]);
        }
      }
    }

    if (existingEntities.length > 0)
      this.entityCommonStoreQuery.UpdateArrayValue(existingEntities, "SG", capabality);

    if (newEntities.length > 0)
      this.entityCommonStoreQuery.AddAll(newEntities);

    this.commonService.updateViewedFabric(FABRICS.ENTITYMANAGEMENT + "_" + capabality);
  }

  filterforBIPeople(data, array, capabality) {
    var Entitiesdata = [];
    for (const key in data) {
      if (data.hasOwnProperty(key) && Array.isArray(data[key])) {
        Entitiesdata = [...data[key], ...Entitiesdata];
      }
    }
    var entities = Entitiesdata;
    let existingEntities = [];
    let newEntities = [];
    let parentEntities = [];
    for (var index in entities) {
      if (entities[index]) {
        if (entities[index].CompanyType && entities[index].CompanyType != "" && entities[index].CompanyType.length > 0) {
          entities[index].CompanyType = entities[index].CompanyType[0];//local dev use
        }

        let entityId = entities[index].EntityId;
        let entityData = this.entityCommonStoreQuery.getEntity(entityId);
        if (entityData) {
          // if BI not exist in SG then add into existing entities list.
          if (entityData["SG"] != undefined) {
            if (!((<Array<any>>entityData["SG"]).indexOf(capabality) >= 0)) {
              existingEntities.push(entityId);
            }
            continue;
          }

          if (capabality == FABRICS.PEOPLEANDCOMPANIES) {
            if ((entityData["Parent"] == undefined || entityData["Parent"] == "") && entities[index]["Parent"]) {
              parentEntities.push(entities[index]);
            }
            // if People not exisit in SG then add into existing entities list.
            if (entityData["SG"] == undefined) {
              existingEntities.push(entityId);
              continue;
            }
          }
        }

        if (capabality == FABRICS.PEOPLEANDCOMPANIES) {
          entities[index]["SG"] = [capabality];
          newEntities.push(entities[index]);
        }
        else {
          entities[index]["SG"] = [capabality];
          entities[index]["TypeOf"] = entities[index]["EntityType"];
          if (entities[index]["EntityType"] == "DashBoard") {
            entities[index]["Dashboardurl"] = entities[index]["DashBoardURL"];
            delete entities[index]["DashBoardURL"];
          }
          newEntities.push(entities[index]);
        }
      }
    }

    if (existingEntities.length > 0)
      this.entityCommonStoreQuery.UpdateArrayValue(existingEntities, "SG", capabality);

    if (newEntities.length > 0)
      this.entityCommonStoreQuery.AddAll(newEntities);

    if (parentEntities.length > 0)
      this.entityCommonStoreQuery.UpdateProperties("Parent", parentEntities);

    this.commonService.updateViewedFabric(FABRICS.ENTITYMANAGEMENT + "_" + capabality);
  }

  sendDataToAngularTree(treeName: string[], treeOperationType: string, payload: any, treeConfig?) {
    var treeMessage: AngularTreeMessageModel = {
      fabric: FABRICS.ENTITYMANAGEMENT.toLocaleUpperCase(),
      treeName: treeName,
      treeOperationType: treeOperationType,
      treePayload: payload,
      treeConfig: treeConfig
    };
    this.commonService.sendDataToAngularTree.next(treeMessage);
  }
  GetEventFromTree(data: AngularTreeMessageModel) {
    try {
      let treeName = data.treeName[0];
      if (data.treePayload.nodeData) {
        data.treePayload.data = data.treePayload.nodeData.data;
      }
      if (data.treePayload.data && data.treePayload.data.EntityId == "HaulLocation")
        return;
      switch (treeName) {


        case TreeTypeNames.Construction:
          switch (data.treeOperationType) {
            case TreeOperations.FullTreeOnCreation:
              this.TreeOnFirstTimeLoad(treeName, data.treePayload);
              break;

            default:
              this.constructionTreeOperations(data);
              break;
          }
          break;

        case TreeTypeNames.BusinessIntelligence:
          {
            switch (data.treeOperationType) {
              case TreeOperations.FullTreeOnCreation:
                this.TreeOnFirstTimeLoad(treeName, data.treePayload);
                break;
              case TreeOperations.NodeOnClick: {
                //    this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC, 'entityMangementForm'], { queryParams: { 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
                this.click(data);
                break;
              }
              case TreeOperations.filteroptionjson: {
                this.filteredjsonstructureBI(treeName);
                break;
              }
              case TreeOperations.selectedfilteroption: {
                this.filterTreeDataBI(data);
                break;
              }
              case TreeOperations.RemoveFilter:
                this.RemoveFilterFromTree(data);
                break;
              case TreeOperations.entityFilter:
                this.entityFilterBI(data);
                break;
            }
          }
          break;

        case TreeTypeNames.PeopleAndCompanies:
          {
            switch (data.treeOperationType) {
              case TreeOperations.FullTreeOnCreation:
                this.TreeOnFirstTimeLoad(treeName, data.treePayload);
                break;
              case TreeOperations.NodeOnClick: {
                this.click(data);
                break;
              }
              case TreeOperations.filteroptionjson: {
                this.filteredjsonstructurePEOPLE(treeName);
                break;
              }
              case TreeOperations.selectedfilteroption: {
                this.filterTreeDataPEOPLE(data);
                break;
              }
              case TreeOperations.RemoveFilter:
                this.RemoveFilterFromTree(data);
                break;
              case TreeOperations.entityFilter:
                this.entityFilterPEOPLE(data);
                break;
              case TreeOperations.NodeOnContextClickBeforeInit: {
                if (this.commonService.CustomerAppsettings.env.UseAsDesktop == true && this.commonService.onlineOffline == false) {
                  this.openCommonAttentionPopup(new Array(CONTEXT_MENU_ACTIVE_MESSAGE));
                } else {
                  this.generateContextMenuJson_for_People(data.treePayload.data);
                }
                break;
              }
              case TreeOperations.NodeContextOptionClicked:
                this.contextClickEvent_for_People(data.treePayload, treeName);
                break;
            }
          }
          break;
        case TreeTypeNames.PeopleAndCompanies_Right:
          switch (data.treeOperationType) {
            case TreeOperations.createRightTree:
              this.generateRightTreeBasedOnStaticEntityTypes([EntityTypes.OFFICE, EntityTypes.PERSON], data, TreeTypeNames.PeopleAndCompanies);
              break;
            case TreeOperations.FullTreeOnCreation:
              {
                var configdata: ITreeConfig = {
                  "treeActiveOnClick": true,
                  "treeExpandAll": true
                };
                this.sendDataToAngularTree([treeName], TreeOperations.treeConfig, configdata);
                break;
              }

            case TreeOperations.entityFilter:
              this.entityFilterForRightTree(data, null, peopleEntityTypes);
              break;
          }
          break;
      }
    }
    catch (e) {
      console.error('Exception in GetEventFromTree() of EntityManagementService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in GetEventFromTree() of EntityManagementService in EntityManagementService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  constructionTreeOperations(data: any) {
    const nodeData: any = data?.treePayload?.data;
    if (nodeData) {
      switch (nodeData.EntityType) {
        case EntityTypes.TaggedItems:
          this.taggedItemsTreeOperations(data);
          break;
      }
    }
  }

  taggedItemsTreeOperations(data: any) {
    switch (data.treeOperationType) {
      case TreeOperations.NodeOnClick:
        console.log(data);
        this.click(data);
        break;
    }
  }



  updateRemovedentityinState(data, capabality) {
    try {
      var entities = [...data.Removedlocations,
      ...data.equipmentsToRemove,
      ];

      for (var index in entities) {
        if (entities[index]) {
          let entityId = entities[index].EntityId;
          let entityData = this.commonService.getEntitiesById(entityId);
          if (entityData) {
            this.commonService.removeEntitiesByIds(capabality, [entityId]);
          }
        }
      }

      this.commonService.updateViewedFabric(FABRICS.ENTITYMANAGEMENT + "_" + capabality);
    } catch (e) {
      console.error("updateRemovedentityinState " + e);
    }
  }


  openCommonAttentionPopup(messageToshow): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: false,
        content: [messageToshow],
        subContent: [],
        operation: PopupOperation.Attention
      };
      let matConfig = new MatDialogConfig();
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      if (!this.dialog.getDialogById(matConfig.id)) {
        this.dialog.open(PopUpAlertComponent, matConfig);
      }
    }
    catch (ex) {
    }
  }

  updateRightTreeBasedOnEntityType() {
    var PupmJson = { EntityName: "Pump", EntityId: "Pump", EntityType: "Pump", children: [] };
    if (this.locationTypeForTreeFilterFlag) {
      let data = { "InsertAtPosition": "Miscellaneous", "NodeData": PupmJson };
      this.sendDataToAngularTree(["Production Management_RIGHT"], TreeOperations.AddNewNode, data);
    }
    else {
      this.sendDataToAngularTree(["Production Management_RIGHT"], TreeOperations.deleteNodeById, "Pump");

    }
  }


  generateContextMenuJson_for_People(data) {
    try {
      let list = [];
      // switch (typeOfEntity) {
      //   case TypeOfEntity.Company:
      //   case TypeOfEntity.Group:
      //     let option = { "displayName": "Create Entity", "children": [] }
      //     list.push(option);
      //     break;
      // }
      let commonOption = { "displayName": "Delete Entity", "children": [] };
      list.push(commonOption);
      let payload = {
        "fabric": FABRICS.ENTITYMANAGEMENT,
        "dataType": "treeFabricConfigData",
        "tab": this.commonService.treeIndexLabel,
        "Payload": {
          "contextMenuOption": [
            {
              "displayName": "People Context Menu",
              "children": list
            }
          ]
        }
      };
      this.sendDataToAngularTree([this.commonService.treeIndexLabel], TreeOperations.treeContextMenuOption, payload.Payload.contextMenuOption);
      this.commonService.activeContextMenuNodeId = data.EntityId;
    }
    catch (e) {
      console.error('Exception in generateContextMenuJson_for_People() of EntityManagementService at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }
  TreeOnFirstTimeLoad(treename: string, tree: object) {
    try {
      var jsonObject = { TreeType: treename, MessageData: tree };
      this.onTreeEventChanges.next(jsonObject);
    }
    catch (e) {
      console.error('Exception in TreeOnFirstTimeLoad() of EntityMgt in  EntityMgt/service at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }
  generateContextMenuJson_for_Pigging(data) {
    try {
      let list = [];
      list.push({ "displayName": "Create New Sender", "children": [] });
      list.push({ "displayName": "Create New Receiver", "children": [] });
      list.push({ "displayName": "Delete", "children": [] });
      let payload = {
        "fabric": FabricsNames.ENTITYMANAGEMENT,
        "dataType": "treeFabricConfigData",
        "tab": this.commonService.treeIndexLabel,
        "Payload": {
          "contextMenuOption": [
            {
              "displayName": "Fdc Context Menu",
              "children": list
            }
          ]
        }
      };
      this.sendDataToAngularTree([this.commonService.treeIndexLabel], TreeOperations.treeContextMenuOption, payload.Payload.contextMenuOption);
      this.commonService.activeContextMenuNodeId = data.EntityId;
    }
    catch (e) {
      console.error('Exception in generateContextMenuJson_for_Pigging() of EntityManagementService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in generateContextMenuJson_for_Pigging() of EntityManagementService at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }
  generateContextMenuJson_for_OPF(data) {
    try {
      let list = [];
      list.push({ "displayName": "Delete", "children": [] });
      let payload = {
        "fabric": FabricsNames.ENTITYMANAGEMENT,
        "dataType": "treeFabricConfigData",
        "tab": this.commonService.treeIndexLabel,
        "Payload": {
          "contextMenuOption": [
            {
              "displayName": "Fdc Context Menu",
              "children": list
            }
          ]
        }
      };
      this.sendDataToAngularTree([this.commonService.treeIndexLabel], TreeOperations.treeContextMenuOption, payload.Payload.contextMenuOption);
      this.commonService.activeContextMenuNodeId = data.EntityId;
    }
    catch (e) {
      console.error('Exception in generateContextMenuJson_for_Pigging() of EntityManagementService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in generateContextMenuJson_for_Pigging() of EntityManagementService at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }
  generateContextMenuJson_for_fdc(data) {
    try {
      var typeOfEntity = data.EntityType;
      let list = [];
      let listOption = [];
      let locoption1 = { "displayName": "Create New Equipment", "children": [] };
      let locoption2 = { "displayName": "Copy To New Entity", "children": [] };

      if (this.commonService.isCapbilityAdmin()) {
        if (this.commonService.treeIndexLabel != TreeTypeNames.LIST) {
          list.push(locoption1);
          list.push(locoption2);
        }
      }
      // else {
      //   this.allFormsRoleSchema.forEach(item => {
      //     if (AppConsts.Equipment.includes(item.TypeOf) && !listOption.includes(locoption1["displayName"])) {
      //       list.push(locoption1);
      //       listOption.push(locoption1["displayName"]);
      //     }
      //     if (((item.TypeOf == "Well" && Wells.includes(typeOfEntity)) || (item.TypeOf == "Delivered" && Delivered.includes(typeOfEntity)) || (item.TypeOf == "Facility" && Facilities.includes(typeOfEntity))) && !listOption.includes(locoption2["displayName"])) {
      //       list.push(locoption2);
      //       listOption.push(locoption2["displayName"]);
      //     }
      //   })
      // }

      let commdeleteoption2 = { "displayName": "Delete", "children": [] };
      if (this.commonService.isCapbilityAdmin()) {
        if (this.commonService.treeIndexLabel != TreeTypeNames.LIST) {
          list.push(commdeleteoption2);
        }
      }
      // else {
      //   this.allFormsRoleDeleteSchema.forEach(item => {
      //     if ((item.TypeOf == data.EntityType || ((item.TypeOf == "Well" && Wells.includes(typeOfEntity)) || (item.TypeOf == "Delivered" && Delivered.includes(typeOfEntity)) || (item.TypeOf == "Facility" && Facilities.includes(typeOfEntity)))) && !listOption.includes(commdeleteoption2["displayName"])) {
      //       list.push(commdeleteoption2);
      //       listOption.push(commdeleteoption2["displayName"]);
      //     }
      //   });
      // }
      if (!this.commonService.CustomerAppsettings.env.UseAsDesktop) {
        let commoption5 = { "displayName": "Open In New Tab", "children": [] };
        list.push(commoption5);
      }

      let payload = {
        "fabric": FabricsNames.ENTITYMANAGEMENT,
        "dataType": "treeFabricConfigData",
        "tab": this.commonService.treeIndexLabel,
        "Payload": {
          "contextMenuOption": [
            {
              "displayName": "Fdc Context Menu",
              "children": list
            }
          ]
        }
      };
      this.sendDataToAngularTree([this.commonService.treeIndexLabel], TreeOperations.treeContextMenuOption, payload.Payload.contextMenuOption);
      this.commonService.activeContextMenuNodeId = data.EntityId;
    }
    catch (e) {
      console.error('Exception in generateContextMenuJson() of EntityManagementService in EntityManagement/Service at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in generateContextMenuJson() of EntityManagementService in EntityManagement/Service at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }
  contextClickEvent_for_OPF(data, treeName) {
    switch (data.optionSelected) {
      case "Delete":
        this.DeleteOperation_For_FDC(data);
        break;

      default:
        break;
    }
  }
  contextClickEvent_for_pigging(data, treeName) {
    switch (data.optionSelected) {
      case "Delete":
        this.DeleteOperation_For_FDC(data);
        break;

      default:
        let nodeData = data.nodeData.data;
        let obj = {};
        obj['Parent'] = { 'EntityType': nodeData.EntityType, 'id': nodeData.EntityId, 'text': nodeData.EntityName };
        obj['EntityType'] = { Value: nodeData.EntityType, EntityName: nodeData.EntityType };

        obj['CategoryType'] = { 'EntityName': "Non-Wells", 'Value': "Non-Wells" };
        this.usersStateQuery.add(UserStateConstantsKeys.ContextMenuData, obj);// Reset Viewed Fabrics
        if (data.optionSelected == "Create New Sender" || data.optionSelected == "Create New Receiver") {
          this.sendDataToAngularTree([treeName], TreeOperations.deactivateSelectedNode, "");
          let type = '';
          if (data.optionSelected == "Create New Sender")
            type = 'Sender';
          else
            type = 'Receiver';
          if (this.getFormName() == "addnewentity") {
            var url = '/' + FabricRouter.ENTITYMANAGEMENT_FABRIC + '/addnewentity';
            this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC], { queryParams: { 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
            this.commonService.RerouteToAddNew = true;
          }
          setTimeout(() => {
            this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC, 'addnewentity'], { queryParams: { EntityType: type, 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
          }, 200);
        }
        break;
    }

  }
  contextClickEvent_for_People(data, treeName) {
    try {
      let nodeData = data.nodeData.data;
      switch (data.optionSelected) {
        case "Create Entity":
          break;
        case "Delete Entity":
          this.DeleteOperation_For_People(nodeData);
          break;
      }
    } catch (e) {
      this.commonService.printException('', 'EntityManagementService', e);
    }
  }
  DeleteOperation_For_People(data) {
    try {
      if (data.EntityId != this.commonService.tenantID) {
        let config = {
          header: 'Delete Entity/Data.  ' + data.EntityName,
          isSubmit: true,
          EntityId: data.EntityId,
          content: [DELETE_ENTITY],
          subContent: [],
          operation: PopupOperation.Delete,
          payload: []
        };
        if (data.children.length > 0 && data.EntityType == EntityTypes.COMPANY) {
          config.payload.push({
            "type": "select",
            "label": "Company",
            "actualname": "Company",
            "bindLabel": "EntityName",
            "bindValue": "EntityId",
            "validation": {
              "required": true,
              "minLength": "",
              "maxLength": "",
              "pattern": "",
              "custom": ""
            },
            "disable": true,
            "options": [],
            "measures_left": null,
            "isCalculated": true,
            "conditional": {
              "show": true,
              "when": "",
              "operator": "==",
              "eq": true
            },
            "DefaultValue": null,
            "colspan": 3
          });
          config.content = [DELETE_PEOPLE_WITH_OFFICES_PERSONS];
          config.subContent = [TRANSFEROFFICEPERSON];
          config.operation = PopupOperation.DeletePeople;
        }
        else if (data.children.length > 0 && data.EntityType == EntityTypes.OFFICE) {
          config.payload = [{
            "type": "select",
            "label": "Company",
            "actualname": "Company",
            "bindLabel": "EntityName",
            "bindValue": "EntityId",
            "validation": {
              "required": true,
              "minLength": "",
              "maxLength": "",
              "pattern": "",
              "custom": ""
            },
            "disable": true,
            "options": [],
            "measures_left": null,
            "isCalculated": true,
            "conditional": {
              "show": true,
              "when": "",
              "operator": "==",
              "eq": true
            },
            "DefaultValue": null,
            "colspan": 3
          },
          {
            "type": "select",
            "label": "Office",
            "actualname": "Office",
            "bindLabel": "EntityName",
            "bindValue": "EntityId",
            "validation": {
              "required": true,
              "minLength": "",
              "maxLength": "",
              "pattern": "",
              "custom": ""
            },
            "disable": true,
            "options": [],
            "measures_left": null,
            "isCalculated": true,
            "conditional": {
              "show": true,
              "when": "",
              "operator": "==",
              "eq": true
            },
            "DefaultValue": null,
            "colspan": 3
          }];
          config.content = [DELETE_PEOPLE_WITH_PERSONS];
          config.subContent = [TRANSFERPERSON];
          config.operation = PopupOperation.DeletePeople;
        }
        let matConfig = new MatDialogConfig();
        matConfig.data = config;
        matConfig.width = '500px';
        matConfig.disableClose = true;
        let dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
        dialogRef.componentInstance.emitResponse.subscribe((res: any) => {
          this.deletePeopleEntities(data, res, config.operation);
        });
        dialogRef.componentInstance.emitCancelResponse.subscribe((data: any) => {
          this.commonService.SkipSurePopUp = false;
        });
      }
      else {
        let config = {
          header: 'Delete Entity/Data.  ' + data.EntityName,
          isSubmit: true,
          EntityId: data.EntityId,
          content: [TENANT_COMPANY_DELETE_MESSAGE],
          subContent: [],
          operation: PopupOperation.Delete,
          payload: []
        };

        let matConfig = new MatDialogConfig();
        matConfig.data = config;
        matConfig.width = '500px';
        matConfig.disableClose = true;
        let dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
        dialogRef.componentInstance.emitResponse.subscribe((res: any) => {

        });
        dialogRef.componentInstance.emitCancelResponse.subscribe((data: any) => {
          this.commonService.SkipSurePopUp = false;
        });
      }

    }
    catch (ex) {
      this.commonService.appLogException(new Error(ex));
    }
  }
  ActivechildrenFormClose(Entitydata) {
    var flag = false;
    Entitydata.forEach(obj => {
      if (this.RouteParam.EntityID == obj.EntityId) {
        let params = JSON.parse(JSON.stringify(this.RouteParam));
        delete params.EntityID;
        this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC], { queryParams: params });
      }
      if (obj.children && obj.children.length > 0 && !flag) {
        this.ActivechildrenFormClose(obj.children);
      }
    });
  }
  closeLocationForm(EntityId) {
    let locationDataOfOthersession = this.commonService.getEntitiesById(EntityId);
    if (this.RouteParam.EntityID == EntityId) {
      let params = JSON.parse(JSON.stringify(this.RouteParam));
      delete params.EntityID;
      this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC], { queryParams: params });
    }
    else {
      if (locationDataOfOthersession && locationDataOfOthersession.children && locationDataOfOthersession.children.length > 0) {
        this.ActivechildrenFormClose(locationDataOfOthersession.children);
      }
    }
  }
  private closeForm() {
    let params = JSON.parse(JSON.stringify(this.RouteParam));
    delete params.EntityID;
    this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC], { queryParams: params });
  }
  deletePeopleEntities(data, res, option) {
    if (data.EntityId == this.RouteParam.EntityID) {
      this.closeForm();
    }
    else if (data.children && data.children.length > 0) {
      data.children.forEach(value => {
        if (value.EntityId == this.RouteParam.EntityID)
          this.closeForm();
      });
    }
    let Type = 'DeleteChildren';
    if (res.UpdateChildren == 'Yes') {
      Type = 'UpdateChildren';
      this.updateEntitiesWithNewParent(data, res.data);
    }
    let Payload = { 'EntityId': data.EntityId, 'TenantName': this.commonService.tenantName, 'TenantId': this.commonService.tenantID, 'EntityType': data.EntityType, 'Children': JSON.stringify(data.children), Type: Type };
    this.commonService.sendMessageToServer(JSON.stringify(Payload), FABRICS.PEOPLEANDCOMPANIES, data.EntityId, data.EntityType, MessageKind.DELETE, Routing.AllFrontEndButOrigin, 'Details', FABRICS.PEOPLEANDCOMPANIES);

  }
  updateEntitiesWithNewParent(data, Parent) {
    try {
      data.children.forEach(element => {
        element.Parent = Parent[data.EntityType].EntityId;
        element.ParentType = Parent[data.EntityType].EntityType;
        element.OfficeParent = Parent['Company'].EntityId;
      });
    } catch (error) {
      console.error(error);
    }


  }
  DeleteAndRecreatePeopleNode(message) {
    try {
      let payload = JSON.parse(message.Payload);
      let children = JSON.parse(payload.Children);
      children.forEach(element => {
        this.RemoveAndaddNodeBasedOnParent(JSON.parse(JSON.stringify(element)), TreeTypeNames.PeopleAndCompanies, payload.Type);
        if (message.EntityType == EntityTypes.COMPANY) {
          element.children.forEach(element1 => {
            this.RemoveAndaddNodeBasedOnParent(JSON.parse(JSON.stringify(element1)), TreeTypeNames.PeopleAndCompanies, payload.Type);
          });
        }
      });
      this.sendDataToAngularTree([TreeTypeNames.PeopleAndCompanies], TreeOperations.deleteNodeById, message.EntityID);
      this.entityCommonStoreQuery.deleteNodeById("EntityId", message.EntityID);
      this.deleteCompanyFromCompanyTypesStore(message.EntityID, [FABRICS.PEOPLEANDCOMPANIES]);
    } catch (error) {
      console.error(error);
    }


  }
  RemoveAndaddNodeBasedOnParent(element, treeName, operation) {
    this.sendDataToAngularTree([TreeTypeNames.PeopleAndCompanies], TreeOperations.deleteNodeById, element.EntityId);
    if (operation == 'UpdateChildren') {
      let data = JSON.parse(JSON.stringify({
        "InsertAtPosition": element.Parent,
        "NodeData": element
      }));
      this.sendDataToAngularTree([treeName], TreeOperations.AddNewNode, data);
      element.Parent = [{ EntityId: element.Parent, EntityType: element.ParentType }];
      this.commonService.entityCommonStoreQuery.upsert(element["EntityId"], element);
    }
    else
      this.entityCommonStoreQuery.deleteNodeById("EntityId", element["EntityId"]);
  }
  contextClickEvent_for_fdc(data) {
    try {
      switch (data.optionSelected) {
        case "Delete":
          this.DeleteOperation_For_FDC(data);
          break;
        case "Open In New Tab":
          let expand = false;
          let EmTab = this.commonService.treeIndexLabel;
          let formName = this.getFormName();
          if (formName == 'Settings') {
            if (this.route.queryParams && this.route.queryParams['_value']['expand'])
              expand = this.route.queryParams['_value']['expand'];
            if (this.route.queryParams && this.route.queryParams['_value']['EMTab'])
              EmTab = this.route.queryParams['_value']['EMTab'];
          }
          let url = this.router.createUrlTree([FabricRouter.ENTITYMANAGEMENT_FABRIC, 'Settings', data.data.EntityId, this.commonService.treeIndexLabel, this.commonService.treeIndexLabel], { queryParams: { 'EntityID': data.data.EntityId, 'expand': expand, 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable, EMTab: EmTab } }).toString();
          window.open(url, '_blank');
          break;
        case "Copy To New Entity":
          this.commonService.popupformclose = false;
          //this.isCreateContextMenuData = data
          //this.CopyToNewEntity = true;
          // this.commonService.isCopyToEntityForm = true;
          //this.MakeFormsAsReadOnly = false;
          this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC, 'copyentity', { EntityID: data.data.EntityId }], { queryParams: { 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
          this.sendDataToAngularTree([this.commonService.treeIndexLabel], TreeOperations.deactivateSelectedNode, '');
          break;
        case "Create New Equipment":
          let nodeData = data.nodeData.data;
          let obj = {};
          obj['Parent'] = { 'EntityType': nodeData.EntityType, 'id': nodeData.EntityId, 'text': nodeData.EntityName };
          obj['CategoryType'] = { Value: "Equipment", EntityName: "Equipment" };
          this.usersStateQuery.add(UserStateConstantsKeys.ContextMenuData, obj);// Reset Viewed Fabrics
          if (this.getFormName() == "addnewentity") {
            // var url = '/' + FabricRouter.ENTITYMANAGEMENT_FABRIC + '/addnewentity';
            this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC], { queryParams: { 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
            this.commonService.RerouteToAddNew = true;
          }
          this.sendDataToAngularTree([this.commonService.treeIndexLabel], TreeOperations.deactivateSelectedNode, '');
          setTimeout(() => {
            this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC, 'addnewentity'], { queryParams: { 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
          }, 200);
          break;

      }

    }
    catch (e) {
      console.error('Exception in contextClickEvent() of EntityManagementService in  EntityManagement/service at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in contextClickEvent() of EntityManagementService in EntityManagement/service at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  commomTreeOperationForFDc_OPF_Pigging(data, treeName) {
    switch (data.treeOperationType) {
      case TreeOperations.NodeOnClick: {

        break;
      }
      case TreeOperations.filteroptionjson: {
        this.filteredjsonstructure(treeName);
        break;
      }
      case TreeOperations.selectedfilteroption: {
        this.filterTreeData(data);
        break;
      }
      case TreeOperations.RemoveFilter:
        this.RemoveFilterFromTree(data);
        break;
      case TreeOperations.entityFilter:
        this.entityFilter(data);
        break;
      case TreeOperations.createRightTree:
        this.generateRightTree(data);
        break;

      // case TreeOperations.FullTreeOnCreation:
      //   {
      //     var configdata: ITreeConfig = {
      //       "treeActiveOnClick": true,
      //       "treeExpandAll": true
      //     }
      //     this.sendDataToAngularTree([treeName], TreeOperations.treeConfig, configdata);
      //     break;
      //   }
    }
  }
  getFormName() {
    try {
      let formType = '';
      let urltree = this.router.parseUrl(this.router.url);
      if (urltree.root.children && urltree.root.children.primary && urltree.root.children.primary.segments && urltree.root.children.primary.segments.length > 0) {
        let segments = urltree.root.children.primary.segments;
        for (let index = 0; index < segments.length; index++) {
          const element = segments[index];
          if (element.path == 'Settings') {
            formType = "Settings";
            break;
          }
          else if (element.path == 'addnewentity') {
            formType = "addnewentity";
            break;
          }
        }
      }
      return formType;
    } catch (error) {

    }
  }
  generateRightTreeBasedOnStaticEntityTypes(EntityTypes, treeData, Fabric) {
    let tree = [];
    let data = {
      "EntityName": Fabric,
      "EntityId": Fabric,
      "EntityType": "EMCategoryCatalog",
      "children": []
    };
    EntityTypes.forEach(d => {
      let rootNode: any = {};
      rootNode.EntityId = d;
      rootNode.EntityType = d;
      rootNode.EntityName = d;
      tree.push(rootNode);
    });
    data.children = tree;
    setTimeout(() => {
      this.sendDataToAngularTree(treeData.treeName, TreeOperations.createTree, [data]);
    }, 20);

  }

  generateRightTree(treeData) {
    let treeType;
    if (treeData.treeName == "Production Management_RIGHT")
      treeType = "EntityTypes";
    else if (treeData.treeName == "Operational Forms_RIGHT")
      treeType = "AssetTypes";

    var configdata: ITreeConfig = {
      "treeActiveOnClick": true,
      "treeExpandAll": true
    };

  }

  getSortingListOfChildren(listOfChildren) {
    try {
      let NewSortedListOfChildren = [];
      return NewSortedListOfChildren;
    }
    catch (e) {
      console.error('Exception in getSortingListOfChildren() of EntityManagement.formtemplate.component in EntityManagementFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  filteredjsonstructure(treeName) {
    try {
      let key = this.commonService.capabilityFabricId + "_" + treeName;
      this.getFilterData$(key, treeName).first().subscribe(data => {
        var message: AngularTreeMessageModel = {
          "fabric": this.commonService.lastOpenedFabric,
          "treeName": [this.commonService.treeIndexLabel],
          "treeOperationType": TreeOperations.filteroptionjson,
          "treePayload": JSON.parse(JSON.stringify(data))
        };
        this.commonService.sendDataToAngularTree.next(message);
      });
    }
    catch (e) {
      console.error('Exception in filteredjsonstructure() of EntityManagementService in EntityManagement/Service at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in filteredjsonstructure() of EntityManagementService in EntityManagement/Service at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }


  getFilterData$(Key, treeName) {
    try {
      return this.entityFilterStoreQuery
        .selectEntity(Key)
        .pipe(switchMap((hashCache) => {
          let newState;
          let apiCall;
          if (!hashCache) {
           
          }
          if (hashCache) {
            newState = JSON.parse(JSON.stringify(hashCache));
          }
          return newState ? of(newState) : apiCall;
        }));
    }
    catch (Ex) {
      console.error(Ex);
    }
  }

  formatData(statusData) {
    let obj = {};
    if (statusData) {
      statusData.forEach(d => {
        if (!obj[d.Type]) {
          obj[d.Type] = {};
        }
        let name = d['StatusesName'];
        obj[d.Type][name] = { EntityId: d['ConfigId'], value: true };
      });
    }
    return obj;
  }

  getFabricEntityTypes(message, filter) {
    try {
      var EntityTypesForSearch = [];
      var payload = JSON.parse(message.Payload);
      if (payload.EntityTypes && payload.EntityTypes.length != 0) {
        payload.EntityTypes.filter(filter).forEach(entity => {
          if (entity.Name != "WaterInjection") {
            var reg = /([A-Z])([A-Z])([a-z])|([a-z])([A-Z])/g;
            entity.Name = entity.Name.replace(reg, '$1$4 $2$3$5');
            EntityTypesForSearch.push(entity.Name);
          }
        });
      }
      return Array.from(new Set<string>(EntityTypesForSearch));
    } catch (e) {
      console.error('Exception in getFabricEntityTypes() of EntityManagementService at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in getFabricEntityTypes() of EntityManagementService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  filterTreeData(Data) {
    let filterData = Data.treePayload;
    let EntityType = Object.keys(filterData.EntityTypes).filter(d => filterData.EntityTypes[d]).reduce((obj, key) => { obj[key] = filterData.EntityTypes[key]; return obj; }, {});
    let Wellstatus = Object.keys(filterData.Status.Well).filter(d => filterData.Status.Well[d]['value']).reduce((obj, key) => { obj[key] = filterData.Status.Well[key]; return obj; }, {});
    let FacilityStatus = Object.keys(filterData.Status.Facility).filter(d => filterData.Status.Facility[d]['value']).reduce((obj, key) => { obj[key] = filterData.Status.Facility[key]; return obj; }, {});
    let WellOperationalStatus = Object.keys(filterData.OperationalStatus.Well).filter(d => filterData.OperationalStatus.Well[d]['value']).reduce((obj, key) => { obj[key] = filterData.OperationalStatus.Well[key]; return obj; }, {});
    let FacilityOperationalStatus = Object.keys(filterData.OperationalStatus.Facility).filter(d => filterData.OperationalStatus.Facility[d]['value']).reduce((obj, key) => { obj[key] = filterData.OperationalStatus.Facility[key]; return obj; }, {});
    let EntityTypes = Object.keys(EntityType);
    let EntityTypesWithOutSpace = [];
    let WellStatusEntityIds = [];
    let FacilityStatusEntityIds = [];
    let WellOperationalStatusKeys = Object.keys(WellOperationalStatus);
    let FacilityOperationalStatusKeys = Object.keys(FacilityOperationalStatus);
    EntityTypes.forEach(element => {
      EntityTypesWithOutSpace.push(element.replace(/\ /gi, ""));
    });
    for (const key in Wellstatus) {
      if (Wellstatus.hasOwnProperty(key)) {
        WellStatusEntityIds.push(Wellstatus[key]['EntityId']);
      }
    }
    for (const key in FacilityStatus) {
      if (FacilityStatus.hasOwnProperty(key)) {
        FacilityStatusEntityIds.push(FacilityStatus[key]['EntityId']);
      }
    }
    WellStatusEntityIds = [...WellStatusEntityIds, ...Object.keys(Wellstatus)];
    FacilityStatusEntityIds = [...FacilityStatusEntityIds, ...Object.keys(FacilityStatus)];
    let filterFunc = d => EntityTypesWithOutSpace.includes(d.EntityType) && ([].includes(d.TypeOf) && FacilityStatusEntityIds.includes(d.Status) && FacilityOperationalStatusKeys.includes(d.OperatedStatus)  && WellOperationalStatusKeys.includes(d.OperatedStatus));
    this.commonService.getEntititesFromStore$('TypeOf',
      [], d => true,
      d => { return d; })
      .pipe(untilDestroyed(this, 'destroy')).first()
      .subscribe((filteredentities: any) => {
        this.sendDataToAngularTree(Data.treeName, TreeOperations.filterTree, { Entities: filteredentities, filterFunc: filterFunc });
      });
  }

  RemoveFilterFromTree(data) {
    this.sendDataToAngularTree(data.treeName, TreeOperations.RemoveFilter, '');
  }

  entityFilter(Data: AngularTreeMessageModel) {
    this.commonService.getEntititesFromStore$('TypeOf',
      [], d => true,
      d => { return d; })
      .pipe(untilDestroyed(this, 'destroy')).first()
      .subscribe((filteredentities: any) => {
        this.sendDataToAngularTree(Data.treeName, TreeOperations.entityFilter, { Entities: filteredentities, value: Data.treePayload });
      });

    if (Data.treePayload == "") {
      setTimeout(() => {
        if (this.router && this.router.url && this.router.url.includes('EntityID')) {
          let entityId = this.commonService.getEntityIDFromUrl(this.router.url);
          this.sendDataToAngularTree(Data.treeName, TreeOperations.ActiveSelectedNode, entityId);
        }
      }, 10);
    }
  }



  entityFilterForRightTree(Data: AngularTreeMessageModel, RightTree_Data?, staticData_To_createJSON?) {
    let treeData = [];
     if (staticData_To_createJSON && Data.treeName && ( Data.treeName[0] == TreeTypeNames.PeopleAndCompanies_Right)) {
      staticData_To_createJSON.forEach(d => {
        let rootNode: any = {};
        rootNode.EntityId = d;
        rootNode.EntityType = d;
        rootNode.EntityName = d;
        treeData.push(rootNode);
      });
    }
    this.sendDataToAngularTree(Data.treeName, TreeOperations.entityFilter, { Entities: treeData, value: Data.treePayload });
  }


  filteredjsonstructureBI(treeName) {
    try {
      let key = this.commonService.capabilityFabricId + "_" + treeName;
      this.entityFilterStoreQuery
        .selectEntity(key).first()
        .subscribe(res => {
          if (res) {
            var message: AngularTreeMessageModel = {
              "fabric": this.commonService.lastOpenedFabric,
              "treeName": [this.commonService.treeIndexLabel],
              "treeOperationType": TreeOperations.filteroptionjson,
              "treePayload": JSON.parse(JSON.stringify(res))
            };
            this.commonService.sendDataToAngularTree.next(message);
          }
          else {
            let EntityTypeKeyValue = {};
            FilterEntitiesfor_BI.forEach(d => { EntityTypeKeyValue[d] = true; });
            let data = {
              EntityTypes: EntityTypeKeyValue,
            };
            let schema = [
              {
                "expand": true,
                "expandIcon": "V3 ToggleDown",
                "name": "ENTITY TYPES",
                "actualName": "EntityTypes",
                "type": "textWithcross",
              }
            ];
            var message: AngularTreeMessageModel = {
              "fabric": this.commonService.lastOpenedFabric,
              "treeName": [this.commonService.treeIndexLabel],
              "treeOperationType": TreeOperations.filteroptionjson,
              "treePayload": {
                schema: schema, data: data, capability: this.commonService.capabilityFabricId, treeName: treeName
              }
            };
            this.commonService.sendDataToAngularTree.next(message);
            // })
            //})

          }
        });



    }
    catch (e) {
      console.error('Exception in filteredjsonstructure() of EntityManagementService in EntityManagement/Service at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  entityFilterBI(Data: AngularTreeMessageModel) {
    var types = [EntityTypes.Category, EntityTypes.DashBoard];
    this.commonService.getEntititesFromStore$("EntityType", types, d => true,
      d => { return d; },
      FABRICS.BUSINESSINTELLIGENCE)
      .pipe(untilDestroyed(this, 'destroy')).first()
      .subscribe((filteredentities: any) => {
        this.sendDataToAngularTree([Data.treeName[0]], TreeOperations.entityFilter, { Entities: filteredentities, value: Data.treePayload });
      });
  }

  filterTreeDataBI(Data) {
    let filterData = Data.treePayload;
    let types = [EntityTypes.Category, EntityTypes.DashBoard];
    let EntityType = Object.keys(filterData.EntityTypes).filter(d => filterData.EntityTypes[d]).reduce((obj, key) => { obj[key] = filterData.EntityTypes[key]; return obj; }, {});
    let EntityTypeArray = Object.keys(EntityType);
    let filterFunc = d => EntityTypeArray.includes(d.EntityType);
    this.commonService.getEntititesFromStore$("EntityType", types, d => true,
      d => { return d; },
      FABRICS.BUSINESSINTELLIGENCE)
      .pipe(untilDestroyed(this, 'destroy')).first()
      .subscribe((filteredentities: any) => {
        this.sendDataToAngularTree(Data.treeName[0], TreeOperations.filterTree, { Entities: filteredentities, filterFunc: filterFunc });
      });
  }

  filteredjsonstructurePEOPLE(treeName) {
    try {
      let key = this.commonService.capabilityFabricId + "_" + treeName;
      this.entityFilterStoreQuery
        .selectEntity(key).first()
        .subscribe(res => {
          if (res) {
            var message: AngularTreeMessageModel = {
              "fabric": this.commonService.lastOpenedFabric,
              "treeName": [this.commonService.treeIndexLabel],
              "treeOperationType": TreeOperations.filteroptionjson,
              "treePayload": JSON.parse(JSON.stringify(res))
            };
            this.commonService.sendDataToAngularTree.next(message);
          }
          else {
            let EntityTypeKeyValue = {};
            peopleEntityTypes.forEach(d => { EntityTypeKeyValue[d] = true; });
            let data = {
              EntityTypes: EntityTypeKeyValue,
            };
            let schema = [
              {
                "expand": true,
                "expandIcon": "V3 ToggleDown",
                "name": "ENTITY TYPES",
                "actualName": "EntityTypes",
                "type": "textWithcross",
              }
            ];
            var message: AngularTreeMessageModel = {
              "fabric": this.commonService.lastOpenedFabric,
              "treeName": [this.commonService.treeIndexLabel],
              "treeOperationType": TreeOperations.filteroptionjson,
              "treePayload": {
                schema: schema, data: data, capability: this.commonService.capabilityFabricId, treeName: treeName
              }
            };
            this.commonService.sendDataToAngularTree.next(message);
            // })
            //})

          }
        });



    }
    catch (e) {
      console.error('Exception in filteredjsonstructure() of EntityManagementService in EntityManagement/Service at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }
  entityFilterPEOPLE(Data: AngularTreeMessageModel) {
    var types = [TypeOfEntity.Company, TypeOfEntity.Office, TypeOfEntity.Person];

    this.commonService.getEntititesFromStore$("EntityType", types, d => true,
      d => { return d; },
      FABRICS.PEOPLEANDCOMPANIES)
      .pipe(untilDestroyed(this, 'destroy')).first()
      .subscribe((filteredentities: any) => {
        this.sendDataToAngularTree([Data.treeName[0]], TreeOperations.entityFilter, { Entities: filteredentities, value: Data.treePayload });
      });
  }
  RemoveFilterFromTreePEOPLE(data) {
    this.sendDataToAngularTree(data.treeName[0], TreeOperations.RemoveFilter, '');
  }

  filterTreeDataPEOPLE(Data) {
    let filterData = Data.treePayload;
    let EntityType = Object.keys(filterData.EntityTypes).filter(d => filterData.EntityTypes[d]).reduce((obj, key) => { obj[key] = filterData.EntityTypes[key]; return obj; }, {});
    let EntityTypes = Object.keys(EntityType);
    let filterFunc = d => EntityTypes.includes(d.EntityType);
    var types = [TypeOfEntity.Company, TypeOfEntity.Office, TypeOfEntity.Person];
    this.commonService.getEntititesFromStore$("EntityType", types, d => true,
      d => { return d; },
      FABRICS.PEOPLEANDCOMPANIES)
      .pipe(untilDestroyed(this, 'destroy')).first()
      .subscribe((filteredentities: any) => {
        this.sendDataToAngularTree(Data.treeName[0], TreeOperations.filterTree, { Entities: filteredentities, filterFunc: filterFunc });
      });
  }

  click(treedata) {
    this.getEMCurrentProductionDay();
    if (this.commonService.rightSideCapabilityList.length != 0) {
      this.commonService.rightSideCapabilityList.forEach(data => {
        if (!data["routerLinkActive"]) {
          let clickModel = this.commonService.getRightClickEventModelObject(data);
          this.commonService.rightTreeClick.next(clickModel);
        }
      });
    }

    if (treedata && treedata.treePayload && treedata.treePayload.data) {
      let expand = false;
      let EmTab = this.commonService.treeIndexLabel;
      let formName = this.getFormName();

      if (formName == 'Settings') {
        if (this.route.queryParams && this.route.queryParams['_value']['expand'])
          expand = this.route.queryParams['_value']['expand'];
        //   if (this.route.queryParams && this.route.queryParams['_value']['EMTab'])
        //     EmTab = this.route.queryParams['_value']['EMTab']
      }

      if (treedata.treePayload.data.EntityType == EntityTypes.OFFICE || treedata.treePayload.data.EntityType == EntityTypes.COMPANY) {
        EmTab = TreeTypeNames.PeopleAndCompanies;
      }
      else if (treedata.treePayload.data.EntityType == EntityTypes.PERSON) {
        EmTab = EntityTypes.PERSON;
      }
      else if (treedata.treePayload.data.EntityType == EntityTypes.Construction) {
        EmTab = EntityTypes.Construction;
      }

      const urlParams = new URLSearchParams(this.router.url);
      const emTab = urlParams.get('EMTab');
      const physicalItem = urlParams.get('physicalItem');

      if (physicalItem || [TreeTypeNames.Materials, TreeTypeNames.Documents, TreeTypeNames.PhysicalInstances, TreeTypeNames.Schedule, TreeTypeNames.TaggedItems].includes(emTab)) {
        const tempTab = physicalItem ? physicalItem : emTab;
        this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC, 'Settings', treedata.treePayload.data.EntityId, this.commonService.treeIndexLabel, tempTab], { queryParams: { 'EntityID': treedata.treePayload.data.EntityId, 'expand': expand, 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable, EMTab: tempTab, physicalItem: tempTab } });
      }
      else {
        this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC, 'Settings', treedata.treePayload.data.EntityId, this.commonService.treeIndexLabel, EmTab], { queryParams: { 'EntityID': treedata.treePayload.data.EntityId, 'expand': expand, 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable, EMTab: EmTab } });
      }
    }
  }



  /**
 *
 * @param treeName Name of the tree e.g. 'Physical Flow'
 * @param treeOperationType "GetTreeInstance"
 */
  getAngularTreeEvent(treeName: Array<string>, treeOperationType: string, payload?): EventEmitter<any> {
    let eventModel: AngularTreeEventMessageModel = {
      fabric: FABRICS.ENTITYMANAGEMENT,
      treeName: treeName,
      treeOperationType: treeOperationType,//
      treePayload: payload ? payload : '',
      Event: new EventEmitter()
    };
    setTimeout(() => {
      this.commonService.getAngularTreeEvent$.next(eventModel);
    });
    return eventModel.Event;
  }
  DeleteOperation_For_FDC(data) {

  }
  DeleteFdcLeftSideEntitiesInSameSession(Capabilities, EntityId) {
    Capabilities.forEach(element => {
      this.deleteChildren(element.CapabilityName, EntityId);
      this.removeEntityFromLeftTree(element.ShowValue, TreeOperations.deleteNodeById, EntityId, EntityId, element.value);
    });
  }


  removeEntityFromLeftTree(treeName?: string, treeOperationType?: string, payload?: any, EntityId?, deleteStatus?: boolean) {
    try {
      let treeSendList = [];

      treeSendList = [
        { tree: treeName, operation: treeOperationType, data: payload }
      ];

      if (deleteStatus !== undefined || deleteStatus !== null) {

        treeSendList = [];

        treeSendList = treeSendList.filter(data => data.deleteStatus === true);
      }

      this.commonService.sendMultiOperationPayloadAngularTree(treeSendList);

    } catch (error) {
      console.error(error);
      throw error;
    }

  }

  deleteChildren(fabric, parentId: string) {
    try {
      let entityIds: any = [];
      entityIds = this.commonService.getEntityIdsOfChildren(parentId, fabric);
      if (entityIds && entityIds.length > 0)
        this.commonService.removeEntitiesByIds(fabric, entityIds);
      this.commonService.removeEntityExistInLists(parentId, fabric);
    }
    catch (e) {
      console.error('Exception in deleteChildren() of EntityManagementService in  EntityManagement/service at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in deleteChildren() of EntityManagementService in EntityManagement/service at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  findNodeIsExist(treedata, EntityId, foundEntity?: boolean) {
    try {
      for (let index = 0; index < treedata.length; index++) {
        const element = treedata[index];
        if (element.EntityId === EntityId) {
          return true;
        }
        else {
          if (element.children && element.children.length > 0)
            foundEntity = this.findNodeIsExist(element.children, EntityId);
          if (foundEntity) {
            break;
          }
        }
      }
      return foundEntity;
    } catch (error) {
      this.commonService.appLogException(new Error(error));
    }

  }

  // PushDownstreamLocations(node, Type, downstreamId) {
  //   try {
  //     let entity;
  //     if (node.EntityId && node.EntityId != downstreamId) {
  //       if (node.Downstream && node.Downstream[0] && (node.Downstream[0].EntityId == downstreamId)) {
  //         entity = {
  //           "EntityId": node.EntityId,
  //           "EntityType": node.EntityType,
  //           "EntityName": node.EntityName,
  //           "DownStreamLocationName": Type["DownStreamLocationName"],
  //           "DownStreamLocationId": Type["DownStreamLocationId"]
  //         }
  //         node.Downstream = [{ EntityId: Type["DownStreamLocationId"], EntityName: Type["DownStreamLocationName"] }]
  //       }
  //       else {
  //         let DownAndUpstreamId;
  //         let DownAndUpstreamEntityName;
  //         if (node.Downstream) {
  //           DownAndUpstreamId = node.Downstream[0].EntityId;
  //           DownAndUpstreamEntityName = node.Downstream[0].EntityName;
  //         }
  //         else {
  //           if (node.Upstream) {
  //             DownAndUpstreamId = node.Upstream[0].EntityId;
  //             DownAndUpstreamEntityName = node.Upstream[0].EntityName;
  //           }
  //         }
  //         entity = {
  //           "EntityId": node.EntityId,
  //           "EntityType": node.EntityType,
  //           "EntityName": node.EntityName,
  //           "DownStreamLocationName": DownAndUpstreamEntityName,
  //           "DownStreamLocationId": DownAndUpstreamId
  //         }
  //       }

  //       let valueChangedFields = {
  //         "DownStreamLocationName": Type["DownStreamLocationId"]
  //       }
  //       this.updateTreeName(entity, true, valueChangedFields);
  //     }
  //     if (node.children && node.children.length > 0) {
  //       node.children.forEach(obj => {
  //         setTimeout(() => { this.PushDownstreamLocations(obj, Type, downstreamId) })
  //       }, 0);
  //     }
  //   }
  //   catch (error) {
  //     this.commonService.appLogException(new Error(error));
  //   }
  // }

  PushDownstreamLocations(node, updatedata, nestedunderid, capabilities?: Array<object>) {
    try {
      if (node.EntityId && node.EntityId != nestedunderid) {
        node.NestedUnderId = updatedata.NestedUnderId;
        node.NestedUnderName = updatedata.NestedUnderName;
        this.updateTreeName(node, false, updatedata, capabilities);
        this.UpdatingStateDownStram(node.EntityId, updatedata);
      }
      if (node.children && node.children.length > 0) {
        node.children.forEach(obj => {
          this.PushDownstreamLocations(obj, updatedata, nestedunderid, capabilities);
        }, 0);
      }
    }
    catch (error) {
      this.commonService.appLogException(new Error(error));
    }
  }
  UpdatingStateDownStram(entityId, updatedata) {
    try {
      var entityData = this.commonService.getEntitiesById(entityId);
      if (entityData && entityData.Downstream && entityData.Downstream[0]) {
        entityData.Downstream[0].EntityId = updatedata.NestedUnderId;
        entityData.Downstream[0].EntityName = updatedata.NestedUnderName;
        this.entityCommonStoreQuery.upsert(entityData.EntityId, entityData);
      }
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in UpdatingStateDownStram() of FDCService at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  updateTreeName(myForm, IsOtherSessionUpdateMessage?: any, ValueChangedFields?: any, capabilities?: any) {
    try {
      //this.updateProps(myForm, IsOtherSessionUpdateMessage, ValueChangedFields);

      let nodeData = {
        "EntityId": myForm.EntityId,
        "EntityName": myForm.EntityName,
        "EntityType": myForm.EntityType,
      };
      let updatedNode = { "EntityId": myForm.EntityId, "NodeData": nodeData };
      this.sendDataToAngularTree([TreeTypeNames.SecurityGroupsEntitiesTree], TreeOperations.updateNodeById, updatedNode);
    }
    catch (e) {
      console.error(e);
    }
  }
  sendDataToMultiTreeName(treeName: string[], treeOperationType: string, payload: any) {
    treeName.forEach(tree => {
      this.sendDataToAngularTree([tree], treeOperationType, payload);
    });
  }
  getTreePayload(payload) {
    var json = {
      EntityName: payload.EntityType == EntityTypes.FACILITYCODE ? payload.FacilityCode : payload.EntityName ? payload.EntityName : "",
      children: payload.children ? payload.children : [],
      EntityId: payload.EntityId,
      EntityType: payload.EntityType,
      Parent: payload.Parent ? payload.Parent : [],
      WellType: payload.WellType ? payload.WellType : "",
      Downstream: payload.Downstream ? payload.Downstream : [],
    };
    return JSON.parse(JSON.stringify(json));
  }

  //********************************************************************************* */
  ///////////////////////////PEOPLE CREATE AND UPDATE METHODS START
  //********************************************************************************* */

  peoplePartialuPdate(message: MessageModel, isCurrentSession) {
    try {
      if (CommonJsMethods.isvalidJson(message.Payload)) {
        let payload = JSON.parse(message.Payload);
        if(payload && payload.OperationType == "invite"){
          this.commonService.visurUser.next(payload.ValueChangedFields);
        }
        else if (payload) {
          let entityType = payload["EntityType"].toString();
          if (entityType == "Person" && payload["ScheduleInvite"] != null) {

            let newMessage = JSON.parse(JSON.stringify(message));
            newMessage.EntityType = message.EntityType + "_ScheduleInvite";
            newMessage.Fabric = message.Fabric + "_em_person_schedule";
            this.commonService.sendMessagesToFabricServices.next(newMessage);
            this.commonService.loadingBarAndSnackbarStatus("complete", "Entity Updated");
          }
          else {
            if (CommonJsMethods.isvalidJson(payload.Payload)) {
               if(payload.EntityType == "Company" || payload.EntityType == "Person"){
                this.commonService.visurUser.next(payload);
               }
              let nodeData = JSON.parse(payload['Payload']);
              this.partialUpdateFromBackendToleftTreePeople(message.EntityID, message.EntityType, nodeData, isCurrentSession);
              this.commonService.loadingBarAndSnackbarStatus("complete", "Entity Updated");
            }
            else {
              console.error("peoplePartialUpdate() EntityInfoJson," + payload.EntityInfoJson);
            }
          }

        }
        else {
          console.error("peoplePartialUpdate() receieved payload," + payload);
        }
      }
      else {
        console.error("peoplePartialuPdate()", message.Payload);
      }
    }
    catch (e) {
      console.error("peoplePartialuPdate" + e);
    }
  }

  AddNewNodeLeftTreePeople(message: MessageModel) {
    try {
      //send to left tree people
      if (CommonJsMethods.isvalidJson(message.Payload)) {
        let msgPayload = JSON.parse(message.Payload);
        if (msgPayload) {
          if (CommonJsMethods.isvalidJson(msgPayload.Payload)) {
            let formPayload = JSON.parse(msgPayload.Payload)[message.EntityType.toLowerCase()];
            switch (message.EntityType.toLowerCase()) {
              case "company":
                {
                  let companyModel: CompanyModel = this.getEntityProperties(formPayload, CompanyModel, null);
                  let companyTreePayload = this.getPeopleTreePayload(companyModel);

                  let companyNodeData = {
                    "InsertAtPosition": "althingTreeRoot",
                    "NodeData": companyTreePayload
                  };
                  this.sendDataToAngularTree([TreeTypeNames.PeopleAndCompanies], TreeOperations.AddNewNode, JSON.parse(JSON.stringify(companyNodeData)));

                }
                break;
              case "office":
                {
                  let officeModel: OfficeModel = this.getEntityProperties(formPayload, OfficeModel, "Company");
                  let officeTreePayload = this.getPeopleTreePayload(officeModel);

                  let officeNodeData = {
                    "InsertAtPosition": officeTreePayload.Parent[0].EntityId,
                    "NodeData": officeTreePayload
                  };
                  this.sendDataToAngularTree([TreeTypeNames.PeopleAndCompanies], TreeOperations.AddNewNode, JSON.parse(JSON.stringify(officeNodeData)));

                  this.expandCompanyOnOfficeCreationFromAccordianPlusBtn(officeTreePayload.Parent[0].EntityId);
                }
                break;
              case "person":
                {
                  let personModel: PersonsModel = this.getEntityProperties(formPayload, PersonsModel, "Company");
                  let personTreePayload = this.getPeopleTreePayload(personModel);

                  let personNodeData = {
                    "InsertAtPosition": personTreePayload.Parent && personTreePayload.Parent.length?personTreePayload.Parent[0].EntityId:"althingTreeRoot",
                    "NodeData": personTreePayload
                  };

                  this.sendDataToAngularTree([TreeTypeNames.PeopleAndCompanies], TreeOperations.AddNewNode, JSON.parse(JSON.stringify(personNodeData)));
                }
                break;
              default:
                break;
            }
          }
        }
      }

      //add or update people state
      this.upsertPeopleState(message);
    }
    catch (e) {
      console.error("AddNewNodeLeftTreePeople" + e);
    }
  }
  expandCompanyOnOfficeCreationFromAccordianPlusBtn(officeParentId) {
    let url = this.router.url;
    if (url) {
      let emTab = this.commonService.getFabricTabByUrl('EMTab');
      if (emTab) {//any em tab than
        //send active node to company
        let isSubscribeTree = true;
        this.commonService.getTreeAndTreeNodeByEntityId(FABRICS.ENTITYMANAGEMENT, [TreeTypeNames.PeopleAndCompanies], officeParentId).filter(d => d && isSubscribeTree).subscribe(res => {
          if (officeParentId && res && res.tree && res.tree.treeModel && res.treeNode) {
            let model: TreeModel = res.tree.treeModel;
            let node = res.treeNode;
            node.expand();
          }
          isSubscribeTree = false;
        });
        // this.sendDataToAngularTree([TreeTypeNames.PeopleAndCompanies], TreeOperations.ActiveSelectedNode, officeParentId);
      }
    }
  }
  getPeopleTreePayload(stateTreeModel) {
    var treeJson = {
      EntityId: stateTreeModel.EntityId,
      EntityName: stateTreeModel.EntityName,
      EntityType: stateTreeModel.EntityType,
      children: [],
      Icon: this.commonService.getPeopleTreeIcon(stateTreeModel['EntityType']),
      Parent: []
    };

    if (stateTreeModel && stateTreeModel.children) {
      treeJson['children'] = stateTreeModel['children'];
    }

    if (stateTreeModel && stateTreeModel.Parent) {
      treeJson["Parent"] = stateTreeModel.Parent;
    }
    else {
      delete treeJson.Parent;
    }
    return treeJson;
  }

  upsertPeopleState(message: MessageModel) {
    try {
      let statePayload = this.getPeopleStatePayload(message);
      if (statePayload)
        this.entityCommonStoreQuery.upsert(statePayload.EntityId, statePayload);
      else
        console.error("Unable upsert people state,received statePayload:" + statePayload);
    }
    catch (e) {
      console.error(e);
    }
  }

  getPeopleStatePayload(message: MessageModel) {
    let statePayload = null;
    if (CommonJsMethods.isvalidJson(message.Payload)) {
      let msgPayload = JSON.parse(message.Payload);
      if (msgPayload) {
        if (CommonJsMethods.isvalidJson(msgPayload.Payload)) {
          let formPayload = JSON.parse(msgPayload.Payload)[message.EntityType.toLowerCase()];
          switch (message.EntityType.toLowerCase()) {
            case "company":
              let companyModel: CompanyModel = this.getEntityProperties(formPayload, CompanyModel, null);
              statePayload = companyModel;
              break;
            case "office":
              let officeModel: OfficeModel = this.getEntityProperties(formPayload, OfficeModel, "Company");
              statePayload = officeModel;
              break;
            case "person":
              let personModel: PersonsModel = this.getEntityProperties(formPayload, PersonsModel, "Company");
              statePayload = personModel;
              break;
            default:
              break;
          }
        }
      }
    }
    return statePayload;
  }

  getEntityProperties(payload, model, parentType?) {
    var instance = new model().getdefultData();
    for (var prop in payload) {
      if (!instance.hasOwnProperty(prop)) {
        continue;
      }
      else {
        if (prop == "Parent" && parentType) {
          if (parentType == EntityTypes.DISTRICT || parentType == EntityTypes.AREA)
            instance[prop] = [{ EntityId: payload[prop].EntityId, EntityType: parentType }];
          else
            instance[prop] = [{ EntityId: payload[prop], EntityType: parentType }];
        }
        else {
          instance[prop] = payload[prop];
        }
      }
    }
    return instance;
  }
  createCompanyTypesIntoState(entityId, entityType, capabilityId, capabilityName, companyType: Array<string>, operationType) {

    let entityCapId = entityId + "_" + capabilityId;
    let compayTypesStateModel: any = this.commonService.entityCompanyTypesStoreQuery.getEntity(entityCapId);

    if (compayTypesStateModel) {//update existing entity
      let stateEntity = JSON.parse(JSON.stringify(compayTypesStateModel));
      if (operationType == "Add")
        stateEntity.CompanyType.push(companyType[0]);
      else if (operationType == "Remove" && stateEntity.CompanyType.length)
        stateEntity.CompanyType = stateEntity.CompanyType.filter(compyType => compyType && compyType != companyType[0]);

      this.addOrUpdateEntityCompanyTypesState(entityCapId, (<PeopleCompanyTypes>stateEntity));
    }
    else { //create new entity
      let peopleAndCompaniesEntity = this.commonService.getEntitiesById(entityId);

      compayTypesStateModel = new PeopleCompanyTypes().getdefultData();
      compayTypesStateModel.EntityId = entityCapId;
      compayTypesStateModel.EntityName = peopleAndCompaniesEntity.EntityName;
      compayTypesStateModel.EntityType = entityType;
      compayTypesStateModel.CapabilityId = capabilityId;
      compayTypesStateModel.CapabilityName = capabilityName;

      if (operationType == "Remove")
        companyType = [];

      compayTypesStateModel.CompanyType = companyType;
      this.addOrUpdateEntityCompanyTypesState(entityCapId, (<PeopleCompanyTypes>compayTypesStateModel));
    }

  }

  addOrUpdateEntityCompanyTypesState(entityCapId, statePayload: PeopleCompanyTypes) {
    this.commonService.entityCompanyTypesStoreQuery.upsert(entityCapId, statePayload);
  }

  /**
   *
   * @param entityId companyId
   * @param capabilities fdc,opf,pigging etc etc fabric capabilityName array
   */
  deleteCompanyFromCompanyTypesStore(entityId, capabilities: Array<string>) {
    try {
      let deleteEntities = [];
      capabilities = this.commonService.getCapabilityIdsBy(capabilities);
      for (var capId of capabilities) {
        deleteEntities.push(entityId + "_" + capId);
      }
      this.commonService.entityCompanyTypesStoreQuery.removeEntities(deleteEntities);
    }
    catch (e) {
      console.error(e);
    }
  }

  peopleActiveNodeLeftTree = true;

  partialUpdateFromBackendToleftTreePeople(EntityId, EntityType, EntityInfoJson, currentSession) {
    try {
      let nodeData = JSON.parse(JSON.stringify(EntityInfoJson));
      if (nodeData && nodeData.ValueChangedFields) {
        let valueChangedFields = JSON.parse(nodeData.ValueChangedFields);
        let valueFieldJson = { EntityId: EntityId, EntityInfoJSON: valueChangedFields };

        if (valueChangedFields["CompanyType"]) {
          let fabric = nodeData["FormTabFabric"];
          let capabilityId = null;
          let capabilityName = null;
          

          if (capabilityId && capabilityName) {
            let companyTypes: Array<string> = valueChangedFields["CompanyType"];
            let operationType = valueChangedFields["AddOrRemove"] == true ? "Add" : "Remove";
            this.createCompanyTypesIntoState(EntityId, EntityType, capabilityId, capabilityName, companyTypes, operationType);
          }
        }

        switch (EntityType.toLowerCase()) {
          case "company":
            if (valueChangedFields[EntityType.toLowerCase()]) {
              this.updateLeftTreeNodePeople(valueFieldJson, "update", "company");

              //update companyTypes state
              this.updateEntityNameCompanyTypesState(EntityId, valueChangedFields["EntityName"]);
            }
            else if (valueChangedFields["CompanyTypes"]) {
            }
            break;
          case "office":
            if (valueChangedFields["EntityName"]) {
              this.updateLeftTreeNodePeople(valueFieldJson, "update", "office");
            }
            //office dropdown parent entity change
            else if (valueChangedFields["Company"]) {
              this.onDropDownChangeUpdateLeftTreeNode(EntityId, EntityInfoJson, "update", "office");

              setTimeout(() => {
                if (currentSession && this.peopleActiveNodeLeftTree)
                  this.sendDataToAngularTree([TreeTypeNames.PeopleAndCompanies], TreeOperations.ActiveSelectedNode, EntityId);
                this.peopleActiveNodeLeftTree = true;
              }, 2000);
            }

            break;
          case "person":
            //EntityName update in tree
            if (valueChangedFields[EntityType.toLowerCase()] && (valueChangedFields[EntityType.toLowerCase()]["firstname"] || valueChangedFields[EntityType.toLowerCase()]["lastname"])){
              this.updateLeftTreeNodePeople(valueFieldJson, "update", "person",nodeData['person']);
            }
            else
              //on drop down change update in left tree
              if (valueChangedFields["Office"]) {
                this.onDropDownChangeUpdateLeftTreeNode(EntityId, EntityInfoJson, "update", "person");

                setTimeout(() => {
                  if (currentSession)
                    this.sendDataToAngularTree([TreeTypeNames.PeopleAndCompanies], TreeOperations.ActiveSelectedNode, EntityId);
                }, 2000);
              }

            break;
          default: break;
        }

        //update state
      }
    }
    catch (e) {
      console.error(e);
    }
  }
  updateEntityNameCompanyTypesState(entityId, entityName) {
    this.commonService.entityCompanyTypesStoreQuery.getAll({
      filterBy: ((entity: any) => entity && entity.EntityId && entity.EntityId.startsWith(entityId))
    }).map((entity: any) => {
      // produce(entity,draf=>{
      //   draf.EntityName=entityName;
      // })
      let stateEntity = JSON.parse(JSON.stringify(entity));
      stateEntity["EntityName"] = entityName;

      this.commonService.entityCompanyTypesStoreQuery.upsert(entity.EntityId, stateEntity);
    });
  }
  updateLeftTreeNodePeople(valueJsonFieldsJSON: { EntityId: string, EntityInfoJSON: any; }, messageKind: string, EntityType: string,nodeData?) {
    var leftTreeNodeData = {};
    var entityInfo = valueJsonFieldsJSON.EntityInfoJSON;
    if (EntityType) {
      switch (EntityType.toLowerCase()) {
        case "company":
          switch (messageKind) {
            case "update": {
              entityInfo["EntityId"] = valueJsonFieldsJSON.EntityId;
              entityInfo["EntityType"] = "Company";
              let treeNodeData = this.getTreePartialPayload(entityInfo, EntityType);
              treeNodeData.EntityName = treeNodeData.EntityName?treeNodeData.EntityName:entityInfo[EntityType.toLowerCase()]['companyname'];
              let updatedNode = { "EntityId": valueJsonFieldsJSON.EntityId, "NodeData": treeNodeData };
              this.sendDataToAngularTree([TreeTypeNames.PeopleAndCompanies], TreeOperations.updateNodeById, JSON.parse(JSON.stringify(updatedNode)));
              this.partialUpdatePeopleState(entityInfo);
            }
              break;
            case "delete":
              break;
          }
          break;
        case "office":
          switch (messageKind) {
            case "update":
              entityInfo["EntityId"] = valueJsonFieldsJSON.EntityId;
              entityInfo["EntityType"] = "Office";
              if (!entityInfo["Company"]) {
                //get company office entity from state and update entityInfo object
                let officeEntity = this.commonService.getEntitiesById(valueJsonFieldsJSON.EntityId);
                if (officeEntity)
                  entityInfo.Company = officeEntity["Parent"][0].EntityId;
                else {
                  this.peopleActiveNodeLeftTree = false;
                  return;
                }
              }
              else if (entityInfo["Company"] == "" && entityInfo["Company"] == undefined && entityInfo["Company"] == null) {
                //get company office entity from state and update entityInfo object
                let officeEntity = this.commonService.getEntitiesById(valueJsonFieldsJSON.EntityId);
                entityInfo.Company = officeEntity["Parent"][0].EntityId;
              }

              let treeNodeData = this.getTreePartialPayload(entityInfo, EntityType);
              let updatedNode = { "EntityId": valueJsonFieldsJSON.EntityId, "NodeData": treeNodeData };
              this.sendDataToAngularTree([TreeTypeNames.PeopleAndCompanies], TreeOperations.updateNodeById, JSON.parse(JSON.stringify(updatedNode)));

              this.partialUpdatePeopleState(entityInfo);
              break;
            case "delete":
              break;
          }
          break;
        case "person":
          switch (messageKind) {
            case "update":
              entityInfo["EntityId"] = valueJsonFieldsJSON.EntityId;
              entityInfo["EntityType"] = "Person";
              //get person entity from state
              let personEntity = this.commonService.getEntitiesById(valueJsonFieldsJSON.EntityId);

              if (personEntity) {
                if (!entityInfo["EntityName"]) {
                  entityInfo["EntityName"] = personEntity["EntityName"];
                  entityInfo["EntityName"] =  nodeData['firstname'] +" "+ nodeData['lastname'];
                }


                if (!entityInfo["company"]) {
                  entityInfo["company"] = personEntity["Parent"]?personEntity["Parent"][0].EntityId:nodeData['Parent'];
                }
                else if (entityInfo["company"] == "" && entityInfo["company"] == undefined && entityInfo["company"] == null) {
                  //get company Office entity from state and update entityInfo object
                  entityInfo["company"] = personEntity["Parent"][0].EntityId?personEntity["Parent"][0].EntityId:nodeData['Parent'];
                }

                let treeNodeData = this.getTreePartialPayload(entityInfo, EntityType);
                let updatedNode = { "EntityId": valueJsonFieldsJSON.EntityId, "NodeData": treeNodeData };
                this.sendDataToAngularTree([TreeTypeNames.PeopleAndCompanies], TreeOperations.updateNodeById, JSON.parse(JSON.stringify(updatedNode)));
                this.partialUpdatePeopleState(entityInfo);
              }
              break;
            case "delete":
              break;
          }
          break;
        default:
          break;
      }
    }


  }

  partialUpdatePeopleState(entity: { EntityId: string, EntityType: string, EntityName: string, Parent: string; }) {
    if (entity && entity.EntityId && entity.EntityType) {
      var statePayload = this.getPeoplePartialStatePayload(entity);
      this.AddorUpdateEntitiyPeopleState(entity.EntityId, statePayload);
    }
  }
  getPeoplePartialStatePayload(entity) {
    var stateJson = {
      "EntityName": entity.EntityName,
      "EntityType": entity.EntityType,
      "EntityId": entity.EntityId,
    };
    switch (entity.EntityType.toLowerCase()) {
      case "company":
        stateJson["CompanyType"] = "";
        stateJson["EntityName"] = stateJson["EntityName"]?stateJson["EntityName"]:entity[entity.EntityType.toLowerCase()]['companyname'];
        break;
      case "office":
        let oParent = {
          EntityType: "Company",
          EntityId: entity.Company//entity.Parent[0].EntityId
        };
        stateJson["Parent"] = [oParent];
        break;
      case "person":
        let pParent = {
          EntityType: "Company",
          EntityId: entity.company//[0].EntityId
        };
        stateJson["Parent"] = [pParent];
        stateJson["EntityName"] = stateJson["EntityName"]?stateJson["EntityName"]:entity[entity.EntityType.toLowerCase()]['companyname'];
        break;
      default: break;
    }
    return stateJson;
  }
  AddorUpdateEntitiyPeopleState(EntityId, EntityData) {
    try {
      EntityData['SG'] = [FABRICS.PEOPLEANDCOMPANIES];
      EntityData["ModifiedDateTime"] = new Date().toISOString();
      this.entityCommonStoreQuery.upsert(EntityId, EntityData);
    }
    catch (e) {
      console.error("People service  AddorUpdateEntitiyState" + e);
    }
  }

  getTreePartialPayload(entityJson, entityType) {
    var treeJson = {
      EntityId: entityJson.EntityId,
      EntityName: entityJson.EntityName,
      EntityType: entityJson.EntityType,
      children: [],
      Icon: this.commonService.getPeopleTreeIcon(entityJson['EntityType'])
    };

    if (entityType == "office") {
      let parent = {
        EntityId: entityJson.Company,
        EntityType: 'Company'
      };

      treeJson["Parent"] = [parent];
    }
    else if (entityType == "person") {
      let parent = {
        EntityId: entityJson.company,
        EntityType: 'Company'
      };

      treeJson["Parent"] = [parent];
    }

    return treeJson;
  }

  onDropDownChangeUpdateLeftTreeNode(entityId, entityInfo, operationType, entityType) {
    let json = JSON.parse(entityInfo.ValueChangedFields);
    let stateEntity = this.commonService.getEntitiesById(entityId);
    var isSubscribeTree = true;
    if (stateEntity) {
      let data = {
        EntityId: entityId,
        EntityName: stateEntity['EntityName'],
        EntityType: stateEntity['EntityType'],
        Parent: '',
        Icon: this.commonService.getPeopleTreeIcon(json['EntityType']),
        children: []
      };

      this.commonService.getTreeAndTreeNodeByEntityId(FABRICS.ENTITYMANAGEMENT, [TreeTypeNames.PeopleAndCompanies], entityId).filter(d => d && isSubscribeTree).subscribe(res => {
        if (entityId && res && res.tree && res.tree.treeModel) {
          let model: TreeModel = res.tree.treeModel;

          switch (entityType) {
            case "person":
              {
                data.Parent = json["Office"];
                let nodeData = {
                  "InsertAtPosition": json["Office"],
                  "NodeData": data
                };
                this.deleteNodeLeftTreePeople(entityId);
                this.sendDataToAngularTree([TreeTypeNames.PeopleAndCompanies], TreeOperations.AddNewNode, JSON.parse(JSON.stringify(nodeData)));
                data["Office"] = json["Office"];
                this.partialUpdatePeopleState(data);

              }
              break;
            case "office":
              {
                data.Parent = json["Company"];
                let nodeData1 = {
                  "InsertAtPosition": json["Company"],
                  "NodeData": data
                };
                model.getNodeBy((nodee) => {
                  if (nodee.data.EntityId == entityId) {
                    let children = nodee.data.children && nodee.data.children.length > 0 ? nodee.data.children : [];
                    data.children = children;
                  }
                });
                this.deleteNodeLeftTreePeople(entityId);
                this.sendDataToAngularTree([TreeTypeNames.PeopleAndCompanies], TreeOperations.AddNewNode, JSON.parse(JSON.stringify(nodeData1)));
                data["Company"] = json["Company"];
                this.partialUpdatePeopleState(data);
              }
              break;
            default: break;
          }
          isSubscribeTree = false;
        }
      });
    }
    else {
      console.error("Entity does not exists in state," + entityId);
    }
  }
  deleteNodeLeftTreePeople(entityId) {
    this.sendDataToAngularTree([TreeTypeNames.PeopleAndCompanies], TreeOperations.deleteNodeById, entityId);
  }


  getEMCurrentProductionDay() {  //Just To Get and Set Current ProductionDay based on timezone...
    try {
      let timeZone = this.commonService.getTimeZoneFromState();
      if (timeZone != undefined && timeZone != "") {
        this.commonService.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/ProductionDate', {}, timeZone).subscribe((res: any) => {
          if (res) {
            this.commonService.EMProductionDate = new Date(res);
            this.commonService.EMProductionDate.setHours(8, 0, 0, 0);
          }
        });
      }
    } catch (e) {
      this.commonService.appLogException(new Error('Exception in getEMCurrentProductionDay() of EMService at time ' + new Date().toString() + '. Exception is : ' + e));

    }
  }
  activateEntityWhenSearchActive(params) {
    this.commonService.searchActive = false;
    var configdata: ITreeConfig = { "treeExpandAll": false };
    this.sendDataToAngularTree(params.tab, TreeOperations.treeConfig, configdata);
    switch (params.tab) {
      case TreeTypeNames.PeopleAndCompanies:
        this.commonService.getEntititesFromStore$("EntityType", [TypeOfEntity.Company, TypeOfEntity.Office, TypeOfEntity.Person], d => true,
          d => { return d; },
          FABRICS.PEOPLEANDCOMPANIES)
          .pipe(untilDestroyed(this, 'destroy')).first()
          .subscribe((filteredentities: any) => {
            this.getAngularTreeEvent([params.tab], TreeOperations.ClearSearch, { Entities: filteredentities, value: '' }).subscribe(res => {
              this.sendDataToAngularTree([params.tab], TreeOperations.ActiveSelectedNode, params.EntityID);
            });
          });
        break;
      case TreeTypeNames.BusinessIntelligence:
        var types = [EntityTypes.Category, EntityTypes.DashBoard];
        this.commonService.getEntititesFromStore$("EntityType", types, d => true,
          d => { return d; },
          FABRICS.BUSINESSINTELLIGENCE)
          .pipe(untilDestroyed(this, 'destroy')).first()
          .subscribe((filteredentities: any) => {
            this.getAngularTreeEvent([params.tab], TreeOperations.ClearSearch, { Entities: filteredentities, value: '' }).subscribe(res => {
              this.sendDataToAngularTree([params.tab], TreeOperations.ActiveSelectedNode, params.EntityID);
            });
          });
        break;
      default:
        break;
    }

  }
  //********************************************************************************** */
  /////////////////////////PEOPLE CREATE AND UPDATE METHODS END
  //********************************************************************************** */

  destroy() { }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.

  }
}
