import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule1 } from '../../../shared/shared1.module';
import { EMGroupTypeComponent } from './em-group-type/em-group-type.component';
import { EntityManagementFormsDeactivateGaurdService } from './entitymanagement-form-can-deactive-guard.dervice';
import { routing } from './entitymanagement.forms.routing';
import { EntityManagementFormTemplate } from './entitymanagement.formtemplate.component';


const EntityManagementFormTemplates = [EntityManagementFormTemplate];

@NgModule({
  imports: [ CommonModule,SharedModule1,routing],
  declarations: [EntityManagementFormTemplates, EMGroupTypeComponent],
    providers:[EntityManagementFormsDeactivateGaurdService]
})

export class EntityManagementFormsModule {


}
