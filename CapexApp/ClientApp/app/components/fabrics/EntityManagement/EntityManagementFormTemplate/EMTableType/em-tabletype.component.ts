import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { WORKER_TOPIC } from '../../../../../../webWorker/app-workers/shared/worker-topic.constants';
import { AppConsts, BiEntityTypes } from '../../../../common/Constants/AppConsts';
import { CommonService } from '../../../../globals/CommonService';
import { Datatypes, FABRICS, FabricsNames, MessageKind } from '../../../../globals/Model/CommonModel';
import { EntityTypes } from '../../../../globals/Model/EntityTypes';
import { Routing } from '../../../../globals/Model/Message';

@Component({
  selector: 'em-table-type',
  templateUrl: './em-tabletype.component.html',
  styleUrls: ['./em-tabletype.component.scss']
})

export class EntityManagementTableTypeComponenet implements OnInit, OnDestroy {

  @Input() EntityId;
  @Input() myForm;
  @Input() tab;
  @Input() EntityType;
  @Input() EntityName;
  @Input() isNewEntity: boolean;
  @Input() IsEntityMgmtDrag: boolean;
  @Input() CapabilityData;
  @Input() IsEntityManagementForm;
  @Input() userId;
  ParentId
  ParentType
  SecurityGroup = [];
  PermissionArray = [];
  Roledata = {"SecurityGroupId":"", "Roles": [], "RemovedRoles": [] };
  constructor(private commonService: CommonService) {}

  ngOnInit() {

      this.ParentId = this.myForm && this.myForm.getRawValue() && this.myForm.getRawValue().ParentId ? this.myForm.getRawValue().ParentId : null;
      this.ParentType = this.myForm && this.myForm.getRawValue() && this.myForm.getRawValue().ParentType ? this.myForm.getRawValue().ParentType : null;
      let payload: any = {}   
      payload.EntityId = this.EntityId
      payload.EntityType = this.EntityType
      payload.Info = 'Details';
      payload.DataType = "SecurityGroup";
      payload.FormCapability = this.tab;
      payload.Fabric = this.commonService.lastOpenedFabric;
      payload.ParentId = this.ParentId;//this.userId;
      payload.CapabilityId = this.commonService.getCapabilityId(this.tab);
      let message = this.commonService.getMessageModel(JSON.stringify(payload), Datatypes.PEOPLE, payload.EntityId, this.EntityType, MessageKind.READ, Routing.OriginSession, payload.Info, FABRICS.PEOPLEANDCOMPANIES);
      this.commonService.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message).subscribe((res: any) => {
      if (res) {
        this.generateSecurityPermissionForm(res);
      }
    });
  }

  generateSecurityPermissionForm(res) {
    try {
      var payload = JSON.parse(res);
      if (payload) {
        payload = JSON.parse(payload.Payload);
        let securitygroupandrole = payload["securitygroupandrole"];
        let usersecuritygroup = payload["usersecuritygroup"] ? payload["usersecuritygroup"] : [];
        if (securitygroupandrole && securitygroupandrole.length > 0) {
          this.SecurityGroup = securitygroupandrole[0]["securitygroup"];
          this.SecurityGroup.sort((firstEntity, secondEntity) => {
            if (firstEntity && secondEntity && firstEntity['EntityName'] && secondEntity['EntityName'])
              return firstEntity['EntityName'].toLowerCase() > secondEntity['EntityName'].toLowerCase() ? 1 : -1
          });
          this.SecurityGroup.forEach(item => {
            var tempobj = item;
            let usersg = usersecuritygroup.find(uitem => uitem.EntityId == tempobj.EntityId)
            if (usersg) {
              tempobj["Checked"] = true;
              tempobj["Role"] = [];
              //tempobj["RolesId"] = [];
              //if (usersg["role"] && usersg["role"].length > 0) {
              //  usersg["role"].forEach(item => {
              //    tempobj["Role"].push(item.EntityId);
              //    tempobj["RolesId"].push(item.EntityId);
              //  })
              //}
            }
            else {
              tempobj["Checked"] = false;
              tempobj["Role"] = [];
              //tempobj["RolesId"] = [];
            }
            if (item["role"] && item["role"].length > 0) {
              item["role"].forEach(subitem => {
                tempobj["Role"].push(subitem.EntityId);
                })
              }
            this.PermissionArray.push(tempobj);
          });
        }
      }
    }
    catch (ex) {

    }
  }

  UpdateSecurityGroupRolebasedonUser(SecurityGroupId, operation,Roledata?) {    
    let Roles = [];
    let RemovedRoles = [];
    let Entities = [];
    let RemovedEntities = [];
    let PeopleCompanies = [];
    let RemovedPeopleCompanies = [];
    switch (operation) {
      case "SecurityGroupSelected": {
        if (this.ParentType == EntityTypes.PERSON) {
          PeopleCompanies.push({ "EntityId": this.userId, "EntityType": EntityTypes.PERSON })
        }
        else if (BiEntityTypes.indexOf(this.ParentType) >= 0) {
          if (this.ParentType == EntityTypes.Category) {
            Entities.push(this.ParentId)
            this.findAllChildrenId(Entities, this.ParentId);
          }
          else {
            Entities.push(this.ParentId)
          }
        }
      }
      break;
      case "SecurityGroupUnSelected": {
        if (this.ParentType == EntityTypes.PERSON) {
          RemovedPeopleCompanies.push({ "EntityId": this.userId, "EntityType": EntityTypes.PERSON })
        }
        else if (BiEntityTypes.indexOf(this.ParentType) >= 0) {
          if (this.ParentType == EntityTypes.Category) {
            RemovedEntities.push(this.ParentId)
            this.findAllChildrenId(RemovedEntities, this.ParentId);
          }
          else {
            RemovedEntities.push(this.ParentId)
          }
        }
      }
      break;
      case "RoleSelectedUnelected": {
        //Roles = Roledata["Roles"];
        //RemovedRoles = Roledata["RemovedRoles"];
      }
      break;
    }
    var payload = {
      "SecurityGroupId": SecurityGroupId, "PeopleCompanies": PeopleCompanies, "Roles": Roles, "RemovedRoles": RemovedRoles, "RemovedPeopleCompanies": RemovedPeopleCompanies, "Entities": Entities, "RemovedEntities": RemovedEntities, "Capability": { "CapabilityId": this.commonService.getCapabilityId(this.tab), "Name": this.tab }
    };
    //this.commonService.sendMessageToServer(JSON.stringify(payload), "SecurityGroups", SecurityGroupId, "SecurityGroup", MessageKind.UPDATE, Routing.AllFrontEndButOrigin, null, FABRICS.FDC);
  }

  ontogglechange(event,index,data) {
    
    if (event.checked) {
      this.UpdateSecurityGroupRolebasedonUser(data.EntityId,"SecurityGroupSelected")
    }
    else {
      //this.Roledata.RemovedRoles = this.SecurityGroup[index]["Role"];
      this.UpdateSecurityGroupRolebasedonUser(data.EntityId, "SecurityGroupUnSelected")
      //this.SecurityGroup[index]["Role"] = [];
      //this.SecurityGroup[index]["RolesId"] = [];
    }
  }

  //onSelctionChange(event, index, data) {
  //  this.Roledata.SecurityGroupId = data.Checked ?  data.EntityId : "";
  //  if (this.SecurityGroup[index]["Role"].length == 0) {
  //    event.value.forEach(item => {
  //      let role = this.Roles.find(litem => litem.EntityId == item);
  //      if (role) {
  //        this.Roledata.Roles.push(role.EntityId);
  //      }
  //    });
  //   // this.SecurityGroup[index]["Role"] = this.Roledata.Roles;
  //  }
  //  else {
  //    event.value.forEach(item => {
  //      let sgrole = this.SecurityGroup[index]["Role"].find(litem => litem == item);
  //      if (!sgrole) {
  //        let role = this.Roles.find(litem => litem.EntityId == item);
  //        if (role) {
  //          this.Roledata.Roles.push(role.EntityId);
  //         // this.SecurityGroup[index]["Role"].push(role.EntityId); 
  //        }
  //      }
  //    });
  //    this.SecurityGroup[index]["Role"].forEach(item => {
  //      let sgrole = event.value.find(litem => litem == item);
  //      if (!sgrole) {
  //        let role = this.Roles.find(litem => litem.EntityId == item);
  //        if (role) {
  //          this.Roledata.RemovedRoles.push(role.EntityId);
  //         // this.SecurityGroup[index]["Role"].splice(index, 1); 
  //        }
  //      }
  //    });
  //  }
    
  //}

  //transformicon(event: any,index) {   
  //  if (!event) {
  //    if (!this.Roledata.SecurityGroupId) {
  //      if (this.Roledata.Roles.length > 0) {
  //        this.entitymgmtService.openCommonAttentionPopup("Please Select SecurityGroup!");
  //      }
  //      this.Roledata = { "SecurityGroupId": "", "Roles": [], "RemovedRoles": [] };
  //      this.SecurityGroup[index]["Role"] = [];
  //      this.SecurityGroup[index]["RolesId"] = [];
  //    }
  //    else {
  //      this.SecurityGroup[index]["Role"] = this.Roledata.Roles;
  //      this.UpdateSecurityGroupRolebasedonUser(this.Roledata.SecurityGroupId, "RoleSelectedUnelected", this.Roledata)
  //    }
  //  }
  //  else {
  //    this.Roledata = { "SecurityGroupId": "", "Roles": [], "RemovedRoles": [] };
  //  }
  //}

  findAllChildrenId(Entities, ParentId) {
    let childrensData = this.commonService.getEntitiesByParent(ParentId, undefined, d => true, (d) => { return d }, FABRICS.BUSINESSINTELLIGENCE);
    childrensData.forEach(item => {
      if (item.EntityType == EntityTypes.Category) {
        Entities.push(item.EntityId);
        this.findAllChildrenId(Entities, item.EntityId);
      }
      else {
        Entities.push(item.EntityId);
      }
    })
  }

  ngOnDestroy() {

  }

} 
