import { Routes, RouterModule } from '@angular/router';
import {EntityManagementFormTemplate} from '../EntityManagementFormTemplate/entitymanagement.formtemplate.component';
import { EntityManagementFormsDeactivateGaurdService } from './entitymanagement-form-can-deactive-guard.dervice';
// import { FdcGlobalEntityComponent } from '../../../fabrics/FDC/components/FDCGlobalFormTemplate/FDCGlobalForm/fdc-globalform.component';
// import { FDCAnalysisFormComponent } from '../../../fabrics/FDC/components/FDCGlobalFormTemplate/FDCAnalysisForm/FDCAnalysisForm.component';

const routes: Routes = [
    {
    path: '', component: EntityManagementFormTemplate,canDeactivate: [EntityManagementFormsDeactivateGaurdService], children: [
      // {
      //   path: 'globalform',
      //   component: FdcGlobalEntityComponent
      // },
      // {
      //   path: 'analysisadmin',
      //   component: FDCAnalysisFormComponent, children: [
      //     {
      //       path: 'globalform',
      //       component: FdcGlobalEntityComponent
      //     }

      //   ]
      // }
    ]
    }
];

export const routing = RouterModule.forChild(routes);
