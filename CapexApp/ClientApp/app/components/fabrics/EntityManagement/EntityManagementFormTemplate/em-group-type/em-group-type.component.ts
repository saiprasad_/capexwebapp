import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'ClientApp/app/components/globals/CommonService';
import { FABRICS, MessageKind, TreeTypeNames } from 'ClientApp/app/components/globals/Model/CommonModel';
import { EntityTypes } from 'ClientApp/app/components/globals/Model/EntityTypes';
import { MessageModel } from 'ClientApp/app/components/globals/Model/Message';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { EntityManagementService } from '../../services/EntityManagementService';
@Component({
  selector: 'em-group-type',
  templateUrl: './em-group-type.component.html',
  styleUrls: ['./em-group-type.component.scss']
})
export class EMGroupTypeComponent implements OnInit {
@Input() myForm: FormGroup
@Input() entityMgmtCommonInputVariable;

matIcon
icon
ExpandSingle_Icon_Idle = "V3 ToggleDown";
CollapsSingle_Icon_Idle = "ExpandAccordion";
expandCollapse:boolean;
groupName='listOfChildren'
isChild:boolean = false;
RouteParams:any={}
takeUntilDestroyObservables=new Subject();

  constructor(private entitymgmtService : EntityManagementService,private commonService:CommonService,private formBuilder:FormBuilder,private route:ActivatedRoute) {
    this.entitymgmtService.EntityMgmtCommonObservable.pipe(
      filter((data: any) =>
         data.MessageKind == MessageKind.DELETE)).pipe(this.compUntilDestroyed())
      .subscribe((message: MessageModel) => {
        this.deleteOperation(message);
      });
   }

  ngOnInit() {
    this.route.queryParams.pipe(this.compUntilDestroyed()).subscribe((params:any) => {
      try {            
        if(Object.keys(this.RouteParams).length==0|| this.RouteParams.expand!= params.expand /*&& this.commonService.isRefreshedUrl*/){
          this.expandCollapse=JSON.parse(params.expand)
          this.matIcon = this.expandCollapse ? this.ExpandSingle_Icon_Idle : this.CollapsSingle_Icon_Idle;
          this.RouteParams=JSON.parse(JSON.stringify(params))
        }
      } catch (e) {
        this.commonService.appLogException(new Error('Exception inroute.queryParams subscriber of constructor of dataentry.component in  DataEntryFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
      }
    })
    this.initializeForm()
  }
  expandPannel(expand){
    try {
      this.expandCollapse=expand
      this.matIcon = expand ? this.ExpandSingle_Icon_Idle : this.CollapsSingle_Icon_Idle;
    } catch (error) {
      console.error()
    }
  }
  initializeForm(){
    this.icon = this.myForm.getRawValue().EntityType;
    this.myForm.addControl(this.groupName, this.formBuilder.array([]));
    const control = this.myForm.controls[this.groupName] as FormArray;
    let ParentOfGroup=this.myForm.getRawValue()
    switch (ParentOfGroup.tab) {
      case TreeTypeNames.PeopleAndCompanies:
        this.commonService.getPeopleEntitiesFromStoreOrBackend$("EntityType", [EntityTypes.OFFICE],d=>true, d => {
          return { "EntityId": d.EntityId, "EntityType": d.EntityType, "EntityName": d.EntityName,Parent:d.Parent }
        }, FABRICS.PEOPLEANDCOMPANIES).pipe(this.compUntilDestroyed()).subscribe((childrensData:any) => {
          if (childrensData) {
            childrensData = JSON.parse(JSON.stringify(childrensData))
            childrensData=childrensData.filter(d => d.Parent&&d.Parent[0]&&d.Parent[0].EntityId==ParentOfGroup.Company)
            if(childrensData.length > 0){
              this.isChild = true;
              childrensData.forEach(ChildrenEntity => {
              control.push(this.formBuilder.group({ "EntityName": ChildrenEntity.EntityName, "EntityType": EntityTypes.OFFICE, "EntityId": ChildrenEntity.EntityId,Parent:ParentOfGroup.Company }))
            });
          }}
        });
        break;
    
      default:
        break;
    }
  }
  entityMagntCommonOutputEmitter(data) {
    try {
      switch (data.OperationType) {
        case 'Delete':
          let payload: any = {
            "Fabric": FABRICS.PEOPLEANDCOMPANIES,
            "EntityID": data.Payload.EntityId,
            "Payload": data.Payload
          };
          this.deleteOperation(payload);
          break;
        default:
          break;
      }
    } catch (error) {

    }
  }
  deleteOperation(message){
    //var payload = JSON.parse(message.Payload);
    switch (message.Fabric.toUpperCase()) {
      case FABRICS.PEOPLEANDCOMPANIES.toUpperCase():
       if (this.myForm && this.myForm.controls && this.myForm.controls.listOfChildren) {
          let controlArray: FormArray = this.myForm.controls[this.groupName] as FormArray;
          this.myForm.controls.listOfChildren['controls'].forEach((control, index) => {
            if (control.value.EntityId == message.EntityID ) {
              controlArray.removeAt(index);
            }
          })
          if(this.myForm.controls.listOfChildren['controls'].length>0)
          this.isChild=true
          else
          this.isChild=false
        }
        break;
      default:
        break;
    }
    
  }
  AddEntity(){
    this.entitymgmtService.isSchemaRead=true;
    const control = this.myForm.controls[this.groupName] as FormArray;
    let ParentOfGroup=this.myForm.getRawValue()
    this.entityMgmtCommonInputVariable.ParentData=ParentOfGroup;
    control.insert(0,this.formBuilder.group({ "EntityName": "", "EntityType": EntityTypes.OFFICE, "EntityId": this.commonService.GetNewUUID(),Parent:ParentOfGroup.EntityId,IsEntityMgmtDrag:true }))
    this.isChild = true;
    this.expandPannel(true)
    
  }
  
compUntilDestroyed():any {
  return takeUntil(this.takeUntilDestroyObservables);
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.takeUntilDestroyObservables.next();
this.takeUntilDestroyObservables.complete();

  }

  ClickEvent(event){
  }
}
