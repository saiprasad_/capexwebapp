import { FormGroup } from "@angular/forms";
import { Subject } from "rxjs";
import { CommonService } from "../../../globals/CommonService";
import { EntityManagementService } from '../services/EntityManagementService'
import { FDCFormGeratorCommon } from "ClientApp/app/components/common/services/FDCFormGeneratorCommonService/FDCFormGeratorCommon";

export abstract class IEntityInterface {
    isFdcAdmin;
    jsonData;
    properties;
    jsonAdminData;
    EntityName;
    EntityType;
    EntityId;
    emService: EntityManagementService;
    ValueChangedFields: any ={};
    AuditUnsubscribe: Subject<any>;
    commonservice: CommonService;
    entityIcon;
    EntityTypeValueValidation: boolean;
    NewTableBody = {};
    GlobalUnitsOfMeasurements: any = [];
    //nested accordian
    valueChangesForList = {};
    PreviousOrCurrentLocations: any = {};
    NewtableBodyData = {};
    headerGrid: any;
    ListValueChangesForField={}
    ListValueChangesForDOI = {};
    isValidTrue: boolean = true;
    fdcFormGeratorCommonService: FDCFormGeratorCommon;
    contentGridArray = [];
    headerGridArray = [];
    localcolsdata;
    contentGrid;
    DowntimeReasonChildren;
    TotalWorkingInterest = 0.0000;
    tankLibraryList:any;
    GlobalGeneralSettingsData;
    isvalueChangesPending=false;
    AccordionName
    LocationData
    //formgenerator
    listOfChildren
    isFdcGlobalAdmin: boolean = false;
    globalAdminFormListData = {};
    groupName = "ListsData";
    NestedAccordionsChanges = {};
    nestedAccordionIdsList = [];
    isDelete: boolean = false;
    DeletedList = {};
    formName: any;
    cdRef;
    IsnewList
    TestSelectedSeparator;
    IntermittentData;
    fabricname;
    isUtilFormInitialization = { value: true };
    dialog
    formGeneratorOutputEmitter
    IsEntityMgmtDrag;
    router;
    GlobalParentAccordionName
    public unsubscribe:Subject<any> = new Subject();

    abstract destroyFormInstance(FormGroup:FormGroup);
    abstract initializeEntityHandler();
    abstract NullifyValuechangedFieldsObject();
    abstract formGroupAuditTimeValueChangesSubscription();
    abstract initializeObservable(instance:any,index:number);
    abstract bindDropdownoptions();
    abstract ClickEvent(event,index);
}
