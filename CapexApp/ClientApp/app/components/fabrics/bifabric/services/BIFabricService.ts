//external imports
import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { map,filter, switchMap, tap } from 'rxjs/operators';
import { Subject ,  Observable, of,combineLatest } from 'rxjs';
import { untilDestroyed } from 'ngx-take-until-destroy';

//internal models and constants imports
import { FABRICS, AngularTreeMessageModel, TreeTypeNames, FabricsNames, MessageKind, TreeOperations, ObservableStateMessage, Datatypes } from '../../../globals/Model/CommonModel';
import { EntityTypes, ITreeConfig } from '../../../globals/Model/EntityTypes';
import { IModelMessage, MessageModel } from '../../../globals/Model/Message';
import { FabricRouter } from '../../../globals/Model/FabricRouter';
import { UUIDGenarator } from 'visur-angular-common'
import { Routing } from '../../../globals/Model/Message';
import { WORKER_TOPIC } from 'ClientApp/webWorker/app-workers/shared/worker-topic.constants';
import { UserStateConstantsKeys } from '../../../../../webWorker/app-workers/commonstore/user/user.state.model';
import { AppConsts,FilterEntitiesfor_BI } from '../../../common/Constants/AppConsts';

//service and states imports
import { CommonService } from '../../../globals/CommonService';
import { WebWorkerService } from '../../../globals/components/ngx-web-worker/web-worker.service';
import { StateRecords } from '../../../common/state/common.state';
import { EntityCommonStoreQuery } from '../../../common/state/entity.state.query';
import { AuthCommonStoreQuery } from '../../../common/state/Auth/auth.state.query';
import { EntityFilterStoreQuery } from 'ClientApp/app/components/common/state/EntityFilter/entityfilter.query';

@Injectable()
export class BIFabricService {
  //string type variables
  SelecteddatasourceID: string = null;
  Reqoptions: any;
  dashboardid: any;
  dashboarurl: any;
  selectedDashboard: any;
  RenameContextTemp;
  ContextMenuData;
  nameoflist;
  Category;
  DashBoardUrlTemp;
  SelectHighLight = "";
  baseUrl
  messageforserver: any;


  //boolean type variables
  ChartCreate = false;
  isclickoncreatelist = false;
  Create = false;

  //subject,observables,emitters variables
  DeleteListDialogEmittor: EventEmitter<any> = new EventEmitter();
  public callNewDashboardObservable = new Subject();//used
  public  newdashboardobservable$ = new Subject();
  public loadDashboard = new Subject();
  public sendToBiCreate = new Subject();
  BiCommonObservable = new Subject();
  refreshDshboard = new Subject();
  public BICommonObservable = new Subject();

  //object,array types variables
  permissionScope = {"DashBoard": {}, "Category": {} };
  databoardjson = [];
  datasourcelist = [];
  newdashboarddata = [];

  constructor(private http: HttpClient, private router: Router, public commonService: CommonService, public _webWorkerService: WebWorkerService,public entityFilterStoreQuery:EntityFilterStoreQuery,
    public stateRecords: StateRecords, public entityCommonStoreQuery: EntityCommonStoreQuery, public authCommonStoreQuery: AuthCommonStoreQuery) {
      var Appset = this.commonService.CustomerAppsettings;
      if (Appset.env.UseAsDesktop == true) {
        this.baseUrl = "https://"+this.commonService.tenantName+"." + Appset.env.domain + "/";
      }else{
        this.baseUrl = this.commonService.baseUrl;
      }
    this.workerInit();
    this.initializeTreeWorkersObservables();
    this.observablesubscribtion();  
   
  }

    CallAdminUserCredential(tenantID: string, tenanName: string) {
      try {
        var Appset = this.commonService.CustomerAppsettings;
        if (Appset.env.UseAsDesktop == true) {
          this.baseUrl = "https://"+this.commonService.getauthElement().TenantName +"." + Appset.env.domain + "/";
        }
        var url = this.baseUrl + "api/AlThings/GetAdminToken?tenantID=" + tenantID + "&tenanName=" + tenanName;
        return this.authCommonStoreQuery.select(state => state.ZoomdataToken).filter(d => tenantID != null).pipe(switchMap((hashCache) => {
        const apiCall = this.commonService.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "get", url)
            .pipe(map(res => {
              this.authCommonStoreQuery.Update({ZoomdataToken:'Bearer ' + res});
            }))
        let newState;
        if (hashCache) {
          newState = hashCache;
        }
         return newState ? of(newState) : apiCall;
        }))
    }
    catch (e) {
      console.error('Exception in CallAdminUserCredential() of commonservice at time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }

  observablesubscribtion() {


    this.authCommonStoreQuery.select([state => state.TenantId, state => state.TenantName]).pipe(tap(d =>d), switchMap(q => this.CallAdminUserCredential(q[0], q[1]))).subscribe(val => {
      //let headers = new Headers();
      //headers.append("Authorization", val);
      let headers = { "Authorization": val };
      this.Reqoptions ={};
      this.Reqoptions.headers = headers;
    });

    this.commonService.sendDataFromAngularTreeToFabric.pipe(untilDestroyed(this,'destroy')).pipe(filter((message: any) => this.commonService.lastOpenedFabric == FABRICS.BUSINESSINTELLIGENCE)).subscribe((message) => {
      try {
        let treeName = message.treeName[0];
      
          this.sendToBIFabric(message);
      
      } catch (e) {
        console.error('Exception in sendDataFromAngularTreeToFabric Observable of BIFabricService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
      }
    });

    // this.commonService.otherSessionUpdateList.pipe(untilDestroyed(this, 'destroy')).pipe(filter((message: any) => (message.Fabric.toUpperCase() == "COMMONFABRIC") && (this.commonService.lastOpenedFabric.toUpperCase() == "BIFABRIC"))).subscribe((message: MessageModel) => {
    //   try {
    //     var flag: boolean = false;
    //     let listname = 'Shared';
    //     let payload = JSON.parse(message.Payload);
    //     let categoryData1 = payload.CategorhyData;

    //     if (message.DataType == 'PersonalList' || message.DataType == 'UserList') {
    //       listname = 'Personal';
    //     }
    //     var existing: any = this.commonService.getEntititesFromStore("EntityType", [EntityTypes.List], d => true, d => { return d }, FABRICS.BUSINESSINTELLIGENCE)
    //       .filter(d => { if (d["Parent"][0]["EntityId"] == listname && d["EntityId"] == categoryData1.EntityId && d["children"].filter(obj => obj.EntityId == this.ContextMenuData.EntityId).length >= 0) { return d; } })

    //     let createdPeople;

    //     if (existing.length == 0) {
    //       let fabricLocation: any = this.commonService.getEntitiesById(categoryData1.data[0].EntityId);
    //       if (fabricLocation) {
    //         createdPeople = {
    //           'InsertAtPosition': listname,
    //           'NodeData': {
    //             EntityId: categoryData1.EntityId,
    //             EntityName: categoryData1.EntityName,
    //             EntityType: categoryData1.Type,
    //             children: []
    //           }
    //         }
    //       }
    //       if (fabricLocation) {
    //         createdPeople.NodeData.children = [
    //           {
    //             EntityId: fabricLocation.EntityId,
    //             EntityName: fabricLocation.EntityName,
    //             EntityType: fabricLocation.EntityType,
    //             Dashboardurl: fabricLocation.Dashboardurl
    //           }
    //         ]
    //       }

    //         this.commonService.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.AddNewNode, createdPeople);
    //     }
    //     else {

    //       let fabricLocation: any = this.commonService.getEntitiesById(categoryData1.data[0].EntityId);
    //       if (fabricLocation) {
    //         existing[0].children.push({
    //           EntityId: fabricLocation.EntityId,
    //           EntityName: fabricLocation.EntityName,
    //           EntityType: fabricLocation.EntityType,
    //           Dashboardurl: fabricLocation.Dashboardurl
    //         })
    //       }
    //         this.commonService.sendDataToAngularTree1(TreeTypeNames.LIST, TreeOperations.updateNodeById, { "EntityId": existing[0]["EntityId"], 'NodeData': { 'EntityName': existing[0]["EntityName"] } });
    //     }
    //   }
    //   catch (e) {
    //     console.error('Exception in otherSessionUpdateList subscriber of BIService  at time ' + new Date().toString() + '. Exception is : ' + e);
    //   }

    // });

    this.commonService.sendMessagesToFabricServices.pipe(filter((message: any) => message.Fabric.toUpperCase() == FABRICS.BUSINESSINTELLIGENCE.toUpperCase())).subscribe(async (message: MessageModel) => {
      this.recieveMessageFromMessagingService(message);
    });

  }

  sendToBIFabric(message) {
    try {
      if (message.treePayload && message.treePayload.nodeData) {
        message.treePayload.data = message.treePayload.nodeData.data;
      }
      if (message.treePayload && message.treePayload.data) {
        this.ContextMenuData = message.treePayload.data;
          this.commonService.ContextMenuData = message.treePayload.data;
        if (message.treePayload.data.EntityType == EntityTypes.List) {
          this.commonService.ListContextMenuData = message.treePayload.data;
          }
      }
        switch (message.treeName[0]) {
            case TreeTypeNames.ALL: {
                switch (message.treeOperationType) {
                    case TreeOperations.NodeOnClick: {
              this.singleClickEvent(message);
              break;
                    }
              case TreeOperations.filteroptionjson: {
                this.filteredjsonstructure(message.treeName[0]);
                break;
              }
              case TreeOperations.selectedfilteroption: {
                this.filterTreeData(message);
                break;
              }
              case TreeOperations.RemoveFilter:
                this.RemoveFilterFromTree(message);
                break;
              case TreeOperations.entityFilter:
                this.entityFilter(message);
                break;
                    case TreeOperations.selectedfilteroption: {
              this.createTreeJsonBasedOnType(message);
              break;
                    }
                    case TreeOperations.NodeContextOptionClicked: {
              switch (message.treePayload.optionSelected) {
                case "Rename": {
                  this.BiCommonObservable.next(message);
                  break;
                }
                case "Delete": {
                  if (this.commonService.AllChildrens.length == 0) {
                    this.BiCommonObservable.next(message);
                  } else {
                    this.commonService.DeleteEntityType = '';
                    this.commonService.AllChildrens = [];
                    // this.commonService.AlertDiv = true;
                    this.BiCommonObservable.next(message);

                  }
                  break;
                }
                case "Copy Dashboard": {
                  this.BiCommonObservable.next(message);
                }
                  break;
                case "Create New List": {
                  this.commonService.ContextMenuData = message.treePayload.nodeData.data;
                  this.commonService.createnewlist()
                  break;
                }
                default: 
                  if (message.treePayload.selectedContextMenuOption.text == "category selection") {
                    if (message.treePayload.data["Parent"] != (undefined && null)) {
                     // message.treePayload.data.Parent[0].EntityId = message.treePayload.selectedContextMenuOption.id;
                      delete message.treePayload.data['Parent'];
                      message.treePayload.data["Parent"] = [];
                      message.treePayload.data["Parent"][0] = { EntityId: message.treePayload.selectedContextMenuOption.id, EntityName: message.treePayload.selectedContextMenuOption.displayName };
                    }
                    else {
                      message.treePayload.data["Parent"] = [];
                      message.treePayload.data["Parent"][0] = { EntityId: message.treePayload.selectedContextMenuOption.id, EntityName: message.treePayload.selectedContextMenuOption.displayName };
                    }
                  message.treePayload["EntityId"] =  message.treePayload.data.EntityId;
                    message.treePayload.NodeData = message.treePayload.data;
                  //  -----------------------------------------------------------------------------
                    let existingItem: any = this.commonService.getEntitiesById(message.treePayload.data.EntityId);
                    existingItem = JSON.parse(JSON.stringify(existingItem))
                    existingItem["Parent"] = message.treePayload.data["Parent"];
                    this.entityCommonStoreQuery.upsert(message.treePayload.data.EntityId, existingItem);
                  //  State Update
                   // ---------------------------------------------------------------------------------
                  var messages = this.commonService.getMessage();
                  var modifiedBy = this.commonService.currentUserName;
                  var currentDateTime = new Date();
                  var date = currentDateTime.toUTCString();
                  var modified = date;
                  var datatosend = {
                    "EntityName":  message.treePayload.data.EntityName , 
                    "Modified": modifiedBy  , 
                    "ModifiedBy": modifiedBy,
                    "ModifiedDateTime": modified,
                    "Capability":  message.treePayload.data.Capability ,
                     "EntityType":  message.treePayload.data.EntityType ,
                      "Dashboardurl":  message.treePayload.data.Dashboardurl , 
                      "EntityId":  message.treePayload.data.EntityId , 
                      "Parent Entity":{ "id": message.treePayload.data.Parent[0].EntityId, "text": message.treePayload.data.Parent[0].EntityName } , 
                      "TenantName":  this.commonService.tenantName , "TenantId": this.commonService.tenantID ,
                      "Parent":message.treePayload.data.Parent[0].EntityId 

                  };
                  datatosend["Payload"] = this.appendPayload(datatosend);
                  var pld = {
                    "EntityId":message.treePayload.data.EntityId,  
                    "TenantName":this.commonService.tenantName,
                    "TenantId":this.commonService.tenantID,
                    "EntityType":message.treePayload.data.EntityType,
                    "Payload":JSON.stringify(datatosend)
                      }
                      let imessageModel: IModelMessage = this.commonService.getMessageWrapper({Payload : JSON.stringify(pld),  DataType : Datatypes.BUSINESSINTELLIGENCEENTITIES,EntityType :  message.treePayload.data.EntityType,EntityID :  message.treePayload.data.EntityId,MessageKind : MessageKind.UPDATE});
                      this.commonService.postDataToBackend(imessageModel, AppConsts.UPDATING).subscribe(response => {
                        console.log(response);
                        this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, AppConsts.UPDATED, AppConsts.LOADER_MESSAGE_DURATION);
                      });
                      this.sendDataToAngularTree(TreeTypeNames.ALL, TreeOperations.deleteNodeById, message.treePayload.data.EntityId)

                    this.commonService.sendDataToAngularTree1(TreeTypeNames.ALL, TreeOperations.ReArrange, message.treePayload)
                    if (message.treePayload.data.EntityId == this.SelectHighLight && message.treePayload.data.EntityType == EntityTypes.DashBoard) {
                      this.sendDataToAngularTree(TreeTypeNames.ALL, TreeOperations.ActiveSelectedNode, message.treePayload.data.EntityId)
                    }

                }else{
                var menuData = message.treePayload.selectedContextMenuOption;

                  if (menuData && menuData.parent && menuData.parent == "Shared To") {
                    this.commonService.ValidateListForm(menuData, "Shared To");
                  } else {
                    if (menuData && menuData.id) {
                      this.commonService.ValidateListForm(menuData);
                    }
                  }}
              }
              break;
                    }
                    case TreeOperations.NodeOnContextClickBeforeInit: {
              this.commonService.ContextMenuData = message.treePayload.data;
              this.createContextMenuToAllTab(message);
              break;
            }
          }
          break;
            }
            case TreeTypeNames.LIST: {
                switch (message.treeOperationType) {
                    case TreeOperations.GetFullTreeData: {
             // this.commonService.makeReadRequestForList(false, "Lists");
              var configdata: ITreeConfig = {
                "treeDropable": true
                        }
                        this.sendDataToAngularTree(message.treeName, TreeOperations.treeConfig, configdata);
              break;
                    }
                    case TreeOperations.NodeOnClick: {
                    if (message.treePayload.data&&message.treePayload.data.EntityType != "List")
                        this.singleClickEvent(message);
                    break;
                    }
                    case TreeOperations.NodeContextOptionClicked: {
              this.contextClickEvent(message.treePayload);
              break;
                    }
                    case TreeOperations.NodeOnContextClickBeforeInit: {
              this.generateContextMenuJsonForList(this.commonService.ContextMenuData.EntityType,message);
              break;
                    }
                  case TreeOperations.filteroptionjson: {
                    this.filteredjsonstructure(message.treeName[0]);
                    break;
                  }
                  case TreeOperations.selectedfilteroption: {
                    this.filterTreeData(message);
                    break;
                  }
                  case TreeOperations.RemoveFilter:
                    this.RemoveFilterFromTree(message);
                    break;
                  case TreeOperations.entityFilter:
                    this.entityFilter(message);
                    break;
                    case TreeOperations.NodeOnDrop: {
              this.commonService.moveNodeFromTo(message.treePayload);
            }
          }
          break;
        }
      }
    } catch (e) {
      console.error('Exception in sendToBIFabric() of BIFabricService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }
  filteredjsonstructure(treeName) {
    try {
      let key =this.commonService.capabilityFabricId+"_"+treeName
      this.entityFilterStoreQuery
        .selectEntity(key).first()
        .subscribe(res=>{
        if(res){
          var message: AngularTreeMessageModel = {
            "fabric": this.commonService.lastOpenedFabric,
            "treeName": [this.commonService.treeIndexLabel],
            "treeOperationType": TreeOperations.filteroptionjson,
            "treePayload": JSON.parse(JSON.stringify(res))
          };
          this.commonService.sendDataToAngularTree.next(message);
        }
        else{
              let EntityTypeKeyValue={}
              FilterEntitiesfor_BI.forEach(d=>{EntityTypeKeyValue[d]=true})
              let data={
                EntityTypes:EntityTypeKeyValue,
              }
                let schema=[
                  {
                    "expand":true,
                    "expandIcon":"V3 ToggleDown",
                    "name": "ENTITY TYPES",
                    "actualName":"EntityTypes",
                    "type": "textWithcross",
                  }
                ]
                var message: AngularTreeMessageModel = {
                  "fabric": this.commonService.lastOpenedFabric,
                  "treeName": [this.commonService.treeIndexLabel],
                  "treeOperationType": TreeOperations.filteroptionjson,
                  "treePayload": {schema: schema,data: data,capability: this.commonService.capabilityFabricId,treeName: treeName
                  }
                };
                this.commonService.sendDataToAngularTree.next(message);
             // })
            //})
           
        }
        });

    
      
    }
    catch (e) {
      console.error('Exception in filteredjsonstructure() of FDCService in FDC/Service at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }
  entityFilter(Data :AngularTreeMessageModel){
    var types = [EntityTypes.Category,EntityTypes.DashBoard]
    this.commonService.getEntititesFromStore$("EntityType", types,d=>true,
      d => { return d },
      FABRICS.BUSINESSINTELLIGENCE)
       .pipe(untilDestroyed(this,'destroy')).first()
       .subscribe((filteredentities: any) => {
        this.sendDataToAngularTree(Data.treeName[0], TreeOperations.entityFilter, {Entities:filteredentities,value:Data.treePayload})
      });
  }
  RemoveFilterFromTree(data){
    this.sendDataToAngularTree(data.treeName[0], TreeOperations.RemoveFilter,'');
  }

  filterTreeData(Data){
    let filterData=Data.treePayload;
    let types = [EntityTypes.Category,EntityTypes.DashBoard]
    let EntityType=Object.keys(filterData.EntityTypes).filter(d=>filterData.EntityTypes[d]).reduce((obj, key) => {obj[key] = filterData.EntityTypes[key];return obj;},{});
    let EntityTypeArray=Object.keys(EntityType)
    let filterFunc=d => EntityTypeArray.includes(d.EntityType) ;
    this.commonService.getEntititesFromStore$("EntityType", types,d=>true,
    d => { return d },
    FABRICS.BUSINESSINTELLIGENCE)
      .pipe(untilDestroyed(this,'destroy')).first()
      .subscribe((filteredentities: any) => {
      this.sendDataToAngularTree(Data.treeName[0], TreeOperations.filterTree, {Entities:filteredentities,filterFunc:filterFunc})
    });
  }
  contextClickEvent(data) {
    if (data.optionSelected != "New Location")
      this.commonService.createbyContextmenu = true;

    switch (data.optionSelected) {
        case "Share":
          this.commonService.isSharedListClicked = true;
            this.router.navigate([FABRICS.BUSINESSINTELLIGENCE], { queryParams: { 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });

            setTimeout(() => {
                this.router.navigate([FABRICS.BUSINESSINTELLIGENCE, 'capabilitylistsearch'], { queryParams: { EntityId: data.data.EntityId, 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
        }, 200);
        break;
      case "Delete":
        if (data.data.EntityType == "Lists" || data.data.EntityType == "List") {
          this.DeleteListDialogEmittor.emit(this.ContextMenuData);
        
      }
      break;
      default:break;
    }
  }

  generateContextMenuJsonForList(typeOfEntity: any,data) {

    if (typeOfEntity == "List") {
      let contextMenuOption = [
        {
          "displayName": "Fdc Context Menu",
          "children": [
            { "displayName": "Share", "children": [] },
            { "displayName": "Delete", "children": [] }
          ]
        }
        ]
        this.sendDataToAngularTree(TreeTypeNames.LIST, TreeOperations.treeContextMenuOption, contextMenuOption);
      this.commonService.activeContextMenuNodeId = data.treePayload.data.EntityId;
    }
  }

  createContextMenuToAllTab(message) {
    try {
        let contextMenuOption;
      if (this.commonService.ContextMenuData.EntityType == EntityTypes.Category) {
        let childlist = [];
        if (this.commonService.isCapbilityAdmin() || this.permissionScope.Category["field"]) {
          if (this.commonService.isCapbilityAdmin()) {
            childlist.push({ displayName: 'Rename', children: [] });
          }
          else {
            this.permissionScope.Category["field"].forEach(item => {
              if (item.Name == "EntityName" && item["field|scope"] == "Update") {
                childlist.push({ displayName: 'Rename', children: [] });
              }
            });
          }
        }
        if (this.commonService.isCapbilityAdmin() || (this.permissionScope.Category["Createroles"] && this.permissionScope.Category["Createroles"].length > 0 && this.permissionScope.Category["Createroles"][0].Delete)) {
          childlist.push({ displayName: 'Delete', children: [] });
        }
        contextMenuOption = [{
          displayName: 'bifabric Context Menu',
          children: childlist
        }]
      } else {
        let childlist = [];
        if (this.commonService.isCapbilityAdmin() || this.permissionScope.DashBoard["field"]) {
          if (this.commonService.isCapbilityAdmin()) {
            childlist.push({ displayName: 'Rename', children: [] });
          }
          else {
            this.permissionScope.DashBoard["field"].forEach(item => {
              if (item.Name == "EntityName" && item["field|scope"] == "Update") {
                childlist.push({ displayName: 'Rename', children: [] });
              }
            });
          }
        }
        if (this.commonService.isCapbilityAdmin() || (this.permissionScope.DashBoard["Createroles"] && this.permissionScope.DashBoard["Createroles"].length > 0 && this.permissionScope.DashBoard["Createroles"][0].Create)) {
          childlist.push({ displayName: 'Copy Dashboard', children: [] });
        }
        if (this.commonService.isCapbilityAdmin() || (this.permissionScope.DashBoard["Createroles"] && this.permissionScope.DashBoard["Createroles"].length > 0 && this.permissionScope.DashBoard["Createroles"][0].Delete)) {
          childlist.push({ displayName: 'Delete', children: [] });
        }
        childlist.push({
          displayName: 'Add to list',
          children: [{ displayName: 'Create New List', children: [] }]
        });
        contextMenuOption = [{
          displayName: 'bifabric Context Menu',
          children: childlist
        }]

        var ListData = this.commonService.getCotegoryList(ListData, FABRICS.BUSINESSINTELLIGENCE)
        ListData.forEach(data => {
          let index = contextMenuOption[0].children.findIndex(item => item.displayName == "Add to list");
          if (index >= 0) {
            contextMenuOption[0].children[index].children.push(data)
          }
      })
    }
      var categoryList = this.GetAllCategories()
      if (this.commonService.isCapbilityAdmin() || this.permissionScope[this.commonService.ContextMenuData.EntityType]["field"]) {
        if (this.commonService.isCapbilityAdmin()) {
          this.addmovetoCategoryinContextmenu(categoryList, contextMenuOption);
        }
        else {
          this.permissionScope[this.commonService.ContextMenuData.EntityType]["field"].forEach(item => {
            if (item.Name == "Parent" && item["field|scope"] == "Update") {
              this.addmovetoCategoryinContextmenu(categoryList, contextMenuOption);
            }
          });
        }
      }
      
        this.sendDataToAngularTree(TreeTypeNames.ALL, TreeOperations.treeContextMenuOption, contextMenuOption);
      this.commonService.activeContextMenuNodeId = message.treePayload.data.EntityId;
    } catch (e) {
      console.error('Exception in createContextMenuToAllTab() of BIFabricService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }
  GetAllCategories(){
  var arr = [];
  var self = this;
  let testarray = [self.commonService.ContextMenuData.EntityId];
    if (self.commonService.ContextMenuData["Parent"] && (self.commonService.ContextMenuData["Parent"].length > 0)) { testarray.push(self.commonService.ContextMenuData.Parent[0]["EntityId"]) }
  var catagoriesList = this.commonService.getEntititesBy('EntityType', ['Category'], d => { return d },FABRICS.BUSINESSINTELLIGENCE).map(d => JSON.parse(JSON.stringify(d)));
    catagoriesList.forEach(function (item)  {
      catagoriesList.forEach(function (obj) {
        if (obj["Parent"] && obj["Parent"].length > 0 && testarray.indexOf(obj["Parent"][0]["EntityId"]) != -1 && testarray.indexOf(obj["EntityId"]) == -1) {
          testarray.push(obj["EntityId"]);
        }
      });
    });
    catagoriesList.filter(function (obj1) {
      if (testarray.indexOf(obj1.EntityId) == -1) {
        arr.push({ children: [], countOfChildren: 1, text: "category selection", "displayName": obj1.EntityName, "id": obj1.EntityId });
      }
    });

   return arr;
  }
 
  addmovetoCategoryinContextmenu(categoryList, contextMenuOption){
    if (categoryList.length > 0) {
      contextMenuOption[0].children.push({
        displayName: 'Move To Category',
        children: []
      });
      categoryList.forEach(data => {
        let index = contextMenuOption[0].children.findIndex(item => item.displayName == "Move To Category");
        if (index >= 0) {
          contextMenuOption[0].children[index].children.push(data)
        } 
      })
    }
  }

  findChildrean(EntityId) {
    var tempArray = [];
    try {
      for (var res of this.commonService.LeftTreePayload['children']) {
        if (res.EntityId == EntityId) {
          tempArray = res.children;
          break;
        }
        else {
            if (res.EntityType == EntityTypes.Category)
            tempArray = this.getChildrean(res.children, EntityId);
        }
      }
    } catch (e) {
      console.error('Exception in findChildrean() of BIFabricService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
      return tempArray;
  }

  getChildrean(children, EntityId) {
    var temparr = [];
    try {
      for (var res of children) {
        if (res.EntityId == EntityId) {
          temparr = res.children;
          break;
        }
        else {
            if (res.EntityType == EntityTypes.Category)
            this.getChildrean(res.children, EntityId)
        }
      }
    } catch (e) {
      console.error('Exception in getChildrean() of BIFabricService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
      return temparr;
  }


  singleClickEvent(data: AngularTreeMessageModel) {
      try {
        if (data.treePayload['data']['EntityType'] == EntityTypes.DashBoard) {
          var msg = { "Payload": data.treePayload['data'] }
          if (this.SelectHighLight != data.treePayload['data']['EntityId'])
            this.commonService.selectedNode.next(msg);
        }
        else if (data && data.treePayload && data.treePayload['data'] && data.treePayload['data']['EntityType']) {
          this.sendDataToAngularTree(this.commonService.treeIndexLabel, TreeOperations.deactivateSelectedNode, data.treePayload['data']['EntityId'])
          if (this.SelectHighLight) {
            this.sendDataToAngularTree(this.commonService.treeIndexLabel, TreeOperations.ActiveSelectedNode, this.SelectHighLight)
          }

        }
    } catch (e) {
      console.error('Exception in singleClickEvent() of BIFabricService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  createTreeJsonBasedOnType(message: AngularTreeMessageModel) {
    
    try {
      let treeName = message.treeName[0];
        switch (message.treePayload) {
            case EntityTypes.Category:
            case EntityTypes.DashBoard:
          var treedata = this.commonService.getEntititesBy('EntityType', [message.treePayload], d => { return d })
                    .map(d => JSON.parse(JSON.stringify(d)), FABRICS.BUSINESSINTELLIGENCE);
                this.sendDataToAngularTree(treeName, TreeOperations.filteredTree, treedata)
                break;
            case TreeTypeNames.ALL:
                this.commonService.getTreeDataFromState$(FABRICS.BUSINESSINTELLIGENCE, treeName).first().subscribe((res) => {
                    this.sendDataToAngularTree(treeName, TreeOperations.filteredTree, res)
          })
          break;
            default:
                this.sendDataToAngularTree(treeName, TreeOperations.filteredTree, [])
          break;
      }
    }
    catch (e) {
      console.error('Exception in createTreeJsonBasedOnType() of BIFabricService at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }


  // GetParentChildRelationship(res) {
  //   try {
  //        var tdata = this.gettreeArray(res);
  //        var data = this.returnTreeJson(tdata)
  //       var availableData = data.filter(obj => obj["EntityId"] == "lkjhgfdsa098765");
  //       if (availableData.length != 0) {
  //           var msg: any = {
  //               "fabric": FABRICS.BUSINESSINTELLIGENCE,
  //           "dataType": "treeData",
  //           "tab": "All", "Payload": availableData["0"]
  //         }
  //         this.LeftTreePayload = availableData["0"];
  //         var resList = this.sortBIHerarchyData(this.LeftTreePayload["children"])
  //         return resList;
  //       }
  //   } catch (e) {
  //     console.error('Exception in GetParentChildRelationship() of BIFabricService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
  //   }
  //   }
    // sortBIHerarchyData(BIHierarchydataData1) {
    //   BIHierarchydataData1.sort((firstEntity, secondEntity) => { return firstEntity["EntityName"].toLowerCase() > secondEntity["EntityName"].toLowerCase() ? 1 : -1 })
    //   BIHierarchydataData1.forEach(element => {
    //     if (element.children.length != 0) {
    //       this.sortBIHerarchyData(element.children);
    //     }
    //   });
    //   return BIHierarchydataData1;
    // }
 
  sendDataToAngularTree(treeName1: string, treeOperationType: string, payload: any) {
    try {
      var treeMessage: AngularTreeMessageModel = {
        "fabric": FABRICS.BUSINESSINTELLIGENCE.toLocaleUpperCase(),
        "treeName": [treeName1],
        "treeOperationType": treeOperationType,
        "treePayload": payload
      }
      this.commonService.sendDataToAngularTree.next(treeMessage);
    } catch (e) {
      console.error('Exception in sendDataToAngularTree() of BIFabricService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  // returnTreeJson(list) {
  //   let checklist2 = list;
  //   try {
  //     for (var i = 0; i < list.length; i++) {
  //       var k = i;
  //       for (var j = 0; j < checklist2.length; j++) {
  //         if (list[i].EntityId == "lkjhgfdsa098765")
  //           this.note = k;

  //         if (list[k].EntityId === checklist2[j].Parent[0]["EntityId"] && this.contain(list[k].children, checklist2[j])) {
  //           list[k].children.push(checklist2[j]);
  //         }
  //         if (list[k].children != null) {
  //           if (list[k].children.length > 0) {
  //             if (list[k].EntityId == "lkjhgfdsa098765" && j >= checklist2.length) {
  //               list[k].children = this.returnTreeJson(list[k].children);
  //             } else if (list[k].EntityId != "lkjhgfdsa098765") {
  //               list[k].children = this.returnTreeJson(list[k].children);
  //             }

  //           }
  //         }
  //       }
  //     }
  //   } catch (e) {
  //     console.error('Exception in returnTreeJson() of BIFabricService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
  //   }
  //     return list;
  // }

  // contain(list1, obj) {
  //   var flag = true;
  //   try {
  //     if (list1.length > 0) {
  //       for (var j = 0; j < list1.length; j++) {
  //         if (list1[j].EntityId === obj.EntityId) {
  //           return !flag;
  //         }
  //       }
  //     }
  //   } catch (e) {
  //     console.error('Exception in contain() of BIFabricService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
  //   }
  //   return flag;
  // }





  // @method PostNewDashBoard
  // @description RestAPI-POST request for creating an empty dashboard template.
  PostNewDashBoard(name, message): any {
    // var self = this;
    // this.BiSrcData = true;
    let postdata = { "name": name, "shareState": "VIEW_AND_EDIT", "type": "Dashboard" };
    var zoomdataurl = this.commonService.zoomdataURl + "/api/dashboards";
    var data = JSON.stringify(postdata);
    let body = { "url": zoomdataurl, "zoomdataaccesstoken": this.commonService.getauthElement().ZoomdataToken, "postData": data, "contenttype": "application/vnd.zoomdata.v2+json", "type": "POST" };
    let url = this.baseUrl +"api/AlThings/ZoomdataPostClient";
    this.http.post(url, body).pipe(map((response: any) => {
      // response;
      // var dbid =response;
      this.dashboardid = response['id'];
     var result = { "json": JSON.stringify(response), "message": message };
     this.databoardjson.push(result);
    })).subscribe((result: any) => {
      this.ValidateForm();
      }, err => console.error("Exception:" + err));
  }


  // @method PutNewChartDashBoard
  // @description RestAPI-PUT request for adding new chart into dashboard template.
  PutNewChartDashBoard(postdata): Observable<any> {
    var zoomdataurl = this.commonService.zoomdataURl + "/api/dashboards/" + this.dashboardid;
    var data = JSON.stringify(postdata);
    let body = { "url": zoomdataurl, "zoomdataaccesstoken": this.commonService.getauthElement().ZoomdataToken, "postData": data, "contenttype": "application/vnd.zoomdata.v2+json", "type": "PUT" };
    let url = this.baseUrl + "api/AlThings/ZoomdataPutClient";
    return this.http.put(url, body).pipe(map((response: any) => {
      response;
      return response;
    }));
  }

  // @method DeleteOrPutClient
  // @description RestAPI-PUT request for adding new chart into dashboard template.
  DeleteOrPutClient(zoomdataurl, postdata, type): Observable<any> {
    let body = { "url": zoomdataurl, "zoomdataaccesstoken": this.commonService.getauthElement().ZoomdataToken, "postData": postdata, "contenttype": "application/vnd.zoomdata.v2+json", "type": type };
    let url = this.baseUrl + "api/AlThings/ZoomdataPutClient";
    return this.http.put(url, body).pipe(map((response: any) => {
      response;
      return response;
    }));
  }

  // @method PostRequest
  // @description RestAPI-PUT request for adding new chart into dashboard template.
  PostRequest(zoomdataurl,postdata): Observable<any> {
    var data = JSON.stringify(postdata);
    let body = { "url": zoomdataurl, "zoomdataaccesstoken": this.commonService.getauthElement().ZoomdataToken, "postData": data, "contenttype": "application/vnd.zoomdata.v2+json", "type": "POST" };
    let url = this.baseUrl + "api/AlThings/ZoomdataPostClient";
    return this.http.post(url, body).pipe(map((response: any) => {
      return response;
    }));
  }

 
 // @method getClient
 // @description For HTTPRequest, get client info, userName, password, account details etc 
  getClient(urllink): Observable<any> {
    var url = this.baseUrl + "api/AlThings/GetZoomdataClient?url=" + urllink + "&accesstoken=" + this.commonService.getauthElement().ZoomdataToken;
    return this.commonService.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "get", url).pipe(map((response: any) => {
      response;
      return JSON.parse(response);
    })); 
  }


  createCopyDashboard(urllink, DashboardName): Observable<any> {
    var url = this.baseUrl + "api/AlThings/CreateCopyDashboard?url=" + urllink + "&accesstoken=" + this.commonService.getauthElement().ZoomdataToken + "&dashboardName=" + DashboardName;
    return this.commonService.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "get", url).pipe(map((response: any) => {
      return JSON.parse(response);
    }));
  }

  // @method getdashboardUrl
  // @description For HTTPRequest, get client info, userName, password, account details etc 
  getdashboardUrl(id): Observable<any> {
    var url = this.baseUrl + "api/AlThings/GetDashBoardUrl?id=" + id + "&tenantid=" + this.commonService.tenantID + "&tenantName=" + this.commonService.tenantName;
    return this.commonService.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "get", url).pipe(map((response: any) => {
      return response;
    }));
  }

  // @method DeleteClient
  // @description For HTTPRequest, detelet client info, userName, password, account details etc 
  DeleteClient(url) {
    //let headers = new Headers();
    //headers.append("Authorization", this.commonService.getauthElement().ZoomdataToken );
    let headers = { "Authorization" : this.commonService.getauthElement().ZoomdataToken}
    this.Reqoptions = {};
    this.Reqoptions.headers = headers;
    this.http.delete(url, this.Reqoptions)

      .subscribe((result: any) => {
      }, err => console.error("Exception:" + err));

  }

 // @method UpdateDashBoard
 // @description to Update existing dashboard 
  UpdateDashBoard(postdata, zoomdataurl) {
    var data = JSON.stringify(postdata);
    let body = { "url": zoomdataurl, "zoomdataaccesstoken": this.commonService.getauthElement().ZoomdataToken, "postData": data, "contenttype": "application/vnd.zoomdata.v2+json", "type": "PUT" };
    let url = this.baseUrl + "api/AlThings/ZoomdataPutClient";
    this.http.put(url, body).pipe(map((response: any) => {
      response;
    })).subscribe((res: any) => {
      if (this.selectedDashboard == this.DashBoardUrlTemp && this.commonService.lastOpenedFabric != FabricsNames.ENTITYMANAGEMENT) {
        this.refreshDshboard.next(this.DashBoardUrlTemp);
      }
      this.DashBoardUrlTemp = undefined;

    }, err => console.error("Exception:" + err));
  }

 

  getlistofdatasource(tenantname: any) {
    var path = "/api/sources?fields=name%2Cdescription%2Ctype%2Cenabled%2CsubStorageType%2CstorageConfiguration%2ClinkedSources%2Ccacheable%2CconnectionTypeId%2CfusedAttributes%2CcreatedByUserID";
    let url = this.commonService.zoomdataURl + path;
    this.getClient(url).subscribe((res: any) => {
        var result: string[] = res;
        this.datasourcelist = [];
        if (!result["token"]) {
          result.forEach(res => {
            if (res['enabled'])
              this.datasourcelist.push({ "name": res['name'], "id": res['id'] });
          });
        }
    });
  }

  ValidateForm() {
    try {
      var data = this.commonService.DataSourceSelection;
      var datasourceid;
      var visualid;
      var dashboardjson = [];
      var result = [];
      var guid = UUIDGenarator.generateUUID().toString();
      this.datasourcelist.forEach((res: any) => {
        if (data == res['name']) {
          datasourceid = res['id'];
        }
      });

      if (datasourceid) {
        let url = this.commonService.zoomdataURl + "/api/visdefs/" + datasourceid;
        this.SelecteddatasourceID = datasourceid;
        this.getClient(url).subscribe((response: any) => {
          result = response;
          
            result.forEach(res => {
              if (!visualid && res['type'] == "CUSTOM" && res['name'] == "Pivot Table Chart") {
                visualid = res['visId'];
              }
              else if (!visualid && res['type'] == "PIVOT_TABLE" && res['name'] == "Pivot Table") {
                visualid = res['visId'];
              }
              else if (!visualid && res['type'] == "RAW_DATA_TABLE" && res['name'] == "Raw Data Table") {
                visualid = res['visId'];
              }
            });
            if (visualid) {
              var url = this.commonService.zoomdataURl + "/api/visdefs/" + datasourceid + "/" + visualid;
              this.getClient(url).subscribe((res: any) => {
                var resultarray = this.getChartJson(res, guid);
                if (this.databoardjson.length > 1)
                  this.databoardjson.splice(0, 1);
                dashboardjson = JSON.parse(this.databoardjson[0]['json']);
                dashboardjson['visualizations'] = [resultarray];
                dashboardjson['selectedWidgetId'] = guid;
                dashboardjson['ownerName'] = this.commonService.zoomdataadminusername;
                this.PutNewChartDashBoard(dashboardjson).subscribe((res: any) => {
                  var dburl = this.commonService.zoomdataURl + "/api/dashboards/" + res['id'] + "/key?expirationDate=2118-09-15T19:15:30.000";
                  this.getClient(dburl).subscribe((res: any) => {
                    this.dashboarurl = res['accountId'] + "+" + this.dashboardid + "?__target=embedded&key=" + res['token'];
                    this.commonService.BiObservableCount = 1;
                    if (this.commonService.lastOpenedFabric == FABRICS.ENTITYMANAGEMENT) {
                      this.dashboardCreation(this.dashboarurl);
                    }
                    else {
                      this.callNewDashboardObservable.next(this.dashboarurl);
                    }
                  });
                });
              });
            }
        });
      }

    } catch (e) {
      console.error('Exception in ValidateForm() of BIService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  getChartJson(chartobject,guid) {
    var newJson = new ChartJson();
    newJson.controlsCfg = chartobject.controlsCfg;
    newJson.dashboardLink = { inheritFilterCfg: true };
    newJson.defaultTitle = chartobject.source.sourceName;
    newJson.layout = { "col": 1, "row": 1, "rowSpan": 12, "colSpan": 16 };
    newJson.name = chartobject.name;
    newJson.source = chartobject.source;
    newJson.type = chartobject.type;
    newJson.visId = chartobject.visId;
    newJson.visualName = chartobject.source.sourceName;
    newJson.visVersion = chartobject.lastModifiedDate;
    newJson.widgetId = guid;
    newJson.widgetName = chartobject.source.sourceName;
    return newJson;
  }

  getInfoJson(infoJson) {
    try {
      var info: any = new Payload();
      info.Capability = infoJson["Capability"] != null ? infoJson["Capability"] : this.commonService.capabilityFabricId;
      info.children = [];
      info.EntityType = infoJson["EntityType"]
      info.EntityName = infoJson["EntityName"]
      if (info.EntityType == EntityTypes.DashBoard)
      info.Dashboardurl = infoJson["Dashboardurl"] != null ? infoJson["Dashboardurl"] : '';
      info.EntityId = infoJson["EntityId"]
      if (infoJson["Parent Entity"])
        info.Parent = infoJson["Parent Entity"].toString() != "" ? [{ EntityId: infoJson["Parent Entity"].id, EntityName: infoJson["Parent Entity"].text }] : [{ EntityName: "lkjhgfdsa098765", EntityId: "lkjhgfdsa098765" }];
      info["SG"] = [FABRICS.BUSINESSINTELLIGENCE];
      info["Capabilities"] = this.commonService.capabilityFabricId;
      return info;
    }
    catch (ex) {

    }
  }


  gettreedictionarydata(data) {
    var objArray = [];
    for (var obj of data) {
      let newobj = {};
      newobj['EntityId'] =  obj['EntityId'];
      newobj['EntityName'] =  obj['EntityName'];
      newobj['EntityType'] =  obj['EntityType'] ;
      if (obj['EntityType'] == EntityTypes.DashBoard)
        newobj['Dashboardurl'] = obj['DashBoardURL'] ;
      if (obj['Parent'])
        newobj['Parent Entity'] =  { id: obj['Parent'][0]['EntityId'], text: obj['Parent'][0]['EntityName'] } ;
      else
        newobj['Parent Entity'] =  "" ;

      objArray.push(newobj);
    }
    return objArray;
  }

  // gettreeArray(child) {
  //   let biLeftTreeData = [];
  //   for (var i = 0; i < child.length; i++) {
  //     var info;
  //     if (child[i]["EntityType"] != (undefined && '')) {
  //       info = child[i];
  //       info["children"] = [];
  //       info["Capability"] = this.commonService.capabilityFabricId;
  //       info["Parent"] = child[i]["Parent"] != null ? child[i]["Parent"] : [{ EntityId: "lkjhgfdsa098765", EntityName: "lkjhgfdsa098765" }];
  //       if (info["DashBoardURL"]) {
  //         info["Dashboardurl"] = info["DashBoardURL"];
  //         delete info["DashBoardURL"];
  //       }
  //       biLeftTreeData.push(info);
  //       if (child[i]["EntityType"] == EntityTypes.Category)
  //         this.commonService.CategoryListBI.push({ "Name": child[i]["EntityName"], "Id": child[i]["EntityId"] });
  //       else
  //         this.commonService.DashboardList.push({ "Name": child[i]["EntityName"], "Id": child[i]["EntityId"] });
  //     }

  //   }
  //   var info1 = { ["Capability"]: "", ["children"]: [], ["EntityType"]: "", ["EntityName"]: "lkjhgfdsa098765", ["EntityId"]: "lkjhgfdsa098765", ["Parent"]: [{ EntityId: "", EntityName: "" }]}
  //   biLeftTreeData.push(info1);

  //   return biLeftTreeData;
  // }

  getLeftTreeData() {
    var stateLastModifiedUTCMS = this.commonService.getUsersStateKey(UserStateConstantsKeys.modifiedTime);
    var isdateTime = false;
    var currentTimeUTCMS = Math.floor((new Date()).getTime() / 1000);

    if (stateLastModifiedUTCMS) {
      var windowCloseUTCMS = this.commonService.getUsersStateKey(UserStateConstantsKeys.windowCloseTime);

      if (windowCloseUTCMS && windowCloseUTCMS > stateLastModifiedUTCMS) {//resetting store
        var closeAndModifiedTimeDifference = currentTimeUTCMS - windowCloseUTCMS;
        if (closeAndModifiedTimeDifference > 6000) {
          //clear all store except users store
          this.stateRecords.reSetStore();
          this.commonService.addUserStateKeyValue(UserStateConstantsKeys.viewedFabrics, []);// Reset Viewed Fabrics
        }
        else {
          //based on datetime fetch latest entities
          isdateTime = true;
        }
      }
      else {
        //based on datetime fetch latest entities
        isdateTime = true;
      }
    }
    else {
      isdateTime = true;
    }

    

    setTimeout(() => {
      let isEntitiesExist: boolean = this.commonService.getViewedFabricsAndCheckStatus(FABRICS.BUSINESSINTELLIGENCE);
      let listStatus:boolean = this.commonService.checkListStateStatus("EntityType","List",FABRICS.BUSINESSINTELLIGENCE);
      
      if (!isEntitiesExist) {
        this.leftTreeSendBackend();
        this.ListTreeSendBackEnd();
      }
      else if(!listStatus){
        this.ListTreeSendBackEnd();
      }
      else if (isdateTime) {
       this.dateTimeBasedReadTreeBackend(currentTimeUTCMS);
     }
    }, 10);
  }


  dateTimeBasedReadTreeBackend(currentTimeUTCMS: number) {
    try {
      let isSubscribe = true;
      this.entityCommonStoreQuery
        .selectAll({
          filterBy: (entity: any) => entity && entity.SG && (<Array<any>>entity.SG).indexOf(FABRICS.BUSINESSINTELLIGENCE) >= 0
        })
        .filter(data => data.length != 0 && isSubscribe)
        .map(data => JSON.parse(JSON.stringify(data)))
        .shareReplay(1).first().subscribe(res => {
          var date = this.commonService.maxModifiedDateTimeStateObject(res);

          var last_utc = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
            date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()) / 1000;

          if (last_utc != NaN && currentTimeUTCMS - last_utc > 60) {
            var dateTime = new Date(last_utc * 1000).toISOString().slice(0, 19).replace('T', ' ');
            this.leftTreeSendBackend(dateTime);
          }
          isSubscribe = false;
        });
    }
    catch (e) {
      console.error("BIService dateTimeBasedReadTreeBackend: " + e);
    }
  }

  ListTreeSendBackEnd() {
    var capabilityName = this.commonService.getCapabilityNameBasedOnFabric();
    var userPayload = {
      "Info": EntityTypes.UserList, "isContextMenu": false, "tab": TreeTypeNames.LIST, "CapabilityName": capabilityName
    };

    let message = this.commonService.getMessage();
    message.Payload = JSON.stringify(userPayload);
    message.DataType = TreeTypeNames.LIST;
    message.EntityID = this.commonService.currentUserName;
    message.EntityType = EntityTypes.UserList;
    message.MessageKind = MessageKind.READ;
    message.Routing = Routing.OriginSession;
    message.Type = "Details";
    //this.commonService.statusCheck(message);
    message.Fabric = FABRICS.BUSINESSINTELLIGENCE.toUpperCase();

    this.commonService
      .httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .map(res => {
        let messageData = JSON.parse(res);
        let data = JSON.parse(messageData["Payload"]);
        return data;
      })
      .subscribe((data: any) => {
        this.commonService.addListEntitiesToState(data, FABRICS.BUSINESSINTELLIGENCE);
      });
  }

  leftTreeSendBackend(dateTime?) {
    try {
      if (this.commonService.capabilityFabricId == null || this.commonService.capabilityFabricId == undefined) {
        this.commonService.capabilityFabricId = this.commonService.FabricCapabilityIdAtLoginTime;
      }

      let opsPayload = { "EntityType": "bifabric", "DataType": "bifabric", "Fabric": FABRICS.BUSINESSINTELLIGENCE, "Info": "General", "CapabilityId": this.commonService.capabilityFabricId };
      if (dateTime) {
        opsPayload["ModifiedDateTime"] = dateTime;
      }
      let CaapbilityId = this.commonService.getCapabilityId(FABRICS.BUSINESSINTELLIGENCE);
      let message = this.commonService.getMessage();
        message.Payload = JSON.stringify(opsPayload);
        message.DataType = FABRICS.BUSINESSINTELLIGENCE;
        message.EntityID = FABRICS.BUSINESSINTELLIGENCE;
        message.EntityType = FABRICS.BUSINESSINTELLIGENCE;
        message.MessageKind = MessageKind.READ;
        message.Routing = Routing.OriginSession;
        message.CapabilityId = CaapbilityId;
        message.Type = "Details";
       //this.commonService.statusCheck(message);
        message.Fabric = FABRICS.BUSINESSINTELLIGENCE;

      this.commonService
        .httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
        .filter(data => CaapbilityId).map(res => {
          var messageData = JSON.parse(res);
          let data = JSON.parse(messageData["Payload"]);

          // Desktop Initial Sync Request
          // if (this.commonService.isElectron && this.commonService.onlineOffline && !this.commonService.desktopInitialSyncStatus) {
          //   this.commonAppService.DesktopDataReadForOffline();
          //   this.commonService.desktopInitialSyncStatus = true;
          // }

          return data;
        })
        .subscribe(data => {
          var entities = [...data.categories,
            ...data.dashboards
          ];

          let existingEntities = [];
          let newEntities = [];
          for (var index in entities) {
            if (entities[index]) {
              let entityId = entities[index].EntityId;
              let entityData = this.entityCommonStoreQuery.getEntity(entityId);
              if (entityData) {
                // if BI not exist in SG then add into existing entities list.
                if (entityData["SG"] != undefined) {
                  if (!((<Array<any>>entityData["SG"]).indexOf(FABRICS.BUSINESSINTELLIGENCE) >= 0)) {
                    existingEntities.push(entityId);
                  }
                  continue;
                }
              }
              entities[index]["SG"] = [FABRICS.BUSINESSINTELLIGENCE];
              entities[index]["TypeOf"] = entities[index]["EntityType"];
              if (entities[index]["EntityType"] == "DashBoard") {
                entities[index]["Dashboardurl"] = entities[index]["DashBoardURL"];
                delete entities[index]["DashBoardURL"];
              }
              newEntities.push(entities[index]);
            }
          }

          if (existingEntities.length > 0)
            this.entityCommonStoreQuery.UpdateArrayValue(existingEntities, "SG", FABRICS.BUSINESSINTELLIGENCE);

          if (newEntities.length > 0)
            this.entityCommonStoreQuery.AddAll(newEntities);

          this.commonService.updateViewedFabric(FABRICS.BUSINESSINTELLIGENCE);
        });
    }
    catch (e) {
      console.error('Exception in leftTreeSendBackend() of BIService at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  initializeTreeWorkersObservables() {
    try {

      //bi common Tree
      this.commonService.initializeTreeWorkersObservablesBI();

      let entities$ = this.entityCommonStoreQuery
        .selectAll({
          filterBy: (entity: any) => entity&&[EntityTypes.Category, EntityTypes.DashBoard].indexOf(entity.EntityType) >= 0 && entity.SG && (<Array<any>>entity.SG).indexOf(FABRICS.BUSINESSINTELLIGENCE) >= 0
        })
        .debounceTime(150)
        .filter(data => data.length != 0)
        .map(data => JSON.parse(JSON.stringify(data)))
        .shareReplay(1);

      let listEntities$ = this.entityCommonStoreQuery
        .selectAll({
          filterBy: (entity: any) =>
            entity&&(([EntityTypes.List, EntityTypes.Personal, EntityTypes.Shared, EntityTypes.Carousel].indexOf(entity.EntityType) >= 0) ||
              (entity.Parent && entity.Parent.length > 0 && entity.Parent[0].EntityName == EntityTypes.Carousel)) && entity.SG && (<Array<any>>entity.SG).indexOf(FABRICS.BUSINESSINTELLIGENCE) >= 0
        })
        .debounceTime(150)
        .filter(data => data.length != 0)
        .map(data => JSON.parse(JSON.stringify(data)))
        .shareReplay(1);

      // let lists$ = listEntities$
      //   .combineLatest(entities$.startWith([]))
      //   .map(d => this.commonService.createListEntities(d[0], FABRICS.BUSINESSINTELLIGENCE));
      let lists$ = combineLatest(listEntities$,entities$.startWith([]))
      .map(d => this.commonService.createListEntities(d[0], FABRICS.BUSINESSINTELLIGENCE));

      let model:ObservableStateMessage=  { Capability: FABRICS.BUSINESSINTELLIGENCE, Tree: TreeTypeNames.LIST, observable: lists$.shareReplay(1) };
      this.commonService.addIntoObservableStateService(model);

    } catch (e) {
      console.error(e);
    }
  }


  readAnalyticsJson(message) {
    return this.commonService.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
      .pipe(map(response => {
        return  response;
      }
      ))
  }

  workerInit(): void {
    this.commonService.worker_WS$.filter(data => data &&data.Fabric&&data.Fabric.toLowerCase() == FABRICS.BUSINESSINTELLIGENCE).pipe(untilDestroyed(this, 'destroy')).subscribe((response) => {
      try {
        this.recieveMessageFromMessagingService(response);
      } catch (e) {
      }
    });

  }

 

  recieveMessageFromMessagingService(message: MessageModel) {
    try {
        switch (message.MessageKind.toUpperCase()) {
          case MessageKind.READ: {
          this.BICommonObservable.next(message);
          this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete);
        }
        break;
          case MessageKind.CREATE: {
            if (message.Type != EntityTypes.UserList) {
              this.Create = true;
              if (this.commonService.lastOpenedFabric == FABRICS.ENTITYMANAGEMENT) {
                this.AddBiEnttiyInTree(message);
              }
              else {
                this.sendToBiCreate.next(message);
              }
            }
          if (message.Routing == Routing.OriginSession) {
            this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, "Entity Created",500);
          }
        }
        break;
        case MessageKind.DELETE: {
          if (message.DataType == "DeleteList") {
            this.sendDataToAngularTree(TreeTypeNames.LIST, TreeOperations.deleteNodeById, message.EntityID);
            this.entityCommonStoreQuery.deleteNodeById("EntityId", message["EntityID"]);
          }
          else {
            this.entityCommonStoreQuery.deleteNodeById("EntityId", message["EntityID"]);
            this.commonService.appLeftTree$.next(message);
          }
          if (message.Routing == Routing.OriginSession) {
            this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, "Entity Deleted",500);
          }
        }
        break;
        case MessageKind.PARTIALUPDATE: {

          let resdata = JSON.parse(message["Payload"]);
          let redjson = JSON.parse(resdata["Payload"]);
          // this.BiReceivedName = true;
          //updating entities state
          this.entityCommonStoreQuery.upsert(redjson["EntityId"], this.getInfoJson(redjson));
          if (redjson.EntityType == EntityTypes.DashBoard && this.selectedDashboard == redjson.Dashboardurl) {
            this.DashBoardUrlTemp = redjson.Dashboardurl;
            this.refreshDshboard.next(redjson.Dashboardurl)
          }
          let NodeData = redjson
          let msg = {
            "EntityId": message.EntityID, "NodeData": { "EntityName": NodeData.EntityName }
          };
          this.sendDataToAngularTree(TreeTypeNames.ALL, TreeOperations.updateNodeById, msg)
          this.sendDataToAngularTree(TreeTypeNames.LIST, TreeOperations.updateNodeById, msg)

          this.DashBoardUrlTemp = undefined;
          // this.BiReceivedName = false;
          if (message.Routing == Routing.OriginSession) {
            setTimeout(() => {
              this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, "Entity Updated",500);
            }, 1000);
          }
        }
        break;
        case MessageKind.UPDATE: {
          if (message.EntityType == EntityTypes.UserPreference) {
            this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete);
          }
          else if (message.Routing == Routing.OriginSession) {
            this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, "Entity Updated",500);
          }
        } break;
        default: {
          if (message.EntityType != 'Error' && message.Payload && message.Payload.treePayload) {
            this.sendToBIFabric(message.Payload);
          }
          if (message.Routing == Routing.OriginSession) {
            this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete);
          }
        }
          break;
      }
    }
    catch (e) {
      console.error('Exception in sendMessagesToFabricServices Observable of BIFabricService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }     
  }

  dashboardCreation(res) {
    try {
      if (this.commonService.BiObservableCount != 0) {
        var url = this.commonService.zoomdataURl + "/visualization/" + res;
          var payload = this.messageforserver.Payload;
          var parse = JSON.parse(payload);
          let info = JSON.parse(parse.Payload);
          info.Dashboardurl = url;
          info["Payload"] = this.appendPayload(info);
          parse.Payload = JSON.stringify(info);
          this.messageforserver.Payload = JSON.stringify(parse);
          let imessageModel: IModelMessage = this.commonService.getMessageWrapper( {Payload : this.messageforserver.Payload,DataType : Datatypes.BUSINESSINTELLIGENCEENTITIES,EntityType :  this.messageforserver.EntityType, EntityID :  this.messageforserver.EntityID,MessageKind : MessageKind.CREATE} );
          this.commonService.postDataToBackend(imessageModel, AppConsts.CREATING).subscribe(response => {
            console.log(response);
            this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, AppConsts.CREATED, AppConsts.LOADER_MESSAGE_DURATION);
            this.Create = true;
            this.sendToBiCreate.next(imessageModel);
          });
      }
    } catch (e) {
      console.error('Exception in dashboardCreation observable of BIFabricService  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  AddBiEnttiyInTree(message) {
    var infoJson;
    var data;
    try {
      if (this.Create) {
        data = JSON.parse(message.Payload);
        infoJson = JSON.parse(data.Payload);
        this.Create = !this.Create;
      } else {
        var data = JSON.parse(message);
        infoJson = JSON.parse(data.Payload);
      }

      let info = this.getInfoJson(infoJson);
      this.entityCommonStoreQuery.upsert(info.EntityId, info);
      let NewNode: any = {
        'InsertAtPosition': infoJson["Parent Entity"] != "" ? infoJson["Parent Entity"]["id"] : "althingTreeRoot",
        'NodeData': info
      }
      if (message.Routing == Routing.AllFrontEndButOrigin) {
        this.commonService.sendDataToAngularTree1(TreeTypeNames.BusinessIntelligence, TreeOperations.AddNewNode, NewNode);
      }
      else {
        this.commonService.sendDataToAngularTree1(TreeTypeNames.BusinessIntelligence, TreeOperations.AddNewNode, NewNode);
        this.sendDataToAngularTree(TreeTypeNames.BusinessIntelligence, TreeOperations.ActiveSelectedNode, infoJson.EntityId);
        this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC, 'Settings', infoJson['EntityId'], this.commonService.treeIndexLabel,this.commonService.treeIndexLabel], { queryParams: { 'EntityID': infoJson['EntityId'], 'expand': true, 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable, EMTab: TreeTypeNames.BusinessIntelligence } });
      }

      this.Create = false;
      // this.commonService.AddTemp = false;

    } catch (e) {
      console.error('Exception in sendToBiCreate observable  of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  appendPayload(data){
    let payload = {};
    if(data){
      Object.keys(data).forEach(key =>{
        payload[key.toLowerCase()] = data[key];
      });
    }
    return JSON.stringify(payload);
  }

  destroy() { }

}

class Payload {
  public Capability;
  public children;
  public EntityType;
  public parent;
  public EntityId;
  public EntityName;
  public Icon;

}

class ChartJson {
  public visVersion;
  public visId;
  public name;
  public type;
  public source;
  public controlsCfg;
  public widgetId;
  public layout;
  public widgetName;
  public visualName;
  public defaultTitle;
  public dashboardLink;
}
