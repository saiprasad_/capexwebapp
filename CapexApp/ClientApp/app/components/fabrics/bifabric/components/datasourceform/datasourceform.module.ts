import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule1 } from '../../../../shared/shared1.module';
import { AngularMaterialModule } from '../../../../../app.module'
import { AddDataSourceComponent } from './datasourceform.component';
import { routing } from './datasourceform.routing';

const CAPABILITY_COMPONENTS = [AddDataSourceComponent];

@NgModule({
    imports: [CommonModule, routing, SharedModule1, RouterModule, AngularMaterialModule],
    exports: [CAPABILITY_COMPONENTS]
})

export class DBFormGeneratorModule {

}
