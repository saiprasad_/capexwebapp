import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Output, Renderer2 } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ALREADY_EXIST3, CONFIGURING_DATA_SOURCE, CONTAIN_DASHBOARDS, DASHBOARD_EXISTS, DASHBOARD_UNSUCCESSFULL, DELETE_ENTITY_PLUS_ALL_CHILD, DELETE_ENTITY_PROCEED, MOVE_DELETE_DASHBOARDS, NO_LONGER_AVAILABLE, READ_FORM_PERMISISON, SURE_CLOSE_DASHBOARD } from 'ClientApp/app/components/common/Constants/AttentionPopupMessage';
// import { untilDestroyed } from 'ngx-take-until-destroy';
import { filter } from 'rxjs/Operators';
import { WORKER_TOPIC } from '../../../../../webWorker/app-workers/shared/worker-topic.constants';
import { AppConsts } from '../../../common/Constants/AppConsts';
import { PopUpAlertComponent } from '../../../common/PopUpAlert/popup-alert.component';
import { EntityCommonStoreQuery } from '../../../common/state/entity.state.query';
import { CommonService } from '../../../globals/CommonService';
import { PopupOperation } from '../../../globals/Model/AlertConfig';
import { AngularTreeMessageModel, Datatypes, FABRICS, FabricsNames, MessageKind, TreeOperations, TreeTypeNames } from '../../../globals/Model/CommonModel';
import { EntityTypes } from '../../../globals/Model/EntityTypes';
import { FabricRouter } from '../../../globals/Model/FabricRouter';
import { MessageType, Routing,IModelMessage } from '../../../globals/Model/Message';
import { NewEntityBIComponent } from '../components/newentitybidialog/newentitybi.component';
import { BIFabricService } from '../services/BIFabricService';
import { RenameComponent } from '../../../common/rename-dialog-form/rename-form.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
/**
 * WorkflowFabricComponent
 */
@Component({
  selector: 'bifabric',
  templateUrl: 'bifabric.component.html',
  styleUrls: ['./bifabric.component.styles.scss']
})
//@Directive({
//  selector: '[appIframeTracker]'
//})
export class BIFabricComponent {
  capability = FABRICS.BUSINESSINTELLIGENCE;
  // static _ref: ComponentRef<BIFabricComponent>;
  // checkWidth = false;
  // isSingleProperty = false;
  // tempThis
  // tempNodeData: any;
  // duration = 750;
  // tempnode: any;
  // treeSvg
  // currentWidth
  // currentHeight
  // minimumZoom
  // maximumZoom
  // zoomvar
  // zoom
  // treemap: any
  // root
  // length;
  pageSize = 1;
  // newnodeEntityId
  // i = 0;
  // json
  // currentComponentWidth = null
  // currentComponentLeft = null;
  // currentComponentHeight = null;
  // graphSvg;
  // currentTreeData;
  // treeJsonData
  // graphJsonData;
  // RootEntityId;
  // nodesInformation = [];
  // isTree = false;
  dashboardurls = [];
  currenttab = '';
  /**
   * Reqoptions
   */
  // Reqoptions

  /**
   * Dashboardtoken
   */
  // Dashboardtoken
  /**
   * datasourcelist array of all available data sources in zoomdata
   */
  // datasourcelist = [];
  /**
   * Dashboardlist array of all Dashboards
   */
  // Dashboardlist = [];
  /**
   * 
   */
  SelectedEntityId: any;
  dashboardurl: SafeResourceUrl;
  /**
   * messageforserver contains payload data of NewDashBoard
   */
  /**
   * URL of Dashnoard 
   */
  // DashboardUrl: any;
  /**
   * BI Icon image
   */
  // count = 0;
  ActiveBIIcon;
  
  @Input() debug: boolean;

  @Output() iframeClick = new EventEmitter<ElementRef>();

  // showbutton: any;
  /**
   * DeleteEntityType temporary vari for Noting type of delete request
   */
  // LeftTreeData = [];
  // DeleteEntityType;
  // RenameContextTemp;
  // RenameThis;
  // promise;
  // Personal;
  // RenameContext;
  ShowHide = true;
  // ContentAfterDelete: any;
  // index;
   message: any;
  // biTemp;
  // delete = false;
  DynamicUrl: SafeResourceUrl;
  treeList = [TreeTypeNames.ALL, TreeTypeNames.LIST]
  dialogRef;
  takeUntilDestroyObservables=new Subject();
  constructor(public renderer:Renderer2, public commonService: CommonService, private router: Router,
     public http: HttpClient, public sanitizer: DomSanitizer, public bIFabricService: BIFabricService, private route: ActivatedRoute,
    public dialog: MatDialog, private ref: ChangeDetectorRef, public entityCommonStoreQuery: EntityCommonStoreQuery) {

    this.commonService.lastOpenedFabric = FabricRouter.BUSINESSINTELLIGENCE_FABRIC;
    this.commonService.ContextMenuData = "";
    this.commonService.showRightsideBar = false;
    if (!this.commonService.isCapbilityAdmin()) {
      this.checkbipermission();
    }
    this.bIFabricService.getLeftTreeData();
    
    // if (this.commonService.DeleteEntityType) {
    //   this.DeleteEntityType = this.commonService.DeleteEntityType;
    // }
    // commonService.currentFabricFilterHeader = "Business Intelligence"
    this.ActiveBIIcon = "wwwroot/images/BI Icons V1/BI_AddAnalysis_Icon.svg";
    var DBurl = '';
    this.dashboardurl = sanitizer.bypassSecurityTrustResourceUrl(DBurl);

    // commonService.channelListHideShow = false;
    commonService.lastOpenedFabric = FabricRouter.BUSINESSINTELLIGENCE_FABRIC;
    // commonService.isAssetsPalleteCreated = false;
    // commonService.isPalleteCreated = false;
    commonService.fabric = FabricRouter.BUSINESSINTELLIGENCE_FABRIC;


    bIFabricService.DeleteListDialogEmittor.pipe(this.compUntilDestroyed()).subscribe(res => {
      this.openDeleteDialog(res);
    });


    bIFabricService.sendToBiCreate.pipe(this.compUntilDestroyed()).subscribe((message: any) => {
      var infoJson;
      var data;
      try {
        if (this.bIFabricService.Create) {
          data = JSON.parse(message.Payload);
          infoJson = JSON.parse(data.Payload);
          this.bIFabricService.Create = !this.bIFabricService.Create;
        } else {
          var data = JSON.parse(message);
          infoJson = JSON.parse(data.Payload);
        }

        let info = this.bIFabricService.getInfoJson(infoJson);
        this.entityCommonStoreQuery.upsert(info.EntityId, info);
        let NewNode: any = {
          'InsertAtPosition': infoJson["Parent Entity"] != "" ? infoJson["Parent Entity"]["id"] :"althingTreeRoot",
          'NodeData': info,
          'SortByName': true
        }
        var navurl = '/' + FabricRouter.BUSINESSINTELLIGENCE_FABRIC;
        if (message.Routing == Routing.AllFrontEndButOrigin) {
          this.commonService.sendDataToAngularTree1(TreeTypeNames.ALL, TreeOperations.AddNewNode, NewNode);
        }
        else {
          this.commonService.sendDataToAngularTree1(TreeTypeNames.ALL, TreeOperations.AddNewNode, NewNode);
          if (infoJson.EntityType == 'DashBoard') {
            this.router.navigate([navurl], { queryParams: { 'EntityId': infoJson['EntityId'], 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
            this.bIFabricService.SelectHighLight = infoJson.EntityId;
            this.bIFabricService.sendDataToAngularTree(TreeTypeNames.ALL, TreeOperations.ActiveSelectedNode, infoJson.EntityId);
          }
        }

        this.bIFabricService.Create = false;
        // this.commonService.AddTemp = false;

      } catch (e) {
        console.error('Exception in sendToBiCreate observable  of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
      }
    });

    this.commonService.appLeftTree$.pipe(filter((val: any) => val != null && (val["Fabric"].toUpperCase() == this.commonService.lastOpenedFabric.toUpperCase()))).pipe(this.compUntilDestroyed()).subscribe((res: any) => {
      var payload: any = res.Payload;
      var messageType: any = res.MessageKind;
      var msg: any = {
        "Label": "All",
        "DataType": res.Fabric,
        "Payload": payload,
        "MessageKind": "",
        "EntityId": "",
        "EntityName": ""
      }
      switch (messageType.toUpperCase()) {
        case MessageKind.UPDATE:
        case MessageKind.PARTIALUPDATE: {
          msg.MessageKind = MessageKind.PARTIALUPDATE;
          msg.EntityId = res.EntityId;
          msg.EntityName = res.Name;
          if (res.Routing == Routing.OriginSession) {
            this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, "Entity Updated", 500);
          }
          this.bIFabricService.RenameContextTemp = '';
          this.RenameNode(msg, msg.EntityName);
        } break;
        case MessageKind.DELETE: {
          msg.MessageKind = MessageKind.DELETE;
          msg.EntityId = res.EntityId;
          if (payload != "Deleted") {
            this.deletedNodeOthers(payload);
            let jdata = JSON.parse(payload);
            if (jdata['EntityType'] == EntityTypes.Category) {
              // this.bIFabricService.TreeJsondata = [this.commonService.getEntitiesById[res.EntityId]];
              let childrean = this.bIFabricService.findChildrean(this.bIFabricService.SelectHighLight);
              if (childrean != undefined && childrean.length >0)
                res.EntityId = childrean['EntityId'];
            }
            if (res.EntityId) {
              this.RemoveIframe(res.EntityId);
            }
          }
        } break;
      }

    });


    this.bIFabricService.BiCommonObservable.pipe(this.compUntilDestroyed()).subscribe((message: any) => {
      try {
        if (message.treePayload.optionSelected == "Rename") {
          // var name = message.treePayload.nodeData.data.EntityName;
          let RenameContext = message.treePayload.nodeData.data.EntityName;
          this.bIFabricService.RenameContextTemp = message.treePayload.nodeData.data;
          let alertmessgae = "Rename : " + RenameContext;
          this.openRenameDialog(alertmessgae, message);

        }
        else if (message.treePayload.optionSelected == "Delete") {
          this.DeletePopUpList(message);
        }
        else if (message.treePayload.optionSelected == "Copy Dashboard") {
          let alertmessgae = "Copy Dashboard : " + message.treePayload.nodeData.data.EntityName;
          this.openRenameDialog(alertmessgae, message);
        }
      } catch (e) {
        console.error('Exception in BiCommonObservable observable of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
      }
    });

    /**
     * selectedNode is subscibed:
     * @event: click on dashBoard node in leftTree , called from capabilityTree
     */


    commonService.selectedNode.pipe(this.compUntilDestroyed()).pipe(filter(obj => commonService.lastOpenedFabric == FabricRouter.BUSINESSINTELLIGENCE_FABRIC)).subscribe((res: AngularTreeMessageModel) => {
      try {
        if (this.commonService.isCapbilityAdmin() || (this.bIFabricService.permissionScope.DashBoard["Createroles"] && this.bIFabricService.permissionScope.DashBoard["Createroles"].length > 0 && this.bIFabricService.permissionScope.DashBoard["Createroles"][0].Read)) {
          this.openSelectedDashboard(res);
        }
        else {
          this.bIFabricService.sendDataToAngularTree(TreeTypeNames.ALL, TreeOperations.deactivateSelectedNode, "");
          this.bIFabricService.sendDataToAngularTree(TreeTypeNames.LIST, TreeOperations.deactivateSelectedNode, "");
          this.openDialog(READ_FORM_PERMISISON);
        }
        //}
      } catch (e) {
        console.error('Exception in selectedNode observable of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
      }
    });

    this.bIFabricService.refreshDshboard.pipe(this.compUntilDestroyed()).subscribe((res: any) => {
      try {
        var url = res;
        let token = this.commonService.getauthElement().ZoomdataToken;
        if (token) {
          var spliting: string[] = url.split('=');
          let analyticsurl = spliting[0].split('/');
          url = this.commonService.zoomdataURl + "/" + analyticsurl[4] + "/" + analyticsurl[5]+ "=native&access_token=" + this.replacetoken(token);
        }

        if (res["Payload"] == undefined) {
          this.dashboardurl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
          var Appset = this.commonService.CustomerAppsettings;
          if (Appset.env.name === "DEV" && Appset.env.UseAsDesktop == true) {
            var d1 = document.getElementById('BIFabricDrawingArea');
            if (d1.childNodes.length > 0)
              d1.removeChild(d1.childNodes[0]);
            var DivToAppend = "<webview src='" + url + "' style='max-width: 100%;height: 91%;overflow-y:auto;' id='ifid'> </webview>";
            d1.insertAdjacentHTML('beforeend', DivToAppend);
          }
          var split: string[] = url.split('+');
          this.bIFabricService.dashboardid = split[1].split('?')[0];
        }
      }
      catch (ex) {

      }
    });


    this.bIFabricService.callNewDashboardObservable.pipe(this.compUntilDestroyed()).subscribe((res: any) => {
      try {
        if (this.commonService.BiObservableCount != 0) {
          var url = this.commonService.zoomdataURl + "/visualization/" + res;
          let token = this.commonService.getauthElement().ZoomdataToken;
          if (token) {
            var spliting: string[] = url.split('=');
            let analyticsurl = spliting[0].split('/');
            url = this.commonService.zoomdataURl + "/" + analyticsurl[4] + "/" + analyticsurl[5] + "=native&access_token=" + this.replacetoken(token);
          }
          this.dashboardurl = sanitizer.bypassSecurityTrustResourceUrl(url);
          var Appset = this.commonService.CustomerAppsettings;
          if (Appset.env.name === "DEV" && Appset.env.UseAsDesktop == true) {
            var d1 = document.getElementById('BIFabricDrawingArea');
            if (d1.childNodes.length > 0)
              d1.removeChild(d1.childNodes[0]);
            var DivToAppend = "<webview src='" + url + "' style='max-width: 100%;height: 91%;overflow-y:auto;' id='ifid'> </webview>";
            d1.insertAdjacentHTML('beforeend', DivToAppend);
          }

            var payload = this.bIFabricService.messageforserver.Payload;
            var parse = JSON.parse(payload);
            let info = JSON.parse(parse.Payload);
            var Icon;
            if (parse.EntityType == "DashBoard")
              Icon = "../../../../../appResourceFiles/images/Bi/BI_Circle_Icon_47x47.svg";
            else
              Icon = "https://althingsblob.blob.core.windows.net/icons/BICategory_Icon_47x47.svg";
            info.Dashboardurl = url;
            info ["Payload"] = this.bIFabricService.appendPayload(info);
            parse.Payload = JSON.stringify(info);
            this.bIFabricService.messageforserver.Payload = JSON.stringify(parse);
            let imessageModel: IModelMessage = this.commonService.getMessageWrapper({ Payload: this.bIFabricService.messageforserver.Payload, DataType: Datatypes.BUSINESSINTELLIGENCEENTITIES,EntityType:this.bIFabricService.messageforserver.EntityType,EntityID:this.bIFabricService.messageforserver.EntityID, MessageKind:  MessageKind.CREATE });
            this.commonService.postDataToBackend(imessageModel, AppConsts.CREATING).subscribe(response => {
              console.log(response);
              this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, AppConsts.CREATED, AppConsts.LOADER_MESSAGE_DURATION);
              this.bIFabricService.Create = true; 
              this.bIFabricService.sendToBiCreate.next(imessageModel);
            });
            this.bIFabricService.newdashboarddata[0]['Dashboardurl'] = url;

        }
      } catch (e) {
        console.error('Exception in callNewDashboardObservable observable of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
      }

    });



    this.commonService.addNewEntity$.pipe(this.compUntilDestroyed()).pipe(filter((data1: any) => this.commonService.lastOpenedFabric == FABRICS.BUSINESSINTELLIGENCE)).subscribe((data1: any) => {
      try {
        if(this.commonService.treeIndexLabel!=TreeTypeNames.LIST)
          this.openCreateNewForm();
      } catch (e) {
        console.error('Exception in addNewEntity$ observable of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
      }
    });

    this.route.queryParams.pipe(this.compUntilDestroyed()).subscribe(params => {
      setTimeout(() => {
        if (params['EntityId']) {
          let url = '';
          var data: any = this.commonService.getEntitiesById(params['EntityId']);
          url = data != undefined ? data.Dashboardurl : '';
          if (url) {
            this.bIFabricService.selectedDashboard = url;
            var spliting: string[] = url.split('=');
            let token = this.commonService.getauthElement().ZoomdataToken;
            let duration = 0;
            if (token) {
              let analyticsurl = spliting[0].split('/');
              let viewtype = this.commonService.isCapbilityAdmin() || (this.bIFabricService.permissionScope.DashBoard["Createroles"] && this.bIFabricService.permissionScope.DashBoard["Createroles"].length > 0 && this.bIFabricService.permissionScope.DashBoard["Createroles"][0].Update) ? "native" : "widget";
              url = this.commonService.zoomdataURl + "/" + analyticsurl[4] + "/" + analyticsurl[5] + "=" + viewtype+"&access_token=" + this.replacetoken(token);
            }
            else {
              duration = 1000;
            }
            if (this.currenttab == params['tab'] || this.currenttab == '') {
              setTimeout(() => {
                token = this.commonService.getauthElement().ZoomdataToken;
                if (token) {
                  let analyticsurl = spliting[0].split('/');
                  url = this.commonService.zoomdataURl + "/" + analyticsurl[4] + "/" + analyticsurl[5] + "=native&access_token=" + this.replacetoken(token);
                  if (this.bIFabricService.permissionScope.DashBoard["Createroles"] && this.bIFabricService.permissionScope.DashBoard["Createroles"].length > 0 && !this.bIFabricService.permissionScope.DashBoard["Createroles"][0].Update) {
                    let dashboardId = analyticsurl[5].split("+")[1].split("?")[0];
                    var dburl = this.commonService.zoomdataURl + "/api/dashboards/" + dashboardId + "/key?expirationDate=" + new Date(Date.now() + 6 * 60 * 60 * 1000).toISOString().replace("Z","");
                    this.bIFabricService.getClient(dburl).subscribe((res: any) => {
                      url = this.commonService.zoomdataURl + "/" + analyticsurl[4] + "/" + analyticsurl[5] + "=widget&key=" + res['token'];
                      let urlVar = this.sanitizer.bypassSecurityTrustResourceUrl(url);
                      this.dashboardurl = urlVar;
                      this.activateNodeForActiveDashboard(data['EntityId']);
                    });
                  }
                  else {
                    let urlVar = this.sanitizer.bypassSecurityTrustResourceUrl(url);
                    this.dashboardurl = urlVar;
                    this.activateNodeForActiveDashboard(data['EntityId']);
                  }
                }
              }, duration);
              var Appset = this.commonService.CustomerAppsettings;
              if (Appset.env.name === "DEV" && Appset.env.UseAsDesktop == true) {
                var d1 = document.getElementById('BIFabricDrawingArea');
                if (d1.childNodes.length > 0)
                  d1.removeChild(d1.childNodes[0]);
                var DivToAppend = "<webview src='" + url + "' style='max-width: 100%;height: 91%;overflow-y:auto;' id='ifid'> </webview>";
                d1.insertAdjacentHTML('beforeend', DivToAppend);
              }
            }
            this.bIFabricService.SelectHighLight = data['EntityId'];
          }
        }
        if (params['tab'] && this.currenttab != params['tab']) {
          this.currenttab = params['tab'];
        }
        this.closeDashboard();
      }, 50);
    })

  }



  activateNodeForActiveDashboard(dashboardId) {
    for (let treeName in this.treeList) {
      if (treeName != this.commonService.treeIndexLabel)
        this.bIFabricService.sendDataToAngularTree(this.treeList[treeName], TreeOperations.ActiveSelectedNode, dashboardId);
    }
  }

  openAttentionPopup(res) {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: true,
        content: [SURE_CLOSE_DASHBOARD],
        subContent: [],
        operation: PopupOperation.AlertConfirm
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      var dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
      dialogRef.componentInstance.emitResponse.subscribe((data: any) => {
        this.openSelectedDashboard(res);
      });
      dialogRef.componentInstance.emitCancelResponse.subscribe((data: any) => {
        this.bIFabricService.sendDataToAngularTree(TreeTypeNames.ALL, TreeOperations.deactivateSelectedNode, res["Payload"]['EntityId']);
        this.bIFabricService.sendDataToAngularTree(TreeTypeNames.ALL, TreeOperations.ActiveSelectedNode, this.bIFabricService.SelectHighLight);
      });
    }
    catch (ex) {

    }
  }



  openSelectedDashboard(res) {
    try {
      let dashboardDetails :any = this.commonService.getEntitiesById(res["Payload"].EntityId)
      let url = dashboardDetails != undefined ? dashboardDetails.Dashboardurl : "";
      this.bIFabricService.SelectHighLight = res["Payload"]['EntityId'];
      let split: string[] = url.split('+');
      let baseurl = this.commonService.zoomdataURl+"/api/dashboards/" + split[1].split('?')[0] + "?__target=native&access_token=" + this.replacetoken(this.commonService.getauthElement().ZoomdataToken);
      this.bIFabricService.getClient(baseurl).pipe(this.compUntilDestroyed()).subscribe((response: any) => {
        var navurl = '/' + FabricRouter.BUSINESSINTELLIGENCE_FABRIC;
        this.router.navigate([navurl], { queryParams: { 'EntityId': res["Payload"].EntityId, 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
        this.bIFabricService.selectedDashboard = res["Payload"]['Dashboardurl'];
        this.bIFabricService.dashboardid = split[1].split('?')[0];
      });


    }
    catch (ex) {

    }
  }


  openCreateNewForm() {
    try {
      let matConfig = new MatDialogConfig()
      matConfig.width = '500px';
      matConfig.disableClose = true;

      const dialogRef = this.dialog.open(NewEntityBIComponent, matConfig);

    }
    catch (ex) {
      console.error(ex);
    }

  }

  switchIframe(index) {
    try {
      this.DynamicUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.dashboardurls[index]);
    } catch (e) {
      console.error('Exception in switchIframe() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  indexOf(children, name) {
    try {
      for (let i = 0; i < children.length; i++) {
        if (children[i].data.EntityName === name)
          return i;
      }
    } catch (e) {
      console.error('Exception in indexOf() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
    return -1;

  }

  closeAll() {
    try {
      this.commonService.isUserMenuHeader = false;
      this.commonService.isSubHeaderEnable = false;
    } catch (e) {
      console.error('Exception in closeAll() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  RenameYes(RenameText) {
    try {
      var validate = this.commonService.CheckExistEntityProperty('EntityName', RenameText, FABRICS.BUSINESSINTELLIGENCE);
      if (!validate) {
        this.bIFabricService.RenameContextTemp.EntityName = RenameText;
        this.RenameNode(this.bIFabricService.RenameContextTemp, RenameText)
        // this.commonService.biDeleteCategory = false;
        // this.commonService.AlertDiv = false;
        // this.bIFabricService.Rename = false;
      }
      else {
        // this.commonService.RenameAlert = true;
        this.openDialog(ALREADY_EXIST3(this.bIFabricService.RenameContextTemp.EntityType, RenameText));
      }
    }
    catch (e) {
      console.error('Exception in RenameYes() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  RenameNode(messages, Name) {
    try {
      this.entityCommonStoreQuery.upsert(messages.EntityId, messages);
      if (this.bIFabricService.RenameContextTemp != ('' && undefined && "")) {
        this.editListName(messages);
        this.bIFabricService.RenameContextTemp = '';
      }
    } catch (e) {
      console.error('Exception in RenameNode() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  openDialog(messageToshow): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: false,
        content: [messageToshow],
        subContent: [],
        operation: PopupOperation.Attention
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      this.dialog.open(PopUpAlertComponent, matConfig);
    }
    catch (e) {
      console.error('Exception in openDialog() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  addDashboardToList(value) {
    try {
      if (value && value.id) {
        this.commonService.ValidateListForm(value);
      }
    } catch (e) {
      console.error('Exception in addDashboardToList() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  editListName(data) {
    try {
      var message = this.commonService.getMessage();
      var modifiedBy = this.commonService.currentUserName;
      var currentDateTime = new Date();
      var date = currentDateTime.toUTCString();
      var modified = date;
      var entityType = data.EntityType;
      var entityID = data.EntityId;
      let newEnitiyName = data.EntityName;
      let parentId = data.Parent && data.Parent.length > 0 ? data.Parent[0]["EntityId"] : "";
      let parent = data.Parent && data.Parent.length > 0 ? { "id": data.Parent[0]["EntityId"], "text": data.Parent[0]["EntityName"] } : "";
      var datatosend = {
        "EntityName": newEnitiyName, "Modified": modifiedBy, "ModifiedBy": modifiedBy, "Capability": data.Capability, "EntityType": data.EntityType, "ModifiedDateTime": modified,
        "EntityId": data.EntityId, "Parent Entity": parent,"Parent":parentId,
        "TenantName": data.TenantName, "TenantId": data.TenantId
      };
      if (entityType == EntityTypes.DashBoard) {
        datatosend["Dashboardurl"] = data.Dashboardurl;
      }
      datatosend ["Payload"] = this.bIFabricService.appendPayload(datatosend);
      var jsonString = JSON.stringify(datatosend);
      var payload = { "EntityId": entityID, "TenantName": message.TenantName, "TenantId": message.TenantID, "EntityType": entityType, "Payload": jsonString };
      let imessageModel: IModelMessage = this.commonService.getMessageWrapper({ Payload: JSON.stringify(payload), DataType: Datatypes.BUSINESSINTELLIGENCEENTITIES,EntityType:entityType,EntityID:entityID, MessageKind: MessageKind.UPDATE }      );
      this.commonService.postDataToBackend(imessageModel, AppConsts.UPDATING).subscribe(response => {
        console.log(response);
        this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, AppConsts.UPDATED, AppConsts.LOADER_MESSAGE_DURATION);
      });
      if (entityType != EntityTypes.Category) {
        var split: string[] = data.Dashboardurl.split('+');
        var dashboardid = split[1].split('?')[0];
        var baseurl = this.commonService.zoomdataURl + "/api/dashboards/" + dashboardid;
        this.bIFabricService.getClient(baseurl).pipe(this.compUntilDestroyed()).subscribe((response: any) => {
          response.name = newEnitiyName;
          this.bIFabricService.DashBoardUrlTemp = data.Dashboardurl;
          this.bIFabricService.UpdateDashBoard(response, baseurl);
        });
      }
      var payld = { "EntityId": entityID, "NodeData": data }
      this.bIFabricService.sendDataToAngularTree(TreeTypeNames.ALL, TreeOperations.updateNodeById, payld);
      this.bIFabricService.sendDataToAngularTree(TreeTypeNames.LIST, TreeOperations.updateNodeById, payld);
    }
    catch (e) {
      console.error('Exception in editListName() of BIFabricComponent at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  deletedialog() {
    var message = "";
    var submessage = "";
    try {
      if (!(this.commonService.AllChildrens.length > 1 && this.commonService.DeleteEntityType == 'Category')) {
        message =   NO_LONGER_AVAILABLE(this.commonService.AllChildrens);
      }
      else {
        message =   CONTAIN_DASHBOARDS(this.commonService.AllChildrens[0]);
        submessage = MOVE_DELETE_DASHBOARDS;
      }
      let config = {
        header: 'Delete ' + this.commonService.DeleteEntityType,
        isSubmit: true,
        content: [message],
        subContent: [submessage],
        operation: PopupOperation.DeleteAll,

      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      var dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
      dialogRef.componentInstance.emitResponse.subscribe((data: any) => {
        if (data.DeleteConfigRecord && data.DeleteProdectionRecord) {
          this.DeleteAll()
        }
      });
    }
    catch (e) {
      console.error('Exception in deletedialog() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }


  DeletePopUpList(message) {
    try {
      if (message.treePayload.nodeData.data.children.length != 0) {
        this.commonService.DeleteEntityType = message.treePayload.nodeData.data.EntityType;
        this.commonService.AllChildrens.push(message.treePayload.nodeData.data.EntityName);
        message.treePayload.nodeData.data.children.forEach(child => {
          this.commonService.AllChildrens.push(child.EntityName);
        })
      }
      else {
        this.commonService.DeleteEntityType = message.treePayload.nodeData.data.EntityType;
        this.commonService.AllChildrens.push(message.treePayload.nodeData.data.EntityName);
      }
      this.bIFabricService.RenameContextTemp = message;
      this.deletedialog();
    }
    catch (e) {
      console.error('Exception in DeletePopUpList() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }

  }



  deleteNode(messages:any) {
    let self = this;
    // this.message = messages;
    let nodedata = messages && messages.treePayload && messages.treePayload.nodeData && messages.treePayload.nodeData.data ? messages.treePayload.nodeData.data : {};
    try {
      if (nodedata.children && nodedata.children.length > 0) {
        // var Childreanjson = {};
        // Childreanjson = self.getchildreanjson(self.bIFabricService.SelectHighLight);
        nodedata.children.forEach(child =>{
          var message = self.commonService.getMessage();
          var modifiedBy = self.commonService.currentUserName;
          var currentDateTime = new Date();
          var date = currentDateTime.toUTCString();
          var modified = date;
          var entityType = child["EntityType"];
          var entityID = child["EntityId"];
          if (entityType == "DashBoard") {
            var dashboardURL1 = child["Dashboardurl"];
            var res = dashboardURL1.split("visualization");

            this.commonService.deletedahsoardid = res[0] + "api/dashboards" + res[1];
            self.deleteItemFromDictinary(self.commonService.lastOpenedFabric, entityID);
          }

          var payload = { "EntityId": entityID, "TenantName": message.TenantName, "TenantId": message.TenantID, "EntityType": entityType };

          message.Payload = JSON.stringify(payload);
          let imessageModel: IModelMessage = self.commonService.getMessageWrapper({Payload:JSON.stringify(payload),DataType:Datatypes.BUSINESSINTELLIGENCEENTITIES,EntityType:entityType,EntityID:entityID,MessageKind:MessageKind.DELETE});
          self.commonService.postDataToBackend(imessageModel, AppConsts.DELETING).subscribe(response => {
            console.log(response);
            self.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, AppConsts.DELETED, AppConsts.LOADER_MESSAGE_DURATION);
          });
          child.children.forEach(SubChild =>{
            var message = self.commonService.getMessage();
            var modifiedBy = self.commonService.currentUserName;
            var currentDateTime = new Date();
            var date = currentDateTime.toUTCString();
            var modified = date;
            var entityType = SubChild["EntityType"];
            var entityID = SubChild["EntityId"];
            if (entityType == "DashBoard") {
              var dashboardURL1 = SubChild["Dashboardurl"];
              var res = dashboardURL1.split("visualization");

              this.commonService.deletedahsoardid = res[0] + "api/dashboards" + res[1];
              self.deleteItemFromDictinary(self.commonService.lastOpenedFabric, entityID);
            }

            var payload = { "EntityId": entityID, "TenantName": message.TenantName, "TenantId": message.TenantID, "EntityType": entityType };
            message.Payload = JSON.stringify(payload);
            let imessageModel: IModelMessage = self.commonService.getMessageWrapper( {Payload : JSON.stringify(payload),DataType : Datatypes.BUSINESSINTELLIGENCEENTITIES,EntityType :  entityType,EntityID :  entityID,MessageKind : MessageKind.DELETE});
            self.commonService.postDataToBackend(imessageModel, AppConsts.DELETING).subscribe(response => {
              console.log(response);
              self.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, AppConsts.DELETED, AppConsts.LOADER_MESSAGE_DURATION);
            });
           
          })
        })

        nodedata.Children = null;
      }
      var message = self.commonService.getMessage();
      var modifiedBy = self.commonService.currentUserName;
      var currentDateTime = new Date();
      var date = currentDateTime.toUTCString();
      var modified = date;
      var entityType = nodedata.EntityType;
      var entityID = nodedata.EntityId;

      if (entityType == "DashBoard") {
        var dashboardURL1 = nodedata.Dashboardurl;
        var res = dashboardURL1.split("visualization");
        this.commonService.deletedahsoardid = res[0] + "api/dashboards" + res[1];
        self.deleteItemFromDictinary(self.commonService.lastOpenedFabric, entityID);
      }
      else {
        this.entityCommonStoreQuery.deleteNodeById("EntityId", entityID);
      }

      var payload = { "EntityId": entityID, "TenantName": message.TenantName, "TenantId": message.TenantID, "EntityType": entityType };
      message.Payload = JSON.stringify(payload);
      if (this.bIFabricService.SelectHighLight == entityID) {
        this.bIFabricService.SelectHighLight = "";
      }
      let imessageModel: IModelMessage = self.commonService.getMessageWrapper({Payload:JSON.stringify(payload),DataType:Datatypes.BUSINESSINTELLIGENCEENTITIES,EntityType:entityType,EntityID:entityID,MessageKind:MessageKind.DELETE});
      self.commonService.postDataToBackend(imessageModel, AppConsts.DELETING).subscribe(response => {
        console.log(response);
        self.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, AppConsts.DELETED, AppConsts.LOADER_MESSAGE_DURATION);
      });
      this.bIFabricService.sendDataToAngularTree(TreeTypeNames.ALL, TreeOperations.deleteNodeById, nodedata.EntityId)
      this.bIFabricService.sendDataToAngularTree(TreeTypeNames.LIST, TreeOperations.deleteNodeById, nodedata.EntityId)
    }
    catch (e) {
      console.error('Exception in deleteNode() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  deleteItemFromDictinary(key: string, EntityId: string) {
    try {
      this.RemoveIframe(EntityId);
      this.entityCommonStoreQuery.deleteNodeById("EntityId", EntityId);
    } catch (e) {
      console.error('Exception in deleteItemFromDictinary() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  getchildreanjson(EntityId) {
    let childNode = this.commonService.getEntitiesById(EntityId);
    return childNode;
  }

  deletedNodeOthers(data) {
    try {
      this.bIFabricService.sendDataToAngularTree(TreeTypeNames.ALL, TreeOperations.deleteNodeById, JSON.parse(data).EntityId)
    } catch (e) {
      console.error('Exception in deletedNodeOthers() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  openRenameDialog(data, message) {
    try {
      let result = undefined;
      let matConfig = new MatDialogConfig()
      matConfig.width = '500px';
      matConfig.height = '150px';
      matConfig.disableClose = true;
      matConfig.data = { "Header": data, "Operation": message.treePayload.optionSelected, "ExistingName": message.treePayload.nodeData.data.EntityName, "Parent": message.treePayload.nodeData.data.Parent };
      let dialogRef = this.dialog.open(RenameComponent, matConfig);

      dialogRef.componentInstance.emitResponse.subscribe((status) => {
        if (status) {
          if (message.treePayload.optionSelected == "Rename") {
            this.RenameYes(status);
          }
          else if (message.treePayload.optionSelected == "Copy Dashboard") {
            var found = this.commonService.CheckExistEntityProperty('EntityName', status.Name, FABRICS.BUSINESSINTELLIGENCE);
            if (found) {
              this.openDialog(DASHBOARD_EXISTS(status.Name));
            }
            else {
              var split: string[] = message.treePayload.nodeData.data.Dashboardurl.split('+');
              var olddashboardid = split[1].split('?')[0];
              let messageToDisplay = CONFIGURING_DATA_SOURCE;
              this.openProgressPopup(messageToDisplay);
              //var baseurl = this.commonService.zoomdataURl + "/api/dashboards/export?bookmarkIds=" + dashboardid;
              //this.bIFabricService.createCopyDashboard(baseurl, status).subscribe((response: any) => {
              //  this.dialogRef.close();
              //  this.commonService.sessionSucess = false;
              //  if (response && response.result && response.result == "Not Created") {
              //    this.openDialog("Something Went Wrong, Please Contact to Admin to Dashboard Copy UnSuccessfull");
              //  }
              //  else {

              //    var url = this.commonService.zoomdataURl + "/visualization/" + response['accountId'] + "+" + response['id'] + "?__target=embedded&key=" + response['token'];
              //    let token = this.commonService.getauthElement().ZoomdataToken;
              //    if (token) {
              //      var spliting: string[] = url.split('=');
              //      url = spliting[0] + "=native&access_token=" + this.replacetoken(token);
              //    }

              //    let jsondata = Object.assign({}, message.treePayload.nodeData.data);
              //    jsondata.Dashboardurl = url;
              //    jsondata.EntityName = status
              //    this.sendCopyDashboardRequestToBackend(jsondata);
              //  }
              //}, err => console.error("Exception:" + err));
              try {
                let zoomdataurl: any = this.commonService.zoomdataURl + "/api/dashboards";
                let postdata: any = { "name": status.Name, "shareState": "VIEW_AND_EDIT", "type": "Dashboard" };
                this.bIFabricService.PostRequest(zoomdataurl, postdata).subscribe((response: any) => {
                  if (response) {
                    let parsingconnection: any = response;
                    this.bIFabricService.dashboardid = parsingconnection["id"];
                    zoomdataurl = this.commonService.zoomdataURl + "/api/dashboards/" + olddashboardid;
                    this.bIFabricService.getClient(zoomdataurl).subscribe((response: any) => {
                      if (response) {
                        if (response["visualizations"]) {
                          for (var visualization of response["visualizations"]) {
                            if (visualization["id"]) {
                              visualization["id"] = this.commonService.GetNewUUID().replace(/\-/g, "").substring(0, 24);
                            }
                            if (visualization["controlsCfg"] && visualization["controlsCfg"]["id"]) {
                              visualization["controlsCfg"]["id"] = this.commonService.GetNewUUID().replace(/\-/g, "").substring(0, 24);
                            }
                          }
                          parsingconnection["visualizations"] = response["visualizations"];
                        }
                        if (response["unifiedBarCfgs"]) {
                          for (var cfgs of response["unifiedBarCfgs"]) {
                            if (cfgs["id"]) {
                              cfgs["id"] = this.commonService.GetNewUUID().replace(/\-/g, "").substring(0, 24);
                            }
                          }
                          parsingconnection["unifiedBarCfgs"] = response["unifiedBarCfgs"];
                        }
                        parsingconnection["type"] = "DASHBOARD";
                        if (response["layout"]) {
                          parsingconnection["layout"] = response["layout"];
                        }
                        parsingconnection["description"] = "";
                        parsingconnection["selectedWidgetId"] = this.commonService.GetNewUUID();
                        this.bIFabricService.PutNewChartDashBoard(parsingconnection).subscribe((response: any) => {
                          this.dialogRef.close();
                          this.commonService.sessionSucess = false;
                          if (response && response.result && response.result == "Not Created") {
                            this.openDialog(DASHBOARD_UNSUCCESSFULL);
                          }
                          else {
                            var url = this.commonService.zoomdataURl + "/visualization/" + response['accountId'] + "+" + response['id'] + "?__target=embedded&key=" + response['token'];
                            let token = this.commonService.getauthElement().ZoomdataToken;
                            if (token) {
                              var spliting: string[] = url.split('=');
                              let analyticsurl = spliting[0].split('/'); 
                              url = this.commonService.zoomdataURl + "/" + analyticsurl[4] + "/" + analyticsurl[5] + "=native&access_token=" + this.replacetoken(token);
                            }

                            let jsondata = Object.assign({}, message.treePayload.nodeData.data);
                            jsondata.Dashboardurl = url;
                            jsondata.EntityName = status.Name;
                            jsondata.Parent = status.Parent;
                            this.sendCopyDashboardRequestToBackend(jsondata);
                          }
                        }, err => console.error("PostException:" + err))
                      }
                    }, err => console.error("GetException:" + err));
                  }
                }, err => console.error("PutException:" + err));
              }
              catch (e) {
                this.dialogRef.close();
                this.openDialog(DASHBOARD_UNSUCCESSFULL);
                console.error('Exception in openRenameDialog() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
              }
            }
          }
        }
      });
    }
    catch (e) {
      console.error('Exception in openRenameDialog() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  openProgressPopup(messageToshow): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: false,
        content: [messageToshow],
        subContent: [],
        operation: 'Progressbar'
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      matConfig.id = messageToshow;
      this.commonService.sessionSucess = true;
      this.dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in openAttentionDialog() of BIFabricComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.commonService.addNew$.next(this.commonService.lastOpenedFabric);        
     }, 100);
  }
  ngAfterContentChecked() {
    try {
      
      this.ref.detectChanges();
    }
    catch (e) {
      console.error('Exception in ngAfterContentChecked() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  DeleteAll() {
    try {
      if (this.bIFabricService.isclickoncreatelist == true) {
        this.bIFabricService.isclickoncreatelist = false;
        let id;
        var dateForFDCEntity;
        let newFormDate = new FormControl(new Date());
        let newFormDate1 = newFormDate.value;
        dateForFDCEntity = newFormDate1.toISOString();
        var entitydata = JSON.parse(localStorage.getItem("Categories") ? localStorage.getItem("Categories") : "{}");
        if (entitydata && entitydata.BI) {
          if (this.bIFabricService.nameoflist != null) {
            id = this.commonService.GetNewUUID();
            let Categorydata = { "EntityId": id, "Type": "List", "EntityName": this.bIFabricService.nameoflist, "data": [this.bIFabricService.ContextMenuData.EntityId], Created: dateForFDCEntity }
            entitydata.BI.filter(data => {
              if (data.EntityId == this.bIFabricService.Category) {
                data.list.push(Categorydata)
              }
            });
          }
          var jsonData = JSON.stringify(entitydata);
          localStorage.setItem("Categories", jsonData)
          var payload = { "Categories": jsonData };
          var dateTime = new Date().toISOString().slice(0, 19).replace('T', ' ');
          var userPayload = { "email": this.commonService.username, "cacheName": "TenantDetails", "UpdatePref": payload , "ModifiedDateTime":dateTime};
          this.commonService.sendMessageToServer(JSON.stringify(userPayload), EntityTypes.UserPreference, this.commonService.currentUserName, "UserPreference", MessageKind.UPDATE, Routing.AllFrontEndButOrigin, "Details", FABRICS.COMMON.toUpperCase());
          this.bIFabricService.isclickoncreatelist = false;
          this.bIFabricService.nameoflist = null;
          this.bIFabricService.Category = null;
        }


      }
      else {
        this.deleteNode(this.bIFabricService.RenameContextTemp);
        this.bIFabricService.RenameContextTemp = '';
        this.commonService.BiObservableCount = 1;
        // this.commonService.biDeleteCategory = false;
        this.commonService.AllChildrens = [];
        this.commonService.BiObservableCount = 0;
      }
    }
    catch (e) {
      console.error('Exception in DeleteAll() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }

  }

  RemoveIframe(resid) {
    try {
      var DBurl = '';
      if (this.commonService.deletedahsoardid) {
        var str = this.commonService.deletedahsoardid;
        var res = str.split("+");
        var s = res[0].split("/");
        s.splice(6);
        s = this.commonService.zoomdataURl + "/" + s[4] + "/" + s[5] + "/";
        var url = s + res[1];
        this.bIFabricService.DeleteClient(url);
      }
      if (this.bIFabricService.SelectHighLight == resid) {
        var navurl = '/' + FabricRouter.BUSINESSINTELLIGENCE_FABRIC;
        this.router.navigate([navurl], { queryParams: { 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
        this.dashboardurl = this.sanitizer.bypassSecurityTrustResourceUrl(DBurl);
        var Appset = this.commonService.CustomerAppsettings;
        if (Appset.env.name === "DEV" && Appset.env.UseAsDesktop == true) {
          var d1 = document.getElementById('BIFabricDrawingArea');
          if (d1.childNodes.length > 0)
            d1.removeChild(d1.childNodes[0]);
          var DivToAppend = "<webview src='" + url + "' style='max-width: 100%;height: 91%;overflow-y:auto;' id='ifid'> </webview>";
          d1.insertAdjacentHTML('beforeend', DivToAppend);
        }
      }
    }
    catch (e) {
      console.error('Exception in RemoveIframe() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  closeDashboard() {
    //if (this.commonService.isSecurityAdmin && this.dashboardurl && this.dashboardurl["changingThisBreaksApplicationSecurity"]) {
    //  this.dashboardurl = this.sanitizer.bypassSecurityTrustResourceUrl("");
    //  this.bIFabricService.SelectHighLight = "";
    //}
  }

  ngOnDestroy() {
    try {

      this.commonService.showRightsideBar = true;
      this.bIFabricService.SelectHighLight = "";
      this.takeUntilDestroyObservables.next();
      this.takeUntilDestroyObservables.complete();
    }
    catch (e) {
      console.error('Exception in ngOnDestroy() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }
  ngOnInit(): void {
    this.commonService.changeFavIcon(FABRICS.BUSINESSINTELLIGENCE);
    setTimeout(() => {
      this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, "");
    }, 3000)
  }
  compUntilDestroyed():any {
    return takeUntil(this.takeUntilDestroyObservables);
  }



  private resetFocusOnWindow() {
    setTimeout(() => {
      this.log('reset focus to window');
      window.focus();
    }, 100);
  }

  private log(message: string) {
    if (this.debug) {
    }
  }

  openDeleteDialog(deletedata) {
    let config = {
      header: 'Delete Entity/Data.',
      isSubmit: true,
      content: [DELETE_ENTITY_PLUS_ALL_CHILD],
      subContent: [],
      operation: 'DeleteConfirm',

    };

    if (deletedata.children.length == 0)
      config.content = [DELETE_ENTITY_PROCEED];

    let matConfig = new MatDialogConfig()
    matConfig.data = config;
    matConfig.width = '500px';
    matConfig.disableClose = true;
    var dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
    dialogRef.componentInstance.emitResponse.subscribe((data: any) => {
      this.commonService.deleteRequestForList(deletedata);
    });
  }

  sendCopyDashboardRequestToBackend(data) {
    try {
      let date = new Date().toLocaleDateString();
      var entityID = this.commonService.GetNewUUID();
      var entityType = data.EntityType;
      var fabric = this.commonService.GetFabricName('/' + this.commonService.lastOpenedFabric);
      data.TenantName = this.commonService.tenantName;
      data.TenantId = this.commonService.tenantID;
      data.CreatedBy = this.commonService.CurrentUserEntityName;
      data.ModifiedBy = this.commonService.CurrentUserEntityName;
      data.Modified = date;
      data.Created = date;
      data.EntityId = entityID;
      data.LastChat = this.commonService.localtoTimestamp();

      data["Parent Entity"] = data.Parent && data.Parent.length > 0 ? { "id": data.Parent[0]["EntityId"], "text": data.Parent[0]["EntityName"] } : "";
      data["Parent"] = data.Parent && data.Parent.length > 0 ? data.Parent[0]["EntityId"] : "";
      data ["Payload"] = this.bIFabricService.appendPayload(data);
      var jsonString = JSON.stringify(data);
      var payload = { "EntityId": entityID, "TenantName": this.commonService.tenantName, "TenantId": this.commonService.tenantID, "EntityType": entityType, "Fabric": fabric, "Payload": jsonString };
      var payloadString = JSON.stringify(payload);
      let imessageModel: IModelMessage = this.commonService.getMessageWrapper({Payload : payloadString,DataType : Datatypes.BUSINESSINTELLIGENCEENTITIES,EntityType :  entityType,EntityID :entityID,MessageKind : MessageKind.CREATE} 
        );
      this.commonService.postDataToBackend(imessageModel, AppConsts.CREATING).subscribe(response => {
        console.log(response);
        this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, AppConsts.CREATED, AppConsts.LOADER_MESSAGE_DURATION);
        this.bIFabricService.Create = true;
        this.bIFabricService.sendToBiCreate.next(imessageModel);
      });
    }
    catch(ex){

    }
  }

  replacetoken(token) {
    return token ? token.replace('Bearer ', '') : null;
  }


  checkbipermission() {
    let payloadForPermission: any = { "CapabilityId": this.commonService.getMessage().CapabilityId };
    var message = this.commonService.getMessageModel(JSON.stringify(payloadForPermission), "Authorization", "checkBIPermission", "checkBIPermission", "READ", "OriginSession", "Details", FABRICS.BUSINESSINTELLIGENCE);
    this.commonService.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message).subscribe((res: any) => {
      try {
        if (res) {
          let message = JSON.parse(res);
          let createpermission = false;
          if (message.Payload) {
            let payload = JSON.parse(message.Payload);
            if (payload && payload.Forms && payload.Forms.length > 0) {
              payload.Forms.forEach(item => {
                switch (item.FormName.toLowerCase()) {
                  case EntityTypes.DashBoard.toLowerCase():
                    this.bIFabricService.permissionScope.DashBoard = item;
                    if (item.Createroles && item.Createroles.length > 0 && item.Createroles[0].Create) {
                      createpermission = true;
                    }
                    break;
                  case EntityTypes.Category.toLowerCase():
                    this.bIFabricService.permissionScope.Category = item;
                    if (item.Createroles && item.Createroles.length > 0 && item.Createroles[0].Create) {
                      createpermission = true;
                    }
                    break;
                }
              })
            }
            this.commonService.addNew$.next({ "createpermission": (createpermission ? [{ "Create": true }]  : []) }  );
          }
          
        }
      }
      catch (ex) {

      }
    });
  }

}



export interface CapabilityMessageModelToFabric {
  "Event": string;
  "selctedNode": any;
  "Tab": string;
  "SelectedMenuName"?: string;
  "SelectedMenuData"?: any
}


