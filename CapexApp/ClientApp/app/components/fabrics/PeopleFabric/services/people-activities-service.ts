//external imports
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PopUpAlertComponent } from 'ClientApp/app/components/common/PopUpAlert/popup-alert.component';
import { EntityFilterStoreQuery } from 'ClientApp/app/components/common/state/EntityFilter/entityfilter.query';
import { PopupOperation } from 'ClientApp/app/components/globals/Model/AlertConfig';
import { WORKER_TOPIC } from 'ClientApp/webWorker/app-workers/shared/worker-topic.constants';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { of, Subject } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { AppConsts, peopleEntityTypes } from '../../../common/Constants/AppConsts';
//internal state,service and components imports
import { CommonService } from '../../../globals/CommonService';
//internal constants and models imports
import { AngularTreeMessageModel, FABRICS, MessageKind, TreeOperations, TreeTypeNames, TypeOfEntity } from '../../../globals/Model/CommonModel';
import { EntityTypes } from '../../../globals/Model/EntityTypes';
import { MessageModel, Routing, IModelMessage } from '../../../globals/Model/Message';
import { FDCStateQuery } from '../../../common/state/KeyValueState/fdc.state.query';
import { DataTypes, MessageEntityTypes } from '../../Security/services/constants/message.constants';



@Injectable()
export class PeopleActivitiesService {
  //boolean variables
  contextmenuFlag: boolean = false;
  rightclickNode: boolean = false;
  listcontext: boolean = true;
  forGenDataForCreateOrUpdate;
  IsEntityMgmtDrag;
  EntityType;
  EntityId;
  //string type variables
  peopleFormData;
  ContextMenuData: any;

  //observables and subjects
  PeopleCommonObservable = new Subject();

  constructor(public commonService: CommonService, public entityFilterStoreQuery: EntityFilterStoreQuery, public fdcStateQuery: FDCStateQuery, public dialog: MatDialog) {
    this.commonService.formGenDataForCreateUpdate$.subscribe((res) => {
      this.createPeopleData(res);

    });
    this.workerInit();
  }

  workerInit(): void {

    //tree messages
    this.commonService.sendDataFromAngularTreeToFabric.pipe(filter(message => this.commonService.lastOpenedFabric && this.commonService.lastOpenedFabric.toLocaleUpperCase() == FABRICS.PEOPLEANDCOMPANIES.toLocaleUpperCase()))
      .subscribe((msg: AngularTreeMessageModel) => {
        try {

          this.GetEventFromTree(msg);

        }
        catch (e) {
          console.error('Exception in sendDataFromAngularTreeToFabric subscriber of PeopleFabric in  people/service at time ' + new Date().toString() + '. Exception is : ' + e);
        }
      })


    //socket messages
    this.commonService.worker_WS$.filter(data => data && data.Fabric && data.Fabric.toLowerCase() == FABRICS.PEOPLEANDCOMPANIES.toLowerCase()).subscribe((response) => {
      try {
        this.recieveMessageFromMessagingService(response);
      } catch (e) {
        console.log(e);
      }
    });

    //from services and components messages
    this.commonService.sendMessagesToFabricServices.pipe(filter((message: any) => message.Fabric.toUpperCase() == FABRICS.PEOPLEANDCOMPANIES.toUpperCase() || this.debug(message))).subscribe(async (message: MessageModel) => {
      this.recieveMessageFromMessagingService(message);
    });

  }
  debug(data) {
    let fabric = FABRICS.PEOPLEANDCOMPANIES.toLowerCase() + "_em_person_schedule";
    let msgFabric = data.Fabric.toLowerCase().toString();

    if (msgFabric == fabric.toString()) {
      return true;
    }
    return false;
  }

  recieveMessageFromMessagingService(message: MessageModel) {
    try {
      if (message != null && (message.Fabric.toLowerCase() == FABRICS.PEOPLEANDCOMPANIES.toLowerCase() || this.debug(message))) {
        console.log("Response People Fabric : " + message);
        if (message.DataType == "Error") {
          if (message.MessageKind == MessageKind.CREATE) {
            console.log("Error :" + message.Payload);
          }
        }
        // else if (message.MessageKind.toUpperCase() == MessageKind.PARTIALUPDATE && message.EntityType == "Company" && message.Payload.toLowerCase() != 'success') {
        //   this.PeopleCommonObservable.next(message);
        // }
        // else if (message.MessageKind.toUpperCase() == MessageKind.PARTIALUPDATE && message.EntityType == "Person_ScheduleInvite") {
        //   this.PeopleCommonObservable.next(message);
        // }
        else if (message.MessageKind.toUpperCase() == MessageKind.READ) {
          console.log(message);
          this.PeopleCommonObservable.next(message);
        }
        else if ([MessageKind.CREATE, MessageKind.UPDATE, MessageKind.DELETE, MessageKind.PARTIALUPDATE].includes(message.MessageKind.toUpperCase()) && message.Payload.toLowerCase() != 'success') {
          this.PeopleCommonObservable.next(message);
        }
      }

      if (message.DataType != 'Error')
        setTimeout(() => {
          this.commonService.loadingBarAndSnackbarStatus("complete", "");
        }, 2000);
      else {
        this.commonService.loadingBarAndSnackbarStatus("complete", message.DataType);
        this.commonService.errorDetails = message.Payload;
        console.error(message);
      }
    }
    catch (e) {
      console.log("Exception : " + e)
    }
  }

  openDialog(messageToshow, Data?): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: false,
        content: [messageToshow],
        subContent: [],
        operation: PopupOperation.Attention
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      matConfig.id = messageToshow;
      if (!this.dialog.getDialogById(matConfig.id)) {
        let dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
        dialogRef.afterClosed().subscribe((res) => {
        });
      }
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in openDialog() of People servive in People Fabric at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  GetEventFromTree(data: AngularTreeMessageModel) {
    try {
      let treeName = data.treeName[0];
      if (data.treePayload.data) {
        this.ContextMenuData = data.treePayload.data;
        this.commonService.ContextMenuData = data.treePayload.data;
      }
      switch (treeName) {
        case TreeTypeNames.ALL: {
          switch (data.treeOperationType) {
            case TreeOperations.GetFullTreeData: {
              //this.allTreeDataFromBackend();
              break;
            }
            // case TreeOperations.NodeOnClick: {
            //   this.commonService.selectNode.next(data.treePayload.data.EntityId);
            //   this.singleClickEvent(data);
            //   break;
            // }
            // case TreeOperations.NodeOnDblClick: {
            //   this.doubleClickEventOnRightSideTree(data);
            //   break;
            // }
            case TreeOperations.filteroptionjson: {
              this.filteredjsonstructure(treeName);
              break;
            }
            case TreeOperations.selectedfilteroption: {
              this.filterTreeData(data);
              break;
            }
            case TreeOperations.RemoveFilter:
              this.RemoveFilterFromTree(data);
              break;
            case TreeOperations.entityFilter:
              this.entityFilter(data);
              break;
            // case TreeOperations.selectedfilteroption: {
            //   this.createTreeJsonBasedOnType(data)
            //   break;
            // }
            case TreeOperations.NodeOnContextClickBeforeInit: {
              this.listcontext = true;
              this.generateContextMenuJson(data.treePayload.data);
              break;
            }
            // case TreeOperations.NodeContextOptionClicked: {
            //   this.contextClickEvent(data.treePayload);
            //   break;
            // }
          }
        }

        case TreeTypeNames.LIST:
          console.log('list called')
          break;

        case TreeTypeNames.SEARCH:
          console.log('list called')
          break;
        default:
          break;
      }
    }
    catch (ex) {
      console.log(ex);
    }
  }

  filteredjsonstructure(treeName) {
    try {
      let key = this.commonService.capabilityFabricId + "_" + treeName
      this.entityFilterStoreQuery
        .selectEntity(key).first()
        .subscribe(res => {
          if (res) {
            var message: AngularTreeMessageModel = {
              "fabric": this.commonService.lastOpenedFabric,
              "treeName": [this.commonService.treeIndexLabel],
              "treeOperationType": TreeOperations.filteroptionjson,
              "treePayload": JSON.parse(JSON.stringify(res))
            };
            this.commonService.sendDataToAngularTree.next(message);
          }
          else {
            let EntityTypeKeyValue = {}
            peopleEntityTypes.forEach(d => { EntityTypeKeyValue[d] = false })
            let data = {
              EntityTypes: EntityTypeKeyValue,
            }
            let schema = [
              {
                "expand": true,
                "expandIcon": "V3 ToggleDown",
                "name": "ENTITY TYPES",
                "actualName": "EntityTypes",
                "type": "textWithcross",
              }
            ]
            var message: AngularTreeMessageModel = {
              "fabric": this.commonService.lastOpenedFabric,
              "treeName": [this.commonService.treeIndexLabel],
              "treeOperationType": TreeOperations.filteroptionjson,
              "treePayload": {
                schema: schema, data: data, capability: this.commonService.capabilityFabricId, treeName: treeName
              }
            };
            this.commonService.sendDataToAngularTree.next(message);
            // })
            //})

          }
        });



    }
    catch (e) {
      console.error('Exception in filteredjsonstructure() of FDCService in FDC/Service at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }
  entityFilter(Data: AngularTreeMessageModel) {
    var types = [TypeOfEntity.Company, TypeOfEntity.Office, TypeOfEntity.Person]

    this.commonService.getEntititesFromStore$("EntityType", types, d => true,
      d => { return d },
      FABRICS.PEOPLEANDCOMPANIES)
      .pipe(untilDestroyed(this, 'destroy')).first()
      .subscribe((filteredentities: any) => {
        this.sendDataToAngularTree(Data.treeName[0], TreeOperations.entityFilter, { Entities: filteredentities, value: Data.treePayload })
      });
  }
  RemoveFilterFromTree(data) {
    this.sendDataToAngularTree(data.treeName[0], TreeOperations.RemoveFilter, '');
  }

  filterTreeData(Data) {
    let filterData = Data.treePayload;
    let EntityType = Object.keys(filterData.EntityTypes).filter(d => filterData.EntityTypes[d]).reduce((obj, key) => { obj[key] = filterData.EntityTypes[key]; return obj; }, {});
    let EntityTypes = Object.keys(EntityType)
    let filterFunc = d => EntityTypes.includes(d.EntityType);
    var types = [TypeOfEntity.Company, TypeOfEntity.Office, TypeOfEntity.Person]
    this.commonService.getEntititesFromStore$("EntityType", types, d => true,
      d => { return d },
      FABRICS.PEOPLEANDCOMPANIES)
      .pipe(untilDestroyed(this, 'destroy')).first()
      .subscribe((filteredentities: any) => {
        this.sendDataToAngularTree(Data.treeName[0], TreeOperations.filterTree, { Entities: filteredentities, filterFunc: filterFunc })
      });
  }
  generateContextMenuJson(data) {
    try {
      var typeOfEntity = data.EntityType;
      let list = [];
      let createOptionInContextMenu = 0;
      switch (typeOfEntity) {
        case TypeOfEntity.Company:
        case TypeOfEntity.Office:
          let option = { "displayName": "Create Entity", "children": [] }
          list.push(option);
          break;
      }
      let commonOption = { "displayName": "Delete Entity", "children": [] };
      list.push(commonOption);
      let payload = {
        "fabric": FABRICS.PEOPLEANDCOMPANIES.toLocaleUpperCase(),
        "dataType": "treeFabricConfigData",
        "tab": this.commonService.treeIndexLabel,
        "Payload": {
          "contextMenuOption": [
            {
              "displayName": "People Context Menu",
              "children": list
            }
          ]
        }
      };
      if (this.listcontext) {
        this.sendDataToAngularTree(this.commonService.treeIndexLabel, TreeOperations.treeContextMenuOption, payload.Payload.contextMenuOption)
        this.commonService.activeContextMenuNodeId = data.EntityId;
      }
    }
    catch (e) {
      console.error('Exception in generateContextMenuJson() of People-activities-service at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  updateCompanyNameState(entity) {
    try {
      let key = "EntityName" + "_Company" + "_" + FABRICS.PEOPLEANDCOMPANIES
      let existingdata = this.fdcStateQuery.getStateKey(key);
      let entities = [];
      if (existingdata) {
        entities = [...existingdata, entity];
        //existingdata.push(entity);
      }

      if (entities.length > 0)
        this.fdcStateQuery.add(key, entities);
    }
    catch (e) {
      console.error(e)
    }
  }

  /***
 * Reading Company Name  Options Data
 */
  getCompanyNameOptions$(Key, type, fabric) {
    try {
      return this.fdcStateQuery
        .select(state => state[Key])
        .pipe(switchMap((hashCache: any) => {
          let newState;
          let apiCall;
          let payload = {};
          if (!(hashCache && hashCache.length != 0)) {
            let message: any = this.payloadForReadingOptionsData(Key, type, fabric);
            apiCall = this.commonService.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
              .pipe(map((res: string) => {

                let msg = typeof res === 'string' ? JSON.parse(res) : res;
                let data = null;
                if (msg.Payload != null && msg.Payload != "") {
                  let payload = JSON.parse(msg.Payload);
                  data = payload["ListsData"] ? payload["ListsData"] : payload;
                  if (data.length) {
                    this.fdcStateQuery.add(Key, data);
                  }
                }
                payload = { "key": Key, "data": data }
                return payload;
              }));
          }
          if (hashCache && hashCache.length != 0) {
            payload = { "key": Key, "data": newState = JSON.parse(JSON.stringify(hashCache)) }
          }
          return newState ? of(payload) : apiCall;
        }));
    }
    catch (Ex) {
      console.error(Ex);
    }
  }
  payloadForReadingOptionsData(Key, type, fabric) {
    var PayLoad = JSON.stringify({});
    let message = this.commonService.getMessage();
    message.Payload = PayLoad;
    message.DataType = "ReadOptions";
    message.EntityID = Key;
    message.EntityType = type;
    message.MessageKind = MessageKind.READ;
    message.Routing = Routing.OriginSession;
    message.Type = "Details";
    message.CapabilityId = this.commonService.getCapabilityId(fabric);
    message.Fabric = fabric.toUpperCase();
    return message;
  }
  initializeCommonFields(messagePayload) {
    messagePayload.TenantName = this.commonService.tenantName;
    messagePayload.TenantId = this.commonService.tenantID;
    messagePayload.ModifiedBy = this.commonService.CurrentUserEntityName;
    messagePayload.ModifiedDateTime = new Date().toISOString().slice(0, 19).replace("T", " ");
    if (!messagePayload.CreatedDateTime)
    messagePayload.CreatedDateTime = messagePayload.ModifiedDateTime;
    if (!messagePayload.CreatedBy) {
      messagePayload.CreatedBy = this.commonService.currentUserName;
    }
    switch (messagePayload.EntityType) {
      case EntityTypes.COMPANY:
        messagePayload.Parent = null;
        messagePayload.ParentType = null;
        break;
      // case EntityTypes.OFFICE:
      //   entityData.Parent = entityData.Company;
      //   entityData.ParentType = EntityTypes.COMPANY;
      //   break;
      case EntityTypes.PERSON:
        // entityData.Parent = entityData.Company;
        messagePayload.ParentType = EntityTypes.COMPANY;
        break
      default:
        break;
    }
    return messagePayload;
  }

  sendDataToAngularTree(treeName: string, treeOperationType: string, payload: any) {
    var treeMessage: AngularTreeMessageModel = {
      "fabric": FABRICS.PEOPLEANDCOMPANIES.toLocaleUpperCase(),
      "treeName": [treeName],
      "treeOperationType": treeOperationType,
      "treePayload": payload
    }

    this.commonService.sendDataToAngularTree.next(treeMessage);
  }
  createPeopleData(data) {
    try {
      let entityData = data["data"];
      let country;
      let EntityName;
      this.EntityId = this.commonService.GetNewUUID();
      if (entityData.EntityType == "Company") {
        this.EntityType = "Company"
        let nameData = entityData["company"];
        EntityName = nameData.companyname;//entityData.Company["companyname"].toString();
      }
      else {
        this.EntityType = "Person";
        let nameData = entityData["person"];
        EntityName = nameData.firstname + nameData.lastname;
        entityData.FirstName = nameData.firstname;
        entityData.LastName = nameData.lastname;
        entityData.Parent = nameData.company;
      }
      if (entityData.TimeZone)
        country = this.commonService.getCountryForTimezone(entityData.TimeZone)['name'];

      var messageKind = MessageKind.CREATE;
      // if (this.EntityType == "Company") {
      //   if (entityData["EntityId"] != entityData["EntityName"]) {
      //     this.EntityId = entityData["EntityId"]
      //   }
      // }
      entityData.EntityName = EntityName;
      entityData.EntityId = this.EntityId;
      entityData.EntityType = this.EntityType;
      entityData.CapabilityName = "People & Companies";
      entityData.CapabilityId = this.commonService.getCapabilityId("People & Companies");
      entityData = this.initializeCommonFields(entityData);
      entityData.Country = country;

      let jsonString = JSON.stringify(entityData);
      let payload = {
        "EntityId": this.EntityId, "TenantName": this.commonService.tenantName,
        "TenantId": this.commonService.tenantID, "EntityType": this.EntityType, "EntityName": EntityName,
        "Fabric": this.commonService.lastOpenedFabric.toUpperCase(), "EntityInfoJson": jsonString, "Info": "Admin",
        ProductionDay: entityData.ProductionDay, Type: this.IsEntityMgmtDrag ? "Drag" : ""
      };

      this.updateCompanyNameState({ EntityName: EntityName, EntityId: this.EntityId });
      var payloadString = JSON.stringify(payload);
      let message = this.commonService.getMessage(FABRICS.PEOPLEANDCOMPANIES);
      message.Payload = payloadString;
      message.DataType = FABRICS.PEOPLEANDCOMPANIES;
      message.EntityID = this.EntityId;
      message.EntityType = this.EntityType;
      message.MessageKind = messageKind;
      message.Routing = Routing.AllFrontEndButOrigin;
      message.Type = "Details";
      message.AbortSignal = AbortSignal;
      let messageWrapper = JSON.stringify(message);
      this.commonService.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/PeopleFabric/CreateUpdatePeopleEntities', {}, messageWrapper).subscribe((res: any) => {
        if (res["Data"]) {
          if (res["Data"]["Data"] == "Unauthorized") {
            console.log(res);
          }
        } else {

          console.log(res);
        }
      });
      //this.commonService.sendMessageToServer(payloadString, FABRICS.PEOPLEANDCOMPANIES, this.EntityId, this.EntityType, messageKind, Routing.AllFrontEndButOrigin, "Details", FABRICS.PEOPLEANDCOMPANIES);
    }
    catch (error) {

    }

  }

  saveIntoDB(event) {
    try {
      let eventData = event && event.data && event.data.EntityType?event.data[event.data.EntityType.toLowerCase()]:null;
      if(eventData) {
        eventData["EntityType"] = event.data.EntityType;
        eventData["EntityId"] = event.data.EntityId;
        var messageKind = event.MessageKind;

        if(eventData["EntityType"] && eventData["EntityType"].toLowerCase() == "company"){
          eventData["EntityName"] = eventData["companyname"];
        }
        else {
          eventData["Parent"] = eventData["company"];//company id
          eventData["EntityName"] = eventData["firstname"]+" "+ eventData["lastname"];
        }

        //if(messageKind==MessageKind.PARTIALUPDATE){
          //event.data["ValueChangedFields"]=JSON.stringify({EntityName:eventData["EntityName"]})
         // this.commonService.getCompaniesFromStore$
        //}
                
        
        let payload = {
          "EntityId":eventData.EntityId,
          "TenantName": this.commonService.tenantName,
          "TenantId": this.commonService.tenantID,
          "EntityType": eventData.EntityType,
          "Fabric": this.commonService.lastOpenedFabric.toUpperCase(), 
          "Payload":"",
          "Info": "Admin",
          "Parent":eventData["Parent"],
          "EntityName":eventData["EntityName"],
          ProductionDay: eventData.ProductionDay,
          Type: this.IsEntityMgmtDrag ? "Drag" : ""
        };

        payload = this.initializeCommonFields(payload);

        event.data["Parent"]=payload["Parent"];
        event.data["ParentType"]=payload["ParentType"];

        payload["Payload"] = JSON.stringify(event.data);
        
        var payloadString = JSON.stringify(payload);
        this.commonService.sendMessageToServer(payloadString, FABRICS.PEOPLEANDCOMPANIES, eventData.EntityId, eventData.EntityType, messageKind, Routing.AllFrontEndButOrigin, "Details", FABRICS.PEOPLEANDCOMPANIES);
    
      }
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in SaveIntoDb() of PeopleService in EntityMgmt People at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  bindPeopleDropDowns(event,column,tab){
    switch(column){
      case "company":
        this.commonService.getCompaniesFromStore$().first().subscribe((res:any) => {
          if (res && res.length) {
           res.sort ((firstEntity, secondEntity) =>firstEntity["entityname"].toLowerCase() > secondEntity["entityname"].toLowerCase() ? 1 : -1)
           event.callback.next(res);
          }
        });
      break;

      case "timezone":
        let timezoneObj = this.commonService.getAllTimeZone();
        let timeZoneList: any[] = [];

        for (const key in timezoneObj) {
          if (timezoneObj.hasOwnProperty(key)) {
            const tempData: any = {};
            tempData.entityid = timezoneObj[key].name + ' (' + timezoneObj[key].utcOffsetStr + ')';
            tempData.entityname = tempData.entityid;
            
            timeZoneList.push(tempData);
            timeZoneList.sort((firstEntity, secondEntity) =>firstEntity["entityname"].toLowerCase() > secondEntity["entityname"].toLowerCase() ? 1 : -1)

          
          }
        }
        timezoneObj = Object.assign({}, timeZoneList);
        event.callback.next(timeZoneList)
        break;

      case "role":
        let fabric = this.commonService.getFabricNameByTab(tab);
        let CapabilityId = this.commonService.getCapabilityId(fabric);
        let imessageModel: IModelMessage = this.commonService.getMessageWrapper( {DataType : DataTypes.FORMSCHEMAROLES,EntityType : MessageEntityTypes.ALLROLES,MessageKind : MessageKind.READ,Payload : JSON.stringify({ Capability: fabric,CapabilityId : CapabilityId})});
          this.commonService.getFormSchemaDataFromBackend(imessageModel, AppConsts.READING).subscribe(data => {
          this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, '', AppConsts.LOADER_MESSAGE_DURATION);
          var rolesArray = [];
          if (data && data.length) {
            data.forEach(role => {
              let roleObj = {entityname : role.EntityName,entityid : role.EntityId ,entittype:role.EntityType}
              rolesArray.push(roleObj);
            });
          } 
          event.callback.next(rolesArray);
        });
        break;    
  }
  }

}
