import { Component, OnInit, Input, OnDestroy, ViewChild, EventEmitter, Output } from '@angular/core';
import { JsonSchemaFormComponent } from '@visur/formgenerator-core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil, filter } from 'rxjs/operators';
import { CommonService } from '../../../../globals/CommonService';
import { IModelMessage, MessageDataTypes, MessageModel, Routing } from '../../../../globals/Model/Message';
import { EntityTypes } from 'ClientApp/app/components/globals/Model/EntityTypes';
import { Datatypes, MessageKind, FABRICS, Tabs, TypeOfEntity} from 'ClientApp/app/components/globals/Model/CommonModel';
import { PeopleActivitiesService } from '../../services/people-activities-service';
import { isObject } from '@datorama/akita';
import { AppConsts } from 'ClientApp/app/components/common/Constants/AppConsts';
import { setData } from 'ClientApp/app/components/globals/helper.functions';
import { Mandatory_Message, MISSING_OR_NON_VALID_DATA, PERSON_EMAIL_ADDRESS_ALREADY_EXIST } from 'ClientApp/app/components/common/Constants/AttentionPopupMessage';
import { PopupOperation } from 'ClientApp/app/components/globals/Model/AlertConfig';
import { PopUpAlertComponent } from 'ClientApp/app/components/common/PopUpAlert/popup-alert.component';
import { FabricRouter } from 'ClientApp/app/components/globals/Model/FabricRouter';

const defaultOptions: any = {
    addSubmit: false, // Add a submit button if layout does not have one
    debug: false, // Don't show inline debugging information
    loadExternalAssets: true, // Load external css and JavaScript for frameworks
    returnEmptyFields: false, // Don't return values for empty input fields
    setSchemaDefaults: true, // Always use schema defaults for empty fields
    defautWidgetOptions: { feedback: true,hideRequired:true }, // Show inline feedback icons,
    framework: 'material-design',
    selectedLanguage: 'en',
    navigateBackOnSubmit: true,
    ngSelectDefaultSearchable: true,
    dynamicCss:false,
    hideToggle:false,
    togglePosition:'before',
    dataTableHeaderHeight: 42,
    dataTableRowHeight: 42,
    floatlabel:true,
    validateOnRender:true
};

@Component({
    selector: 'people-json-schema-form-generator',
    templateUrl: './people-json-schema-form-generator.component.html',
    styleUrls: ['./people-json-schema-form-generator.scss']
})
export class PeopleJsonSchemaFormGeneratorComponent implements OnInit, OnDestroy {
    @Input() header: any;
    @Input() layout: any;
    @Input() data: any;
    @Input() options: any;
    @Input() schema: any;
    @Input() roles: any;
    @Input() images: any;
    @Input() userRoles: any;
    @Input() operationType;
    @Input() schemaPage = null;
    @Input() tableName = null;
    @Input() nestedSchema;
    @Input() search;
    @Input() parentPage;
    @Input() readDataFromOtherSource;
    @Input() queries;
    @Input() updateData;
    @Input() isCompleted = false;
    @Input() BACKEND_SCHEMA_JSON;
    @Input() BACKEND_JSON_DATA;
    @Input() breadcrumbDisplayName;
    @Input() formulas
    @Input() title;
    @Input() $state;
    @Input() jsonData;
    @Input() IsEntityManagementForm;
    @Input() tab;
    @Input() EntityType;
    @Input() EntityId;
    @Input() EntityName;
    @Input() IsEntityMgmtDrag;
    @Input() entityMgmtCommonInputVariable;
    @Input() isNewEntity;
    @Input() LocationData;
    @Input() myForm;
    @Input() lookup;
    @Output() applicationEvent = new EventEmitter();

    @ViewChild(JsonSchemaFormComponent) formGenerator: JsonSchemaFormComponent;

    takeUntilDestroyObservables = new Subject();
    popupHeader: any;
    popupFormType: string;
    livedata: any;
    expanded = false;
    parentEntityId;
    isPopupForm = false;
    isEmailValid  = true;
    /// assgin value to menuid. if the page is root/first page/initial page
    menuId;
    previousRoute: string;
    headerControl = 'headerSelect';
    dialogRef: MatDialogRef<any, any>;
    adminSchema;
    childRouteParameters: any;
    ValueChangedFields = {};
    conditionalFields:any[];
    @Input() treeIndexTab: any;
    constructor(private peopleService: PeopleActivitiesService, private route: ActivatedRoute, private commonService: CommonService, public dialog: MatDialog, private matDialog: MatDialog, public router: Router) {
        try {
            this.commonService.triggerSubmitOnFormGenerator$.pipe(this.compUntilDestroyed()).subscribe((res) => {
                if (!this.isCompleted) {
                    this.formGenerator.submitForm();
                }
            });

            this.peopleService.PeopleCommonObservable.pipe(
                filter((data: any) =>
                    data.Type == "AddNewForm" && data.MessageKind == MessageKind.READ && this.EntityId == data.EntityID))
                .pipe(this.compUntilDestroyed())
                .subscribe((message: MessageModel) => {
                    this.addNewSchemaSubscription(message);
                });

            this.peopleService.PeopleCommonObservable.pipe(
                filter((data: any) =>
                    data.Type != "AddNewForm" && data.MessageKind == MessageKind.READ && this.EntityId == data.EntityID))
                .pipe(this.compUntilDestroyed())
                .subscribe((message: MessageModel) => {
                    this.landingPageSchemaSubscription(message);
                });
        this.commonService.visurUser.pipe(this.compUntilDestroyed())
        .subscribe((res:any) => {
            if(res.EntityType == "Company" || res.EntityType == "Person"){
                this.myForm.controls['EntityName'].setValue(res.EntityName,{emitEvent:false})
            }else{
            this.livedata[Tabs.SECURITY].scheduleinvite = this.livedata[Tabs.SECURITY].scheduleinvite.toString().slice(0, 10);
            res.scheduleinvite = res.scheduleinvite.toString().slice(0, 10);
            this.updateData = setData({'security':res},this.livedata)
            }
         
        });
            this.commonService.emFormSubmitEvent
                .pipe(filter((d: any) => d && (d.id == "save" || d.id =="close") && d.tree == this.treeIndexTab))
                .pipe(this.compUntilDestroyed())
                .subscribe((res: any) => {
                    if(res.id == "close"){
                        this.commonService.isclose = true;
                    }
                    this.formGenerator.submitForm();
                })
            
                this.commonService.headerExpandCollapseEvent
                .pipe(filter((d: any) => d && d.id == "expandCollapseAll" && d.tree == this.treeIndexTab))
                .pipe(this.compUntilDestroyed())
                .subscribe((res: any) => {
                    this.formGenerator.setLayoutOptions(res.value);
                })


        }
        catch (e) {
            console.error('Exception in constructor of people-json-schema-form-generator in People at time ' + new Date().toString() + '. Exception is : ' + e);
        }
    }

    ngOnInit() {
        this.filterOptionsFormFormGenerator();

        this.route.params.pipe(this.compUntilDestroyed()).subscribe(params => {
            this.childRouteParameters = JSON.parse(JSON.stringify(params));
        });
        this.readPeopleSchema();
    }
    ngAfterViewInit() {
    }

    compUntilDestroyed(): any {
        return takeUntil(this.takeUntilDestroyObservables);
    }

    filterOptionsFormFormGenerator() {
        try {
            if(this.isNewEntity){
                defaultOptions.hideToggle = true;
            }else{
                defaultOptions.hideToggle = false;
                defaultOptions.dynamicCss = true;
            }
            this.options = defaultOptions;
            this.adminSchema = this.BACKEND_SCHEMA_JSON;
            //  this.adminSchema=JSON.parse(this.adminSchema);
            if (this.adminSchema) {
                this.layout = this.adminSchema.layout;
                this.schema = this.adminSchema.schema;
                this.conditionalFields = this.adminSchema.conditionalFields;
                this.roles = this.adminSchema.roles;
                this.images = this.adminSchema.images;
                this.options = this.getDefaultOptions(this.adminSchema.options);
                this.nestedSchema = this.adminSchema.nestedSchema;
                this.tableName = this.adminSchema.tableName;
                this.readDataFromOtherSource = this.adminSchema.readDataFromOtherSource;
                this.parentPage = this.adminSchema.parentPage;
                this.queries = this.adminSchema.queries;
                this.formulas = this.adminSchema.formulas
                this.lookup = this.adminSchema.lookup
                if(!this.commonService.isCapbilityAdmin()){
                    this.userRoles = [this.commonService.getRoleName(this.commonService.getFabricNameByUrl(this.router.url))];
                  }
                this.breadcrumbDisplayName = this.adminSchema.breadcrumbDisplayName;
                this.title = this.adminSchema.title ? this.adminSchema.title.toUpperCase() : this.adminSchema.title;
                this.$state = this.adminSchema.$state;
                if (this.BACKEND_JSON_DATA) {

                    if (Object.keys(this.BACKEND_JSON_DATA).length > 0) {
                        this.data = this.createTabDataPayload();//listData[0];
                    } else {
                        this.data = {};
                    }
                }
                let uom = this.adminSchema.unitsOfMeasurement;
            }
        } catch (e) {
            console.error(e);
        }
    }

    createTabDataPayload() {
        let payload = JSON.parse(this.BACKEND_JSON_DATA.Payload);
        this.BACKEND_SCHEMA_JSON.layout[0].items[0].title = payload.EntityName?payload.EntityName:payload[this.EntityType.toLowerCase()].EntityName?payload[this.EntityType.toLowerCase()].EntityName:null;

        return payload;
    }

    getDefaultOptions(options: any) {
        if (options) {
            for (const key of Object.keys(options)) {
                this.options[key] = options[key];
            }
        }
        return this.options;
    }
    onChange(event) {
        this.livedata = event;
        console.log('onChange() :: FormGenerator :: event :: ', event);
    }
    async click(event) {
        let buttonEvent: string = event.buttonEvent ? event.buttonEvent.toLowerCase() : null;
        let jsonPayload = {};

        switch (buttonEvent) {
            case "value_changes":
                this.ValueChanges(event);
                break;
            case "tab_change":
                this.onTabChange(event);
                break;    
            // case "PopUp":
            //   // case FilterEvent:
            //   this.commonService.presentStatusMessageAsync(Loading);
            //   let obs$ = this.formService.openPopup(this.nestedSchema, event, this.schema, this.wrapper.EntityID, this.schemaPage);
            //   obs$.subscribe(dialogRef => {
            //     this.dialogRef = dialogRef;
            //     this.dialogRef.afterClosed().pipe(
            //       finalize(() => this.dialogRef = undefined)
            //     );
            //   });
            //   break;

            case "close":
                this.closePopup(this.dialogRef);
                break;

            case "inline":
                this.addInlineRow(event);
                break;

            case "submit":
                //this.formService.updateFormValueBasedOnSelectedRecord(this.search, this.livedata, this.parentPage, this.schemaPage, this.schema);
                this.closePopup(this.dialogRef);
                break;

            case "search":
                jsonPayload['parentPage'] = this.parentPage;
                jsonPayload['schemaPage'] = this.schemaPage;
                jsonPayload['data'] = this.livedata;
                jsonPayload['search'] = this.search;

                // //this.formService.openSearch(event, jsonPayload).pipe(untilDestroyed(this)).subscribe((res) => {
                //   //this.updateData = res;
                // });
                break;

            // case BARCODESCAN:
            //   const node: any = getNode(event.dataPointer, this.schema);
            //   this.search = this.nestedSchema[node.page];
            //   // this.formService.updateFormValueBasedOnSelectedRecord(this.search, this.livedata, this.parentPage, this.schemaPage, this.schema);
            //   break;

            // case BARCODESEARCH:
            //   this.formService.bindBarcodeSearchData(event, this.schema, this.nestedSchema, this.schemaPage, this.livedata);
            //   break;

            case "route":
            case "routing":
                // for add popup form, on routing closing the popup..
                this.closePopup(this.dialogRef);

                // send page id as menu id if page is first/inital page
                //      let id = this.wrapper.EntityID ? this.wrapper.EntityID : this.menuId;
                //let id = this.wrapper.EntityID ? this.wrapper.EntityID : "0";

                // for AddPopup page message kind will come as create
                // if (this.wrapper.MessageKind == MessageKinds.CREATE) {
                //   id = this.parentEntityId ? this.parentEntityId : "0";
                // }
                // let breadCrumbjson = this.formService.getbreadcrumbPayloadJson(this.route);
                // breadCrumbjson['breadcrumbDisplayName'] = this.breadcrumbDisplayName;
                // this.formService.route(event, this.schema, this.livedata, id, this.parentEntityId, this.wrapper, breadCrumbjson);
                break;

            case "copy previous":
                // this.formService.copyPrevious(event, this.schema, this.livedata, this.queries).pipe(untilDestroyed(this)).subscribe(data => {
                //   if (data) {
                //     //this.updateData[data.datapointer] = data.items;
                //     data.items = modifiedExistingArraydata(data.type, data.items);
                //     let dataPointer = getInlineDataPointer(this.schema, event.dataPointer);
                //     this.formGenerator.insertItems(dataPointer, data.items);
                //   }
                //   else {
                //     //popup alert need to implement
                //   }
                // });
                break;

            // case FAVORITE:
            //   // this.formService.updateFavoriteColumnAsync(event, this.schema)
            //   //   .then(() => 'Success');
            //   break;

            case "back":
                // this.formService.routeTo(this.previousRoute);
                break;

            // case INLINE_GPS:
            //   this.commonService.presentStatusMessageAsync(Loading);
            //   const tempData: any = await this.formService.updateDefaultGPS(event, this.schema);
            //   this.formGenerator.insertItems(tempData.dataPointer, tempData.data);
            //   this.commonService.dismissStausMessage();
            //   break;

            // case CAMERA:
            //   this.commonService.presentStatusMessageAsync(Loading);
            //   const temp: any = await this.formService.updateDefaultImage(event, this.schema);
            //   this.formGenerator.insertItems(temp.dataPointer, temp.data);
            //   this.commonService.dismissStausMessage();
            //   break;

            default:
                this.emitOutputEvent("click", event);
                break;
        }
    }
    emitOutputEvent(type: string, data: any) {
        this.applicationEvent.emit({ "type": type, data: data });
    }
    closePopup(dialogRef: MatDialogRef<any, any>, event?) {
        if (dialogRef != null) {
            dialogRef.close();
        }
        else {
            this.matDialog.closeAll();
        }
        switch (this.popupFormType) {
            case 'filter':
                break;

        }
        //this.logger.debug(event);
    }
    addInlineRow(event: any) {
        //let dataPointer = this.formService.getInlineDataPointer(this.schema, event.dataPointer);
        //this.formGenerator.insertItems(dataPointer, []);
    }

    ValueChanges(event) {
        if(event && event.dataPointer && event.dataPointer.includes("securitypermissions")){
          var dplist = event.dataPointer.split('/'); 
          if(dplist.length > 0){
             var fieldName = dplist[dplist.length-1];  
             switch(fieldName){
                case 'status':
                    this.onSecuritytogglechange(event,event.parentControlData)
                    break;  
            }
          }
        }
        else if(event && event.dataPointer && event.layoutNodeDataPointer && event.layoutNodeDataPointer.length) {
            let tabName = event.layoutNodeDataPointer[0];
            let fieldName = event.layoutNodeDataPointer[1];
            if(this.ValueChangedFields && this.ValueChangedFields.hasOwnProperty(tabName)){
                this.ValueChangedFields[tabName][fieldName] = event.value;
            }
            else{
                let fieldObj=this.getDefaultData(tabName);
                fieldObj[fieldName] = event.value
                this.ValueChangedFields[tabName] = fieldObj;
            }
            if(!this.livedata[tabName]){
                this.livedata[tabName] = {};
            }
            switch(fieldName){
                case 'email':
                    //validate email
                if(this.isNewEntity){
               this.checkEmailAlreadyExistsOrNot(fieldName,event.value);
        }
                    break;
                case 'role':
                        if(this.tab && (!event.value) && this.livedata[tabName]['roleid']&& this.tab.toLowerCase() == tabName.toLowerCase()){
                            this.livedata[tabName]['roleid'] = null;
                            this.ValueChangedFields[tabName]['roleid'] = null;
                            this.updateData = {[tabName]: this.livedata[tabName]}
                        }
                    break;       
                case 'admin':
                    if(event.value && this.livedata[tabName]['role'] && this.tab && this.tab.toLowerCase() == tabName.toLowerCase()){
                        this.livedata[tabName]['role'] = false;
                        this.livedata[tabName]['roleid'] = null;
                        this.ValueChangedFields[tabName]['role'] = false;
                        this.ValueChangedFields[tabName]['roleid'] = null;
                        this.updateData = {[tabName]: this.livedata[tabName]}
                    }
                    break;  
                case 'invite':
                    if(this.tab &&  this.tab.toLowerCase() == Tabs.SECURITY.toLowerCase() && Object.keys(this.livedata[Tabs.SECURITY]).length>0){
                    let country = this.commonService.getTimezoneDetails(this.livedata[Tabs.PERSON].timezone);
                    let Entityid = this.commonService.getEntityIDFromUrl(this.router.url)
                    this.livedata[Tabs.SECURITY].scheduleinvite = new Date(this.livedata[Tabs.SECURITY].scheduleinvite).toISOString();
                    let Payload = {'security':this.livedata[Tabs.SECURITY],'person':this.livedata[Tabs.PERSON], 'FormTabFabric':this.tab,'OperationType':'invite', 'TenantName': this.commonService.tenantName, 'TenantId': this.commonService.tenantID, 'EntityType': TypeOfEntity.Person,  Type: MessageKind.PARTIALUPDATE ,Country:country.name};
                    this.commonService.sendMessageToServer(JSON.stringify(Payload), FABRICS.PEOPLEANDCOMPANIES, Entityid,'Person', MessageKind.PARTIALUPDATE, Routing.AllFrontEndButOrigin, 'Details', FABRICS.PEOPLEANDCOMPANIES);
                    }
                    break;
                case 'scheduleinvite':
                    if(this.tab &&  this.tab.toLowerCase() == Tabs.SECURITY.toLowerCase() && Object.keys(this.livedata[Tabs.SECURITY]).length>0){
                    this.livedata[Tabs.SECURITY].scheduleinvite = this.livedata[Tabs.SECURITY].scheduleinvite.toString().slice(0, 10);
                    this.updateData = {[tabName]: this.livedata[tabName]};
                }
                    break;

            }
        }
    }

    getDefaultData(tabName:string){
        var fieldObj = {};
        let CapabilityName = this.commonService.getCapabilityNameBasedOnTab(tabName);
        if(CapabilityName){
         fieldObj["CapabilityName"] = CapabilityName;
         fieldObj["CapabilityId"] = this.commonService.getCapabilityId(fieldObj["CapabilityName"]);
         fieldObj["UserId"] = this.EntityId;
        }
        return fieldObj;
    }
    
    onSubmitFn(event) {
        let data = event ? event.data : null;
     if(this.isEmailValid){
        if (data && isObject(data) && this.isNotEmpty(data) && event.isValid && !this.commonService.isclose) {
            //clean up data
            for (let key of Object.keys(data)) {
                if (!data[key]) {
                    delete data[key];
                }
            }
            // delete json data if we are reading data from other tables..
            if (this.readDataFromOtherSource) {
                let keys = Object.keys(this.readDataFromOtherSource);
                for (let key of keys) {
                    key = key.toLowerCase();
                    if (data[key]) {
                        delete data[key];
                    }
                }
            }

            if (!this.EntityType && this.myForm)
                this.EntityType = this.myForm.getRawValue().EntityType;

            data["EntityType"] = this.EntityType;
            data["EntityId"] = this.EntityId;
            if (this.isNewEntity) {
                event["MessageKind"] = MessageKind.CREATE;
            }
            else {
                event["MessageKind"] = MessageKind.PARTIALUPDATE;
                if(event.data){
                    event.data.ValueChangedFields = JSON.stringify(this.ValueChangedFields);
                }          
            }
            this.peopleService.saveIntoDB(event);
            this.ValueChangedFields = {};
        }else if(!event.isValid){
            let message = "";
            let isubmit = false;
            let operation;
            if(this.commonService.isclose){
             message = Mandatory_Message
             operation = PopupOperation.AlertConfirm;
             this.commonService.isclose = false;
             isubmit = true;
            }else{
                message = MISSING_OR_NON_VALID_DATA
            }
            this.openDialog(message,operation,isubmit);
        }else if(this.commonService.isclose){
            this.CloseForm();
            this.commonService.isclose = false;
        }
    }
    }
    isNotEmpty(data) {
        try {
            let json = JSON.stringify(data)
            if (json != "{}" && Object.keys(data).length > 0)
                return true;
            else
            return false;
        }
        catch (e) {
            return false;
        }
    }
    lookUpQuery(event) {
        //this.formService.lookUpQuery(event);


        let dataPointer = event.dataPointer //: "/person/company"
        let lookup =  event.lookup ;//: {displayName: "entityname", name: "company", bindValue: "entityid", callback: BehaviorSubject, processing: true}
        let lookupName = event.lookupName; //: "company"
        this.peopleService.bindPeopleDropDowns(lookup, lookupName,this.tab);


    }



    enumValueUpdate(event) {
        //let schemaPage = getTableName(this.schemaPage);
        //this.formService.enumValueUpdate(schemaPage, event);
    }
    getTableName() {
        return this.schemaPage.replace(/ /g, '');
    }
    onToggleChange(data) {
        console.log(data);
        switch (data.buttonEvent) {
            case 'themeChange':
                //this.document.body.classList.toggle('dark', data.event.checked);
                break;
        }
    }
    treeAction(event) {
        // this.formService.treeAction(event).pipe(untilDestroyed(this)).subscribe(data => {
        //   setTimeout(() => {
        //     event.callback.next(data);
        //     (<Subject<any>>event.callback).complete();
        //   }, 0
        //   );
        // });
    }
    setData(data) {
        if (data != null && Object.keys(data).length > 0) {
            //this.updateData = setData(data, this.livedata);
            //this.formService.updateState(this.$state, this.schemaPage, this.updateData);
        }
        //this.commonService.dismissStausMessage();
    }
    // submitForm() {
    //   this.formGenerator.submitForm();
    // }
    async onFilterFn(event) {
        const data: any = JSON.parse(JSON.stringify(event));
        let schemaPage = this.getTableName();
        data.schemapage = schemaPage;
        //await this.formService.filterUpsertAsync(data);
    }

    isValid(event) {
        console.log("isValid method");
        console.log(event);
    }
    validationErrors(event) {
        console.log("In Valida Fields :");
        console.log(event);
    }

    readPeopleSchema() {
        let payload: any = {};
        payload.EntityId = this.EntityId;
        payload.EntityType = this.EntityType
        if (this.isNewEntity || this.IsEntityMgmtDrag) {

            payload.isNewEntity = this.isNewEntity;
            payload.Info = "AddNewForm";
            payload.DataType = FABRICS.PEOPLEANDCOMPANIES;
            payload.FormCapability = FABRICS.PEOPLEANDCOMPANIES;
        }
        else {
            // payload.EntityId = this.EntityId
            // payload.EntityType = this.EntityType
            payload.Info = 'Details';
            payload.DataType = EntityTypes.EntityInfo;
            if (this.EntityType == "Person") {
                payload.FormCapability = FABRICS.PEOPLEANDCOMPANIES;
                let capabilityid = this.commonService.getCapabilityId(this.tab);
                payload.CapabilityId = capabilityid ? capabilityid : "";
            }
            else {
                payload.FormCapability = this.EntityType !== 'Person' && this.tab === 'Person' ? 'People' : this.tab;

                if (payload.FormCapability === '' && this.tab === '' && this.myForm && this.myForm.value && this.myForm.value.tab === 'Companies/People') {
                    payload.FormCapability = 'People';
                }

                if (payload.FormCapability === 'Security' && this.commonService.isNextOrPreviousClicked) {
                    this.commonService.isNextOrPreviousClicked = false;
                    payload.FormCapability = 'People'
                } else {
                    this.commonService.isNextOrPreviousClicked = false;
                }
            }
        }
        payload.FormType = "Configuration";
        //   this.expandCollapse = true;
        payload.Fabric = this.commonService.lastOpenedFabric;
        this.commonService.sendMessageToServer(JSON.stringify(payload), FABRICS.PEOPLEANDCOMPANIES, payload.EntityId, this.EntityType, MessageKind.READ, Routing.OriginSession, payload.Info, FABRICS.PEOPLEANDCOMPANIES);

    }

    addNewSchemaSubscription(message: MessageModel) {
        console.log(message);
        let schemaData = JSON.parse(message["Payload"]);
        this.BACKEND_SCHEMA_JSON = JSON.parse(schemaData["Schema"].toString());
        this.BACKEND_SCHEMA_JSON.layout[0].items[0].title = this.EntityType;

        if (this.EntityType == "Person" && this.isNewEntity) {
            // this.BACKEND_SCHEMA_JSON.layout[0].items[0].type = 'div';

            this.BACKEND_SCHEMA_JSON.layout[0].items[0].items.forEach(item => {
                if (item.tabIcon == "Person")
                    item["disabled"] = false;
                else
                    item["disabled"] = true;
            })

        }
        else if (this.EntityType == "Company" && this.isNewEntity) {

            this.BACKEND_SCHEMA_JSON.layout[0].items[0].items.forEach(item => {
                if (item.tabIcon == "Company")
                    item["disabled"] = false;
                else
                    item["disabled"] = true;
            })
        }
        this.filterOptionsFormFormGenerator()
    }

    landingPageSchemaSubscription(message: MessageModel) {
        let schemaData = JSON.parse(message["Payload"]);
        this.BACKEND_JSON_DATA = JSON.parse(schemaData["Data"] ? schemaData["Data"].toString() : "{}");

        this.BACKEND_SCHEMA_JSON = JSON.parse(schemaData["Schema"].toString());
        this.BACKEND_SCHEMA_JSON.layout[0].items[0].items.forEach(item => {
            item["disabled"] = false;
        })




        this.filterOptionsFormFormGenerator();
        if(this.data && this.data[Tabs.SECURITY] && Object.keys(this.data[Tabs.SECURITY]).length>0){
            this.data[Tabs.SECURITY].scheduleinvite = this.data[Tabs.SECURITY].scheduleinvite.toString().slice(0, 10);
            this.updateData = {[Tabs.SECURITY]:this.data[Tabs.SECURITY]}
            this.updateData[Tabs.PERSON] = this.data[Tabs.PERSON]
        }
    }

    onTabChange(event){
        let tabName = event.layoutNode.options.title;
        this.tab = tabName;
        let fabric = this.commonService.getFabricNameByTab(tabName);
        let CapabilityId = this.commonService.getCapabilityId(fabric);
        if(CapabilityId){
        let parentId = this.BACKEND_JSON_DATA.EntityId;
        let entityid = "security&permissions";
        let payload: any = {}   
        payload.EntityId = entityid;
        payload.EntityType = entityid;
        payload.Info = 'Details';
        payload.DataType = "SecurityGroup";
        payload.FormCapability = tabName;
        payload.Fabric = this.commonService.lastOpenedFabric;
        payload.ParentId = parentId;
        payload.CapabilityId = CapabilityId;
        let datatype = Datatypes.USERPERMISSION;
        let imessageModel = this.commonService.getMessageWrapper({Payload:JSON.stringify(payload),DataType:Datatypes.USERPERMISSION,EntityID:entityid,EntityType:entityid,MessageKind:MessageKind.READ});
        this.commonService.postDataToBackend(imessageModel, AppConsts.READING).subscribe(response => {
            this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, "", AppConsts.LOADER_MESSAGE_DURATION);
            if (response) {
                this.generateSecurityPermissionForm(response);
              }
          });
        }
        else{
            this.updateData = setData({"securitypermissions" : []}, this.livedata);
        }
    }

    generateSecurityPermissionForm(res) {
        try {
            var payload = res;
            let securitygroupandrole = payload["securitygroupandrole"];
            let usersecuritygroup = payload["usersecuritygroup"] ? payload["usersecuritygroup"] : [];
            let PermissionArray =[];
            if (securitygroupandrole && securitygroupandrole.length > 0) {
              let SecurityGroup = securitygroupandrole[0]["securitygroup"];
              SecurityGroup.sort((firstEntity, secondEntity) => {
                if (firstEntity && secondEntity && firstEntity['entityname'] && secondEntity['entityname'])
                  return firstEntity['entityname'].toLowerCase() > secondEntity['entityname'].toLowerCase() ? 1 : -1
              });
              SecurityGroup.forEach(item => {
                var tempobj = item;
                let usersg = usersecuritygroup.find(uitem => uitem.entityid == tempobj.entityid)
                if (usersg) {
                    tempobj["status"] = true;
                }
                else {
                    tempobj["status"] = false;
                }
                PermissionArray.push(tempobj);
              });
            }
            this.updateData = setData({"securitypermissions" : PermissionArray}, this.livedata);
        }
        catch (ex) {
    
        }
      }

      
  onSecuritytogglechange(event,data) {  
    let operation = event.value ? "SecurityGroupSelected" : "SecurityGroupUnSelected";
    this.UpdateSecurityGroupbasedonUser(data.entityid,operation)
  }

  UpdateSecurityGroupbasedonUser(SecurityGroupId, operation,Roledata?) {    
    let Roles = [];
    let RemovedRoles = [];
    let Entities = [];
    let RemovedEntities = [];
    let PeopleCompanies = [];
    let RemovedPeopleCompanies = [];
    switch (operation) {
      case "SecurityGroupSelected": {
        if (this.EntityType == EntityTypes.PERSON) {
          PeopleCompanies.push({ "EntityId": this.EntityId, "EntityType": EntityTypes.PERSON })
        }
      }
      break;
      case "SecurityGroupUnSelected": {
        if (this.EntityType == EntityTypes.PERSON) {
          RemovedPeopleCompanies.push({ "EntityId": this.EntityId, "EntityType": EntityTypes.PERSON })
        }
      }
      break;
    }
    let fabric = this.commonService.getFabricNameByTab(this.tab);
    let CapabilityId = this.commonService.getCapabilityId(fabric);
    var payload = {
      "SecurityGroupId": SecurityGroupId, "PeopleCompanies": PeopleCompanies, "Roles": Roles, "RemovedRoles": RemovedRoles, "RemovedPeopleCompanies": RemovedPeopleCompanies, "Entities": Entities, "RemovedEntities": RemovedEntities, "Capability": { "CapabilityId": CapabilityId, "CapabilityName": fabric }
    };
    let imessageModel: IModelMessage = this.commonService.getMessageWrapper({Payload:JSON.stringify(payload),DataType:MessageDataTypes.SECURITYGROUPS,EntityType:EntityTypes.SECURITYGROUP,EntityID:SecurityGroupId,MessageKind:MessageKind.UPDATE});

    this.commonService.postDataToBackend(imessageModel,AppConsts.UPDATING).subscribe(response => {
        this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, AppConsts.UPDATED, AppConsts.LOADER_MESSAGE_DURATION);
      });;
  }

  checkEmailAlreadyExistsOrNot(propertyName:string,email:string) {
    var regex = /^[0-9A-Za-z]+[A-Za-z0-9._]+@[A-Za-z]+\.[A-Za-z.]{2,5}$/;
    if(String(email).match(regex)){
        this.commonService.checkEmailExist(email, propertyName).pipe(this.compUntilDestroyed()).subscribe(
            (response) => {
                if(response!=null||response!=undefined){
                    let result = response.toString().toLowerCase();
                    if(result=="false"){
                        this.commonService.loadingBarAndSnackbarStatus("complete", "validated");
                        this.commonService.loadingBarAndSnackbarStatus("complete", "");
                        this.isEmailValid = true;
                    }
                    else if(result =="true")
                    {
                        this.isEmailValid = false;
                        this.commonService.loadingBarAndSnackbarStatus("complete", "Email address already exists");
                        let message = PERSON_EMAIL_ADDRESS_ALREADY_EXIST;
                        this.peopleService.openDialog(message);
                        setTimeout(() => {
                            this.commonService.loadingBarAndSnackbarStatus("", "");
                        }, 1000);
                    }
                }
          },
          error=>{
            console.error(error)
          }
        );
    } 	
}

openDialog(messageToshow, Operation?,isubmit?): void {
    try {
        let config = {
          header: 'Attention!',
          isSubmit: isubmit ? true : false,
          content: [messageToshow],
          subContent: [],
          operation: Operation != undefined ? Operation : PopupOperation.Attention
        };
        let matConfig = new MatDialogConfig();
        matConfig.data = config;
        matConfig.width = '500px';
        matConfig.disableClose = true;
        matConfig.id = messageToshow;
        if (!this.dialog.getDialogById(messageToshow)) {
          this.dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
           this.dialogRef.componentInstance.emitResponse.subscribe((res) => {
             this.CloseForm();
           });
        }
      }
      catch (e) {
        this.commonService.appLogException(new Error('Exception in  openDialog() of peoplejsonformgenerator at time ' + new Date().toString() + '. Exception is : ' + e));
      }  }
  
  CloseForm() {
    try {
        this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC], { queryParams: { 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in CloseForm() of peoplejsonformgenerator at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

    ngOnDestroy() {
        this.takeUntilDestroyObservables.next();
        this.takeUntilDestroyObservables.complete();

    }
}
